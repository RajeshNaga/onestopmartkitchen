package com.mart.onestopkitchen.databinding;

import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.mart.onestopkitchen.model.PaymentResponseModel;

public abstract class FragmentPaymentDoneSuccessfullyBinding extends ViewDataBinding {
  @NonNull
  public final Button btnDone;

  @NonNull
  public final Button btnPDF;

  @NonNull
  public final ImageView imageView2;

  @NonNull
  public final LinearLayout llBtn;

  @NonNull
  public final RecyclerView rvOrderGroup;

  @NonNull
  public final TableRow taxRow;

  @NonNull
  public final TextView textView2;

  @NonNull
  public final TextView textView4;

  @NonNull
  public final TextView textView6;

  @NonNull
  public final TextView tvAddress;

  @NonNull
  public final TextView tvPhoneNumber;

  @NonNull
  public final TextView tvProductAmount;

  @NonNull
  public final TextView tvRefrenceNumber;

  @NonNull
  public final TextView tvShippingCharge;

  @NonNull
  public final TextView tvTaxCharge;

  @NonNull
  public final TextView tvTotalAmount;

  @NonNull
  public final TextView tvTotalOrderCount;

  @Bindable
  protected PaymentResponseModel mModel;

  @Bindable
  protected Spannable mTotalAmount;

  protected FragmentPaymentDoneSuccessfullyBinding(DataBindingComponent _bindingComponent,
      View _root, int _localFieldCount, Button btnDone, Button btnPDF, ImageView imageView2,
      LinearLayout llBtn, RecyclerView rvOrderGroup, TableRow taxRow, TextView textView2,
      TextView textView4, TextView textView6, TextView tvAddress, TextView tvPhoneNumber,
      TextView tvProductAmount, TextView tvRefrenceNumber, TextView tvShippingCharge,
      TextView tvTaxCharge, TextView tvTotalAmount, TextView tvTotalOrderCount) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnDone = btnDone;
    this.btnPDF = btnPDF;
    this.imageView2 = imageView2;
    this.llBtn = llBtn;
    this.rvOrderGroup = rvOrderGroup;
    this.taxRow = taxRow;
    this.textView2 = textView2;
    this.textView4 = textView4;
    this.textView6 = textView6;
    this.tvAddress = tvAddress;
    this.tvPhoneNumber = tvPhoneNumber;
    this.tvProductAmount = tvProductAmount;
    this.tvRefrenceNumber = tvRefrenceNumber;
    this.tvShippingCharge = tvShippingCharge;
    this.tvTaxCharge = tvTaxCharge;
    this.tvTotalAmount = tvTotalAmount;
    this.tvTotalOrderCount = tvTotalOrderCount;
  }

  public abstract void setModel(@Nullable PaymentResponseModel model);

  @Nullable
  public PaymentResponseModel getModel() {
    return mModel;
  }

  public abstract void setTotalAmount(@Nullable Spannable totalAmount);

  @Nullable
  public Spannable getTotalAmount() {
    return mTotalAmount;
  }

  @NonNull
  public static FragmentPaymentDoneSuccessfullyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPaymentDoneSuccessfullyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPaymentDoneSuccessfullyBinding>inflate(inflater, com.mart.onestopkitchen.R.layout.fragment_payment_done_successfully, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentPaymentDoneSuccessfullyBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPaymentDoneSuccessfullyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPaymentDoneSuccessfullyBinding>inflate(inflater, com.mart.onestopkitchen.R.layout.fragment_payment_done_successfully, null, false, component);
  }

  public static FragmentPaymentDoneSuccessfullyBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentPaymentDoneSuccessfullyBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentPaymentDoneSuccessfullyBinding)bind(component, view, com.mart.onestopkitchen.R.layout.fragment_payment_done_successfully);
  }
}
