package com.mart.onestopkitchen.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.mart.onestopkitchen.model.OrdersModel;
import java.lang.Integer;

public abstract class LayoutOrderGroupReceiptPageBinding extends ViewDataBinding {
  @NonNull
  public final TextView tvOrder;

  @NonNull
  public final TextView tvOrderNumber;

  @Bindable
  protected OrdersModel mOrderModel;

  @Bindable
  protected Integer mPosition;

  protected LayoutOrderGroupReceiptPageBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView tvOrder, TextView tvOrderNumber) {
    super(_bindingComponent, _root, _localFieldCount);
    this.tvOrder = tvOrder;
    this.tvOrderNumber = tvOrderNumber;
  }

  public abstract void setOrderModel(@Nullable OrdersModel orderModel);

  @Nullable
  public OrdersModel getOrderModel() {
    return mOrderModel;
  }

  public abstract void setPosition(@Nullable Integer position);

  @Nullable
  public Integer getPosition() {
    return mPosition;
  }

  @NonNull
  public static LayoutOrderGroupReceiptPageBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static LayoutOrderGroupReceiptPageBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<LayoutOrderGroupReceiptPageBinding>inflate(inflater, com.mart.onestopkitchen.R.layout.layout_order_group_receipt_page, root, attachToRoot, component);
  }

  @NonNull
  public static LayoutOrderGroupReceiptPageBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static LayoutOrderGroupReceiptPageBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<LayoutOrderGroupReceiptPageBinding>inflate(inflater, com.mart.onestopkitchen.R.layout.layout_order_group_receipt_page, null, false, component);
  }

  public static LayoutOrderGroupReceiptPageBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static LayoutOrderGroupReceiptPageBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (LayoutOrderGroupReceiptPageBinding)bind(component, view, com.mart.onestopkitchen.R.layout.layout_order_group_receipt_page);
  }
}
