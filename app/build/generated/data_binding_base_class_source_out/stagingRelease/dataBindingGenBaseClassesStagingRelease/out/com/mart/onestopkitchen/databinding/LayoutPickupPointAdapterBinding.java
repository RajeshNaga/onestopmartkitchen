package com.mart.onestopkitchen.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.mart.onestopkitchen.model.PickupPointItem;
import com.mart.onestopkitchen.ui.adapter.PickupPointAdapter;

public abstract class LayoutPickupPointAdapterBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout llItem;

  @Bindable
  protected PickupPointItem mPickupPointItem;

  @Bindable
  protected PickupPointAdapter.OnListItemClick mItemListener;

  protected LayoutPickupPointAdapterBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout llItem) {
    super(_bindingComponent, _root, _localFieldCount);
    this.llItem = llItem;
  }

  public abstract void setPickupPointItem(@Nullable PickupPointItem pickupPointItem);

  @Nullable
  public PickupPointItem getPickupPointItem() {
    return mPickupPointItem;
  }

  public abstract void setItemListener(@Nullable PickupPointAdapter.OnListItemClick ItemListener);

  @Nullable
  public PickupPointAdapter.OnListItemClick getItemListener() {
    return mItemListener;
  }

  @NonNull
  public static LayoutPickupPointAdapterBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static LayoutPickupPointAdapterBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<LayoutPickupPointAdapterBinding>inflate(inflater, com.mart.onestopkitchen.R.layout.layout_pickup_point_adapter, root, attachToRoot, component);
  }

  @NonNull
  public static LayoutPickupPointAdapterBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static LayoutPickupPointAdapterBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<LayoutPickupPointAdapterBinding>inflate(inflater, com.mart.onestopkitchen.R.layout.layout_pickup_point_adapter, null, false, component);
  }

  public static LayoutPickupPointAdapterBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static LayoutPickupPointAdapterBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (LayoutPickupPointAdapterBinding)bind(component, view, com.mart.onestopkitchen.R.layout.layout_pickup_point_adapter);
  }
}
