/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mart.onestopkitchen;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.mart.onestopkitchen";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "staging";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from product flavor: staging
  public static final Boolean isProduction = false;
}
