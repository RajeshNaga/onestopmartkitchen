package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int pickupPointItem = 1;

  public static final int totalAmount = 2;

  public static final int itemListener = 3;

  public static final int model = 4;

  public static final int ItemListener = 5;

  public static final int position = 6;

  public static final int orderModel = 7;
}
