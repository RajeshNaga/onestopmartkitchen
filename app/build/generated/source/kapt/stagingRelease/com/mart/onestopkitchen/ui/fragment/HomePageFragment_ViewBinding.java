// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.daimajia.slider.library.SliderLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomePageFragment_ViewBinding implements Unbinder {
  private HomePageFragment target;

  @UiThread
  public HomePageFragment_ViewBinding(HomePageFragment target, View source) {
    this.target = target;

    target.bannerView = Utils.findRequiredView(source, R.id.banner, "field 'bannerView'");
    target.sliderLayout = Utils.findRequiredViewAsType(source, R.id.slider, "field 'sliderLayout'", SliderLayout.class);
    target.rlRootView = Utils.findRequiredViewAsType(source, R.id.rootView, "field 'rlRootView'", RelativeLayout.class);
    target.featureCategoriesLinearLayout = Utils.findRequiredViewAsType(source, R.id.vg_featureCategories, "field 'featureCategoriesLinearLayout'", LinearLayout.class);
    target.featureProductLinearLayout = Utils.findRequiredViewAsType(source, R.id.vg_featureProduct, "field 'featureProductLinearLayout'", LinearLayout.class);
    target.featuredCategoryContainerLayout = Utils.findRequiredViewAsType(source, R.id.featuredCategoryContainerLayout, "field 'featuredCategoryContainerLayout'", LinearLayout.class);
    target.refreshLayout = Utils.findRequiredViewAsType(source, R.id.swipe_container, "field 'refreshLayout'", SwipeRefreshLayout.class);
    target.btn_view_all_top_recent = Utils.findRequiredViewAsType(source, R.id.btn_view_all_top_recent, "field 'btn_view_all_top_recent'", Button.class);
    target.rel_recent = Utils.findRequiredViewAsType(source, R.id.rel_recent, "field 'rel_recent'", RelativeLayout.class);
    target.recent_Rv = Utils.findRequiredViewAsType(source, R.id.recent_Rv, "field 'recent_Rv'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HomePageFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.bannerView = null;
    target.sliderLayout = null;
    target.rlRootView = null;
    target.featureCategoriesLinearLayout = null;
    target.featureProductLinearLayout = null;
    target.featuredCategoryContainerLayout = null;
    target.refreshLayout = null;
    target.btn_view_all_top_recent = null;
    target.rel_recent = null;
    target.recent_Rv = null;
  }
}
