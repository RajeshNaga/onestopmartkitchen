// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ContactUsFragment_ViewBinding implements Unbinder {
  private ContactUsFragment target;

  @UiThread
  public ContactUsFragment_ViewBinding(ContactUsFragment target, View source) {
    this.target = target;

    target.nameEditText = Utils.findRequiredViewAsType(source, R.id.et_name, "field 'nameEditText'", EditText.class);
    target.emailEditText = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'emailEditText'", EditText.class);
    target.enquiryEditText = Utils.findRequiredViewAsType(source, R.id.et_enquiry, "field 'enquiryEditText'", EditText.class);
    target.emailBtn = Utils.findRequiredViewAsType(source, R.id.btn_email, "field 'emailBtn'", Button.class);
    target.smsBtn = Utils.findRequiredViewAsType(source, R.id.btn_sms, "field 'smsBtn'", Button.class);
    target.callBtn = Utils.findRequiredViewAsType(source, R.id.btn_call, "field 'callBtn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ContactUsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.nameEditText = null;
    target.emailEditText = null;
    target.enquiryEditText = null;
    target.emailBtn = null;
    target.smsBtn = null;
    target.callBtn = null;
  }
}
