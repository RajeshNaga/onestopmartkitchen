// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerAddressesFragment_ViewBinding implements Unbinder {
  private CustomerAddressesFragment target;

  @UiThread
  public CustomerAddressesFragment_ViewBinding(CustomerAddressesFragment target, View source) {
    this.target = target;

    target.addAddressButton = Utils.findRequiredViewAsType(source, R.id.btn_add, "field 'addAddressButton'", Button.class);
    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view_address, "field 'mRecyclerView'", RecyclerView.class);
    target.txtNoRecord = Utils.findRequiredViewAsType(source, R.id.txt_no_record, "field 'txtNoRecord'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerAddressesFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.addAddressButton = null;
    target.mRecyclerView = null;
    target.txtNoRecord = null;
  }
}
