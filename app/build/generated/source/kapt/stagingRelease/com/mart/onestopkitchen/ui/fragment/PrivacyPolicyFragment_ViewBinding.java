// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.webkit.WebView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PrivacyPolicyFragment_ViewBinding implements Unbinder {
  private PrivacyPolicyFragment target;

  @UiThread
  public PrivacyPolicyFragment_ViewBinding(PrivacyPolicyFragment target, View source) {
    this.target = target;

    target.wv_privacy_policy = Utils.findRequiredViewAsType(source, R.id.wv_privacy_policy, "field 'wv_privacy_policy'", WebView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PrivacyPolicyFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.wv_privacy_policy = null;
  }
}
