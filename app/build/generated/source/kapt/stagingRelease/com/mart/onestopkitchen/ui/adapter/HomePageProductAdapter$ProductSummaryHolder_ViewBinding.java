// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomePageProductAdapter$ProductSummaryHolder_ViewBinding implements Unbinder {
  private HomePageProductAdapter.ProductSummaryHolder target;

  @UiThread
  public HomePageProductAdapter$ProductSummaryHolder_ViewBinding(
      HomePageProductAdapter.ProductSummaryHolder target, View source) {
    this.target = target;

    target.productImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'productImage'", ImageView.class);
    target.productPrice = Utils.findRequiredViewAsType(source, R.id.tv_productPrice, "field 'productPrice'", TextView.class);
    target.productName = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'productName'", TextView.class);
    target.productOldPrice = Utils.findRequiredViewAsType(source, R.id.tv_productOldPrice, "field 'productOldPrice'", TextView.class);
    target.productOfferPercentage = Utils.findRequiredViewAsType(source, R.id.tv_offerPercentage, "field 'productOfferPercentage'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HomePageProductAdapter.ProductSummaryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.productImage = null;
    target.productPrice = null;
    target.productName = null;
    target.productOldPrice = null;
    target.productOfferPercentage = null;
  }
}
