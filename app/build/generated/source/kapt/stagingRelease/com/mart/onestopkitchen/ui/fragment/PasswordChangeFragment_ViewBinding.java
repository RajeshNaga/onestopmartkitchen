// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PasswordChangeFragment_ViewBinding implements Unbinder {
  private PasswordChangeFragment target;

  @UiThread
  public PasswordChangeFragment_ViewBinding(PasswordChangeFragment target, View source) {
    this.target = target;

    target.currentPasswordEditText = Utils.findRequiredViewAsType(source, R.id.et_current_password, "field 'currentPasswordEditText'", EditText.class);
    target.passwordEditText = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'passwordEditText'", EditText.class);
    target.confirmPasswordEditText = Utils.findRequiredViewAsType(source, R.id.et_confirm_password, "field 'confirmPasswordEditText'", EditText.class);
    target.saveButton = Utils.findRequiredViewAsType(source, R.id.btn_save, "field 'saveButton'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PasswordChangeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.currentPasswordEditText = null;
    target.passwordEditText = null;
    target.confirmPasswordEditText = null;
    target.saveButton = null;
  }
}
