// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SliderMenuAdapter$ViewHolder_ViewBinding implements Unbinder {
  private SliderMenuAdapter.ViewHolder target;

  @UiThread
  public SliderMenuAdapter$ViewHolder_ViewBinding(SliderMenuAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.subName = Utils.findRequiredViewAsType(source, R.id.subName, "field 'subName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SliderMenuAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.subName = null;
  }
}
