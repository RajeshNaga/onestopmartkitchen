package com.mart.onestopkitchen;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.mart.onestopkitchen.databinding.FragmentPaymentDoneSuccessfullyBindingImpl;
import com.mart.onestopkitchen.databinding.LayoutOrderGroupReceiptPageBindingImpl;
import com.mart.onestopkitchen.databinding.LayoutPickupPointAdapterBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_FRAGMENTPAYMENTDONESUCCESSFULLY = 1;

  private static final int LAYOUT_LAYOUTORDERGROUPRECEIPTPAGE = 2;

  private static final int LAYOUT_LAYOUTPICKUPPOINTADAPTER = 3;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(3);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.mart.onestopkitchen.R.layout.fragment_payment_done_successfully, LAYOUT_FRAGMENTPAYMENTDONESUCCESSFULLY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.mart.onestopkitchen.R.layout.layout_order_group_receipt_page, LAYOUT_LAYOUTORDERGROUPRECEIPTPAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.mart.onestopkitchen.R.layout.layout_pickup_point_adapter, LAYOUT_LAYOUTPICKUPPOINTADAPTER);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_FRAGMENTPAYMENTDONESUCCESSFULLY: {
          if ("layout/fragment_payment_done_successfully_0".equals(tag)) {
            return new FragmentPaymentDoneSuccessfullyBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_payment_done_successfully is invalid. Received: " + tag);
        }
        case  LAYOUT_LAYOUTORDERGROUPRECEIPTPAGE: {
          if ("layout/layout_order_group_receipt_page_0".equals(tag)) {
            return new LayoutOrderGroupReceiptPageBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for layout_order_group_receipt_page is invalid. Received: " + tag);
        }
        case  LAYOUT_LAYOUTPICKUPPOINTADAPTER: {
          if ("layout/layout_pickup_point_adapter_0".equals(tag)) {
            return new LayoutPickupPointAdapterBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for layout_pickup_point_adapter is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(9);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "pickupPointItem");
      sKeys.put(2, "totalAmount");
      sKeys.put(3, "itemListener");
      sKeys.put(4, "model");
      sKeys.put(5, "ItemListener");
      sKeys.put(6, "position");
      sKeys.put(7, "orderModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(3);

    static {
      sKeys.put("layout/fragment_payment_done_successfully_0", com.mart.onestopkitchen.R.layout.fragment_payment_done_successfully);
      sKeys.put("layout/layout_order_group_receipt_page_0", com.mart.onestopkitchen.R.layout.layout_order_group_receipt_page);
      sKeys.put("layout/layout_pickup_point_adapter_0", com.mart.onestopkitchen.R.layout.layout_pickup_point_adapter);
    }
  }
}
