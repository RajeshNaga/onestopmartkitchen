package com.mart.onestopkitchen.table;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.mart.onestopkitchen.model.creditcarddao.CreditCardDao;
import com.mart.onestopkitchen.model.creditcarddao.CreditCardDao_Impl;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDatabase_Impl extends AppDatabase {
  private volatile CreditCardDao _creditCardDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `cardDetailTable` (`userId` INTEGER PRIMARY KEY AUTOINCREMENT, `cardHolderName` TEXT NOT NULL, `cardNumber` TEXT NOT NULL, `cardCvv` TEXT NOT NULL, `cardExpMonth` TEXT NOT NULL, `cardExpYear` TEXT NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1cda68f7f78c33eecff2d506fb496500')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `cardDetailTable`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsCardDetailTable = new HashMap<String, TableInfo.Column>(6);
        _columnsCardDetailTable.put("userId", new TableInfo.Column("userId", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCardDetailTable.put("cardHolderName", new TableInfo.Column("cardHolderName", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCardDetailTable.put("cardNumber", new TableInfo.Column("cardNumber", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCardDetailTable.put("cardCvv", new TableInfo.Column("cardCvv", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCardDetailTable.put("cardExpMonth", new TableInfo.Column("cardExpMonth", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCardDetailTable.put("cardExpYear", new TableInfo.Column("cardExpYear", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCardDetailTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCardDetailTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoCardDetailTable = new TableInfo("cardDetailTable", _columnsCardDetailTable, _foreignKeysCardDetailTable, _indicesCardDetailTable);
        final TableInfo _existingCardDetailTable = TableInfo.read(_db, "cardDetailTable");
        if (! _infoCardDetailTable.equals(_existingCardDetailTable)) {
          return new RoomOpenHelper.ValidationResult(false, "cardDetailTable(com.mart.onestopkitchen.model.creditcarddao.CardDetailModel).\n"
                  + " Expected:\n" + _infoCardDetailTable + "\n"
                  + " Found:\n" + _existingCardDetailTable);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "1cda68f7f78c33eecff2d506fb496500", "3734173557b1836370b4cb460adb4d20");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "cardDetailTable");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `cardDetailTable`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public CreditCardDao cardDetails() {
    if (_creditCardDao != null) {
      return _creditCardDao;
    } else {
      synchronized(this) {
        if(_creditCardDao == null) {
          _creditCardDao = new CreditCardDao_Impl(this);
        }
        return _creditCardDao;
      }
    }
  }
}
