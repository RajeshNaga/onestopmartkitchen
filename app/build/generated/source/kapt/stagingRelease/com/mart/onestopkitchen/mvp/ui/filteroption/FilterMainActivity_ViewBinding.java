// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filteroption;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterMainActivity_ViewBinding implements Unbinder {
  private FilterMainActivity target;

  @UiThread
  public FilterMainActivity_ViewBinding(FilterMainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FilterMainActivity_ViewBinding(FilterMainActivity target, View source) {
    this.target = target;

    target.toolFilter = Utils.findRequiredViewAsType(source, R.id.tool_filter, "field 'toolFilter'", Toolbar.class);
    target.txtToolbar = Utils.findRequiredViewAsType(source, R.id.txt_toolbar, "field 'txtToolbar'", TextView.class);
    target.frameFilter = Utils.findRequiredViewAsType(source, R.id.frame_filter, "field 'frameFilter'", FrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterMainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolFilter = null;
    target.txtToolbar = null;
    target.frameFilter = null;
  }
}
