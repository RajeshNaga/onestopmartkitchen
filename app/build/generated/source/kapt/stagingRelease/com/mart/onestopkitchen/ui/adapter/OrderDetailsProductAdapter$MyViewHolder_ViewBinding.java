// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatRatingBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderDetailsProductAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private OrderDetailsProductAdapter.MyViewHolder target;

  @UiThread
  public OrderDetailsProductAdapter$MyViewHolder_ViewBinding(
      OrderDetailsProductAdapter.MyViewHolder target, View source) {
    this.target = target;

    target.productImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'productImage'", ImageView.class);
    target.offer = Utils.findRequiredViewAsType(source, R.id.offer, "field 'offer'", TextView.class);
    target.Tv_attributeInfo = Utils.findRequiredViewAsType(source, R.id.Tv_attributeInfo, "field 'Tv_attributeInfo'", TextView.class);
    target.productName = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'productName'", TextView.class);
    target.tv_product_acturl_price = Utils.findRequiredViewAsType(source, R.id.tv_product_acturl_price, "field 'tv_product_acturl_price'", TextView.class);
    target.tv_actual_price = Utils.findRequiredViewAsType(source, R.id.tv_actual_price, "field 'tv_actual_price'", TextView.class);
    target.tv_rating_number = Utils.findRequiredViewAsType(source, R.id.tv_rating_number, "field 'tv_rating_number'", TextView.class);
    target.tv_rating_reviews = Utils.findRequiredViewAsType(source, R.id.tv_rating_reviews, "field 'tv_rating_reviews'", TextView.class);
    target.row_quantity = Utils.findRequiredViewAsType(source, R.id.lin_qu, "field 'row_quantity'", LinearLayout.class);
    target.lin_write_review = Utils.findRequiredViewAsType(source, R.id.lin_write_review, "field 'lin_write_review'", LinearLayout.class);
    target.rating = Utils.findRequiredViewAsType(source, R.id.rating, "field 'rating'", AppCompatRatingBar.class);
    target.view_hide = Utils.findRequiredView(source, R.id.view_hide, "field 'view_hide'");
    target.tv_quantity = Utils.findRequiredViewAsType(source, R.id.tv_quantity, "field 'tv_quantity'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderDetailsProductAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.productImage = null;
    target.offer = null;
    target.Tv_attributeInfo = null;
    target.productName = null;
    target.tv_product_acturl_price = null;
    target.tv_actual_price = null;
    target.tv_rating_number = null;
    target.tv_rating_reviews = null;
    target.row_quantity = null;
    target.lin_write_review = null;
    target.rating = null;
    target.view_hide = null;
    target.tv_quantity = null;
  }
}
