// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BaseCheckoutStepFragment_ViewBinding implements Unbinder {
  private BaseCheckoutStepFragment target;

  @UiThread
  public BaseCheckoutStepFragment_ViewBinding(BaseCheckoutStepFragment target, View source) {
    this.target = target;

    target.addressTab = Utils.findRequiredView(source, R.id.tab_address, "field 'addressTab'");
    target.shippingTab = Utils.findRequiredView(source, R.id.tab_shipping, "field 'shippingTab'");
    target.paymentTab = Utils.findRequiredView(source, R.id.tab_payment, "field 'paymentTab'");
    target.confirmTab = Utils.findRequiredView(source, R.id.tab_confirm, "field 'confirmTab'");
  }

  @Override
  @CallSuper
  public void unbind() {
    BaseCheckoutStepFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.addressTab = null;
    target.shippingTab = null;
    target.paymentTab = null;
    target.confirmTab = null;
  }
}
