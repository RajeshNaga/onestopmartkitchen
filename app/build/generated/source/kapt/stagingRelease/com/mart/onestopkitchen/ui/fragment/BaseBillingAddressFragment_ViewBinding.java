// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.utils.CustomEdittext;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BaseBillingAddressFragment_ViewBinding implements Unbinder {
  private BaseBillingAddressFragment target;

  private View view7f0a00aa;

  @UiThread
  public BaseBillingAddressFragment_ViewBinding(final BaseBillingAddressFragment target,
      View source) {
    this.target = target;

    View view;
    target.fullName = Utils.findRequiredViewAsType(source, R.id.et_firstName, "field 'fullName'", EditText.class);
    target.addressOne = Utils.findRequiredViewAsType(source, R.id.et_address1, "field 'addressOne'", EditText.class);
    target.tvCountryCode = Utils.findRequiredViewAsType(source, R.id.txt_country_code, "field 'tvCountryCode'", TextView.class);
    target.addressTwo = Utils.findRequiredViewAsType(source, R.id.et_address2, "field 'addressTwo'", EditText.class);
    target.stateSpinner = Utils.findRequiredViewAsType(source, R.id.spinner_state, "field 'stateSpinner'", AppCompatSpinner.class);
    target.phoneNumber = Utils.findRequiredViewAsType(source, R.id.et_phone_number, "field 'phoneNumber'", CustomEdittext.class);
    target.adddressParentLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_adddressParentLayout, "field 'adddressParentLinearLayout'", LinearLayout.class);
    target.linRopes = Utils.findRequiredViewAsType(source, R.id.lin_ropes, "field 'linRopes'", LinearLayout.class);
    target.citySpinner = Utils.findRequiredViewAsType(source, R.id.spinner_city, "field 'citySpinner'", AppCompatSpinner.class);
    target.spinnerRope = Utils.findRequiredViewAsType(source, R.id.spinner_rope, "field 'spinnerRope'", AppCompatSpinner.class);
    target.etHouseNumber = Utils.findRequiredViewAsType(source, R.id.et_house_number, "field 'etHouseNumber'", TextInputEditText.class);
    target.etFloorNumber = Utils.findRequiredViewAsType(source, R.id.et_floor_number, "field 'etFloorNumber'", TextInputEditText.class);
    target.etRoomNumber = Utils.findRequiredViewAsType(source, R.id.et_room_number, "field 'etRoomNumber'", TextInputEditText.class);
    target.rbYes = Utils.findRequiredViewAsType(source, R.id.radio_yes, "field 'rbYes'", RadioButton.class);
    target.rbNo = Utils.findRequiredViewAsType(source, R.id.radio_no, "field 'rbNo'", RadioButton.class);
    target.rg_lift_option = Utils.findRequiredViewAsType(source, R.id.rg_lift_option, "field 'rg_lift_option'", RadioGroup.class);
    target.tvFullName = Utils.findRequiredViewAsType(source, R.id.tvFullName, "field 'tvFullName'", TextView.class);
    target.tvAddressOne = Utils.findRequiredViewAsType(source, R.id.tvAddressOne, "field 'tvAddressOne'", TextView.class);
    target.tvAddressTwo = Utils.findRequiredViewAsType(source, R.id.tvAddressTwo, "field 'tvAddressTwo'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_continue, "field 'btnContinue' and method 'onBtnContinue'");
    target.btnContinue = Utils.castView(view, R.id.btn_continue, "field 'btnContinue'", Button.class);
    view7f0a00aa = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnContinue();
      }
    });
    target.tv_PhoneNumber = Utils.findRequiredViewAsType(source, R.id.tv_PhoneNumber, "field 'tv_PhoneNumber'", TextView.class);
    target.tv_stateDivision = Utils.findRequiredViewAsType(source, R.id.tv_stateDivision, "field 'tv_stateDivision'", TextView.class);
    target.tv_Township = Utils.findRequiredViewAsType(source, R.id.tv_Township, "field 'tv_Township'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BaseBillingAddressFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.fullName = null;
    target.addressOne = null;
    target.tvCountryCode = null;
    target.addressTwo = null;
    target.stateSpinner = null;
    target.phoneNumber = null;
    target.adddressParentLinearLayout = null;
    target.linRopes = null;
    target.citySpinner = null;
    target.spinnerRope = null;
    target.etHouseNumber = null;
    target.etFloorNumber = null;
    target.etRoomNumber = null;
    target.rbYes = null;
    target.rbNo = null;
    target.rg_lift_option = null;
    target.tvFullName = null;
    target.tvAddressOne = null;
    target.tvAddressTwo = null;
    target.btnContinue = null;
    target.tv_PhoneNumber = null;
    target.tv_stateDivision = null;
    target.tv_Township = null;

    view7f0a00aa.setOnClickListener(null);
    view7f0a00aa = null;
  }
}
