// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerOrdersFragment_ViewBinding implements Unbinder {
  private CustomerOrdersFragment target;

  @UiThread
  public CustomerOrdersFragment_ViewBinding(CustomerOrdersFragment target, View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.recycler_view_orders, "field 'mRecyclerView'", RecyclerView.class);
    target.order_not_fount = Utils.findRequiredViewAsType(source, R.id.order_not_fount, "field 'order_not_fount'", LinearLayout.class);
    target.continueShopping = Utils.findRequiredViewAsType(source, R.id.btn_continue_shopping, "field 'continueShopping'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerOrdersFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
    target.order_not_fount = null;
    target.continueShopping = null;
  }
}
