// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.cashin;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AgentListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private AgentListAdapter.ViewHolder target;

  @UiThread
  public AgentListAdapter$ViewHolder_ViewBinding(AgentListAdapter.ViewHolder target, View source) {
    this.target = target;

    target.textName = Utils.findRequiredViewAsType(source, R.id.text_name, "field 'textName'", TextView.class);
    target.textAddress = Utils.findRequiredViewAsType(source, R.id.text_address, "field 'textAddress'", TextView.class);
    target.textOpenClose = Utils.findRequiredViewAsType(source, R.id.text_status, "field 'textOpenClose'", TextView.class);
    target.layoutInfo = Utils.findRequiredViewAsType(source, R.id.layout_info, "field 'layoutInfo'", LinearLayout.class);
    target.imageMobileAgent = Utils.findRequiredViewAsType(source, R.id.image_mobile_agent, "field 'imageMobileAgent'", ImageView.class);
    target.imageCall = Utils.findRequiredViewAsType(source, R.id.image_call, "field 'imageCall'", ImageView.class);
    target.layoutActions = Utils.findRequiredViewAsType(source, R.id.layout_actions, "field 'layoutActions'", LinearLayout.class);
    target.layoutTop = Utils.findRequiredViewAsType(source, R.id.layout_top, "field 'layoutTop'", LinearLayout.class);
    target.view = Utils.findRequiredView(source, R.id.view, "field 'view'");
    target.textRating = Utils.findRequiredViewAsType(source, R.id.text_rating, "field 'textRating'", TextView.class);
    target.textTime = Utils.findRequiredViewAsType(source, R.id.text_time, "field 'textTime'", TextView.class);
    target.textMinFee = Utils.findRequiredViewAsType(source, R.id.text_min_fee, "field 'textMinFee'", TextView.class);
    target.layoutFooter = Utils.findRequiredViewAsType(source, R.id.layout_footer, "field 'layoutFooter'", LinearLayout.class);
    target.textPhone = Utils.findRequiredViewAsType(source, R.id.text_phone, "field 'textPhone'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AgentListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.textName = null;
    target.textAddress = null;
    target.textOpenClose = null;
    target.layoutInfo = null;
    target.imageMobileAgent = null;
    target.imageCall = null;
    target.layoutActions = null;
    target.layoutTop = null;
    target.view = null;
    target.textRating = null;
    target.textTime = null;
    target.textMinFee = null;
    target.layoutFooter = null;
    target.textPhone = null;
  }
}
