// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.customview.RadioGridGroup;
import com.mart.onestopkitchen.utils.CustomTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductDetailFragment_ViewBinding implements Unbinder {
  private ProductDetailFragment target;

  @UiThread
  public ProductDetailFragment_ViewBinding(ProductDetailFragment target, View source) {
    this.target = target;

    target.productOldPriceTextView = Utils.findRequiredViewAsType(source, R.id.tv_product_old_price, "field 'productOldPriceTextView'", TextView.class);
    target.productNameTextview = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'productNameTextview'", TextView.class);
    target.productPriceTextview = Utils.findRequiredViewAsType(source, R.id.tv_productPrice, "field 'productPriceTextview'", TextView.class);
    target.tv_description = Utils.findRequiredViewAsType(source, R.id.tv_description, "field 'tv_description'", TextView.class);
    target.expandCollapseTextView = Utils.findRequiredViewAsType(source, R.id.tv_expand_collaps, "field 'expandCollapseTextView'", CustomTextView.class);
    target.addtoCartBtn = Utils.findRequiredViewAsType(source, R.id.btn_addtoCart, "field 'addtoCartBtn'", Button.class);
    target.addToBuyBtn = Utils.findRequiredViewAsType(source, R.id.btn_buy_now, "field 'addToBuyBtn'", Button.class);
    target.groupProducrgridLayout = Utils.findRequiredViewAsType(source, R.id.gridLayout_group_product, "field 'groupProducrgridLayout'", RadioGridGroup.class);
    target.dynamicAttributeLayout = Utils.findRequiredViewAsType(source, R.id.dynamicAttributeLayout, "field 'dynamicAttributeLayout'", LinearLayout.class);
    target.RelatedProductList = Utils.findRequiredViewAsType(source, R.id.rclv_product_list, "field 'RelatedProductList'", RecyclerView.class);
    target.relatedProductsCardView = Utils.findRequiredViewAsType(source, R.id.cv_category_product_container, "field 'relatedProductsCardView'", LinearLayout.class);
    target.expandDsc = Utils.findRequiredViewAsType(source, R.id.expand_dsc, "field 'expandDsc'", ImageView.class);
    target.expandView = Utils.findRequiredView(source, R.id.expand_view, "field 'expandView'");
    target.specificationTextView = Utils.findRequiredViewAsType(source, R.id.tv_specifications, "field 'specificationTextView'", TextView.class);
    target.quantitiyUp = Utils.findRequiredViewAsType(source, R.id.quantitiyUp, "field 'quantitiyUp'", ImageView.class);
    target.quantityDown = Utils.findRequiredViewAsType(source, R.id.quantityDown, "field 'quantityDown'", ImageView.class);
    target.textviewQuantity = Utils.findRequiredViewAsType(source, R.id.textviewQuantity, "field 'textviewQuantity'", TextView.class);
    target.quantityLayout = Utils.findRequiredViewAsType(source, R.id.quantityLayout, "field 'quantityLayout'", LinearLayout.class);
    target.tvOfferAmount = Utils.findRequiredViewAsType(source, R.id.tv_price_off, "field 'tvOfferAmount'", TextView.class);
    target.relTotal = Utils.findRequiredViewAsType(source, R.id.rel_total, "field 'relTotal'", RelativeLayout.class);
    target.specificelayout = Utils.findRequiredViewAsType(source, R.id.specificelayout, "field 'specificelayout'", LinearLayout.class);
    target.detailsScrollview = Utils.findRequiredViewAsType(source, R.id.detailsScrollview, "field 'detailsScrollview'", NestedScrollView.class);
    target.imageShowRecyclerview = Utils.findRequiredViewAsType(source, R.id.recyclerView_image, "field 'imageShowRecyclerview'", RecyclerView.class);
    target.tv_delivery_date = Utils.findRequiredViewAsType(source, R.id.tv_delivery_date, "field 'tv_delivery_date'", TextView.class);
    target.stockCount = Utils.findRequiredViewAsType(source, R.id.stock_count, "field 'stockCount'", TextView.class);
    target.tv_rating_number = Utils.findRequiredViewAsType(source, R.id.tv_rating_number, "field 'tv_rating_number'", TextView.class);
    target.tv_rating_reviews = Utils.findRequiredViewAsType(source, R.id.tv_rating_reviews, "field 'tv_rating_reviews'", TextView.class);
    target.measurementView = Utils.findRequiredViewAsType(source, R.id.measurement_view, "field 'measurementView'", LinearLayout.class);
    target.sizeMainLayout = Utils.findRequiredViewAsType(source, R.id.size_main_layout, "field 'sizeMainLayout'", RelativeLayout.class);
    target.colorMainLayout = Utils.findRequiredViewAsType(source, R.id.color_main_layout, "field 'colorMainLayout'", RelativeLayout.class);
    target.sizeRecyclerView = Utils.findRequiredViewAsType(source, R.id.size_recyclerview, "field 'sizeRecyclerView'", RecyclerView.class);
    target.colorRecyclerView = Utils.findRequiredViewAsType(source, R.id.color_recyclerview, "field 'colorRecyclerView'", RecyclerView.class);
    target.measureView = Utils.findRequiredViewAsType(source, R.id.rv_measure_view, "field 'measureView'", RecyclerView.class);
    target.viewLayout = Utils.findRequiredViewAsType(source, R.id.merge_recyclerview, "field 'viewLayout'", LinearLayout.class);
    target.containerLayout = Utils.findRequiredViewAsType(source, R.id.include_Container_Layout, "field 'containerLayout'", LinearLayout.class);
    target.textSizeHeader = Utils.findRequiredViewAsType(source, R.id.tv_size, "field 'textSizeHeader'", TextView.class);
    target.textColorHeader = Utils.findRequiredViewAsType(source, R.id.tv_color, "field 'textColorHeader'", TextView.class);
    target.toggleButton = Utils.findRequiredViewAsType(source, R.id.button_favorite, "field 'toggleButton'", ToggleButton.class);
    target.specificationView = Utils.findRequiredViewAsType(source, R.id.rv_spec_view, "field 'specificationView'", RecyclerView.class);
    target.tvShortDescription = Utils.findRequiredViewAsType(source, R.id.tv_short_description, "field 'tvShortDescription'", TextView.class);
    target.linShortDescription = Utils.findRequiredViewAsType(source, R.id.lin_short_description, "field 'linShortDescription'", LinearLayout.class);
    target.descLayout = Utils.findRequiredViewAsType(source, R.id.lin_exp, "field 'descLayout'", LinearLayout.class);
    target.relatedLayout = Utils.findRequiredViewAsType(source, R.id.ll_related_products, "field 'relatedLayout'", LinearLayout.class);
    target.quant = Utils.findRequiredViewAsType(source, R.id.spinner_quantities, "field 'quant'", Spinner.class);
    target.qtyLayout = Utils.findRequiredViewAsType(source, R.id.layout_allowed_quantities, "field 'qtyLayout'", RelativeLayout.class);
    target.gsmReview = Utils.findRequiredViewAsType(source, R.id.tv_gsmreview, "field 'gsmReview'", TextView.class);
    target.img_sharing = Utils.findRequiredViewAsType(source, R.id.img_sharing, "field 'img_sharing'", ImageView.class);
    target.notifyLayout = Utils.findRequiredViewAsType(source, R.id.notifyLayout, "field 'notifyLayout'", CardView.class);
    target.notify = Utils.findRequiredViewAsType(source, R.id.btn_nofify, "field 'notify'", Button.class);
    target.fixedLayoput = Utils.findRequiredViewAsType(source, R.id.fixedLayout, "field 'fixedLayoput'", CardView.class);
    target.endDate = Utils.findRequiredViewAsType(source, R.id.tv_endDate, "field 'endDate'", TextView.class);
    target.availableLayout = Utils.findRequiredViewAsType(source, R.id.ll_available, "field 'availableLayout'", LinearLayout.class);
    target.btn_chat_msg = Utils.findRequiredViewAsType(source, R.id.btn_chat_msg, "field 'btn_chat_msg'", FloatingActionButton.class);
    target.btn_chat_audio_video = Utils.findRequiredViewAsType(source, R.id.btn_chat_audio_video, "field 'btn_chat_audio_video'", FloatingActionButton.class);
    target.ll_Msg_Call = Utils.findRequiredViewAsType(source, R.id.ll_Msg_Call, "field 'll_Msg_Call'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductDetailFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.productOldPriceTextView = null;
    target.productNameTextview = null;
    target.productPriceTextview = null;
    target.tv_description = null;
    target.expandCollapseTextView = null;
    target.addtoCartBtn = null;
    target.addToBuyBtn = null;
    target.groupProducrgridLayout = null;
    target.dynamicAttributeLayout = null;
    target.RelatedProductList = null;
    target.relatedProductsCardView = null;
    target.expandDsc = null;
    target.expandView = null;
    target.specificationTextView = null;
    target.quantitiyUp = null;
    target.quantityDown = null;
    target.textviewQuantity = null;
    target.quantityLayout = null;
    target.tvOfferAmount = null;
    target.relTotal = null;
    target.specificelayout = null;
    target.detailsScrollview = null;
    target.imageShowRecyclerview = null;
    target.tv_delivery_date = null;
    target.stockCount = null;
    target.tv_rating_number = null;
    target.tv_rating_reviews = null;
    target.measurementView = null;
    target.sizeMainLayout = null;
    target.colorMainLayout = null;
    target.sizeRecyclerView = null;
    target.colorRecyclerView = null;
    target.measureView = null;
    target.viewLayout = null;
    target.containerLayout = null;
    target.textSizeHeader = null;
    target.textColorHeader = null;
    target.toggleButton = null;
    target.specificationView = null;
    target.tvShortDescription = null;
    target.linShortDescription = null;
    target.descLayout = null;
    target.relatedLayout = null;
    target.quant = null;
    target.qtyLayout = null;
    target.gsmReview = null;
    target.img_sharing = null;
    target.notifyLayout = null;
    target.notify = null;
    target.fixedLayoput = null;
    target.endDate = null;
    target.availableLayout = null;
    target.btn_chat_msg = null;
    target.btn_chat_audio_video = null;
    target.ll_Msg_Call = null;
  }
}
