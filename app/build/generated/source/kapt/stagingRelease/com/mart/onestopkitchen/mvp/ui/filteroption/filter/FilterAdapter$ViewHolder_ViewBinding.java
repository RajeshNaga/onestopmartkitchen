// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filteroption.filter;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterAdapter$ViewHolder_ViewBinding implements Unbinder {
  private FilterAdapter.ViewHolder target;

  private View view7f0a0386;

  @UiThread
  public FilterAdapter$ViewHolder_ViewBinding(final FilterAdapter.ViewHolder target, View source) {
    this.target = target;

    View view;
    target.txtFilterTitle = Utils.findRequiredViewAsType(source, R.id.txt_filter_title, "field 'txtFilterTitle'", TextView.class);
    target.txtSubItem = Utils.findRequiredViewAsType(source, R.id.txt_sub_item, "field 'txtSubItem'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rel_root, "field 'relRoot' and method 'onViewClicked'");
    target.relRoot = Utils.castView(view, R.id.rel_root, "field 'relRoot'", RelativeLayout.class);
    view7f0a0386 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtFilterTitle = null;
    target.txtSubItem = null;
    target.relRoot = null;

    view7f0a0386.setOnClickListener(null);
    view7f0a0386 = null;
  }
}
