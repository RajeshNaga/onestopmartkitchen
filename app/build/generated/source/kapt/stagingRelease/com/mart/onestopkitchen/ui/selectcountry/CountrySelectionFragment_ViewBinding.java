// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.selectcountry;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CountrySelectionFragment_ViewBinding implements Unbinder {
  private CountrySelectionFragment target;

  @UiThread
  public CountrySelectionFragment_ViewBinding(CountrySelectionFragment target, View source) {
    this.target = target;

    target.toolCommon = Utils.findRequiredViewAsType(source, R.id.tool_common, "field 'toolCommon'", Toolbar.class);
    target.edtSearchCountry = Utils.findRequiredViewAsType(source, R.id.edt_search_country, "field 'edtSearchCountry'", EditText.class);
    target.relCountryList = Utils.findRequiredViewAsType(source, R.id.rel_country_list, "field 'relCountryList'", RecyclerView.class);
    target.txtTitle = Utils.findRequiredViewAsType(source, R.id.txt_title, "field 'txtTitle'", TextView.class);
    target.txtNoRecord = Utils.findRequiredViewAsType(source, R.id.txt_no_record, "field 'txtNoRecord'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CountrySelectionFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolCommon = null;
    target.edtSearchCountry = null;
    target.relCountryList = null;
    target.txtTitle = null;
    target.txtNoRecord = null;
  }
}
