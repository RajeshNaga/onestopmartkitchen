// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.cashin.map;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NearByAgentMapActivity_ViewBinding implements Unbinder {
  private NearByAgentMapActivity target;

  @UiThread
  public NearByAgentMapActivity_ViewBinding(NearByAgentMapActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public NearByAgentMapActivity_ViewBinding(NearByAgentMapActivity target, View source) {
    this.target = target;

    target.toolFilterMap = Utils.findRequiredViewAsType(source, R.id.tool_filter_map, "field 'toolFilterMap'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    NearByAgentMapActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolFilterMap = null;
  }
}
