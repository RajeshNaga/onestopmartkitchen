// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filteroption.filterselection;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterSelectionFragment_ViewBinding implements Unbinder {
  private FilterSelectionFragment target;

  private View view7f0a00bc;

  @UiThread
  public FilterSelectionFragment_ViewBinding(final FilterSelectionFragment target, View source) {
    this.target = target;

    View view;
    target.rvFilterSelection = Utils.findRequiredViewAsType(source, R.id.rv_filter_selection, "field 'rvFilterSelection'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_selection, "field 'btnSelection' and method 'onViewClicked'");
    target.btnSelection = Utils.castView(view, R.id.btn_selection, "field 'btnSelection'", Button.class);
    view7f0a00bc = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterSelectionFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvFilterSelection = null;
    target.btnSelection = null;

    view7f0a00bc.setOnClickListener(null);
    view7f0a00bc = null;
  }
}
