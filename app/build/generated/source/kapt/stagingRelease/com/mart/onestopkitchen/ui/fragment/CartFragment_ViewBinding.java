// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CartFragment_ViewBinding implements Unbinder {
  private CartFragment target;

  private View view7f0a009e;

  @UiThread
  public CartFragment_ViewBinding(final CartFragment target, View source) {
    this.target = target;

    View view;
    target.nestedScrollView = Utils.findRequiredViewAsType(source, R.id.nestedScrollView, "field 'nestedScrollView'", NestedScrollView.class);
    target.cartProductRecyclerList = Utils.findRequiredViewAsType(source, R.id.rclv_cartList, "field 'cartProductRecyclerList'", RecyclerView.class);
    target.checkoutBtn = Utils.findRequiredViewAsType(source, R.id.btn_proceed_to_Checkout, "field 'checkoutBtn'", Button.class);
    target.applyCouponBtn = Utils.findRequiredViewAsType(source, R.id.btn_apply_coupon, "field 'applyCouponBtn'", Button.class);
    target.ll_existing_address = Utils.findRequiredViewAsType(source, R.id.ll_existing_address, "field 'll_existing_address'", LinearLayout.class);
    target.subTotalTextView = Utils.findRequiredViewAsType(source, R.id.tv_subtotal, "field 'subTotalTextView'", TextView.class);
    target.shippingTextView = Utils.findRequiredViewAsType(source, R.id.tv_shipping, "field 'shippingTextView'", TextView.class);
    target.taxTextView = Utils.findRequiredViewAsType(source, R.id.tv_tax, "field 'taxTextView'", TextView.class);
    target.totalAmountTextView = Utils.findRequiredViewAsType(source, R.id.tv_Total, "field 'totalAmountTextView'", TextView.class);
    target.discountTextView = Utils.findRequiredViewAsType(source, R.id.tv_discount, "field 'discountTextView'", TextView.class);
    target.discountTableRow = Utils.findRequiredViewAsType(source, R.id.tr_discount, "field 'discountTableRow'", TableRow.class);
    target.taxRow = Utils.findRequiredViewAsType(source, R.id.taxRow, "field 'taxRow'", TableRow.class);
    target.dynamicAttributeLayout = Utils.findRequiredViewAsType(source, R.id.dynamicAttributeLayout, "field 'dynamicAttributeLayout'", LinearLayout.class);
    target.couponCodeEditText = Utils.findRequiredViewAsType(source, R.id.et_coupon_code, "field 'couponCodeEditText'", EditText.class);
    target.productAttributeCardView = Utils.findRequiredViewAsType(source, R.id.cv_product_attribute, "field 'productAttributeCardView'", CardView.class);
    target.CartInfoLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_cartInfoLayout, "field 'CartInfoLinearLayout'", LinearLayout.class);
    target.NoItemCartLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_noCartItemLayout, "field 'NoItemCartLinearLayout'", LinearLayout.class);
    target.btn_continue_shopping = Utils.findRequiredViewAsType(source, R.id.btn_continue_shopping, "field 'btn_continue_shopping'", Button.class);
    target.discountLayout = Utils.findRequiredViewAsType(source, R.id.discountlayout, "field 'discountLayout'", CardView.class);
    target.rb_pickup_point = Utils.findRequiredViewAsType(source, R.id.rb_pickup_point, "field 'rb_pickup_point'", RadioButton.class);
    target.rb_door_address = Utils.findRequiredViewAsType(source, R.id.rb_door_address, "field 'rb_door_address'", RadioButton.class);
    target.table_orderTotal = Utils.findRequiredViewAsType(source, R.id.table_orderTotal, "field 'table_orderTotal'", TableLayout.class);
    target.taxKey = Utils.findRequiredViewAsType(source, R.id.taxKey, "field 'taxKey'", TextView.class);
    target.shimmer_view_container = Utils.findRequiredViewAsType(source, R.id.shimmer_view_container, "field 'shimmer_view_container'", ShimmerFrameLayout.class);
    target.tvUsername = Utils.findRequiredViewAsType(source, R.id.tv_username, "field 'tvUsername'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tv_address, "field 'tvAddress'", TextView.class);
    target.tvPhoneNumber = Utils.findRequiredViewAsType(source, R.id.tv_phone_number, "field 'tvPhoneNumber'", TextView.class);
    target.btnPickupPointLocation = Utils.findRequiredViewAsType(source, R.id.btnPickupPointLocation, "field 'btnPickupPointLocation'", TextView.class);
    target.rg_delivery_option = Utils.findRequiredViewAsType(source, R.id.rg_delivery_option, "field 'rg_delivery_option'", RadioGroup.class);
    target.tvPickupLocation = Utils.findRequiredViewAsType(source, R.id.tv_pickup_location, "field 'tvPickupLocation'", TextView.class);
    target.tvPickupName = Utils.findRequiredViewAsType(source, R.id.tv_pickup_name, "field 'tvPickupName'", TextView.class);
    target.btn_edit = Utils.findRequiredViewAsType(source, R.id.btn_edit, "field 'btn_edit'", ImageView.class);
    target.ll_pickup_address = Utils.findRequiredViewAsType(source, R.id.ll_pickup_address, "field 'll_pickup_address'", LinearLayout.class);
    target.img_edit_address = Utils.findRequiredViewAsType(source, R.id.img_edit_address, "field 'img_edit_address'", ImageView.class);
    target.pickupAddressLayout = Utils.findRequiredViewAsType(source, R.id.ll_pickupaddress_layout, "field 'pickupAddressLayout'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_add_address, "method 'selectOrAddAddress'");
    view7f0a009e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.selectOrAddAddress();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CartFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.nestedScrollView = null;
    target.cartProductRecyclerList = null;
    target.checkoutBtn = null;
    target.applyCouponBtn = null;
    target.ll_existing_address = null;
    target.subTotalTextView = null;
    target.shippingTextView = null;
    target.taxTextView = null;
    target.totalAmountTextView = null;
    target.discountTextView = null;
    target.discountTableRow = null;
    target.taxRow = null;
    target.dynamicAttributeLayout = null;
    target.couponCodeEditText = null;
    target.productAttributeCardView = null;
    target.CartInfoLinearLayout = null;
    target.NoItemCartLinearLayout = null;
    target.btn_continue_shopping = null;
    target.discountLayout = null;
    target.rb_pickup_point = null;
    target.rb_door_address = null;
    target.table_orderTotal = null;
    target.taxKey = null;
    target.shimmer_view_container = null;
    target.tvUsername = null;
    target.tvAddress = null;
    target.tvPhoneNumber = null;
    target.btnPickupPointLocation = null;
    target.rg_delivery_option = null;
    target.tvPickupLocation = null;
    target.tvPickupName = null;
    target.btn_edit = null;
    target.ll_pickup_address = null;
    target.img_edit_address = null;
    target.pickupAddressLayout = null;

    view7f0a009e.setOnClickListener(null);
    view7f0a009e = null;
  }
}
