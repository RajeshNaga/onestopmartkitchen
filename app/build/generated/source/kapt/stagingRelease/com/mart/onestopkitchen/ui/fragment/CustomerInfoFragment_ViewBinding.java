// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.utils.CircleImageView;
import com.mart.onestopkitchen.utils.CustomEdittext;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerInfoFragment_ViewBinding implements Unbinder {
  private CustomerInfoFragment target;

  private View view7f0a00ba;

  private View view7f0a0209;

  private View view7f0a040b;

  private View view7f0a00d1;

  private View view7f0a0137;

  private View view7f0a0138;

  @UiThread
  public CustomerInfoFragment_ViewBinding(final CustomerInfoFragment target, View source) {
    this.target = target;

    View view;
    target.customerNameTextView = Utils.findRequiredViewAsType(source, R.id.customer_name, "field 'customerNameTextView'", TextView.class);
    target.dateOfBirthTextView = Utils.findRequiredViewAsType(source, R.id.dateOfBirth, "field 'dateOfBirthTextView'", TextView.class);
    target.emailTextView = Utils.findRequiredViewAsType(source, R.id.customer_email, "field 'emailTextView'", TextView.class);
    target.row_customerEmail = Utils.findRequiredViewAsType(source, R.id.row_customerEmail, "field 'row_customerEmail'", TableRow.class);
    target.emailEditText = Utils.findRequiredViewAsType(source, R.id.et_customer_email, "field 'emailEditText'", EditText.class);
    target.genderMaleRadioButton = Utils.findRequiredViewAsType(source, R.id.rb_male, "field 'genderMaleRadioButton'", RadioButton.class);
    target.genderFemaleRadioButton = Utils.findRequiredViewAsType(source, R.id.rb_female, "field 'genderFemaleRadioButton'", RadioButton.class);
    target.genderRadioGroup = Utils.findRequiredViewAsType(source, R.id.genderRadioGroup, "field 'genderRadioGroup'", RadioGroup.class);
    view = Utils.findRequiredView(source, R.id.btn_save, "field 'saveBtn' and method 'OnClickBtn'");
    target.saveBtn = Utils.castView(view, R.id.btn_save, "field 'saveBtn'", Button.class);
    view7f0a00ba = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickBtn(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_user, "field 'img_user' and method 'OnClickBtn'");
    target.img_user = Utils.castView(view, R.id.img_user, "field 'img_user'", CircleImageView.class);
    view7f0a0209 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickBtn(p0);
      }
    });
    target.et_Customer_phoneone = Utils.findRequiredViewAsType(source, R.id.et_Customer_phoneone, "field 'et_Customer_phoneone'", CustomEdittext.class);
    target.txt_country_code = Utils.findRequiredViewAsType(source, R.id.txt_country_code, "field 'txt_country_code'", TextView.class);
    view = Utils.findRequiredView(source, R.id.showpass, "field 'show_password' and method 'OnClickBtn'");
    target.show_password = Utils.castView(view, R.id.showpass, "field 'show_password'", ImageView.class);
    view7f0a040b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickBtn(p0);
      }
    });
    target.password_show = Utils.findRequiredViewAsType(source, R.id.password_show, "field 'password_show'", TextView.class);
    target.txt_country_code_anotherNumber = Utils.findRequiredViewAsType(source, R.id.txt_country_code_anotherNumber, "field 'txt_country_code_anotherNumber'", TextView.class);
    target.additional_customer_phone = Utils.findRequiredViewAsType(source, R.id.additional_customer_phone, "field 'additional_customer_phone'", TextView.class);
    target.phoneEditText = Utils.findRequiredViewAsType(source, R.id.et_customer_phone, "field 'phoneEditText'", CustomEdittext.class);
    target.relRoot = Utils.findRequiredViewAsType(source, R.id.rel_root, "field 'relRoot'", RelativeLayout.class);
    target.linEmptyMobile = Utils.findRequiredViewAsType(source, R.id.lin_empty_mobile, "field 'linEmptyMobile'", LinearLayout.class);
    target.linEnterPhone = Utils.findRequiredViewAsType(source, R.id.lin_enter_phone, "field 'linEnterPhone'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.camera_img, "method 'OnClickBtn'");
    view7f0a00d1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickBtn(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.editAdditionalCustomerPhone, "method 'OnClickBtn'");
    view7f0a0137 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickBtn(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.editCustomerEmail, "method 'OnClickBtn'");
    view7f0a0138 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickBtn(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerInfoFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.customerNameTextView = null;
    target.dateOfBirthTextView = null;
    target.emailTextView = null;
    target.row_customerEmail = null;
    target.emailEditText = null;
    target.genderMaleRadioButton = null;
    target.genderFemaleRadioButton = null;
    target.genderRadioGroup = null;
    target.saveBtn = null;
    target.img_user = null;
    target.et_Customer_phoneone = null;
    target.txt_country_code = null;
    target.show_password = null;
    target.password_show = null;
    target.txt_country_code_anotherNumber = null;
    target.additional_customer_phone = null;
    target.phoneEditText = null;
    target.relRoot = null;
    target.linEmptyMobile = null;
    target.linEnterPhone = null;

    view7f0a00ba.setOnClickListener(null);
    view7f0a00ba = null;
    view7f0a0209.setOnClickListener(null);
    view7f0a0209 = null;
    view7f0a040b.setOnClickListener(null);
    view7f0a040b = null;
    view7f0a00d1.setOnClickListener(null);
    view7f0a00d1 = null;
    view7f0a0137.setOnClickListener(null);
    view7f0a0137 = null;
    view7f0a0138.setOnClickListener(null);
    view7f0a0138 = null;
  }
}
