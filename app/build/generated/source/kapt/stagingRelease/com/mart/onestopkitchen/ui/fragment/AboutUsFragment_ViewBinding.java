// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.webkit.WebView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AboutUsFragment_ViewBinding implements Unbinder {
  private AboutUsFragment target;

  @UiThread
  public AboutUsFragment_ViewBinding(AboutUsFragment target, View source) {
    this.target = target;

    target.wv_about_us = Utils.findRequiredViewAsType(source, R.id.wv_about_us, "field 'wv_about_us'", WebView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AboutUsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.wv_about_us = null;
  }
}
