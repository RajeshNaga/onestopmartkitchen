// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.activity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;
import pl.droidsonroids.gif.GifImageView;

public class SplashScreenActivity_ViewBinding implements Unbinder {
  private SplashScreenActivity target;

  private View view7f0a00b1;

  @UiThread
  public SplashScreenActivity_ViewBinding(SplashScreenActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SplashScreenActivity_ViewBinding(final SplashScreenActivity target, View source) {
    this.target = target;

    View view;
    target.txtToUseApp = Utils.findRequiredViewAsType(source, R.id.txt_to_use_app, "field 'txtToUseApp'", TextView.class);
    target.linAllowGif = Utils.findRequiredViewAsType(source, R.id.lin_allow_gif, "field 'linAllowGif'", LinearLayout.class);
    target.relPermission = Utils.findRequiredViewAsType(source, R.id.rel_permission, "field 'relPermission'", RelativeLayout.class);
    target.relPermissionUi = Utils.findRequiredViewAsType(source, R.id.rel_permission_ui, "field 'relPermissionUi'", RelativeLayout.class);
    target.relSplLoading = Utils.findRequiredViewAsType(source, R.id.rel_spl_loading, "field 'relSplLoading'", RelativeLayout.class);
    target.txtToGetStarted = Utils.findRequiredViewAsType(source, R.id.txt_to_get_started, "field 'txtToGetStarted'", TextView.class);
    target.gif = Utils.findRequiredViewAsType(source, R.id.gif, "field 'gif'", GifImageView.class);
    view = Utils.findRequiredView(source, R.id.btn_next, "method 'onViewClicked'");
    view7f0a00b1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SplashScreenActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtToUseApp = null;
    target.linAllowGif = null;
    target.relPermission = null;
    target.relPermissionUi = null;
    target.relSplLoading = null;
    target.txtToGetStarted = null;
    target.gif = null;

    view7f0a00b1.setOnClickListener(null);
    view7f0a00b1 = null;
  }
}
