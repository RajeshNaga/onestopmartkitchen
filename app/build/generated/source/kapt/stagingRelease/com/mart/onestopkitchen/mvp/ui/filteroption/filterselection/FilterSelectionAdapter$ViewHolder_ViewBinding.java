// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filteroption.filterselection;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterSelectionAdapter$ViewHolder_ViewBinding implements Unbinder {
  private FilterSelectionAdapter.ViewHolder target;

  @UiThread
  public FilterSelectionAdapter$ViewHolder_ViewBinding(FilterSelectionAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.txtTitle = Utils.findRequiredViewAsType(source, R.id.txt_title, "field 'txtTitle'", TextView.class);
    target.cheSelectedItem = Utils.findRequiredViewAsType(source, R.id.che_selected_item, "field 'cheSelectedItem'", CheckBox.class);
    target.relativeLayout = Utils.findRequiredViewAsType(source, R.id.rel_root, "field 'relativeLayout'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterSelectionAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTitle = null;
    target.cheSelectedItem = null;
    target.relativeLayout = null;
  }
}
