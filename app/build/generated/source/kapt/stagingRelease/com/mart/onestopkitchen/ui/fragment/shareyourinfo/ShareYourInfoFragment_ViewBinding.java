// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.shareyourinfo;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ShareYourInfoFragment_ViewBinding implements Unbinder {
  private ShareYourInfoFragment target;

  private View view7f0a054c;

  private View view7f0a0381;

  private View view7f0a00bb;

  @UiThread
  public ShareYourInfoFragment_ViewBinding(final ShareYourInfoFragment target, View source) {
    this.target = target;

    View view;
    target.txtHeader = Utils.findRequiredViewAsType(source, R.id.txt_header, "field 'txtHeader'", TextView.class);
    target.txtOr = Utils.findRequiredViewAsType(source, R.id.txt_or, "field 'txtOr'", TextView.class);
    view = Utils.findRequiredView(source, R.id.txt_enter_manually_size, "field 'txtEnterManuallySize' and method 'onViewClicked'");
    target.txtEnterManuallySize = Utils.castView(view, R.id.txt_enter_manually_size, "field 'txtEnterManuallySize'", TextView.class);
    view7f0a054c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtSelete = Utils.findRequiredViewAsType(source, R.id.txt_selete, "field 'txtSelete'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rel_fav_brand_select, "field 'relFavBrandSelect' and method 'onViewClicked'");
    target.relFavBrandSelect = Utils.castView(view, R.id.rel_fav_brand_select, "field 'relFavBrandSelect'", RelativeLayout.class);
    view7f0a0381 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.linSelectedSizes = Utils.findRequiredViewAsType(source, R.id.lin_selected_sizes, "field 'linSelectedSizes'", LinearLayout.class);
    target.rvSizes = Utils.findRequiredViewAsType(source, R.id.rv_sizes, "field 'rvSizes'", RecyclerView.class);
    target.txtEnterSize = Utils.findRequiredViewAsType(source, R.id.txt_enter_size, "field 'txtEnterSize'", TextView.class);
    target.rvManuallySizes = Utils.findRequiredViewAsType(source, R.id.rv_manually_sizes, "field 'rvManuallySizes'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_search, "field 'btnSearch' and method 'onViewClicked'");
    target.btnSearch = Utils.castView(view, R.id.btn_search, "field 'btnSearch'", TextView.class);
    view7f0a00bb = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ShareYourInfoFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtHeader = null;
    target.txtOr = null;
    target.txtEnterManuallySize = null;
    target.txtSelete = null;
    target.relFavBrandSelect = null;
    target.linSelectedSizes = null;
    target.rvSizes = null;
    target.txtEnterSize = null;
    target.rvManuallySizes = null;
    target.btnSearch = null;

    view7f0a054c.setOnClickListener(null);
    view7f0a054c = null;
    view7f0a0381.setOnClickListener(null);
    view7f0a0381 = null;
    view7f0a00bb.setOnClickListener(null);
    view7f0a00bb = null;
  }
}
