// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.products.view;

import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductNavigationProduct_ViewBinding implements Unbinder {
  private ProductNavigationProduct target;

  @UiThread
  public ProductNavigationProduct_ViewBinding(ProductNavigationProduct target, View source) {
    this.target = target;

    target.subCategoryRecyclerList = Utils.findRequiredViewAsType(source, R.id.rv_subcategory, "field 'subCategoryRecyclerList'", RecyclerView.class);
    target.back = Utils.findRequiredViewAsType(source, R.id.ll_back, "field 'back'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductNavigationProduct target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.subCategoryRecyclerList = null;
    target.back = null;
  }
}
