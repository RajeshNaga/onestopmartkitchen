// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductListFragment_ViewBinding implements Unbinder {
  private ProductListFragment target;

  @UiThread
  public ProductListFragment_ViewBinding(ProductListFragment target, View source) {
    this.target = target;

    target.listProduct = Utils.findRequiredViewAsType(source, R.id.list_product, "field 'listProduct'", RecyclerView.class);
    target.rootViewRelativeLayout = Utils.findRequiredViewAsType(source, R.id.rl_rootLayout, "field 'rootViewRelativeLayout'", RelativeLayout.class);
    target.categoryNameTextView = Utils.findRequiredViewAsType(source, R.id.tv_category_name, "field 'categoryNameTextView'", TextView.class);
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawerLayout, "field 'drawerLayout'", DrawerLayout.class);
    target.bottomSheetLayout = Utils.findRequiredViewAsType(source, R.id.bottomsheet, "field 'bottomSheetLayout'", BottomSheetLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductListFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.listProduct = null;
    target.rootViewRelativeLayout = null;
    target.categoryNameTextView = null;
    target.drawerLayout = null;
    target.bottomSheetLayout = null;
  }
}
