// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductListFragmentFor3_8_ViewBinding implements Unbinder {
  private ProductListFragmentFor3_8 target;

  private View view7f0a03a1;

  private View view7f0a039b;

  @UiThread
  public ProductListFragmentFor3_8_ViewBinding(final ProductListFragmentFor3_8 target,
      View source) {
    this.target = target;

    View view;
    target.listProduct = Utils.findRequiredViewAsType(source, R.id.list_product, "field 'listProduct'", RecyclerView.class);
    target.rootViewRelativeLayout = Utils.findRequiredViewAsType(source, R.id.rl_rootLayout, "field 'rootViewRelativeLayout'", RelativeLayout.class);
    target.categoryNameTextView = Utils.findRequiredViewAsType(source, R.id.tv_category_name, "field 'categoryNameTextView'", TextView.class);
    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawerLayout, "field 'drawerLayout'", DrawerLayout.class);
    target.bottomSheetLayout = Utils.findRequiredViewAsType(source, R.id.bottomsheet, "field 'bottomSheetLayout'", BottomSheetLayout.class);
    view = Utils.findRequiredView(source, R.id.rl_sortby, "field 'rl_sortby' and method 'showSortByView'");
    target.rl_sortby = Utils.castView(view, R.id.rl_sortby, "field 'rl_sortby'", RelativeLayout.class);
    view7f0a03a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rl_filter, "field 'rl_filter' and method 'showSortByView'");
    target.rl_filter = Utils.castView(view, R.id.rl_filter, "field 'rl_filter'", RelativeLayout.class);
    view7f0a039b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductListFragmentFor3_8 target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.listProduct = null;
    target.rootViewRelativeLayout = null;
    target.categoryNameTextView = null;
    target.drawerLayout = null;
    target.bottomSheetLayout = null;
    target.rl_sortby = null;
    target.rl_filter = null;

    view7f0a03a1.setOnClickListener(null);
    view7f0a03a1 = null;
    view7f0a039b.setOnClickListener(null);
    view7f0a039b = null;
  }
}
