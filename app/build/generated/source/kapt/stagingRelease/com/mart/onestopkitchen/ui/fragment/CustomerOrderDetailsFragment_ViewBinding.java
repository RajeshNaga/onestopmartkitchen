// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerOrderDetailsFragment_ViewBinding implements Unbinder {
  private CustomerOrderDetailsFragment target;

  @UiThread
  public CustomerOrderDetailsFragment_ViewBinding(CustomerOrderDetailsFragment target,
      View source) {
    this.target = target;

    target.nesScrell = Utils.findRequiredViewAsType(source, R.id.nes_screll, "field 'nesScrell'", NestedScrollView.class);
    target.subTotalTextView = Utils.findRequiredViewAsType(source, R.id.tv_subtotal, "field 'subTotalTextView'", TextView.class);
    target.shippingTextView = Utils.findRequiredViewAsType(source, R.id.tv_shipping, "field 'shippingTextView'", TextView.class);
    target.taxTextView = Utils.findRequiredViewAsType(source, R.id.tv_tax, "field 'taxTextView'", TextView.class);
    target.totalAmountTextView = Utils.findRequiredViewAsType(source, R.id.tv_Total, "field 'totalAmountTextView'", TextView.class);
    target.discountTextView = Utils.findRequiredViewAsType(source, R.id.tv_discount, "field 'discountTextView'", TextView.class);
    target.orderProductList = Utils.findRequiredViewAsType(source, R.id.rclv_orderProductList, "field 'orderProductList'", RecyclerView.class);
    target.orderstatusList = Utils.findRequiredViewAsType(source, R.id.rclv_orderstatus, "field 'orderstatusList'", RecyclerView.class);
    target.billingAddressLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_billing_address, "field 'billingAddressLinearLayout'", LinearLayout.class);
    target.ShippingAddressLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_shipping_address, "field 'ShippingAddressLinearLayout'", LinearLayout.class);
    target.orderTitleTextView = Utils.findRequiredViewAsType(source, R.id.tv_order_title, "field 'orderTitleTextView'", TextView.class);
    target.orderSummeryTextView = Utils.findRequiredViewAsType(source, R.id.ordered_and_approved, "field 'orderSummeryTextView'", TextView.class);
    target.paymentDetailsTextView = Utils.findRequiredViewAsType(source, R.id.tv_payment_details, "field 'paymentDetailsTextView'", TextView.class);
    target.shippingDetailsTextView = Utils.findRequiredViewAsType(source, R.id.tv_shipping_details, "field 'shippingDetailsTextView'", TextView.class);
    target.checkoutAttrInfoTextView = Utils.findRequiredViewAsType(source, R.id.tv_checkout_attr_info, "field 'checkoutAttrInfoTextView'", TextView.class);
    target.tv_total_amount = Utils.findRequiredViewAsType(source, R.id.tv_total_amount, "field 'tv_total_amount'", TextView.class);
    target.tv_full_address = Utils.findRequiredViewAsType(source, R.id.tv_full_address, "field 'tv_full_address'", AutoCompleteTextView.class);
    target.tv_full_address2 = Utils.findRequiredViewAsType(source, R.id.tv_full_address2, "field 'tv_full_address2'", AutoCompleteTextView.class);
    target.tv_payment_mode = Utils.findRequiredViewAsType(source, R.id.tv_payment_mode, "field 'tv_payment_mode'", TextView.class);
    target.tv_product_refrence_number = Utils.findRequiredViewAsType(source, R.id.tv_product_refrence_number, "field 'tv_product_refrence_number'", TextView.class);
    target.tv_name = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'tv_name'", TextView.class);
    target.tv_product_order_number = Utils.findRequiredViewAsType(source, R.id.tv_product_order_number, "field 'tv_product_order_number'", TextView.class);
    target.ll_store_address = Utils.findRequiredViewAsType(source, R.id.ll_store_address, "field 'll_store_address'", LinearLayout.class);
    target.tv_product_amount = Utils.findRequiredViewAsType(source, R.id.tv_product_amount, "field 'tv_product_amount'", TextView.class);
    target.shippingLayout = Utils.findRequiredViewAsType(source, R.id.shippingLayout, "field 'shippingLayout'", LinearLayout.class);
    target.storeLayouts = Utils.findRequiredViewAsType(source, R.id.storeLayouts, "field 'storeLayouts'", LinearLayout.class);
    target.taxKey = Utils.findRequiredViewAsType(source, R.id.taxKey, "field 'taxKey'", TextView.class);
    target.tv_phone_number = Utils.findRequiredViewAsType(source, R.id.tv_phone_number, "field 'tv_phone_number'", TextView.class);
    target.taxrow = Utils.findRequiredViewAsType(source, R.id.tax_row, "field 'taxrow'", TableRow.class);
    target.deliverypoint = Utils.findRequiredViewAsType(source, R.id.deliverypoint, "field 'deliverypoint'", TextView.class);
    target.customerName = Utils.findRequiredViewAsType(source, R.id.customerName, "field 'customerName'", TextView.class);
    target.tv_falatNumber = Utils.findRequiredViewAsType(source, R.id.tv_falatNumber, "field 'tv_falatNumber'", TextView.class);
    target.tvPaymentStatus = Utils.findRequiredViewAsType(source, R.id.tvPaymentStatus, "field 'tvPaymentStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerOrderDetailsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.nesScrell = null;
    target.subTotalTextView = null;
    target.shippingTextView = null;
    target.taxTextView = null;
    target.totalAmountTextView = null;
    target.discountTextView = null;
    target.orderProductList = null;
    target.orderstatusList = null;
    target.billingAddressLinearLayout = null;
    target.ShippingAddressLinearLayout = null;
    target.orderTitleTextView = null;
    target.orderSummeryTextView = null;
    target.paymentDetailsTextView = null;
    target.shippingDetailsTextView = null;
    target.checkoutAttrInfoTextView = null;
    target.tv_total_amount = null;
    target.tv_full_address = null;
    target.tv_full_address2 = null;
    target.tv_payment_mode = null;
    target.tv_product_refrence_number = null;
    target.tv_name = null;
    target.tv_product_order_number = null;
    target.ll_store_address = null;
    target.tv_product_amount = null;
    target.shippingLayout = null;
    target.storeLayouts = null;
    target.taxKey = null;
    target.tv_phone_number = null;
    target.taxrow = null;
    target.deliverypoint = null;
    target.customerName = null;
    target.tv_falatNumber = null;
    target.tvPaymentStatus = null;
  }
}
