// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.products.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductListFragment_ViewBinding implements Unbinder {
  private ProductListFragment target;

  private View view7f0a03a1;

  private View view7f0a039b;

  private View view7f0a01bf;

  @UiThread
  public ProductListFragment_ViewBinding(final ProductListFragment target, View source) {
    this.target = target;

    View view;
    target.sortFilterView = Utils.findRequiredView(source, R.id.sub_filter, "field 'sortFilterView'");
    target.listProduct = Utils.findRequiredViewAsType(source, R.id.rv_filter_list, "field 'listProduct'", RecyclerView.class);
    target.llProductList = Utils.findRequiredViewAsType(source, R.id.ll_product_list, "field 'llProductList'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.rl_sortby, "field 'rl_sortby' and method 'showSortByView'");
    target.rl_sortby = Utils.castView(view, R.id.rl_sortby, "field 'rl_sortby'", RelativeLayout.class);
    view7f0a03a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rl_filter, "field 'rl_filter' and method 'showSortByView'");
    target.rl_filter = Utils.castView(view, R.id.rl_filter, "field 'rl_filter'", RelativeLayout.class);
    view7f0a039b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
    target.bottomSheetLayout = Utils.findRequiredViewAsType(source, R.id.bottomsheet, "field 'bottomSheetLayout'", BottomSheetLayout.class);
    target.shimmerFrameLayout = Utils.findRequiredViewAsType(source, R.id.shimmerLayout, "field 'shimmerFrameLayout'", ShimmerFrameLayout.class);
    view = Utils.findRequiredView(source, R.id.grid_view, "field 'gridView' and method 'showSortByView'");
    target.gridView = Utils.castView(view, R.id.grid_view, "field 'gridView'", ImageView.class);
    view7f0a01bf = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
    target.gridViewBorder = Utils.findRequiredView(source, R.id.grid_view_border, "field 'gridViewBorder'");
    target.noRecordFound = Utils.findRequiredViewAsType(source, R.id.txt_no_record_found, "field 'noRecordFound'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductListFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.sortFilterView = null;
    target.listProduct = null;
    target.llProductList = null;
    target.rl_sortby = null;
    target.rl_filter = null;
    target.bottomSheetLayout = null;
    target.shimmerFrameLayout = null;
    target.gridView = null;
    target.gridViewBorder = null;
    target.noRecordFound = null;

    view7f0a03a1.setOnClickListener(null);
    view7f0a03a1 = null;
    view7f0a039b.setOnClickListener(null);
    view7f0a039b = null;
    view7f0a01bf.setOnClickListener(null);
    view7f0a01bf = null;
  }
}
