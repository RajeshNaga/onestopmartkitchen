// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CartAdapter$ProductSummaryHolder_ViewBinding implements Unbinder {
  private CartAdapter.ProductSummaryHolder target;

  @UiThread
  public CartAdapter$ProductSummaryHolder_ViewBinding(CartAdapter.ProductSummaryHolder target,
      View source) {
    this.target = target;

    target.tvDeliveryDate = Utils.findRequiredViewAsType(source, R.id.tvDeliveryDate, "field 'tvDeliveryDate'", TextView.class);
    target.productImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'productImage'", ImageView.class);
    target.productPrice = Utils.findRequiredViewAsType(source, R.id.tv_productPrice, "field 'productPrice'", TextView.class);
    target.productName = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'productName'", TextView.class);
    target.productShortdescription = Utils.findRequiredViewAsType(source, R.id.tv_product_short_descrption, "field 'productShortdescription'", TextView.class);
    target.productQuantity = Utils.findRequiredViewAsType(source, R.id.et_quantity, "field 'productQuantity'", TextView.class);
    target.removeItem = Utils.findRequiredViewAsType(source, R.id.tv_remove, "field 'removeItem'", TextView.class);
    target.tvAddToWishLish = Utils.findRequiredViewAsType(source, R.id.tv_add_to_wishlish, "field 'tvAddToWishLish'", TextView.class);
    target.qunatityUpImageView = Utils.findRequiredViewAsType(source, R.id.iv_up, "field 'qunatityUpImageView'", ImageView.class);
    target.qunatityDownImageView = Utils.findRequiredViewAsType(source, R.id.iv_down, "field 'qunatityDownImageView'", ImageView.class);
    target.tvStockMsg = Utils.findRequiredViewAsType(source, R.id.tvStockMsg, "field 'tvStockMsg'", TextView.class);
    target.productsAttributes = Utils.findRequiredViewAsType(source, R.id.tvAttributes, "field 'productsAttributes'", TextView.class);
    target.tvOldPrice = Utils.findRequiredViewAsType(source, R.id.tvOldPrice, "field 'tvOldPrice'", TextView.class);
    target.tvOffer = Utils.findRequiredViewAsType(source, R.id.tvOffer, "field 'tvOffer'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CartAdapter.ProductSummaryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvDeliveryDate = null;
    target.productImage = null;
    target.productPrice = null;
    target.productName = null;
    target.productShortdescription = null;
    target.productQuantity = null;
    target.removeItem = null;
    target.tvAddToWishLish = null;
    target.qunatityUpImageView = null;
    target.qunatityDownImageView = null;
    target.tvStockMsg = null;
    target.productsAttributes = null;
    target.tvOldPrice = null;
    target.tvOffer = null;
  }
}
