package com.mart.onestopkitchen.databinding;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentPaymentDoneSuccessfullyBindingImpl extends FragmentPaymentDoneSuccessfullyBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView2, 11);
        sViewsWithIds.put(R.id.imageView2, 12);
        sViewsWithIds.put(R.id.textView4, 13);
        sViewsWithIds.put(R.id.rv_order_group, 14);
        sViewsWithIds.put(R.id.textView6, 15);
        sViewsWithIds.put(R.id.llBtn, 16);
        sViewsWithIds.put(R.id.btnPDF, 17);
        sViewsWithIds.put(R.id.btnDone, 18);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentPaymentDoneSuccessfullyBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private FragmentPaymentDoneSuccessfullyBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[18]
            , (android.widget.Button) bindings[17]
            , (android.widget.ImageView) bindings[12]
            , (android.widget.LinearLayout) bindings[16]
            , (androidx.recyclerview.widget.RecyclerView) bindings[14]
            , (android.widget.TableRow) bindings[8]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[1]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.taxRow.setTag(null);
        this.tvAddress.setTag(null);
        this.tvPhoneNumber.setTag(null);
        this.tvProductAmount.setTag(null);
        this.tvRefrenceNumber.setTag(null);
        this.tvShippingCharge.setTag(null);
        this.tvTaxCharge.setTag(null);
        this.tvTotalAmount.setTag(null);
        this.tvTotalOrderCount.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.totalAmount == variableId) {
            setTotalAmount((android.text.Spannable) variable);
        }
        else if (BR.model == variableId) {
            setModel((com.mart.onestopkitchen.model.PaymentResponseModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setTotalAmount(@Nullable android.text.Spannable TotalAmount) {
        this.mTotalAmount = TotalAmount;
    }
    public void setModel(@Nullable com.mart.onestopkitchen.model.PaymentResponseModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean modelOrdersSizeInt0 = false;
        android.text.Spannable appUtilsGetMMKStringTvShippingChargeAndroidDimen12sdpModelShippingChargesStartsWithStringValueOfInt0TvShippingChargeAndroidStringFreeModelShippingChargesInt0 = null;
        java.lang.String modelReferenceNumber = null;
        java.lang.String preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSIDEqualsStringValueOfInt0MboundView3AndroidStringDeliveryAddressMboundView3AndroidStringPickupAddress = null;
        java.lang.String modelShippingAddress = null;
        java.lang.String modelShippingCharges = null;
        java.lang.String modelProductAmount = null;
        android.text.Spannable appUtilsGetMMKStringTvTaxChargeAndroidDimen12sdpModelTotalTaxInt0 = null;
        int modelTotalTaxStartsWithStringValueOfInt0ViewGONEViewVISIBLE = 0;
        android.text.Spannable appUtilsGetMMKStringTvTotalAmountAndroidDimen15sdpModelTotalPaidAmountInt0 = null;
        int modelOrdersSize = 0;
        com.mart.onestopkitchen.model.PaymentResponseModel model = mModel;
        boolean preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSIDEqualsStringValueOfInt0 = false;
        java.lang.String appUtilsFormattedNumberModelPhoneNumber = null;
        boolean modelShippingChargesStartsWithStringValueOfInt0 = false;
        android.text.Spannable appUtilsGetMMKStringTvProductAmountAndroidDimen12sdpModelProductAmountInt0 = null;
        int ModelOrdersSize1 = 0;
        java.lang.String preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSID = null;
        java.lang.String tvTotalOrderCountAndroidStringTotalOrderNumberModelOrdersSize = null;
        java.lang.Object modelOrdersSizeInt0TvTotalOrderCountAndroidStringTotalOrderNumberModelOrdersSizeInt0 = null;
        java.lang.String modelTotalPaidAmount = null;
        boolean modelTotalTaxStartsWithStringValueOfInt0 = false;
        java.lang.String modelTotalTax = null;
        java.lang.String preferenceServicePICKUPPOINTADDRESSID = null;
        java.lang.String modelPhoneNumber = null;
        java.util.List<com.mart.onestopkitchen.model.OrdersModel> modelOrders = null;
        java.lang.String modelShippingChargesStartsWithStringValueOfInt0TvShippingChargeAndroidStringFreeModelShippingCharges = null;

        if ((dirtyFlags & 0x6L) != 0) {



                if (model != null) {
                    // read model.referenceNumber
                    modelReferenceNumber = model.getReferenceNumber();
                    // read model.shippingAddress
                    modelShippingAddress = model.getShippingAddress();
                    // read model.shippingCharges
                    modelShippingCharges = model.getShippingCharges();
                    // read model.productAmount
                    modelProductAmount = model.getProductAmount();
                    // read model.totalPaidAmount
                    modelTotalPaidAmount = model.getTotalPaidAmount();
                    // read model.TotalTax
                    modelTotalTax = model.getTotalTax();
                    // read model.phoneNumber
                    modelPhoneNumber = model.getPhoneNumber();
                    // read model.orders
                    modelOrders = model.getOrders();
                }


                if (modelShippingCharges != null) {
                    // read model.shippingCharges.startsWith(String.valueOf(0))
                    modelShippingChargesStartsWithStringValueOfInt0 = modelShippingCharges.startsWith(java.lang.String.valueOf(0));
                }
            if((dirtyFlags & 0x6L) != 0) {
                if(modelShippingChargesStartsWithStringValueOfInt0) {
                        dirtyFlags |= 0x400L;
                }
                else {
                        dirtyFlags |= 0x200L;
                }
            }
                // read AppUtils.getMMKString(@android:dimen/_12sdp, model.productAmount, 0)
                appUtilsGetMMKStringTvProductAmountAndroidDimen12sdpModelProductAmountInt0 = com.mart.onestopkitchen.utils.AppUtils.getMMKString(tvProductAmount.getResources().getDimension(R.dimen._12sdp), modelProductAmount, 0);
                // read AppUtils.getMMKString(@android:dimen/_15sdp, model.totalPaidAmount, 0)
                appUtilsGetMMKStringTvTotalAmountAndroidDimen15sdpModelTotalPaidAmountInt0 = com.mart.onestopkitchen.utils.AppUtils.getMMKString(tvTotalAmount.getResources().getDimension(R.dimen._15sdp), modelTotalPaidAmount, 0);
                // read AppUtils.getMMKString(@android:dimen/_12sdp, model.TotalTax, 0)
                appUtilsGetMMKStringTvTaxChargeAndroidDimen12sdpModelTotalTaxInt0 = com.mart.onestopkitchen.utils.AppUtils.getMMKString(tvTaxCharge.getResources().getDimension(R.dimen._12sdp), modelTotalTax, 0);
                // read AppUtils.formattedNumber(model.phoneNumber)
                appUtilsFormattedNumberModelPhoneNumber = com.mart.onestopkitchen.utils.AppUtils.formattedNumber(modelPhoneNumber);
                if (modelTotalTax != null) {
                    // read model.TotalTax.startsWith(String.valueOf(0))
                    modelTotalTaxStartsWithStringValueOfInt0 = modelTotalTax.startsWith(java.lang.String.valueOf(0));
                }
            if((dirtyFlags & 0x6L) != 0) {
                if(modelTotalTaxStartsWithStringValueOfInt0) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }
                if (modelOrders != null) {
                    // read model.orders.size
                    ModelOrdersSize1 = modelOrders.size();
                }


                // read model.TotalTax.startsWith(String.valueOf(0)) ? View.GONE : View.VISIBLE
                modelTotalTaxStartsWithStringValueOfInt0ViewGONEViewVISIBLE = ((modelTotalTaxStartsWithStringValueOfInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                // read model.orders.size > 0
                modelOrdersSizeInt0 = (ModelOrdersSize1) > (0);
            if((dirtyFlags & 0x6L) != 0) {
                if(modelOrdersSizeInt0) {
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x80L;
                }
            }
        }
        if ((dirtyFlags & 0x4L) != 0) {

                // read PreferenceService.PICKUP_POINT_ADDRESS_ID
                preferenceServicePICKUPPOINTADDRESSID = com.mart.onestopkitchen.service.PreferenceService.PICKUP_POINT_ADDRESS_ID;


                // read PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID)
                preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSID = com.mart.onestopkitchen.service.PreferenceService.getInstance().GetPreferenceValue(preferenceServicePICKUPPOINTADDRESSID);


                if (preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSID != null) {
                    // read PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID).equals(String.valueOf(0))
                    preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSIDEqualsStringValueOfInt0 = preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSID.equals(java.lang.String.valueOf(0));
                }
            if((dirtyFlags & 0x4L) != 0) {
                if(preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSIDEqualsStringValueOfInt0) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID).equals(String.valueOf(0)) ? @android:string/delivery_address : @android:string/pickup_address
                preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSIDEqualsStringValueOfInt0MboundView3AndroidStringDeliveryAddressMboundView3AndroidStringPickupAddress = ((preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSIDEqualsStringValueOfInt0) ? (mboundView3.getResources().getString(R.string.delivery_address)) : (mboundView3.getResources().getString(R.string.pickup_address)));
        }
        // batch finished

        if ((dirtyFlags & 0x100L) != 0) {

                if (modelOrders != null) {
                    // read model.orders.size()
                    modelOrdersSize = modelOrders.size();
                }


                // read (@android:string/total_order_number) + (model.orders.size())
                tvTotalOrderCountAndroidStringTotalOrderNumberModelOrdersSize = (tvTotalOrderCount.getResources().getString(R.string.total_order_number)) + (modelOrdersSize);
        }
        if ((dirtyFlags & 0x6L) != 0) {

                // read model.shippingCharges.startsWith(String.valueOf(0)) ? @android:string/free : model.shippingCharges
                modelShippingChargesStartsWithStringValueOfInt0TvShippingChargeAndroidStringFreeModelShippingCharges = ((modelShippingChargesStartsWithStringValueOfInt0) ? (tvShippingCharge.getResources().getString(R.string.free)) : (modelShippingCharges));


                // read AppUtils.getMMKString(@android:dimen/_12sdp, model.shippingCharges.startsWith(String.valueOf(0)) ? @android:string/free : model.shippingCharges, 0)
                appUtilsGetMMKStringTvShippingChargeAndroidDimen12sdpModelShippingChargesStartsWithStringValueOfInt0TvShippingChargeAndroidStringFreeModelShippingChargesInt0 = com.mart.onestopkitchen.utils.AppUtils.getMMKString(tvShippingCharge.getResources().getDimension(R.dimen._12sdp), modelShippingChargesStartsWithStringValueOfInt0TvShippingChargeAndroidStringFreeModelShippingCharges, 0);
        }

        if ((dirtyFlags & 0x6L) != 0) {

                // read model.orders.size > 0 ? (@android:string/total_order_number) + (model.orders.size()) : 0
                modelOrdersSizeInt0TvTotalOrderCountAndroidStringTotalOrderNumberModelOrdersSizeInt0 = ((modelOrdersSizeInt0) ? (tvTotalOrderCountAndroidStringTotalOrderNumberModelOrdersSize) : (0));
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, preferenceServiceGetInstanceGetPreferenceValuePreferenceServicePICKUPPOINTADDRESSIDEqualsStringValueOfInt0MboundView3AndroidStringDeliveryAddressMboundView3AndroidStringPickupAddress);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            this.taxRow.setVisibility(modelTotalTaxStartsWithStringValueOfInt0ViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvAddress, modelShippingAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPhoneNumber, appUtilsFormattedNumberModelPhoneNumber);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProductAmount, appUtilsGetMMKStringTvProductAmountAndroidDimen12sdpModelProductAmountInt0);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvRefrenceNumber, modelReferenceNumber);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvShippingCharge, appUtilsGetMMKStringTvShippingChargeAndroidDimen12sdpModelShippingChargesStartsWithStringValueOfInt0TvShippingChargeAndroidStringFreeModelShippingChargesInt0);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTaxCharge, appUtilsGetMMKStringTvTaxChargeAndroidDimen12sdpModelTotalTaxInt0);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalAmount, appUtilsGetMMKStringTvTotalAmountAndroidDimen15sdpModelTotalPaidAmountInt0);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalOrderCount, (java.lang.CharSequence) modelOrdersSizeInt0TvTotalOrderCountAndroidStringTotalOrderNumberModelOrdersSizeInt0);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): totalAmount
        flag 1 (0x2L): model
        flag 2 (0x3L): null
        flag 3 (0x4L): PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID).equals(String.valueOf(0)) ? @android:string/delivery_address : @android:string/pickup_address
        flag 4 (0x5L): PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID).equals(String.valueOf(0)) ? @android:string/delivery_address : @android:string/pickup_address
        flag 5 (0x6L): model.TotalTax.startsWith(String.valueOf(0)) ? View.GONE : View.VISIBLE
        flag 6 (0x7L): model.TotalTax.startsWith(String.valueOf(0)) ? View.GONE : View.VISIBLE
        flag 7 (0x8L): model.orders.size > 0 ? (@android:string/total_order_number) + (model.orders.size()) : 0
        flag 8 (0x9L): model.orders.size > 0 ? (@android:string/total_order_number) + (model.orders.size()) : 0
        flag 9 (0xaL): model.shippingCharges.startsWith(String.valueOf(0)) ? @android:string/free : model.shippingCharges
        flag 10 (0xbL): model.shippingCharges.startsWith(String.valueOf(0)) ? @android:string/free : model.shippingCharges
    flag mapping end*/
    //end
}