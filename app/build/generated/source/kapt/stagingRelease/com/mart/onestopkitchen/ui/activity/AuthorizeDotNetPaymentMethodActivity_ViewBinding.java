// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AuthorizeDotNetPaymentMethodActivity_ViewBinding implements Unbinder {
  private AuthorizeDotNetPaymentMethodActivity target;

  @UiThread
  public AuthorizeDotNetPaymentMethodActivity_ViewBinding(
      AuthorizeDotNetPaymentMethodActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AuthorizeDotNetPaymentMethodActivity_ViewBinding(
      AuthorizeDotNetPaymentMethodActivity target, View source) {
    this.target = target;

    target.creditCardTypeSpinner = Utils.findRequiredViewAsType(source, R.id.spinner_credit_card, "field 'creditCardTypeSpinner'", Spinner.class);
    target.monthSpinner = Utils.findRequiredViewAsType(source, R.id.spinner_month, "field 'monthSpinner'", Spinner.class);
    target.yearSpinner = Utils.findRequiredViewAsType(source, R.id.spinner_year, "field 'yearSpinner'", Spinner.class);
    target.nameEditText = Utils.findRequiredViewAsType(source, R.id.et_card_holder_name, "field 'nameEditText'", EditText.class);
    target.cardNumberEditText = Utils.findRequiredViewAsType(source, R.id.et_card_number, "field 'cardNumberEditText'", EditText.class);
    target.cardCodeEditText = Utils.findRequiredViewAsType(source, R.id.et_card_code, "field 'cardCodeEditText'", EditText.class);
    target.continueBtn = Utils.findRequiredViewAsType(source, R.id.btn_continue, "field 'continueBtn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AuthorizeDotNetPaymentMethodActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.creditCardTypeSpinner = null;
    target.monthSpinner = null;
    target.yearSpinner = null;
    target.nameEditText = null;
    target.cardNumberEditText = null;
    target.cardCodeEditText = null;
    target.continueBtn = null;
  }
}
