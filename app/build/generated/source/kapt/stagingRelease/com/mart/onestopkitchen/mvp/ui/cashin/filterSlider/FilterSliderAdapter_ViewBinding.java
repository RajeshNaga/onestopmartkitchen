// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.cashin.filterSlider;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterSliderAdapter_ViewBinding implements Unbinder {
  private FilterSliderAdapter target;

  private View view7f0a037e;

  @UiThread
  public FilterSliderAdapter_ViewBinding(final FilterSliderAdapter target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.rel_check_box, "method 'onViewClicked'");
    view7f0a037e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view7f0a037e.setOnClickListener(null);
    view7f0a037e = null;
  }
}
