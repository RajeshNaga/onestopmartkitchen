// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductListFragmentOptional_ViewBinding implements Unbinder {
  private ProductListFragmentOptional target;

  @UiThread
  public ProductListFragmentOptional_ViewBinding(ProductListFragmentOptional target, View source) {
    this.target = target;

    target.drawerLayout = Utils.findRequiredViewAsType(source, R.id.drawerLayout, "field 'drawerLayout'", DrawerLayout.class);
    target.filterDrawer = Utils.findRequiredViewAsType(source, R.id.drawer_filter, "field 'filterDrawer'", FrameLayout.class);
    target.listProduct = Utils.findRequiredViewAsType(source, R.id.list_product, "field 'listProduct'", UltimateRecyclerView.class);
    target.rootViewRelativeLayout = Utils.findRequiredViewAsType(source, R.id.rl_rootLayout, "field 'rootViewRelativeLayout'", RelativeLayout.class);
    target.bottomSheetLayout = Utils.findRequiredViewAsType(source, R.id.bottomsheet, "field 'bottomSheetLayout'", BottomSheetLayout.class);
    target.productViewTypeImgBtn = Utils.findRequiredViewAsType(source, R.id.imgBtn_viewtype, "field 'productViewTypeImgBtn'", ImageButton.class);
    target.categoryNameTextView = Utils.findRequiredViewAsType(source, R.id.tv_category_name, "field 'categoryNameTextView'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductListFragmentOptional target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.drawerLayout = null;
    target.filterDrawer = null;
    target.listProduct = null;
    target.rootViewRelativeLayout = null;
    target.bottomSheetLayout = null;
    target.productViewTypeImgBtn = null;
    target.categoryNameTextView = null;
  }
}
