// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.activity;

import android.view.View;
import android.widget.ImageButton;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import com.viewpagerindicator.CirclePageIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FullScreenImageActivity_ViewBinding implements Unbinder {
  private FullScreenImageActivity target;

  @UiThread
  public FullScreenImageActivity_ViewBinding(FullScreenImageActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FullScreenImageActivity_ViewBinding(FullScreenImageActivity target, View source) {
    this.target = target;

    target.btnWishList = Utils.findRequiredViewAsType(source, R.id.btn_wishlist, "field 'btnWishList'", ImageButton.class);
    target.circlePageIndicator = Utils.findRequiredViewAsType(source, R.id.indicator, "field 'circlePageIndicator'", CirclePageIndicator.class);
    target.recyclerView_image = Utils.findRequiredViewAsType(source, R.id.recyclerView_image, "field 'recyclerView_image'", RecyclerView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.app_toolbar, "field 'toolbar'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FullScreenImageActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnWishList = null;
    target.circlePageIndicator = null;
    target.recyclerView_image = null;
    target.toolbar = null;
  }
}
