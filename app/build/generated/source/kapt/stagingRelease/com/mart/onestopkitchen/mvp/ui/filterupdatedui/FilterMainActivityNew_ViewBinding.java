// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filterupdatedui;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterMainActivityNew_ViewBinding implements Unbinder {
  private FilterMainActivityNew target;

  @UiThread
  public FilterMainActivityNew_ViewBinding(FilterMainActivityNew target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FilterMainActivityNew_ViewBinding(FilterMainActivityNew target, View source) {
    this.target = target;

    target.toolFilter = Utils.findRequiredViewAsType(source, R.id.tool_filter, "field 'toolFilter'", Toolbar.class);
    target.txtToolbar = Utils.findRequiredViewAsType(source, R.id.txt_toolbar, "field 'txtToolbar'", TextView.class);
    target.frameFilter = Utils.findRequiredViewAsType(source, R.id.frame_filter, "field 'frameFilter'", FrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterMainActivityNew target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolFilter = null;
    target.txtToolbar = null;
    target.frameFilter = null;
  }
}
