// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckoutOrderProductAdapter$ProductSummaryHolder_ViewBinding implements Unbinder {
  private CheckoutOrderProductAdapter.ProductSummaryHolder target;

  @UiThread
  public CheckoutOrderProductAdapter$ProductSummaryHolder_ViewBinding(
      CheckoutOrderProductAdapter.ProductSummaryHolder target, View source) {
    this.target = target;

    target.productImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'productImage'", ImageView.class);
    target.productPrice = Utils.findRequiredViewAsType(source, R.id.tv_productPrice, "field 'productPrice'", TextView.class);
    target.productName = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'productName'", TextView.class);
    target.productQuantity = Utils.findRequiredViewAsType(source, R.id.tv_product_quantity, "field 'productQuantity'", TextView.class);
    target.tvDeliveryDate = Utils.findRequiredViewAsType(source, R.id.tv_delivery_date, "field 'tvDeliveryDate'", TextView.class);
    target.tvStockMsg = Utils.findRequiredViewAsType(source, R.id.tv_stock_msg, "field 'tvStockMsg'", TextView.class);
    target.tvAttributes = Utils.findRequiredViewAsType(source, R.id.tvAttributes, "field 'tvAttributes'", TextView.class);
    target.tvOldPrice = Utils.findRequiredViewAsType(source, R.id.tvOldPrice, "field 'tvOldPrice'", TextView.class);
    target.tvOffer = Utils.findRequiredViewAsType(source, R.id.tvOffer, "field 'tvOffer'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CheckoutOrderProductAdapter.ProductSummaryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.productImage = null;
    target.productPrice = null;
    target.productName = null;
    target.productQuantity = null;
    target.tvDeliveryDate = null;
    target.tvStockMsg = null;
    target.tvAttributes = null;
    target.tvOldPrice = null;
    target.tvOffer = null;
  }
}
