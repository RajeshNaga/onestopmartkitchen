// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ForgotPasswordFragment_ViewBinding implements Unbinder {
  private ForgotPasswordFragment target;

  @UiThread
  public ForgotPasswordFragment_ViewBinding(ForgotPasswordFragment target, View source) {
    this.target = target;

    target.forgetSend = Utils.findRequiredViewAsType(source, R.id.forgetSend, "field 'forgetSend'", Button.class);
    target.etForgetEmail = Utils.findRequiredViewAsType(source, R.id.etForgetEmail, "field 'etForgetEmail'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ForgotPasswordFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.forgetSend = null;
    target.etForgetEmail = null;
  }
}
