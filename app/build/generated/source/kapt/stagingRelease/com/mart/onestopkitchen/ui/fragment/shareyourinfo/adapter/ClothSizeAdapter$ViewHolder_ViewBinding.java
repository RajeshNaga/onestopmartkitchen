// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.shareyourinfo.adapter;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ClothSizeAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ClothSizeAdapter.ViewHolder target;

  private View view7f0a055c;

  @UiThread
  public ClothSizeAdapter$ViewHolder_ViewBinding(final ClothSizeAdapter.ViewHolder target,
      View source) {
    this.target = target;

    View view;
    target.relSelectedPos = Utils.findRequiredViewAsType(source, R.id.rel_selected_pos, "field 'relSelectedPos'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.txt_size_guide, "field 'txtSizeGuide' and method 'onViewClicked'");
    target.txtSizeGuide = Utils.castView(view, R.id.txt_size_guide, "field 'txtSizeGuide'", TextView.class);
    view7f0a055c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ClothSizeAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.relSelectedPos = null;
    target.txtSizeGuide = null;

    view7f0a055c.setOnClickListener(null);
    view7f0a055c = null;
  }
}
