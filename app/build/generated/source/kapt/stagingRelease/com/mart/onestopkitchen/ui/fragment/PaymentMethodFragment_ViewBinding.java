// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.customview.RadioGridGroupforReyMaterial;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PaymentMethodFragment_ViewBinding implements Unbinder {
  private PaymentMethodFragment target;

  @UiThread
  public PaymentMethodFragment_ViewBinding(PaymentMethodFragment target, View source) {
    this.target = target;

    target.radioGridGroup = Utils.findRequiredViewAsType(source, R.id.rg_shipiingMethod, "field 'radioGridGroup'", RadioGridGroupforReyMaterial.class);
    target.continueBtn = Utils.findRequiredViewAsType(source, R.id.btn_continue, "field 'continueBtn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PaymentMethodFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.radioGridGroup = null;
    target.continueBtn = null;
  }
}
