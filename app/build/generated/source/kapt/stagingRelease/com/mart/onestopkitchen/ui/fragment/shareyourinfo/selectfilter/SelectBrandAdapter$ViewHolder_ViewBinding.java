// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectBrandAdapter$ViewHolder_ViewBinding implements Unbinder {
  private SelectBrandAdapter.ViewHolder target;

  private View view7f0a025c;

  @UiThread
  public SelectBrandAdapter$ViewHolder_ViewBinding(final SelectBrandAdapter.ViewHolder target,
      View source) {
    this.target = target;

    View view;
    target.txtTitle = Utils.findRequiredViewAsType(source, R.id.txt_title, "field 'txtTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.lin_root, "field 'linRoot' and method 'onViewClicked'");
    target.linRoot = Utils.castView(view, R.id.lin_root, "field 'linRoot'", LinearLayout.class);
    view7f0a025c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectBrandAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTitle = null;
    target.linRoot = null;

    view7f0a025c.setOnClickListener(null);
    view7f0a025c = null;
  }
}
