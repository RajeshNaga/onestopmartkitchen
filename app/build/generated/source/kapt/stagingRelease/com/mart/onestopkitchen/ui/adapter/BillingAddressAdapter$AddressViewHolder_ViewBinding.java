// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BillingAddressAdapter$AddressViewHolder_ViewBinding implements Unbinder {
  private BillingAddressAdapter.AddressViewHolder target;

  @UiThread
  public BillingAddressAdapter$AddressViewHolder_ViewBinding(
      BillingAddressAdapter.AddressViewHolder target, View source) {
    this.target = target;

    target.ll_billing_address = Utils.findRequiredViewAsType(source, R.id.ll_billing_address, "field 'll_billing_address'", LinearLayout.class);
    target.tv_username = Utils.findRequiredViewAsType(source, R.id.tv_username, "field 'tv_username'", TextView.class);
    target.tv_address = Utils.findRequiredViewAsType(source, R.id.tv_address, "field 'tv_address'", TextView.class);
    target.tv_phone_number = Utils.findRequiredViewAsType(source, R.id.tv_phone_number, "field 'tv_phone_number'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BillingAddressAdapter.AddressViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ll_billing_address = null;
    target.tv_username = null;
    target.tv_address = null;
    target.tv_phone_number = null;
  }
}
