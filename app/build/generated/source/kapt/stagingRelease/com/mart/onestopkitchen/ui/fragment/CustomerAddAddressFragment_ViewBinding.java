// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerAddAddressFragment_ViewBinding implements Unbinder {
  private CustomerAddAddressFragment target;

  @UiThread
  public CustomerAddAddressFragment_ViewBinding(CustomerAddAddressFragment target, View source) {
    this.target = target;

    target.firstNameEditText = Utils.findRequiredViewAsType(source, R.id.et_first_name, "field 'firstNameEditText'", EditText.class);
    target.lastNameEditText = Utils.findRequiredViewAsType(source, R.id.et_last_name, "field 'lastNameEditText'", EditText.class);
    target.emailEditText = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'emailEditText'", EditText.class);
    target.cityEditText = Utils.findRequiredViewAsType(source, R.id.et_city, "field 'cityEditText'", EditText.class);
    target.address1EditText = Utils.findRequiredViewAsType(source, R.id.et_address1, "field 'address1EditText'", EditText.class);
    target.address2EditText = Utils.findRequiredViewAsType(source, R.id.et_address2, "field 'address2EditText'", EditText.class);
    target.zipOrPostalCodeEditText = Utils.findRequiredViewAsType(source, R.id.et_zip_code, "field 'zipOrPostalCodeEditText'", EditText.class);
    target.faxNumberEditText = Utils.findRequiredViewAsType(source, R.id.et_fax_number, "field 'faxNumberEditText'", EditText.class);
    target.phoneNumberEditText = Utils.findRequiredViewAsType(source, R.id.et_phone_number, "field 'phoneNumberEditText'", EditText.class);
    target.companyEditText = Utils.findRequiredViewAsType(source, R.id.et_company, "field 'companyEditText'", EditText.class);
    target.submitButton = Utils.findRequiredViewAsType(source, R.id.btn_save, "field 'submitButton'", Button.class);
    target.countrySpinner = Utils.findRequiredViewAsType(source, R.id.sp_country, "field 'countrySpinner'", AppCompatSpinner.class);
    target.stateSpinner = Utils.findRequiredViewAsType(source, R.id.spinner_state, "field 'stateSpinner'", AppCompatSpinner.class);
    target.citySpinner = Utils.findRequiredViewAsType(source, R.id.spinner_city, "field 'citySpinner'", AppCompatSpinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerAddAddressFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.firstNameEditText = null;
    target.lastNameEditText = null;
    target.emailEditText = null;
    target.cityEditText = null;
    target.address1EditText = null;
    target.address2EditText = null;
    target.zipOrPostalCodeEditText = null;
    target.faxNumberEditText = null;
    target.phoneNumberEditText = null;
    target.companyEditText = null;
    target.submitButton = null;
    target.countrySpinner = null;
    target.stateSpinner = null;
    target.citySpinner = null;
  }
}
