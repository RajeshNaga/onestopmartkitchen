// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ConfirmOrderFragment_ViewBinding implements Unbinder {
  private ConfirmOrderFragment target;

  @UiThread
  public ConfirmOrderFragment_ViewBinding(ConfirmOrderFragment target, View source) {
    this.target = target;

    target.subTotalTextView = Utils.findRequiredViewAsType(source, R.id.tv_subtotal, "field 'subTotalTextView'", TextView.class);
    target.shippingTextView = Utils.findRequiredViewAsType(source, R.id.tv_shipping, "field 'shippingTextView'", TextView.class);
    target.taxTextView = Utils.findRequiredViewAsType(source, R.id.tv_tax, "field 'taxTextView'", TextView.class);
    target.taxRow = Utils.findRequiredViewAsType(source, R.id.taxRow, "field 'taxRow'", TableRow.class);
    target.totalAmountTextView = Utils.findRequiredViewAsType(source, R.id.tv_Total, "field 'totalAmountTextView'", TextView.class);
    target.discountTextView = Utils.findRequiredViewAsType(source, R.id.tv_discount, "field 'discountTextView'", TextView.class);
    target.confirmButton = Utils.findRequiredViewAsType(source, R.id.btn_continue, "field 'confirmButton'", Button.class);
    target.discountTableRow = Utils.findRequiredViewAsType(source, R.id.tr_discount, "field 'discountTableRow'", TableRow.class);
    target.checkoutProductList = Utils.findRequiredViewAsType(source, R.id.rclv_chekoutProductList, "field 'checkoutProductList'", RecyclerView.class);
    target.billingAddressLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_billing_address, "field 'billingAddressLinearLayout'", LinearLayout.class);
    target.ll_shipping_address = Utils.findRequiredViewAsType(source, R.id.ll_shipping_address, "field 'll_shipping_address'", LinearLayout.class);
    target.ll_store_address = Utils.findRequiredViewAsType(source, R.id.ll_store_address, "field 'll_store_address'", LinearLayout.class);
    target.shippingLayout = Utils.findRequiredViewAsType(source, R.id.shippingLayout, "field 'shippingLayout'", LinearLayout.class);
    target.storeLayouts = Utils.findRequiredViewAsType(source, R.id.storeLayouts, "field 'storeLayouts'", LinearLayout.class);
    target.taxKey = Utils.findRequiredViewAsType(source, R.id.taxKey, "field 'taxKey'", TextView.class);
    target.tv_addresss = Utils.findRequiredViewAsType(source, R.id.tv_addresss, "field 'tv_addresss'", TextView.class);
    target.tv_citys = Utils.findRequiredViewAsType(source, R.id.tv_citys, "field 'tv_citys'", TextView.class);
    target.tv_countrys = Utils.findRequiredViewAsType(source, R.id.tv_countrys, "field 'tv_countrys'", TextView.class);
    target.label_deliveryAddress = Utils.findRequiredViewAsType(source, R.id.label_deliveryAddress, "field 'label_deliveryAddress'", TextView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ConfirmOrderFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.subTotalTextView = null;
    target.shippingTextView = null;
    target.taxTextView = null;
    target.taxRow = null;
    target.totalAmountTextView = null;
    target.discountTextView = null;
    target.confirmButton = null;
    target.discountTableRow = null;
    target.checkoutProductList = null;
    target.billingAddressLinearLayout = null;
    target.ll_shipping_address = null;
    target.ll_store_address = null;
    target.shippingLayout = null;
    target.storeLayouts = null;
    target.taxKey = null;
    target.tv_addresss = null;
    target.tv_citys = null;
    target.tv_countrys = null;
    target.label_deliveryAddress = null;
    target.progressBar = null;
  }
}
