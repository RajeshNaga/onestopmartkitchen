// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RecentViewAllFragment_ViewBinding implements Unbinder {
  private RecentViewAllFragment target;

  @UiThread
  public RecentViewAllFragment_ViewBinding(RecentViewAllFragment target, View source) {
    this.target = target;

    target.imgBtnViewtype = Utils.findRequiredViewAsType(source, R.id.imgBtn_viewtype, "field 'imgBtnViewtype'", ImageButton.class);
    target.gridView = Utils.findRequiredViewAsType(source, R.id.grid_view, "field 'gridView'", ImageView.class);
    target.gridViewBorder = Utils.findRequiredView(source, R.id.grid_view_border, "field 'gridViewBorder'");
    target.tvPopularity = Utils.findRequiredViewAsType(source, R.id.tv_popularity, "field 'tvPopularity'", TextView.class);
    target.image1 = Utils.findRequiredViewAsType(source, R.id.image1, "field 'image1'", ImageView.class);
    target.rlSortby = Utils.findRequiredViewAsType(source, R.id.rl_sortby, "field 'rlSortby'", RelativeLayout.class);
    target.tvFilter = Utils.findRequiredViewAsType(source, R.id.tv_filter, "field 'tvFilter'", TextView.class);
    target.image2 = Utils.findRequiredViewAsType(source, R.id.image2, "field 'image2'", ImageView.class);
    target.rlFilter = Utils.findRequiredViewAsType(source, R.id.rl_filter, "field 'rlFilter'", RelativeLayout.class);
    target.tvCategoryName = Utils.findRequiredViewAsType(source, R.id.tv_category_name, "field 'tvCategoryName'", TextView.class);
    target.txtTotal = Utils.findRequiredViewAsType(source, R.id.txt_total, "field 'txtTotal'", TextView.class);
    target.rvViewAll = Utils.findRequiredViewAsType(source, R.id.rv_view_all, "field 'rvViewAll'", RecyclerView.class);
    target.txtNorecord = Utils.findRequiredViewAsType(source, R.id.txt_norecord, "field 'txtNorecord'", TextView.class);
    target.bottomsheet = Utils.findRequiredViewAsType(source, R.id.bottomsheet, "field 'bottomsheet'", BottomSheetLayout.class);
    target.rel = Utils.findRequiredViewAsType(source, R.id.rel, "field 'rel'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RecentViewAllFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgBtnViewtype = null;
    target.gridView = null;
    target.gridViewBorder = null;
    target.tvPopularity = null;
    target.image1 = null;
    target.rlSortby = null;
    target.tvFilter = null;
    target.image2 = null;
    target.rlFilter = null;
    target.tvCategoryName = null;
    target.txtTotal = null;
    target.rvViewAll = null;
    target.txtNorecord = null;
    target.bottomsheet = null;
    target.rel = null;
  }
}
