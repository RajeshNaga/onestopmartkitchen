// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BarCodeCaptureFragment_ViewBinding implements Unbinder {
  private BarCodeCaptureFragment target;

  @UiThread
  public BarCodeCaptureFragment_ViewBinding(BarCodeCaptureFragment target, View source) {
    this.target = target;

    target.autoFocus = Utils.findRequiredViewAsType(source, R.id.cb_auto_focus, "field 'autoFocus'", CompoundButton.class);
    target.useFlash = Utils.findRequiredViewAsType(source, R.id.cb_use_flash, "field 'useFlash'", CompoundButton.class);
    target.barcodeValue = Utils.findRequiredViewAsType(source, R.id.tv_barcode_value, "field 'barcodeValue'", TextView.class);
    target.statusMessage = Utils.findRequiredViewAsType(source, R.id.tv_status_message, "field 'statusMessage'", TextView.class);
    target.readBarcodeBtn = Utils.findRequiredViewAsType(source, R.id.read_barcode, "field 'readBarcodeBtn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BarCodeCaptureFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.autoFocus = null;
    target.useFlash = null;
    target.barcodeValue = null;
    target.statusMessage = null;
    target.readBarcodeBtn = null;
  }
}
