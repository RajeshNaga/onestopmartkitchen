// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.florescu.android.rangeseekbar.RangeSeekBar;

public class FilterFragment_ViewBinding implements Unbinder {
  private FilterFragment target;

  @UiThread
  public FilterFragment_ViewBinding(FilterFragment target, View source) {
    this.target = target;

    target.rootLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_rootLayout, "field 'rootLinearLayout'", LinearLayout.class);
    target.maxPriceTextView = Utils.findRequiredViewAsType(source, R.id.tv_max_price, "field 'maxPriceTextView'", TextView.class);
    target.minPriceTextView = Utils.findRequiredViewAsType(source, R.id.tv_min_price, "field 'minPriceTextView'", TextView.class);
    target.priceRangeSeekBar = Utils.findRequiredViewAsType(source, R.id.rangeSeekbar_price, "field 'priceRangeSeekBar'", RangeSeekBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rootLinearLayout = null;
    target.maxPriceTextView = null;
    target.minPriceTextView = null;
    target.priceRangeSeekBar = null;
  }
}
