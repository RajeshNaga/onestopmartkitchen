// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filterupdatedui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoriesListAdapter$ViewHolder_ViewBinding implements Unbinder {
  private CategoriesListAdapter.ViewHolder target;

  private View view7f0a0251;

  @UiThread
  public CategoriesListAdapter$ViewHolder_ViewBinding(final CategoriesListAdapter.ViewHolder target,
      View source) {
    this.target = target;

    View view;
    target.imgTick = Utils.findRequiredViewAsType(source, R.id.img_tick, "field 'imgTick'", ImageView.class);
    target.txtSubTitle = Utils.findRequiredViewAsType(source, R.id.txt_sub_title, "field 'txtSubTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.lin_full, "field 'linFull' and method 'onViewClicked'");
    target.linFull = Utils.castView(view, R.id.lin_full, "field 'linFull'", LinearLayout.class);
    view7f0a0251 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CategoriesListAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgTick = null;
    target.txtSubTitle = null;
    target.linFull = null;

    view7f0a0251.setOnClickListener(null);
    view7f0a0251 = null;
  }
}
