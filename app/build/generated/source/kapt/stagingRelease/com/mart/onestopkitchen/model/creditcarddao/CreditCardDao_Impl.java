package com.mart.onestopkitchen.model.creditcarddao;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;

@SuppressWarnings({"unchecked", "deprecation"})
public final class CreditCardDao_Impl implements CreditCardDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<CardDetailModel> __insertionAdapterOfCardDetailModel;

  private final EntityDeletionOrUpdateAdapter<CardDetailModel> __updateAdapterOfCardDetailModel;

  public CreditCardDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfCardDetailModel = new EntityInsertionAdapter<CardDetailModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `cardDetailTable` (`userId`,`cardHolderName`,`cardNumber`,`cardCvv`,`cardExpMonth`,`cardExpYear`) VALUES (?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CardDetailModel value) {
        if (value.getUserId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getUserId());
        }
        if (value.getCardHolderName() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCardHolderName());
        }
        if (value.getCardNumber() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getCardNumber());
        }
        if (value.getCardCvv() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCardCvv());
        }
        if (value.getCardExpMonth() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getCardExpMonth());
        }
        if (value.getCardExpYear() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getCardExpYear());
        }
      }
    };
    this.__updateAdapterOfCardDetailModel = new EntityDeletionOrUpdateAdapter<CardDetailModel>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `cardDetailTable` SET `userId` = ?,`cardHolderName` = ?,`cardNumber` = ?,`cardCvv` = ?,`cardExpMonth` = ?,`cardExpYear` = ? WHERE `userId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CardDetailModel value) {
        if (value.getUserId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getUserId());
        }
        if (value.getCardHolderName() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCardHolderName());
        }
        if (value.getCardNumber() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getCardNumber());
        }
        if (value.getCardCvv() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCardCvv());
        }
        if (value.getCardExpMonth() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getCardExpMonth());
        }
        if (value.getCardExpYear() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getCardExpYear());
        }
        if (value.getUserId() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindLong(7, value.getUserId());
        }
      }
    };
  }

  @Override
  public void insertCardDetails(final CardDetailModel card) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfCardDetailModel.insert(card);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int updateCardDetails(final CardDetailModel card) {
    __db.assertNotSuspendingTransaction();
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__updateAdapterOfCardDetailModel.handle(card);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public CardDetailModel getCardDetails() {
    final String _sql = "Select * From cardDetailTable order by userId desc Limit 1 ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfUserId = CursorUtil.getColumnIndexOrThrow(_cursor, "userId");
      final int _cursorIndexOfCardHolderName = CursorUtil.getColumnIndexOrThrow(_cursor, "cardHolderName");
      final int _cursorIndexOfCardNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "cardNumber");
      final int _cursorIndexOfCardCvv = CursorUtil.getColumnIndexOrThrow(_cursor, "cardCvv");
      final int _cursorIndexOfCardExpMonth = CursorUtil.getColumnIndexOrThrow(_cursor, "cardExpMonth");
      final int _cursorIndexOfCardExpYear = CursorUtil.getColumnIndexOrThrow(_cursor, "cardExpYear");
      final CardDetailModel _result;
      if(_cursor.moveToFirst()) {
        _result = new CardDetailModel();
        final Integer _tmpUserId;
        if (_cursor.isNull(_cursorIndexOfUserId)) {
          _tmpUserId = null;
        } else {
          _tmpUserId = _cursor.getInt(_cursorIndexOfUserId);
        }
        _result.setUserId(_tmpUserId);
        final String _tmpCardHolderName;
        _tmpCardHolderName = _cursor.getString(_cursorIndexOfCardHolderName);
        _result.setCardHolderName(_tmpCardHolderName);
        final String _tmpCardNumber;
        _tmpCardNumber = _cursor.getString(_cursorIndexOfCardNumber);
        _result.setCardNumber(_tmpCardNumber);
        final String _tmpCardCvv;
        _tmpCardCvv = _cursor.getString(_cursorIndexOfCardCvv);
        _result.setCardCvv(_tmpCardCvv);
        final String _tmpCardExpMonth;
        _tmpCardExpMonth = _cursor.getString(_cursorIndexOfCardExpMonth);
        _result.setCardExpMonth(_tmpCardExpMonth);
        final String _tmpCardExpYear;
        _tmpCardExpYear = _cursor.getString(_cursorIndexOfCardExpYear);
        _result.setCardExpYear(_tmpCardExpYear);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
