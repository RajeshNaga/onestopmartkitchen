// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WishListFragment_ViewBinding implements Unbinder {
  private WishListFragment target;

  @UiThread
  public WishListFragment_ViewBinding(WishListFragment target, View source) {
    this.target = target;

    target.addAllItemsToCartBtn = Utils.findRequiredViewAsType(source, R.id.btn_add_all_items_to_cart, "field 'addAllItemsToCartBtn'", Button.class);
    target.wishRecyclerList = Utils.findRequiredViewAsType(source, R.id.rclv_wish_list, "field 'wishRecyclerList'", RecyclerView.class);
    target.noItemLayoput = Utils.findRequiredViewAsType(source, R.id.ll_noWishListItemLayout, "field 'noItemLayoput'", LinearLayout.class);
    target.continueShopping = Utils.findRequiredViewAsType(source, R.id.btn_continue_shopping, "field 'continueShopping'", Button.class);
    target.shimmerFrameLayout = Utils.findRequiredViewAsType(source, R.id.shimmerLayout, "field 'shimmerFrameLayout'", ShimmerFrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WishListFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.addAllItemsToCartBtn = null;
    target.wishRecyclerList = null;
    target.noItemLayoput = null;
    target.continueShopping = null;
    target.shimmerFrameLayout = null;
  }
}
