package com.mart.onestopkitchen.databinding;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutOrderGroupReceiptPageBindingImpl extends LayoutOrderGroupReceiptPageBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutOrderGroupReceiptPageBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private LayoutOrderGroupReceiptPageBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvOrder.setTag(null);
        this.tvOrderNumber.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.position == variableId) {
            setPosition((java.lang.Integer) variable);
        }
        else if (BR.orderModel == variableId) {
            setOrderModel((com.mart.onestopkitchen.model.OrdersModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPosition(@Nullable java.lang.Integer Position) {
        this.mPosition = Position;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.position);
        super.requestRebind();
    }
    public void setOrderModel(@Nullable com.mart.onestopkitchen.model.OrdersModel OrderModel) {
        this.mOrderModel = OrderModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.orderModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int positionInt1 = 0;
        java.lang.String javaLangStringOrderModelCustomOrderNumber = null;
        java.lang.String orderModelCustomOrderNumber = null;
        int androidxDatabindingViewDataBindingSafeUnboxPosition = 0;
        java.lang.Integer position = mPosition;
        com.mart.onestopkitchen.model.OrdersModel orderModel = mOrderModel;
        java.lang.String tvOrderAndroidStringOrderNumberPositionInt1 = null;

        if ((dirtyFlags & 0x5L) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(position)
                androidxDatabindingViewDataBindingSafeUnboxPosition = androidx.databinding.ViewDataBinding.safeUnbox(position);


                // read (androidx.databinding.ViewDataBinding.safeUnbox(position)) + (1)
                positionInt1 = (androidxDatabindingViewDataBindingSafeUnboxPosition) + (1);


                // read (@android:string/order_number) + ((androidx.databinding.ViewDataBinding.safeUnbox(position)) + (1))
                tvOrderAndroidStringOrderNumberPositionInt1 = (tvOrder.getResources().getString(R.string.order_number)) + (positionInt1);
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (orderModel != null) {
                    // read orderModel.customOrderNumber
                    orderModelCustomOrderNumber = orderModel.getCustomOrderNumber();
                }


                // read ("#") + (orderModel.customOrderNumber)
                javaLangStringOrderModelCustomOrderNumber = ("#") + (orderModelCustomOrderNumber);
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvOrder, tvOrderAndroidStringOrderNumberPositionInt1);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvOrderNumber, javaLangStringOrderModelCustomOrderNumber);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): position
        flag 1 (0x2L): orderModel
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}