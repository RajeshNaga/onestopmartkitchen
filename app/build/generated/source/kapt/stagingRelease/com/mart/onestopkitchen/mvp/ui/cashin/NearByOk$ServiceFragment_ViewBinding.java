// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.cashin;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NearByOk$ServiceFragment_ViewBinding implements Unbinder {
  private NearByOk$ServiceFragment target;

  private View view7f0a0197;

  @UiThread
  public NearByOk$ServiceFragment_ViewBinding(final NearByOk$ServiceFragment target, View source) {
    this.target = target;

    View view;
    target.relAgentList = Utils.findRequiredViewAsType(source, R.id.rel_agent_list, "field 'relAgentList'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.fab_btn, "field 'fabBtn' and method 'onViewClicked'");
    target.fabBtn = Utils.castView(view, R.id.fab_btn, "field 'fabBtn'", FloatingActionButton.class);
    view7f0a0197 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.txtNoRecord = Utils.findRequiredViewAsType(source, R.id.txtNoRecord, "field 'txtNoRecord'", TextView.class);
    target.drawerFilter = Utils.findRequiredViewAsType(source, R.id.drawer_filter, "field 'drawerFilter'", DrawerLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    NearByOk$ServiceFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.relAgentList = null;
    target.fabBtn = null;
    target.txtNoRecord = null;
    target.drawerFilter = null;

    view7f0a0197.setOnClickListener(null);
    view7f0a0197 = null;
  }
}
