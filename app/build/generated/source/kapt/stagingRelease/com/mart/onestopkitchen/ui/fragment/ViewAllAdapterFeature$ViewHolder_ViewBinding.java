// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewAllAdapterFeature$ViewHolder_ViewBinding implements Unbinder {
  private ViewAllAdapterFeature.ViewHolder target;

  private View view7f0a025c;

  @UiThread
  public ViewAllAdapterFeature$ViewHolder_ViewBinding(final ViewAllAdapterFeature.ViewHolder target,
      View source) {
    this.target = target;

    View view;
    target.fav = Utils.findRequiredViewAsType(source, R.id.fav, "field 'fav'", CheckBox.class);
    target.imgProductImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'imgProductImage'", ImageView.class);
    target.tvProductName = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'tvProductName'", TextView.class);
    view = Utils.findRequiredView(source, R.id.lin_root, "field 'linRoot' and method 'onViewClicked'");
    target.linRoot = Utils.castView(view, R.id.lin_root, "field 'linRoot'", LinearLayout.class);
    view7f0a025c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewAllAdapterFeature.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.fav = null;
    target.imgProductImage = null;
    target.tvProductName = null;
    target.linRoot = null;

    view7f0a025c.setOnClickListener(null);
    view7f0a025c = null;
  }
}
