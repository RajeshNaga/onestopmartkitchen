// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.submenu.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubMenuItemAdapter$ViewHolder_ViewBinding implements Unbinder {
  private SubMenuItemAdapter.ViewHolder target;

  @UiThread
  public SubMenuItemAdapter$ViewHolder_ViewBinding(SubMenuItemAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.txtTitle = Utils.findRequiredViewAsType(source, R.id.txt_title, "field 'txtTitle'", TextView.class);
    target.imgDrop = Utils.findRequiredViewAsType(source, R.id.img_drop, "field 'imgDrop'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SubMenuItemAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTitle = null;
    target.imgDrop = null;
  }
}
