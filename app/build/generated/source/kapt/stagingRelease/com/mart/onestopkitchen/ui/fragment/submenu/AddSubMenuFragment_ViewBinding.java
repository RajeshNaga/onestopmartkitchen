// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.submenu;

import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddSubMenuFragment_ViewBinding implements Unbinder {
  private AddSubMenuFragment target;

  @UiThread
  public AddSubMenuFragment_ViewBinding(AddSubMenuFragment target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddSubMenuFragment_ViewBinding(AddSubMenuFragment target, View source) {
    this.target = target;

    target.containerNew = Utils.findRequiredViewAsType(source, R.id.container_new, "field 'containerNew'", FrameLayout.class);
    target.rvSelectdList = Utils.findRequiredViewAsType(source, R.id.rv_selectd_list, "field 'rvSelectdList'", RecyclerView.class);
    target.toolBar = Utils.findRequiredViewAsType(source, R.id.tool_bar, "field 'toolBar'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddSubMenuFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.containerNew = null;
    target.rvSelectdList = null;
    target.toolBar = null;
  }
}
