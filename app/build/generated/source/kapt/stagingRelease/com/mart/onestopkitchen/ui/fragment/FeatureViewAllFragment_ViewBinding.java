// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FeatureViewAllFragment_ViewBinding implements Unbinder {
  private FeatureViewAllFragment target;

  private View view7f0a03a1;

  private View view7f0a039b;

  private View view7f0a01bf;

  @UiThread
  public FeatureViewAllFragment_ViewBinding(final FeatureViewAllFragment target, View source) {
    this.target = target;

    View view;
    target.bottomSheetLayout = Utils.findRequiredViewAsType(source, R.id.bottomsheet, "field 'bottomSheetLayout'", BottomSheetLayout.class);
    target.rel = Utils.findRequiredViewAsType(source, R.id.rel, "field 'rel'", RelativeLayout.class);
    target.rvViewAll = Utils.findRequiredViewAsType(source, R.id.rv_view_all, "field 'rvViewAll'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.rl_sortby, "field 'rl_sortby' and method 'showSortByView'");
    target.rl_sortby = Utils.castView(view, R.id.rl_sortby, "field 'rl_sortby'", RelativeLayout.class);
    view7f0a03a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.rl_filter, "field 'rl_filter' and method 'showSortByView'");
    target.rl_filter = Utils.castView(view, R.id.rl_filter, "field 'rl_filter'", RelativeLayout.class);
    view7f0a039b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
    target.txt_norecord = Utils.findRequiredViewAsType(source, R.id.txt_norecord, "field 'txt_norecord'", TextView.class);
    view = Utils.findRequiredView(source, R.id.grid_view, "field 'gridView' and method 'showSortByView'");
    target.gridView = Utils.castView(view, R.id.grid_view, "field 'gridView'", ImageView.class);
    view7f0a01bf = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showSortByView(p0);
      }
    });
    target.gridViewBorder = Utils.findRequiredView(source, R.id.grid_view_border, "field 'gridViewBorder'");
  }

  @Override
  @CallSuper
  public void unbind() {
    FeatureViewAllFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.bottomSheetLayout = null;
    target.rel = null;
    target.rvViewAll = null;
    target.rl_sortby = null;
    target.rl_filter = null;
    target.txt_norecord = null;
    target.gridView = null;
    target.gridViewBorder = null;

    view7f0a03a1.setOnClickListener(null);
    view7f0a03a1 = null;
    view7f0a039b.setOnClickListener(null);
    view7f0a039b = null;
    view7f0a01bf.setOnClickListener(null);
    view7f0a01bf = null;
  }
}
