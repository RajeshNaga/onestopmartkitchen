// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewAllAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ViewAllAdapter.ViewHolder target;

  private View view7f0a025c;

  @UiThread
  public ViewAllAdapter$ViewHolder_ViewBinding(final ViewAllAdapter.ViewHolder target,
      View source) {
    this.target = target;

    View view;
    target.imgProductImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'imgProductImage'", ImageView.class);
    target.tvProductName = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'tvProductName'", TextView.class);
    target.tvProductPrice = Utils.findRequiredViewAsType(source, R.id.tv_productPrice, "field 'tvProductPrice'", TextView.class);
    target.tvProductOldPrice = Utils.findRequiredViewAsType(source, R.id.tv_productOldPrice, "field 'tvProductOldPrice'", TextView.class);
    target.buttonFavoriteCategory = Utils.findRequiredViewAsType(source, R.id.button_favorite_category, "field 'buttonFavoriteCategory'", ToggleButton.class);
    target.tvOfferPercentage = Utils.findRequiredViewAsType(source, R.id.tv_offerPercentage, "field 'tvOfferPercentage'", TextView.class);
    view = Utils.findRequiredView(source, R.id.lin_root, "method 'onViewClicked'");
    view7f0a025c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewAllAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgProductImage = null;
    target.tvProductName = null;
    target.tvProductPrice = null;
    target.tvProductOldPrice = null;
    target.buttonFavoriteCategory = null;
    target.tvOfferPercentage = null;

    view7f0a025c.setOnClickListener(null);
    view7f0a025c = null;
  }
}
