// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchFragmentOnOptionMenu_ViewBinding implements Unbinder {
  private SearchFragmentOnOptionMenu target;

  private View view7f0a01f0;

  private View view7f0a01f2;

  private View view7f0a01bf;

  private View view7f0a03a1;

  private View view7f0a039b;

  private View view7f0a0201;

  @UiThread
  public SearchFragmentOnOptionMenu_ViewBinding(final SearchFragmentOnOptionMenu target,
      View source) {
    this.target = target;

    View view;
    target.productListRecyclerView = Utils.findRequiredViewAsType(source, R.id.rv_product_list, "field 'productListRecyclerView'", RecyclerView.class);
    target.abbBar = Utils.findRequiredViewAsType(source, R.id.abbBar, "field 'abbBar'", RelativeLayout.class);
    target.txtHide = Utils.findRequiredViewAsType(source, R.id.txt_hide, "field 'txtHide'", TextView.class);
    target.txtNoRecord = Utils.findRequiredViewAsType(source, R.id.txt_no_record, "field 'txtNoRecord'", TextView.class);
    view = Utils.findRequiredView(source, R.id.img_back, "field 'imgBack' and method 'onViewClicked'");
    target.imgBack = Utils.castView(view, R.id.img_back, "field 'imgBack'", ImageView.class);
    view7f0a01f0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.edtSearchText = Utils.findRequiredViewAsType(source, R.id.edt_search_text, "field 'edtSearchText'", EditText.class);
    view = Utils.findRequiredView(source, R.id.img_close, "field 'imgClose' and method 'onViewClicked'");
    target.imgClose = Utils.castView(view, R.id.img_close, "field 'imgClose'", ImageView.class);
    view7f0a01f2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.grid_view, "field 'gridView' and method 'onViewClicked'");
    target.gridView = Utils.castView(view, R.id.grid_view, "field 'gridView'", ImageView.class);
    view7f0a01bf = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.gridViewBorder = Utils.findRequiredView(source, R.id.grid_view_border, "field 'gridViewBorder'");
    target.relRoot = Utils.findRequiredViewAsType(source, R.id.rel_root, "field 'relRoot'", RelativeLayout.class);
    target.imgBtnViewtype = Utils.findRequiredViewAsType(source, R.id.imgBtn_viewtype, "field 'imgBtnViewtype'", ImageButton.class);
    target.tvPopularity = Utils.findRequiredViewAsType(source, R.id.tv_popularity, "field 'tvPopularity'", TextView.class);
    target.image1 = Utils.findRequiredViewAsType(source, R.id.image1, "field 'image1'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.rl_sortby, "field 'rlSortby' and method 'onViewClicked'");
    target.rlSortby = Utils.castView(view, R.id.rl_sortby, "field 'rlSortby'", RelativeLayout.class);
    view7f0a03a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvFilter = Utils.findRequiredViewAsType(source, R.id.tv_filter, "field 'tvFilter'", TextView.class);
    target.image2 = Utils.findRequiredViewAsType(source, R.id.image2, "field 'image2'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.rl_filter, "field 'rlFilter' and method 'onViewClicked'");
    target.rlFilter = Utils.castView(view, R.id.rl_filter, "field 'rlFilter'", RelativeLayout.class);
    view7f0a039b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvCategoryName = Utils.findRequiredViewAsType(source, R.id.tv_category_name, "field 'tvCategoryName'", TextView.class);
    target.bottomSheetLayout = Utils.findRequiredViewAsType(source, R.id.bottomsheet, "field 'bottomSheetLayout'", BottomSheetLayout.class);
    target.sortAndFilter = Utils.findRequiredViewAsType(source, R.id.sub_filter, "field 'sortAndFilter'", LinearLayout.class);
    target.relSearchView = Utils.findRequiredViewAsType(source, R.id.rel_search_view, "field 'relSearchView'", RelativeLayout.class);
    target.txtShowText = Utils.findRequiredViewAsType(source, R.id.txt_show_text, "field 'txtShowText'", TextView.class);
    view = Utils.findRequiredView(source, R.id.img_search, "field 'imgSearch' and method 'onViewClicked'");
    target.imgSearch = Utils.castView(view, R.id.img_search, "field 'imgSearch'", ImageView.class);
    view7f0a0201 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.relSearchShow = Utils.findRequiredViewAsType(source, R.id.rel_search_show, "field 'relSearchShow'", RelativeLayout.class);
    target.linRootRv = Utils.findRequiredViewAsType(source, R.id.lin_root_rv, "field 'linRootRv'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SearchFragmentOnOptionMenu target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.productListRecyclerView = null;
    target.abbBar = null;
    target.txtHide = null;
    target.txtNoRecord = null;
    target.imgBack = null;
    target.edtSearchText = null;
    target.imgClose = null;
    target.gridView = null;
    target.gridViewBorder = null;
    target.relRoot = null;
    target.imgBtnViewtype = null;
    target.tvPopularity = null;
    target.image1 = null;
    target.rlSortby = null;
    target.tvFilter = null;
    target.image2 = null;
    target.rlFilter = null;
    target.tvCategoryName = null;
    target.bottomSheetLayout = null;
    target.sortAndFilter = null;
    target.relSearchView = null;
    target.txtShowText = null;
    target.imgSearch = null;
    target.relSearchShow = null;
    target.linRootRv = null;

    view7f0a01f0.setOnClickListener(null);
    view7f0a01f0 = null;
    view7f0a01f2.setOnClickListener(null);
    view7f0a01f2 = null;
    view7f0a01bf.setOnClickListener(null);
    view7f0a01bf = null;
    view7f0a03a1.setOnClickListener(null);
    view7f0a03a1 = null;
    view7f0a039b.setOnClickListener(null);
    view7f0a039b = null;
    view7f0a0201.setOnClickListener(null);
    view7f0a0201 = null;
  }
}
