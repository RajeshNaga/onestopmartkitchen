// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filteroption.filter;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.jaygoo.widget.RangeSeekBar;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterFragment_ViewBinding implements Unbinder {
  private FilterFragment target;

  private View view7f0a00a0;

  private View view7f0a00a3;

  @UiThread
  public FilterFragment_ViewBinding(final FilterFragment target, View source) {
    this.target = target;

    View view;
    target.rangeSeekBar = Utils.findRequiredViewAsType(source, R.id.range_seek_bar, "field 'rangeSeekBar'", RangeSeekBar.class);
    target.rvCategories = Utils.findRequiredViewAsType(source, R.id.rv_categories, "field 'rvCategories'", RecyclerView.class);
    target.linCateFilter = Utils.findRequiredViewAsType(source, R.id.lin_cate_filter, "field 'linCateFilter'", LinearLayout.class);
    target.txtMinPrice = Utils.findRequiredViewAsType(source, R.id.txt_min_price, "field 'txtMinPrice'", TextView.class);
    target.txtMaxPrice = Utils.findRequiredViewAsType(source, R.id.txt_max_price, "field 'txtMaxPrice'", TextView.class);
    target.linSeekBar = Utils.findRequiredViewAsType(source, R.id.lin_seek_bar, "field 'linSeekBar'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_addtoCart, "field 'btnAddtoCart' and method 'onViewClicked'");
    target.btnAddtoCart = Utils.castView(view, R.id.btn_addtoCart, "field 'btnAddtoCart'", Button.class);
    view7f0a00a0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_buy_now, "field 'btnBuyNow' and method 'onViewClicked'");
    target.btnBuyNow = Utils.castView(view, R.id.btn_buy_now, "field 'btnBuyNow'", Button.class);
    view7f0a00a3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.viewLin = Utils.findRequiredView(source, R.id.view_lin, "field 'viewLin'");
    target.addtoCartLayout = Utils.findRequiredViewAsType(source, R.id.addtoCartLayout, "field 'addtoCartLayout'", LinearLayout.class);
    target.fixedLayout = Utils.findRequiredViewAsType(source, R.id.fixedLayout, "field 'fixedLayout'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rangeSeekBar = null;
    target.rvCategories = null;
    target.linCateFilter = null;
    target.txtMinPrice = null;
    target.txtMaxPrice = null;
    target.linSeekBar = null;
    target.btnAddtoCart = null;
    target.btnBuyNow = null;
    target.viewLin = null;
    target.addtoCartLayout = null;
    target.fixedLayout = null;

    view7f0a00a0.setOnClickListener(null);
    view7f0a00a0 = null;
    view7f0a00a3.setOnClickListener(null);
    view7f0a00a3 = null;
  }
}
