// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterFragment_ViewBinding implements Unbinder {
  private RegisterFragment target;

  @UiThread
  public RegisterFragment_ViewBinding(RegisterFragment target, View source) {
    this.target = target;

    target.nameTitleTextView = Utils.findRequiredViewAsType(source, R.id.tvName, "field 'nameTitleTextView'", TextView.class);
    target.customerNameTextView = Utils.findRequiredViewAsType(source, R.id.customer_name, "field 'customerNameTextView'", TextView.class);
    target.dateOfBirthTextView = Utils.findRequiredViewAsType(source, R.id.dateOfBirth, "field 'dateOfBirthTextView'", TextView.class);
    target.emailTextView = Utils.findRequiredViewAsType(source, R.id.customer_email, "field 'emailTextView'", TextView.class);
    target.genderTextView = Utils.findRequiredViewAsType(source, R.id.gender, "field 'genderTextView'", TextView.class);
    target.customerFirstNameEditText = Utils.findRequiredViewAsType(source, R.id.et_customer_first_name, "field 'customerFirstNameEditText'", EditText.class);
    target.customerLastNameEditText = Utils.findRequiredViewAsType(source, R.id.et_customer_last_name, "field 'customerLastNameEditText'", EditText.class);
    target.emailEditText = Utils.findRequiredViewAsType(source, R.id.et_customer_email, "field 'emailEditText'", EditText.class);
    target.genderMaleRadioButton = Utils.findRequiredViewAsType(source, R.id.rb_male, "field 'genderMaleRadioButton'", RadioButton.class);
    target.genderFemaleRadioButton = Utils.findRequiredViewAsType(source, R.id.rb_female, "field 'genderFemaleRadioButton'", RadioButton.class);
    target.genderRadioGroup = Utils.findRequiredViewAsType(source, R.id.genderRadioGroup, "field 'genderRadioGroup'", RadioGroup.class);
    target.saveBtn = Utils.findRequiredViewAsType(source, R.id.btn_save, "field 'saveBtn'", Button.class);
    target.usernameTextView = Utils.findRequiredViewAsType(source, R.id.customer_username, "field 'usernameTextView'", TextView.class);
    target.usernameEditText = Utils.findRequiredViewAsType(source, R.id.et_customer_username, "field 'usernameEditText'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.nameTitleTextView = null;
    target.customerNameTextView = null;
    target.dateOfBirthTextView = null;
    target.emailTextView = null;
    target.genderTextView = null;
    target.customerFirstNameEditText = null;
    target.customerLastNameEditText = null;
    target.emailEditText = null;
    target.genderMaleRadioButton = null;
    target.genderFemaleRadioButton = null;
    target.genderRadioGroup = null;
    target.saveBtn = null;
    target.usernameTextView = null;
    target.usernameEditText = null;
  }
}
