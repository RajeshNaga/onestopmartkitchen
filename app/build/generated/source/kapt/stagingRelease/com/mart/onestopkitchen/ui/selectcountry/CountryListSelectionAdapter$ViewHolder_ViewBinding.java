// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.selectcountry;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CountryListSelectionAdapter$ViewHolder_ViewBinding implements Unbinder {
  private CountryListSelectionAdapter.ViewHolder target;

  private View view7f0a024a;

  @UiThread
  public CountryListSelectionAdapter$ViewHolder_ViewBinding(
      final CountryListSelectionAdapter.ViewHolder target, View source) {
    this.target = target;

    View view;
    target.imgCounty = Utils.findRequiredViewAsType(source, R.id.img_county, "field 'imgCounty'", ImageView.class);
    target.txtCountyName = Utils.findRequiredViewAsType(source, R.id.txt_county_name, "field 'txtCountyName'", TextView.class);
    view = Utils.findRequiredView(source, R.id.lin_county_select, "field 'linCountySelect' and method 'onViewClicked'");
    target.linCountySelect = Utils.castView(view, R.id.lin_county_select, "field 'linCountySelect'", LinearLayout.class);
    view7f0a024a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CountryListSelectionAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgCounty = null;
    target.txtCountyName = null;
    target.linCountySelect = null;

    view7f0a024a.setOnClickListener(null);
    view7f0a024a = null;
  }
}
