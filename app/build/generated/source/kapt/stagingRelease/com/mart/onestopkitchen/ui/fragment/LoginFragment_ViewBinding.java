// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.utils.CircleImageView;
import com.mart.onestopkitchen.utils.CustomEdittext;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginFragment_ViewBinding implements Unbinder {
  private LoginFragment target;

  private View view7f0a00c3;

  private View view7f0a00af;

  private View view7f0a01f1;

  private View view7f0a0258;

  private View view7f0a0249;

  private View view7f0a01fc;

  private View view7f0a0217;

  private View view7f0a024b;

  private View view7f0a01f4;

  private View view7f0a01ff;

  @UiThread
  public LoginFragment_ViewBinding(final LoginFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.btn_verify, "field 'btnVerify' and method 'onViewClicked'");
    target.btnVerify = Utils.castView(view, R.id.btn_verify, "field 'btnVerify'", Button.class);
    view7f0a00c3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_guest, "field 'btnGuest' and method 'onViewClicked'");
    target.btnGuest = Utils.castView(view, R.id.btn_guest, "field 'btnGuest'", Button.class);
    view7f0a00af = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtCountryCode = Utils.findRequiredViewAsType(source, R.id.txt_country_code, "field 'txtCountryCode'", TextView.class);
    target.edtMobileNumber = Utils.findRequiredViewAsType(source, R.id.edt_mobile_number, "field 'edtMobileNumber'", CustomEdittext.class);
    target.linHeader = Utils.findRequiredViewAsType(source, R.id.linHeader, "field 'linHeader'", LinearLayout.class);
    target.edtEmailId = Utils.findRequiredViewAsType(source, R.id.edt_email_id, "field 'edtEmailId'", EditText.class);
    target.linEmailId = Utils.findRequiredViewAsType(source, R.id.lin_email_id, "field 'linEmailId'", LinearLayout.class);
    target.edtDob = Utils.findRequiredViewAsType(source, R.id.edt_dob, "field 'edtDob'", EditText.class);
    target.relRoot = Utils.findRequiredViewAsType(source, R.id.rel_root, "field 'relRoot'", RelativeLayout.class);
    target.radioMale = Utils.findRequiredViewAsType(source, R.id.radioMale, "field 'radioMale'", RadioButton.class);
    target.radioFemale = Utils.findRequiredViewAsType(source, R.id.radioFemale, "field 'radioFemale'", RadioButton.class);
    target.radioBtnSex = Utils.findRequiredViewAsType(source, R.id.radio_btn_sex, "field 'radioBtnSex'", RadioGroup.class);
    target.linRegisterForm = Utils.findRequiredViewAsType(source, R.id.lin_register_form, "field 'linRegisterForm'", LinearLayout.class);
    target.linOkDollar = Utils.findRequiredViewAsType(source, R.id.lin_ok_dollar, "field 'linOkDollar'", LinearLayout.class);
    target.username = Utils.findRequiredViewAsType(source, R.id.username, "field 'username'", EditText.class);
    target.edtPasword = Utils.findRequiredViewAsType(source, R.id.edt_pasword, "field 'edtPasword'", EditText.class);
    target.edtConfirmPaswordpasword = Utils.findRequiredViewAsType(source, R.id.edt_confirm_paswordpasword, "field 'edtConfirmPaswordpasword'", EditText.class);
    target.ivProfileImage = Utils.findRequiredViewAsType(source, R.id.iv_profile_image, "field 'ivProfileImage'", CircleImageView.class);
    view = Utils.findRequiredView(source, R.id.img_clear, "field 'imgClear' and method 'onViewClicked'");
    target.imgClear = Utils.castView(view, R.id.img_clear, "field 'imgClear'", ImageView.class);
    view7f0a01f1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.lin_profile_img, "field 'linProfileImg' and method 'onViewClicked'");
    target.linProfileImg = Utils.castView(view, R.id.lin_profile_img, "field 'linProfileImg'", RelativeLayout.class);
    view7f0a0258 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.edtProfilePic = Utils.findRequiredViewAsType(source, R.id.edt_profile_pic, "field 'edtProfilePic'", EditText.class);
    target.img_select_country_code_verify = Utils.findRequiredViewAsType(source, R.id.img_select_country_code_verify, "field 'img_select_country_code_verify'", ImageView.class);
    target.linProfilePic = Utils.findRequiredViewAsType(source, R.id.lin_profile_pic, "field 'linProfilePic'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.lin_country_selection, "field 'linCountrySelection' and method 'onViewClicked'");
    target.linCountrySelection = Utils.castView(view, R.id.lin_country_selection, "field 'linCountrySelection'", LinearLayout.class);
    view7f0a0249 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_ok_dollar_login, "method 'onViewClicked'");
    view7f0a01fc = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_camera, "method 'onViewClicked'");
    view7f0a0217 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.lin_dob_select, "method 'onViewClicked'");
    view7f0a024b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_date_picker, "method 'onViewClicked'");
    view7f0a01f4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_profile_select, "method 'onViewClicked'");
    view7f0a01ff = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnVerify = null;
    target.btnGuest = null;
    target.txtCountryCode = null;
    target.edtMobileNumber = null;
    target.linHeader = null;
    target.edtEmailId = null;
    target.linEmailId = null;
    target.edtDob = null;
    target.relRoot = null;
    target.radioMale = null;
    target.radioFemale = null;
    target.radioBtnSex = null;
    target.linRegisterForm = null;
    target.linOkDollar = null;
    target.username = null;
    target.edtPasword = null;
    target.edtConfirmPaswordpasword = null;
    target.ivProfileImage = null;
    target.imgClear = null;
    target.linProfileImg = null;
    target.edtProfilePic = null;
    target.img_select_country_code_verify = null;
    target.linProfilePic = null;
    target.linCountrySelection = null;

    view7f0a00c3.setOnClickListener(null);
    view7f0a00c3 = null;
    view7f0a00af.setOnClickListener(null);
    view7f0a00af = null;
    view7f0a01f1.setOnClickListener(null);
    view7f0a01f1 = null;
    view7f0a0258.setOnClickListener(null);
    view7f0a0258 = null;
    view7f0a0249.setOnClickListener(null);
    view7f0a0249 = null;
    view7f0a01fc.setOnClickListener(null);
    view7f0a01fc = null;
    view7f0a0217.setOnClickListener(null);
    view7f0a0217 = null;
    view7f0a024b.setOnClickListener(null);
    view7f0a024b = null;
    view7f0a01f4.setOnClickListener(null);
    view7f0a01f4 = null;
    view7f0a01ff.setOnClickListener(null);
    view7f0a01ff = null;
  }
}
