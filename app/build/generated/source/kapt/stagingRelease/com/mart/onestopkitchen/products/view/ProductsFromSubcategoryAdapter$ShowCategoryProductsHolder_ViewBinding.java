// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.products.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductsFromSubcategoryAdapter$ShowCategoryProductsHolder_ViewBinding implements Unbinder {
  private ProductsFromSubcategoryAdapter.ShowCategoryProductsHolder target;

  @UiThread
  public ProductsFromSubcategoryAdapter$ShowCategoryProductsHolder_ViewBinding(
      ProductsFromSubcategoryAdapter.ShowCategoryProductsHolder target, View source) {
    this.target = target;

    target.pImage = Utils.findRequiredViewAsType(source, R.id.iv_productImage, "field 'pImage'", ImageView.class);
    target.pName = Utils.findRequiredViewAsType(source, R.id.tv_products_name, "field 'pName'", TextView.class);
    target.productPrice = Utils.findRequiredViewAsType(source, R.id.tv_product_Price, "field 'productPrice'", TextView.class);
    target.productOldPrice = Utils.findRequiredViewAsType(source, R.id.tv_product_OldPrice, "field 'productOldPrice'", TextView.class);
    target.tvDiscountPercentage = Utils.findRequiredViewAsType(source, R.id.tv_discount_percentage, "field 'tvDiscountPercentage'", TextView.class);
    target.wishList = Utils.findRequiredViewAsType(source, R.id.button_favorite_category, "field 'wishList'", ToggleButton.class);
    target.gif_mark_new = Utils.findRequiredViewAsType(source, R.id.gif_mark_new, "field 'gif_mark_new'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ProductsFromSubcategoryAdapter.ShowCategoryProductsHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.pImage = null;
    target.pName = null;
    target.productPrice = null;
    target.productOldPrice = null;
    target.tvDiscountPercentage = null;
    target.wishList = null;
    target.gif_mark_new = null;
  }
}
