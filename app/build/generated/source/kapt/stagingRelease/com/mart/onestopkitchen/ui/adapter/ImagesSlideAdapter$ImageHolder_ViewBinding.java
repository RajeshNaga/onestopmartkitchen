// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ImagesSlideAdapter$ImageHolder_ViewBinding implements Unbinder {
  private ImagesSlideAdapter.ImageHolder target;

  @UiThread
  public ImagesSlideAdapter$ImageHolder_ViewBinding(ImagesSlideAdapter.ImageHolder target,
      View source) {
    this.target = target;

    target.image = Utils.findRequiredViewAsType(source, R.id.image, "field 'image'", ImageView.class);
    target.grayMask = Utils.findRequiredViewAsType(source, R.id.iv_mask, "field 'grayMask'", LinearLayout.class);
    target.count = Utils.findRequiredViewAsType(source, R.id.tv_count, "field 'count'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ImagesSlideAdapter.ImageHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.image = null;
    target.grayMask = null;
    target.count = null;
  }
}
