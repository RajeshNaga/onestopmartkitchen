// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoryListAdapter$ProductSummaryHolder_ViewBinding implements Unbinder {
  private CategoryListAdapter.ProductSummaryHolder target;

  @UiThread
  public CategoryListAdapter$ProductSummaryHolder_ViewBinding(
      CategoryListAdapter.ProductSummaryHolder target, View source) {
    this.target = target;

    target.linLine = Utils.findRequiredViewAsType(source, R.id.lin_line, "field 'linLine'", LinearLayout.class);
    target.subName = Utils.findRequiredViewAsType(source, R.id.subName, "field 'subName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CategoryListAdapter.ProductSummaryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.linLine = null;
    target.subName = null;
  }
}
