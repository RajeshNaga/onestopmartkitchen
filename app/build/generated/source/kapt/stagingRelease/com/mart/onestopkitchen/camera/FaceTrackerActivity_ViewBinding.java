// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.camera;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public final class FaceTrackerActivity_ViewBinding implements Unbinder {
  private FaceTrackerActivity target;

  @UiThread
  public FaceTrackerActivity_ViewBinding(FaceTrackerActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FaceTrackerActivity_ViewBinding(FaceTrackerActivity target, View source) {
    this.target = target;

    target.toolbar1 = Utils.findRequiredViewAsType(source, R.id.toolbar1, "field 'toolbar1'", Toolbar.class);
    target.topLayout = Utils.findRequiredViewAsType(source, R.id.topLayout, "field 'topLayout'", RelativeLayout.class);
    target.capture = Utils.findRequiredViewAsType(source, R.id.capture, "field 'capture'", TextView.class);
    target.imgPreview = Utils.findRequiredViewAsType(source, R.id.img_preview, "field 'imgPreview'", ImageView.class);
    target.linearLayout2 = Utils.findRequiredViewAsType(source, R.id.linearLayout2, "field 'linearLayout2'", LinearLayout.class);
  }

  @Override
  public void unbind() {
    FaceTrackerActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar1 = null;
    target.topLayout = null;
    target.capture = null;
    target.imgPreview = null;
    target.linearLayout2 = null;
  }
}
