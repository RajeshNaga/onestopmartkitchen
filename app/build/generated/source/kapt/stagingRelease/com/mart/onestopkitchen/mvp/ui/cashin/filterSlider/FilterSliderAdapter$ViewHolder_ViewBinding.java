// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.cashin.filterSlider;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterSliderAdapter$ViewHolder_ViewBinding implements Unbinder {
  private FilterSliderAdapter.ViewHolder target;

  @UiThread
  public FilterSliderAdapter$ViewHolder_ViewBinding(FilterSliderAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.txtCheckTitle = Utils.findRequiredViewAsType(source, R.id.txt_check_title, "field 'txtCheckTitle'", TextView.class);
    target.checkboxSlider = Utils.findRequiredViewAsType(source, R.id.checkbox_slider, "field 'checkboxSlider'", CheckBox.class);
    target.relCheckBox = Utils.findRequiredViewAsType(source, R.id.rel_check_box, "field 'relCheckBox'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterSliderAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtCheckTitle = null;
    target.checkboxSlider = null;
    target.relCheckBox = null;
  }
}
