// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddressAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private AddressAdapter.MyViewHolder target;

  @UiThread
  public AddressAdapter$MyViewHolder_ViewBinding(AddressAdapter.MyViewHolder target, View source) {
    this.target = target;

    target.tvUsername = Utils.findRequiredViewAsType(source, R.id.tv_username, "field 'tvUsername'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.tv_address, "field 'tvAddress'", TextView.class);
    target.tvMobile = Utils.findRequiredViewAsType(source, R.id.tv_mobile_number, "field 'tvMobile'", TextView.class);
    target.ll_address_item = Utils.findRequiredViewAsType(source, R.id.ll_address_item, "field 'll_address_item'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddressAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvUsername = null;
    target.tvAddress = null;
    target.tvMobile = null;
    target.ll_address_item = null;
  }
}
