// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoryNewFragment_ViewBinding implements Unbinder {
  private CategoryNewFragment target;

  @UiThread
  public CategoryNewFragment_ViewBinding(CategoryNewFragment target, View source) {
    this.target = target;

    target.RootViewLinearLayout = Utils.findRequiredViewAsType(source, R.id.ll_rootLayout, "field 'RootViewLinearLayout'", RelativeLayout.class);
    target.refreshLayout = Utils.findRequiredViewAsType(source, R.id.swipe_container, "field 'refreshLayout'", SwipeRefreshLayout.class);
    target.expandableRecylerview = Utils.findRequiredViewAsType(source, R.id.expandList, "field 'expandableRecylerview'", RecyclerView.class);
    target.customerInfo = Utils.findRequiredViewAsType(source, R.id.customer_info, "field 'customerInfo'", RecyclerView.class);
    target.txtUserName = Utils.findRequiredViewAsType(source, R.id.txt_user_name, "field 'txtUserName'", TextView.class);
    target.tvAppVersionName = Utils.findRequiredViewAsType(source, R.id.tvAppVersion, "field 'tvAppVersionName'", TextView.class);
    target.imgLogo = Utils.findRequiredViewAsType(source, R.id.imgLogo, "field 'imgLogo'", ImageView.class);
    target.tvHome = Utils.findRequiredViewAsType(source, R.id.tvHome, "field 'tvHome'", TextView.class);
    target.txtEngEel = Utils.findRequiredViewAsType(source, R.id.txt_eng_rel, "field 'txtEngEel'", RelativeLayout.class);
    target.txtBurmRel = Utils.findRequiredViewAsType(source, R.id.txt_burm_rel, "field 'txtBurmRel'", RelativeLayout.class);
    target.language = Utils.findRequiredViewAsType(source, R.id.lin_language, "field 'language'", LinearLayout.class);
    target.tvEng = Utils.findRequiredViewAsType(source, R.id.tv_en_lan, "field 'tvEng'", TextView.class);
    target.tvBu = Utils.findRequiredViewAsType(source, R.id.tv_bu_lan, "field 'tvBu'", TextView.class);
    target.txt_choose_lang = Utils.findRequiredViewAsType(source, R.id.txt_choose_lang, "field 'txt_choose_lang'", TextView.class);
    target.txt_choose_bur = Utils.findRequiredViewAsType(source, R.id.txt_choose_bur, "field 'txt_choose_bur'", TextView.class);
    target.lin_change_lang = Utils.findRequiredViewAsType(source, R.id.lin_change_lang, "field 'lin_change_lang'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CategoryNewFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.RootViewLinearLayout = null;
    target.refreshLayout = null;
    target.expandableRecylerview = null;
    target.customerInfo = null;
    target.txtUserName = null;
    target.tvAppVersionName = null;
    target.imgLogo = null;
    target.tvHome = null;
    target.txtEngEel = null;
    target.txtBurmRel = null;
    target.language = null;
    target.tvEng = null;
    target.tvBu = null;
    target.txt_choose_lang = null;
    target.txt_choose_bur = null;
    target.lin_change_lang = null;
  }
}
