// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.daimajia.slider.library.SliderLayout;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerOrderAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private CustomerOrderAdapter.MyViewHolder target;

  @UiThread
  public CustomerOrderAdapter$MyViewHolder_ViewBinding(CustomerOrderAdapter.MyViewHolder target,
      View source) {
    this.target = target;

    target.row = Utils.findRequiredViewAsType(source, R.id.row, "field 'row'", LinearLayout.class);
    target.productImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'productImage'", ImageView.class);
    target.tv_moretext = Utils.findRequiredViewAsType(source, R.id.tv_moretext, "field 'tv_moretext'", TextView.class);
    target.tv_product_acturl_price = Utils.findRequiredViewAsType(source, R.id.tv_product_acturl_price, "field 'tv_product_acturl_price'", TextView.class);
    target.order_processing = Utils.findRequiredViewAsType(source, R.id.order_processing, "field 'order_processing'", TextView.class);
    target.product_Name = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'product_Name'", TextView.class);
    target.tv_quantity = Utils.findRequiredViewAsType(source, R.id.tv_quantity, "field 'tv_quantity'", TextView.class);
    target.tvquantity = Utils.findRequiredViewAsType(source, R.id.tvquantity, "field 'tvquantity'", TextView.class);
    target.tv_totalPrice = Utils.findRequiredViewAsType(source, R.id.tv_totalPrice, "field 'tv_totalPrice'", TextView.class);
    target.tv_cutomer_order_number = Utils.findRequiredViewAsType(source, R.id.tv_cutomer_order_number, "field 'tv_cutomer_order_number'", TextView.class);
    target.sliderLayout = Utils.findRequiredViewAsType(source, R.id.slider, "field 'sliderLayout'", SliderLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerOrderAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.row = null;
    target.productImage = null;
    target.tv_moretext = null;
    target.tv_product_acturl_price = null;
    target.order_processing = null;
    target.product_Name = null;
    target.tv_quantity = null;
    target.tvquantity = null;
    target.tv_totalPrice = null;
    target.tv_cutomer_order_number = null;
    target.sliderLayout = null;
  }
}
