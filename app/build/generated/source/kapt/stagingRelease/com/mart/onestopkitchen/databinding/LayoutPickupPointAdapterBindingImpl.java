package com.mart.onestopkitchen.databinding;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutPickupPointAdapterBindingImpl extends LayoutPickupPointAdapterBinding implements com.mart.onestopkitchen.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView4;
    @NonNull
    private final android.widget.TextView mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutPickupPointAdapterBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private LayoutPickupPointAdapterBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[1]
            );
        this.llItem.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new com.mart.onestopkitchen.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.ItemListener == variableId) {
            setItemListener((com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.OnListItemClick) variable);
        }
        else if (BR.pickupPointItem == variableId) {
            setPickupPointItem((com.mart.onestopkitchen.model.PickupPointItem) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemListener(@Nullable com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.OnListItemClick ItemListener) {
        this.mItemListener = ItemListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.ItemListener);
        super.requestRebind();
    }
    public void setPickupPointItem(@Nullable com.mart.onestopkitchen.model.PickupPointItem PickupPointItem) {
        this.mPickupPointItem = PickupPointItem;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.pickupPointItem);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.OnListItemClick itemListener = mItemListener;
        java.lang.String pickupPointItemDistance = null;
        java.lang.String pickupPointItemDescription = null;
        java.lang.String pickupPointItemOpeningHours = null;
        com.mart.onestopkitchen.model.PickupPointItem pickupPointItem = mPickupPointItem;
        java.lang.String pickupPointItemName = null;

        if ((dirtyFlags & 0x6L) != 0) {



                if (pickupPointItem != null) {
                    // read pickupPointItem.distance
                    pickupPointItemDistance = pickupPointItem.getDistance();
                    // read pickupPointItem.description
                    pickupPointItemDescription = pickupPointItem.getDescription();
                    // read pickupPointItem.openingHours
                    pickupPointItemOpeningHours = pickupPointItem.getOpeningHours();
                    // read pickupPointItem.name
                    pickupPointItemName = pickupPointItem.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.llItem.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, pickupPointItemName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, pickupPointItemDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, pickupPointItemOpeningHours);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, pickupPointItemDistance);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // ItemListener != null
        boolean itemListenerJavaLangObjectNull = false;
        // ItemListener
        com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.OnListItemClick itemListener = mItemListener;
        // pickupPointItem
        com.mart.onestopkitchen.model.PickupPointItem pickupPointItem = mPickupPointItem;



        itemListenerJavaLangObjectNull = (itemListener) != (null);
        if (itemListenerJavaLangObjectNull) {



            itemListener.onListItemClicked(pickupPointItem);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): ItemListener
        flag 1 (0x2L): pickupPointItem
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}