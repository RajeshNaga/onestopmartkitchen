// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.activity;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OkDollarPayment_ViewBinding implements Unbinder {
  private OkDollarPayment target;

  @UiThread
  public OkDollarPayment_ViewBinding(OkDollarPayment target, View source) {
    this.target = target;

    target.btn_payment = Utils.findRequiredViewAsType(source, R.id.btn_payment, "field 'btn_payment'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OkDollarPayment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btn_payment = null;
  }
}
