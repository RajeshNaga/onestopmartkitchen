// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerOrderStatusAdapter$OrderNotesHolder_ViewBinding implements Unbinder {
  private CustomerOrderStatusAdapter.OrderNotesHolder target;

  @UiThread
  public CustomerOrderStatusAdapter$OrderNotesHolder_ViewBinding(
      CustomerOrderStatusAdapter.OrderNotesHolder target, View source) {
    this.target = target;

    target.order_status = Utils.findRequiredViewAsType(source, R.id.tv_order_status, "field 'order_status'", TextView.class);
    target.order_status_date = Utils.findRequiredViewAsType(source, R.id.tv_order_status_date, "field 'order_status_date'", TextView.class);
    target.lastView = Utils.findRequiredView(source, R.id.lastview, "field 'lastView'");
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerOrderStatusAdapter.OrderNotesHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.order_status = null;
    target.order_status_date = null;
    target.lastView = null;
  }
}
