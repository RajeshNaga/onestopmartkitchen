// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.activity;

import android.view.View;
import android.webkit.WebView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CyberSourceActivity_ViewBinding implements Unbinder {
  private CyberSourceActivity target;

  @UiThread
  public CyberSourceActivity_ViewBinding(CyberSourceActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CyberSourceActivity_ViewBinding(CyberSourceActivity target, View source) {
    this.target = target;

    target.cyberSourceWebView = Utils.findRequiredViewAsType(source, R.id.wv_cyber_source, "field 'cyberSourceWebView'", WebView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CyberSourceActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cyberSourceWebView = null;
  }
}
