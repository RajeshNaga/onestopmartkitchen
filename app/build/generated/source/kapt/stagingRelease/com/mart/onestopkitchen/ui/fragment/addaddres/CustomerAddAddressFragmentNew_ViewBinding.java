// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.addaddres;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.materialspinner.MaterialSpinner;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerAddAddressFragmentNew_ViewBinding implements Unbinder {
  private CustomerAddAddressFragmentNew target;

  private View view7f0a00c0;

  @UiThread
  public CustomerAddAddressFragmentNew_ViewBinding(final CustomerAddAddressFragmentNew target,
      View source) {
    this.target = target;

    View view;
    target.relRoot = Utils.findRequiredViewAsType(source, R.id.rel_root, "field 'relRoot'", RelativeLayout.class);
    target.username = Utils.findRequiredViewAsType(source, R.id.username, "field 'username'", EditText.class);
    target.spinnerCountry = Utils.findRequiredViewAsType(source, R.id.spinner_country, "field 'spinnerCountry'", MaterialSpinner.class);
    target.spinnerState = Utils.findRequiredViewAsType(source, R.id.spinner_state, "field 'spinnerState'", MaterialSpinner.class);
    target.spinnerCity = Utils.findRequiredViewAsType(source, R.id.spinner_city, "field 'spinnerCity'", MaterialSpinner.class);
    target.linEnterCity = Utils.findRequiredViewAsType(source, R.id.lin_enter_city, "field 'linEnterCity'", LinearLayout.class);
    target.edtAddressOne = Utils.findRequiredViewAsType(source, R.id.edt_address_one, "field 'edtAddressOne'", EditText.class);
    target.edtAddressTwo = Utils.findRequiredViewAsType(source, R.id.edt_address_two, "field 'edtAddressTwo'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_submit, "field 'btnSubmit' and method 'onViewClicked'");
    target.btnSubmit = Utils.castView(view, R.id.btn_submit, "field 'btnSubmit'", Button.class);
    view7f0a00c0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerAddAddressFragmentNew target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.relRoot = null;
    target.username = null;
    target.spinnerCountry = null;
    target.spinnerState = null;
    target.spinnerCity = null;
    target.linEnterCity = null;
    target.edtAddressOne = null;
    target.edtAddressTwo = null;
    target.btnSubmit = null;

    view7f0a00c0.setOnClickListener(null);
    view7f0a00c0 = null;
  }
}
