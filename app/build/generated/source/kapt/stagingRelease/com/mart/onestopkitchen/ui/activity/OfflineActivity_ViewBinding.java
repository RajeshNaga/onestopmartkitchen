// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.activity;

import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OfflineActivity_ViewBinding implements Unbinder {
  private OfflineActivity target;

  @UiThread
  public OfflineActivity_ViewBinding(OfflineActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OfflineActivity_ViewBinding(OfflineActivity target, View source) {
    this.target = target;

    target.offlineButton = Utils.findRequiredViewAsType(source, R.id.btn_tryagain, "field 'offlineButton'", Button.class);
    target.relative_layout = Utils.findRequiredViewAsType(source, R.id.id_relative_layout, "field 'relative_layout'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OfflineActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.offlineButton = null;
    target.relative_layout = null;
  }
}
