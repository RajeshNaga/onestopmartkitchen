// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.shareyourinfo.adapter;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EnterManualAdapter$ViewHolder_ViewBinding implements Unbinder {
  private EnterManualAdapter.ViewHolder target;

  @UiThread
  public EnterManualAdapter$ViewHolder_ViewBinding(EnterManualAdapter.ViewHolder target,
      View source) {
    this.target = target;

    target.txtTitle = Utils.findRequiredViewAsType(source, R.id.txt_title, "field 'txtTitle'", TextView.class);
    target.edtTitle = Utils.findRequiredViewAsType(source, R.id.edt_title, "field 'edtTitle'", EditText.class);
    target.linRoot = Utils.findRequiredViewAsType(source, R.id.lin_root, "field 'linRoot'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EnterManualAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTitle = null;
    target.edtTitle = null;
    target.linRoot = null;
  }
}
