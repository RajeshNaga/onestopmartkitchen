// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.filterupdatedui.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.jaygoo.widget.RangeSeekBar;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterFragmentUpdate_ViewBinding implements Unbinder {
  private FilterFragmentUpdate target;

  private View view7f0a00b9;

  private View view7f0a00a1;

  private View view7f0a0256;

  private View view7f0a01f2;

  @UiThread
  public FilterFragmentUpdate_ViewBinding(final FilterFragmentUpdate target, View source) {
    this.target = target;

    View view;
    target.rvCate = Utils.findRequiredViewAsType(source, R.id.rv_cate, "field 'rvCate'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_reset, "field 'btnReset' and method 'onViewClicked'");
    target.btnReset = Utils.castView(view, R.id.btn_reset, "field 'btnReset'", Button.class);
    view7f0a00b9 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.imgSearch = Utils.findRequiredViewAsType(source, R.id.img_search, "field 'imgSearch'", ImageView.class);
    target.relSearch = Utils.findRequiredViewAsType(source, R.id.rel_search, "field 'relSearch'", RelativeLayout.class);
    target.rvCateList = Utils.findRequiredViewAsType(source, R.id.rv_cate_list, "field 'rvCateList'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_apply, "field 'btnApply' and method 'onViewClicked'");
    target.btnApply = Utils.castView(view, R.id.btn_apply, "field 'btnApply'", Button.class);
    view7f0a00a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtCate = Utils.findRequiredViewAsType(source, R.id.txt_cate, "field 'txtCate'", TextView.class);
    target.viewLine = Utils.findRequiredView(source, R.id.view_line, "field 'viewLine'");
    target.imghView = Utils.findRequiredViewAsType(source, R.id.imgh_view, "field 'imghView'", ImageView.class);
    target.imgArrow = Utils.findRequiredViewAsType(source, R.id.img_arrow, "field 'imgArrow'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.lin_price, "field 'linPrice' and method 'onViewClicked'");
    target.linPrice = Utils.castView(view, R.id.lin_price, "field 'linPrice'", LinearLayout.class);
    view7f0a0256 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.edtCateSearch = Utils.findRequiredViewAsType(source, R.id.edt_cate_search, "field 'edtCateSearch'", EditText.class);
    view = Utils.findRequiredView(source, R.id.img_close, "field 'imgClose' and method 'onViewClicked'");
    target.imgClose = Utils.castView(view, R.id.img_close, "field 'imgClose'", ImageView.class);
    view7f0a01f2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtMinPrice = Utils.findRequiredViewAsType(source, R.id.txt_min_price, "field 'txtMinPrice'", TextView.class);
    target.txtMaxPrice = Utils.findRequiredViewAsType(source, R.id.txt_max_price, "field 'txtMaxPrice'", TextView.class);
    target.rangeSeekBar = Utils.findRequiredViewAsType(source, R.id.range_seek_bar, "field 'rangeSeekBar'", RangeSeekBar.class);
    target.linSeekBar = Utils.findRequiredViewAsType(source, R.id.lin_seek_bar, "field 'linSeekBar'", LinearLayout.class);
    target.linPriceData = Utils.findRequiredViewAsType(source, R.id.lin_price_data, "field 'linPriceData'", LinearLayout.class);
    target.noRecordFound = Utils.findRequiredViewAsType(source, R.id.txt_no_record, "field 'noRecordFound'", TextView.class);
    target.relRoot = Utils.findRequiredViewAsType(source, R.id.rel_root, "field 'relRoot'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterFragmentUpdate target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvCate = null;
    target.btnReset = null;
    target.imgSearch = null;
    target.relSearch = null;
    target.rvCateList = null;
    target.btnApply = null;
    target.txtCate = null;
    target.viewLine = null;
    target.imghView = null;
    target.imgArrow = null;
    target.linPrice = null;
    target.edtCateSearch = null;
    target.imgClose = null;
    target.txtMinPrice = null;
    target.txtMaxPrice = null;
    target.rangeSeekBar = null;
    target.linSeekBar = null;
    target.linPriceData = null;
    target.noRecordFound = null;
    target.relRoot = null;

    view7f0a00b9.setOnClickListener(null);
    view7f0a00b9 = null;
    view7f0a00a1.setOnClickListener(null);
    view7f0a00a1 = null;
    view7f0a0256.setOnClickListener(null);
    view7f0a0256 = null;
    view7f0a01f2.setOnClickListener(null);
    view7f0a01f2 = null;
  }
}
