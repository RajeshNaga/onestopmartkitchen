// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchPopUpWindowAdapter$ViewHolder_ViewBinding implements Unbinder {
  private SearchPopUpWindowAdapter.ViewHolder target;

  private View view7f0a053b;

  @UiThread
  public SearchPopUpWindowAdapter$ViewHolder_ViewBinding(
      final SearchPopUpWindowAdapter.ViewHolder target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.txt, "field 'txt' and method 'onViewClicked'");
    target.txt = Utils.castView(view, R.id.txt, "field 'txt'", TextView.class);
    view7f0a053b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SearchPopUpWindowAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txt = null;

    view7f0a053b.setOnClickListener(null);
    view7f0a053b = null;
  }
}
