// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.submenu;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddSubMenuFragmentChild_ViewBinding implements Unbinder {
  private AddSubMenuFragmentChild target;

  @UiThread
  public AddSubMenuFragmentChild_ViewBinding(AddSubMenuFragmentChild target, View source) {
    this.target = target;

    target.rvCategeries = Utils.findRequiredViewAsType(source, R.id.rv_categeries, "field 'rvCategeries'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddSubMenuFragmentChild target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvCategeries = null;
  }
}
