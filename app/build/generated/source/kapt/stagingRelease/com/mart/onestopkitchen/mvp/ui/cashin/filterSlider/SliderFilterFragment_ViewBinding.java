// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.mvp.ui.cashin.filterSlider;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SliderFilterFragment_ViewBinding implements Unbinder {
  private SliderFilterFragment target;

  private View view7f0a0458;

  @UiThread
  public SliderFilterFragment_ViewBinding(final SliderFilterFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.textDone, "field 'textDone' and method 'onViewClicked'");
    target.textDone = Utils.castView(view, R.id.textDone, "field 'textDone'", TextView.class);
    view7f0a0458 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.texcheckboxFull = Utils.findRequiredViewAsType(source, R.id.checkbox_full, "field 'texcheckboxFull'", CheckBox.class);
    target.check_open = Utils.findRequiredViewAsType(source, R.id.checkbox_opend, "field 'check_open'", CheckBox.class);
    target.check_close = Utils.findRequiredViewAsType(source, R.id.checkbox_closing, "field 'check_close'", CheckBox.class);
    target.all = Utils.findRequiredViewAsType(source, R.id.checkbox_slider, "field 'all'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SliderFilterFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.textDone = null;
    target.texcheckboxFull = null;
    target.check_open = null;
    target.check_close = null;
    target.all = null;

    view7f0a0458.setOnClickListener(null);
    view7f0a0458 = null;
  }
}
