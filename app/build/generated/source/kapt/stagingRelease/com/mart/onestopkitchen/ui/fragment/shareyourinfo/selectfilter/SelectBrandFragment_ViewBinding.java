// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectBrandFragment_ViewBinding implements Unbinder {
  private SelectBrandFragment target;

  private View view7f0a0202;

  @UiThread
  public SelectBrandFragment_ViewBinding(final SelectBrandFragment target, View source) {
    this.target = target;

    View view;
    target.imgSearch = Utils.findRequiredViewAsType(source, R.id.img_search, "field 'imgSearch'", ImageView.class);
    target.txtNoRecord = Utils.findRequiredViewAsType(source, R.id.txt_no_record, "field 'txtNoRecord'", TextView.class);
    target.relSearch = Utils.findRequiredViewAsType(source, R.id.rel_search, "field 'relSearch'", RelativeLayout.class);
    target.rvBrandes = Utils.findRequiredViewAsType(source, R.id.rv_brandes, "field 'rvBrandes'", RecyclerView.class);
    target.edtSearchView = Utils.findRequiredViewAsType(source, R.id.edt_search_view, "field 'edtSearchView'", EditText.class);
    view = Utils.findRequiredView(source, R.id.img_search_close, "field 'imgSearchClose' and method 'onViewClicked'");
    target.imgSearchClose = Utils.castView(view, R.id.img_search_close, "field 'imgSearchClose'", ImageView.class);
    view7f0a0202 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectBrandFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgSearch = null;
    target.txtNoRecord = null;
    target.relSearch = null;
    target.rvBrandes = null;
    target.edtSearchView = null;
    target.imgSearchClose = null;

    view7f0a0202.setOnClickListener(null);
    view7f0a0202 = null;
  }
}
