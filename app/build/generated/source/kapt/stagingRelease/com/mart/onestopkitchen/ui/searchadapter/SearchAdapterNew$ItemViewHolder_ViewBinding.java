// Generated code from Butter Knife. Do not modify!
package com.mart.onestopkitchen.ui.searchadapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mart.onestopkitchen.R;
import com.pnikosis.materialishprogress.ProgressWheel;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchAdapterNew$ItemViewHolder_ViewBinding implements Unbinder {
  private SearchAdapterNew.ItemViewHolder target;

  @UiThread
  public SearchAdapterNew$ItemViewHolder_ViewBinding(SearchAdapterNew.ItemViewHolder target,
      View source) {
    this.target = target;

    target.cardView = Utils.findRequiredViewAsType(source, R.id.card_total, "field 'cardView'", CardView.class);
    target.wishList = Utils.findRequiredViewAsType(source, R.id.button_favorite_category, "field 'wishList'", ToggleButton.class);
    target.productImage = Utils.findRequiredViewAsType(source, R.id.img_productImage, "field 'productImage'", ImageView.class);
    target.productPrice = Utils.findRequiredViewAsType(source, R.id.tv_productPrice, "field 'productPrice'", TextView.class);
    target.productOldPrice = Utils.findRequiredViewAsType(source, R.id.tv_productOldPrice, "field 'productOldPrice'", TextView.class);
    target.productName = Utils.findRequiredViewAsType(source, R.id.tv_productName, "field 'productName'", TextView.class);
    target.tv_discount_percentage = Utils.findRequiredViewAsType(source, R.id.tv_discount_percentage, "field 'tv_discount_percentage'", TextView.class);
    target.relContentView = Utils.findRequiredViewAsType(source, R.id.rel_content_view, "field 'relContentView'", RelativeLayout.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress_bar, "field 'progressBar'", ProgressWheel.class);
    target.gif_mark_new = Utils.findRequiredViewAsType(source, R.id.gif_mark_new, "field 'gif_mark_new'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SearchAdapterNew.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cardView = null;
    target.wishList = null;
    target.productImage = null;
    target.productPrice = null;
    target.productOldPrice = null;
    target.productName = null;
    target.tv_discount_percentage = null;
    target.relContentView = null;
    target.progressBar = null;
    target.gif_mark_new = null;
  }
}
