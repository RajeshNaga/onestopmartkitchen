/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.eftimoff.mylibrary;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int circleColor = 0x7f0400ae;
        public static final int circleSize = 0x7f0400b0;
        public static final int dotColor = 0x7f040111;
        public static final int gridColumns = 0x7f04017f;
        public static final int gridRows = 0x7f040180;
        public static final int pathColor = 0x7f0402ae;
    }
    public static final class drawable {
        private drawable() {}

        public static final int pattern_btn_touched = 0x7f0802f5;
        public static final int pattern_button_untouched = 0x7f0802f6;
        public static final int pattern_circle_blue = 0x7f0802f7;
        public static final int pattern_circle_green = 0x7f0802f8;
        public static final int pattern_circle_white = 0x7f0802f9;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] PatternView = { 0x7f0400ae, 0x7f0400b0, 0x7f040111, 0x7f04017f, 0x7f040180, 0x7f0402ae };
        public static final int PatternView_circleColor = 0;
        public static final int PatternView_circleSize = 1;
        public static final int PatternView_dotColor = 2;
        public static final int PatternView_gridColumns = 3;
        public static final int PatternView_gridRows = 4;
        public static final int PatternView_pathColor = 5;
    }
}
