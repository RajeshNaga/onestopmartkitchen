/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package org.apmem.tools.layouts;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int debugDraw = 0x7f040104;
        public static final int layoutDirection = 0x7f0401c1;
        public static final int layout_newLine = 0x7f040202;
        public static final int layout_weight = 0x7f04020a;
        public static final int weightDefault = 0x7f04042d;
    }
    public static final class id {
        private id() {}

        public static final int ltr = 0x7f0a02b6;
        public static final int rtl = 0x7f0a03ce;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] FlowLayout = { 0x10100af, 0x10100c4, 0x7f040104, 0x7f0401b1, 0x7f0401c1, 0x7f040211, 0x7f04042d };
        public static final int FlowLayout_android_gravity = 0;
        public static final int FlowLayout_android_orientation = 1;
        public static final int FlowLayout_debugDraw = 2;
        public static final int FlowLayout_itemSpacing = 3;
        public static final int FlowLayout_layoutDirection = 4;
        public static final int FlowLayout_lineSpacing = 5;
        public static final int FlowLayout_weightDefault = 6;
        public static final int[] FlowLayout_LayoutParams = { 0x10100b3, 0x7f040202, 0x7f04020a };
        public static final int FlowLayout_LayoutParams_android_layout_gravity = 0;
        public static final int FlowLayout_LayoutParams_layout_newLine = 1;
        public static final int FlowLayout_LayoutParams_layout_weight = 2;
    }
}
