package basicsetup.me.com.base;

import java.lang.System;

/**
 * Created by Thavam on 10-Nov-18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\bf\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lbasicsetup/me/com/base/BasePresenter;", "V", "", "destroyView", "", "setView", "getView", "(Ljava/lang/Object;)V", "app_stagingRelease"})
public abstract interface BasePresenter<V extends java.lang.Object> {
    
    public abstract void destroyView();
    
    public abstract void setView(V getView);
}