package com.mart.onestopkitchen.chat.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0000\n\u0002\u0010!\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0014\u0010\u0012\u001a\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u0005J\u0010\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\fH\u0016J\"\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0016\u001a\u00020\f2\b\u0010\u0019\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0006H\u0014R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\u0002\b\u00030\u000ej\u0006\u0012\u0002\b\u0003`\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001f"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/CheckboxUsersAdapter;", "Lcom/mart/onestopkitchen/chat/ui/adapters/MsgUsersAdapter;", "context", "Landroid/content/Context;", "users", "", "Lcom/quickblox/users/model/QBUser;", "(Landroid/content/Context;Ljava/util/List;)V", "_selectedUsers", "", "initiallySelectedUsers", "", "", "selectedUsers", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "getSelectedUsers", "()Ljava/util/ArrayList;", "addSelectedUsers", "", "userIdList", "getItem", "position", "getView", "Landroid/view/View;", "convertView", "parent", "Landroid/view/ViewGroup;", "isAvailableForSelection", "", "user", "app_stagingRelease"})
public final class CheckboxUsersAdapter extends com.mart.onestopkitchen.chat.ui.adapters.MsgUsersAdapter {
    private final java.util.List<java.lang.Integer> initiallySelectedUsers = null;
    private final java.util.Set<com.quickblox.users.model.QBUser> _selectedUsers = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<?> getSelectedUsers() {
        return null;
    }
    
    public final void addSelectedUsers(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Integer> userIdList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View getView(int position, @org.jetbrains.annotations.Nullable()
    android.view.View convertView, @org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.quickblox.users.model.QBUser getItem(int position) {
        return null;
    }
    
    @java.lang.Override()
    protected boolean isAvailableForSelection(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser user) {
        return false;
    }
    
    public CheckboxUsersAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.users.model.QBUser> users) {
        super(null, null);
    }
}