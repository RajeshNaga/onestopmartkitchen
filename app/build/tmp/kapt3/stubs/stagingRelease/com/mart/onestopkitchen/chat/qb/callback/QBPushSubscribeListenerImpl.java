package com.mart.onestopkitchen.chat.qb.callback;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001c\u0010\b\u001a\u00020\u00042\n\u0010\t\u001a\u00060\nj\u0002`\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016\u00a8\u0006\u000e"}, d2 = {"Lcom/mart/onestopkitchen/chat/qb/callback/QBPushSubscribeListenerImpl;", "Lcom/quickblox/messages/services/QBPushManager$QBSubscribeListener;", "()V", "onSubscriptionCreated", "", "onSubscriptionDeleted", "b", "", "onSubscriptionError", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "i", "", "app_stagingRelease"})
public class QBPushSubscribeListenerImpl implements com.quickblox.messages.services.QBPushManager.QBSubscribeListener {
    
    @java.lang.Override()
    public void onSubscriptionCreated() {
    }
    
    @java.lang.Override()
    public void onSubscriptionError(@org.jetbrains.annotations.NotNull()
    java.lang.Exception e, int i) {
    }
    
    @java.lang.Override()
    public void onSubscriptionDeleted(boolean b) {
    }
    
    public QBPushSubscribeListenerImpl() {
        super();
    }
}