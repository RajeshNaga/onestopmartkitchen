package com.mart.onestopkitchen.ui.fragment;

import java.lang.System;

/**
 * Created by Akash Garg on 15/01/2019.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0016\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u0018H\u0016J\b\u0010\u001a\u001a\u00020\u000bH\u0002J&\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\b\u0010#\u001a\u00020\u0018H\u0002J\b\u0010$\u001a\u00020\u0018H\u0016J\u001a\u0010%\u001a\u00020\u00182\u0006\u0010&\u001a\u00020\u001c2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0017J\b\u0010\'\u001a\u00020\u0018H\u0002J\b\u0010(\u001a\u00020\u0018H\u0002J\u0010\u0010)\u001a\u00020\u00182\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010*\u001a\u00020\u0018H\u0002J\b\u0010+\u001a\u00020\u0018H\u0002J\b\u0010,\u001a\u00020\u0018H\u0016J\u0010\u0010-\u001a\u00020\u00182\u0006\u0010.\u001a\u00020\rH\u0016J\b\u0010/\u001a\u00020\u0018H\u0016R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\r0\u000fj\b\u0012\u0004\u0012\u00020\r`\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\r0\u000fj\b\u0012\u0004\u0012\u00020\r`\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lcom/mart/onestopkitchen/ui/fragment/CardDetailsFragment;", "Lcom/mart/onestopkitchen/ui/fragment/BaseFragment;", "Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintBaseView;", "()V", "dialog", "Landroid/app/Dialog;", "getDialog", "()Landroid/app/Dialog;", "setDialog", "(Landroid/app/Dialog;)V", "isValid", "", "month", "", "monthList", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "presenter", "Lcom/mart/onestopkitchen/utils/fingerprint/FingerPrintPresenter;", "savesCard", "Lcom/mart/onestopkitchen/model/creditcarddao/CardDetailModel;", "year", "yearsList", "error", "", "failure", "isFormValid", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onItemSelected", "onResume", "onViewCreated", "view", "openFingerPrintPopup", "payBtnClick", "setCardData", "setMonth", "setYears", "showFingerPrintPopup", "showMsg", "msg", "success", "app_stagingRelease"})
public class CardDetailsFragment extends com.mart.onestopkitchen.ui.fragment.BaseFragment implements com.mart.onestopkitchen.utils.fingerprint.FingerprintBaseView {
    private boolean isValid;
    private java.lang.String month;
    private java.lang.String year;
    private com.mart.onestopkitchen.model.creditcarddao.CardDetailModel savesCard;
    private com.mart.onestopkitchen.utils.fingerprint.FingerPrintPresenter presenter;
    @org.jetbrains.annotations.Nullable()
    private android.app.Dialog dialog;
    private java.util.ArrayList<java.lang.String> monthList;
    private java.util.ArrayList<java.lang.String> yearsList;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.Dialog getDialog() {
        return null;
    }
    
    public final void setDialog(@org.jetbrains.annotations.Nullable()
    android.app.Dialog p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void openFingerPrintPopup() {
    }
    
    private final void setCardData(com.mart.onestopkitchen.model.creditcarddao.CardDetailModel savesCard) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    private final void setMonth() {
    }
    
    private final void setYears() {
    }
    
    private final boolean isFormValid() {
        return false;
    }
    
    private final void onItemSelected() {
    }
    
    private final void payBtnClick() {
    }
    
    @java.lang.Override()
    public void showMsg(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @java.lang.Override()
    public void success() {
    }
    
    @java.lang.Override()
    public void failure() {
    }
    
    @java.lang.Override()
    public void error() {
    }
    
    @java.lang.Override()
    public void showFingerPrintPopup() {
    }
    
    public CardDetailsFragment() {
        super();
    }
}