package com.mart.onestopkitchen.ui.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001dB\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u001c\u0010\u0015\u001a\u00020\u00162\n\u0010\u0017\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u0014H\u0016J\u001c\u0010\u0019\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0014H\u0016R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/OrderGroupReceiptPageAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/mart/onestopkitchen/ui/adapter/OrderGroupReceiptPageAdapter$OrderGroupViewHolder;", "mContext", "Landroid/content/Context;", "orderNumbers", "", "Lcom/mart/onestopkitchen/model/OrdersModel;", "(Landroid/content/Context;Ljava/util/List;)V", "inflater", "Landroid/view/LayoutInflater;", "getInflater", "()Landroid/view/LayoutInflater;", "setInflater", "(Landroid/view/LayoutInflater;)V", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "OrderGroupViewHolder", "app_stagingRelease"})
public class OrderGroupReceiptPageAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.mart.onestopkitchen.ui.adapter.OrderGroupReceiptPageAdapter.OrderGroupViewHolder> {
    @org.jetbrains.annotations.Nullable()
    private android.view.LayoutInflater inflater;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context mContext;
    private final java.util.List<com.mart.onestopkitchen.model.OrdersModel> orderNumbers = null;
    
    @org.jetbrains.annotations.Nullable()
    protected final android.view.LayoutInflater getInflater() {
        return null;
    }
    
    protected final void setInflater(@org.jetbrains.annotations.Nullable()
    android.view.LayoutInflater p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.mart.onestopkitchen.ui.adapter.OrderGroupReceiptPageAdapter.OrderGroupViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.ui.adapter.OrderGroupReceiptPageAdapter.OrderGroupViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    public OrderGroupReceiptPageAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mart.onestopkitchen.model.OrdersModel> orderNumbers) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/OrderGroupReceiptPageAdapter$OrderGroupViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Landroidx/databinding/ViewDataBinding;", "(Lcom/mart/onestopkitchen/ui/adapter/OrderGroupReceiptPageAdapter;Landroidx/databinding/ViewDataBinding;)V", "bind", "", "data", "Lcom/mart/onestopkitchen/model/OrdersModel;", "app_stagingRelease"})
    public final class OrderGroupViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private androidx.databinding.ViewDataBinding binding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.model.OrdersModel data) {
        }
        
        public OrderGroupViewHolder(@org.jetbrains.annotations.NotNull()
        androidx.databinding.ViewDataBinding binding) {
            super(null);
        }
    }
}