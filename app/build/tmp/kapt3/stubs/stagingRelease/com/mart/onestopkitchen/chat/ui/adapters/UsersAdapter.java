package com.mart.onestopkitchen.chat.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\'(B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0015H\u0007J\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u0018\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001aH\u0016J\u0018\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u001aH\u0016J\u000e\u0010#\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010$\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020\u0007H\u0002J\u0014\u0010&\u001a\u00020\u001c2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/UsersAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/mart/onestopkitchen/chat/ui/adapters/UsersAdapter$ViewHolder;", "context", "Landroid/content/Context;", "usersList", "", "Lcom/quickblox/users/model/QBUser;", "(Landroid/content/Context;Ljava/util/List;)V", "_selectedUsers", "", "getContext", "()Landroid/content/Context;", "isOnline", "", "selectedItemsCountsChangedListener", "Lcom/mart/onestopkitchen/chat/ui/adapters/UsersAdapter$SelectedItemsCountsChangedListener;", "selectedUsers", "getSelectedUsers", "()Ljava/util/List;", "getDate", "", "milliSeconds", "", "dateFormat", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setSelectedItemsCountsChangedListener", "toggleSelection", "qbUser", "updateUsersList", "SelectedItemsCountsChangedListener", "ViewHolder", "app_stagingRelease"})
public final class UsersAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.mart.onestopkitchen.chat.ui.adapters.UsersAdapter.ViewHolder> {
    private final java.util.List<com.quickblox.users.model.QBUser> _selectedUsers = null;
    private boolean isOnline;
    private com.mart.onestopkitchen.chat.ui.adapters.UsersAdapter.SelectedItemsCountsChangedListener selectedItemsCountsChangedListener;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    private java.util.List<? extends com.quickblox.users.model.QBUser> usersList;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.quickblox.users.model.QBUser> getSelectedUsers() {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.ui.adapters.UsersAdapter.ViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SimpleDateFormat"})
    public final java.lang.String getDate(long milliSeconds, @org.jetbrains.annotations.NotNull()
    java.lang.String dateFormat) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void updateUsersList(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.users.model.QBUser> usersList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.mart.onestopkitchen.chat.ui.adapters.UsersAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    private final void toggleSelection(com.quickblox.users.model.QBUser qbUser) {
    }
    
    public final void setSelectedItemsCountsChangedListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.ui.adapters.UsersAdapter.SelectedItemsCountsChangedListener selectedItemsCountsChangedListener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public UsersAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.users.model.QBUser> usersList) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006\u0017"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/UsersAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "opponentIcon", "Landroid/widget/ImageView;", "getOpponentIcon", "()Landroid/widget/ImageView;", "opponentName", "Lcom/mart/onestopkitchen/utils/CustomTextView;", "getOpponentName", "()Lcom/mart/onestopkitchen/utils/CustomTextView;", "opponentStatus", "getOpponentStatus", "rootLayout", "Landroid/widget/LinearLayout;", "getRootLayout", "()Landroid/widget/LinearLayout;", "status", "Lde/hdodenhof/circleimageview/CircleImageView;", "getStatus", "()Lde/hdodenhof/circleimageview/CircleImageView;", "app_stagingRelease"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageView opponentIcon = null;
        @org.jetbrains.annotations.NotNull()
        private final com.mart.onestopkitchen.utils.CustomTextView opponentName = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.LinearLayout rootLayout = null;
        @org.jetbrains.annotations.NotNull()
        private final de.hdodenhof.circleimageview.CircleImageView status = null;
        @org.jetbrains.annotations.NotNull()
        private final com.mart.onestopkitchen.utils.CustomTextView opponentStatus = null;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getOpponentIcon() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.CustomTextView getOpponentName() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getRootLayout() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final de.hdodenhof.circleimageview.CircleImageView getStatus() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.CustomTextView getOpponentStatus() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/UsersAdapter$SelectedItemsCountsChangedListener;", "", "onCountSelectedItemsChanged", "", "count", "", "app_stagingRelease"})
    public static abstract interface SelectedItemsCountsChangedListener {
        
        public abstract void onCountSelectedItemsChanged(int count);
    }
}