package com.mart.onestopkitchen.utils.bluetooth;

import java.lang.System;

@java.lang.SuppressWarnings(value = {"ALL"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\n\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 42\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001:\u00014B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\"\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\nH\u0002J\u0012\u0010\u001d\u001a\u00020\u001e2\b\u0010$\u001a\u0004\u0018\u00010%H\u0002J\u0010\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\'H\u0002J\b\u0010(\u001a\u00020)H\u0002J\b\u0010*\u001a\u00020\u001eH\u0014J\b\u0010+\u001a\u00020\u001eH\u0014J \u0010,\u001a\u00020\u001e2\u0006\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020\b2\b\u0010/\u001a\u0004\u0018\u000100J\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020)02J\b\u00103\u001a\u00020\u001eH\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00065"}, d2 = {"Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothScan;", "Landroidx/lifecycle/LiveData;", "", "Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothModel;", "appContext", "Landroid/content/Context;", "(Landroid/content/Context;)V", "RC_BLUETOOTH", "", "TAG", "", "allBluetoothList", "", "bleJob", "Lkotlinx/coroutines/Job;", "getBleJob", "()Lkotlinx/coroutines/Job;", "setBleJob", "(Lkotlinx/coroutines/Job;)V", "bleScanner", "Landroid/bluetooth/le/BluetoothLeScanner;", "mBleCallback", "Landroid/bluetooth/le/ScanCallback;", "getMBleCallback", "()Landroid/bluetooth/le/ScanCallback;", "mBluetoothAdapter", "Landroid/bluetooth/BluetoothAdapter;", "scanResultReceiver", "Landroid/content/BroadcastReceiver;", "addDeviceDetails", "", "device", "Landroid/bluetooth/BluetoothDevice;", "rssi", "", "uuid", "scanResult", "Landroid/bluetooth/le/ScanResult;", "getPairedDevices", "Ljava/util/ArrayList;", "isBleAvailable", "", "onActive", "onInactive", "onPermissionActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "scanBluetoothDevices", "Lkotlin/Pair;", "startBLeScanning", "Companion", "app_stagingRelease"})
public final class BluetoothScan extends androidx.lifecycle.LiveData<java.util.List<? extends com.mart.onestopkitchen.utils.bluetooth.BluetoothModel>> {
    private java.lang.String TAG;
    private android.bluetooth.BluetoothAdapter mBluetoothAdapter;
    private final int RC_BLUETOOTH = 99;
    private java.util.List<com.mart.onestopkitchen.utils.bluetooth.BluetoothModel> allBluetoothList;
    private android.content.BroadcastReceiver scanResultReceiver;
    private android.bluetooth.le.BluetoothLeScanner bleScanner;
    @org.jetbrains.annotations.Nullable()
    private kotlinx.coroutines.Job bleJob;
    @org.jetbrains.annotations.NotNull()
    private final android.bluetooth.le.ScanCallback mBleCallback = null;
    private android.content.Context appContext;
    private static com.mart.onestopkitchen.utils.bluetooth.BluetoothScan instance;
    public static final com.mart.onestopkitchen.utils.bluetooth.BluetoothScan.Companion Companion = null;
    
    private final void addDeviceDetails(android.bluetooth.BluetoothDevice device, short rssi, java.lang.String uuid) {
    }
    
    private final java.util.ArrayList<com.mart.onestopkitchen.utils.bluetooth.BluetoothModel> getPairedDevices() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.Pair<com.mart.onestopkitchen.utils.bluetooth.BluetoothScan, java.lang.Boolean> scanBluetoothDevices() {
        return null;
    }
    
    public final void onPermissionActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    protected void onActive() {
    }
    
    @java.lang.Override()
    protected void onInactive() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlinx.coroutines.Job getBleJob() {
        return null;
    }
    
    public final void setBleJob(@org.jetbrains.annotations.Nullable()
    kotlinx.coroutines.Job p0) {
    }
    
    /**
     * *********************************************************************  BLE Scanner
     */
    private final boolean isBleAvailable() {
        return false;
    }
    
    private final void startBLeScanning() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.bluetooth.le.ScanCallback getMBleCallback() {
        return null;
    }
    
    private final void addDeviceDetails(android.bluetooth.le.ScanResult scanResult) {
    }
    
    public BluetoothScan(@org.jetbrains.annotations.NotNull()
    android.content.Context appContext) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothScan$Companion;", "", "()V", "instance", "Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothScan;", "getInstance", "appContext", "Landroid/content/Context;", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.bluetooth.BluetoothScan getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context appContext) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}