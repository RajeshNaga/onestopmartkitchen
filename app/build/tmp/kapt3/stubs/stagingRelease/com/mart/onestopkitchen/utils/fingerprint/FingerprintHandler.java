package com.mart.onestopkitchen.utils.fingerprint;

import java.lang.System;

@androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
@kotlin.Suppress(names = {"DEPRECATION"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001a\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u0012H\u0016J\u001a\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00142\b\u0010\u001a\u001a\u0004\u0018\u00010\u0016H\u0016J\u0012\u0010\u001b\u001a\u00020\u00122\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\u0016\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006#"}, d2 = {"Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintHandler;", "Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;", "mContext", "Landroid/content/Context;", "listener", "Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintResult;", "(Landroid/content/Context;Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintResult;)V", "cancellationSignal", "Landroid/os/CancellationSignal;", "getListener", "()Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintResult;", "setListener", "(Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintResult;)V", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "onAuthenticationError", "", "errorCode", "", "errString", "", "onAuthenticationFailed", "onAuthenticationHelp", "helpCode", "helpString", "onAuthenticationSucceeded", "result", "Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;", "startAuth", "fingerprintManager", "Landroid/hardware/fingerprint/FingerprintManager;", "cryptoObject", "Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;", "app_stagingRelease"})
public final class FingerprintHandler extends android.hardware.fingerprint.FingerprintManager.AuthenticationCallback {
    private android.os.CancellationSignal cancellationSignal;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context mContext;
    @org.jetbrains.annotations.NotNull()
    private com.mart.onestopkitchen.utils.fingerprint.FingerprintResult listener;
    
    @java.lang.Override()
    public void onAuthenticationError(int errorCode, @org.jetbrains.annotations.Nullable()
    java.lang.CharSequence errString) {
    }
    
    @java.lang.Override()
    public void onAuthenticationSucceeded(@org.jetbrains.annotations.Nullable()
    android.hardware.fingerprint.FingerprintManager.AuthenticationResult result) {
    }
    
    @java.lang.Override()
    public void onAuthenticationHelp(int helpCode, @org.jetbrains.annotations.Nullable()
    java.lang.CharSequence helpString) {
    }
    
    @java.lang.Override()
    public void onAuthenticationFailed() {
    }
    
    public final void startAuth(@org.jetbrains.annotations.NotNull()
    android.hardware.fingerprint.FingerprintManager fingerprintManager, @org.jetbrains.annotations.NotNull()
    android.hardware.fingerprint.FingerprintManager.CryptoObject cryptoObject) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.utils.fingerprint.FingerprintResult getListener() {
        return null;
    }
    
    public final void setListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.utils.fingerprint.FingerprintResult p0) {
    }
    
    public FingerprintHandler(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.utils.fingerprint.FingerprintResult listener) {
        super();
    }
}