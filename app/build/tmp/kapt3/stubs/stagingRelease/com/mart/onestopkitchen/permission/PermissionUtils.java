package com.mart.onestopkitchen.permission;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0002\b\u0007\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0005\u00a2\u0006\u0002\u0010\u0002J\u001b\u0010\r\u001a\u00020\u000e2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000fH\u0002\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u0011\u001a\u00020\u0004H\u0002J)\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00142\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u000f2\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\u0019\u0010\u0019\u001a\u00020\u000e2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u000f\u00a2\u0006\u0002\u0010\u0010J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/mart/onestopkitchen/permission/PermissionUtils;", "", "()V", "deniedPermission", "", "loopExecute", "mContext", "Landroid/content/Context;", "mListener", "Lcom/mart/onestopkitchen/permission/PermissionListener;", "permission", "Ljava/util/ArrayList;", "", "checkPermissionAlReadyGranted", "", "", "([Ljava/lang/String;)V", "isAvailableAllPermission", "onPermissionRequestResult", "requestCode", "", "permissions", "grantResults", "", "(I[Ljava/lang/String;[I)V", "requestPermission", "setListener", "with", "context", "Companion", "app_stagingRelease"})
public final class PermissionUtils {
    private boolean loopExecute;
    private final java.util.ArrayList<java.lang.String> permission = null;
    private android.content.Context mContext;
    private com.mart.onestopkitchen.permission.PermissionListener mListener;
    private boolean deniedPermission;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String ACCESS_FINE_LOCATION;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String READ_PHONE_STATE;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String CAMERA;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String RECORD_AUDIO;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String GET_ACCOUNTS;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String WRITE_EXTERNAL_STORAGE;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String READ_EXTERNAL_STORAGE;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String READ_CONTACTS;
    private static int REQUEST_CODE;
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"StaticFieldLeak"})
    private static final com.mart.onestopkitchen.permission.PermissionUtils instance = null;
    public static final com.mart.onestopkitchen.permission.PermissionUtils.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.permission.PermissionUtils with(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.permission.PermissionUtils setListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.permission.PermissionListener mListener) {
        return null;
    }
    
    private final boolean isAvailableAllPermission() {
        return false;
    }
    
    public final void requestPermission(@org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions) {
    }
    
    private final void checkPermissionAlReadyGranted(java.lang.String[] permission) {
    }
    
    public final void onPermissionRequestResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public PermissionUtils() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0017\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001a\u0010\u000f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001a\u0010\u0012\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR\u001a\u0010\u0015\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR\u001a\u0010\u0018\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR\u001a\u0010\u001b\u001a\u00020\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001a\u0010!\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0006\"\u0004\b#\u0010\bR\u0016\u0010$\u001a\u00020%8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\'\u00a8\u0006("}, d2 = {"Lcom/mart/onestopkitchen/permission/PermissionUtils$Companion;", "", "()V", "ACCESS_FINE_LOCATION", "", "getACCESS_FINE_LOCATION", "()Ljava/lang/String;", "setACCESS_FINE_LOCATION", "(Ljava/lang/String;)V", "CAMERA", "getCAMERA", "setCAMERA", "GET_ACCOUNTS", "getGET_ACCOUNTS", "setGET_ACCOUNTS", "READ_CONTACTS", "getREAD_CONTACTS", "setREAD_CONTACTS", "READ_EXTERNAL_STORAGE", "getREAD_EXTERNAL_STORAGE", "setREAD_EXTERNAL_STORAGE", "READ_PHONE_STATE", "getREAD_PHONE_STATE", "setREAD_PHONE_STATE", "RECORD_AUDIO", "getRECORD_AUDIO", "setRECORD_AUDIO", "REQUEST_CODE", "", "getREQUEST_CODE", "()I", "setREQUEST_CODE", "(I)V", "WRITE_EXTERNAL_STORAGE", "getWRITE_EXTERNAL_STORAGE", "setWRITE_EXTERNAL_STORAGE", "instance", "Lcom/mart/onestopkitchen/permission/PermissionUtils;", "getInstance", "()Lcom/mart/onestopkitchen/permission/PermissionUtils;", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getACCESS_FINE_LOCATION() {
            return null;
        }
        
        public final void setACCESS_FINE_LOCATION(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getREAD_PHONE_STATE() {
            return null;
        }
        
        public final void setREAD_PHONE_STATE(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getCAMERA() {
            return null;
        }
        
        public final void setCAMERA(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getRECORD_AUDIO() {
            return null;
        }
        
        public final void setRECORD_AUDIO(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getGET_ACCOUNTS() {
            return null;
        }
        
        public final void setGET_ACCOUNTS(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getWRITE_EXTERNAL_STORAGE() {
            return null;
        }
        
        public final void setWRITE_EXTERNAL_STORAGE(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getREAD_EXTERNAL_STORAGE() {
            return null;
        }
        
        public final void setREAD_EXTERNAL_STORAGE(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getREAD_CONTACTS() {
            return null;
        }
        
        public final void setREAD_CONTACTS(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        public final int getREQUEST_CODE() {
            return 0;
        }
        
        public final void setREQUEST_CODE(int p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.permission.PermissionUtils getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}