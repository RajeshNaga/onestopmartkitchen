package com.mart.onestopkitchen.chat.services.fcm;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\b\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"}, d2 = {"NOTIFICATION_ID", "", "app_stagingRelease"})
public final class PushListenerServiceKt {
    private static final int NOTIFICATION_ID = 1;
}