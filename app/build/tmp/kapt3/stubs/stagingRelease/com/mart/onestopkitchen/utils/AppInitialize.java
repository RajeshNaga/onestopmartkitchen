package com.mart.onestopkitchen.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0016\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/utils/AppInitialize;", "", "()V", "initializeModules", "", "Companion", "app_stagingRelease"})
public class AppInitialize {
    @org.jetbrains.annotations.Nullable()
    private static com.google.gson.Gson gson;
    public static final com.mart.onestopkitchen.utils.AppInitialize.Companion Companion = null;
    
    public final void initializeModules() {
    }
    
    public AppInitialize() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\t\u001a\u00020\u0004R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\n"}, d2 = {"Lcom/mart/onestopkitchen/utils/AppInitialize$Companion;", "", "()V", "gson", "Lcom/google/gson/Gson;", "getGson", "()Lcom/google/gson/Gson;", "setGson", "(Lcom/google/gson/Gson;)V", "getGsonSingleton", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.google.gson.Gson getGson() {
            return null;
        }
        
        public final void setGson(@org.jetbrains.annotations.Nullable()
        com.google.gson.Gson p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.gson.Gson getGsonSingleton() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}