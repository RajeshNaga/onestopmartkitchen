package com.mart.onestopkitchen.rxjava;

import java.lang.System;

/**
 * Created by Thavam on 10-Nov-18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/mart/onestopkitchen/rxjava/RxJavaUtils;", "", "()V", "Companion", "app_stagingRelease"})
public final class RxJavaUtils {
    public static final com.mart.onestopkitchen.rxjava.RxJavaUtils.Companion Companion = null;
    
    public RxJavaUtils() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0000\u0010\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/rxjava/RxJavaUtils$Companion;", "", "()V", "applySchedulers", "Lio/reactivex/ObservableTransformer;", "T", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final <T extends java.lang.Object>io.reactivex.ObservableTransformer<T, T> applySchedulers() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}