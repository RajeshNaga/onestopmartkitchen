package com.mart.onestopkitchen.chat.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003345B3\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\"\u001a\u00020\u000b2\u0006\u0010#\u001a\u00020\u000bJ\b\u0010$\u001a\u00020\u000bH\u0016J\u0010\u0010%\u001a\u00020&2\u0006\u0010#\u001a\u00020\u000bH\u0016J\u0018\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u00022\u0006\u0010#\u001a\u00020\u000bH\u0016J\u0018\u0010*\u001a\u00020\u00022\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u000bH\u0017J\u000e\u0010.\u001a\u00020(2\u0006\u0010/\u001a\u00020\u000bJ\u0016\u00100\u001a\u00020(2\u0006\u0010#\u001a\u00020\u000b2\u0006\u00101\u001a\u00020\tJ\u000e\u00102\u001a\u00020(2\u0006\u0010\u0013\u001a\u00020\u0014R\u0016\u0010\u000e\u001a\n \u0010*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\f\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u001e\u0010\u001fR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u001fR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001a\u00a8\u00066"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$ViewHolder;", "context", "Landroid/content/Context;", "baseConversationFragment", "Lcom/mart/onestopkitchen/chat/ui/fragments/BaseConversationFragment;", "users", "", "Lcom/quickblox/users/model/QBUser;", "width", "", "height", "(Landroid/content/Context;Lcom/mart/onestopkitchen/chat/ui/fragments/BaseConversationFragment;Ljava/util/List;II)V", "TAG", "", "kotlin.jvm.PlatformType", "_opponents", "", "adapterListener", "Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$OnAdapterEventListener;", "getBaseConversationFragment", "()Lcom/mart/onestopkitchen/chat/ui/fragments/BaseConversationFragment;", "getContext", "()Landroid/content/Context;", "getHeight", "()I", "inflater", "Landroid/view/LayoutInflater;", "opponents", "getOpponents", "()Ljava/util/List;", "getUsers", "getWidth", "getItem", "position", "getItemCount", "getItemId", "", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "removeItem", "index", "replaceUsers", "qbUser", "setAdapterListener", "OnAdapterEventListener", "QBRTCSessionStatus", "ViewHolder", "app_stagingRelease"})
public final class OpponentsFromCallAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.ViewHolder> {
    private final java.lang.String TAG = null;
    private java.util.List<com.quickblox.users.model.QBUser> _opponents;
    private android.view.LayoutInflater inflater;
    private com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.OnAdapterEventListener adapterListener;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.mart.onestopkitchen.chat.ui.fragments.BaseConversationFragment baseConversationFragment = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.quickblox.users.model.QBUser> users = null;
    private final int width = 0;
    private final int height = 0;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.quickblox.users.model.QBUser> getOpponents() {
        return null;
    }
    
    public final void setAdapterListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.OnAdapterEventListener adapterListener) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final int getItem(int position) {
        return 0;
    }
    
    public final void removeItem(int index) {
    }
    
    public final void replaceUsers(int position, @org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser qbUser) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"InflateParams"})
    @java.lang.Override()
    public com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public long getItemId(int position) {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.chat.ui.fragments.BaseConversationFragment getBaseConversationFragment() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.quickblox.users.model.QBUser> getUsers() {
        return null;
    }
    
    public final int getWidth() {
        return 0;
    }
    
    public final int getHeight() {
        return 0;
    }
    
    public OpponentsFromCallAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.ui.fragments.BaseConversationFragment baseConversationFragment, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.users.model.QBUser> users, int width, int height) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001%B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\u0015\u001a\u00020\tJ\u0006\u0010\u0016\u001a\u00020\u0010J\u0006\u0010\u0017\u001a\u00020\u0012J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0004H\u0016J\u000e\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eJ\u000e\u0010\u001f\u001a\u00020\u00192\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010 \u001a\u00020\u00192\u0006\u0010!\u001a\u00020\u001eJ\u000e\u0010\"\u001a\u00020\u00192\u0006\u0010#\u001a\u00020$R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u00020\u0007X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View$OnClickListener;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "connectionStatus", "Lcom/mart/onestopkitchen/utils/CustomTextView;", "opponentView", "Lcom/quickblox/videochat/webrtc/view/QBRTCSurfaceView;", "opponentsName", "getOpponentsName$app_stagingRelease", "()Lcom/mart/onestopkitchen/utils/CustomTextView;", "setOpponentsName$app_stagingRelease", "(Lcom/mart/onestopkitchen/utils/CustomTextView;)V", "progressBar", "Landroid/widget/ProgressBar;", "userId", "", "viewHolderClickListener", "Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$ViewHolder$ViewHolderClickListener;", "getOpponentView", "getProgressBar", "getUserId", "onClick", "", "v", "setListener", "setStatus", "status", "", "setUserId", "setUserName", "userName", "showOpponentView", "show", "", "ViewHolderClickListener", "app_stagingRelease"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder implements android.view.View.OnClickListener {
        @org.jetbrains.annotations.NotNull()
        private com.mart.onestopkitchen.utils.CustomTextView opponentsName;
        private com.mart.onestopkitchen.utils.CustomTextView connectionStatus;
        private com.quickblox.videochat.webrtc.view.QBRTCSurfaceView opponentView;
        private android.widget.ProgressBar progressBar;
        private int userId;
        private com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.ViewHolder.ViewHolderClickListener viewHolderClickListener;
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.CustomTextView getOpponentsName$app_stagingRelease() {
            return null;
        }
        
        public final void setOpponentsName$app_stagingRelease(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.utils.CustomTextView p0) {
        }
        
        public final void setListener(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.ViewHolder.ViewHolderClickListener viewHolderClickListener) {
        }
        
        public final void setStatus(@org.jetbrains.annotations.NotNull()
        java.lang.String status) {
        }
        
        public final void setUserName(@org.jetbrains.annotations.NotNull()
        java.lang.String userName) {
        }
        
        public final void setUserId(int userId) {
        }
        
        public final int getUserId() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ProgressBar getProgressBar() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.quickblox.videochat.webrtc.view.QBRTCSurfaceView getOpponentView() {
            return null;
        }
        
        public final void showOpponentView(boolean show) {
        }
        
        @java.lang.Override()
        public void onClick(@org.jetbrains.annotations.NotNull()
        android.view.View v) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$ViewHolder$ViewHolderClickListener;", "", "onShowOpponent", "", "callerId", "", "app_stagingRelease"})
        public static abstract interface ViewHolderClickListener {
            
            public abstract void onShowOpponent(int callerId);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$QBRTCSessionStatus;", "", "()V", "peerStateDescriptions", "Landroid/util/SparseIntArray;", "getStatusDescription", "", "connectionState", "Lcom/quickblox/videochat/webrtc/QBRTCTypes$QBRTCConnectionState;", "app_stagingRelease"})
    static final class QBRTCSessionStatus {
        private final android.util.SparseIntArray peerStateDescriptions = null;
        
        public final int getStatusDescription(@org.jetbrains.annotations.NotNull()
        com.quickblox.videochat.webrtc.QBRTCTypes.QBRTCConnectionState connectionState) {
            return 0;
        }
        
        public QBRTCSessionStatus() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$OnAdapterEventListener;", "", "onBindLastViewHolder", "", "holder", "Lcom/mart/onestopkitchen/chat/ui/adapters/OpponentsFromCallAdapter$ViewHolder;", "position", "", "onItemClick", "app_stagingRelease"})
    public static abstract interface OnAdapterEventListener {
        
        public abstract void onBindLastViewHolder(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.chat.ui.adapters.OpponentsFromCallAdapter.ViewHolder holder, int position);
        
        public abstract void onItemClick(int position);
    }
}