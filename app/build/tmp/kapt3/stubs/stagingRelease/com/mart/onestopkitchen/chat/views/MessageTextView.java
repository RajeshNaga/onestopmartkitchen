package com.mart.onestopkitchen.chat.views;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b&\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0004\u001a\u00020\u0005H\u0003J\u0012\u0010\u001d\u001a\u00020\u001c2\b\b\u0001\u0010\u001e\u001a\u00020\u001fH\u0002J\u001c\u0010 \u001a\u00020\u001c2\b\b\u0001\u0010!\u001a\u00020\u001f2\b\b\u0001\u0010\"\u001a\u00020\u001fH\u0002J\b\u0010#\u001a\u00020\u001cH$J\u0012\u0010$\u001a\u00020\u001c2\b\b\u0001\u0010%\u001a\u00020\u001fH\u0002J\b\u0010&\u001a\u00020\u001cH$R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u00020\u000bX\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u000bX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\r\"\u0004\b\u0014\u0010\u000fR\u001a\u0010\u0015\u001a\u00020\u0016X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006\'"}, d2 = {"Lcom/mart/onestopkitchen/chat/views/MessageTextView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "TAG", "", "kotlin.jvm.PlatformType", "frameLinear", "Landroid/widget/LinearLayout;", "getFrameLinear", "()Landroid/widget/LinearLayout;", "setFrameLinear", "(Landroid/widget/LinearLayout;)V", "inflater", "Landroid/view/LayoutInflater;", "layoutStub", "getLayoutStub", "setLayoutStub", "viewTextStub", "Landroid/view/ViewStub;", "getViewTextStub", "()Landroid/view/ViewStub;", "setViewTextStub", "(Landroid/view/ViewStub;)V", "applyAttributes", "", "init", "layoutId", "", "setCustomWidgets", "widgetIdBottom", "widgetIdTop", "setLinearSide", "setLinkPreviewWidget", "linkPreviewWidget", "setTextLayout", "app_stagingRelease"})
public abstract class MessageTextView extends android.widget.FrameLayout {
    private final java.lang.String TAG = null;
    @org.jetbrains.annotations.NotNull()
    protected android.widget.LinearLayout frameLinear;
    @org.jetbrains.annotations.NotNull()
    protected android.view.ViewStub viewTextStub;
    private android.view.LayoutInflater inflater;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout layoutStub;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.LinearLayout getFrameLinear() {
        return null;
    }
    
    protected final void setFrameLinear(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.view.ViewStub getViewTextStub() {
        return null;
    }
    
    protected final void setViewTextStub(@org.jetbrains.annotations.NotNull()
    android.view.ViewStub p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final android.widget.LinearLayout getLayoutStub() {
        return null;
    }
    
    protected final void setLayoutStub(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    private final void init(@androidx.annotation.LayoutRes()
    int layoutId) {
    }
    
    @android.annotation.SuppressLint(value = {"CustomViewStyleable"})
    private final void applyAttributes(android.util.AttributeSet attrs) {
    }
    
    private final void setCustomWidgets(@androidx.annotation.LayoutRes()
    int widgetIdBottom, @androidx.annotation.LayoutRes()
    int widgetIdTop) {
    }
    
    private final void setLinkPreviewWidget(@androidx.annotation.LayoutRes()
    int linkPreviewWidget) {
    }
    
    protected abstract void setLinearSide();
    
    protected abstract void setTextLayout();
    
    public MessageTextView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        super(null);
    }
}