package com.mart.onestopkitchen.utils.wifi;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b%\u0018\u00002\u00020\u0001B\u0089\u0001\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u000eJ\b\u0010\'\u001a\u00020\u0003H\u0016R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0010\"\u0004\b\u0014\u0010\u0012R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0010\"\u0004\b\u0016\u0010\u0012R\u001c\u0010\f\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0010\"\u0004\b\u0018\u0010\u0012R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0010\"\u0004\b\u001a\u0010\u0012R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0010\"\u0004\b\u001c\u0010\u0012R\u001c\u0010\b\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0010\"\u0004\b\u001e\u0010\u0012R\u001c\u0010\t\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0010\"\u0004\b \u0010\u0012R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u0010\"\u0004\b\"\u0010\u0012R\u001c\u0010\r\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0010\"\u0004\b$\u0010\u0012R\u001c\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0010\"\u0004\b&\u0010\u0012\u00a8\u0006("}, d2 = {"Lcom/mart/onestopkitchen/utils/wifi/WifiModel;", "", "CenterChannel", "", "Bssid", "PrimaryChannel", "PrimaryFrequency", "CenterFrequency", "Security", "Ssid", "Width", "Strength", "Distance", "Timestamp", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBssid", "()Ljava/lang/String;", "setBssid", "(Ljava/lang/String;)V", "getCenterChannel", "setCenterChannel", "getCenterFrequency", "setCenterFrequency", "getDistance", "setDistance", "getPrimaryChannel", "setPrimaryChannel", "getPrimaryFrequency", "setPrimaryFrequency", "getSecurity", "setSecurity", "getSsid", "setSsid", "getStrength", "setStrength", "getTimestamp", "setTimestamp", "getWidth", "setWidth", "toString", "app_stagingRelease"})
public final class WifiModel {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String CenterChannel;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String Bssid;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String PrimaryChannel;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String PrimaryFrequency;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String CenterFrequency;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String Security;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String Ssid;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String Width;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String Strength;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String Distance;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String Timestamp;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCenterChannel() {
        return null;
    }
    
    public final void setCenterChannel(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBssid() {
        return null;
    }
    
    public final void setBssid(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPrimaryChannel() {
        return null;
    }
    
    public final void setPrimaryChannel(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPrimaryFrequency() {
        return null;
    }
    
    public final void setPrimaryFrequency(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCenterFrequency() {
        return null;
    }
    
    public final void setCenterFrequency(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSecurity() {
        return null;
    }
    
    public final void setSecurity(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSsid() {
        return null;
    }
    
    public final void setSsid(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWidth() {
        return null;
    }
    
    public final void setWidth(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStrength() {
        return null;
    }
    
    public final void setStrength(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDistance() {
        return null;
    }
    
    public final void setDistance(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTimestamp() {
        return null;
    }
    
    public final void setTimestamp(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public WifiModel(@org.jetbrains.annotations.Nullable()
    java.lang.String CenterChannel, @org.jetbrains.annotations.Nullable()
    java.lang.String Bssid, @org.jetbrains.annotations.Nullable()
    java.lang.String PrimaryChannel, @org.jetbrains.annotations.Nullable()
    java.lang.String PrimaryFrequency, @org.jetbrains.annotations.Nullable()
    java.lang.String CenterFrequency, @org.jetbrains.annotations.Nullable()
    java.lang.String Security, @org.jetbrains.annotations.Nullable()
    java.lang.String Ssid, @org.jetbrains.annotations.Nullable()
    java.lang.String Width, @org.jetbrains.annotations.Nullable()
    java.lang.String Strength, @org.jetbrains.annotations.Nullable()
    java.lang.String Distance, @org.jetbrains.annotations.Nullable()
    java.lang.String Timestamp) {
        super();
    }
    
    public WifiModel() {
        super();
    }
}