package com.mart.onestopkitchen.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00070\fH\u0002J\b\u0010\r\u001a\u00020\u000eH\u0007J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0010H\u0002J\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0012R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u0004\u00a8\u0006\u0013"}, d2 = {"Lcom/mart/onestopkitchen/utils/ConfirmOrderResource;", "", "request", "Lcom/mart/onestopkitchen/model/SaveTransactionStatusModel;", "(Lcom/mart/onestopkitchen/model/SaveTransactionStatusModel;)V", "data", "Landroidx/lifecycle/MutableLiveData;", "Lcom/mart/onestopkitchen/model/PaymentResponseModel;", "getRequest", "()Lcom/mart/onestopkitchen/model/SaveTransactionStatusModel;", "setRequest", "getObserver", "Lio/reactivex/observers/DisposableObserver;", "getPlaceOrder", "", "getPlaceorderObservable", "Lio/reactivex/Observable;", "getResponse", "Landroidx/lifecycle/LiveData;", "app_stagingRelease"})
public final class ConfirmOrderResource {
    private androidx.lifecycle.MutableLiveData<com.mart.onestopkitchen.model.PaymentResponseModel> data;
    @org.jetbrains.annotations.NotNull()
    private com.mart.onestopkitchen.model.SaveTransactionStatusModel request;
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.LiveData<com.mart.onestopkitchen.model.PaymentResponseModel> getResponse() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    public final void getPlaceOrder() {
    }
    
    private final io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.model.PaymentResponseModel> getObserver() {
        return null;
    }
    
    private final io.reactivex.Observable<com.mart.onestopkitchen.model.PaymentResponseModel> getPlaceorderObservable() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.model.SaveTransactionStatusModel getRequest() {
        return null;
    }
    
    public final void setRequest(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.model.SaveTransactionStatusModel p0) {
    }
    
    public ConfirmOrderResource(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.model.SaveTransactionStatusModel request) {
        super();
    }
}