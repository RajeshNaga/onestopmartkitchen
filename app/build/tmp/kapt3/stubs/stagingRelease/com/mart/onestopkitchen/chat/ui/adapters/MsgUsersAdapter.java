package com.mart.onestopkitchen.chat.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u0001:\u0001\u001eB\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0006J\b\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0013\u001a\u00020\u0011H\u0016J\"\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0013\u001a\u00020\u00112\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0006H\u0014J\u0010\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0006H\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u001f"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/MsgUsersAdapter;", "Landroid/widget/BaseAdapter;", "context", "Landroid/content/Context;", "userList", "", "Lcom/quickblox/users/model/QBUser;", "(Landroid/content/Context;Ljava/util/List;)V", "getContext", "()Landroid/content/Context;", "currentUser", "getUserList", "()Ljava/util/List;", "addUserToUserList", "", "user", "getCount", "", "getItem", "position", "getItemId", "", "getView", "Landroid/view/View;", "convertView", "parent", "Landroid/view/ViewGroup;", "isAvailableForSelection", "", "isUserMe", "ViewHolder", "app_stagingRelease"})
public class MsgUsersAdapter extends android.widget.BaseAdapter {
    private com.quickblox.users.model.QBUser currentUser;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.quickblox.users.model.QBUser> userList = null;
    
    public final void addUserToUserList(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser user) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View getView(int position, @org.jetbrains.annotations.Nullable()
    android.view.View convertView, @org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent) {
        return null;
    }
    
    @java.lang.Override()
    public long getItemId(int position) {
        return 0L;
    }
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.quickblox.users.model.QBUser getItem(int position) {
        return null;
    }
    
    protected final boolean isUserMe(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser user) {
        return false;
    }
    
    protected boolean isAvailableForSelection(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser user) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.quickblox.users.model.QBUser> getUserList() {
        return null;
    }
    
    public MsgUsersAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.quickblox.users.model.QBUser> userList) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001a\u0010!\u001a\u00020\"X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&\u00a8\u0006\'"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/MsgUsersAdapter$ViewHolder;", "", "()V", "linearLayoutRoot", "Landroid/widget/LinearLayout;", "getLinearLayoutRoot", "()Landroid/widget/LinearLayout;", "setLinearLayoutRoot", "(Landroid/widget/LinearLayout;)V", "loginTextView", "Landroid/widget/TextView;", "getLoginTextView", "()Landroid/widget/TextView;", "setLoginTextView", "(Landroid/widget/TextView;)V", "opponentIcon", "Landroid/widget/ImageView;", "getOpponentIcon", "()Landroid/widget/ImageView;", "setOpponentIcon", "(Landroid/widget/ImageView;)V", "status", "Lde/hdodenhof/circleimageview/CircleImageView;", "getStatus", "()Lde/hdodenhof/circleimageview/CircleImageView;", "setStatus", "(Lde/hdodenhof/circleimageview/CircleImageView;)V", "tvOpponentsStatus", "Lcom/mart/onestopkitchen/utils/CustomTextView;", "getTvOpponentsStatus", "()Lcom/mart/onestopkitchen/utils/CustomTextView;", "setTvOpponentsStatus", "(Lcom/mart/onestopkitchen/utils/CustomTextView;)V", "userCheckBox", "Landroid/widget/CheckBox;", "getUserCheckBox", "()Landroid/widget/CheckBox;", "setUserCheckBox", "(Landroid/widget/CheckBox;)V", "app_stagingRelease"})
    public static final class ViewHolder {
        @org.jetbrains.annotations.NotNull()
        public android.widget.ImageView opponentIcon;
        @org.jetbrains.annotations.NotNull()
        public android.widget.TextView loginTextView;
        @org.jetbrains.annotations.NotNull()
        public android.widget.CheckBox userCheckBox;
        @org.jetbrains.annotations.NotNull()
        public android.widget.LinearLayout linearLayoutRoot;
        @org.jetbrains.annotations.NotNull()
        public de.hdodenhof.circleimageview.CircleImageView status;
        @org.jetbrains.annotations.NotNull()
        public com.mart.onestopkitchen.utils.CustomTextView tvOpponentsStatus;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getOpponentIcon() {
            return null;
        }
        
        public final void setOpponentIcon(@org.jetbrains.annotations.NotNull()
        android.widget.ImageView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getLoginTextView() {
            return null;
        }
        
        public final void setLoginTextView(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.CheckBox getUserCheckBox() {
            return null;
        }
        
        public final void setUserCheckBox(@org.jetbrains.annotations.NotNull()
        android.widget.CheckBox p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getLinearLayoutRoot() {
            return null;
        }
        
        public final void setLinearLayoutRoot(@org.jetbrains.annotations.NotNull()
        android.widget.LinearLayout p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final de.hdodenhof.circleimageview.CircleImageView getStatus() {
            return null;
        }
        
        public final void setStatus(@org.jetbrains.annotations.NotNull()
        de.hdodenhof.circleimageview.CircleImageView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.CustomTextView getTvOpponentsStatus() {
            return null;
        }
        
        public final void setTvOpponentsStatus(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.utils.CustomTextView p0) {
        }
        
        public ViewHolder() {
            super();
        }
    }
}