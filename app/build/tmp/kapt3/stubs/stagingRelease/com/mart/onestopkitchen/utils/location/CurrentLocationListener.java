package com.mart.onestopkitchen.utils.location;

import java.lang.System;

@android.annotation.SuppressLint(value = {"ALL"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0007\u0018\u0000 \u00172\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0017B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0014H\u0014J\b\u0010\u0016\u001a\u00020\u0014H\u0014R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0005R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/mart/onestopkitchen/utils/location/CurrentLocationListener;", "Landroidx/lifecycle/LiveData;", "Landroid/location/Location;", "appContext", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getAppContext", "()Landroid/content/Context;", "setAppContext", "mFusedLocationClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "mLocationCallback", "Lcom/google/android/gms/location/LocationCallback;", "getMLocationCallback$app_stagingRelease", "()Lcom/google/android/gms/location/LocationCallback;", "setMLocationCallback$app_stagingRelease", "(Lcom/google/android/gms/location/LocationCallback;)V", "mLocationRequest", "Lcom/google/android/gms/location/LocationRequest;", "createLocationRequest", "", "onActive", "onInactive", "Companion", "app_stagingRelease"})
public final class CurrentLocationListener extends androidx.lifecycle.LiveData<android.location.Location> {
    private com.google.android.gms.location.FusedLocationProviderClient mFusedLocationClient;
    private com.google.android.gms.location.LocationRequest mLocationRequest;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.location.LocationCallback mLocationCallback;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context appContext;
    private static com.mart.onestopkitchen.utils.location.CurrentLocationListener instance;
    public static final com.mart.onestopkitchen.utils.location.CurrentLocationListener.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.location.LocationCallback getMLocationCallback$app_stagingRelease() {
        return null;
    }
    
    public final void setMLocationCallback$app_stagingRelease(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.location.LocationCallback p0) {
    }
    
    private final void createLocationRequest() {
    }
    
    @java.lang.Override()
    protected void onActive() {
    }
    
    @java.lang.Override()
    protected void onInactive() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getAppContext() {
        return null;
    }
    
    public final void setAppContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    private CurrentLocationListener(android.content.Context appContext) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/mart/onestopkitchen/utils/location/CurrentLocationListener$Companion;", "", "()V", "instance", "Lcom/mart/onestopkitchen/utils/location/CurrentLocationListener;", "getInstance", "appContext", "Landroid/content/Context;", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.location.CurrentLocationListener getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context appContext) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}