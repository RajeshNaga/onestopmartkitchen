package com.mart.onestopkitchen.chat.qb;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\b\u0010\u0010\u001a\u00020\u000bH\u0016J\u0014\u0010\u0011\u001a\u00020\u000b2\n\u0010\u0012\u001a\u00060\u0013j\u0002`\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0014\u0010\u0018\u001a\u00020\u000b2\n\u0010\u0019\u001a\u00060\u0013j\u0002`\u0014H\u0016J\b\u0010\u001a\u001a\u00020\u000bH\u0016R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/mart/onestopkitchen/chat/qb/VerboseQbChatConnectionListener;", "Lorg/jivesoftware/smack/ConnectionListener;", "rootView", "Landroid/view/View;", "(Landroid/view/View;)V", "TAG", "", "kotlin.jvm.PlatformType", "snackbar", "Lcom/google/android/material/snackbar/Snackbar;", "authenticated", "", "connection", "Lorg/jivesoftware/smack/XMPPConnection;", "", "connected", "connectionClosed", "connectionClosedOnError", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "reconnectingIn", "seconds", "", "reconnectionFailed", "error", "reconnectionSuccessful", "app_stagingRelease"})
public class VerboseQbChatConnectionListener implements org.jivesoftware.smack.ConnectionListener {
    private final java.lang.String TAG = null;
    private com.google.android.material.snackbar.Snackbar snackbar;
    private final android.view.View rootView = null;
    
    @java.lang.Override()
    public void connected(@org.jetbrains.annotations.NotNull()
    org.jivesoftware.smack.XMPPConnection connection) {
    }
    
    @java.lang.Override()
    public void authenticated(@org.jetbrains.annotations.NotNull()
    org.jivesoftware.smack.XMPPConnection connection, boolean authenticated) {
    }
    
    @java.lang.Override()
    public void connectionClosed() {
    }
    
    @java.lang.Override()
    public void connectionClosedOnError(@org.jetbrains.annotations.NotNull()
    java.lang.Exception e) {
    }
    
    @java.lang.Override()
    public void reconnectingIn(int seconds) {
    }
    
    @java.lang.Override()
    public void reconnectionSuccessful() {
    }
    
    @java.lang.Override()
    public void reconnectionFailed(@org.jetbrains.annotations.NotNull()
    java.lang.Exception error) {
    }
    
    public VerboseQbChatConnectionListener(@org.jetbrains.annotations.NotNull()
    android.view.View rootView) {
        super();
    }
}