package com.mart.onestopkitchen.table;

import java.lang.System;

/**
 * Created by Akash Garg on 18/01/2019.
 */
@androidx.room.Database(entities = {com.mart.onestopkitchen.model.creditcarddao.CardDetailModel.class}, version = 1, exportSchema = false)
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\'\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\b"}, d2 = {"Lcom/mart/onestopkitchen/table/AppDatabase;", "Landroidx/room/RoomDatabase;", "()V", "cardDetails", "Lcom/mart/onestopkitchen/model/creditcarddao/CreditCardDao;", "destroyInstance", "", "Companion", "app_stagingRelease"})
public abstract class AppDatabase extends androidx.room.RoomDatabase {
    private static com.mart.onestopkitchen.table.AppDatabase Instance;
    private static final java.lang.String DB_NAME = "mart.db";
    @org.jetbrains.annotations.NotNull()
    private static final androidx.room.migration.Migration mygration1_2 = null;
    @org.jetbrains.annotations.NotNull()
    private static final androidx.room.migration.Migration mygration2_3 = null;
    public static final com.mart.onestopkitchen.table.AppDatabase.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.mart.onestopkitchen.model.creditcarddao.CreditCardDao cardDetails();
    
    public final void destroyInstance() {
    }
    
    public AppDatabase() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\n\u00a8\u0006\u0010"}, d2 = {"Lcom/mart/onestopkitchen/table/AppDatabase$Companion;", "", "()V", "DB_NAME", "", "Instance", "Lcom/mart/onestopkitchen/table/AppDatabase;", "mygration1_2", "Landroidx/room/migration/Migration;", "getMygration1_2", "()Landroidx/room/migration/Migration;", "mygration2_3", "getMygration2_3", "getInstance", "context", "Landroid/content/Context;", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.table.AppDatabase getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.room.migration.Migration getMygration1_2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.room.migration.Migration getMygration2_3() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}