package com.mart.onestopkitchen.utils;

import java.lang.System;

/**
 * Created by Akash Garg 01/10/2018
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0017\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001b\u001a\u00020\u001cH\u0002J\b\u0010\u001d\u001a\u00020\u001cH\u0002J\b\u0010\u001e\u001a\u00020\u001cH\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001a\u0010\u000f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001a\u0010\u0012\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR\u001a\u0010\u0015\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR\u001a\u0010\u0018\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\b\u00a8\u0006 "}, d2 = {"Lcom/mart/onestopkitchen/utils/ApisModel;", "", "()V", "BASE_URL", "", "getBASE_URL", "()Ljava/lang/String;", "setBASE_URL", "(Ljava/lang/String;)V", "CICO_AGENT_LIST", "getCICO_AGENT_LIST", "setCICO_AGENT_LIST", "CONTACT_SYN", "getCONTACT_SYN", "setCONTACT_SYN", "NST_KEY", "getNST_KEY", "setNST_KEY", "NST_SECRET", "getNST_SECRET", "setNST_SECRET", "OK_DOLLAR_PROFILE", "getOK_DOLLAR_PROFILE", "setOK_DOLLAR_PROFILE", "OK_DOLLAR_URL", "getOK_DOLLAR_URL", "setOK_DOLLAR_URL", "initServer", "", "productionServer", "stagingServer", "Companion", "app_stagingRelease"})
public final class ApisModel {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String BASE_URL;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String OK_DOLLAR_PROFILE;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String CONTACT_SYN;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String CICO_AGENT_LIST;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String NST_KEY;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String NST_SECRET;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String OK_DOLLAR_URL;
    private static com.mart.onestopkitchen.utils.ApisModel Instance;
    public static final com.mart.onestopkitchen.utils.ApisModel.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBASE_URL() {
        return null;
    }
    
    public final void setBASE_URL(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOK_DOLLAR_PROFILE() {
        return null;
    }
    
    public final void setOK_DOLLAR_PROFILE(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCONTACT_SYN() {
        return null;
    }
    
    public final void setCONTACT_SYN(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCICO_AGENT_LIST() {
        return null;
    }
    
    public final void setCICO_AGENT_LIST(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getNST_KEY() {
        return null;
    }
    
    public final void setNST_KEY(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getNST_SECRET() {
        return null;
    }
    
    public final void setNST_SECRET(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOK_DOLLAR_URL() {
        return null;
    }
    
    public final void setOK_DOLLAR_URL(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    private final void initServer() {
    }
    
    private final void stagingServer() {
    }
    
    private final void productionServer() {
    }
    
    public ApisModel() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/utils/ApisModel$Companion;", "", "()V", "Instance", "Lcom/mart/onestopkitchen/utils/ApisModel;", "getInstance", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.ApisModel getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}