package com.mart.onestopkitchen.chat.views;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0014J\b\u0010\t\u001a\u00020\bH\u0014\u00a8\u0006\n"}, d2 = {"Lcom/mart/onestopkitchen/chat/views/MessageTextViewRight;", "Lcom/mart/onestopkitchen/chat/views/MessageTextView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "setLinearSide", "", "setTextLayout", "app_stagingRelease"})
public final class MessageTextViewRight extends com.mart.onestopkitchen.chat.views.MessageTextView {
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void setLinearSide() {
    }
    
    @java.lang.Override()
    protected void setTextLayout() {
    }
    
    public MessageTextViewRight(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        super(null, null);
    }
}