package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0002J:\u0010\f\u001a\u00020\r2\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\b2\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000b2\b\b\u0001\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0003J@\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\b2\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000b2\b\b\u0001\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0010\u00a8\u0006\u0017"}, d2 = {"Lcom/mart/onestopkitchen/chat/utils/NotificationUtils;", "", "()V", "buildContentIntent", "Landroid/app/PendingIntent;", "context", "Landroid/content/Context;", "activityClass", "Ljava/lang/Class;", "Landroid/app/Activity;", "message", "", "buildNotification", "Landroid/app/Notification;", "title", "icon", "", "createChannelIfNotExist", "", "notificationManager", "Landroid/app/NotificationManager;", "showNotification", "notificationId", "app_stagingRelease"})
public final class NotificationUtils {
    public static final com.mart.onestopkitchen.chat.utils.NotificationUtils INSTANCE = null;
    
    public final void showNotification(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.Class<? extends android.app.Activity> activityClass, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @androidx.annotation.DrawableRes()
    int icon, int notificationId) {
    }
    
    @androidx.annotation.RequiresApi(api = android.os.Build.VERSION_CODES.O)
    private final void createChannelIfNotExist(android.app.NotificationManager notificationManager) {
    }
    
    private final android.app.Notification buildNotification(android.content.Context context, java.lang.Class<? extends android.app.Activity> activityClass, java.lang.String title, java.lang.String message, @androidx.annotation.DrawableRes()
    int icon) {
        return null;
    }
    
    private final android.app.PendingIntent buildContentIntent(android.content.Context context, java.lang.Class<? extends android.app.Activity> activityClass, java.lang.String message) {
        return null;
    }
    
    private NotificationUtils() {
        super();
    }
}