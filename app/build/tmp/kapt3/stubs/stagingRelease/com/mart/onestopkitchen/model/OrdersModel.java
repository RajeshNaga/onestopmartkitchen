package com.mart.onestopkitchen.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b+\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B\u00bf\u0001\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014\u00a2\u0006\u0002\u0010\u0016J\u000b\u0010.\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00103\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u00104\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014H\u00c6\u0003J\u000b\u00105\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u00106\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010\u001cJ\u0010\u00107\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010\u001cJ\u000b\u00108\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u00109\u001a\u0004\u0018\u00010\nH\u00c6\u0003\u00a2\u0006\u0002\u0010*J\u000b\u0010:\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010<\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u00c8\u0001\u0010=\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014H\u00c6\u0001\u00a2\u0006\u0002\u0010>J\u0013\u0010?\u001a\u00020\n2\b\u0010@\u001a\u0004\u0018\u00010AH\u00d6\u0003J\t\u0010B\u001a\u00020\u0006H\u00d6\u0001J\t\u0010C\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0018R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0018R\u0015\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010\u001d\u001a\u0004\b\u001b\u0010\u001cR\"\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010\u001d\u001a\u0004\b\"\u0010\u001cR\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0018R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0018R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0018R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0018R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\u0018R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u0018R\u0015\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010+\u001a\u0004\b)\u0010*R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010\u0018R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010\u0018\u00a8\u0006D"}, d2 = {"Lcom/mart/onestopkitchen/model/OrdersModel;", "Lcom/mart/onestopkitchen/networking/BaseResponse;", "CustomOrderNumber", "", "ExpectedDeliveryDate", "OrderId", "", "Id", "CreatedOn", "PickUpInStore", "", "OrderStatus", "PaymentMethod", "PaymentMethodStatus", "OrderSubtotal", "OrderShipping", "Tax", "RedeemedRewardPoints", "OrderTotal", "Items", "", "Lcom/mart/onestopkitchen/model/PDFProductItem;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getCreatedOn", "()Ljava/lang/String;", "getCustomOrderNumber", "getExpectedDeliveryDate", "getId", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getItems", "()Ljava/util/List;", "setItems", "(Ljava/util/List;)V", "getOrderId", "getOrderShipping", "getOrderStatus", "getOrderSubtotal", "getOrderTotal", "getPaymentMethod", "getPaymentMethodStatus", "getPickUpInStore", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getRedeemedRewardPoints", "getTax", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/mart/onestopkitchen/model/OrdersModel;", "equals", "other", "", "hashCode", "toString", "app_stagingRelease"})
public final class OrdersModel extends com.mart.onestopkitchen.networking.BaseResponse {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String CustomOrderNumber = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String ExpectedDeliveryDate = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer OrderId = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer Id = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String CreatedOn = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Boolean PickUpInStore = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String OrderStatus = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String PaymentMethod = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String PaymentMethodStatus = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String OrderSubtotal = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String OrderShipping = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String Tax = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String RedeemedRewardPoints = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String OrderTotal = null;
    @org.jetbrains.annotations.Nullable()
    private java.util.List<? extends com.mart.onestopkitchen.model.PDFProductItem> Items;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCustomOrderNumber() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExpectedDeliveryDate() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getOrderId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCreatedOn() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getPickUpInStore() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrderStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPaymentMethod() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPaymentMethodStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrderSubtotal() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrderShipping() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTax() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRedeemedRewardPoints() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrderTotal() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.mart.onestopkitchen.model.PDFProductItem> getItems() {
        return null;
    }
    
    public final void setItems(@org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.mart.onestopkitchen.model.PDFProductItem> p0) {
    }
    
    public OrdersModel(@org.jetbrains.annotations.Nullable()
    java.lang.String CustomOrderNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String ExpectedDeliveryDate, @org.jetbrains.annotations.Nullable()
    java.lang.Integer OrderId, @org.jetbrains.annotations.Nullable()
    java.lang.Integer Id, @org.jetbrains.annotations.Nullable()
    java.lang.String CreatedOn, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean PickUpInStore, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String PaymentMethod, @org.jetbrains.annotations.Nullable()
    java.lang.String PaymentMethodStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderSubtotal, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderShipping, @org.jetbrains.annotations.Nullable()
    java.lang.String Tax, @org.jetbrains.annotations.Nullable()
    java.lang.String RedeemedRewardPoints, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderTotal, @org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.mart.onestopkitchen.model.PDFProductItem> Items) {
        super();
    }
    
    public OrdersModel() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.mart.onestopkitchen.model.PDFProductItem> component15() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.model.OrdersModel copy(@org.jetbrains.annotations.Nullable()
    java.lang.String CustomOrderNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String ExpectedDeliveryDate, @org.jetbrains.annotations.Nullable()
    java.lang.Integer OrderId, @org.jetbrains.annotations.Nullable()
    java.lang.Integer Id, @org.jetbrains.annotations.Nullable()
    java.lang.String CreatedOn, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean PickUpInStore, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String PaymentMethod, @org.jetbrains.annotations.Nullable()
    java.lang.String PaymentMethodStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderSubtotal, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderShipping, @org.jetbrains.annotations.Nullable()
    java.lang.String Tax, @org.jetbrains.annotations.Nullable()
    java.lang.String RedeemedRewardPoints, @org.jetbrains.annotations.Nullable()
    java.lang.String OrderTotal, @org.jetbrains.annotations.Nullable()
    java.util.List<? extends com.mart.onestopkitchen.model.PDFProductItem> Items) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}