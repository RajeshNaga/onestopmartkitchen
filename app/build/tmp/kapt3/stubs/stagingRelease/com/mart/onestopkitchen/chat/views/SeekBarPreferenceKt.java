package com.mart.onestopkitchen.chat.views;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"ANDROID_NS", "", "SEEKBAR_NS", "app_stagingRelease"})
public final class SeekBarPreferenceKt {
    private static final java.lang.String ANDROID_NS = "http://schemas.android.com/apk/res/android";
    private static final java.lang.String SEEKBAR_NS = "http://schemas.android.com/apk/res-auto";
}