package com.mart.onestopkitchen.utils.location;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\t\u0018\u0000 \'2\u00020\u0001:\u0001\'B\u0005\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u0004H\u0002J\"\u0010\u001c\u001a\u00020\u001d2\b\u0010\f\u001a\u0004\u0018\u00010\r2\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00040\u001fH\u0007J\u000e\u0010 \u001a\u00020\u001d2\u0006\u0010!\u001a\u00020\u0006J\u0010\u0010\"\u001a\u00020\u001d2\u0006\u0010!\u001a\u00020\u0006H\u0002J\u0010\u0010#\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\bH\u0002J\u0010\u0010%\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\bH\u0002J\u0010\u0010&\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/mart/onestopkitchen/utils/location/GetAutoLocationDetails;", "", "()V", "activeLocale", "", "dataIntent", "Landroid/content/Intent;", "defaultAddress", "Landroid/location/Address;", "getDefaultAddress", "()Landroid/location/Address;", "defaultTag", "mCallback", "Lcom/mart/onestopkitchen/utils/location/OnLocationFetched;", "mContext", "Landroid/content/Context;", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "showProgress", "", "getAddress", "latitude", "", "longitude", "streetAddress", "locale", "getCurrentLocationDetails", "", "mVarargs", "", "initialize", "intent", "setLocation", "validateCity", "address", "validateState", "validateStreet", "Companion", "app_stagingRelease"})
public final class GetAutoLocationDetails {
    private java.lang.String activeLocale;
    private java.lang.String defaultTag;
    private com.mart.onestopkitchen.utils.location.OnLocationFetched mCallback;
    private android.content.Intent dataIntent;
    private boolean showProgress;
    @org.jetbrains.annotations.Nullable()
    private android.content.Context mContext;
    @android.annotation.SuppressLint(value = {"StaticFieldLeak"})
    private static com.mart.onestopkitchen.utils.location.GetAutoLocationDetails INSTANCE;
    public static final com.mart.onestopkitchen.utils.location.GetAutoLocationDetails.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    private final android.location.Address getDefaultAddress() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    public final void getCurrentLocationDetails(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.utils.location.OnLocationFetched mCallback, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> mVarargs) {
    }
    
    public final void initialize(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
    }
    
    private final android.content.Intent getAddress(double latitude, double longitude, java.lang.String streetAddress, java.lang.String locale) {
        return null;
    }
    
    private final java.lang.String validateCity(android.location.Address address) {
        return null;
    }
    
    private final java.lang.String validateState(android.location.Address address) {
        return null;
    }
    
    private final java.lang.String validateStreet(android.location.Address address) {
        return null;
    }
    
    private final void setLocation(android.content.Intent intent) {
    }
    
    public GetAutoLocationDetails() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/mart/onestopkitchen/utils/location/GetAutoLocationDetails$Companion;", "", "()V", "INSTANCE", "Lcom/mart/onestopkitchen/utils/location/GetAutoLocationDetails;", "getInstance", "mContext", "Landroid/content/Context;", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.location.GetAutoLocationDetails getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context mContext) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}