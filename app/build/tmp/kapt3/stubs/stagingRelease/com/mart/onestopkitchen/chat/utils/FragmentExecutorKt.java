package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a&\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t\u001a&\u0010\n\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t\u00a8\u0006\u000b"}, d2 = {"addFragment", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "containerId", "", "fragment", "Landroidx/fragment/app/Fragment;", "tag", "", "addFragmentWithBackStack", "app_stagingRelease"})
public final class FragmentExecutorKt {
    
    public static final void addFragment(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager fragmentManager, int containerId, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
    
    public static final void addFragmentWithBackStack(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager fragmentManager, int containerId, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
}