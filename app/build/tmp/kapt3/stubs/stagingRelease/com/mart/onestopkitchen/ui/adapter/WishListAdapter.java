package com.mart.onestopkitchen.ui.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u0000 #2\u00020\u0001:\u0003#$%B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\b\u0010\u0012\u001a\u00020\u0011H\u0016J\u0018\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0018\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0010\u001a\u00020\u0011H\u0017J\u0018\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0011H\u0016J\u0018\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0016\u0010\u001f\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter;", "Lcom/mart/onestopkitchen/ui/adapter/CartAdapter;", "context", "Landroid/content/Context;", "productsList", "", "Lcom/mart/onestopkitchen/model/CartProduct;", "fragment", "Landroidx/fragment/app/Fragment;", "preferenceService", "Lcom/mart/onestopkitchen/service/PreferenceService;", "clickListener", "Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$OnProductSelectListener;", "(Landroid/content/Context;Ljava/util/List;Landroidx/fragment/app/Fragment;Lcom/mart/onestopkitchen/service/PreferenceService;Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$OnProductSelectListener;)V", "callAddToCartWebService", "", "position", "", "getItemCount", "onAddtoCartButtonClicked", "addToCartBtn", "Landroid/widget/ImageView;", "onBindViewHolder", "bindViewHolder", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "onTrashClicked", "itemview", "updateCartItem", "key", "", "value", "Companion", "OnProductSelectListener", "ProductSummaryHolder", "app_stagingRelease"})
public final class WishListAdapter extends com.mart.onestopkitchen.ui.adapter.CartAdapter {
    private final com.mart.onestopkitchen.ui.adapter.WishListAdapter.OnProductSelectListener clickListener = null;
    @org.jetbrains.annotations.Nullable()
    private static com.mart.onestopkitchen.ui.adapter.WishListAdapter.OnProductSelectListener mClickListener;
    public static final com.mart.onestopkitchen.ui.adapter.WishListAdapter.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.recyclerview.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder bindViewHolder, int position) {
    }
    
    private final void onAddtoCartButtonClicked(android.widget.ImageView addToCartBtn, int position) {
    }
    
    private final void callAddToCartWebService(int position) {
    }
    
    private final void onTrashClicked(android.widget.ImageView itemview, int position) {
    }
    
    public final void updateCartItem(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public WishListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mart.onestopkitchen.model.CartProduct> productsList, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.service.PreferenceService preferenceService, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.ui.adapter.WishListAdapter.OnProductSelectListener clickListener) {
        super(null, null, null, null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$ProductSummaryHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindData", "", "data", "Lcom/mart/onestopkitchen/model/CartProduct;", "app_stagingRelease"})
    public static final class ProductSummaryHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public final void bindData(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.model.CartProduct data) {
        }
        
        public ProductSummaryHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$OnProductSelectListener;", "", "onItemSelect", "", "pId", "", "name", "", "app_stagingRelease"})
    public static abstract interface OnProductSelectListener {
        
        public abstract void onItemSelect(int pId, @org.jetbrains.annotations.NotNull()
        java.lang.String name);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$Companion;", "", "()V", "mClickListener", "Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$OnProductSelectListener;", "getMClickListener", "()Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$OnProductSelectListener;", "setMClickListener", "(Lcom/mart/onestopkitchen/ui/adapter/WishListAdapter$OnProductSelectListener;)V", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.mart.onestopkitchen.ui.adapter.WishListAdapter.OnProductSelectListener getMClickListener() {
            return null;
        }
        
        public final void setMClickListener(@org.jetbrains.annotations.Nullable()
        com.mart.onestopkitchen.ui.adapter.WishListAdapter.OnProductSelectListener p0) {
        }
        
        private Companion() {
            super();
        }
    }
}