package com.mart.onestopkitchen.utils.location;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u001f\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0086\b\u00a8\u0006\u0003"}, d2 = {"dropBreadcrumb", "Lio/reactivex/Observable;", "T", "app_stagingRelease"})
public final class GetAutoLocationDetailsKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Object>io.reactivex.Observable<T> dropBreadcrumb(@org.jetbrains.annotations.NotNull()
    io.reactivex.Observable<T> $this$dropBreadcrumb) {
        return null;
    }
}