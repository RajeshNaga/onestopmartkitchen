package com.mart.onestopkitchen.ui.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002 !B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0014H\u0016J\u0018\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0014H\u0016J\u000e\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u001e\u001a\u00020\u001fR \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/HelpSupportAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/mart/onestopkitchen/ui/adapter/HelpSupportAdapter$MyHelpSupportViewHolder;", "mContext", "Landroid/content/Context;", "data", "", "Lcom/mart/onestopkitchen/model/HelpSupportDataItem;", "(Landroid/content/Context;Ljava/util/List;)V", "getData", "()Ljava/util/List;", "setData", "(Ljava/util/List;)V", "inflater", "Landroid/view/LayoutInflater;", "getMContext", "()Landroid/content/Context;", "mListener", "Lcom/mart/onestopkitchen/ui/adapter/HelpSupportAdapter$CallToHelpSupport;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setListener", "listener", "Lcom/mart/onestopkitchen/ui/fragment/helpandsupport/HelpAndSupportFragment;", "CallToHelpSupport", "MyHelpSupportViewHolder", "app_stagingRelease"})
public final class HelpSupportAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.mart.onestopkitchen.ui.adapter.HelpSupportAdapter.MyHelpSupportViewHolder> {
    private android.view.LayoutInflater inflater;
    private com.mart.onestopkitchen.ui.adapter.HelpSupportAdapter.CallToHelpSupport mListener;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context mContext = null;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.mart.onestopkitchen.model.HelpSupportDataItem> data;
    
    public final void setListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.ui.fragment.helpandsupport.HelpAndSupportFragment listener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.mart.onestopkitchen.ui.adapter.HelpSupportAdapter.MyHelpSupportViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.ui.adapter.HelpSupportAdapter.MyHelpSupportViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mart.onestopkitchen.model.HelpSupportDataItem> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.mart.onestopkitchen.model.HelpSupportDataItem> p0) {
    }
    
    public HelpSupportAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    java.util.List<com.mart.onestopkitchen.model.HelpSupportDataItem> data) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u000f"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/HelpSupportAdapter$MyHelpSupportViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "mContext", "Landroid/content/Context;", "(Landroid/view/View;Landroid/content/Context;)V", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "bind", "", "data", "Lcom/mart/onestopkitchen/model/HelpSupportDataItem;", "app_stagingRelease"})
    public static final class MyHelpSupportViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private android.content.Context mContext;
        
        public final void bind(@org.jetbrains.annotations.Nullable()
        com.mart.onestopkitchen.model.HelpSupportDataItem data) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Context getMContext() {
            return null;
        }
        
        public final void setMContext(@org.jetbrains.annotations.NotNull()
        android.content.Context p0) {
        }
        
        public MyHelpSupportViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView, @org.jetbrains.annotations.NotNull()
        android.content.Context mContext) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/HelpSupportAdapter$CallToHelpSupport;", "", "callToHelp", "", "data", "Lcom/mart/onestopkitchen/model/HelpSupportDataItem;", "app_stagingRelease"})
    public static abstract interface CallToHelpSupport {
        
        public abstract void callToHelp(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.model.HelpSupportDataItem data);
    }
}