package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000&\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u001a\u000e\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0003\u001a\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003\u001a\u000e\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003\u001a\u0010\u0010\u0010\u001a\u00020\r2\b\b\u0001\u0010\u0011\u001a\u00020\u0003\u001a\u0006\u0010\u0012\u001a\u00020\r\u001a\u0006\u0010\u0013\u001a\u00020\u0003\u001a\u000e\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082D\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\"\u001c\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u00078\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"COLOR_ALPHA", "", "COLOR_MAX_VALUE", "", "RANDOM_COLOR_END_RANGE", "RANDOM_COLOR_START_RANGE", "colorsMap", "Ljava/util/HashMap;", "random", "Ljava/util/Random;", "dpToPx", "dp", "getColorCircleDrawable", "Landroid/graphics/drawable/Drawable;", "colorPosition", "getColorCircleGrayDrawable", "getColoredCircleDrawable", "color", "getGreyCircleDrawable", "getRandomColor", "getRandomTextColorById", "senderId", "app_stagingRelease"})
public final class ResourceUtilsKt {
    private static final int RANDOM_COLOR_START_RANGE = 0;
    private static final int RANDOM_COLOR_END_RANGE = 9;
    private static final int COLOR_MAX_VALUE = 255;
    private static final float COLOR_ALPHA = 0.8F;
    @android.annotation.SuppressLint(value = {"UseSparseArrays"})
    private static final java.util.HashMap<java.lang.Integer, java.lang.Integer> colorsMap = null;
    private static final java.util.Random random = null;
    
    public static final int dpToPx(int dp) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.drawable.Drawable getColorCircleDrawable(int colorPosition) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.drawable.Drawable getColorCircleGrayDrawable(int colorPosition) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.drawable.Drawable getGreyCircleDrawable() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.drawable.Drawable getColoredCircleDrawable(@androidx.annotation.ColorInt()
    int color) {
        return null;
    }
    
    public static final int getRandomTextColorById(int senderId) {
        return 0;
    }
    
    public static final int getRandomColor() {
        return 0;
    }
}