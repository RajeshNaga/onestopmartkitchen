package com.mart.onestopkitchen.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000*\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\f\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u000e\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\u0001\u001a\u000e\u0010\t\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0001\u001a\u0016\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010\u00a8\u0006\u0011"}, d2 = {"calculateDistance", "", "mLocationNew", "Landroid/location/Location;", "mLocationOld", "unit", "", "deg2rad", "deg", "rad2deg", "rad", "showToast", "", "mMessage", "", "mContext", "Landroid/content/Context;", "app_stagingRelease"})
public final class FunctionsKt {
    
    public static final void showToast(@org.jetbrains.annotations.NotNull()
    java.lang.String mMessage, @org.jetbrains.annotations.NotNull()
    android.content.Context mContext) {
    }
    
    public static final double calculateDistance(@org.jetbrains.annotations.NotNull()
    android.location.Location mLocationNew, @org.jetbrains.annotations.NotNull()
    android.location.Location mLocationOld, char unit) {
        return 0.0;
    }
    
    public static final double deg2rad(double deg) {
        return 0.0;
    }
    
    public static final double rad2deg(double rad) {
        return 0.0;
    }
}