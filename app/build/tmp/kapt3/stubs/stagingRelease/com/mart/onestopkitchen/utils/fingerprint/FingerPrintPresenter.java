package com.mart.onestopkitchen.utils.fingerprint;

import java.lang.System;

/**
 * Created by Akash Garg on 17/01/2019.
 */
@androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.M)
@android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.M)
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u000eB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\bH\u0016J\b\u0010\n\u001a\u00020\bH\u0016J\b\u0010\u000b\u001a\u00020\bH\u0002J\b\u0010\f\u001a\u00020\rH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/mart/onestopkitchen/utils/fingerprint/FingerPrintPresenter;", "Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintResult;", "mContext", "Landroid/content/Context;", "fingerPrintBaseView", "Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintBaseView;", "(Landroid/content/Context;Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintBaseView;)V", "authenticationError", "", "authenticationFailure", "authenticationSuccess", "generateKey", "initCipher", "", "FingerprintException", "app_stagingRelease"})
public final class FingerPrintPresenter implements com.mart.onestopkitchen.utils.fingerprint.FingerprintResult {
    private final com.mart.onestopkitchen.utils.fingerprint.FingerprintBaseView fingerPrintBaseView = null;
    
    private final void generateKey() throws com.mart.onestopkitchen.utils.fingerprint.FingerPrintPresenter.FingerprintException {
    }
    
    private final boolean initCipher() {
        return false;
    }
    
    @java.lang.Override()
    public void authenticationSuccess() {
    }
    
    @java.lang.Override()
    public void authenticationFailure() {
    }
    
    @java.lang.Override()
    public void authenticationError() {
    }
    
    public FingerPrintPresenter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.utils.fingerprint.FingerprintBaseView fingerPrintBaseView) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0082\u0004\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0011\u0012\n\u0010\u0003\u001a\u00060\u0001j\u0002`\u0002\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/mart/onestopkitchen/utils/fingerprint/FingerPrintPresenter$FingerprintException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "e", "(Lcom/mart/onestopkitchen/utils/fingerprint/FingerPrintPresenter;Ljava/lang/Exception;)V", "app_stagingRelease"})
    final class FingerprintException extends java.lang.Exception {
        
        public FingerprintException(@org.jetbrains.annotations.NotNull()
        java.lang.Exception e) {
            super();
        }
    }
}