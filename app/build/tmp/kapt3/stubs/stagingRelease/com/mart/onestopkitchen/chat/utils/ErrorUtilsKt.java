package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u00000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a:\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0001\u0010\u000b\u001a\u00020\f2\u000e\u0010\r\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f2\b\b\u0001\u0010\u0010\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u0012\u001a2\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0001\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u00012\b\b\u0001\u0010\u0010\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u0012\u001a*\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u00012\b\b\u0001\u0010\u0010\u001a\u00020\f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u001a<\u0010\u0015\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0001\u0010\u0016\u001a\u00020\f2\u000e\u0010\r\u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u000f2\b\b\u0001\u0010\u0010\u001a\u00020\f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u001a6\u0010\u0015\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0001\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u00012\b\b\u0001\u0010\u0010\u001a\u00020\f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002\u001a,\u0010\u0015\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u00012\b\b\u0001\u0010\u0010\u001a\u00020\f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\u0005\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"EMPTY_STRING", "", "NO_CONNECTION_ERROR", "kotlin.jvm.PlatformType", "NO_INTERNET_CONNECTION", "NO_RESPONSE_TIMEOUT", "NO_SERVER_CONNECTION", "showErrorSnackbar", "Lcom/google/android/material/snackbar/Snackbar;", "view", "Landroid/view/View;", "errorMessage", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "actionLabel", "clickListener", "Landroid/view/View$OnClickListener;", "error", "message", "showSnackbar", "errorMessageResource", "app_stagingRelease"})
public final class ErrorUtilsKt {
    private static final java.lang.String EMPTY_STRING = "";
    private static final java.lang.String NO_CONNECTION_ERROR = null;
    private static final java.lang.String NO_RESPONSE_TIMEOUT = null;
    private static final java.lang.String NO_INTERNET_CONNECTION = null;
    private static final java.lang.String NO_SERVER_CONNECTION = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.snackbar.Snackbar showErrorSnackbar(@org.jetbrains.annotations.NotNull()
    android.view.View view, @androidx.annotation.StringRes()
    int errorMessage, @org.jetbrains.annotations.Nullable()
    java.lang.Exception e, @androidx.annotation.StringRes()
    int actionLabel, @org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener clickListener) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.snackbar.Snackbar showErrorSnackbar(@org.jetbrains.annotations.NotNull()
    android.view.View view, @androidx.annotation.StringRes()
    int errorMessage, @org.jetbrains.annotations.NotNull()
    java.lang.String error, @androidx.annotation.StringRes()
    int actionLabel, @org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener clickListener) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.snackbar.Snackbar showErrorSnackbar(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @androidx.annotation.StringRes()
    int actionLabel, @org.jetbrains.annotations.Nullable()
    android.view.View.OnClickListener clickListener) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.google.android.material.snackbar.Snackbar showSnackbar(@org.jetbrains.annotations.NotNull()
    android.view.View view, @androidx.annotation.StringRes()
    int errorMessageResource, @org.jetbrains.annotations.Nullable()
    java.lang.Exception e, @androidx.annotation.StringRes()
    int actionLabel, @org.jetbrains.annotations.Nullable()
    android.view.View.OnClickListener clickListener) {
        return null;
    }
    
    private static final com.google.android.material.snackbar.Snackbar showSnackbar(android.view.View view, @androidx.annotation.StringRes()
    int errorMessage, java.lang.String error, @androidx.annotation.StringRes()
    int actionLabel, android.view.View.OnClickListener clickListener) {
        return null;
    }
    
    private static final com.google.android.material.snackbar.Snackbar showSnackbar(android.view.View view, java.lang.String message, @androidx.annotation.StringRes()
    int actionLabel, android.view.View.OnClickListener clickListener) {
        return null;
    }
}