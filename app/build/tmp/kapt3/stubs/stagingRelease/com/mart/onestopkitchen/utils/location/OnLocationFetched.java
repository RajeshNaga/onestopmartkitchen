package com.mart.onestopkitchen.utils.location;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J8\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0005H&J8\u0010\f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0005H&\u00a8\u0006\r"}, d2 = {"Lcom/mart/onestopkitchen/utils/location/OnLocationFetched;", "", "onLocationBurmese", "", "roadName", "", "city", "state", "fullAddress", "address", "Landroid/location/Address;", "locale", "onLocationEnglish", "app_stagingRelease"})
public abstract interface OnLocationFetched {
    
    public abstract void onLocationEnglish(@org.jetbrains.annotations.NotNull()
    java.lang.String roadName, @org.jetbrains.annotations.NotNull()
    java.lang.String city, @org.jetbrains.annotations.NotNull()
    java.lang.String state, @org.jetbrains.annotations.NotNull()
    java.lang.String fullAddress, @org.jetbrains.annotations.NotNull()
    android.location.Address address, @org.jetbrains.annotations.NotNull()
    java.lang.String locale);
    
    public abstract void onLocationBurmese(@org.jetbrains.annotations.NotNull()
    java.lang.String roadName, @org.jetbrains.annotations.NotNull()
    java.lang.String city, @org.jetbrains.annotations.NotNull()
    java.lang.String state, @org.jetbrains.annotations.NotNull()
    java.lang.String fullAddress, @org.jetbrains.annotations.NotNull()
    android.location.Address address, @org.jetbrains.annotations.NotNull()
    java.lang.String locale);
}