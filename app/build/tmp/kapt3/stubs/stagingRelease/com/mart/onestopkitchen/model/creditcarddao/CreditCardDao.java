package com.mart.onestopkitchen.model.creditcarddao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0003H\'J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u0003H\'\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/model/creditcarddao/CreditCardDao;", "", "getCardDetails", "Lcom/mart/onestopkitchen/model/creditcarddao/CardDetailModel;", "insertCardDetails", "", "card", "updateCardDetails", "", "app_stagingRelease"})
public abstract interface CreditCardDao {
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void insertCardDetails(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.model.creditcarddao.CardDetailModel card);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "Select * From cardDetailTable order by userId desc Limit 1 ")
    public abstract com.mart.onestopkitchen.model.creditcarddao.CardDetailModel getCardDetails();
    
    @androidx.room.Update()
    public abstract int updateCardDetails(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.model.creditcarddao.CardDetailModel card);
}