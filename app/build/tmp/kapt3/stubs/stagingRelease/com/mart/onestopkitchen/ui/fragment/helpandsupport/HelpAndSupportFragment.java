package com.mart.onestopkitchen.ui.fragment.helpandsupport;

import java.lang.System;

/**
 * Created By Akash Garg-14-Feb-2019
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u000e\u0018\u0000 52\u00020\u00012\u00020\u0002:\u00015B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u0005H\u0002J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0005H\u0003J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0005H\u0002J&\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u000e\u0010\u001f\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020!J+\u0010\"\u001a\u00020\u000e2\u0006\u0010#\u001a\u00020$2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050&2\u0006\u0010\'\u001a\u00020(H\u0016\u00a2\u0006\u0002\u0010)J\b\u0010*\u001a\u00020\u000eH\u0016J\u001a\u0010+\u001a\u00020\u000e2\u0006\u0010,\u001a\u00020\u00182\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u0010\u0010-\u001a\u00020\u000e2\u0006\u0010.\u001a\u00020\u0005H\u0002J\u0010\u0010/\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u0005H\u0002J\u0010\u00100\u001a\u00020\u000e2\u0006\u00101\u001a\u00020\u0005H\u0002J\u0018\u00102\u001a\u00020\u000e2\u0006\u00103\u001a\u00020\u00052\u0006\u00104\u001a\u00020\u0005H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00066"}, d2 = {"Lcom/mart/onestopkitchen/ui/fragment/helpandsupport/HelpAndSupportFragment;", "Lcom/mart/onestopkitchen/ui/fragment/BaseFragment;", "Lcom/mart/onestopkitchen/ui/adapter/HelpSupportAdapter$CallToHelpSupport;", "()V", "TAG", "", "phoneNumber", "getPhoneNumber", "()Ljava/lang/String;", "setPhoneNumber", "(Ljava/lang/String;)V", "user", "Lcom/quickblox/users/model/QBUser;", "callGetHelpSupportWebservice", "", "callPhone", "callToHelp", "data", "Lcom/mart/onestopkitchen/model/HelpSupportDataItem;", "callUsingPhone", "number", "callUsingViber", "dialNumber", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onEvent", "response", "Lcom/mart/onestopkitchen/model/HelpSupportModel;", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onViewCreated", "view", "onWhatsApp", "whatsappNumber", "onWhatsAppClick", "sendEmail", "mail", "sendSMS", "phoneNo", "msg", "Companion", "app_stagingRelease"})
public final class HelpAndSupportFragment extends com.mart.onestopkitchen.ui.fragment.BaseFragment implements com.mart.onestopkitchen.ui.adapter.HelpSupportAdapter.CallToHelpSupport {
    private final java.lang.String TAG = "HelpAndSupportFragment";
    private com.quickblox.users.model.QBUser user;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String phoneNumber;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;
    public static final com.mart.onestopkitchen.ui.fragment.helpandsupport.HelpAndSupportFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPhoneNumber() {
        return null;
    }
    
    public final void setPhoneNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void callGetHelpSupportWebservice() {
    }
    
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.model.HelpSupportModel response) {
    }
    
    @java.lang.Override()
    public void callToHelp(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.model.HelpSupportDataItem data) {
    }
    
    private final void onWhatsAppClick(java.lang.String number) {
    }
    
    private final void onWhatsApp(java.lang.String whatsappNumber) {
    }
    
    private final void sendSMS(java.lang.String phoneNo, java.lang.String msg) {
    }
    
    private final void callUsingViber(java.lang.String dialNumber) {
    }
    
    private final void sendEmail(java.lang.String mail) {
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    private final void callUsingPhone(java.lang.String number) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final void callPhone(java.lang.String phoneNumber) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    public HelpAndSupportFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/mart/onestopkitchen/ui/fragment/helpandsupport/HelpAndSupportFragment$Companion;", "", "()V", "MY_PERMISSIONS_REQUEST_CALL_PHONE", "", "app_stagingRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}