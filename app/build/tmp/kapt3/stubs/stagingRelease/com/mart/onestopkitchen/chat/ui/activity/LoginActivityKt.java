package com.mart.onestopkitchen.chat.ui.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\b\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"}, d2 = {"ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS", "", "app_stagingRelease"})
public final class LoginActivityKt {
    public static final int ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS = 422;
}