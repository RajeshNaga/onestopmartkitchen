package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@androidx.annotation.IntDef(value = {1, 0})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0082\u0002\u0018\u00002\u00020\u0001B\u0000\u00a8\u0006\u0002"}, d2 = {"Lcom/mart/onestopkitchen/chat/utils/ToastLength;", "", "app_stagingRelease"})
@java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
abstract @interface ToastLength {
}