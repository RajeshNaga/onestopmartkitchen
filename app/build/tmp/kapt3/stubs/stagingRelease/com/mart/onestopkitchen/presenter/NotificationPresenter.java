package com.mart.onestopkitchen.presenter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\u0017H\u0002J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002R \u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\u0019"}, d2 = {"Lcom/mart/onestopkitchen/presenter/NotificationPresenter;", "", "mContext", "Landroid/content/Context;", "listener", "Lcom/mart/onestopkitchen/baseview/NotificationView;", "(Landroid/content/Context;Lcom/mart/onestopkitchen/baseview/NotificationView;)V", "disposableObserver", "Lio/reactivex/observers/DisposableObserver;", "Lcom/mart/onestopkitchen/model/NotificationModel;", "getDisposableObserver", "()Lio/reactivex/observers/DisposableObserver;", "setDisposableObserver", "(Lio/reactivex/observers/DisposableObserver;)V", "getListener", "()Lcom/mart/onestopkitchen/baseview/NotificationView;", "setListener", "(Lcom/mart/onestopkitchen/baseview/NotificationView;)V", "getMContext", "()Landroid/content/Context;", "getNotificationList", "", "getNotificationObservable", "Lio/reactivex/Observable;", "getObserver", "app_stagingRelease"})
public final class NotificationPresenter {
    @org.jetbrains.annotations.NotNull()
    public io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.model.NotificationModel> disposableObserver;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context mContext = null;
    @org.jetbrains.annotations.NotNull()
    private com.mart.onestopkitchen.baseview.NotificationView listener;
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.model.NotificationModel> getDisposableObserver() {
        return null;
    }
    
    public final void setDisposableObserver(@org.jetbrains.annotations.NotNull()
    io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.model.NotificationModel> p0) {
    }
    
    private final void getNotificationList() {
    }
    
    private final io.reactivex.Observable<com.mart.onestopkitchen.model.NotificationModel> getNotificationObservable() {
        return null;
    }
    
    private final io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.model.NotificationModel> getObserver() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.baseview.NotificationView getListener() {
        return null;
    }
    
    public final void setListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.baseview.NotificationView p0) {
    }
    
    public NotificationPresenter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.baseview.NotificationView listener) {
        super();
    }
}