package com.mart.onestopkitchen.model.creditcarddao;

import java.lang.System;

/**
 * Created by Akash Garg on 16/01/2019.
 */
@androidx.room.Entity(tableName = "cardDetailTable")
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001e\u0010\f\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001e\u0010\u000f\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001e\u0010\u0012\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR\"\u0010\u0015\u001a\u0004\u0018\u00010\u00168\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001b\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006\u001c"}, d2 = {"Lcom/mart/onestopkitchen/model/creditcarddao/CardDetailModel;", "", "()V", "cardCvv", "", "getCardCvv", "()Ljava/lang/String;", "setCardCvv", "(Ljava/lang/String;)V", "cardExpMonth", "getCardExpMonth", "setCardExpMonth", "cardExpYear", "getCardExpYear", "setCardExpYear", "cardHolderName", "getCardHolderName", "setCardHolderName", "cardNumber", "getCardNumber", "setCardNumber", "userId", "", "getUserId", "()Ljava/lang/Integer;", "setUserId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "app_stagingRelease"})
public final class CardDetailModel {
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "userId")
    @androidx.room.PrimaryKey(autoGenerate = true)
    private java.lang.Integer userId;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "cardHolderName")
    private java.lang.String cardHolderName;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "cardNumber")
    private java.lang.String cardNumber;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "cardCvv")
    private java.lang.String cardCvv;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "cardExpMonth")
    private java.lang.String cardExpMonth;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "cardExpYear")
    private java.lang.String cardExpYear;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getUserId() {
        return null;
    }
    
    public final void setUserId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCardHolderName() {
        return null;
    }
    
    public final void setCardHolderName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCardNumber() {
        return null;
    }
    
    public final void setCardNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCardCvv() {
        return null;
    }
    
    public final void setCardCvv(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCardExpMonth() {
        return null;
    }
    
    public final void setCardExpMonth(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCardExpYear() {
        return null;
    }
    
    public final void setCardExpYear(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public CardDetailModel() {
        super();
    }
}