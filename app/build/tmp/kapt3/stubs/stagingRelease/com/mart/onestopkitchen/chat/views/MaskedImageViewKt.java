package com.mart.onestopkitchen.chat.views;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"FULL_COLOR_LAYER_SAVE_FLAG", "", "HAS_ALPHA_LAYER_SAVE_FLAG", "app_stagingRelease"})
public final class MaskedImageViewKt {
    private static final int HAS_ALPHA_LAYER_SAVE_FLAG = 0;
    private static final int FULL_COLOR_LAYER_SAVE_FLAG = 8;
}