package com.mart.onestopkitchen.ui.fragment;

import java.lang.System;

/**
 * Created by Akash Garg
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\f\u001a\u00020\rH\u0016J\b\u0010\u000e\u001a\u00020\rH\u0002J&\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0017J\b\u0010\u0017\u001a\u00020\rH\u0016J\b\u0010\u0018\u001a\u00020\rH\u0016J\u001a\u0010\u0019\u001a\u00020\r2\u0006\u0010\u001a\u001a\u00020\u00102\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u0010\u0010\u001b\u001a\u00020\r2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\b\u0010\u001e\u001a\u00020\rH\u0016J\u0012\u0010\u001f\u001a\u00020\r2\b\u0010 \u001a\u0004\u0018\u00010\u001dH\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lcom/mart/onestopkitchen/ui/fragment/PickupPointListFragment;", "Lcom/mart/onestopkitchen/ui/fragment/BaseFragment;", "Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$SelectedPickupPoint;", "()V", "TAG", "", "pickupPoinrViewModel", "Lcom/mart/onestopkitchen/utils/PickupPointViewModel;", "pickupPointPresenter", "Lcom/mart/onestopkitchen/presenter/PickupPointPresenter;", "viewBinding", "Landroidx/databinding/ViewDataBinding;", "hideLoader", "", "noDataFound", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onSelectedPickupPoint", "onViewCreated", "view", "setPickupPointList", "model", "Lcom/mart/onestopkitchen/networking/response/PickupPointAddressResponse;", "showLoader", "updateData", "pickupPointList", "app_stagingRelease"})
public final class PickupPointListFragment extends com.mart.onestopkitchen.ui.fragment.BaseFragment implements com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.SelectedPickupPoint {
    private final java.lang.String TAG = "PickupPointListFragment";
    private androidx.databinding.ViewDataBinding viewBinding;
    private com.mart.onestopkitchen.presenter.PickupPointPresenter pickupPointPresenter;
    private com.mart.onestopkitchen.utils.PickupPointViewModel pickupPoinrViewModel;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    @javax.annotation.Nullable()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void updateData(com.mart.onestopkitchen.networking.response.PickupPointAddressResponse pickupPointList) {
    }
    
    private final void noDataFound() {
    }
    
    private final void setPickupPointList(com.mart.onestopkitchen.networking.response.PickupPointAddressResponse model) {
    }
    
    @java.lang.Override()
    public void onSelectedPickupPoint() {
    }
    
    @java.lang.Override()
    public void showLoader() {
    }
    
    @java.lang.Override()
    public void hideLoader() {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    public PickupPointListFragment() {
        super();
    }
}