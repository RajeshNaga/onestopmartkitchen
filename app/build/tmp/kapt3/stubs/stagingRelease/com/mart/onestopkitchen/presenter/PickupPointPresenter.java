package com.mart.onestopkitchen.presenter;

import java.lang.System;

/**
 * Created by Akash Garg
 */
@android.annotation.SuppressLint(value = {"StaticFieldLeak"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H\u0002J\b\u0010\b\u001a\u00020\tH\u0007J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u000bH\u0002J\u000e\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\rR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/mart/onestopkitchen/presenter/PickupPointPresenter;", "", "()V", "data", "Landroidx/lifecycle/MutableLiveData;", "Lcom/mart/onestopkitchen/networking/response/PickupPointAddressResponse;", "getObserver", "Lio/reactivex/observers/DisposableObserver;", "getPickUpList", "", "getPickupPointObservable", "Lio/reactivex/Observable;", "getResponse", "Landroidx/lifecycle/LiveData;", "app_stagingRelease"})
public class PickupPointPresenter {
    private androidx.lifecycle.MutableLiveData<com.mart.onestopkitchen.networking.response.PickupPointAddressResponse> data;
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.LiveData<com.mart.onestopkitchen.networking.response.PickupPointAddressResponse> getResponse() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    public final void getPickUpList() {
    }
    
    private final io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.networking.response.PickupPointAddressResponse> getObserver() {
        return null;
    }
    
    private final io.reactivex.Observable<com.mart.onestopkitchen.networking.response.PickupPointAddressResponse> getPickupPointObservable() {
        return null;
    }
    
    public PickupPointPresenter() {
        super();
    }
}