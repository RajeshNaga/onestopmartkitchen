package com.mart.onestopkitchen.chat.imagepick;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u001c\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0007\u001a\u00060\bj\u0002`\tH&J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\fH&\u00a8\u0006\r"}, d2 = {"Lcom/mart/onestopkitchen/chat/imagepick/OnImagePickedListener;", "", "onImagePickClosed", "", "requestCode", "", "onImagePickError", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onImagePicked", "file", "Ljava/io/File;", "app_stagingRelease"})
public abstract interface OnImagePickedListener {
    
    public abstract void onImagePicked(int requestCode, @org.jetbrains.annotations.NotNull()
    java.io.File file);
    
    public abstract void onImagePickError(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.Exception e);
    
    public abstract void onImagePickClosed(int requestCode);
}