package com.mart.onestopkitchen.utils.fingerprint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000,\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082D\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"KEY_NAME", "", "cipher", "Ljavax/crypto/Cipher;", "cryptoObject", "Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;", "fingerprintManager", "Landroid/hardware/fingerprint/FingerprintManager;", "keyGenerator", "Ljavax/crypto/KeyGenerator;", "keyStore", "Ljava/security/KeyStore;", "keyguardManager", "Landroid/app/KeyguardManager;", "app_stagingRelease"})
public final class FingerPrintPresenterKt {
    private static final java.lang.String KEY_NAME = "yourKey";
    private static javax.crypto.Cipher cipher;
    private static java.security.KeyStore keyStore;
    private static javax.crypto.KeyGenerator keyGenerator;
    private static android.hardware.fingerprint.FingerprintManager.CryptoObject cryptoObject;
    private static android.hardware.fingerprint.FingerprintManager fingerprintManager;
    private static android.app.KeyguardManager keyguardManager;
}