package com.mart.onestopkitchen.chat.ui.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\rH\u0002J\b\u0010\u000e\u001a\u00020\rH\u0002J\u0012\u0010\u000f\u001a\u00020\r2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/AttachmentImageActivity;", "Lcom/mart/onestopkitchen/ui/activity/BaseActivity;", "()V", "imageView", "Landroid/widget/ImageView;", "img_back", "progressBar", "Landroid/widget/ProgressBar;", "dispatchTouchEvent", "", "ev", "Landroid/view/MotionEvent;", "initUI", "", "loadImage", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "DrawableListener", "app_stagingRelease"})
public final class AttachmentImageActivity extends com.mart.onestopkitchen.ui.activity.BaseActivity {
    private android.widget.ImageView imageView;
    private android.widget.ProgressBar progressBar;
    private android.widget.ImageView img_back;
    public static final com.mart.onestopkitchen.chat.ui.activity.AttachmentImageActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initUI() {
    }
    
    private final void loadImage() {
    }
    
    @java.lang.Override()
    public boolean dispatchTouchEvent(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent ev) {
        return false;
    }
    
    public AttachmentImageActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0082\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J6\u0010\u0007\u001a\u00020\b2\u000e\u0010\t\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`\u000b2\u0006\u0010\f\u001a\u00020\u00022\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000e2\u0006\u0010\u000f\u001a\u00020\bH\u0016J6\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u00022\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000e2\u0006\u0010\u0012\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/AttachmentImageActivity$DrawableListener;", "Lcom/bumptech/glide/request/RequestListener;", "", "Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;", "progressBar", "Landroid/widget/ProgressBar;", "(Lcom/mart/onestopkitchen/chat/ui/activity/AttachmentImageActivity;Landroid/widget/ProgressBar;)V", "onException", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "model", "target", "Lcom/bumptech/glide/request/target/Target;", "isFirstResource", "onResourceReady", "resource", "isFromMemoryCache", "app_stagingRelease"})
    final class DrawableListener implements com.bumptech.glide.request.RequestListener<java.lang.String, com.bumptech.glide.load.resource.drawable.GlideDrawable> {
        private final android.widget.ProgressBar progressBar = null;
        
        @java.lang.Override()
        public boolean onException(@org.jetbrains.annotations.Nullable()
        java.lang.Exception e, @org.jetbrains.annotations.NotNull()
        java.lang.String model, @org.jetbrains.annotations.NotNull()
        com.bumptech.glide.request.target.Target<com.bumptech.glide.load.resource.drawable.GlideDrawable> target, boolean isFirstResource) {
            return false;
        }
        
        @java.lang.Override()
        public boolean onResourceReady(@org.jetbrains.annotations.NotNull()
        com.bumptech.glide.load.resource.drawable.GlideDrawable resource, @org.jetbrains.annotations.NotNull()
        java.lang.String model, @org.jetbrains.annotations.NotNull()
        com.bumptech.glide.request.target.Target<com.bumptech.glide.load.resource.drawable.GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
            return false;
        }
        
        public DrawableListener(@org.jetbrains.annotations.NotNull()
        android.widget.ProgressBar progressBar) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/AttachmentImageActivity$Companion;", "", "()V", "start", "", "context", "Landroid/content/Context;", "url", "", "app_stagingRelease"})
    public static final class Companion {
        
        public final void start(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String url) {
        }
        
        private Companion() {
            super();
        }
    }
}