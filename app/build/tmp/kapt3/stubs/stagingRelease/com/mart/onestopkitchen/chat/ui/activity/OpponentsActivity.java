package com.mart.onestopkitchen.chat.ui.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 /2\u00020\u0001:\u0001/B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\f\u001a\u00020\rH\u0002J&\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u00100\u000fj\b\u0012\u0004\u0012\u00020\u0010`\u00112\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00070\u0013H\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0015H\u0002J\b\u0010\u0017\u001a\u00020\u0015H\u0002J\u0014\u0010\u0018\u001a\u00020\r2\n\u0010\u0019\u001a\u0006\u0012\u0002\b\u00030\u001aH\u0002J\b\u0010\u001b\u001a\u00020\u0015H\u0002J\u0012\u0010\u001c\u001a\u00020\u00152\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\u0010\u0010\u001f\u001a\u00020\r2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\r2\u0006\u0010#\u001a\u00020$H\u0016J\b\u0010%\u001a\u00020\u0015H\u0014J\u0010\u0010&\u001a\u00020\u00152\u0006\u0010\'\u001a\u00020\rH\u0002J\b\u0010(\u001a\u00020\u0015H\u0002J\u001e\u0010)\u001a\u00020\u00042\u0006\u0010*\u001a\u00020\u00042\u0006\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u0004J\u0010\u0010-\u001a\u00020\u00152\u0006\u0010.\u001a\u00020\u0010H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/OpponentsActivity;", "Lcom/mart/onestopkitchen/ui/activity/BaseActivity;", "()V", "TAG", "", "kotlin.jvm.PlatformType", "currentUser", "Lcom/quickblox/users/model/QBUser;", "usersAdapter", "Lcom/mart/onestopkitchen/chat/ui/adapters/UsersAdapter;", "usersRecyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "checkIsLoggedInChat", "", "getIdsSelectedOpponents", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "selectedUsers", "", "initDefaultActionBar", "", "initUI", "initUsersList", "isCallServiceRunning", "serviceClass", "Ljava/lang/Class;", "loadUsers", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onResume", "startCall", "isVideoCall", "startLoginService", "uTCToLocal", "dateFormatInPut", "dateFomratOutPut", "datesToConvert", "updateActionBar", "countSelectedUsers", "Companion", "app_stagingRelease"})
public final class OpponentsActivity extends com.mart.onestopkitchen.ui.activity.BaseActivity {
    private final java.lang.String TAG = null;
    private androidx.recyclerview.widget.RecyclerView usersRecyclerView;
    private com.quickblox.users.model.QBUser currentUser;
    private com.mart.onestopkitchen.chat.ui.adapters.UsersAdapter usersAdapter;
    public static final com.mart.onestopkitchen.chat.ui.activity.OpponentsActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final boolean isCallServiceRunning(java.lang.Class<?> serviceClass) {
        return false;
    }
    
    private final void loadUsers() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String uTCToLocal(@org.jetbrains.annotations.NotNull()
    java.lang.String dateFormatInPut, @org.jetbrains.annotations.NotNull()
    java.lang.String dateFomratOutPut, @org.jetbrains.annotations.NotNull()
    java.lang.String datesToConvert) {
        return null;
    }
    
    private final void initUI() {
    }
    
    private final void initUsersList() {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final boolean checkIsLoggedInChat() {
        return false;
    }
    
    private final void startLoginService() {
    }
    
    private final void startCall(boolean isVideoCall) {
    }
    
    private final java.util.ArrayList<java.lang.Integer> getIdsSelectedOpponents(java.util.Collection<? extends com.quickblox.users.model.QBUser> selectedUsers) {
        return null;
    }
    
    private final void updateActionBar(int countSelectedUsers) {
    }
    
    private final void initDefaultActionBar() {
    }
    
    public OpponentsActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/OpponentsActivity$Companion;", "", "()V", "start", "", "context", "Landroid/content/Context;", "app_stagingRelease"})
    public static final class Companion {
        
        public final void start(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        private Companion() {
            super();
        }
    }
}