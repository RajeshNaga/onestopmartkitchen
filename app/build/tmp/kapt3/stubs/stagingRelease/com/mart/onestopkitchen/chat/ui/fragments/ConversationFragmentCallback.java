package com.mart.onestopkitchen.chat.ui.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\bf\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005H&J\u0012\u0010\u0007\u001a\u00020\u00032\b\u0010\b\u001a\u0004\u0018\u00010\tH&J\u0012\u0010\n\u001a\u00020\u00032\b\u0010\u000b\u001a\u0004\u0018\u00010\fH&J\u0012\u0010\r\u001a\u00020\u00032\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH&J\u0012\u0010\u0010\u001a\u00020\u00032\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H&J\u0016\u0010\u0013\u001a\u00020\u00032\f\u0010\u0014\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0015H&J\u0018\u0010\u0016\u001a\u00020\u00032\u000e\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0019\u0018\u00010\u0018H&J\b\u0010\u001a\u001a\u00020\u001bH&J\u000f\u0010\u001c\u001a\u0004\u0018\u00010\u001dH&\u00a2\u0006\u0002\u0010\u001eJ\n\u0010\u001f\u001a\u0004\u0018\u00010 H&J\u0010\u0010!\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\"H&J\u0012\u0010#\u001a\u0004\u0018\u00010$2\u0006\u0010%\u001a\u00020\u001dH&J\u0012\u0010&\u001a\u0004\u0018\u00010\'2\u0006\u0010%\u001a\u00020\u001dH&J\u0014\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\'0)H&J\b\u0010*\u001a\u00020\u001bH&J\b\u0010+\u001a\u00020\u001bH&J\b\u0010,\u001a\u00020\u0003H&J\u0010\u0010-\u001a\u00020\u00032\u0006\u0010.\u001a\u00020\u001bH&J\u0010\u0010/\u001a\u00020\u00032\u0006\u00100\u001a\u00020\u001bH&J\b\u00101\u001a\u00020\u0003H&J\b\u00102\u001a\u00020\u0003H&J\u0010\u00103\u001a\u00020\u00032\u0006\u00104\u001a\u000205H&J\u0012\u00106\u001a\u00020\u00032\b\u0010\b\u001a\u0004\u0018\u00010\tH&J\u0012\u00107\u001a\u00020\u00032\b\u0010\u000b\u001a\u0004\u0018\u00010\fH&J\u0012\u00108\u001a\u00020\u00032\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH&J\u0012\u00109\u001a\u00020\u00032\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H&J\u0016\u0010:\u001a\u00020\u00032\f\u0010\u0014\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0015H&J\u0018\u0010;\u001a\u00020\u00032\u000e\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0019\u0018\u00010\u0018H&J\u001c\u0010<\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005H&\u00a8\u0006="}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/fragments/ConversationFragmentCallback;", "", "acceptCall", "", "userInfo", "", "", "addConnectionListener", "connectionCallback", "Lorg/jivesoftware/smack/ConnectionListener;", "addCurrentCallStateListener", "currentCallStateCallback", "Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$CurrentCallStateCallback;", "addOnChangeAudioDeviceListener", "onChangeDynamicCallback", "Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$OnChangeAudioDevice;", "addSessionEventsListener", "eventsCallback", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionEventsCallback;", "addSessionStateListener", "clientConnectionCallbacks", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionStateCallback;", "addVideoTrackListener", "callback", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientVideoTracksCallbacks;", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "currentSessionExist", "", "getCallerId", "", "()Ljava/lang/Integer;", "getCurrentSessionState", "Lcom/quickblox/videochat/webrtc/BaseSession$QBRTCSessionState;", "getOpponents", "", "getPeerChannel", "Lcom/quickblox/videochat/webrtc/QBRTCTypes$QBRTCConnectionState;", "userId", "getVideoTrack", "Lcom/quickblox/videochat/webrtc/view/QBRTCVideoTrack;", "getVideoTrackMap", "", "isCallState", "isMediaStreamManagerExist", "onHangUpCurrentSession", "onSetAudioEnabled", "isAudioEnabled", "onSetVideoEnabled", "isNeedEnableCam", "onStartScreenSharing", "onSwitchAudio", "onSwitchCamera", "cameraSwitchHandler", "Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;", "removeConnectionListener", "removeCurrentCallStateListener", "removeOnChangeAudioDeviceListener", "removeSessionEventsListener", "removeSessionStateListener", "removeVideoTrackListener", "startCall", "app_stagingRelease"})
public abstract interface ConversationFragmentCallback {
    
    public abstract void addConnectionListener(@org.jetbrains.annotations.Nullable()
    org.jivesoftware.smack.ConnectionListener connectionCallback);
    
    public abstract void removeConnectionListener(@org.jetbrains.annotations.Nullable()
    org.jivesoftware.smack.ConnectionListener connectionCallback);
    
    public abstract void addSessionStateListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<?> clientConnectionCallbacks);
    
    public abstract void removeSessionStateListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<?> clientConnectionCallbacks);
    
    public abstract void addVideoTrackListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession> callback);
    
    public abstract void removeVideoTrackListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession> callback);
    
    public abstract void addSessionEventsListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback eventsCallback);
    
    public abstract void removeSessionEventsListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback eventsCallback);
    
    public abstract void addCurrentCallStateListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.CurrentCallStateCallback currentCallStateCallback);
    
    public abstract void removeCurrentCallStateListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.CurrentCallStateCallback currentCallStateCallback);
    
    public abstract void addOnChangeAudioDeviceListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.OnChangeAudioDevice onChangeDynamicCallback);
    
    public abstract void removeOnChangeAudioDeviceListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.OnChangeAudioDevice onChangeDynamicCallback);
    
    public abstract void onSetAudioEnabled(boolean isAudioEnabled);
    
    public abstract void onSetVideoEnabled(boolean isNeedEnableCam);
    
    public abstract void onSwitchAudio();
    
    public abstract void onHangUpCurrentSession();
    
    public abstract void onStartScreenSharing();
    
    public abstract void onSwitchCamera(@org.jetbrains.annotations.NotNull()
    org.webrtc.CameraVideoCapturer.CameraSwitchHandler cameraSwitchHandler);
    
    public abstract void acceptCall(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo);
    
    public abstract void startCall(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo);
    
    public abstract boolean currentSessionExist();
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.util.List<java.lang.Integer> getOpponents();
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Integer getCallerId();
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.quickblox.videochat.webrtc.BaseSession.QBRTCSessionState getCurrentSessionState();
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.quickblox.videochat.webrtc.QBRTCTypes.QBRTCConnectionState getPeerChannel(int userId);
    
    public abstract boolean isMediaStreamManagerExist();
    
    public abstract boolean isCallState();
    
    @org.jetbrains.annotations.NotNull()
    public abstract java.util.Map<java.lang.Integer, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack> getVideoTrackMap();
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.quickblox.videochat.webrtc.view.QBRTCVideoTrack getVideoTrack(int userId);
}