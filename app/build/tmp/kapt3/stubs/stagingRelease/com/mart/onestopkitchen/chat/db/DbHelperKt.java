package com.mart.onestopkitchen.chat.db;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0014\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0003\"\u0014\u0010\u0004\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0003\"\u0014\u0010\u0006\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0003\"\u0014\u0010\b\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0003\"\u0014\u0010\n\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u0003\"\u0014\u0010\f\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u0003\"\u0014\u0010\u000e\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0003\"\u0014\u0010\u0010\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0003\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082D\u00a2\u0006\u0002\n\u0000\"\u0014\u0010\u0013\u001a\u00020\u0001X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0003\u00a8\u0006\u0015"}, d2 = {"DB_COLUMN_ID", "", "getDB_COLUMN_ID", "()Ljava/lang/String;", "DB_COLUMN_USER_CUSTOM_DATA", "getDB_COLUMN_USER_CUSTOM_DATA", "DB_COLUMN_USER_FULL_NAME", "getDB_COLUMN_USER_FULL_NAME", "DB_COLUMN_USER_ID", "getDB_COLUMN_USER_ID", "DB_COLUMN_USER_LAST_LOGIN_TIME", "getDB_COLUMN_USER_LAST_LOGIN_TIME", "DB_COLUMN_USER_LOGIN", "getDB_COLUMN_USER_LOGIN", "DB_COLUMN_USER_PASSWORD", "getDB_COLUMN_USER_PASSWORD", "DB_COLUMN_USER_TAG", "getDB_COLUMN_USER_TAG", "DB_NAME", "DB_TABLE_NAME", "getDB_TABLE_NAME", "app_stagingRelease"})
public final class DbHelperKt {
    private static final java.lang.String DB_NAME = "groupchatwebrtcDB";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_TABLE_NAME = "users";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_ID = "ID";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_USER_FULL_NAME = "userFullName";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_USER_LOGIN = "userLogin";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_USER_ID = "userID";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_USER_PASSWORD = "userPass";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_USER_TAG = "userTag";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_USER_LAST_LOGIN_TIME = "last_login_time";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String DB_COLUMN_USER_CUSTOM_DATA = "custom_data";
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_TABLE_NAME() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_ID() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_USER_FULL_NAME() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_USER_LOGIN() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_USER_ID() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_USER_PASSWORD() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_USER_TAG() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_USER_LAST_LOGIN_TIME() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDB_COLUMN_USER_CUSTOM_DATA() {
        return null;
    }
}