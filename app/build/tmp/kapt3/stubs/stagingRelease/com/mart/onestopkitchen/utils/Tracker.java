package com.mart.onestopkitchen.utils;

import java.lang.System;

/**
 * Created by dinesh on 9/1/18
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\nJ\u000e\u0010$\u001a\u00020 2\u0006\u0010!\u001a\u00020\"J\u000e\u0010%\u001a\u00020 2\u0006\u0010!\u001a\u00020\"J\b\u0010&\u001a\u00020\'H\u0002J\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010J\u001c\u0010)\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010+H\u0002JD\u0010,\u001a\u00020 2\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002002\b\b\u0002\u00102\u001a\u00020\'2\n\b\u0002\u0010*\u001a\u0004\u0018\u0001032\u000e\b\u0002\u00104\u001a\b\u0012\u0004\u0012\u00020\'05J\b\u00106\u001a\u0004\u0018\u00010\u0013J\f\u00107\u001a\b\u0012\u0004\u0012\u0002080\u0010J\u0018\u00109\u001a\u00020 2\u000e\u0010:\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u0010H\u0002J\u0018\u0010;\u001a\u00020 2\u000e\u0010<\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010H\u0002J\u000e\u0010=\u001a\u00020 2\u0006\u0010!\u001a\u00020\"J\u001a\u0010>\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010+J\u000e\u0010?\u001a\u00020 2\u0006\u0010!\u001a\u00020\"R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\u000b\"\u0004\b\f\u0010\rR\u000e\u0010\u000e\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0016\u001a\u0004\u0018\u00010\u00178\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"}, d2 = {"Lcom/mart/onestopkitchen/utils/Tracker;", "", "()V", "bluetoothScanning", "Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothScan;", "getBluetoothScanning", "()Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothScan;", "setBluetoothScanning", "(Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothScan;)V", "isBluetoothObserving", "", "()Z", "setBluetoothObserving", "(Z)V", "isObserving", "latestBluetoothResult", "", "Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothModel;", "latestLocation", "Landroid/location/Location;", "locationDisposable", "Lio/reactivex/disposables/Disposable;", "locationSettings", "Lcom/mart/onestopkitchen/utils/location/LocationSettings;", "getLocationSettings", "()Lcom/mart/onestopkitchen/utils/location/LocationSettings;", "setLocationSettings", "(Lcom/mart/onestopkitchen/utils/location/LocationSettings;)V", "mWifiScanResult", "Landroid/net/wifi/ScanResult;", "oldLocation", "disableBluetoothTrack", "", "mActivity", "Landroidx/appcompat/app/AppCompatActivity;", "languageChanged", "disableTrack", "disableWifiTrack", "getActiveLocale", "", "getBluetoothResult", "getCurrentLocation", "mCallback", "Lcom/mart/onestopkitchen/utils/callback/CurrentLocationCallback;", "getGeoCoderLocation", "mContext", "Landroid/content/Context;", "mLat", "", "mLang", "mSearchAddress", "Lcom/mart/onestopkitchen/utils/location/OnLocationFetched;", "locales", "", "getLatestLocation", "getWifiScanResult", "Lcom/mart/onestopkitchen/utils/wifi/WifiModel;", "saveResult", "mList", "saveScanResult", "mScanResult", "trackBluetooth", "trackLocation", "trackWifi", "app_stagingRelease"})
public final class Tracker {
    private io.reactivex.disposables.Disposable locationDisposable;
    private android.location.Location latestLocation;
    private android.location.Location oldLocation;
    @org.jetbrains.annotations.Nullable()
    @android.annotation.SuppressLint(value = {"StaticFieldLeak"})
    private com.mart.onestopkitchen.utils.location.LocationSettings locationSettings;
    
    /**
     * ************************** bluetooth track
     */
    @org.jetbrains.annotations.Nullable()
    @android.annotation.SuppressLint(value = {"StaticFieldLeak"})
    private com.mart.onestopkitchen.utils.bluetooth.BluetoothScan bluetoothScanning;
    private boolean isBluetoothObserving;
    private java.util.List<com.mart.onestopkitchen.utils.bluetooth.BluetoothModel> latestBluetoothResult;
    
    /**
     * ******************** wifi track
     */
    private boolean isObserving;
    private java.util.List<? extends android.net.wifi.ScanResult> mWifiScanResult;
    
    @org.jetbrains.annotations.Nullable()
    public final com.mart.onestopkitchen.utils.location.LocationSettings getLocationSettings() {
        return null;
    }
    
    public final void setLocationSettings(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.utils.location.LocationSettings p0) {
    }
    
    public final void trackLocation(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity, @org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.utils.callback.CurrentLocationCallback mCallback) {
    }
    
    private final void getCurrentLocation(androidx.appcompat.app.AppCompatActivity mActivity, com.mart.onestopkitchen.utils.callback.CurrentLocationCallback mCallback) {
    }
    
    public final void disableTrack(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.location.Location getLatestLocation() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.mart.onestopkitchen.utils.bluetooth.BluetoothScan getBluetoothScanning() {
        return null;
    }
    
    public final void setBluetoothScanning(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.utils.bluetooth.BluetoothScan p0) {
    }
    
    public final boolean isBluetoothObserving() {
        return false;
    }
    
    public final void setBluetoothObserving(boolean p0) {
    }
    
    public final void trackBluetooth(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity) {
    }
    
    private final void saveScanResult(java.util.List<com.mart.onestopkitchen.utils.bluetooth.BluetoothModel> mScanResult) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mart.onestopkitchen.utils.bluetooth.BluetoothModel> getBluetoothResult() {
        return null;
    }
    
    public final void disableBluetoothTrack(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity, boolean languageChanged) {
    }
    
    public final void trackWifi(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity) {
    }
    
    private final void saveResult(java.util.List<? extends android.net.wifi.ScanResult> mList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.mart.onestopkitchen.utils.wifi.WifiModel> getWifiScanResult() {
        return null;
    }
    
    public final void disableWifiTrack(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity) {
    }
    
    /**
     * ************************************** Get Location From Geo Coder
     */
    public final void getGeoCoderLocation(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, double mLat, double mLang, @org.jetbrains.annotations.NotNull()
    java.lang.String mSearchAddress, @org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.utils.location.OnLocationFetched mCallback, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> locales) {
    }
    
    private final java.lang.String getActiveLocale() {
        return null;
    }
    
    public Tracker() {
        super();
    }
}