package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u00000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010 \n\u0002\b\u0006\u001a\u000e\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u001a(\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u0001H\u0002\u001a&\u0010\u000e\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u0001\u001a(\u0010\u0010\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u0001H\u0002\u001a*\u0010\u0010\u001a\u0004\u0018\u00010\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u0003H\u0002\u001a\u000e\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u0018\u0010\u0012\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u001a\b\u0010\u0013\u001a\u00020\u0005H\u0002\u001a\u0016\u0010\u0014\u001a\u00020\u00052\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00010\u0016H\u0002\u001a\u0018\u0010\u0017\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u001a$\u0010\u0018\u001a\u00020\u00052\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00010\u00162\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u0010\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u001a\u001a\u00020\u0001H\u0002\u001a\u0010\u0010\u001b\u001a\u00020\u00052\u0006\u0010\u001a\u001a\u00020\u0001H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"LIMIT_MEMBERS", "", "TAG", "", "configRTCTimers", "", "context", "Landroid/content/Context;", "getPreferenceBoolean", "", "sharedPref", "Landroid/content/SharedPreferences;", "StrRes", "strResDefValue", "getPreferenceInt", "strResKey", "getPreferenceString", "isManageSpeakerPhoneByProximity", "setCommonSettings", "setDefaultVideoQuality", "setSettingsForMultiCall", "users", "", "setSettingsFromPreferences", "setSettingsStrategy", "setVideoFromLibraryPreferences", "resolutionItem", "setVideoQuality", "app_stagingRelease"})
public final class SettingsUtilKt {
    private static final java.lang.String TAG = "SettingsUtil";
    private static final int LIMIT_MEMBERS = 4;
    
    private static final void setSettingsForMultiCall(java.util.List<java.lang.Integer> users) {
    }
    
    public static final void setSettingsStrategy(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Integer> users, @org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences sharedPref, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public static final void configRTCTimers(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public static final boolean isManageSpeakerPhoneByProximity(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public static final int getPreferenceInt(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences sharedPref, @org.jetbrains.annotations.NotNull()
    android.content.Context context, int strResKey, int strResDefValue) {
        return 0;
    }
    
    private static final void setCommonSettings(android.content.SharedPreferences sharedPref, android.content.Context context) {
    }
    
    private static final void setSettingsFromPreferences(android.content.SharedPreferences sharedPref, android.content.Context context) {
    }
    
    private static final void setVideoQuality(int resolutionItem) {
    }
    
    private static final void setDefaultVideoQuality() {
    }
    
    private static final void setVideoFromLibraryPreferences(int resolutionItem) {
    }
    
    private static final java.lang.String getPreferenceString(android.content.SharedPreferences sharedPref, android.content.Context context, int strResKey, int strResDefValue) {
        return null;
    }
    
    private static final java.lang.String getPreferenceString(android.content.SharedPreferences sharedPref, android.content.Context context, int strResKey, java.lang.String strResDefValue) {
        return null;
    }
    
    private static final boolean getPreferenceBoolean(android.content.SharedPreferences sharedPref, android.content.Context context, int StrRes, int strResDefValue) {
        return false;
    }
}