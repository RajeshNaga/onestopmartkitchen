package com.mart.onestopkitchen.utils.bluetooth;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b-\u0018\u00002\u00020\u0001B\u0091\u0001\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\u0003\u0012\b\b\u0002\u0010\n\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\r\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0011R\u001a\u0010\u0007\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0010\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0013\"\u0004\b\u0017\u0010\u0015R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0013\"\u0004\b\u0019\u0010\u0015R\u001a\u0010\u000e\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0013\"\u0004\b\u001b\u0010\u0015R\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0013\"\u0004\b\u001d\u0010\u0015R\u001a\u0010\b\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0013\"\u0004\b\u001f\u0010\u0015R\u001a\u0010\u000b\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0013\"\u0004\b!\u0010\u0015R\u001a\u0010\u0006\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0013\"\u0004\b#\u0010\u0015R\u001a\u0010\n\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0013\"\u0004\b%\u0010\u0015R\u001a\u0010\r\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0013\"\u0004\b\'\u0010\u0015R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0013\"\u0004\b)\u0010\u0015R\u001a\u0010\u000f\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0013\"\u0004\b+\u0010\u0015R\u001a\u0010\f\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u0013\"\u0004\b-\u0010\u0015R\u001a\u0010\t\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0013\"\u0004\b/\u0010\u0015\u00a8\u00060"}, d2 = {"Lcom/mart/onestopkitchen/utils/bluetooth/BluetoothModel;", "", "Minor", "", "Distance", "CurrentRssi", "IBeacon", "Accuracy", "FirstRssi", "Uuid", "Mac", "FirstTimestamp", "TxPower", "Major", "CurrentTimestamp", "Name", "AdRecord", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccuracy", "()Ljava/lang/String;", "setAccuracy", "(Ljava/lang/String;)V", "getAdRecord", "setAdRecord", "getCurrentRssi", "setCurrentRssi", "getCurrentTimestamp", "setCurrentTimestamp", "getDistance", "setDistance", "getFirstRssi", "setFirstRssi", "getFirstTimestamp", "setFirstTimestamp", "getIBeacon", "setIBeacon", "getMac", "setMac", "getMajor", "setMajor", "getMinor", "setMinor", "getName", "setName", "getTxPower", "setTxPower", "getUuid", "setUuid", "app_stagingRelease"})
public final class BluetoothModel {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String Minor;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String Distance;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String CurrentRssi;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String IBeacon;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String Accuracy;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String FirstRssi;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String Uuid;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String Mac;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String FirstTimestamp;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String TxPower;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String Major;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String CurrentTimestamp;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String Name;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String AdRecord;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMinor() {
        return null;
    }
    
    public final void setMinor(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDistance() {
        return null;
    }
    
    public final void setDistance(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCurrentRssi() {
        return null;
    }
    
    public final void setCurrentRssi(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getIBeacon() {
        return null;
    }
    
    public final void setIBeacon(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAccuracy() {
        return null;
    }
    
    public final void setAccuracy(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFirstRssi() {
        return null;
    }
    
    public final void setFirstRssi(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUuid() {
        return null;
    }
    
    public final void setUuid(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMac() {
        return null;
    }
    
    public final void setMac(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFirstTimestamp() {
        return null;
    }
    
    public final void setFirstTimestamp(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTxPower() {
        return null;
    }
    
    public final void setTxPower(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMajor() {
        return null;
    }
    
    public final void setMajor(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCurrentTimestamp() {
        return null;
    }
    
    public final void setCurrentTimestamp(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    public final void setName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAdRecord() {
        return null;
    }
    
    public final void setAdRecord(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public BluetoothModel(@org.jetbrains.annotations.NotNull()
    java.lang.String Minor, @org.jetbrains.annotations.NotNull()
    java.lang.String Distance, @org.jetbrains.annotations.NotNull()
    java.lang.String CurrentRssi, @org.jetbrains.annotations.NotNull()
    java.lang.String IBeacon, @org.jetbrains.annotations.NotNull()
    java.lang.String Accuracy, @org.jetbrains.annotations.NotNull()
    java.lang.String FirstRssi, @org.jetbrains.annotations.NotNull()
    java.lang.String Uuid, @org.jetbrains.annotations.NotNull()
    java.lang.String Mac, @org.jetbrains.annotations.NotNull()
    java.lang.String FirstTimestamp, @org.jetbrains.annotations.NotNull()
    java.lang.String TxPower, @org.jetbrains.annotations.NotNull()
    java.lang.String Major, @org.jetbrains.annotations.NotNull()
    java.lang.String CurrentTimestamp, @org.jetbrains.annotations.NotNull()
    java.lang.String Name, @org.jetbrains.annotations.NotNull()
    java.lang.String AdRecord) {
        super();
    }
    
    public BluetoothModel() {
        super();
    }
}