package com.mart.onestopkitchen.chat.qb.callback;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u00012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00010\u0002B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u00a2\u0006\u0002\u0010\u0005J\u001f\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00028\u00002\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/mart/onestopkitchen/chat/qb/callback/QbEntityCallbackWrapper;", "T", "Lcom/mart/onestopkitchen/chat/qb/callback/QbEntityCallbackTwoTypeWrapper;", "callback", "Lcom/quickblox/core/QBEntityCallback;", "(Lcom/quickblox/core/QBEntityCallback;)V", "onSuccess", "", "t", "bundle", "Landroid/os/Bundle;", "(Ljava/lang/Object;Landroid/os/Bundle;)V", "app_stagingRelease"})
public class QbEntityCallbackWrapper<T extends java.lang.Object> extends com.mart.onestopkitchen.chat.qb.callback.QbEntityCallbackTwoTypeWrapper<T, T> {
    
    @java.lang.Override()
    public void onSuccess(T t, @org.jetbrains.annotations.Nullable()
    android.os.Bundle bundle) {
    }
    
    public QbEntityCallbackWrapper(@org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<T> callback) {
        super(null);
    }
}