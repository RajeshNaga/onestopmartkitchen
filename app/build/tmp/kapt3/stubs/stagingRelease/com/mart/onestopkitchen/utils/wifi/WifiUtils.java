package com.mart.onestopkitchen.utils.wifi;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\bJ\u000e\u0010\u000b\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0005J\u0015\u0010\r\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u000e\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000fR\u001c\u0010\u0003\u001a\u0010\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/mart/onestopkitchen/utils/wifi/WifiUtils;", "", "()V", "channelsFrequency", "Ljava/util/ArrayList;", "", "kotlin.jvm.PlatformType", "calculateDistance", "", "signalLevelInDb", "freqInMHz", "getChannelFromFrequency", "frequency", "getFrequencyFromChannel", "channel", "(I)Ljava/lang/Integer;", "app_stagingRelease"})
public final class WifiUtils {
    private final java.util.ArrayList<java.lang.Integer> channelsFrequency = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getFrequencyFromChannel(int channel) {
        return null;
    }
    
    public final int getChannelFromFrequency(int frequency) {
        return 0;
    }
    
    public final double calculateDistance(double signalLevelInDb, double freqInMHz) {
        return 0.0;
    }
    
    public WifiUtils() {
        super();
    }
}