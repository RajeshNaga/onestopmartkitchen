package com.mart.onestopkitchen.permission;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u001b\u0010\u0004\u001a\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&\u00a2\u0006\u0002\u0010\bJ\u001b\u0010\t\u001a\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&\u00a2\u0006\u0002\u0010\b\u00a8\u0006\n"}, d2 = {"Lcom/mart/onestopkitchen/permission/PermissionListener;", "", "allPermissionGranted", "", "onDenied", "string", "", "", "([Ljava/lang/String;)V", "onNeverAskAgainPermission", "app_stagingRelease"})
public abstract interface PermissionListener {
    
    public abstract void allPermissionGranted();
    
    public abstract void onNeverAskAgainPermission(@org.jetbrains.annotations.NotNull()
    java.lang.String[] string);
    
    public abstract void onDenied(@org.jetbrains.annotations.NotNull()
    java.lang.String[] string);
}