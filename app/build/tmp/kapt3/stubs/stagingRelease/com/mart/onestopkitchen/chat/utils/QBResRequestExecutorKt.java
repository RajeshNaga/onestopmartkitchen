package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000:\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\u001a\u001c\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u001a(\u0010\u0007\u001a\u00020\u00012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\t2\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u0005\u001a\"\u0010\f\u001a\u00020\u00012\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u00052\u0006\u0010\r\u001a\u00020\u000e\u001a\"\u0010\u000f\u001a\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u00112\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u0005\u001a\u001c\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u000b2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0005\u001a\u0006\u0010\u0014\u001a\u00020\u0001\u001a\u001c\u0010\u0015\u001a\u00020\u00012\u0006\u0010\u0016\u001a\u00020\u000b2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0005\u00a8\u0006\u0017"}, d2 = {"deleteCurrentUser", "", "currentQbUserID", "", "callback", "Lcom/quickblox/core/QBEntityCallback;", "Ljava/lang/Void;", "loadUsersByIds", "usersIDs", "", "Ljava/util/ArrayList;", "Lcom/quickblox/users/model/QBUser;", "loadUsersByPagedRequestBuilder", "requestBuilder", "Lcom/quickblox/core/request/QBPagedRequestBuilder;", "loadUsersByTag", "tag", "", "signInUser", "currentQbUser", "signOut", "signUp", "newQbUser", "app_stagingRelease"})
public final class QBResRequestExecutorKt {
    
    public static final void signUp(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser newQbUser, @org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<com.quickblox.users.model.QBUser> callback) {
    }
    
    public static final void signInUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser currentQbUser, @org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<com.quickblox.users.model.QBUser> callback) {
    }
    
    public static final void signOut() {
    }
    
    public static final void deleteCurrentUser(int currentQbUserID, @org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<java.lang.Void> callback) {
    }
    
    public static final void loadUsersByTag(@org.jetbrains.annotations.NotNull()
    java.lang.String tag, @org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<java.util.ArrayList<com.quickblox.users.model.QBUser>> callback) {
    }
    
    public static final void loadUsersByPagedRequestBuilder(@org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<java.util.ArrayList<com.quickblox.users.model.QBUser>> callback, @org.jetbrains.annotations.NotNull()
    com.quickblox.core.request.QBPagedRequestBuilder requestBuilder) {
    }
    
    public static final void loadUsersByIds(@org.jetbrains.annotations.NotNull()
    java.util.Collection<java.lang.Integer> usersIDs, @org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<java.util.ArrayList<com.quickblox.users.model.QBUser>> callback) {
    }
}