package com.mart.onestopkitchen.baseview;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0003H&\u00a8\u0006\u0007"}, d2 = {"Lcom/mart/onestopkitchen/baseview/AppUpdateView;", "", "appUpdateData", "", "data", "Lcom/mart/onestopkitchen/update/AppUpdateModel;", "error", "app_stagingRelease"})
public abstract interface AppUpdateView {
    
    public abstract void appUpdateData(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.update.AppUpdateModel data);
    
    public abstract void error();
}