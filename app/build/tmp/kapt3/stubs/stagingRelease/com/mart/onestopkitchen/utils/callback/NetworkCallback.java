package com.mart.onestopkitchen.utils.callback;

import java.lang.System;

/**
 * Created by DineshS on 9/11/2017.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/utils/callback/NetworkCallback;", "", "networkChange", "", "isConnected", "", "app_stagingRelease"})
public abstract interface NetworkCallback {
    
    public abstract void networkChange(boolean isConnected);
}