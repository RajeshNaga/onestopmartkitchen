package com.mart.onestopkitchen.mvp.ui.ratingandreview;

import java.lang.System;

@android.annotation.SuppressLint(value = {"ValidFragment"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B)\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\b\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\u001a\u001a\u00020\u001bH\u0002J\b\u0010\u001c\u001a\u00020\u001dH\u0016J\u0012\u0010\u001e\u001a\u00020\u001b2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\b\u0010!\u001a\u00020\u001bH\u0016J\b\u0010\"\u001a\u00020\u001bH\u0002J\u0012\u0010#\u001a\u00020\u001b2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J&\u0010&\u001a\u0004\u0018\u00010%2\u0006\u0010\'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\b\u0010-\u001a\u00020\u001bH\u0016J\u0010\u0010.\u001a\u00020\u001b2\u0006\u0010/\u001a\u000200H\u0016J\u0018\u00101\u001a\u00020\u001b2\u0006\u00102\u001a\u00020\b2\u0006\u00103\u001a\u000204H\u0016J\u001a\u00105\u001a\u00020\u001b2\u0006\u00106\u001a\u00020%2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\u0012\u00107\u001a\u00020\u001b2\b\u00108\u001a\u0004\u0018\u000109H\u0016J\b\u0010\t\u001a\u00020\bH\u0016J\b\u0010:\u001a\u00020\u001bH\u0002J\u0010\u0010;\u001a\u00020\u001b2\u0006\u0010<\u001a\u000204H\u0002J\b\u0010=\u001a\u00020>H\u0016J\n\u0010?\u001a\u0004\u0018\u00010@H\u0016J\u0010\u0010A\u001a\u00020\u001b2\u0006\u0010B\u001a\u00020\u0006H\u0002J\u0018\u0010C\u001a\u00020\u001b2\u0006\u0010D\u001a\u00020\u00062\u0006\u0010E\u001a\u00020\u0006H\u0002J\u0010\u0010F\u001a\u00020\u001b2\u0006\u0010G\u001a\u00020\u0006H\u0016J\b\u0010H\u001a\u00020\u001bH\u0016J\u0012\u0010I\u001a\u00020\u001b2\b\u0010G\u001a\u0004\u0018\u00010JH\u0016J\b\u0010K\u001a\u00020\u001bH\u0002J\u0012\u0010L\u001a\u00020\u001b2\b\u0010M\u001a\u0004\u0018\u00010NH\u0017J\b\u0010O\u001a\u00020\u001bH\u0002J\b\u0010P\u001a\u000204H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0015\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\n\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0011\u0010\t\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006Q"}, d2 = {"Lcom/mart/onestopkitchen/mvp/ui/ratingandreview/RatingAndReviewFragment;", "Lcom/mart/onestopkitchen/ui/fragment/BaseFragment;", "Lcom/mart/onestopkitchen/mvp/ui/ratingandreview/ReviewListener;", "Lcom/mart/onestopkitchen/ratingview/SmileRating$OnSmileySelectionListener;", "Landroid/view/View$OnClickListener;", "fromWhere", "", "orderId", "", "productId", "orderIds", "(Ljava/lang/String;Ljava/lang/Integer;ILjava/lang/String;)V", "getRating", "mainActivity", "Lcom/mart/onestopkitchen/ui/activity/MainActivity;", "getOrderId", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getOrderIds", "()Ljava/lang/String;", "setOrderIds", "(Ljava/lang/String;)V", "getProductId", "()I", "ratingAndReviewPresenter", "Lcom/mart/onestopkitchen/mvp/ui/ratingandreview/RatingAndReviewPresenter;", "continueAction", "", "getActContext", "Landroid/content/Context;", "getSuccessFromServer", "rating", "Lcom/mart/onestopkitchen/model/ratingreview/RatingGettingModel;", "hideLoading", "initUi", "onClick", "id", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onPrepareOptionsMenu", "menu", "Landroid/view/Menu;", "onSmileySelected", "smiley", "reselected", "", "onViewCreated", "view", "orderReviewAndRating", "model", "Lcom/mart/onestopkitchen/model/appratingandreview/AppRatingResponseModel;", "ratingGetAPIAction", "resetLanguage", "b", "review", "Lcom/mart/onestopkitchen/model/ratingreview/RatingReviewRequestModel;", "reviewByOrder", "Lcom/mart/onestopkitchen/model/appratingandreview/AppReviewAndRating;", "setEngLang", "engLang", "setTextToView", "string", "str", "showError", "msg", "showLoading", "showServerError", "Lcom/mart/onestopkitchen/baseview/AppErrorResponseModel;", "skipAction", "successFromServer", "t", "Lcom/mart/onestopkitchen/model/ratingreview/RatingRequestModel;", "uiListener", "validation", "app_stagingRelease"})
public final class RatingAndReviewFragment extends com.mart.onestopkitchen.ui.fragment.BaseFragment implements com.mart.onestopkitchen.mvp.ui.ratingandreview.ReviewListener, com.mart.onestopkitchen.ratingview.SmileRating.OnSmileySelectionListener, android.view.View.OnClickListener {
    private java.lang.String getRating;
    private com.mart.onestopkitchen.mvp.ui.ratingandreview.RatingAndReviewPresenter ratingAndReviewPresenter;
    private com.mart.onestopkitchen.ui.activity.MainActivity mainActivity;
    private final java.lang.String fromWhere = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer orderId = null;
    private final int productId = 0;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String orderIds;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void uiListener() {
    }
    
    private final void initUi() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public com.mart.onestopkitchen.model.appratingandreview.AppReviewAndRating reviewByOrder() {
        return null;
    }
    
    private final void resetLanguage(boolean b) {
    }
    
    private final void setEngLang(java.lang.String engLang) {
    }
    
    @java.lang.Override()
    public void orderReviewAndRating(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel model) {
    }
    
    @java.lang.Override()
    public void showServerError(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.baseview.AppErrorResponseModel msg) {
    }
    
    @java.lang.Override()
    public int productId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.mart.onestopkitchen.model.ratingreview.RatingReviewRequestModel review() {
        return null;
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View id) {
    }
    
    private final void skipAction() {
    }
    
    private final void continueAction() {
    }
    
    private final void ratingGetAPIAction() {
    }
    
    private final boolean validation() {
        return false;
    }
    
    @java.lang.Override()
    public void onSmileySelected(int smiley, boolean reselected) {
    }
    
    private final void setTextToView(java.lang.String string, java.lang.String str) {
    }
    
    @java.lang.Override()
    public void getSuccessFromServer(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.ratingreview.RatingGettingModel rating) {
    }
    
    @android.annotation.SuppressLint(value = {"LongLogTag"})
    @java.lang.Override()
    public void successFromServer(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.ratingreview.RatingRequestModel t) {
    }
    
    @java.lang.Override()
    public void showLoading() {
    }
    
    @java.lang.Override()
    public void hideLoading() {
    }
    
    @java.lang.Override()
    public void showError(@org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.content.Context getActContext() {
        return null;
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void onPrepareOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getOrderId() {
        return null;
    }
    
    public final int getProductId() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrderIds() {
        return null;
    }
    
    public final void setOrderIds(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public RatingAndReviewFragment(@org.jetbrains.annotations.NotNull()
    java.lang.String fromWhere, @org.jetbrains.annotations.Nullable()
    java.lang.Integer orderId, int productId, @org.jetbrains.annotations.Nullable()
    java.lang.String orderIds) {
        super();
    }
}