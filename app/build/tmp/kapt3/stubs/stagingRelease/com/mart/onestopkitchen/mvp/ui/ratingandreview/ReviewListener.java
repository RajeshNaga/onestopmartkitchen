package com.mart.onestopkitchen.mvp.ui.ratingandreview;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\u0012\u0010\u0006\u001a\u00020\u00032\b\u0010\u0007\u001a\u0004\u0018\u00010\bH&J\b\u0010\t\u001a\u00020\nH&J\n\u0010\u000b\u001a\u0004\u0018\u00010\fH&J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH&J\u0012\u0010\u000f\u001a\u00020\u00032\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H&\u00a8\u0006\u0012"}, d2 = {"Lcom/mart/onestopkitchen/mvp/ui/ratingandreview/ReviewListener;", "Lbasicsetup/me/com/base/LoadBaseView;", "getSuccessFromServer", "", "rating", "Lcom/mart/onestopkitchen/model/ratingreview/RatingGettingModel;", "orderReviewAndRating", "model", "Lcom/mart/onestopkitchen/model/appratingandreview/AppRatingResponseModel;", "productId", "", "review", "Lcom/mart/onestopkitchen/model/ratingreview/RatingReviewRequestModel;", "reviewByOrder", "Lcom/mart/onestopkitchen/model/appratingandreview/AppReviewAndRating;", "successFromServer", "t", "Lcom/mart/onestopkitchen/model/ratingreview/RatingRequestModel;", "app_stagingRelease"})
public abstract interface ReviewListener extends basicsetup.me.com.base.LoadBaseView {
    
    public abstract void successFromServer(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.ratingreview.RatingRequestModel t);
    
    public abstract void getSuccessFromServer(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.ratingreview.RatingGettingModel rating);
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.mart.onestopkitchen.model.ratingreview.RatingReviewRequestModel review();
    
    public abstract int productId();
    
    @org.jetbrains.annotations.Nullable()
    public abstract com.mart.onestopkitchen.model.appratingandreview.AppReviewAndRating reviewByOrder();
    
    public abstract void orderReviewAndRating(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel model);
}