package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0010R\u0016\u0010\b\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/mart/onestopkitchen/chat/utils/RingtonePlayer;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "resource", "", "(Landroid/content/Context;I)V", "TAG", "", "kotlin.jvm.PlatformType", "mediaPlayer", "Landroid/media/MediaPlayer;", "getNotification", "Landroid/net/Uri;", "play", "", "looping", "", "stop", "app_stagingRelease"})
public final class RingtonePlayer {
    private java.lang.String TAG;
    private android.media.MediaPlayer mediaPlayer;
    
    private final android.net.Uri getNotification() {
        return null;
    }
    
    public final void play(boolean looping) {
    }
    
    public final synchronized void stop() {
    }
    
    public RingtonePlayer(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    public RingtonePlayer(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int resource) {
        super();
    }
}