package com.mart.onestopkitchen.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\fR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/mart/onestopkitchen/utils/ConfirmOrderViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "placeOrder", "Lcom/mart/onestopkitchen/utils/ConfirmOrderResource;", "placeOrderResponse", "Landroidx/lifecycle/LiveData;", "Lcom/mart/onestopkitchen/model/PaymentResponseModel;", "getPlaceOrder", "request", "Lcom/mart/onestopkitchen/model/SaveTransactionStatusModel;", "releaseObj", "", "app_stagingRelease"})
public final class ConfirmOrderViewModel extends androidx.lifecycle.ViewModel {
    private androidx.lifecycle.LiveData<com.mart.onestopkitchen.model.PaymentResponseModel> placeOrderResponse;
    private com.mart.onestopkitchen.utils.ConfirmOrderResource placeOrder;
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.LiveData<com.mart.onestopkitchen.model.PaymentResponseModel> getPlaceOrder(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.model.SaveTransactionStatusModel request) {
        return null;
    }
    
    public final void releaseObj() {
    }
    
    public ConfirmOrderViewModel() {
        super();
    }
}