package com.mart.onestopkitchen.chat.views;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0015J(\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0010H\u0014J\u0012\u0010\u001b\u001a\u00020\u00132\b\b\u0001\u0010\u000f\u001a\u00020\u0010H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/mart/onestopkitchen/chat/views/MaskedImageView;", "Landroid/widget/ImageView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "boundsRect", "Landroid/graphics/Rect;", "boundsRectF", "Landroid/graphics/RectF;", "copyPaint", "Landroid/graphics/Paint;", "maskDrawable", "Landroid/graphics/drawable/Drawable;", "maskResourceId", "", "maskedPaint", "onDraw", "", "canvas", "Landroid/graphics/Canvas;", "onSizeChanged", "width", "height", "oldWidth", "oldHeight", "setMaskResourceId", "app_stagingRelease"})
public final class MaskedImageView extends android.widget.ImageView {
    private android.graphics.Paint maskedPaint;
    private android.graphics.Paint copyPaint;
    private android.graphics.drawable.Drawable maskDrawable;
    private int maskResourceId;
    private android.graphics.Rect boundsRect;
    private android.graphics.RectF boundsRectF;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
    }
    
    @android.annotation.SuppressLint(value = {"WrongConstant"})
    @java.lang.Override()
    protected void onDraw(@org.jetbrains.annotations.NotNull()
    android.graphics.Canvas canvas) {
    }
    
    private final void setMaskResourceId(@androidx.annotation.DrawableRes()
    int maskResourceId) {
    }
    
    public MaskedImageView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        super(null);
    }
}