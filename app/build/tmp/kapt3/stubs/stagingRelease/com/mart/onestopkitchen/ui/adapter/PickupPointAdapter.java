package com.mart.onestopkitchen.ui.adapter;

import java.lang.System;

/**
 * Created by Akash Garg
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0003\u001c\u001d\u001eB\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u001c\u0010\u0014\u001a\u00020\u00152\n\u0010\u0016\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0013H\u0016J\u001c\u0010\u0018\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0013H\u0016R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$PickupPointHolder;", "mContext", "Landroid/content/Context;", "pickupList", "Lcom/mart/onestopkitchen/networking/response/PickupPointAddressResponse;", "listener", "Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$SelectedPickupPoint;", "(Landroid/content/Context;Lcom/mart/onestopkitchen/networking/response/PickupPointAddressResponse;Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$SelectedPickupPoint;)V", "inflater", "Landroid/view/LayoutInflater;", "getListener", "()Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$SelectedPickupPoint;", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "OnListItemClick", "PickupPointHolder", "SelectedPickupPoint", "app_stagingRelease"})
public final class PickupPointAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.PickupPointHolder> {
    private android.view.LayoutInflater inflater;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context mContext;
    private com.mart.onestopkitchen.networking.response.PickupPointAddressResponse pickupList;
    @org.jetbrains.annotations.NotNull()
    private final com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.SelectedPickupPoint listener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.PickupPointHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.PickupPointHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.SelectedPickupPoint getListener() {
        return null;
    }
    
    public PickupPointAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.networking.response.PickupPointAddressResponse pickupList, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.ui.adapter.PickupPointAdapter.SelectedPickupPoint listener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$PickupPointHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Landroidx/databinding/ViewDataBinding;", "(Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter;Landroidx/databinding/ViewDataBinding;)V", "bind", "", "data", "Lcom/mart/onestopkitchen/model/PickupPointItem;", "app_stagingRelease"})
    public final class PickupPointHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private androidx.databinding.ViewDataBinding binding;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.model.PickupPointItem data) {
        }
        
        public PickupPointHolder(@org.jetbrains.annotations.NotNull()
        androidx.databinding.ViewDataBinding binding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\b\u0096\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0004J\u0010\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$OnListItemClick;", "Lcom/mart/onestopkitchen/presenter/PickupPointSelectedPresenter$SavePickUpPoint;", "(Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter;)V", "pickupPointAddress", "Lcom/mart/onestopkitchen/model/PickupPointItem;", "errorPickupPoint", "", "onListItemClicked", "savedPickupPoint", "isSave", "", "app_stagingRelease"})
    public class OnListItemClick implements com.mart.onestopkitchen.presenter.PickupPointSelectedPresenter.SavePickUpPoint {
        private com.mart.onestopkitchen.model.PickupPointItem pickupPointAddress;
        
        public final void onListItemClicked(@org.jetbrains.annotations.NotNull()
        com.mart.onestopkitchen.model.PickupPointItem pickupPointAddress) {
        }
        
        @java.lang.Override()
        public void savedPickupPoint(boolean isSave) {
        }
        
        @java.lang.Override()
        public void errorPickupPoint() {
        }
        
        public OnListItemClick() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/ui/adapter/PickupPointAdapter$SelectedPickupPoint;", "", "hideLoader", "", "onSelectedPickupPoint", "showLoader", "app_stagingRelease"})
    public static abstract interface SelectedPickupPoint {
        
        public abstract void onSelectedPickupPoint();
        
        public abstract void showLoader();
        
        public abstract void hideLoader();
    }
}