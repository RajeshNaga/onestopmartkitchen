package com.mart.onestopkitchen.rxjava;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007J\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\fJ\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00042\u0006\u0010\u0006\u001a\u00020\u000fJ\u001e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u00042\u0006\u0010\u0006\u001a\u00020\u000f2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0012J\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u00042\u0006\u0010\u0006\u001a\u00020\u000f\u00a8\u0006\u0016"}, d2 = {"Lcom/mart/onestopkitchen/rxjava/ApiHelper;", "", "()V", "availableSort", "Lio/reactivex/Observable;", "Lcom/mart/onestopkitchen/model/featureproduct/FeatureProductResponseModel;", "request", "", "size", "getRatingAndReviewByOrder", "Lcom/mart/onestopkitchen/model/appratingandreview/AppRatingResponseModel;", "review", "Lcom/mart/onestopkitchen/model/appratingandreview/AppReviewAndRating;", "getRatingAndReviewOrder", "Lcom/mart/onestopkitchen/model/ratingreview/RatingGettingModel;", "", "getRatingAndReviewProduct", "Lcom/mart/onestopkitchen/model/ratingreview/RatingRequestModel;", "Lcom/mart/onestopkitchen/model/ratingreview/RatingReviewRequestModel;", "setUserLanguage", "Lcom/mart/onestopkitchen/model/OrderDetailsResponse;", "Companion", "app_stagingRelease"})
public final class ApiHelper {
    private static com.mart.onestopkitchen.networking.Api serviceFactory;
    private static com.mart.onestopkitchen.rxjava.ApiHelper helper;
    public static final com.mart.onestopkitchen.rxjava.ApiHelper.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<com.mart.onestopkitchen.model.ratingreview.RatingRequestModel> getRatingAndReviewProduct(int request, @org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.ratingreview.RatingReviewRequestModel review) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<com.mart.onestopkitchen.model.ratingreview.RatingGettingModel> getRatingAndReviewOrder(int request) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<com.mart.onestopkitchen.model.OrderDetailsResponse> setUserLanguage(int request) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<com.mart.onestopkitchen.model.featureproduct.FeatureProductResponseModel> availableSort(@org.jetbrains.annotations.NotNull()
    java.lang.String request, @org.jetbrains.annotations.NotNull()
    java.lang.String size) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel> getRatingAndReviewByOrder(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.appratingandreview.AppReviewAndRating review) {
        return null;
    }
    
    public ApiHelper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/mart/onestopkitchen/rxjava/ApiHelper$Companion;", "", "()V", "helper", "Lcom/mart/onestopkitchen/rxjava/ApiHelper;", "serviceFactory", "Lcom/mart/onestopkitchen/networking/Api;", "getDataHelper", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.rxjava.ApiHelper getDataHelper() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}