package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u001c\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"sendPushMessage", "", "recipients", "Ljava/util/ArrayList;", "", "senderName", "", "app_stagingRelease"})
public final class PushNotificationSenderKt {
    
    public static final void sendPushMessage(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Integer> recipients, @org.jetbrains.annotations.NotNull()
    java.lang.String senderName) {
    }
}