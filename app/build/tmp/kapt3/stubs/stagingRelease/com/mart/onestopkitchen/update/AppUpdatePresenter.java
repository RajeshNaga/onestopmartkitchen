package com.mart.onestopkitchen.update;

import java.lang.System;

/**
 * @author AKASH GARG -: 9 JAN-2020.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\t0\u0013H\u0002J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002R\u0016\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0015"}, d2 = {"Lcom/mart/onestopkitchen/update/AppUpdatePresenter;", "", "mContext", "Landroid/content/Context;", "listener", "Lcom/mart/onestopkitchen/baseview/AppUpdateView;", "(Landroid/content/Context;Lcom/mart/onestopkitchen/baseview/AppUpdateView;)V", "disposableObserver", "Lio/reactivex/observers/DisposableObserver;", "Lcom/mart/onestopkitchen/update/AppUpdateModel;", "getListener", "()Lcom/mart/onestopkitchen/baseview/AppUpdateView;", "setListener", "(Lcom/mart/onestopkitchen/baseview/AppUpdateView;)V", "getMContext", "()Landroid/content/Context;", "getAppUpdate", "", "getAppUpdateObservable", "Lio/reactivex/Observable;", "getObserver", "app_stagingRelease"})
public final class AppUpdatePresenter {
    private io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.update.AppUpdateModel> disposableObserver;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context mContext = null;
    @org.jetbrains.annotations.NotNull()
    private com.mart.onestopkitchen.baseview.AppUpdateView listener;
    
    private final void getAppUpdate() {
    }
    
    private final io.reactivex.Observable<com.mart.onestopkitchen.update.AppUpdateModel> getAppUpdateObservable() {
        return null;
    }
    
    private final io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.update.AppUpdateModel> getObserver() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.baseview.AppUpdateView getListener() {
        return null;
    }
    
    public final void setListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.baseview.AppUpdateView p0) {
    }
    
    public AppUpdatePresenter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.baseview.AppUpdateView listener) {
        super();
    }
}