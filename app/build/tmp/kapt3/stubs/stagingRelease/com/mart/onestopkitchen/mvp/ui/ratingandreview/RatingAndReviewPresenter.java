package com.mart.onestopkitchen.mvp.ui.ratingandreview;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\t\u001a\u00020\nH\u0007J\b\u0010\u000b\u001a\u00020\nH\u0007J\b\u0010\f\u001a\u00020\rH\u0002J\b\u0010\u000e\u001a\u00020\nH\u0007J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/mart/onestopkitchen/mvp/ui/ratingandreview/RatingAndReviewPresenter;", "Lcom/mart/onestopkitchen/baseview/AbstractBasePresenter;", "Lcom/mart/onestopkitchen/mvp/ui/ratingandreview/ReviewListener;", "mContext", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getMContext", "()Landroid/content/Context;", "reviewListener", "callReviewApi", "", "callReviewApiNew", "getProductId", "", "getcallReviewApi", "setView", "getView", "showLoading", "status", "", "app_stagingRelease"})
public final class RatingAndReviewPresenter extends com.mart.onestopkitchen.baseview.AbstractBasePresenter<com.mart.onestopkitchen.mvp.ui.ratingandreview.ReviewListener> {
    private com.mart.onestopkitchen.mvp.ui.ratingandreview.ReviewListener reviewListener;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context mContext = null;
    
    @java.lang.Override()
    public void setView(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.mvp.ui.ratingandreview.ReviewListener getView) {
    }
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    public final void callReviewApi() {
    }
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    public final void getcallReviewApi() {
    }
    
    private final int getProductId() {
        return 0;
    }
    
    private final void showLoading(boolean status) {
    }
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    public final void callReviewApiNew() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public RatingAndReviewPresenter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext) {
        super();
    }
}