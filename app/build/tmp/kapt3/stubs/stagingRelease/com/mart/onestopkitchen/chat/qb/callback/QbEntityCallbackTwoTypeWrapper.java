package com.mart.onestopkitchen.chat.qb.callback;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u001f\u0010\r\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00028\u00002\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016\u00a2\u0006\u0002\u0010\u0011J\u001f\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00028\u00012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0004\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/mart/onestopkitchen/chat/qb/callback/QbEntityCallbackTwoTypeWrapper;", "T", "R", "Lcom/quickblox/core/QBEntityCallback;", "callback", "(Lcom/quickblox/core/QBEntityCallback;)V", "mainThreadHandler", "Landroid/os/Handler;", "onError", "", "error", "Lcom/quickblox/core/exception/QBResponseException;", "onErrorInMainThread", "onSuccess", "t", "bundle", "Landroid/os/Bundle;", "(Ljava/lang/Object;Landroid/os/Bundle;)V", "onSuccessInMainThread", "result", "app_stagingRelease"})
public class QbEntityCallbackTwoTypeWrapper<T extends java.lang.Object, R extends java.lang.Object> implements com.quickblox.core.QBEntityCallback<T> {
    private final android.os.Handler mainThreadHandler = null;
    private com.quickblox.core.QBEntityCallback<R> callback;
    
    @java.lang.Override()
    public void onSuccess(T t, @org.jetbrains.annotations.Nullable()
    android.os.Bundle bundle) {
    }
    
    @java.lang.Override()
    public void onError(@org.jetbrains.annotations.NotNull()
    com.quickblox.core.exception.QBResponseException error) {
    }
    
    protected final void onSuccessInMainThread(R result, @org.jetbrains.annotations.Nullable()
    android.os.Bundle bundle) {
    }
    
    private final void onErrorInMainThread(com.quickblox.core.exception.QBResponseException error) {
    }
    
    public QbEntityCallbackTwoTypeWrapper(@org.jetbrains.annotations.NotNull()
    com.quickblox.core.QBEntityCallback<R> callback) {
        super();
    }
}