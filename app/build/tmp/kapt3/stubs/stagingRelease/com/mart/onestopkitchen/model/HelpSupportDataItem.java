package com.mart.onestopkitchen.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/model/HelpSupportDataItem;", "", "Value", "", "Title", "(Ljava/lang/String;Ljava/lang/String;)V", "getTitle", "()Ljava/lang/String;", "getValue", "app_stagingRelease"})
public final class HelpSupportDataItem {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String Value = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String Title = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getValue() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTitle() {
        return null;
    }
    
    public HelpSupportDataItem(@org.jetbrains.annotations.Nullable()
    java.lang.String Value, @org.jetbrains.annotations.Nullable()
    java.lang.String Title) {
        super();
    }
    
    public HelpSupportDataItem() {
        super();
    }
}