package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\u0010\u0010\u0000\u001a\u00020\u00012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0002\u001a\u00020\u00042\b\b\u0001\u0010\u0007\u001a\u00020\u0003H\u0002\u001a\u0010\u0010\b\u001a\u00020\u00012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u001a\u000e\u0010\b\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0004\u001a\u001a\u0010\t\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00042\b\b\u0001\u0010\u0007\u001a\u00020\u0003H\u0002\u00a8\u0006\n"}, d2 = {"longToast", "", "text", "", "", "makeToast", "Landroid/widget/Toast;", "length", "shortToast", "show", "app_stagingRelease"})
public final class ToastUtilsKt {
    
    public static final void shortToast(@androidx.annotation.StringRes()
    int text) {
    }
    
    public static final void shortToast(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public static final void longToast(@androidx.annotation.StringRes()
    int text) {
    }
    
    public static final void longToast(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    private static final android.widget.Toast makeToast(java.lang.String text, @ToastLength()
    int length) {
        return null;
    }
    
    private static final void show(java.lang.String text, @ToastLength()
    int length) {
    }
}