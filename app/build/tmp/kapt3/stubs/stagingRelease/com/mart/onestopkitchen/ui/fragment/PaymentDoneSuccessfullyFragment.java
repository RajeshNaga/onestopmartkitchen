package com.mart.onestopkitchen.ui.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\tH\u0002J\b\u0010\u0012\u001a\u00020\u0010H\u0002J&\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u001a\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u00142\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010\u001d\u001a\u00020\u0010H\u0002J\b\u0010\u001e\u001a\u00020\u0010H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u000e\u0010\u000e\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/mart/onestopkitchen/ui/fragment/PaymentDoneSuccessfullyFragment;", "Lcom/mart/onestopkitchen/ui/fragment/BaseFragment;", "()V", "TAG", "", "kotlin.jvm.PlatformType", "binding", "Landroidx/databinding/ViewDataBinding;", "data", "Lcom/mart/onestopkitchen/model/PaymentResponseModel;", "getData", "()Lcom/mart/onestopkitchen/model/PaymentResponseModel;", "setData", "(Lcom/mart/onestopkitchen/model/PaymentResponseModel;)V", "pdfFileName", "createPdfInvoice", "", "response", "nextFragment", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "pdfSharing", "setPaymentResponse", "app_stagingRelease"})
public final class PaymentDoneSuccessfullyFragment extends com.mart.onestopkitchen.ui.fragment.BaseFragment {
    private final java.lang.String TAG = null;
    private androidx.databinding.ViewDataBinding binding;
    @org.jetbrains.annotations.Nullable()
    private com.mart.onestopkitchen.model.PaymentResponseModel data;
    private java.lang.String pdfFileName;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.mart.onestopkitchen.model.PaymentResponseModel getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.model.PaymentResponseModel p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void nextFragment() {
    }
    
    private final void setPaymentResponse() {
    }
    
    private final void createPdfInvoice(com.mart.onestopkitchen.model.PaymentResponseModel response) {
    }
    
    private final void pdfSharing() {
    }
    
    public PaymentDoneSuccessfullyFragment() {
        super();
    }
}