package com.mart.onestopkitchen.chat.views;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\u000e\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\bR\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/mart/onestopkitchen/chat/views/AttachmentPreviewAdapterView;", "Landroid/widget/HorizontalScrollView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "adapter", "Landroid/widget/Adapter;", "container", "Landroid/widget/LinearLayout;", "dataSetObserver", "Landroid/database/DataSetObserver;", "mainThreadHandler", "Landroid/os/Handler;", "populateWithViewsFromAdapter", "", "setAdapter", "newAdapter", "app_stagingRelease"})
public final class AttachmentPreviewAdapterView extends android.widget.HorizontalScrollView {
    private final android.os.Handler mainThreadHandler = null;
    private final android.widget.LinearLayout container = null;
    private android.widget.Adapter adapter;
    private final android.database.DataSetObserver dataSetObserver = null;
    private java.util.HashMap _$_findViewCache;
    
    public final void setAdapter(@org.jetbrains.annotations.NotNull()
    android.widget.Adapter newAdapter) {
    }
    
    private final void populateWithViewsFromAdapter() {
    }
    
    public AttachmentPreviewAdapterView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        super(null);
    }
}