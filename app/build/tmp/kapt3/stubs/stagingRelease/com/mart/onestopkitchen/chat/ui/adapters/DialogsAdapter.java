package com.mart.onestopkitchen.chat.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u00011B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u0016\u001a\u00020\u0017J\b\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u0019H\u0016J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0019H\u0016J\u0010\u0010\u001f\u001a\u00020\u00192\u0006\u0010 \u001a\u00020\u0006H\u0002J\"\u0010!\u001a\u00020\"2\u0006\u0010\u001b\u001a\u00020\u00192\b\u0010#\u001a\u0004\u0018\u00010\"2\u0006\u0010$\u001a\u00020%H\u0016J\u0010\u0010&\u001a\u00020\'2\u0006\u0010\u001b\u001a\u00020\u0019H\u0002J\u0010\u0010(\u001a\u00020\'2\u0006\u0010)\u001a\u00020\u0006H\u0002J\u0010\u0010*\u001a\u00020+2\u0006\u0010 \u001a\u00020\u0006H\u0002J\u000e\u0010,\u001a\u00020\u00172\u0006\u0010-\u001a\u00020\u0006J\u000e\u0010.\u001a\u00020\u00172\u0006\u0010-\u001a\u00020\u0006J\u0014\u0010/\u001a\u00020\u00172\f\u00100\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005R\u001e\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\u00060\tj\b\u0012\u0004\u0012\u00020\u0006`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R!\u0010\u0013\u001a\u0012\u0012\u0004\u0012\u00020\u00060\tj\b\u0012\u0004\u0012\u00020\u0006`\n8F\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015\u00a8\u00062"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/DialogsAdapter;", "Landroid/widget/BaseAdapter;", "context", "Landroid/content/Context;", "dialogs", "", "Lcom/quickblox/chat/model/QBChatDialog;", "(Landroid/content/Context;Ljava/util/List;)V", "_selectedItems", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "getContext", "()Landroid/content/Context;", "setContext", "(Landroid/content/Context;)V", "getDialogs", "()Ljava/util/List;", "setDialogs", "(Ljava/util/List;)V", "selectedItems", "getSelectedItems", "()Ljava/util/ArrayList;", "clearSelection", "", "getCount", "", "getItem", "position", "getItemId", "", "id", "getUnreadMsgCount", "chatDialog", "getView", "Landroid/view/View;", "convertView", "parent", "Landroid/view/ViewGroup;", "isItemSelected", "", "isLastMessageAttachment", "dialog", "prepareTextLastMessage", "", "selectItem", "item", "toggleSelection", "updateList", "newData", "ViewHolder", "app_stagingRelease"})
public final class DialogsAdapter extends android.widget.BaseAdapter {
    private java.util.ArrayList<com.quickblox.chat.model.QBChatDialog> _selectedItems;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends com.quickblox.chat.model.QBChatDialog> dialogs;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.quickblox.chat.model.QBChatDialog> getSelectedItems() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View getView(int position, @org.jetbrains.annotations.Nullable()
    android.view.View convertView, @org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.quickblox.chat.model.QBChatDialog getItem(int position) {
        return null;
    }
    
    @java.lang.Override()
    public long getItemId(int id) {
        return 0L;
    }
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    private final boolean isItemSelected(int position) {
        return false;
    }
    
    private final int getUnreadMsgCount(com.quickblox.chat.model.QBChatDialog chatDialog) {
        return 0;
    }
    
    private final boolean isLastMessageAttachment(com.quickblox.chat.model.QBChatDialog dialog) {
        return false;
    }
    
    private final java.lang.String prepareTextLastMessage(com.quickblox.chat.model.QBChatDialog chatDialog) {
        return null;
    }
    
    public final void clearSelection() {
    }
    
    public final void updateList(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.chat.model.QBChatDialog> newData) {
    }
    
    public final void selectItem(@org.jetbrains.annotations.NotNull()
    com.quickblox.chat.model.QBChatDialog item) {
    }
    
    public final void toggleSelection(@org.jetbrains.annotations.NotNull()
    com.quickblox.chat.model.QBChatDialog item) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.quickblox.chat.model.QBChatDialog> getDialogs() {
        return null;
    }
    
    public final void setDialogs(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.chat.model.QBChatDialog> p0) {
    }
    
    public DialogsAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.quickblox.chat.model.QBChatDialog> dialogs) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\f\"\u0004\b\u0011\u0010\u000eR\u001a\u0010\u0012\u001a\u00020\u0013X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0018\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\f\"\u0004\b\u001a\u0010\u000e\u00a8\u0006\u001b"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/DialogsAdapter$ViewHolder;", "", "()V", "dialogImageView", "Landroid/widget/ImageView;", "getDialogImageView", "()Landroid/widget/ImageView;", "setDialogImageView", "(Landroid/widget/ImageView;)V", "lastMessageTextView", "Landroid/widget/TextView;", "getLastMessageTextView", "()Landroid/widget/TextView;", "setLastMessageTextView", "(Landroid/widget/TextView;)V", "nameTextView", "getNameTextView", "setNameTextView", "rootLayout", "Landroid/view/ViewGroup;", "getRootLayout", "()Landroid/view/ViewGroup;", "setRootLayout", "(Landroid/view/ViewGroup;)V", "unreadCounterTextView", "getUnreadCounterTextView", "setUnreadCounterTextView", "app_stagingRelease"})
    static final class ViewHolder {
        @org.jetbrains.annotations.NotNull()
        public android.view.ViewGroup rootLayout;
        @org.jetbrains.annotations.NotNull()
        public android.widget.ImageView dialogImageView;
        @org.jetbrains.annotations.NotNull()
        public android.widget.TextView nameTextView;
        @org.jetbrains.annotations.NotNull()
        public android.widget.TextView lastMessageTextView;
        @org.jetbrains.annotations.NotNull()
        public android.widget.TextView unreadCounterTextView;
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.ViewGroup getRootLayout() {
            return null;
        }
        
        public final void setRootLayout(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getDialogImageView() {
            return null;
        }
        
        public final void setDialogImageView(@org.jetbrains.annotations.NotNull()
        android.widget.ImageView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getNameTextView() {
            return null;
        }
        
        public final void setNameTextView(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getLastMessageTextView() {
            return null;
        }
        
        public final void setLastMessageTextView(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getUnreadCounterTextView() {
            return null;
        }
        
        public final void setUnreadCounterTextView(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        public ViewHolder() {
            super();
        }
    }
}