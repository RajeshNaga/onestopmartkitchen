package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0002\u0010\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\n\u001a\u00020\u000bH\u0002J\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\tJ\u000e\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\tR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/mart/onestopkitchen/chat/utils/NetworkConnectionChecker;", "", "context", "Landroid/app/Application;", "(Landroid/app/Application;)V", "connectivityManager", "Landroid/net/ConnectivityManager;", "listeners", "Ljava/util/concurrent/CopyOnWriteArraySet;", "Lcom/mart/onestopkitchen/chat/utils/NetworkConnectionChecker$OnConnectivityChangedListener;", "isConnectedNow", "", "registerListener", "", "listener", "unregisterListener", "NetworkStateReceiver", "OnConnectivityChangedListener", "app_stagingRelease"})
public final class NetworkConnectionChecker {
    private final android.net.ConnectivityManager connectivityManager = null;
    private final java.util.concurrent.CopyOnWriteArraySet<com.mart.onestopkitchen.chat.utils.NetworkConnectionChecker.OnConnectivityChangedListener> listeners = null;
    
    public final void registerListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.utils.NetworkConnectionChecker.OnConnectivityChangedListener listener) {
    }
    
    public final void unregisterListener(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.utils.NetworkConnectionChecker.OnConnectivityChangedListener listener) {
    }
    
    private final boolean isConnectedNow() {
        return false;
    }
    
    public NetworkConnectionChecker(@org.jetbrains.annotations.NotNull()
    android.app.Application context) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/chat/utils/NetworkConnectionChecker$NetworkStateReceiver;", "Landroid/content/BroadcastReceiver;", "(Lcom/mart/onestopkitchen/chat/utils/NetworkConnectionChecker;)V", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "app_stagingRelease"})
    final class NetworkStateReceiver extends android.content.BroadcastReceiver {
        
        @java.lang.Override()
        public void onReceive(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        android.content.Intent intent) {
        }
        
        public NetworkStateReceiver() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/chat/utils/NetworkConnectionChecker$OnConnectivityChangedListener;", "", "connectivityChanged", "", "availableNow", "", "app_stagingRelease"})
    public static abstract interface OnConnectivityChangedListener {
        
        public abstract void connectivityChanged(boolean availableNow);
    }
}