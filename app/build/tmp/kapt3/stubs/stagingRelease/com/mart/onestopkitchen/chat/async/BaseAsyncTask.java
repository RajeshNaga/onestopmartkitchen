package com.mart.onestopkitchen.chat.async;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b&\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u0002*\u0004\b\u0002\u0010\u00032\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J#\u0010\r\u001a\u0004\u0018\u00018\u00022\u0012\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u000f\"\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010\u0010J\u0014\u0010\u0011\u001a\u00020\u00122\n\u0010\u0013\u001a\u00060\u0014j\u0002`\u0015H\u0016J\u0015\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00028\u0002H\u0014\u00a2\u0006\u0002\u0010\u0018J\u0017\u0010\u0019\u001a\u00020\u00122\b\u0010\u0017\u001a\u0004\u0018\u00018\u0002H&\u00a2\u0006\u0002\u0010\u0018J#\u0010\u001a\u001a\u0004\u0018\u00018\u00022\u0012\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u000f\"\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0010R\u0016\u0010\u0006\u001a\n \b*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/mart/onestopkitchen/chat/async/BaseAsyncTask;", "Params", "Progress", "Result", "Landroid/os/AsyncTask;", "()V", "TAG", "", "kotlin.jvm.PlatformType", "isExceptionOccurred", "", "mainThreadHandler", "Landroid/os/Handler;", "doInBackground", "params", "", "([Ljava/lang/Object;)Ljava/lang/Object;", "onException", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onPostExecute", "result", "(Ljava/lang/Object;)V", "onResult", "performInBackground", "app_stagingRelease"})
public abstract class BaseAsyncTask<Params extends java.lang.Object, Progress extends java.lang.Object, Result extends java.lang.Object> extends android.os.AsyncTask<Params, Progress, Result> {
    private final java.lang.String TAG = null;
    private final android.os.Handler mainThreadHandler = null;
    private boolean isExceptionOccurred;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected Result doInBackground(@org.jetbrains.annotations.NotNull()
    Params... params) {
        return null;
    }
    
    @java.lang.Override()
    protected void onPostExecute(Result result) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public abstract Result performInBackground(@org.jetbrains.annotations.NotNull()
    Params... params);
    
    public abstract void onResult(@org.jetbrains.annotations.Nullable()
    Result result);
    
    public void onException(@org.jetbrains.annotations.NotNull()
    java.lang.Exception e) {
    }
    
    public BaseAsyncTask() {
        super();
    }
}