package com.mart.onestopkitchen.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0004J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bH\u0007\u00a8\u0006\u000e"}, d2 = {"Lcom/mart/onestopkitchen/utils/LocaleUtils;", "", "()V", "extractLocaleCountry", "", "langCode", "extractLocaleLanguage", "transform", "Ljava/util/Locale;", "updateLocaleConfiguration", "Landroid/content/res/Configuration;", "activity", "Landroid/content/Context;", "newLocale", "app_stagingRelease"})
public final class LocaleUtils {
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Locale transform(@org.jetbrains.annotations.NotNull()
    java.lang.String langCode) {
        return null;
    }
    
    private final java.lang.String extractLocaleCountry(java.lang.String langCode) {
        return null;
    }
    
    private final java.lang.String extractLocaleLanguage(java.lang.String langCode) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.JELLY_BEAN_MR1)
    public final android.content.res.Configuration updateLocaleConfiguration(@org.jetbrains.annotations.NotNull()
    android.content.Context activity, @org.jetbrains.annotations.NotNull()
    java.util.Locale newLocale) {
        return null;
    }
    
    public LocaleUtils() {
        super();
    }
}