package com.mart.onestopkitchen.utils.location;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u001d\u001a\u00020\u001eH\u0002J\b\u0010\u001f\u001a\u00020\u0013H\u0002J\u0006\u0010 \u001a\u00020\u001eJ\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\r0\"J\b\u0010#\u001a\u00020$H\u0002J\u000e\u0010%\u001a\u00020\r2\u0006\u0010&\u001a\u00020\'J\u000e\u0010(\u001a\u00020\r2\u0006\u0010)\u001a\u00020\'J\u0012\u0010*\u001a\u00020\u001e2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\u0010\u0010-\u001a\u00020\u001e2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00100\u001a\u00020\u001e2\u0006\u0010.\u001a\u00020\bH\u0016J\"\u00101\u001a\u00020\u001e2\u0006\u00102\u001a\u00020\b2\u0006\u00103\u001a\u00020\b2\b\u00104\u001a\u0004\u0018\u000105H\u0016J-\u00106\u001a\u00020\u001e2\u0006\u00102\u001a\u00020\b2\u000e\u00107\u001a\n\u0012\u0006\b\u0001\u0012\u000209082\u0006\u0010:\u001a\u00020;H\u0016\u00a2\u0006\u0002\u0010<J\b\u0010=\u001a\u00020\u001eH\u0002J\b\u0010>\u001a\u00020\u001eH\u0002J\b\u0010?\u001a\u00020\u001eH\u0002J\b\u0010@\u001a\u00020\u001eH\u0002J\b\u0010A\u001a\u00020\rH\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006B"}, d2 = {"Lcom/mart/onestopkitchen/utils/location/LocationSettings;", "Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;", "Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;", "Lcom/mart/onestopkitchen/utils/callback/LocationRequestCallback;", "mActivity", "Landroidx/appcompat/app/AppCompatActivity;", "(Landroidx/appcompat/app/AppCompatActivity;)V", "RC_LOCATION_CHECK_SETTINGS", "", "RC_LOCATION_RUNTIME_PERMISSION", "RC_PLAY_SERVICES_RESOLUTION", "emitter", "Lio/reactivex/ObservableEmitter;", "", "isAskLocationCancled", "isShownErroDialog", "getMActivity", "()Landroidx/appcompat/app/AppCompatActivity;", "mAlert", "Landroid/app/AlertDialog;", "getMAlert", "()Landroid/app/AlertDialog;", "setMAlert", "(Landroid/app/AlertDialog;)V", "mGoogleApiClient", "Lcom/google/android/gms/common/api/GoogleApiClient;", "runtimePermissionDenied", "settingsClient", "Lcom/google/android/gms/location/SettingsClient;", "freeResource", "", "getDialog", "getLocationOnObserver", "getObserver", "Lio/reactivex/Observable;", "initLocationRequest", "Lcom/google/android/gms/location/LocationRequest;", "isLocationEnabled", "context", "Landroid/content/Context;", "isValidAppPermission", "mContext", "onConnected", "bundle", "Landroid/os/Bundle;", "onConnectionFailed", "connectionResult", "Lcom/google/android/gms/common/ConnectionResult;", "onConnectionSuspended", "onPermissionActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onPermissionRequestResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "openLocationSetting", "requestCurrentLocation", "requestLocationUpdates", "showDenyPopUp", "validateAlertShowing", "app_stagingRelease"})
public final class LocationSettings implements com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener, com.mart.onestopkitchen.utils.callback.LocationRequestCallback {
    private final int RC_PLAY_SERVICES_RESOLUTION = 9000;
    private final int RC_LOCATION_CHECK_SETTINGS = 405;
    private final int RC_LOCATION_RUNTIME_PERMISSION = 501;
    private com.google.android.gms.common.api.GoogleApiClient mGoogleApiClient;
    private com.google.android.gms.location.SettingsClient settingsClient;
    private boolean isShownErroDialog;
    private boolean runtimePermissionDenied;
    private boolean isAskLocationCancled;
    private io.reactivex.ObservableEmitter<java.lang.Boolean> emitter;
    @org.jetbrains.annotations.Nullable()
    private android.app.AlertDialog mAlert;
    @org.jetbrains.annotations.NotNull()
    private final androidx.appcompat.app.AppCompatActivity mActivity = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.AlertDialog getMAlert() {
        return null;
    }
    
    public final void setMAlert(@org.jetbrains.annotations.Nullable()
    android.app.AlertDialog p0) {
    }
    
    @java.lang.Override()
    public void onConnectionFailed(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.common.ConnectionResult connectionResult) {
    }
    
    @java.lang.Override()
    public void onConnected(@org.jetbrains.annotations.Nullable()
    android.os.Bundle bundle) {
    }
    
    @java.lang.Override()
    public void onConnectionSuspended(int connectionResult) {
    }
    
    @java.lang.Override()
    public void onPermissionRequestResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final void showDenyPopUp() {
    }
    
    private final android.app.AlertDialog getDialog() {
        return null;
    }
    
    private final void openLocationSetting() {
    }
    
    @java.lang.Override()
    public void onPermissionActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public final boolean isLocationEnabled(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final boolean isValidAppPermission(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext) {
        return false;
    }
    
    private final void requestLocationUpdates() {
    }
    
    private final boolean validateAlertShowing() {
        return false;
    }
    
    private final void requestCurrentLocation() {
    }
    
    private final com.google.android.gms.location.LocationRequest initLocationRequest() {
        return null;
    }
    
    public final void getLocationOnObserver() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observable<java.lang.Boolean> getObserver() {
        return null;
    }
    
    private final void freeResource() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.appcompat.app.AppCompatActivity getMActivity() {
        return null;
    }
    
    public LocationSettings(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity) {
        super();
    }
}