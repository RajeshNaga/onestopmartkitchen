package com.mart.onestopkitchen.chat.ui.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00e4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\t\n\u0002\b\u000b\u0018\u0000 \u0093\u00012\u00020\u00012\u00020\u00022\b\u0012\u0004\u0012\u00020\u00040\u00032\u00020\u00052\u00020\u00062\u00020\u0007:\f\u0091\u0001\u0092\u0001\u0093\u0001\u0094\u0001\u0095\u0001\u0096\u0001B\u0005\u00a2\u0006\u0002\u0010\bJ\u001c\u0010!\u001a\u00020\"2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0$H\u0016J\u0012\u0010%\u001a\u00020\"2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u0010\u0010(\u001a\u00020\"2\u0006\u0010)\u001a\u00020\u0016H\u0002J\u0012\u0010*\u001a\u00020\"2\b\u0010+\u001a\u0004\u0018\u00010\u0014H\u0016J\b\u0010,\u001a\u00020\"H\u0002J\b\u0010-\u001a\u00020\"H\u0002J\u0012\u0010.\u001a\u00020\"2\b\u0010/\u001a\u0004\u0018\u000100H\u0016J\u0012\u00101\u001a\u00020\"2\b\u00102\u001a\u0004\u0018\u000103H\u0016J\u0016\u00104\u001a\u00020\"2\f\u00105\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0003H\u0016J\u0018\u00106\u001a\u00020\"2\u000e\u00107\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u000108H\u0016J\b\u00109\u001a\u00020\"H\u0002J\b\u0010:\u001a\u00020\u0016H\u0016J\b\u0010;\u001a\u00020\"H\u0016J\u000f\u0010<\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010=J\n\u0010>\u001a\u0004\u0018\u00010?H\u0016J\u0010\u0010@\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019H\u0016J\u0012\u0010A\u001a\u0004\u0018\u00010B2\u0006\u0010C\u001a\u00020\u001aH\u0016J\u0012\u0010D\u001a\u0004\u0018\u00010E2\u0006\u0010C\u001a\u00020\u001aH\u0016J\u0014\u0010F\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020E0GH\u0016J\b\u0010H\u001a\u00020\"H\u0002J\b\u0010I\u001a\u00020\"H\u0002J\b\u0010J\u001a\u00020\"H\u0002J\b\u0010K\u001a\u00020\"H\u0002J\b\u0010L\u001a\u00020\u0016H\u0016J\b\u0010M\u001a\u00020\u0016H\u0016J\b\u0010N\u001a\u00020\"H\u0002J\b\u0010O\u001a\u00020\"H\u0002J\u0010\u0010P\u001a\u00020\"2\u0006\u0010Q\u001a\u00020\nH\u0002J \u0010R\u001a\u00020\"2\u0016\u0010S\u001a\u0012\u0012\u0004\u0012\u00020T0\u0013j\b\u0012\u0004\u0012\u00020T`UH\u0002J\b\u0010V\u001a\u00020\"H\u0016J\"\u0010W\u001a\u00020\"2\u0006\u0010X\u001a\u00020\u001a2\u0006\u0010Y\u001a\u00020\u001a2\b\u0010Z\u001a\u0004\u0018\u00010[H\u0014J\b\u0010\\\u001a\u00020\"H\u0016J7\u0010]\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001a2\u0014\u0010_\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n\u0018\u00010GH\u0016\u00a2\u0006\u0002\u0010`J7\u0010a\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001a2\u0014\u0010_\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n\u0018\u00010GH\u0016\u00a2\u0006\u0002\u0010`J!\u0010b\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010cJ!\u0010d\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010cJ\u0012\u0010e\u001a\u00020\"2\b\u0010f\u001a\u0004\u0018\u00010gH\u0014J!\u0010h\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010cJ\b\u0010i\u001a\u00020\"H\u0016J\b\u0010j\u001a\u00020\"H\u0014J7\u0010k\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001a2\u0014\u0010_\u001a\u0010\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n\u0018\u00010GH\u0016\u00a2\u0006\u0002\u0010`J\u0012\u0010l\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u0004H\u0016J\b\u0010m\u001a\u00020\"H\u0016J\b\u0010n\u001a\u00020\"H\u0014J\u0012\u0010o\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u0004H\u0016J\u0012\u0010p\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u0004H\u0016J\u0010\u0010q\u001a\u00020\"2\u0006\u0010r\u001a\u00020\u0016H\u0016J\u0010\u0010s\u001a\u00020\"2\u0006\u0010t\u001a\u00020\u0016H\u0016J\b\u0010u\u001a\u00020\"H\u0017J\u001c\u0010v\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010w\u001a\u0004\u0018\u00010?H\u0016J\b\u0010x\u001a\u00020\"H\u0016J\b\u0010y\u001a\u00020\"H\u0016J\u0010\u0010z\u001a\u00020\"2\u0006\u0010{\u001a\u00020|H\u0016J!\u0010}\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010cJ!\u0010~\u001a\u00020\"2\b\u0010^\u001a\u0004\u0018\u00010\u00042\b\u0010C\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010cJ\u0012\u0010\u007f\u001a\u00020\"2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u0013\u0010\u0080\u0001\u001a\u00020\"2\b\u0010+\u001a\u0004\u0018\u00010\u0014H\u0016J\t\u0010\u0081\u0001\u001a\u00020\"H\u0002J\u0013\u0010\u0082\u0001\u001a\u00020\"2\b\u0010/\u001a\u0004\u0018\u000100H\u0016J\u0013\u0010\u0083\u0001\u001a\u00020\"2\b\u00102\u001a\u0004\u0018\u000103H\u0016J\u0017\u0010\u0084\u0001\u001a\u00020\"2\f\u00105\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0003H\u0016J\u0019\u0010\u0085\u0001\u001a\u00020\"2\u000e\u00107\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u000108H\u0016J\u001b\u0010\u0086\u0001\u001a\u00020\"2\u0007\u0010\u0087\u0001\u001a\u00020\u001a2\u0007\u0010\u0088\u0001\u001a\u00020\u0016H\u0002J\u001d\u0010\u0089\u0001\u001a\u00020\"2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0$H\u0016J\u0013\u0010\u008a\u0001\u001a\u00020\"2\b\u0010\u008b\u0001\u001a\u00030\u008c\u0001H\u0002J\t\u0010\u008d\u0001\u001a\u00020\"H\u0002J\u0013\u0010\u008e\u0001\u001a\u00020\"2\b\u0010Z\u001a\u0004\u0018\u00010[H\u0002J\u0011\u0010\u008f\u0001\u001a\u00020\"2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\t\u0010\u0090\u0001\u001a\u00020\"H\u0002R\u0016\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0018\u00010\u0011R\u00020\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0097\u0001"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity;", "Lcom/mart/onestopkitchen/ui/activity/BaseActivity;", "Lcom/mart/onestopkitchen/chat/ui/fragments/IncomeCallFragmentCallbackListener;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionStateCallback;", "Lcom/quickblox/videochat/webrtc/QBRTCSession;", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientSessionCallbacks;", "Lcom/mart/onestopkitchen/chat/ui/fragments/ConversationFragmentCallback;", "Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment$OnSharingEvents;", "()V", "TAG", "", "kotlin.jvm.PlatformType", "callService", "Lcom/mart/onestopkitchen/chat/services/CallService;", "callServiceConnection", "Landroid/content/ServiceConnection;", "connectionListener", "Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$ConnectionListenerImpl;", "currentCallStateCallbackList", "Ljava/util/ArrayList;", "Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$CurrentCallStateCallback;", "isInComingCall", "", "isVideoCall", "opponentsIdsList", "", "", "sharedPref", "Landroid/content/SharedPreferences;", "showIncomingCallWindowTask", "Ljava/lang/Runnable;", "showIncomingCallWindowTaskHandler", "Landroid/os/Handler;", "acceptCall", "", "userInfo", "", "addConnectionListener", "connectionCallback", "Lorg/jivesoftware/smack/ConnectionListener;", "addConversationFragment", "isIncomingCall", "addCurrentCallStateListener", "currentCallStateCallback", "addIncomeCallFragment", "addListeners", "addOnChangeAudioDeviceListener", "onChangeDynamicCallback", "Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$OnChangeAudioDevice;", "addSessionEventsListener", "eventsCallback", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCSessionEventsCallback;", "addSessionStateListener", "clientConnectionCallbacks", "addVideoTrackListener", "callback", "Lcom/quickblox/videochat/webrtc/callbacks/QBRTCClientVideoTracksCallbacks;", "bindCallService", "currentSessionExist", "finish", "getCallerId", "()Ljava/lang/Integer;", "getCurrentSessionState", "Lcom/quickblox/videochat/webrtc/BaseSession$QBRTCSessionState;", "getOpponents", "getPeerChannel", "Lcom/quickblox/videochat/webrtc/QBRTCTypes$QBRTCConnectionState;", "userId", "getVideoTrack", "Lcom/quickblox/videochat/webrtc/view/QBRTCVideoTrack;", "getVideoTrackMap", "", "hangUpCurrentSession", "initIncomingCallTask", "initScreen", "initSettingsStrategy", "isCallState", "isMediaStreamManagerExist", "notifyCallStateListenersCallStarted", "notifyCallStateListenersCallStopped", "notifyCallStateListenersCallTime", "callTime", "notifyCallStateListenersNeedUpdateOpponentsList", "newUsers", "Lcom/quickblox/users/model/QBUser;", "Lkotlin/collections/ArrayList;", "onAcceptCurrentSession", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCallAcceptByUser", "session", "map", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Ljava/lang/Integer;Ljava/util/Map;)V", "onCallRejectByUser", "onConnectedToUser", "(Lcom/quickblox/videochat/webrtc/QBRTCSession;Ljava/lang/Integer;)V", "onConnectionClosedForUser", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDisconnectedFromUser", "onHangUpCurrentSession", "onPause", "onReceiveHangUpFromUser", "onReceiveNewSession", "onRejectCurrentSession", "onResume", "onSessionClosed", "onSessionStartClose", "onSetAudioEnabled", "isAudioEnabled", "onSetVideoEnabled", "isNeedEnableCam", "onStartScreenSharing", "onStateChanged", "sessiontState", "onStopPreview", "onSwitchAudio", "onSwitchCamera", "cameraSwitchHandler", "Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;", "onUserNoActions", "onUserNotAnswer", "removeConnectionListener", "removeCurrentCallStateListener", "removeListeners", "removeOnChangeAudioDeviceListener", "removeSessionEventsListener", "removeSessionStateListener", "removeVideoTrackListener", "showNotificationPopUp", "text", "show", "startCall", "startIncomeCallTimer", "time", "", "startLoadAbsentUsers", "startScreenSharing", "startSuitableFragment", "stopIncomeCallTimer", "CallServiceConnection", "CallTimerCallback", "Companion", "ConnectionListenerImpl", "CurrentCallStateCallback", "OnChangeAudioDevice", "app_stagingRelease"})
public final class CallActivity extends com.mart.onestopkitchen.ui.activity.BaseActivity implements com.mart.onestopkitchen.chat.ui.fragments.IncomeCallFragmentCallbackListener, com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<com.quickblox.videochat.webrtc.QBRTCSession>, com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks, com.mart.onestopkitchen.chat.ui.fragments.ConversationFragmentCallback, com.mart.onestopkitchen.chat.ui.fragments.ScreenShareFragment.OnSharingEvents {
    private java.lang.String TAG;
    private final java.util.ArrayList<com.mart.onestopkitchen.chat.ui.activity.CallActivity.CurrentCallStateCallback> currentCallStateCallbackList = null;
    private android.os.Handler showIncomingCallWindowTaskHandler;
    private com.mart.onestopkitchen.chat.ui.activity.CallActivity.ConnectionListenerImpl connectionListener;
    private android.content.ServiceConnection callServiceConnection;
    private java.lang.Runnable showIncomingCallWindowTask;
    private android.content.SharedPreferences sharedPref;
    private java.util.List<java.lang.Integer> opponentsIdsList;
    private com.mart.onestopkitchen.chat.services.CallService callService;
    private boolean isInComingCall;
    private boolean isVideoCall;
    public static final com.mart.onestopkitchen.chat.ui.activity.CallActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initScreen() {
    }
    
    private final void addListeners() {
    }
    
    private final void removeListeners() {
    }
    
    private final void bindCallService() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void startScreenSharing(android.content.Intent data) {
    }
    
    private final void startSuitableFragment(boolean isInComingCall) {
    }
    
    private final void startLoadAbsentUsers() {
    }
    
    private final void initSettingsStrategy() {
    }
    
    private final void initIncomingCallTask() {
    }
    
    private final void hangUpCurrentSession() {
    }
    
    private final void startIncomeCallTimer(long time) {
    }
    
    private final void stopIncomeCallTimer() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    @java.lang.Override()
    public void finish() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    private final void addIncomeCallFragment() {
    }
    
    private final void addConversationFragment(boolean isIncomingCall) {
    }
    
    private final void showNotificationPopUp(int text, boolean show) {
    }
    
    @java.lang.Override()
    public void onDisconnectedFromUser(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId) {
    }
    
    @java.lang.Override()
    public void onConnectedToUser(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId) {
    }
    
    @java.lang.Override()
    public void onConnectionClosedForUser(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId) {
    }
    
    @java.lang.Override()
    public void onStateChanged(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.BaseSession.QBRTCSessionState sessiontState) {
    }
    
    @java.lang.Override()
    public void onUserNotAnswer(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId) {
    }
    
    @java.lang.Override()
    public void onSessionStartClose(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session) {
    }
    
    @java.lang.Override()
    public void onReceiveHangUpFromUser(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId, @org.jetbrains.annotations.Nullable()
    java.util.Map<java.lang.String, java.lang.String> map) {
    }
    
    @java.lang.Override()
    public void onCallAcceptByUser(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId, @org.jetbrains.annotations.Nullable()
    java.util.Map<java.lang.String, java.lang.String> map) {
    }
    
    @java.lang.Override()
    public void onReceiveNewSession(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session) {
    }
    
    @java.lang.Override()
    public void onUserNoActions(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId) {
    }
    
    @java.lang.Override()
    public void onSessionClosed(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session) {
    }
    
    @java.lang.Override()
    public void onCallRejectByUser(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.QBRTCSession session, @org.jetbrains.annotations.Nullable()
    java.lang.Integer userId, @org.jetbrains.annotations.Nullable()
    java.util.Map<java.lang.String, java.lang.String> map) {
    }
    
    @java.lang.Override()
    public void onAcceptCurrentSession() {
    }
    
    @java.lang.Override()
    public void onRejectCurrentSession() {
    }
    
    @java.lang.Override()
    public void addConnectionListener(@org.jetbrains.annotations.Nullable()
    org.jivesoftware.smack.ConnectionListener connectionCallback) {
    }
    
    @java.lang.Override()
    public void removeConnectionListener(@org.jetbrains.annotations.Nullable()
    org.jivesoftware.smack.ConnectionListener connectionCallback) {
    }
    
    @java.lang.Override()
    public void addSessionStateListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<?> clientConnectionCallbacks) {
    }
    
    @java.lang.Override()
    public void addSessionEventsListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback eventsCallback) {
    }
    
    @java.lang.Override()
    public void onSetAudioEnabled(boolean isAudioEnabled) {
    }
    
    @java.lang.Override()
    public void onHangUpCurrentSession() {
    }
    
    @android.annotation.TargetApi(value = 21)
    @java.lang.Override()
    public void onStartScreenSharing() {
    }
    
    @java.lang.Override()
    public void onSwitchCamera(@org.jetbrains.annotations.NotNull()
    org.webrtc.CameraVideoCapturer.CameraSwitchHandler cameraSwitchHandler) {
    }
    
    @java.lang.Override()
    public void onSetVideoEnabled(boolean isNeedEnableCam) {
    }
    
    @java.lang.Override()
    public void onSwitchAudio() {
    }
    
    @java.lang.Override()
    public void removeSessionStateListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback<?> clientConnectionCallbacks) {
    }
    
    @java.lang.Override()
    public void removeSessionEventsListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback eventsCallback) {
    }
    
    @java.lang.Override()
    public void addCurrentCallStateListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.CurrentCallStateCallback currentCallStateCallback) {
    }
    
    @java.lang.Override()
    public void removeCurrentCallStateListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.CurrentCallStateCallback currentCallStateCallback) {
    }
    
    @java.lang.Override()
    public void addOnChangeAudioDeviceListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.OnChangeAudioDevice onChangeDynamicCallback) {
    }
    
    @java.lang.Override()
    public void removeOnChangeAudioDeviceListener(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.chat.ui.activity.CallActivity.OnChangeAudioDevice onChangeDynamicCallback) {
    }
    
    @java.lang.Override()
    public void acceptCall(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    @java.lang.Override()
    public void startCall(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, java.lang.String> userInfo) {
    }
    
    @java.lang.Override()
    public boolean currentSessionExist() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.util.List<java.lang.Integer> getOpponents() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getCallerId() {
        return null;
    }
    
    @java.lang.Override()
    public void addVideoTrackListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession> callback) {
    }
    
    @java.lang.Override()
    public void removeVideoTrackListener(@org.jetbrains.annotations.Nullable()
    com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks<com.quickblox.videochat.webrtc.QBRTCSession> callback) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public com.quickblox.videochat.webrtc.BaseSession.QBRTCSessionState getCurrentSessionState() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public com.quickblox.videochat.webrtc.QBRTCTypes.QBRTCConnectionState getPeerChannel(int userId) {
        return null;
    }
    
    @java.lang.Override()
    public boolean isMediaStreamManagerExist() {
        return false;
    }
    
    @java.lang.Override()
    public boolean isCallState() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.Map<java.lang.Integer, com.quickblox.videochat.webrtc.view.QBRTCVideoTrack> getVideoTrackMap() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public com.quickblox.videochat.webrtc.view.QBRTCVideoTrack getVideoTrack(int userId) {
        return null;
    }
    
    @java.lang.Override()
    public void onStopPreview() {
    }
    
    private final void notifyCallStateListenersCallStarted() {
    }
    
    private final void notifyCallStateListenersCallStopped() {
    }
    
    private final void notifyCallStateListenersNeedUpdateOpponentsList(java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
    }
    
    private final void notifyCallStateListenersCallTime(java.lang.String callTime) {
    }
    
    public CallActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u000e\u0010\u0005\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007H\u0016J\b\u0010\b\u001a\u00020\u0004H\u0016\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$ConnectionListenerImpl;", "Lorg/jivesoftware/smack/AbstractConnectionListener;", "(Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity;)V", "connectionClosedOnError", "", "e", "Ljava/lang/Exception;", "Lkotlin/Exception;", "reconnectionSuccessful", "app_stagingRelease"})
    final class ConnectionListenerImpl extends org.jivesoftware.smack.AbstractConnectionListener {
        
        @java.lang.Override()
        public void connectionClosedOnError(@org.jetbrains.annotations.Nullable()
        java.lang.Exception e) {
        }
        
        @java.lang.Override()
        public void reconnectionSuccessful() {
        }
        
        public ConnectionListenerImpl() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\u001c\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\u0012\u0010\n\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016\u00a8\u0006\u000b"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$CallServiceConnection;", "Landroid/content/ServiceConnection;", "(Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity;)V", "login", "", "onServiceConnected", "name", "Landroid/content/ComponentName;", "service", "Landroid/os/IBinder;", "onServiceDisconnected", "app_stagingRelease"})
    final class CallServiceConnection implements android.content.ServiceConnection {
        
        @java.lang.Override()
        public void onServiceDisconnected(@org.jetbrains.annotations.Nullable()
        android.content.ComponentName name) {
        }
        
        @java.lang.Override()
        public void onServiceConnected(@org.jetbrains.annotations.Nullable()
        android.content.ComponentName name, @org.jetbrains.annotations.Nullable()
        android.os.IBinder service) {
        }
        
        private final void login() {
        }
        
        public CallServiceConnection() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$CallTimerCallback;", "Lcom/mart/onestopkitchen/chat/services/CallService$CallTimerListener;", "(Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity;)V", "onCallTimeUpdate", "", "time", "", "app_stagingRelease"})
    final class CallTimerCallback implements com.mart.onestopkitchen.chat.services.CallService.CallTimerListener {
        
        @java.lang.Override()
        public void onCallTimeUpdate(@org.jetbrains.annotations.NotNull()
        java.lang.String time) {
        }
        
        public CallTimerCallback() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$OnChangeAudioDevice;", "", "audioDeviceChanged", "", "newAudioDevice", "Lcom/quickblox/videochat/webrtc/AppRTCAudioManager$AudioDevice;", "app_stagingRelease"})
    public static abstract interface OnChangeAudioDevice {
        
        public abstract void audioDeviceChanged(@org.jetbrains.annotations.NotNull()
        com.quickblox.videochat.webrtc.AppRTCAudioManager.AudioDevice newAudioDevice);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J \u0010\b\u001a\u00020\u00032\u0016\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u000b0\nj\b\u0012\u0004\u0012\u00020\u000b`\fH&\u00a8\u0006\r"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$CurrentCallStateCallback;", "", "onCallStarted", "", "onCallStopped", "onCallTimeUpdate", "time", "", "onOpponentsListUpdated", "newUsers", "Ljava/util/ArrayList;", "Lcom/quickblox/users/model/QBUser;", "Lkotlin/collections/ArrayList;", "app_stagingRelease"})
    public static abstract interface CurrentCallStateCallback {
        
        public abstract void onCallStarted();
        
        public abstract void onCallStopped();
        
        public abstract void onOpponentsListUpdated(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers);
        
        public abstract void onCallTimeUpdate(@org.jetbrains.annotations.NotNull()
        java.lang.String time);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$Companion;", "", "()V", "start", "", "context", "Landroid/content/Context;", "isIncomingCall", "", "app_stagingRelease"})
    public static final class Companion {
        
        public final void start(@org.jetbrains.annotations.NotNull()
        android.content.Context context, boolean isIncomingCall) {
        }
        
        private Companion() {
            super();
        }
    }
}