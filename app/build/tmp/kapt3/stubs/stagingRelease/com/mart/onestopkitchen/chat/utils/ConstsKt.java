package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000 \n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0011\n\u0002\b\u000b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0003X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0003X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\"\u0019\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00010\u0010\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u0011\u0010\u0012\"\u0011\u0010\u0014\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u000e\u0010\u0017\u001a\u00020\u0003X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0018\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001a\u001a\u00020\u0003X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"ACTION_NEW_FCM_EVENT", "", "ALLOW_LISTEN_NETWORK", "", "AUTO_JOIN", "AUTO_MARK_DELIVERED", "CHAT_PORT", "", "EXTRA_FCM_MESSAGE", "EXTRA_IS_INCOMING_CALL", "EXTRA_LOGIN_ERROR_MESSAGE", "EXTRA_LOGIN_RESULT", "EXTRA_LOGIN_RESULT_CODE", "KEEP_ALIVE", "MAX_OPPONENTS_COUNT", "PERMISSIONS", "", "getPERMISSIONS", "()[Ljava/lang/String;", "[Ljava/lang/String;", "PREFERRED_IMAGE_SIZE_FULL", "getPREFERRED_IMAGE_SIZE_FULL", "()I", "RECONNECTION_ALLOWED", "SOCKET_TIMEOUT", "USER_DEFAULT_PASSWORD", "USE_TLS", "app_stagingRelease"})
public final class ConstsKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_FCM_MESSAGE = "message";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ACTION_NEW_FCM_EVENT = "new-push-event";
    private static final int PREFERRED_IMAGE_SIZE_FULL = 0;
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String[] PERMISSIONS = null;
    public static final int MAX_OPPONENTS_COUNT = 6;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_LOGIN_RESULT = "login_result";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_LOGIN_ERROR_MESSAGE = "login_error_message";
    public static final int EXTRA_LOGIN_RESULT_CODE = 1002;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_IS_INCOMING_CALL = "conversation_reason";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_DEFAULT_PASSWORD = "quickblox";
    public static final int CHAT_PORT = 5223;
    public static final int SOCKET_TIMEOUT = 300;
    public static final boolean KEEP_ALIVE = true;
    public static final boolean USE_TLS = true;
    public static final boolean AUTO_JOIN = false;
    public static final boolean AUTO_MARK_DELIVERED = true;
    public static final boolean RECONNECTION_ALLOWED = true;
    public static final boolean ALLOW_LISTEN_NETWORK = true;
    
    public static final int getPREFERRED_IMAGE_SIZE_FULL() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String[] getPERMISSIONS() {
        return null;
    }
}