package com.mart.onestopkitchen.chat.qb.callback;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001f\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00028\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016\u00a2\u0006\u0002\u0010\f\u00a8\u0006\r"}, d2 = {"Lcom/mart/onestopkitchen/chat/qb/callback/QbEntityCallbackImpl;", "T", "Lcom/quickblox/core/QBEntityCallback;", "()V", "onError", "", "e", "Lcom/quickblox/core/exception/QBResponseException;", "onSuccess", "result", "bundle", "Landroid/os/Bundle;", "(Ljava/lang/Object;Landroid/os/Bundle;)V", "app_stagingRelease"})
public class QbEntityCallbackImpl<T extends java.lang.Object> implements com.quickblox.core.QBEntityCallback<T> {
    
    @java.lang.Override()
    public void onSuccess(T result, @org.jetbrains.annotations.Nullable()
    android.os.Bundle bundle) {
    }
    
    @java.lang.Override()
    public void onError(@org.jetbrains.annotations.NotNull()
    com.quickblox.core.exception.QBResponseException e) {
    }
    
    public QbEntityCallbackImpl() {
        super();
    }
}