package com.mart.onestopkitchen.mapper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002BCB\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u0019J\u0006\u0010)\u001a\u00020\'J\u0012\u0010*\u001a\u00020\'2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010+J\u0006\u0010,\u001a\u00020\'J\u0006\u0010-\u001a\u00020.J \u0010/\u001a\u00020\'2\u0006\u00100\u001a\u00020.2\u0006\u00101\u001a\u00020.2\b\u00102\u001a\u0004\u0018\u000103J+\u00104\u001a\u00020\'2\u0006\u00100\u001a\u00020.2\u000e\u00105\u001a\n\u0012\u0006\b\u0001\u0012\u000207062\u0006\u00108\u001a\u000209\u00a2\u0006\u0002\u0010:J\u0010\u0010;\u001a\u00020\'2\b\u0010(\u001a\u0004\u0018\u00010<J\u0010\u0010=\u001a\u00020\'2\b\u0010(\u001a\u0004\u0018\u00010>J\b\u0010?\u001a\u00020\'H\u0002J\b\u0010@\u001a\u00020\'H\u0002J\u000e\u0010A\u001a\u00020\'2\u0006\u0010\u0018\u001a\u00020\u0019R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001b\u0010\u001e\u001a\u00020\u001f8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\"\u0010\u0017\u001a\u0004\b \u0010!R\u0010\u0010#\u001a\u0004\u0018\u00010$X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010$X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006D"}, d2 = {"Lcom/mart/onestopkitchen/mapper/BaseActivityMapper;", "", "()V", "isBluetoothNeedEnable", "", "isEntered", "isLanguageChanged", "()Z", "setLanguageChanged", "(Z)V", "isLocaleEnglish", "isWifiNeedEnable", "job", "Lkotlinx/coroutines/Job;", "getJob", "()Lkotlinx/coroutines/Job;", "setJob", "(Lkotlinx/coroutines/Job;)V", "localeUtils", "Lcom/mart/onestopkitchen/utils/LocaleUtils;", "getLocaleUtils", "()Lcom/mart/onestopkitchen/utils/LocaleUtils;", "localeUtils$delegate", "Lkotlin/Lazy;", "mActivity", "Landroidx/appcompat/app/AppCompatActivity;", "getMActivity", "()Landroidx/appcompat/app/AppCompatActivity;", "setMActivity", "(Landroidx/appcompat/app/AppCompatActivity;)V", "mTracker", "Lcom/mart/onestopkitchen/utils/Tracker;", "getMTracker", "()Lcom/mart/onestopkitchen/utils/Tracker;", "mTracker$delegate", "networkBroadcast", "Landroid/content/BroadcastReceiver;", "syncReceiver", "disbleTracking", "", "mListner", "enableBluetoothTracking", "enableLocationTracking", "Lcom/mart/onestopkitchen/utils/callback/CurrentLocationCallback;", "enableWifiTracking", "getFlagIcon", "", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "registerDataSyncReceiver", "Lcom/mart/onestopkitchen/mapper/BaseActivityMapper$OnDataSynced;", "registerNetworkBroadcast", "Lcom/mart/onestopkitchen/mapper/BaseActivityMapper$OnNetworkChange;", "startBluetoothScanning", "startWifiScanning", "withActivity", "OnDataSynced", "OnNetworkChange", "app_stagingRelease"})
public final class BaseActivityMapper {
    private android.content.BroadcastReceiver networkBroadcast;
    private android.content.BroadcastReceiver syncReceiver;
    private boolean isLocaleEnglish;
    private boolean isLanguageChanged;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy mTracker$delegate = null;
    private final kotlin.Lazy localeUtils$delegate = null;
    @org.jetbrains.annotations.Nullable()
    private androidx.appcompat.app.AppCompatActivity mActivity;
    private boolean isEntered;
    @org.jetbrains.annotations.Nullable()
    private kotlinx.coroutines.Job job;
    private boolean isBluetoothNeedEnable;
    private boolean isWifiNeedEnable;
    
    public final boolean isLanguageChanged() {
        return false;
    }
    
    public final void setLanguageChanged(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.utils.Tracker getMTracker() {
        return null;
    }
    
    private final com.mart.onestopkitchen.utils.LocaleUtils getLocaleUtils() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.appcompat.app.AppCompatActivity getMActivity() {
        return null;
    }
    
    public final void setMActivity(@org.jetbrains.annotations.Nullable()
    androidx.appcompat.app.AppCompatActivity p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlinx.coroutines.Job getJob() {
        return null;
    }
    
    public final void setJob(@org.jetbrains.annotations.Nullable()
    kotlinx.coroutines.Job p0) {
    }
    
    public final void withActivity(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mActivity) {
    }
    
    public final void registerNetworkBroadcast(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.mapper.BaseActivityMapper.OnNetworkChange mListner) {
    }
    
    public final void registerDataSyncReceiver(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.mapper.BaseActivityMapper.OnDataSynced mListner) {
    }
    
    public final void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public final void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public final void enableLocationTracking(@org.jetbrains.annotations.Nullable()
    com.mart.onestopkitchen.utils.callback.CurrentLocationCallback mListner) {
    }
    
    public final void enableBluetoothTracking() {
    }
    
    private final void startBluetoothScanning() {
    }
    
    public final void enableWifiTracking() {
    }
    
    private final void startWifiScanning() {
    }
    
    public final void disbleTracking(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity mListner) {
    }
    
    public final int getFlagIcon() {
        return 0;
    }
    
    public BaseActivityMapper() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/mapper/BaseActivityMapper$OnNetworkChange;", "", "networkUpdated", "", "isConnected", "", "app_stagingRelease"})
    public static abstract interface OnNetworkChange {
        
        public abstract void networkUpdated(boolean isConnected);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/mart/onestopkitchen/mapper/BaseActivityMapper$OnDataSynced;", "", "onDataSynced", "", "app_stagingRelease"})
    public static abstract interface OnDataSynced {
        
        public abstract void onDataSynced();
    }
}