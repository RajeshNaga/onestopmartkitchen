package com.mart.onestopkitchen.utils.fingerprint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&J\b\u0010\t\u001a\u00020\u0003H&\u00a8\u0006\n"}, d2 = {"Lcom/mart/onestopkitchen/utils/fingerprint/FingerprintBaseView;", "", "error", "", "failure", "showFingerPrintPopup", "showMsg", "msg", "", "success", "app_stagingRelease"})
public abstract interface FingerprintBaseView {
    
    public abstract void showFingerPrintPopup();
    
    public abstract void showMsg(@org.jetbrains.annotations.NotNull()
    java.lang.String msg);
    
    public abstract void success();
    
    public abstract void failure();
    
    public abstract void error();
}