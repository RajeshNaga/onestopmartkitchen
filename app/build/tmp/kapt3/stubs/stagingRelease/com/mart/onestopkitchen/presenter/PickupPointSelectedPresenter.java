package com.mart.onestopkitchen.presenter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002J\u000e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0011H\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0003R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/mart/onestopkitchen/presenter/PickupPointSelectedPresenter;", "", "mContext", "Landroid/content/Context;", "pId", "", "listener", "Lcom/mart/onestopkitchen/presenter/PickupPointSelectedPresenter$SavePickUpPoint;", "(Landroid/content/Context;Ljava/lang/String;Lcom/mart/onestopkitchen/presenter/PickupPointSelectedPresenter$SavePickUpPoint;)V", "getListener", "()Lcom/mart/onestopkitchen/presenter/PickupPointSelectedPresenter$SavePickUpPoint;", "getMContext", "()Landroid/content/Context;", "getObservable", "Lio/reactivex/Observable;", "Lcom/mart/onestopkitchen/networking/response/StoreSaveResponse;", "getObserver", "Lio/reactivex/observers/DisposableObserver;", "setPickupPoint", "", "SavePickUpPoint", "app_stagingRelease"})
public final class PickupPointSelectedPresenter {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context mContext = null;
    private final java.lang.String pId = null;
    @org.jetbrains.annotations.NotNull()
    private final com.mart.onestopkitchen.presenter.PickupPointSelectedPresenter.SavePickUpPoint listener = null;
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    private final void setPickupPoint() {
    }
    
    private final io.reactivex.observers.DisposableObserver<com.mart.onestopkitchen.networking.response.StoreSaveResponse> getObserver() {
        return null;
    }
    
    private final io.reactivex.Observable<com.mart.onestopkitchen.networking.response.StoreSaveResponse> getObservable() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.presenter.PickupPointSelectedPresenter.SavePickUpPoint getListener() {
        return null;
    }
    
    public PickupPointSelectedPresenter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    java.lang.String pId, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.presenter.PickupPointSelectedPresenter.SavePickUpPoint listener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0007"}, d2 = {"Lcom/mart/onestopkitchen/presenter/PickupPointSelectedPresenter$SavePickUpPoint;", "", "errorPickupPoint", "", "savedPickupPoint", "isSave", "", "app_stagingRelease"})
    public static abstract interface SavePickUpPoint {
        
        public abstract void savedPickupPoint(boolean isSave);
        
        public abstract void errorPickupPoint();
    }
}