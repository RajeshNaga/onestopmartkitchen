package com.mart.onestopkitchen.chat.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\u001a\u000e\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005\u001a\u000e\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0005\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"MMMM_dd_PATTERN", "", "ddMMyyyy_PATTERN", "getDate", "milliseconds", "", "getDateAsHeaderId", "app_stagingRelease"})
public final class TimeUtilsKt {
    private static final java.lang.String MMMM_dd_PATTERN = "MMMM dd";
    private static final java.lang.String ddMMyyyy_PATTERN = "ddMMyyyy";
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDate(long milliseconds) {
        return null;
    }
    
    public static final long getDateAsHeaderId(long milliseconds) {
        return 0L;
    }
}