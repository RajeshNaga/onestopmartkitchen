package com.mart.onestopkitchen.baseview;

import java.lang.System;

/**
 * Created by Thavam on 10-Nov-18.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0016\u0018\u0000*\n\b\u0000\u0010\u0001 \u0000*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\r\u001a\u00020\u000eH\u0016J\u000e\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011J\u0015\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0012\u0010\u000b\u001a\u0004\u0018\u00018\u0000X\u0088\u000e\u00a2\u0006\u0004\n\u0002\u0010\f\u00a8\u0006\u0015"}, d2 = {"Lcom/mart/onestopkitchen/baseview/AbstractBasePresenter;", "V", "Lbasicsetup/me/com/base/LoadBaseView;", "Lbasicsetup/me/com/base/BasePresenter;", "()V", "apiHelper", "Lcom/mart/onestopkitchen/rxjava/ApiHelper;", "getApiHelper", "()Lcom/mart/onestopkitchen/rxjava/ApiHelper;", "setApiHelper", "(Lcom/mart/onestopkitchen/rxjava/ApiHelper;)V", "view", "Lbasicsetup/me/com/base/LoadBaseView;", "destroyView", "", "exceptionHandler", "throwable", "", "setView", "getView", "(Lbasicsetup/me/com/base/LoadBaseView;)V", "app_stagingRelease"})
public class AbstractBasePresenter<V extends basicsetup.me.com.base.LoadBaseView> implements basicsetup.me.com.base.BasePresenter<V> {
    @org.jetbrains.annotations.NotNull()
    private com.mart.onestopkitchen.rxjava.ApiHelper apiHelper;
    private V view;
    
    @org.jetbrains.annotations.NotNull()
    protected final com.mart.onestopkitchen.rxjava.ApiHelper getApiHelper() {
        return null;
    }
    
    protected final void setApiHelper(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.rxjava.ApiHelper p0) {
    }
    
    @java.lang.Override()
    public void destroyView() {
    }
    
    @java.lang.Override()
    public void setView(@org.jetbrains.annotations.NotNull()
    V getView) {
    }
    
    public final void exceptionHandler(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable throwable) {
    }
    
    public AbstractBasePresenter() {
        super();
    }
}