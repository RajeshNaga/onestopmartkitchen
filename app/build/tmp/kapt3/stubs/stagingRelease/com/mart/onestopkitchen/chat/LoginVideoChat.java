package com.mart.onestopkitchen.chat;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000fH\u0002J\u000e\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u000e\u001a\u00020\u000fJ\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u000fH\u0002J\u0010\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000fH\u0002J\u0010\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0016\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000fR\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\u00020\u000fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013\u00a8\u0006\u001f"}, d2 = {"Lcom/mart/onestopkitchen/chat/LoginVideoChat;", "", "()V", "ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS", "", "getERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS", "()I", "UNAUTHORIZED", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "setContext", "(Landroid/content/Context;)V", "user", "Lcom/quickblox/users/model/QBUser;", "getUser", "()Lcom/quickblox/users/model/QBUser;", "setUser", "(Lcom/quickblox/users/model/QBUser;)V", "loginToChat", "", "qbUser", "signInCreatedUser", "signUpNewUser", "newUser", "startLoginService", "updateUserOnServer", "videoChat", "mContext", "Companion", "app_stagingRelease"})
public final class LoginVideoChat {
    private final int UNAUTHORIZED = 401;
    private final int ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS = 422;
    @org.jetbrains.annotations.NotNull()
    public com.quickblox.users.model.QBUser user;
    @org.jetbrains.annotations.Nullable()
    private android.content.Context context;
    @android.annotation.SuppressLint(value = {"StaticFieldLeak"})
    private static volatile com.mart.onestopkitchen.chat.LoginVideoChat INSTANCE;
    public static final com.mart.onestopkitchen.chat.LoginVideoChat.Companion Companion = null;
    
    public final int getERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.quickblox.users.model.QBUser getUser() {
        return null;
    }
    
    public final void setUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    public final void videoChat(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser user) {
    }
    
    private final void signUpNewUser(com.quickblox.users.model.QBUser newUser) {
    }
    
    private final void loginToChat(com.quickblox.users.model.QBUser qbUser) {
    }
    
    private final void startLoginService(com.quickblox.users.model.QBUser qbUser) {
    }
    
    public final void signInCreatedUser(@org.jetbrains.annotations.NotNull()
    com.quickblox.users.model.QBUser user) {
    }
    
    private final void updateUserOnServer(com.quickblox.users.model.QBUser user) {
    }
    
    public LoginVideoChat() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/chat/LoginVideoChat$Companion;", "", "()V", "INSTANCE", "Lcom/mart/onestopkitchen/chat/LoginVideoChat;", "getInstance", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.chat.LoginVideoChat getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}