package com.mart.onestopkitchen.baseview;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0002\u001a\u0004\u0018\u00010\u0004H&J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\b\u0010\b\u001a\u00020\u0003H&\u00a8\u0006\t"}, d2 = {"Lcom/mart/onestopkitchen/baseview/PickupPointView;", "", "error", "", "", "loadData", "pickupPoints", "Lcom/mart/onestopkitchen/networking/response/PickupPointAddressResponse;", "noDataFound", "app_stagingRelease"})
public abstract interface PickupPointView {
    
    public abstract void loadData(@org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.networking.response.PickupPointAddressResponse pickupPoints);
    
    public abstract void noDataFound();
    
    public abstract void error(@org.jetbrains.annotations.Nullable()
    java.lang.String error);
}