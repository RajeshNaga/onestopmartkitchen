package com.mart.onestopkitchen.utils.wifi;

import java.lang.System;

@android.annotation.SuppressLint(value = {"ALL"})
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0007\u0018\u0000 \u001a2\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001:\u0001\u001aB\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\bH\u0002J\b\u0010\u0016\u001a\u00020\u0014H\u0014J\b\u0010\u0017\u001a\u00020\u0014H\u0014J\u0006\u0010\u0018\u001a\u00020\u0000J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012\u00a8\u0006\u001b"}, d2 = {"Lcom/mart/onestopkitchen/utils/wifi/WifiScan;", "Landroidx/lifecycle/LiveData;", "", "Landroid/net/wifi/ScanResult;", "appContext", "Landroid/content/Context;", "(Landroid/content/Context;)V", "isWifiEnabledForcefully", "", "scanResultreceiver", "Landroid/content/BroadcastReceiver;", "getScanResultreceiver", "()Landroid/content/BroadcastReceiver;", "wifi", "Landroid/net/wifi/WifiManager;", "getWifi", "()Landroid/net/wifi/WifiManager;", "setWifi", "(Landroid/net/wifi/WifiManager;)V", "enableWifi", "", "isWifiEnabled", "onActive", "onInactive", "startScanning", "with", "Companion", "app_stagingRelease"})
public final class WifiScan extends androidx.lifecycle.LiveData<java.util.List<? extends android.net.wifi.ScanResult>> {
    @org.jetbrains.annotations.Nullable()
    private android.net.wifi.WifiManager wifi;
    private boolean isWifiEnabledForcefully;
    @org.jetbrains.annotations.NotNull()
    private final android.content.BroadcastReceiver scanResultreceiver = null;
    private android.content.Context appContext;
    private static com.mart.onestopkitchen.utils.wifi.WifiScan instance;
    public static final com.mart.onestopkitchen.utils.wifi.WifiScan.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.net.wifi.WifiManager getWifi() {
        return null;
    }
    
    public final void setWifi(@org.jetbrains.annotations.Nullable()
    android.net.wifi.WifiManager p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.utils.wifi.WifiScan with(@org.jetbrains.annotations.NotNull()
    android.content.Context appContext) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.BroadcastReceiver getScanResultreceiver() {
        return null;
    }
    
    @java.lang.Override()
    protected void onActive() {
    }
    
    @java.lang.Override()
    protected void onInactive() {
    }
    
    private final boolean isWifiEnabled() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.utils.wifi.WifiScan startScanning() {
        return null;
    }
    
    private final void enableWifi() {
    }
    
    public WifiScan(@org.jetbrains.annotations.NotNull()
    android.content.Context appContext) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/mart/onestopkitchen/utils/wifi/WifiScan$Companion;", "", "()V", "instance", "Lcom/mart/onestopkitchen/utils/wifi/WifiScan;", "getInstance", "appContext", "Landroid/content/Context;", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.utils.wifi.WifiScan getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context appContext) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}