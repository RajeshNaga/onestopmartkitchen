package com.mart.onestopkitchen.chat.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0003123B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u000bJ\b\u0010\"\u001a\u00020\u0018H\u0016J\u0010\u0010#\u001a\u00020\u000b2\u0006\u0010$\u001a\u00020\u0018H\u0016J\u0010\u0010%\u001a\u00020&2\u0006\u0010$\u001a\u00020\u0018H\u0016J$\u0010\'\u001a\u00020(2\u0006\u0010$\u001a\u00020\u00182\b\u0010)\u001a\u0004\u0018\u00010(2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J\u0010\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020\u000bH\u0002J\u000e\u0010/\u001a\u00020 2\u0006\u00100\u001a\u00020\fJ\u000e\u0010/\u001a\u00020 2\u0006\u0010!\u001a\u00020\u000bR\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R \u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00180\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\f0\u001c8F\u00a2\u0006\u0006\u001a\u0004\b\u001d\u0010\u001e\u00a8\u00064"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter;", "Landroid/widget/BaseAdapter;", "context", "Landroid/content/Context;", "attachmentCountChangedListener", "Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$AttachmentCountChangedListener;", "errorListener", "Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$AttachmentUploadErrorListener;", "(Landroid/content/Context;Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$AttachmentCountChangedListener;Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$AttachmentUploadErrorListener;)V", "_fileQBAttachmentMap", "", "Ljava/io/File;", "Lcom/quickblox/chat/model/QBAttachment;", "getContext", "()Landroid/content/Context;", "getErrorListener", "()Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$AttachmentUploadErrorListener;", "fileList", "", "getFileList", "()Ljava/util/List;", "setFileList", "(Ljava/util/List;)V", "fileUploadProgressMap", "", "mainThreadHandler", "Landroid/os/Handler;", "uploadedAttachments", "", "getUploadedAttachments", "()Ljava/util/Collection;", "add", "", "item", "getCount", "getItem", "position", "getItemId", "", "getView", "Landroid/view/View;", "convertView", "parent", "Landroid/view/ViewGroup;", "isFileUploading", "", "attachmentFile", "remove", "qbAttachment", "AttachmentCountChangedListener", "AttachmentUploadErrorListener", "ViewHolder", "app_stagingRelease"})
public final class AttachmentPreviewAdapter extends android.widget.BaseAdapter {
    private final android.os.Handler mainThreadHandler = null;
    private java.util.Map<java.io.File, com.quickblox.chat.model.QBAttachment> _fileQBAttachmentMap;
    private java.util.Map<java.io.File, java.lang.Integer> fileUploadProgressMap;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<java.io.File> fileList;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    private final com.mart.onestopkitchen.chat.ui.adapters.AttachmentPreviewAdapter.AttachmentCountChangedListener attachmentCountChangedListener = null;
    @org.jetbrains.annotations.NotNull()
    private final com.mart.onestopkitchen.chat.ui.adapters.AttachmentPreviewAdapter.AttachmentUploadErrorListener errorListener = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Collection<com.quickblox.chat.model.QBAttachment> getUploadedAttachments() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.io.File> getFileList() {
        return null;
    }
    
    public final void setFileList(@org.jetbrains.annotations.NotNull()
    java.util.List<java.io.File> p0) {
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    java.io.File item) {
    }
    
    public final void remove(@org.jetbrains.annotations.NotNull()
    java.io.File item) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View getView(int position, @org.jetbrains.annotations.Nullable()
    android.view.View convertView, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup parent) {
        return null;
    }
    
    public final void remove(@org.jetbrains.annotations.NotNull()
    com.quickblox.chat.model.QBAttachment qbAttachment) {
    }
    
    private final boolean isFileUploading(java.io.File attachmentFile) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.io.File getItem(int position) {
        return null;
    }
    
    @java.lang.Override()
    public long getItemId(int position) {
        return 0L;
    }
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.mart.onestopkitchen.chat.ui.adapters.AttachmentPreviewAdapter.AttachmentUploadErrorListener getErrorListener() {
        return null;
    }
    
    public AttachmentPreviewAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.ui.adapters.AttachmentPreviewAdapter.AttachmentCountChangedListener attachmentCountChangedListener, @org.jetbrains.annotations.NotNull()
    com.mart.onestopkitchen.chat.ui.adapters.AttachmentPreviewAdapter.AttachmentUploadErrorListener errorListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$ViewHolder;", "", "()V", "attachmentImageView", "Landroid/widget/ImageView;", "getAttachmentImageView", "()Landroid/widget/ImageView;", "setAttachmentImageView", "(Landroid/widget/ImageView;)V", "deleteButton", "Landroid/widget/ImageButton;", "getDeleteButton", "()Landroid/widget/ImageButton;", "setDeleteButton", "(Landroid/widget/ImageButton;)V", "progressBar", "Landroid/widget/ProgressBar;", "getProgressBar", "()Landroid/widget/ProgressBar;", "setProgressBar", "(Landroid/widget/ProgressBar;)V", "app_stagingRelease"})
    static final class ViewHolder {
        @org.jetbrains.annotations.NotNull()
        public android.widget.ImageView attachmentImageView;
        @org.jetbrains.annotations.NotNull()
        public android.widget.ProgressBar progressBar;
        @org.jetbrains.annotations.NotNull()
        public android.widget.ImageButton deleteButton;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getAttachmentImageView() {
            return null;
        }
        
        public final void setAttachmentImageView(@org.jetbrains.annotations.NotNull()
        android.widget.ImageView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ProgressBar getProgressBar() {
            return null;
        }
        
        public final void setProgressBar(@org.jetbrains.annotations.NotNull()
        android.widget.ProgressBar p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageButton getDeleteButton() {
            return null;
        }
        
        public final void setDeleteButton(@org.jetbrains.annotations.NotNull()
        android.widget.ImageButton p0) {
        }
        
        public ViewHolder() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$AttachmentCountChangedListener;", "", "onAttachmentCountChanged", "", "count", "", "app_stagingRelease"})
    public static abstract interface AttachmentCountChangedListener {
        
        public abstract void onAttachmentCountChanged(int count);
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/adapters/AttachmentPreviewAdapter$AttachmentUploadErrorListener;", "", "onAttachmentUploadError", "", "e", "Lcom/quickblox/core/exception/QBResponseException;", "app_stagingRelease"})
    public static abstract interface AttachmentUploadErrorListener {
        
        public abstract void onAttachmentUploadError(@org.jetbrains.annotations.NotNull()
        com.quickblox.core.exception.QBResponseException e);
    }
}