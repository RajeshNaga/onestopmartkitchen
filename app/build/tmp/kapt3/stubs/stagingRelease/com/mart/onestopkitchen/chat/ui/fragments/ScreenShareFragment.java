package com.mart.onestopkitchen.chat.ui.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \"2\u00020\u0001:\u0004\"#$%B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0014J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J&\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0012\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010\u001b\u001a\u00020\fH\u0016J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\b\u0010 \u001a\u00020\fH\u0016J\b\u0010!\u001a\u00020\fH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment;", "Lcom/mart/onestopkitchen/chat/ui/fragments/BaseToolBarFragment;", "()V", "TAG", "", "currentCallStateCallback", "Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$CurrentCallStateCallback;", "onSharingEvents", "Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment$OnSharingEvents;", "getFragmentLayout", "", "onAttach", "", "context", "Landroid/content/Context;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDetach", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onPause", "onResume", "Companion", "CurrentCallStateCallbackImpl", "ImagesAdapter", "OnSharingEvents", "app_stagingRelease"})
public final class ScreenShareFragment extends com.mart.onestopkitchen.chat.ui.fragments.BaseToolBarFragment {
    private final java.lang.String TAG = null;
    private com.mart.onestopkitchen.chat.ui.fragments.ScreenShareFragment.OnSharingEvents onSharingEvents;
    private com.mart.onestopkitchen.chat.ui.activity.CallActivity.CurrentCallStateCallback currentCallStateCallback;
    public static final com.mart.onestopkitchen.chat.ui.fragments.ScreenShareFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected int getFragmentLayout() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu, @org.jetbrains.annotations.NotNull()
    android.view.MenuInflater inflater) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onDetach() {
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    public ScreenShareFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\bH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment$ImagesAdapter;", "Landroidx/fragment/app/FragmentPagerAdapter;", "fm", "Landroidx/fragment/app/FragmentManager;", "(Landroidx/fragment/app/FragmentManager;)V", "images", "", "getCount", "", "getItem", "Landroidx/fragment/app/Fragment;", "position", "app_stagingRelease"})
    public static final class ImagesAdapter extends androidx.fragment.app.FragmentPagerAdapter {
        private final int[] images = null;
        
        @java.lang.Override()
        public int getCount() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public androidx.fragment.app.Fragment getItem(int position) {
            return null;
        }
        
        public ImagesAdapter(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.FragmentManager fm) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0004H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0016J \u0010\t\u001a\u00020\u00042\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\rH\u0016\u00a8\u0006\u000e"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment$CurrentCallStateCallbackImpl;", "Lcom/mart/onestopkitchen/chat/ui/activity/CallActivity$CurrentCallStateCallback;", "(Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment;)V", "onCallStarted", "", "onCallStopped", "onCallTimeUpdate", "time", "", "onOpponentsListUpdated", "newUsers", "Ljava/util/ArrayList;", "Lcom/quickblox/users/model/QBUser;", "Lkotlin/collections/ArrayList;", "app_stagingRelease"})
    final class CurrentCallStateCallbackImpl implements com.mart.onestopkitchen.chat.ui.activity.CallActivity.CurrentCallStateCallback {
        
        @java.lang.Override()
        public void onCallStarted() {
        }
        
        @java.lang.Override()
        public void onCallStopped() {
        }
        
        @java.lang.Override()
        public void onOpponentsListUpdated(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.quickblox.users.model.QBUser> newUsers) {
        }
        
        @java.lang.Override()
        public void onCallTimeUpdate(@org.jetbrains.annotations.NotNull()
        java.lang.String time) {
        }
        
        public CurrentCallStateCallbackImpl() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment$OnSharingEvents;", "", "onStopPreview", "", "app_stagingRelease"})
    public static abstract interface OnSharingEvents {
        
        public abstract void onStopPreview();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment$Companion;", "", "()V", "newInstance", "Lcom/mart/onestopkitchen/chat/ui/fragments/ScreenShareFragment;", "app_stagingRelease"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.mart.onestopkitchen.chat.ui.fragments.ScreenShareFragment newInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}