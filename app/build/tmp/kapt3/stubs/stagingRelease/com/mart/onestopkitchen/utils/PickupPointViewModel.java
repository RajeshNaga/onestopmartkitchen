package com.mart.onestopkitchen.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004J\u0006\u0010\t\u001a\u00020\nR\u0016\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/mart/onestopkitchen/utils/PickupPointViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "pickPointResponse", "Landroidx/lifecycle/LiveData;", "Lcom/mart/onestopkitchen/networking/response/PickupPointAddressResponse;", "pickupPointPresenter", "Lcom/mart/onestopkitchen/presenter/PickupPointPresenter;", "getPickupPointList", "releaseObj", "", "app_stagingRelease"})
public class PickupPointViewModel extends androidx.lifecycle.ViewModel {
    private androidx.lifecycle.LiveData<com.mart.onestopkitchen.networking.response.PickupPointAddressResponse> pickPointResponse;
    private com.mart.onestopkitchen.presenter.PickupPointPresenter pickupPointPresenter;
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.LiveData<com.mart.onestopkitchen.networking.response.PickupPointAddressResponse> getPickupPointList() {
        return null;
    }
    
    public final void releaseObj() {
    }
    
    public PickupPointViewModel() {
        super();
    }
}