package com.mart.onestopkitchen.application;

import android.content.Context;
import android.os.StrictMode;

import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.binjar.prefsdroid.Preference;
import com.crashlytics.android.Crashlytics;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.ServiceZone;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.chat.db.DbHelper;
import com.mart.onestopkitchen.chat.managers.BackgroundListener;
import com.mart.onestopkitchen.chat.utils.ActivityLifecycle;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.FilterSubDataModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.UserSelectedList;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.AppConstants;
import com.mart.onestopkitchen.utils.AppInitialize;
import com.mart.onestopkitchen.utils.mobilevalidation.CACHEDKEY;
import com.mart.onestopkitchen.utils.mobilevalidation.GetCacheData;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import kotlin.jvm.Synchronized;

/**
 * Created by Ashraful on 11/4/2015.
 */

public class MyApplication extends MultiDexApplication {
    public static boolean PROCEED_AS_GUEST = false;
    private static Api api;
    private static MyApplication context;
    public List<CartProduct> orderItems;
    public HashMap<CACHEDKEY, WeakReference<Object>> cacheObject = null;
    public List<FilterSubDataModel> alreadySelected;
    public String alreadySelectedPrice = "0-0";
    public int rightValInt = 0;
    public int leftValInt = 0;
    public UserSelectedList selectedModelLis;
    public String fromMenuSelection = "";
    public ArrayList<String> toolBarTitle = new ArrayList<>(0);
    public String fromWhere = "";
    public boolean clearTextCalled = false;
    public boolean fromSearchView = false;
    public boolean isSearchFragment;
    public String PRODUCT_CATEGORY_NAME = "Others";
    public boolean isLogoutOptionShow = false;

    /*make a separate class to do this*/
    private String productAmount = "";
    private String totalAmount = "";
    private String shippingCharges = "";

    private DbHelper dbHelper = null;

    public static MyApplication getAppContext() {
        if (context == null)
            context = new MyApplication();
        return context;
    }

    public static Api getApi() {
        if (api == null)
            api = RetroClient.getApi();
        return api;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        Preference.load().using(this).with(PreferenceService.SHARED_PREF_KEY).prepare();
        new AppInitialize();
        api = RetroClient.getApi();
        context = this;
        super.onCreate();
        ActivityLifecycle.init(this);
        dbHelper = new DbHelper(this);
        checkCredentials();
        initCredentials();
        initFabric();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new BackgroundListener());
        // strictMode();
    }

    private void initFabric() {
        if (BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
//            Stetho.initializeWithDefaults(this);
        }
    }

    private void strictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .penaltyDeath()
                .build());

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());

    }

    public Object getCachedObject(CACHEDKEY cachedkey) {
        if (null == cacheObject)
            cacheObject = new HashMap<>();

        if (cacheObject.containsKey(cachedkey)) {
            if (cacheObject.get(cachedkey).get() == null)
                cacheObject.put(cachedkey, new WeakReference<>(GetCacheData.cacheData(cachedkey)));
            checkBlankArray(cachedkey);
        } else
            cacheObject.put(cachedkey, new WeakReference<>(GetCacheData.cacheData(cachedkey)));

        return cacheObject.get(cachedkey).get();
    }

    public void checkBlankArray(@NotNull CACHEDKEY cachedkey) {
        switch (cachedkey) {
            case NETWORKOPERATOR:
            case COUNTRYOPERATOR:
                if (((ArrayList) cacheObject.get(cachedkey).get()).size() == 0)
                    cacheObject.put(cachedkey, new WeakReference<>(GetCacheData.cacheData(cachedkey)));
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
//        Log.e("OneStopKitchen Application --", "------- onLowMemory() called--------------");
    }

    private void checkCredentials() {
        if (AppConstants.QB_APPLICATION_ID.isEmpty() || AppConstants.QB_AUTH_KEY.isEmpty() || AppConstants.QB_AUTH_SECRET.isEmpty() || AppConstants.QB_ACCOUNT_KEY.isEmpty()) {
            throw new AssertionError(getString(R.string.error_qb_credentials_empty));
        }
    }

    private void initCredentials() {
        QBSettings.getInstance().init(getApplicationContext(), AppConstants.QB_APPLICATION_ID, AppConstants.QB_AUTH_KEY, AppConstants.QB_AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(AppConstants.QB_ACCOUNT_KEY);

        // Uncomment and put your Api and Chat servers endpoints if you want to point the sample against your own server.
        QBSettings.getInstance().setEndpoints(AppConstants.API_DOAMIN, AppConstants.CHAT_DOMAIN, ServiceZone.DEVELOPMENT);
        QBSettings.getInstance().setZone(ServiceZone.DEVELOPMENT);
    }

    @Synchronized
    public DbHelper getDbHelper() {
        return dbHelper;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(String shippingCharges) {
        this.shippingCharges = shippingCharges;
    }


    //Demo App credentials
    //    private final String APPLICATION_ID = "78239";
    //    private final String AUTH_KEY = "jUGhxdyWGt-58VR";
    //    private final String AUTH_SECRET = "6h7wZgUWZOzH7p9";
    //    private final String ACCOUNT_KEY = "Y1P_8Yemx68pHaEPy4vo";


    //App credentials
    //    private final String APPLICATION_ID = "4";
    //    private final String AUTH_KEY = "AquOguJUMPBEMMk";
    //    private final String AUTH_SECRET = "mGvE8NQVKpfh2ms";
    //    private final String ACCOUNT_KEY = "LA-ECHmoSCFVh33NYpTh";

}
