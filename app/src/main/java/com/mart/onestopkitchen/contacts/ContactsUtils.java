package com.mart.onestopkitchen.contacts;

import com.okdollar.paymentgateway.utils.Constant;

import java.util.ArrayList;

import static com.mart.onestopkitchen.utils.AppUtils.mptNumber7digitsValidation;
import static com.mart.onestopkitchen.utils.AppUtils.mptNumber8digitsValidation;


public class ContactsUtils implements Constant {


    static ArrayList<String> vCard;


    public static String formatMobileNumber(String countryCode, String mobileNo) {

        if (!countryCode.equals("") && mobileNo.length() > 3) {
            if (!mobileNo.startsWith("+")) {
                if (!mobileNo.startsWith("00")) {
                    if (mobileNo.startsWith("0")) {
                        if (mobileNo.startsWith("09")) {
                            if (!mobileNo.startsWith("090") && !mobileNo.startsWith("090")) {
                                if (validateBurmaNumber(mobileNo) == mobileNo.length()) {
                                    mobileNo = "+95" + "" + mobileNo.substring(1);
                                } else {
                                    mobileNo = countryCode + "" + mobileNo.substring(1);
                                }
                            } else {
                                mobileNo = countryCode + "" + mobileNo.substring(1);
                            }
                        } else {
                            mobileNo = countryCode + "" + mobileNo.substring(1);
                        }
                    } else {
                        mobileNo = countryCode + "" + mobileNo;
                    }
                } else {
                    mobileNo = mobileNo.replaceFirst("00", "+");
                }
            }
        } else {
            mobileNo = countryCode + "" + mobileNo;
        }

        return mobileNo;
    }

    private static int validateBurmaNumber(String number) {
        int isReachMaxLength;
        if (mptNumber7digitsValidation(number)) {
            isReachMaxLength = 9;
        } else if (mptNumber8digitsValidation(number) || number.startsWith("0934") ||
                number.startsWith("0935") || number.toString().startsWith("0936")) {

            isReachMaxLength = 11;
        } else if (mptNumber8digitsValidation(number) ||
                number.startsWith("093")) {
            isReachMaxLength = 10;
        } else {
            isReachMaxLength = 11;
        }

        return isReachMaxLength;
    }


}
