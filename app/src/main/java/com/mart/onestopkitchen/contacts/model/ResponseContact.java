package com.mart.onestopkitchen.contacts.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseContact {

    @SerializedName("SuccessMessage")
    private String successMessage;

    @SerializedName("ErrorList")
    private List<Object> errorList;

    @SerializedName("Data")
    private Object data;

    @SerializedName("StatusCode")
    private int statusCode;

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public List<Object> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Object> errorList) {
        this.errorList = errorList;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}