package com.mart.onestopkitchen.contacts;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.contacts.model.ConstactModel;
import com.mart.onestopkitchen.service.PreferenceService;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static com.mart.onestopkitchen.service.PreferenceService.USER_COUNTRY_CODE;


/**
 * The type Contacts service.
 */
public class ContactsService extends Service {

    private static final String TAG = "ContactsService";
    /**
     * The Flag.
     */
    static boolean flag = false;

    //Holds contact data
    private ArrayList<ConstactModel> newData;
    private ArrayList<ContentValues> contentValueArrayList;

    private static int validateDataIsPresentOrNot(ArrayList<ConstactModel> arrayList, String names, String phoneNumber) {
        int j = -1;
        if (!arrayList.isEmpty()) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getName().equals(names) && arrayList.get(i).getMobileNo().equals(phoneNumber)) {
                    j = i;
                }
            }
        }
        return j;
    }

    private int getContactCount() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

        } else {
            Cursor cursor = null;
            try {
                cursor = MyApplication.getAppContext().getContentResolver().query(
                        ContactsContract.Contacts.CONTENT_URI, null, null, null,
                        null);
                if (cursor != null) {
                    return cursor.getCount();
                } else {
                    return 0;
                }
            } catch (Exception ignore) {
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return 0;
    }

    private ArrayList<ConstactModel> ReadLocalDeviceContacts(boolean isPartialSync) {
        ArrayList<ConstactModel> arrayList = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

        } else {


            try {

                ContentResolver cr = MyApplication.getAppContext().getContentResolver();
                Cursor cursor;
                if (!isPartialSync) {
                    cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                            null, null, ContactsContract.Contacts.DISPLAY_NAME + " DESC ");
                } else {
                    int value1 = getContactCount();
                    int value2 = PreferenceService.getInstance().getTotalPhoneCursorContact(MyApplication.getAppContext());
                    int limit = 0;
                    if (value2 > value1) {
                        limit = value2 - value1;
                    }
                    if (value1 > value2) {
                        limit = value1 - value2;
                    }
                    if (value1 == value2) {
                        limit = 5;
                    }

                    String selection;
                    String sortOrder;
                    String[] selectionArgs = {""};


                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        //add last updated time stamp to the query
                        selection = ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + " > ? ";
                        //let's take change made in last 60 seconds
                        selectionArgs[0] = String.valueOf(System.currentTimeMillis() - (1 * 24 * 60 * 60 * 1000));
                        //sort by descending
                        sortOrder = ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP + " DESC LIMIT " + limit + " OFFSET " + 0 + "";
                        cursor = cr.query(
                                ContactsContract.Contacts.CONTENT_URI,
                                null,
                                selection,
                                selectionArgs,
                                sortOrder);
                    } else {

//                        cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
//                                null, null,
//                                ContactsContract.Contacts.DISPLAY_NAME + " DESC LIMIT " + limit + " OFFSET " + 0 + "");
                        cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                                null, null, ContactsContract.Contacts.DISPLAY_NAME + " DESC ");
                    }
                }


                if (cursor.getCount() > 0) {
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            // get the contact's information
                            String ids = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                            String names = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            Integer hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                            // get the user's email address
                            String emailAddress = "";
                            String countryCode = PreferenceService.getInstance().GetPreferenceValue(USER_COUNTRY_CODE);

                            Cursor ce = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{ids}, null);
                            if (ce != null && ce.moveToFirst()) {
                                do {
                                    emailAddress += ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)) + "-";
                                } while (ce.moveToNext());
                            }
                            if (ce != null && !ce.isClosed()) {
                                ce.close();
                            }
                            // get the user's phone number
                            String phoneNumber = "";
                            String contactUri = "";
                            String lookupKey = "";
                            Uri uri;
                            if (hasPhone > 0) {
                                Cursor cp = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{ids}, null);
                                if (cp != null && cp.moveToFirst()) {
                                    contactUri = cp.getString(cp.getColumnIndex(
                                            ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                                    do {
                                        phoneNumber = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                        lookupKey = cp.getString(cp.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                                        uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);


                                        if (!TextUtils.isEmpty(phoneNumber)) {
                                            if (phoneNumber.contains(" "))
                                                phoneNumber = phoneNumber.replaceAll(" ", "");
                                            if (phoneNumber.contains("-"))
                                                phoneNumber = phoneNumber.replaceAll("-", "");
                                            if (phoneNumber.contains(","))
                                                phoneNumber = phoneNumber.replaceAll(",", "");
                                            if (contactUri == null)
                                                contactUri = "";

                                            phoneNumber = ContactsUtils.formatMobileNumber(countryCode, phoneNumber);
                                            ConstactModel model = new ConstactModel();


                                            Pattern numberPattern = Pattern.compile("[0-9]");

                                            if (names == null || names.equals("null") || names.equals("")) {
                                                names = phoneNumber;
                                            }

                                            if (names.equals(phoneNumber) || numberPattern.matcher(names.substring(0, 1)).matches() ||
                                                    names.equals("+")) {
                                                names = "#";
                                            }


                                            phoneNumber = phoneNumber.replace("+", "00");
                                            // System.out.println(TAG + " phone book contacts syncing " + phoneNumber);

                                            model.setName(names);
                                            model.setMobileNo(phoneNumber);
                                            model.setEmail(emailAddress);
                                            model.setPhotoUrl(contactUri);
                                            model.setId("false");
                                            model.setIsContactUpload("false");
                                            model.setVcfUri(uri.toString());
                                            model.setIsVerified("false");
                                            // System.out.println("db service contact " + names + " ph" + phoneNumber + " email "+emailAddress);
                                            if (!arrayList.isEmpty()) {
                                                int validate = validateDataIsPresentOrNot(arrayList, names, phoneNumber);
                                                if (validate != -1) {

                                                    if (!emailAddress.equals("")) {
                                                        model.setEmail(emailAddress);
                                                    } else if (!arrayList.get(validate).getEmail().equals("")) {
                                                        model.setEmail(arrayList.get(validate).getEmail());
                                                    }

                                                    arrayList.set(validate, model);
                                                    //      System.out.println("db service contact update");
                                                } else {
                                                    arrayList.add(model);
                                                    //      System.out.println("db service contact insert");
                                                }
                                            } else {
                                                arrayList.add(model);
                                            }

                                        }

                                    } while (cp.moveToNext());
                                }
                                if (cp != null && !cp.isClosed())
                                    cp.close();

                            }

                        } while (cursor.moveToNext());


                    }

                }
                if (cursor != null && !cursor.isClosed())
                    cursor.close();


                return arrayList;


            } catch (Exception e) {
                e.printStackTrace();
                return arrayList;
            }
        }

        return arrayList;
    }

    /**
     * Merge local and db contacts array list.
     *
     * @param serverSavedContaacts the server saved contaacts
     * @param localContacts        the local contacts
     * @return the array list
     */
    public ArrayList<ConstactModel> mergeLocalAndDbContacts(ArrayList<ConstactModel> serverSavedContaacts,
                                                            ArrayList<ConstactModel> localContacts) {

        ArrayList<ConstactModel> newData = new ArrayList<>();
        newData.addAll(localContacts);

        try {
            if (serverSavedContaacts.size() > 0) {
                for (int i = 0; i < localContacts.size(); i++) {
                    for (int j = 0; j < serverSavedContaacts.size(); j++) {
                        if (localContacts.get(i).getMobileNo().equals(serverSavedContaacts.get(j).getMobileNo())) {
                            ConstactModel model = new ConstactModel();
                            model.setName(localContacts.get(i).getName().equalsIgnoreCase("") ? serverSavedContaacts.get(j).getName() : localContacts.get(i).getName());
                            model.setMobileNo(localContacts.get(i).getMobileNo());
                            model.setEmail(localContacts.get(i).getEmail().equalsIgnoreCase("") ? serverSavedContaacts.get(j).getEmail() : localContacts.get(i).getEmail());
                            model.setPhotoUrl(localContacts.get(i).getPhotoUrl().equalsIgnoreCase("") ? serverSavedContaacts.get(j).getPhotoUrl() : localContacts.get(i).getPhotoUrl());
                            model.setId(serverSavedContaacts.get(j).getId());
                            model.set_id(serverSavedContaacts.get(j).get_id());
                            model.setIsContactUpload(serverSavedContaacts.get(j).getIsContactUpload());
                            model.setVcfUri(localContacts.get(i).getVcfUri());
                            model.setIsVerified(serverSavedContaacts.get(j).getIsVerified());
                            newData.set(i, model);
                        }
                    }
                }
            }
            return newData;
        } catch (Exception e) {
            e.printStackTrace();
            return newData;
        }
    }

    private void removeDuplicatets(
            ArrayList<ConstactModel> serverSavedContaacts,
            ArrayList<ConstactModel> localContacts) {

        Log.d(TAG, " mergeLocalAndServerContacts  " + serverSavedContaacts.size() + " , DB Contacts  " + localContacts.size());
        ArrayList<ConstactModel> arrayList = new ArrayList<>();
        newData = new ArrayList<>();
        contentValueArrayList = new ArrayList<>();
        try {
            arrayList.addAll(localContacts);
            arrayList.addAll(serverSavedContaacts);
            Log.i(TAG, " merge array size " + arrayList.size());

            if (!arrayList.isEmpty()) {
                for (ConstactModel person : arrayList) {
                    ConstactModel model = new ConstactModel();
                    model.setName(person.getName());
                    model.setMobileNo(person.getMobileNo());
                    model.setEmail(person.getEmail());
                    model.setPhotoUrl(person.getPhotoUrl());
                    model.setId(person.getId());
                    model.setIsContactUpload(person.getIsContactUpload());
                    model.setVcfUri(person.getVcfUri());
                    model.setIsVerified(person.getIsVerified());
                    if (!newData.isEmpty()) {
                        int validate = validateDataIsPresentOrNot(newData, person.getName(), person.getMobileNo());
                        if (validate != -1) {
                            if (!person.getEmail().equals("")) {
                                model.setEmail(person.getEmail());
                            } else if (!newData.get(validate).getEmail().equals("")) {
                                model.setEmail(newData.get(validate).getEmail());
                            }

                            if (!person.getPhotoUrl().equals("")) {
                                model.setPhotoUrl(person.getPhotoUrl());
                            } else if (!newData.get(validate).getPhotoUrl().equals("")) {
                                model.setPhotoUrl(newData.get(validate).getPhotoUrl());
                            }

                            if (!person.getVcfUri().equals("")) {
                                model.setVcfUri(person.getVcfUri());
                            } else if (!newData.get(validate).getVcfUri().equals("")) {
                                model.setVcfUri(newData.get(validate).getVcfUri());
                            }

                            if (newData.get(validate).getId().equals("true")) {
                                model.setId("true");
                            } else if (person.getId().equals("true")) {
                                model.setId("true");
                            } else {
                                model.setId("false");
                            }

                            if (person.getIsContactUpload().equals("true")) {
                                model.setIsContactUpload("true");
                            } else if (newData.get(validate).getIsContactUpload().equals("true")) {
                                model.setIsContactUpload("true");
                            } else {
                                model.setIsContactUpload("false");
                            }


                            if (person.getIsVerified().equals("true")) {
                                model.setIsVerified("true");
                            } else if (newData.get(validate).getIsVerified().equals("true")) {
                                model.setIsVerified("true");
                            } else {
                                model.setIsVerified("false");
                            }

                            setDataContentValue(model, validate);

                        } else {
                            addDataContentValue(model);
                        }
                    } else {
                        addDataContentValue(model);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addDataContentValue(ConstactModel contactModel) {
        this.newData.add(contactModel);
        //Insert contact data
        ContentValues values = new ContentValues();
        //values.put(OKDollar.Contacts._ID, -1);
        values.put(OneStopKitchenListener.Contacts.COLUMN_NAME, contactModel.getName());
        values.put(OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID, contactModel.getEmail());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ID, contactModel.getId());
        values.put(OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL, contactModel.getPhotoUrl());
        values.put(OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER, contactModel.getMobileNo());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED, contactModel.getIsContactUpload());
        values.put(OneStopKitchenListener.Contacts.COLUMN_VCFURI, contactModel.getVcfUri());
        values.put(OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY, contactModel.getIsVerified());

        contentValueArrayList.add(values);

    }

    private void setDataContentValue(ConstactModel contactModel, int index) {
        this.newData.set(index, contactModel);

        ContentValues values = new ContentValues();
        //values.put(OKDollar.Contacts._ID, -1);
        values.put(OneStopKitchenListener.Contacts.COLUMN_NAME, contactModel.getName());
        values.put(OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID, contactModel.getEmail());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ID, contactModel.getId());
        values.put(OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL, contactModel.getPhotoUrl());
        values.put(OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER, contactModel.getMobileNo());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED, contactModel.getIsContactUpload());
        values.put(OneStopKitchenListener.Contacts.COLUMN_VCFURI, contactModel.getVcfUri());
        values.put(OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY, contactModel.getIsVerified());
        contentValueArrayList.set(index, values);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        validateContacts(intent);
        return START_STICKY;
    }

    private void validateContacts(Intent intent) {
        if (ContactDb.getInstance(getApplicationContext()).getContactsDetailsDB().isEmpty()) {
            new GetAllContacts().execute(false);
        } else if (PreferenceService.getInstance().getTotalPhoneCursorContact(MyApplication.getAppContext()) != getContactCount()) {
            new GetAllContacts().execute(true);
        } else if (!PreferenceService.getInstance().getTotalContactSync()) {
            new GetAllContacts().execute(true);
        } else {
            stopSelf();
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * The type Get all contacts.
     */
    @SuppressLint("StaticFieldLeak")
    class GetAllContacts extends AsyncTask<Boolean, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Boolean... booleans) {
            try {
                removeDuplicatets(mergeLocalAndDbContacts(ContactDb.getInstance(getApplicationContext()).getContactsDetailsDB(),
                        ReadLocalDeviceContacts(booleans[0])), ContactDb.getInstance(getApplicationContext()).getContactsDetailsDB());
                if ((newData != null && newData.size() > 0)
                        && (contentValueArrayList != null && contentValueArrayList.size() > 0)) {
                    //Delete Contact previous data
                    getContentResolver().delete(OneStopKitchenListener.Contacts.CONTENT_URI, null, null);
                    Log.d(TAG, " data " + newData.size());
                    ContentValues[] contentValuesArray = new ContentValues[contentValueArrayList.size()];
                    contentValuesArray = contentValueArrayList.toArray(contentValuesArray);
                    getContentResolver().bulkInsert(OneStopKitchenListener.Contacts.CONTENT_URI, contentValuesArray);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d(TAG, " Saved In Local Db ");
            PreferenceService.getInstance().setTotalContactSync(MyApplication.getAppContext(), true);
            PreferenceService.getInstance().setTotalPhoneContact(MyApplication.getAppContext(), newData.size());
            PreferenceService.getInstance().setTotalPhoneCursorContact(MyApplication.getAppContext(), getContactCount());
            stopSelf();
        }
    }


}



