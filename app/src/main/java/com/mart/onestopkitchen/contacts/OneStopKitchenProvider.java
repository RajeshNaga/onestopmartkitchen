/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.mart.onestopkitchen.contacts;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mart.onestopkitchen.data.sqldb.DBHelper;
import com.mart.onestopkitchen.data.sqldb.DatabaseHelper;

import java.util.HashMap;


public class OneStopKitchenProvider extends ContentProvider {


    private static final int CONTACTS = 1;

    private static final int MESSAGES = 2;

    private static final int BUS = 3;

    private static final int KICKBACK = 4;

    private static final int BANKCASHOUTNUMBER = 5;

    private static final UriMatcher URI_MATCHER;

    /**
     * The constant mOpenHelper.
     */
    public static DatabaseHelper mOpenHelper;

    private static HashMap<String, String> sContactsProjectionMap;


    static {

        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

        URI_MATCHER.addURI(OneStopKitchenListener.AUTHORITY, OneStopKitchenListener.Contacts.TABLE_NAME, CONTACTS);


        sContactsProjectionMap = new HashMap<>();


        sContactsProjectionMap.put(OneStopKitchenListener.Contacts._ID,
                OneStopKitchenListener.Contacts._ID);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_NAME,
                OneStopKitchenListener.Contacts.COLUMN_NAME);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID,
                OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_ID,
                OneStopKitchenListener.Contacts.COLUMN_ID);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL,
                OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER,
                OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED,
                OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_VCFURI,
                OneStopKitchenListener.Contacts.COLUMN_VCFURI);

        sContactsProjectionMap.put(OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY,
                OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY);

    }

    @Override
    public boolean onCreate() {
        mOpenHelper = DBHelper.getDataHelper(getContext()).dm;
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setStrict(true);
        switch (URI_MATCHER.match(uri)) {
            // If the incoming URI is for country, chooses the COUNTRY PROFILE
            case CONTACTS:
                qb.setTables(OneStopKitchenListener.Contacts.TABLE_NAME);
                qb.setProjectionMap(sContactsProjectionMap);
                break;

        }
        // Opens the database object in "read" mode, since no writes need to be done.
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();

        /** Performs the query. If no problems occur trying to read the database, then a Cursor
         *  object is returned; otherwise, the cursor variable contains null. If no records were
         *  selected, then the Cursor object is empty, and Cursor.getRownCount() returns 0.
         * */
        try {
            return qb.query(
                    db,            // The database to query
                    projection,    // The columns to return from the query
                    selection,     // The columns for the where clause
                    selectionArgs, // The values for the where clause
                    null,          // don't group the rows
                    null,          // don't filter by row groups
                    sortOrder        // The sort order
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues initialValues) {
        String tableName = "";
        switch (URI_MATCHER.match(uri)) {
            case CONTACTS:
                tableName = OneStopKitchenListener.Contacts.TABLE_NAME;
                break;
            case MESSAGES:
                tableName = OneStopKitchenListener.Messages.TABLE_NAME;
                break;
            case BUS:
                tableName = OneStopKitchenListener.Bus.TABLE_NAME;
                break;
            case KICKBACK:
                tableName = OneStopKitchenListener.KickBack.TABLE_NAME;
                break;
            case BANKCASHOUTNUMBER:
                tableName = OneStopKitchenListener.CashBankOutNumber.TABLE_NAME;
                break;
        }
        if (!TextUtils.isEmpty(tableName)) {
            // Opens the database object in "write" mode.
            SQLiteDatabase db = mOpenHelper.getWritableDatabase();

            // Performs the insert and returns the ID of the new note.
            long rowId = db.insert(
                    tableName,      // The table to insert into.
                    null,   // A hack, SQLite sets this column value to null if values is empty.
                    initialValues   // A map of column names,
                    // and the values to insert into the columns.
            );

            uri = ContentUris.withAppendedId(uri, rowId);


        }

        // If the insert didn't succeed, then the rowID is <= 0. Throws an exception.
        //throw new SQLException("Failed to insert row into " + uri);
        //}
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String where, String[] whereArgs) {
        int count = 0;

        String tableName = "";

        // Does the deleteFavorite based on the incoming URI pattern.
        switch (URI_MATCHER.match(uri)) {
            case CONTACTS:
                tableName = OneStopKitchenListener.Contacts.TABLE_NAME;
                break;


        }

        if (!TextUtils.isEmpty(tableName)) {
            // Opens the database object in "write" mode.
            SQLiteDatabase db = mOpenHelper.getWritableDatabase();
            count = db.delete(
                    tableName,  // The database table name
                    where,                     // The incoming where clause column names
                    whereArgs                  // The incoming where clause values
            );
        }

        // Returns the number of rows deleted.
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int rowIdRequest = 0;

        String tableName = "";

        switch (URI_MATCHER.match(uri)) {
            case CONTACTS:
                tableName = OneStopKitchenListener.Contacts.TABLE_NAME;
                break;

        }

        if (!TextUtils.isEmpty(tableName)) {
            // Opens the database object in "write" mode.
            SQLiteDatabase sqLiteDb = mOpenHelper.getWritableDatabase();

            // Performs the update and returns the ID of the new row.
            rowIdRequest = sqLiteDb.update(
                    tableName,    // The table to update into.
                    values,
                    selection,
                    null);
        }

        return rowIdRequest;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        int rowIdRequest = 0;
        // Opens the database object in "write" mode.
        SQLiteDatabase sqLiteDb = mOpenHelper.getWritableDatabase();

        switch (URI_MATCHER.match(uri)) {
            case CONTACTS:
                String sql = "INSERT INTO " + OneStopKitchenListener.Contacts.TABLE_NAME
                        + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                SQLiteStatement statement = sqLiteDb.compileStatement(sql);

                sqLiteDb.beginTransaction();

                for (ContentValues value : values) {
                    try {
                        statement.clearBindings();
//                        statement.bindString(1,
//                                value.getAsString(OKDollar.Contacts._ID));
                        statement.bindString(2,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_NAME));
                        statement.bindString(3,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID));
                        statement.bindString(4,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_ID));
                        statement.bindString(5,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL));
                        statement.bindString(6,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER));
                        statement.bindString(7,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED));
                        statement.bindString(8,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_VCFURI));
                        statement.bindString(9,
                                value.getAsString(OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY));

                        statement.execute();
                    } catch (Exception ignore) {
                    }
                }

                sqLiteDb.setTransactionSuccessful();
                sqLiteDb.endTransaction();
                break;

            default:
                break;
        }

        return rowIdRequest;
    }


}
