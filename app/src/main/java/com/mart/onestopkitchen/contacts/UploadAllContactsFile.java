package com.mart.onestopkitchen.contacts;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.JsonObject;
import com.mart.onestopkitchen.contacts.model.ConstactModel;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.DateUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mart.onestopkitchen.contacts.UpdateAllContactsOnceSynced.isRequestedSuccess;
import static com.mart.onestopkitchen.service.PreferenceService.USER_MOBILE_NUMBER;
import static com.mart.onestopkitchen.ui.AppConstant.COMMA_DELIMITER;
import static com.mart.onestopkitchen.ui.AppConstant.NEW_LINE_SEPARATOR;
import static com.mart.onestopkitchen.utils.AppConstants.OK_DOLLAR_PROFILE;


/**
 * The type Upload all contacts.
 */
public class UploadAllContactsFile extends Service {

    private final String TAG = getClass().getName();

    public static String getUrl(String data, String key) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            return jsonObject.getString(key);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Map<String, String> getData() {
        Map<String, String> s = new HashMap<>();
    /*    s.put("MobileNumber", PreferenceService.getInstance().GetPreferenceValue(USER_MOBILE_NUMBER));
        s.put("Simid", PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_SIM_ID));
        s.put("MSID", PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_MSID));
        s.put("OSType", "0");
        s.put("OTP", "0");*/
        s.put("MobileNumber", "00959766378289");
        s.put("Simid", "89950617410461187698");
        s.put("MSID", "414061137568260");
        s.put("OSType", "0");
        s.put("OTP", "0");
        return s;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new CreateFile().execute();
        return START_STICKY;
    }

    /**
     * Create csv file boolean.
     *
     * @param listOfContacts the list of contacts
     * @return the boolean
     */
    public Boolean createCSVFile(ArrayList<ConstactModel> listOfContacts) {
        String fileName = "onestopkitchen" + PreferenceService.getInstance().GetPreferenceValue(USER_MOBILE_NUMBER);

        FileWriter fileWriter = null;
        try {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + fileName + ".csv");
            file.createNewFile();
            fileWriter = new FileWriter(file);
            for (ConstactModel model : listOfContacts) {
                fileWriter.append(model.getName().replaceAll(",", " "));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(model.getMobileNo().replaceAll(",", " "));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(model.getEmail().replaceAll(",", " "));
                //fileWriter.append(COMMA_DELIMITER);
                //fileWriter.append(model.getVcfUri().replaceAll(","," "));
                //fileWriter.append(COMMA_DELIMITER);
                //fileWriter.append(model.getPhotoUrl().replaceAll(","," "));
                fileWriter.append(NEW_LINE_SEPARATOR);
            }
            fileWriter.flush();
            fileWriter.close();
            file.getAbsolutePath();
            return true;
        } catch (Exception e) {
            Log.d(TAG, "Error in CsvFileWriter !!!");
            e.printStackTrace();
            return false;
        }
    }

    private void uploadFile(String Url) {
        final String path = "onestopkitchen" + PreferenceService.getInstance().GetPreferenceValue(USER_MOBILE_NUMBER) + ".csv";
        Api service = RetroClient.initializeRetrofitsToken(OK_DOLLAR_PROFILE).create(Api.class);

        File file = new File(Environment.getExternalStorageDirectory() + File.separator + path);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("picture", file.getName(), requestFile);
        Call<JsonObject> newcall = service.putCSVFile(Url, "multipart/form-data;boundary=" + System.currentTimeMillis(), body);
        final String finalPath = path;
        newcall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call,
                                   @NotNull Response<JsonObject> response) {
                File file = new File(Environment.getExternalStorageDirectory(), Environment.getExternalStorageDirectory() + File.separator + path);
                if (file.exists()) {
                    file.delete();
                }
                changeStatusOfContacts();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //validateError();
                File file = new File(Environment.getExternalStorageDirectory(), Environment.getExternalStorageDirectory() + File.separator + path);
                if (file.exists()) {
                    file.delete();
                }
                changeStatusOfContacts();
            }
        });
    }

    private void changeStatusOfContacts() {
        new UpdateDataBaseContacts(ContactDb.getInstance(getApplicationContext()).getAllContactsNotUploaded()).execute();
    }

    private void updateData(ConstactModel contactModel) {
        ContentValues values = new ContentValues();
        values.put(OneStopKitchenListener.Contacts._ID, contactModel.get_id());
        values.put(OneStopKitchenListener.Contacts.COLUMN_NAME, contactModel.getName());
        values.put(OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID, contactModel.getEmail());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ID, contactModel.getId());
        values.put(OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL, contactModel.getPhotoUrl());
        values.put(OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER, contactModel.getMobileNo());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED, contactModel.getIsContactUpload());
        values.put(OneStopKitchenListener.Contacts.COLUMN_VCFURI, contactModel.getVcfUri());
        values.put(OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY, contactModel.getIsVerified());
        getContentResolver().update(OneStopKitchenListener.Contacts.CONTENT_URI, values,
                OneStopKitchenListener.Contacts._ID + " = '"
                        + values.get(OneStopKitchenListener.Contacts._ID) + "'", null);
    }

    private void getUrl() {
        Api service = RetroClient.initializeRetrofitsToken(OK_DOLLAR_PROFILE).create(Api.class);

        Call<JsonObject> call = service.getUrl(getData());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    try {
                        if (isRequestedSuccess(response.body().toString())) {
                            if (!getUrl(response.body().toString(), "Data").equalsIgnoreCase("")) {
//need to change
                                if (!PreferenceService.getInstance().GetPreferenceValue("simid").equals("") &&
                                        !PreferenceService.getInstance().GetPreferenceValue("msid").equals("")) {
                                    //    if (true) {

                                    uploadFile(getUrl(response.body().toString(), "Data"));

                                    // upload(Parse.getUrl(response.body().toString(),Keys.Data),getApplicationContext());
                                }

                            } else {
                                validateError();
                                stopSelf();
                            }
                        } else {
                            validateError();
                            stopSelf();
                        }
                    } catch (Exception e) {
                        validateError();
                        e.printStackTrace();
                        stopSelf();
                    }
                } else {
                    validateError();
                    stopSelf();
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                validateError();
                stopSelf();
            }
        });
    }

    private void validateError() {
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.CONTACTS_UPLOAD_FUNCTION,
                DateUtils.parseDate(AppUtils.AddDays(1), DateUtils.getInputType(9)));
    }

    class UpdateDataBaseContacts extends AsyncTask<Void, Void, Boolean> {

        ArrayList<ConstactModel> arrayList;

        public UpdateDataBaseContacts(ArrayList<ConstactModel> iconLists) {
            this.arrayList = iconLists;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                if (arrayList.size() > 0) {
                    for (ConstactModel model : arrayList) {
                        model.setIsContactUpload("true");
                        updateData(model);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                stopSelf();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean uri) {
            super.onPostExecute(uri);
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.Validate_Contact_Uploaded, true);
            stopSelf();
        }
    }

    class CreateFile extends AsyncTask<Boolean, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Boolean... booleans) {
            try {

                //if (true) {
                //  if (!PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.Validate_Contact_Uploaded)) {
                if (!PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_MOBILE_NUMBER).equalsIgnoreCase("")) {
                    if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_MOBILE_NUMBER).equalsIgnoreCase("")) {
                        if (ContactDb.getInstance(getApplicationContext()).getContactsDetailsDB().size() > 0) {
                            return createCSVFile(ContactDb.getInstance(getApplicationContext()).getContactsDetailsDB());
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {


                boolean call = false;
                if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CONTACTS_UPLOAD_FUNCTION).equals("")) {
                    call = true;
                }

                if (!PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CONTACTS_UPLOAD_FUNCTION).equals("")) {

                    int value = AppUtils.compareDates(AppUtils.getCurrentDateAndTime(),
                            DateUtils.getDate(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CONTACTS_SCHEDULE_FUNCTION_WEEK), DateUtils.getOutputType(9)));
                    call = value >= 0 && value > 0;

                }
//need to change..
                if (!PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_SIM_ID).equals("") &&
                        !PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_DEVICE_ID).equals("")) {

                    //     if ( PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_SIM_ID).equals("") ) {
                    if (call) {
                        getUrl();
                    } else {
                        stopSelf();
                    }
                } else {
                    stopSelf();
                }

            } else {
                stopSelf();
            }
        }
    }

}
