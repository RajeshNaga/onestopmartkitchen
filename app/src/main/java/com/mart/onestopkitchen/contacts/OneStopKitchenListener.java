package com.mart.onestopkitchen.contacts;

import android.net.Uri;
import android.provider.BaseColumns;

import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;


public interface OneStopKitchenListener {

    /**
     * The constant AUTHORITY.
     */
    String AUTHORITY = "onestopkitchen";


    /**
     * The constant DATABASE_NAME.
     */
    String DATABASE_NAME = BuildConfig.APPLICATION_ID;

    /**
     * The constant DATABASE_VERSION.
     */
    int DATABASE_VERSION = 1;
    int[] listOfFlags = {R.drawable.abkhazia, R.drawable.afganistan, R.drawable.albania, R.drawable.algeria, R.drawable.american,
            R.drawable.andorra, R.drawable.angola, R.drawable.anguilla, R.drawable.antigua, R.drawable.argentina,
            R.drawable.armenia, R.drawable.aruba, R.drawable.australia, R.drawable.austria, R.drawable.azerbaijan,
            R.drawable.bahamas, R.drawable.bahrain, R.drawable.bangladesh, R.drawable.barbados, R.drawable.belarus,
            R.drawable.belgium, R.drawable.belize, R.drawable.benin, R.drawable.bermuda, R.drawable.bhutan,
            R.drawable.bolivia, R.drawable.bosnia, R.drawable.botswana, R.drawable.brazil, R.drawable.british,
            R.drawable.brunei, R.drawable.bulgaria, R.drawable.burkina, R.drawable.burundi, R.drawable.cambodia,
            R.drawable.cameroon, R.drawable.canada, R.drawable.cape, R.drawable.cayman, R.drawable.central,
            R.drawable.chad, R.drawable.chile, R.drawable.china, R.drawable.colombia, R.drawable.comoros,
            R.drawable.cook, R.drawable.costa, R.drawable.cote, R.drawable.croatia, R.drawable.cuba,
            R.drawable.cyprus, R.drawable.czech, R.drawable.democratic, R.drawable.denmark, R.drawable.djibouti,
            R.drawable.dominica, R.drawable.dominican, R.drawable.ecuador, R.drawable.egypt, R.drawable.elsalvador,
            R.drawable.equatorial, R.drawable.eritrea, R.drawable.estonia, R.drawable.ethiopia, R.drawable.falklandislands, R.drawable.faroeislands,
            R.drawable.fiji, R.drawable.finland, R.drawable.france, R.drawable.frenchguiana, R.drawable.frenchpolynesia,
            R.drawable.gabon, R.drawable.gambia, R.drawable.georgia, R.drawable.germany, R.drawable.ghana,
            R.drawable.gibraltar, R.drawable.greece, R.drawable.greenland, R.drawable.grenada, R.drawable.guam,
            R.drawable.guatemala, R.drawable.guinea, R.drawable.guineabissau, R.drawable.guyana, R.drawable.haiti,
            R.drawable.honduras, R.drawable.hongkong, R.drawable.hungary, R.drawable.iceland, R.drawable.india,
            R.drawable.india, R.drawable.india, R.drawable.indonesia, R.drawable.iran,
            R.drawable.iraq, R.drawable.ireland, R.drawable.israel, R.drawable.italy, R.drawable.jamaica,
            R.drawable.japan, R.drawable.japan, R.drawable.jordan, R.drawable.kazakhstan, R.drawable.kenya, R.drawable.kiribati,
            R.drawable.koreanorth, R.drawable.koreasouth, R.drawable.kuwait, R.drawable.kyrgyzstan, R.drawable.laos,
            R.drawable.latvia, R.drawable.lebanon, R.drawable.lesotho, R.drawable.liberia, R.drawable.libya,
            R.drawable.liechtenstein, R.drawable.lithuania, R.drawable.luxembourg, R.drawable.macao, R.drawable.macedonia,
            R.drawable.madagascar, R.drawable.malawi, R.drawable.malaysia, R.drawable.maldives, R.drawable.mali,
            R.drawable.malta, R.drawable.marshallisland, R.drawable.martinique, R.drawable.mauritania, R.drawable.mauritius, R.drawable.micronesia,
            R.drawable.moldova, R.drawable.monaco, R.drawable.mongolia, R.drawable.montenegro, R.drawable.montserrat,
            R.drawable.morocco, R.drawable.mozambique, R.drawable.myanmar_flag, R.drawable.namibia, R.drawable.nauru, R.drawable.nepal,
            R.drawable.netherlands, R.drawable.netherlandsantilles, R.drawable.newcaledonia, R.drawable.newzealand, R.drawable.nicaragua,
            R.drawable.niger, R.drawable.nigeria, R.drawable.niue, R.drawable.norway, R.drawable.oman, R.drawable.pakistan,
            R.drawable.palau, R.drawable.panama, R.drawable.papua, R.drawable.paraquay, R.drawable.peru,
            R.drawable.philippines, R.drawable.poland, R.drawable.portugal, R.drawable.puerto, R.drawable.quatar,
            R.drawable.republiccongo, R.drawable.reunion, R.drawable.romania, R.drawable.russia, R.drawable.rwanda,
            R.drawable.saintkittsnevis, R.drawable.saintlucia, R.drawable.saintvincentgrenadines, R.drawable.samoa, R.drawable.sanmarino, R.drawable.saotome,
            R.drawable.saudiarabia, R.drawable.senegal, R.drawable.serbia, R.drawable.seychelles, R.drawable.sierra,
            R.drawable.singapore, R.drawable.slovakia, R.drawable.slovenia, R.drawable.solomon, R.drawable.somalia,
            R.drawable.southafrica, R.drawable.southsudan, R.drawable.spain, R.drawable.srilanka, R.drawable.sthelenaascensionandtristan,
            R.drawable.stpierreandmiquelon, R.drawable.sudan, R.drawable.suriname, R.drawable.swaziland, R.drawable.sweden,
            R.drawable.switzerland, R.drawable.syrian, R.drawable.taiwan, R.drawable.tajikistan, R.drawable.tanzania,
            R.drawable.thailand_flag, R.drawable.timorlesteeasttimor, R.drawable.togo, R.drawable.tonga, R.drawable.trinidadtobago,
            R.drawable.tunisia, R.drawable.turkey, R.drawable.turkmenistan, R.drawable.turkscaicos, R.drawable.tuvalu, R.drawable.uganda,
            R.drawable.uk, R.drawable.ukraine, R.drawable.unitedarabemirates, R.drawable.unitedstates, R.drawable.unitedstates,
            R.drawable.unitedstates, R.drawable.unitedstates, R.drawable.uruguay, R.drawable.uzbekistan, R.drawable.vanuatu, R.drawable.venezuela,
            R.drawable.vietnam, R.drawable.wallisfutuna, R.drawable.yemen, R.drawable.zambia, R.drawable.zimbabwe
    };

    /**
     * The interface Contacts.
     */
    interface Contacts extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "Contacts";

        /**
         * The constant COLUMN_NAME.
         */
        String COLUMN_NAME = "name";

        /**
         * The constant COLUMN_EMAIL_ID.
         */
        String COLUMN_EMAIL_ID = "email";

        /**
         * The constant COLUMN_ID.
         */
        String COLUMN_ID = "id";

        /**
         * The constant COLUMN_PHOTO_URL.
         */
        String COLUMN_PHOTO_URL = "photourl";

        /**
         * The constant COLUMN_MOBILE_NUMBER.
         */
        String COLUMN_MOBILE_NUMBER = "mobileNumber";

        /**
         * The constant COLUMN_ISCONTACT_UPLOAED.
         */
        String COLUMN_ISCONTACT_UPLOAED = "isContactUpload";

        /**
         * The constant COLUMN_VCFURI.
         */
        String COLUMN_VCFURI = "vcfUri";

        /**
         * The constant COLUMN_IS_VERIFY.
         */
        String COLUMN_IS_VERIFY = "IsVerify";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_CONTACTS.
         */
        String PATH_CONTACTS = "/Contacts";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_CONTACTS);

    }


    /**
     * The interface Messages.
     */
    interface Messages extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "table_messages";

        /**
         * The constant COLUMN_MESSAGE_BODY_TEXT.
         */
        String COLUMN_MESSAGE_BODY_TEXT = "message";

        /**
         * The constant COLUMN_SENDER_MOBILE_NUBMER.
         */
        String COLUMN_SENDER_MOBILE_NUBMER = "sender_mb_no";

        /**
         * The constant COLUMN_SOURCE_NUBMER.
         */
        String COLUMN_SOURCE_NUBMER = "source_number";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_MESSAGES.
         */
        String PATH_MESSAGES = "/table_messages";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_MESSAGES);


    }

    /**
     * The interface Bus.
     */
    interface Bus extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "BUSCITY";

        /**
         * The constant COLUMN_KEY_BUS_CITY_ID.
         */
        String COLUMN_KEY_BUS_CITY_ID = "buscityid";

        /**
         * The constant COLUMN_KEY_BUS_CITY_NAME.
         */
        String COLUMN_KEY_BUS_CITY_NAME = "name";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_BUS.
         */
        String PATH_BUS = "/BUSCITY";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_BUS);

    }


    /**
     * The interface Kick back.
     */
    interface KickBack extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "viewkickback";

        /**
         * The constant COLUMN_KICKBACK_MINRANGE.
         */
        String COLUMN_KICKBACK_MINRANGE = "minrange";

        /**
         * The constant COLUMN_KICKBACK_MAXRANGE.
         */
        String COLUMN_KICKBACK_MAXRANGE = "maxrange";

        /**
         * The constant COLUMN_KICKBACK_DISBTYPE.
         */
        String COLUMN_KICKBACK_DISBTYPE = "disbtype";

        /**
         * The constant COLUMN_KICKBACK_PERAMT.
         */
        String COLUMN_KICKBACK_PERAMT = "peramt";

        /**
         * The constant COLUMN_KICKBACK_AMT.
         */
        String COLUMN_KICKBACK_AMT = "amount";

        /**
         * The constant COLUMN_KICKBACK_Status.
         */
        String COLUMN_KICKBACK_Status = "status";

        /**
         * The constant COLUMN_KICKBACK_UPDATE_BIT.
         */
        String COLUMN_KICKBACK_UPDATE_BIT = "update_bit";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_KICKBACK.
         */
        String PATH_KICKBACK = "/viewkickback";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_KICKBACK);


    }


    /**
     * The interface Cash bank out number.
     */
    interface CashBankOutNumber extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "CashBankOutNumber";

        /**
         * The constant COLUMN_MSISIDN.
         */
        String COLUMN_MSISIDN = "Msisidn";

        /**
         * The constant COLUMN_COUNTRYCODE.
         */
        String COLUMN_COUNTRYCODE = "Countrycode";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_CASHBANKOUT.
         */
        String PATH_CASHBANKOUT = "/CashBankOutNumber";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_CASHBANKOUT);


    }


    /**
     * The interface Dummy merchant list.
     */
    interface DummyMerchantList extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "dummymerchantlist_table";

        /**
         * The constant COLUMN_DUMMYMERCHANTLIST_NAME.
         */
        String COLUMN_DUMMYMERCHANTLIST_NAME = "name";

        /**
         * The constant COLUMN_DUMMYMERCHANTLIST_MOBILENO.
         */
        String COLUMN_DUMMYMERCHANTLIST_MOBILENO = "mobileno";

        /**
         * The constant COLUMN_DUMMYMERCHANTLIST_COUNTRY.
         */
        String COLUMN_DUMMYMERCHANTLIST_COUNTRY = "country";

        /**
         * The constant COLUMN_DUMMYMERCHANTLIST_STATE.
         */
        String COLUMN_DUMMYMERCHANTLIST_STATE = "state";

        /**
         * The constant COLUMN_DUMMYMERCHANTLIST_CITY.
         */
        String COLUMN_DUMMYMERCHANTLIST_CITY = "city";

        /**
         * The constant COLUMN_DUMMYMERCHANTLIST_STATUS.
         */
        String COLUMN_DUMMYMERCHANTLIST_STATUS = "status";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_DUMMYMERCHANT.
         */
        String PATH_DUMMYMERCHANT = "/dummymerchantlist_table";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_DUMMYMERCHANT);

    }

    /**
     * The interface Offer.
     */
    interface Offer extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "OFFER";

        /**
         * The constant COLUMN_KEY_CATEG_NAME.
         */
        String COLUMN_KEY_CATEG_NAME = "categoryName";

        /**
         * The constant COLUMN_KEY_CATEG_ID.
         */
        String COLUMN_KEY_CATEG_ID = "categoryId";

        /**
         * The constant COLUMN_KEY_CATEG_DES.
         */
        String COLUMN_KEY_CATEG_DES = "categoryDescription";

        /**
         * The constant COLUMN_KEY_CATEG_BNAME.
         */
        String COLUMN_KEY_CATEG_BNAME = "categoryBName";

        /**
         * The constant COLUMN_KEY_CATEG_BDES.
         */
        String COLUMN_KEY_CATEG_BDES = "categoryBDescription";

        /**
         * The constant COLUMN_KEY_CATEG_CNAME.
         */
        String COLUMN_KEY_CATEG_CNAME = "categoryCName";

        /**
         * The constant COLUMN_KEY_CATEG_CDES.
         */
        String COLUMN_KEY_CATEG_CDES = "categoryCDescription";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_OFFERS.
         */
        String PATH_OFFERS = "/OFFER";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_OFFERS);

    }


    /**
     * The interface Offer code.
     */
    interface OfferCode extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "OFFER_CODE";

        /**
         * The constant COLUMN_KEY_CATEG_MOB.
         */
        String COLUMN_KEY_CATEG_MOB = "mobilenumber";

        /**
         * The constant COLUMN_KEY_CATEG_ID.
         */
        String COLUMN_KEY_CATEG_ID = "categoryId";

        /**
         * The constant COLUMN_KEY_CATEG_PNAME.
         */
        String COLUMN_KEY_CATEG_PNAME = "promotionName";

        /**
         * The constant COLUMN_KEY_CATEG_PCODE.
         */
        String COLUMN_KEY_CATEG_PCODE = "promotionCode";

        /**
         * The constant COLUMN_KEY_CATEG_PSDATE.
         */
        String COLUMN_KEY_CATEG_PSDATE = "promotionStartDate";

        /**
         * The constant COLUMN_KEY_CATEG_PEDATE.
         */
        String COLUMN_KEY_CATEG_PEDATE = "promotionExpiryDate";

        /**
         * The constant COLUMN_KEY_CATEG_PDES.
         */
        String COLUMN_KEY_CATEG_PDES = "promotionDescr";

        /**
         * The constant COLUMN_KEY_CATEG_PIMG.
         */
        String COLUMN_KEY_CATEG_PIMG = "promotionImg";

        /**
         * The constant COLUMN_KEY_CATEG_PID.
         */
        String COLUMN_KEY_CATEG_PID = "promotionId";

        /**
         * The constant COLUMN_KEY_TRANSACTION_TYPE.
         */
        String COLUMN_KEY_TRANSACTION_TYPE = "transactionTypeName";

        /**
         * The constant COLUMN_KEY_PAYMENT_TYPE.
         */
        String COLUMN_KEY_PAYMENT_TYPE = "paymentType";

        /**
         * The constant COLUMN_KEY_PROMOTION_BNAME.
         */
        String COLUMN_KEY_PROMOTION_BNAME = "promotionBName";

        /**
         * The constant COLUMN_KEY_PROMOTION_BDEC.
         */
        String COLUMN_KEY_PROMOTION_BDEC = "promotionBDescription";

        /**
         * The constant COLUMN_KEY_PROMOTION_CNAME.
         */
        String COLUMN_KEY_PROMOTION_CNAME = "promotionCName";

        /**
         * The constant COLUMN_KEY_PROMOTION_CDES.
         */
        String COLUMN_KEY_PROMOTION_CDES = "promotionCDescription";

        /**
         * The constant COLUMN_KEY_AMOUNT_TYPE.
         */
        String COLUMN_KEY_AMOUNT_TYPE = "amountType";

        /**
         * The constant COLUMN_KEY_AMOUNT_VALUE.
         */
        String COLUMN_KEY_AMOUNT_VALUE = "value";

        /**
         * The constant COLUMN_KEY_PROMOTION_TYPE.
         */
        String COLUMN_KEY_PROMOTION_TYPE = "promotionType";

        /**
         * The constant COLUMN_KEY_PREDEFINE_NO.
         */
        String COLUMN_KEY_PREDEFINE_NO = "predefinedMobileNumebrType";

        /**
         * The constant COLUMN_KEY_CUSTOM_ERROR_MSG.
         */
        String COLUMN_KEY_CUSTOM_ERROR_MSG = "customErrorMessage";

        /**
         * The constant COLUMN_KEY_ISLIMIT.
         */
        String COLUMN_KEY_ISLIMIT = "isLimit";

        /**
         * The constant COLUMN_KEY_MINIMUMAMOUNT_BALANCE.
         */
        String COLUMN_KEY_MINIMUMAMOUNT_BALANCE = "minimumBalnceAmount";

        /**
         * The constant COLUMN_KEY_DISCOUNTTO.
         */
        String COLUMN_KEY_DISCOUNTTO = "discountPayTo";

        /**
         * The constant COLUMN_KEY_CASHBACK_FLAG.
         */
        String COLUMN_KEY_CASHBACK_FLAG = "CashBackFlag";

        /**
         * The constant COLUMN_KEY_TERM_CONDITIONB.
         */
        String COLUMN_KEY_TERM_CONDITIONB = "termsConditionB";

        /**
         * The constant COLUMN_KEY_TERM_CONDITION.
         */
        String COLUMN_KEY_TERM_CONDITION = "termsCondition";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_OFFERS_CODE.
         */
        String PATH_OFFERS_CODE = "/OFFER_CODE";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_OFFERS_CODE);


    }


    /**
     * The interface Top up details.
     */
    interface TopUpDetails extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "topup_details";

        /**
         * The constant COLUMN_SOURCE_NO.
         */
        String COLUMN_SOURCE_NO = "source_no";

        /**
         * The constant COLUMN_AMOUNT.
         */
        String COLUMN_AMOUNT = "amount";

        /**
         * The constant COLUMN_DATE.
         */
        String COLUMN_DATE = "date";

        /**
         * The constant COLUMN_BENIFITIARY.
         */
        String COLUMN_BENIFITIARY = "beneficiary_no";

        /**
         * The constant COLUMN_ESTEL_OPERATOR.
         */
        String COLUMN_ESTEL_OPERATOR = "dummymerchant_no";

        /**
         * The constant COLUMN_OPERATOR_NAME.
         */
        String COLUMN_OPERATOR_NAME = "mOperatorName";

        /**
         * The constant COLUMN_TRANSACTION_TYPE.
         */
        String COLUMN_TRANSACTION_TYPE = "Trans_Type";

        /**
         * The constant COLUMN_TOPUP_DATE.
         */
        String COLUMN_TOPUP_DATE = "topup_date";

        /**
         * The constant COLUMN_CASHBACK.
         */
        String COLUMN_CASHBACK = "cashback";

        /**
         * The constant COLUMN_LOYALITY.
         */
        String COLUMN_LOYALITY = "loyality";

        /**
         * The constant COLUMN_TRANSACTION_DATE.
         */
        String COLUMN_TRANSACTION_DATE = "Dateonly";

        /**
         * The constant COLUMN_TOPUP_DAY_OF_MONTH.
         */
        String COLUMN_TOPUP_DAY_OF_MONTH = "DAY";

        /**
         * The constant COLUMN_TOPUP_MONTH_OF_YEAR.
         */
        String COLUMN_TOPUP_MONTH_OF_YEAR = "MONTH";

        /**
         * The constant COLUMN_TOPUP_YEAR.
         */
        String COLUMN_TOPUP_YEAR = "YEAR";

        /**
         * The constant COLUMN_KEY_TRANSID.
         */
        String COLUMN_KEY_TRANSID = "transid";

        /**
         * The constant COLUMN_TOPUP_BALANCE.
         */
        String COLUMN_TOPUP_BALANCE = "balance";

        /**
         * The constant COLUMN_DESTINATION.
         */
        String COLUMN_DESTINATION = "des";

        /**
         * The constant COLUMN_KEY_NAME.
         */
        String COLUMN_KEY_NAME = "name";

        /**
         * The constant COLUMN_KEY_TRANS_STATUS.
         */
        String COLUMN_KEY_TRANS_STATUS = "Trans_Status";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_TOPUP_DETAILS.
         */
        String PATH_TOPUP_DETAILS = "/topup_details";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_TOPUP_DETAILS);


    }

    /**
     * The interface Home icons.
     */
    interface HomeIcons extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "home_icon";

        /**
         * The constant COLUMN_ICON_NAME.
         */
        String COLUMN_ICON_NAME = "icon_name";

        /**
         * The constant COLUMN_ICON_URL.
         */
        String COLUMN_ICON_URL = "icon_url";

        /**
         * The constant COLUMN_ICON_CODE.
         */
        String COLUMN_ICON_CODE = "icon_code";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_HOME_ICONS.
         */
        String PATH_HOME_ICONS = "/home_icon";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_HOME_ICONS);


    }


    /**
     * The interface Recent search.
     */
    interface RecentSearch extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "recent_search";

        /**
         * The constant COLUMN_SEARCH_TEXT.
         */
        String COLUMN_SEARCH_TEXT = "search_text";

        /**
         * The constant COLUMN_SEARCH_DATE.
         */
        String COLUMN_SEARCH_DATE = "date";

        /**
         * The constant COLUMN_SEARCH_TYPE.
         */
        String COLUMN_SEARCH_TYPE = "cat_type";

        /**
         * The constant COLUMN_KEY_ISDISPLAY.
         */
        String COLUMN_KEY_ISDISPLAY = "isDisplay";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_RECENTS_SERACH.
         */
        String PATH_RECENTS_SERACH = "/recent_search";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_RECENTS_SERACH);

    }

    /**
     * The interface Rating.
     */
    interface Rating extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "rating";

        /**
         * The constant COLUMN_KEY_MOBILE_NUMBER.
         */
        String COLUMN_KEY_MOBILE_NUMBER = "mobileNumber";

        /**
         * The constant COLUMN_KEY_RATING.
         */
        String COLUMN_KEY_RATING = "rating";

        /**
         * The constant COLUMN_KEY_NAME.
         */
        String COLUMN_KEY_NAME = "name";

        /**
         * The constant COLUMN_KEY_RATING_ID.
         */
        String COLUMN_KEY_RATING_ID = "ratingid";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_RATING.
         */
        String PATH_RATING = "/rating";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_RATING);

    }


    /**
     * The interface Otp log details.
     */
    interface OtpLogDetails extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "OTP_Log_Details";

        /**
         * The constant COLUMN_GENERATE_DATE.
         */
        String COLUMN_GENERATE_DATE = "generate_date";

        /**
         * The constant COLUMN_AMOUNT.
         */
        String COLUMN_AMOUNT = "amount";

        /**
         * The constant COLUMN_SCANNING_DATE.
         */
        String COLUMN_SCANNING_DATE = "scanning_date";

        /**
         * The constant COLUMN_GENERATE_NAME.
         */
        String COLUMN_GENERATE_NAME = "generate_name";

        /**
         * The constant COLUMN_GENERATE_NO.
         */
        String COLUMN_GENERATE_NO = "generate_no";

        /**
         * The constant COLUMN_SCANNER_NAME.
         */
        String COLUMN_SCANNER_NAME = "scanner_name";

        /**
         * The constant COLUMN_KEY_TRANSTYPE.
         */
        String COLUMN_KEY_TRANSTYPE = "istrans";

        /**
         * The constant COLUMN_KEY_TRANSID.
         */
        String COLUMN_KEY_TRANSID = "transid";

        /**
         * The constant COLUMN_SCANNER_NO.
         */
        String COLUMN_SCANNER_NO = "scanner_no";

        /**
         * The constant COLUMN_CASHBACK.
         */
        String COLUMN_CASHBACK = "cashback";

        /**
         * The constant COLUMN_LOYALITY.
         */
        String COLUMN_LOYALITY = "loyality";

        /**
         * The constant COLUMN_GENDER.
         */
        String COLUMN_GENDER = "Gender";

        /**
         * The constant COLUMN_AGE.
         */
        String COLUMN_AGE = "age";

        /**
         * The constant COLUMN_GLOCATION.
         */
        String COLUMN_GLOCATION = "glocation";

        /**
         * The constant COLUMN_SLOCATION.
         */
        String COLUMN_SLOCATION = "slocation";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_OTP_LOG_DETAILS.
         */
        String PATH_OTP_LOG_DETAILS = "/OTP_Log_Details";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_OTP_LOG_DETAILS);

    }


    /**
     * The interface Bank deposit counter.
     */
    interface BankDepositCounter extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "BankDepositCounter_table";

        /**
         * The constant COLUMN_DATA.
         */
        String COLUMN_DATA = "Data";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_BANK_DEPOSIT_COUNTER.
         */
        String PATH_BANK_DEPOSIT_COUNTER = "/BankDepositCounter_table";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_BANK_DEPOSIT_COUNTER);


    }


    /**
     * The interface Auto top up.
     */
    interface AutoTopUp extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "Auto_topup";

        /**
         * The constant COLUMN_AUTO_TOPUP_MIN_AMT.
         */
        String COLUMN_AUTO_TOPUP_MIN_AMT = "minamt";

        /**
         * The constant COLUMN_AUTOTOPUP_TOPUP_AMT.
         */
        String COLUMN_AUTOTOPUP_TOPUP_AMT = "topupamt";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_AUTO_TOPUP.
         */
        String PATH_AUTO_TOPUP = "/Auto_topup";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_AUTO_TOPUP);


    }


    /**
     * The interface Global search.
     */
    interface GlobalSearch extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "global_search";

        /**
         * The constant COLUMN_SEARCH_DATE.
         */
        String COLUMN_SEARCH_DATE = "date";

        /**
         * The constant COLUMN_SEARCH_NAME.
         */
        String COLUMN_SEARCH_NAME = "search_name";

        /**
         * The constant COLUMN_SEARCH_BURMESE_NAME.
         */
        String COLUMN_SEARCH_BURMESE_NAME = "search_burmese_name";

        /**
         * The constant COLUMN_SEARCH_CHINESE_NAME.
         */
        String COLUMN_SEARCH_CHINESE_NAME = "search_chinese_name";

        /**
         * The constant COLUMN_SEARCH_THAI_NAME.
         */
        String COLUMN_SEARCH_THAI_NAME = "search_thai_name";

        /**
         * The constant COLUMN_SEARCH_TAG.
         */
        String COLUMN_SEARCH_TAG = "search_tag";

        /**
         * The constant COLUMN_SEARCH_IMAGE_NAME.
         */
        String COLUMN_SEARCH_IMAGE_NAME = "search_image_name";

        /**
         * The constant COLUMN_SEARCH_COUNT.
         */
        String COLUMN_SEARCH_COUNT = "search_count";

        /**
         * The constant COLUMN_SEARCH_MENU_COUNT.
         */
        String COLUMN_SEARCH_MENU_COUNT = "search_menu_count";

        /**
         * The constant COLUMN_SEARCH_COUNT_TYPE.
         */
        String COLUMN_SEARCH_COUNT_TYPE = "search_count_type";

        /**
         * The constant COLUMN_SEARCH_IS_LOGIN.
         */
        String COLUMN_SEARCH_IS_LOGIN = "search_isLogin";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_GLOBAL_SEARCH.
         */
        String PATH_GLOBAL_SEARCH = "/global_search";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_GLOBAL_SEARCH);


    }

    /**
     * The interface Advertise.
     */
    interface Advertise extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "AdvertisementDetails";

        /**
         * The constant COLUMN_AD_ImagePath.
         */
        String COLUMN_AD_ImagePath = "ImagePath";

        /**
         * The constant COLUMN_AD_ID.
         */
        String COLUMN_AD_ID = "Id";

        /**
         * The constant COLUMN_AD_URL.
         */
        String COLUMN_AD_URL = "URL";

        /**
         * The constant COLUMN_AD_Description.
         */
        String COLUMN_AD_Description = "Description";

        /**
         * The constant COLUMN_AD_Resolution.
         */
        String COLUMN_AD_Resolution = "Resolution";

        /**
         * The constant COLUMN_AD_Position.
         */
        String COLUMN_AD_Position = "Position";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_ADVERTISE.
         */
        String PATH_ADVERTISE = "/AdvertisementDetails";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_ADVERTISE);


    }


    /**
     * The interface Exist bank profile.
     */
    interface ExistBankProfile extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "Existbank_Profile";

        /**
         * The constant COLUMN_EXIST_ASSET_ID.
         */
        String COLUMN_EXIST_ASSET_ID = "AssetId";

        /**
         * The constant COLUMN_EXIST_BANKID.
         */
        String COLUMN_EXIST_BANKID = "BankId";

        /**
         * The constant COLUMN_EXIST_ACC_NO.
         */
        String COLUMN_EXIST_ACC_NO = "AccountNumber";

        /**
         * The constant COLUMN_EXIST_ACC_TYPE.
         */
        String COLUMN_EXIST_ACC_TYPE = "AccountType";

        /**
         * The constant COLUMN_EXIST_IS_ACTIVE.
         */
        String COLUMN_EXIST_IS_ACTIVE = "IsActive";

        /**
         * The constant COLUMN_EXIST_BRANCHID.
         */
        String COLUMN_EXIST_BRANCHID = "Branchid";

        /**
         * The constant COLUMN_EXIST_ID_TYPE.
         */
        String COLUMN_EXIST_ID_TYPE = "IdType";

        /**
         * The constant COLUMN_EXIST_ID_NO.
         */
        String COLUMN_EXIST_ID_NO = "IdNo";

        /**
         * The constant COLUMN_EXIST_PHONE_NO.
         */
        String COLUMN_EXIST_PHONE_NO = "BankPhoneNumber";

        /**
         * The constant COLUMN_EXIST_ACCOUNT_NAME.
         */
        String COLUMN_EXIST_ACCOUNT_NAME = "AccountName";

        /**
         * The constant COLUMN_EXIST_ACCOUNT_MODEL.
         */
        String COLUMN_EXIST_ACCOUNT_MODEL = "AccountModel";

        /**
         * The constant COLUMN_EXIST_BRANCH_ADDRESS.
         */
        String COLUMN_EXIST_BRANCH_ADDRESS = "BranchAddress";

        /**
         * The constant COLUMN_EXIST_BRANCH_ADDRESS_BURMESE.
         */
        String COLUMN_EXIST_BRANCH_ADDRESS_BURMESE = "BranchAddressBurmese";

        /**
         * The constant COLUMN_EXIST_STSTE.
         */
        String COLUMN_EXIST_STSTE = "State";

        /**
         * The constant COLUMN_EXIST_TOWNSHIP.
         */
        String COLUMN_EXIST_TOWNSHIP = "Township";

        /**
         * The constant COLUMN_EXIST_BANKNAME.
         */
        String COLUMN_EXIST_BANKNAME = "BankName";

        /**
         * The constant COLUMN_EXIST_BRANCHNAME.
         */
        String COLUMN_EXIST_BRANCHNAME = "BranchName";

        /**
         * The constant COLUMN_EXIST_BRANCHNAMEBURMESE.
         */
        String COLUMN_EXIST_BRANCHNAMEBURMESE = "branchBurmeseName";

        /**
         * The constant COLUMN_EXIST_BENEFICIARY_NUMBER.
         */
        String COLUMN_EXIST_BENEFICIARY_NUMBER = "Beneficiary_Mobilenumber";

        /**
         * The constant COLUMN_EXIST_BURMESE_BANK.
         */
        String COLUMN_EXIST_BURMESE_BANK = "burmese_BankName";

        /**
         * The constant COLUMN_EXIST_BURMESE_TOWNSHIP.
         */
        String COLUMN_EXIST_BURMESE_TOWNSHIP = "burmese_Township";

        /**
         * The constant COLUMN_EXIST_BURMESE_DIVISION.
         */
        String COLUMN_EXIST_BURMESE_DIVISION = "burmese_Division";

        /**
         * The constant COLUMN_EMAIL.
         */
        String COLUMN_EMAIL = "email";

        /**
         * The constant COLUMN_REMARKS.
         */
        String COLUMN_REMARKS = "remarks";

        /**
         * The constant COLUMN_SEARCH_DATE.
         */
        String COLUMN_SEARCH_DATE = "date";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_EXISTBANKPROFILE.
         */
        String PATH_EXISTBANKPROFILE = "/Existbank_Profile";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_EXISTBANKPROFILE);


    }


    /**
     * The interface Bank details.
     */
    interface BankDetails extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "BankDetail";

        /**
         * The constant ACCOUNTNUMBER.
         */
        String ACCOUNTNUMBER = "AccountNumber";

        /**
         * The constant BANKNAME.
         */
        String BANKNAME = "BankName";

        /**
         * The constant BANKID.
         */
        String BANKID = "BankId";

        /**
         * The constant ACCOUNTTYPE.
         */
        String ACCOUNTTYPE = "AccountType";

        /**
         * The constant IDTYPE.
         */
        String IDTYPE = "IDType";

        /**
         * The constant STATE.
         */
        String STATE = "State";

        /**
         * The constant TOWNSHIP.
         */
        String TOWNSHIP = "Township";

        /**
         * The constant ADDRESS2.
         */
        String ADDRESS2 = "Address2";

        /**
         * The constant BRANCH.
         */
        String BRANCH = "Branch";

        /**
         * The constant BRANCH_ADDRESS.
         */
        String BRANCH_ADDRESS = "Address";

        /**
         * The constant BANK_PHONE.
         */
        String BANK_PHONE = "Phone";

        /**
         * The constant IDNO.
         */
        String IDNO = "ID_NO";

        /**
         * The constant BRANCH_ID.
         */
        String BRANCH_ID = "Branch_Id";

        /**
         * The constant BANKNAME_BURMESE.
         */
        String BANKNAME_BURMESE = "BankNameBurmese";

        /**
         * The constant BRANCHNAME_BURMESE.
         */
        String BRANCHNAME_BURMESE = "BranchNameBurmese";

        /**
         * The constant ACCOUNTNAME.
         */
        String ACCOUNTNAME = "AccountName";

        /**
         * The constant ACCOUNTMODEL.
         */
        String ACCOUNTMODEL = "AccountModel";

        /**
         * The constant NRCNO.
         */
        String NRCNO = "Nrc_No";

        /**
         * The constant BANK_STATE.
         */
        String BANK_STATE = "Bank_state";

        /**
         * The constant BANK_TOWNSHIP.
         */
        String BANK_TOWNSHIP = "Bank_township";

        /**
         * The constant BANK_BENEFICIARY_MOBNUMBER.
         */
        String BANK_BENEFICIARY_MOBNUMBER = "BeneficiaryMobNumber";

        /**
         * The constant BANK_OSTYPE.
         */
        String BANK_OSTYPE = "Bank_ostype";

        /**
         * The constant BANK_OTP.
         */
        String BANK_OTP = "Bank_otp";

        /**
         * The constant BANK_SIMID.
         */
        String BANK_SIMID = "Bank_simid";

        /**
         * The constant BANK_MSID.
         */
        String BANK_MSID = "Bank_msid";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_BANKDETAILS.
         */
        String PATH_BANKDETAILS = "/BankDetail";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_BANKDETAILS);


    }


    /**
     * The interface Sms.
     */
    interface Sms extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "sms";

        /**
         * The constant KEY_SMS_ID.
         */
        String KEY_SMS_ID = "sms_id";

        /**
         * The constant KEY_THREAD_ID.
         */
        String KEY_THREAD_ID = "thread_id";

        /**
         * The constant KEY_ADDRESS.
         */
        String KEY_ADDRESS = "address";

        /**
         * The constant KEY_BODY.
         */
        String KEY_BODY = "body";

        /**
         * The constant KEY_ISDISPLAY.
         */
        String KEY_ISDISPLAY = "isDisplay";

        /**
         * The constant KEY_DATE.
         */
        String KEY_DATE = "date";

        /**
         * The constant KEY_STATUS.
         */
        String KEY_STATUS = "Status";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_SMS.
         */
        String PATH_SMS = "/sms";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_SMS);


    }

    /**
     * The interface Favourite.
     */
    interface Favourite extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "favourite";

        /**
         * The constant COLUMN_FAV_SHOPNAME.
         */
        String COLUMN_FAV_SHOPNAME = "shopname";

        /**
         * The constant COLUMN_FAV_MOBILE_NUMBER.
         */
        String COLUMN_FAV_MOBILE_NUMBER = "mobilenumber";

        /**
         * The constant COLUMN_FAV_SHOP_STATUS.
         */
        String COLUMN_FAV_SHOP_STATUS = "shopstatus";

        /**
         * The constant COLUMN_FAV_DISTANCE.
         */
        String COLUMN_FAV_DISTANCE = "distance";

        /**
         * The constant COLUMN_FAV_LAT.
         */
        String COLUMN_FAV_LAT = "lat";

        /**
         * The constant COLUMN_FAV_LANG.
         */
        String COLUMN_FAV_LANG = "lang";

        /**
         * The constant COLUMN_FAV_STATUS.
         */
        String COLUMN_FAV_STATUS = "favstatus";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_FAVOURITE.
         */
        String PATH_FAVOURITE = "/favourite";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_FAVOURITE);


    }

    /**
     * The interface Recents.
     */
    interface Recents extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "recent";

        /**
         * The constant COLUMN_SHOPNAME.
         */
        String COLUMN_SHOPNAME = "shopname";

        /**
         * The constant COLUMN_MOBILE_NUMBER.
         */
        String COLUMN_MOBILE_NUMBER = "mobilenumber";

        /**
         * The constant COLUMN_SHOP_STATUS.
         */
        String COLUMN_SHOP_STATUS = "shopstatus";

        /**
         * The constant COLUMN_DISTANCE.
         */
        String COLUMN_DISTANCE = "distance";

        /**
         * The constant COLUMN_LAT.
         */
        String COLUMN_LAT = "lat";

        /**
         * The constant COLUMN_LANG.
         */
        String COLUMN_LANG = "lang";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_RECENTS.
         */
        String PATH_RECENTS = "/recent";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_RECENTS);


    }

    /**
     * The interface Fav contacts.
     */
    interface FavContacts extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "favcontact";

        /**
         * The constant COLUMN_ID.
         */
        String COLUMN_ID = "id";

        /**
         * The constant COLUMN_PHOTO_URL.
         */
        String COLUMN_PHOTO_URL = "photourl";

        /**
         * The constant KEY_NAME.
         */
        String KEY_NAME = "name";

        /**
         * The constant KEY_TRANSTYPE.
         */
        String KEY_TRANSTYPE = "istrans";

        /**
         * The constant KEY_MOBILE_NUMBER.
         */
        String KEY_MOBILE_NUMBER = "mobileNumber";

        /**
         * The constant KEY_DATE.
         */
        String KEY_DATE = "date";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_FAV_CONTACTS.
         */
        String PATH_FAV_CONTACTS = "/favcontact";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_FAV_CONTACTS);


    }


    /**
     * The interface Success transaction logs.
     */
    interface SuccessTransactionLogs extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "SuccessTransactionLogs";

        /**
         * The constant SUCCESS_MOBILE.
         */
        String SUCCESS_MOBILE = "lMobile";

        /**
         * The constant SUCCESS_DESTINATION.
         */
        String SUCCESS_DESTINATION = "lDestination";

        /**
         * The constant SUCCESS_AMOUNT.
         */
        String SUCCESS_AMOUNT = "lAmount";

        /**
         * The constant SUCCESS_TYPE.
         */
        String SUCCESS_TYPE = "lType";

        /**
         * The constant SUCCESS_TRANSID.
         */
        String SUCCESS_TRANSID = "lTransactionID";

        /**
         * The constant SUCCESS_TRANSTIME.
         */
        String SUCCESS_TRANSTIME = "lTransTime";

        /**
         * The constant SUCCESS_WALLATE.
         */
        String SUCCESS_WALLATE = "lWallet";

        /**
         * The constant SUCCESS_LATITUDE.
         */
        String SUCCESS_LATITUDE = "lLatitude";

        /**
         * The constant SUCCESS_LONGITUDE.
         */
        String SUCCESS_LONGITUDE = "lLongitude";

        /**
         * The constant SUCCESS_CELLID.
         */
        String SUCCESS_CELLID = "lCellID";

        /**
         * The constant SUCCESS_RESULTCODE.
         */
        String SUCCESS_RESULTCODE = "resultcode";

        /**
         * The constant SUCCESS_DESCRIPTION.
         */
        String SUCCESS_DESCRIPTION = "descripton";

        /**
         * The constant SUCCESS_MODE.
         */
        String SUCCESS_MODE = "mode";

        /**
         * The constant SUCCESS_COMMENTS.
         */
        String SUCCESS_COMMENTS = "comments";

        /**
         * The constant SUCCESS_START.
         */
        String SUCCESS_START = "start";

        /**
         * The constant SUCCESS_END.
         */
        String SUCCESS_END = "end";

        /**
         * The constant SUCCESS_DIRECTION.
         */
        String SUCCESS_DIRECTION = "direction";

        /**
         * The constant SUCCESS_VENDORTRANSID.
         */
        String SUCCESS_VENDORTRANSID = "vendorTxnId";

        /**
         * The constant SUCCESS_STATUS.
         */
        String SUCCESS_STATUS = "status";

        /**
         * The constant SUCCESS_KICKBACK.
         */
        String SUCCESS_KICKBACK = "kickback";

        /**
         * The constant SUCCESS_BONUS.
         */
        String SUCCESS_BONUS = "bonus";

        /**
         * The constant SUCCESS_BLT.
         */
        String SUCCESS_BLT = "bltdevice";

        /**
         * The constant SUCCESS_WIFI.
         */
        String SUCCESS_WIFI = "wifidevice";

        /**
         * The constant SUCCESS_MNC.
         */
        String SUCCESS_MNC = "mnc";

        /**
         * The constant SUCCESS_MCC.
         */
        String SUCCESS_MCC = "mcc";

        /**
         * The constant SUCCESS_LAC.
         */
        String SUCCESS_LAC = "lac";

        /**
         * The constant SUCCESS_SSID.
         */
        String SUCCESS_SSID = "ssid";

        /**
         * The constant SUCCESS_MERCHANT.
         */
        String SUCCESS_MERCHANT = "merchantname";

        /**
         * The constant SUCCESS_BUSINESS.
         */
        String SUCCESS_BUSINESS = "businessname";

        /**
         * The constant SUCCESS_SIGNAL.
         */
        String SUCCESS_SIGNAL = "signal";

        /**
         * The constant SUCCESS_MAC.
         */
        String SUCCESS_MAC = "mac";


        String SUCCESS_SENDER_BUSINESS = "sender_business";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_SUCCESS_TRANSACTION_LOGS.
         */
        String PATH_SUCCESS_TRANSACTION_LOGS = "/SuccessTransactionLogs";


        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_SUCCESS_TRANSACTION_LOGS);


    }

    /**
     * The interface Notify message storage.
     */
    interface NotifyMessageStorage extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "notify_msg_storage";

        /**
         * The constant KEY_NOTIFY_MSG.
         */
        String KEY_NOTIFY_MSG = "notify_message";

        /**
         * The constant KEY_NOTIFY_DATE.
         */
        String KEY_NOTIFY_DATE = "notify_date";

        /**
         * The constant KEY_NOTIFY_TYPE.
         */
        String KEY_NOTIFY_TYPE = "notify_type";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_NOTIFY_MSG_STORAGE.
         */
        String PATH_NOTIFY_MSG_STORAGE = "/notify_msg_storage";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_NOTIFY_MSG_STORAGE);


    }


    /**
     * The interface Promotion.
     */
    interface Promotion extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "promotion";


        /**
         * The constant PROMOTION_TITLE.
         */
        String PROMOTION_TITLE = "pTitle";

        /**
         * The constant PROMOTION_DESCRIPTION.
         */
        String PROMOTION_DESCRIPTION = "pDescriotion";

        /**
         * The constant PROMOTION_MOBILENO.
         */
        String PROMOTION_MOBILENO = "pMObileNo";

        /**
         * The constant PROMOTION_WEBSITE.
         */
        String PROMOTION_WEBSITE = "pWebsite";

        /**
         * The constant PROMOTION_FACEBOOK.
         */
        String PROMOTION_FACEBOOK = "pFacebook";

        /**
         * The constant PROMOTIONID.
         */
        String PROMOTIONID = "pID";

        /**
         * The constant LAST_UPDATED.
         */
        String LAST_UPDATED = "last_updated";

        /**
         * The constant EMAIL.
         */
        String EMAIL = "email";

        /**
         * The constant CONTACT_NAME.
         */
        String CONTACT_NAME = "name";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_PROMOTION.
         */
        String PATH_PROMOTION = "/promotion";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_PROMOTION);

    }

    /**
     * The interface Merchant shop details.
     */
    interface MerchantShopDetails extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "merchantShopDetails";


        /**
         * The constant ShopID.
         */
        String ShopID = "shopid";

        /**
         * The constant ShopName.
         */
        String ShopName = "shopName";

        /**
         * The constant ShopCellId.
         */
        String ShopCellId = "shopCellid";

        /**
         * The constant ShopStatus.
         */
        String ShopStatus = "shopStatus";

        /**
         * The constant ShopWifiName.
         */
        String ShopWifiName = "shopWifi";

        /**
         * The constant ShopLat.
         */
        String ShopLat = "lat";

        /**
         * The constant ShopLang.
         */
        String ShopLang = "lang";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_MERCHANT_SHOP_DETAILS.
         */
        String PATH_MERCHANT_SHOP_DETAILS = "/merchantShopDetails";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_MERCHANT_SHOP_DETAILS);

    }

    /**
     * The interface Customer survey.
     */
    interface CustomerSurvey extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "CustomerSurvey";


        /**
         * The constant KEY_TRANSID.
         */
        String KEY_TRANSID = "transid";

        /**
         * The constant KEY_TRANSCATION_DATE.
         */
        String KEY_TRANSCATION_DATE = "transcationDate";


        /**
         * The constant SURVEY_QUES1.
         */
        String SURVEY_QUES1 = "ques1";

        /**
         * The constant SURVEY_QUES2.
         */
        String SURVEY_QUES2 = "ques2";

        /**
         * The constant SURVEY_QUES3.
         */
        String SURVEY_QUES3 = "ques3";

        /**
         * The constant SURVEY_QUES4.
         */
        String SURVEY_QUES4 = "ques4";

        /**
         * The constant SURVEY_QUES5.
         */
        String SURVEY_QUES5 = "ques5";

        /**
         * The constant SURVEY_QUES6.
         */
        String SURVEY_QUES6 = "ques6";

        /**
         * The constant SURVEY_QUES7.
         */
        String SURVEY_QUES7 = "ques7";

        /**
         * The constant SURVEY_QUES8.
         */
        String SURVEY_QUES8 = "ques8";

        /**
         * The constant SURVEY_TOTAL.
         */
        String SURVEY_TOTAL = "total";

        /**
         * The constant SURVEY_PHONE.
         */
        String SURVEY_PHONE = "phone";

        /**
         * The constant SURVEY_SUGGESTION.
         */
        String SURVEY_SUGGESTION = "suggestion";

        /**
         * The constant SURVEY_GENDER.
         */
        String SURVEY_GENDER = "gender";

        /**
         * The constant SURVEY_AGE.
         */
        String SURVEY_AGE = "age";

        /**
         * The constant SURVEY_LOCATION.
         */
        String SURVEY_LOCATION = "city";

        /**
         * The constant SURVEY_DAY.
         */
        String SURVEY_DAY = "day";

        /**
         * The constant SURVEY_HOUR.
         */
        String SURVEY_HOUR = "hour";

        /**
         * The constant SURVEY_MINUTE.
         */
        String SURVEY_MINUTE = "minutes";

        /**
         * The constant SURVEY_WEEK.
         */
        String SURVEY_WEEK = "week";

        /**
         * The constant SURVEY_MONTH.
         */
        String SURVEY_MONTH = "month";

        /**
         * The constant SURVEY_YEAR.
         */
        String SURVEY_YEAR = "year";

        /**
         * The constant SURVEY_DATE.
         */
        String SURVEY_DATE = "survey_date";

        /**
         * The constant SURVEY_AMOUNT.
         */
        String SURVEY_AMOUNT = "amount";

        /**
         * The constant SURVEY_DATE_HOUR.
         */
        String SURVEY_DATE_HOUR = "date_hour";

        /**
         * The constant SURVEY_TRANS_TYPE.
         */
        String SURVEY_TRANS_TYPE = "type";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_CUSTOMER_SURVEY.
         */
        String PATH_CUSTOMER_SURVEY = "/CustomerSurvey";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_CUSTOMER_SURVEY);

    }


    /**
     * The interface User profile.
     */
    interface UserProfile extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UserProfile";


        /**
         * The constant MOBILENUMBER.
         */
        String MOBILENUMBER = "MobileNumber";

        /**
         * The constant NAME.
         */
        String NAME = "Name";

        /**
         * The constant GCMID.
         */
        String GCMID = "GcmID";

        /**
         * The constant ENCRYPTED.
         */
        String ENCRYPTED = "Encrypted";

        /**
         * The constant SIMID.
         */
        String SIMID = "SimID";

        /**
         * The constant MSID.
         */
        String MSID = "MSID";

        /**
         * The constant IMEI.
         */
        String IMEI = "IMEI";

        /**
         * The constant LTYPE.
         */
        String LTYPE = "lType";

        /**
         * The constant APPID.
         */
        String APPID = "AppID";

        /**
         * The constant RECOMMENDED.
         */
        String RECOMMENDED = "Recommended";

        /**
         * The constant STATE.
         */
        String STATE = "State";

        /**
         * The constant TOWNSHIP.
         */
        String TOWNSHIP = "Township";

        /**
         * The constant FATHER.
         */
        String FATHER = "Father";

        /**
         * The constant GENDER.
         */
        String GENDER = "Gender";

        /**
         * The constant DATEOFBIRTH.
         */
        String DATEOFBIRTH = "DateOfBirth";

        /**
         * The constant NRC.
         */
        String NRC = "NRC";

        /**
         * The constant IDTYPE.
         */
        String IDTYPE = "IDType";

        /**
         * The constant PHONE.
         */
        String PHONE = "Phone";

        /**
         * The constant BUSINESSTYPE.
         */
        String BUSINESSTYPE = "BusinessType";

        /**
         * The constant BUSINESSCATEGORY.
         */
        String BUSINESSCATEGORY = "BusinessCategory";

        /**
         * The constant CAR.
         */
        String CAR = "Car";

        /**
         * The constant CARTYPE.
         */
        String CARTYPE = "CarType";

        /**
         * The constant LATITUDE.
         */
        String LATITUDE = "Latitude";

        /**
         * The constant LONGITUDE.
         */
        String LONGITUDE = "Longitude";

        /**
         * The constant ADDRESS1.
         */
        String ADDRESS1 = "Address1";

        /**
         * The constant ADDRESS2.
         */
        String ADDRESS2 = "Address2";

        /**
         * The constant ACCOUNTTYPE.
         */
        String ACCOUNTTYPE = "AccountType";

        /**
         * The constant OSTYPE.
         */
        String OSTYPE = "OSType";

        /**
         * The constant PROFILEPIC.
         */
        String PROFILEPIC = "ProfilePic";

        /**
         * The constant SIGNATURE.
         */
        String SIGNATURE = "signature";

        /**
         * The constant PASSWORD.
         */
        String PASSWORD = "Password";

        /**
         * The constant EMAILID.
         */
        String EMAILID = "EmailID";

        /**
         * The constant ADDRESSTYPE.
         */
        String ADDRESSTYPE = "AddressType";

        /**
         * The constant CELLTOWERID.
         */
        String CELLTOWERID = "cell_id";

        /**
         * The constant BUSINESSNAME.
         */
        String BUSINESSNAME = "BusinessName";

        /**
         * The constant KICKBACK_COLUMN.
         */
        String KICKBACK_COLUMN = "Kickback";

        /**
         * The constant CODEALTERNATE.
         */
        String CODEALTERNATE = "CodeAlternate";

        /**
         * The constant COUNTRY.
         */
        String COUNTRY = "Country";

        /**
         * The constant PARENTACCOUNT.
         */
        String PARENTACCOUNT = "ParentAccount";

        /**
         * The constant CODERECOMMENDED.
         */
        String CODERECOMMENDED = "CodeRecommended";

        /**
         * The constant PAYMENTGATEWAY.
         */
        String PAYMENTGATEWAY = "PaymentGateway";

        /**
         * The constant LOYALTY.
         */
        String LOYALTY = "Loyalty";

        /**
         * The constant SECURETOKEN.
         */
        String SECURETOKEN = "SecureToken";

        /**
         * The constant BANKNAME.
         */
        String BANKNAME = "BankName";

        /**
         * The constant FBEMAILID.
         */
        String FBEMAILID = "FBEmailId";

        /**
         * The constant AGENTCODE.
         */
        String AGENTCODE = "AgentCode";

        /**
         * The constant COUNTRY_CODE.
         */
        String COUNTRY_CODE = "CountryCode";


        /**
         * The constant COUNTRY_Of_CITIZEN.
         */
        String COUNTRY_Of_CITIZEN = "CountryOfCitizen";

        /**
         * The constant ID_PHOTO.
         */
        String ID_PHOTO = "IDPhoto";

        /**
         * The constant ID_PHOTO1.
         */
        String ID_PHOTO1 = "IDPhoto1";

        /**
         * The constant ID_EXP_DATE.
         */
        String ID_EXP_DATE = "IDExpiryDate";

        /**
         * The constant HOUSE_BLOCK_NO.
         */
        String HOUSE_BLOCK_NO = "HouseBlockNo";

        /**
         * The constant FLOOR_NO.
         */
        String FLOOR_NO = "HouseFloorNo";

        /**
         * The constant ROOM_NO.
         */
        String ROOM_NO = "HouseRoomNo";

        /**
         * The constant ZONE_NAME.
         */
        String ZONE_NAME = "HouseName";

        /**
         * The constant VILLAGE_NAME.
         */
        String VILLAGE_NAME = "VillageName";

        /**
         * The constant BUSINESS_OPENTIME.
         */
        String BUSINESS_OPENTIME = "OpenTime";

        /**
         * The constant BUSINESS_CLOSETIME.
         */
        String BUSINESS_CLOSETIME = "CloseTime";

        /**
         * The constant BUSINESS_CLOSE_DAYS.
         */
        String BUSINESS_CLOSE_DAYS = "ClosedDays";

        /**
         * The constant BUSINESS_LICENSE_PIC1.
         */
        String BUSINESS_LICENSE_PIC1 = "BusinessIdPhoto1";

        /**
         * The constant BUSINESS_LICENSE_PIC2.
         */
        String BUSINESS_LICENSE_PIC2 = "BusinessIdPhoto2";

        /**
         * The constant SECURITY_QUESTION.
         */
        String SECURITY_QUESTION = "SecurityQuestionCode";

        /**
         * The constant SECURITY_ANSWER.
         */
        String SECURITY_ANSWER = "SecurityQuestionAnswer";

        /**
         * The constant REGISTRATION_STATUS.
         */
        String REGISTRATION_STATUS = "RegistrationStatus";

        /**
         * The constant LANGUAGE.
         */
        String LANGUAGE = "RegistedLanguage";

        /**
         * The constant PASSWORD_TYPE.
         */
        String PASSWORD_TYPE = "PasswordType";

        /**
         * The constant ENCRYPTED_MNO.
         */
        String ENCRYPTED_MNO = "encrypt_mno";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_USER_PROFILE.
         */
        String PATH_USER_PROFILE = "/UserProfile";

        String CREATED_DATE = "created_date";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_USER_PROFILE);

    }

    /**
     * The interface User profile.
     */
    interface Profile extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "PROFILE";


        /**
         * The constant MOBILENUMBER.
         */
        String MOBILENUMBER = "MobileNumber";

        /**
         * The constant NAME.
         */
        String NAME = "Name";

        /**
         * The constant GCMID.
         */
        String GCMID = "GcmID";

        /**
         * The constant ENCRYPTED.
         */
        String ENCRYPTED = "Encrypted";

        /**
         * The constant SIMID.
         */
        String SIMID = "SimID";

        /**
         * The constant MSID.
         */
        String MSID = "MSID";

        /**
         * The constant IMEI.
         */
        String IMEI = "IMEI";

        /**
         * The constant LTYPE.
         */
        String LTYPE = "lType";

        /**
         * The constant APPID.
         */
        String APPID = "AppID";

        /**
         * The constant RECOMMENDED.
         */
        String RECOMMENDED = "Recommended";

        /**
         * The constant STATE.
         */
        String STATE = "State";

        /**
         * The constant TOWNSHIP.
         */
        String TOWNSHIP = "Township";

        /**
         * The constant FATHER.
         */
        String FATHER = "Father";

        /**
         * The constant GENDER.
         */
        String GENDER = "Gender";

        /**
         * The constant DATEOFBIRTH.
         */
        String DATEOFBIRTH = "DateOfBirth";

        /**
         * The constant NRC.
         */
        String NRC = "NRC";

        /**
         * The constant IDTYPE.
         */
        String IDTYPE = "IDType";

        /**
         * The constant PHONE.
         */
        String PHONE = "Phone";

        /**
         * The constant BUSINESSTYPE.
         */
        String BUSINESSTYPE = "BusinessType";

        /**
         * The constant BUSINESSCATEGORY.
         */
        String BUSINESSCATEGORY = "BusinessCategory";

        /**
         * The constant CAR.
         */
        String CAR = "Car";

        /**
         * The constant CARTYPE.
         */
        String CARTYPE = "CarType";

        /**
         * The constant LATITUDE.
         */
        String LATITUDE = "Latitude";

        /**
         * The constant LONGITUDE.
         */
        String LONGITUDE = "Longitude";

        /**
         * The constant ADDRESS1.
         */
        String ADDRESS1 = "Address1";

        /**
         * The constant ADDRESS2.
         */
        String ADDRESS2 = "Address2";

        /**
         * The constant ACCOUNTTYPE.
         */
        String ACCOUNTTYPE = "AccountType";

        /**
         * The constant OSTYPE.
         */
        String OSTYPE = "OSType";

        /**
         * The constant PROFILEPIC.
         */
        String PROFILEPIC = "ProfilePic";

        /**
         * The constant SIGNATURE.
         */
        String SIGNATURE = "signature";

        /**
         * The constant PASSWORD.
         */
        String PASSWORD = "Password";

        /**
         * The constant EMAILID.
         */
        String EMAILID = "EmailID";

        /**
         * The constant ADDRESSTYPE.
         */
        String ADDRESSTYPE = "AddressType";

        /**
         * The constant CELLTOWERID.
         */
        String CELLTOWERID = "cell_id";

        /**
         * The constant BUSINESSNAME.
         */
        String BUSINESSNAME = "BusinessName";

        /**
         * The constant KICKBACK_COLUMN.
         */
        String KICKBACK_COLUMN = "Kickback";

        /**
         * The constant CODEALTERNATE.
         */
        String CODEALTERNATE = "CodeAlternate";

        /**
         * The constant COUNTRY.
         */
        String COUNTRY = "Country";

        /**
         * The constant PARENTACCOUNT.
         */
        String PARENTACCOUNT = "ParentAccount";

        /**
         * The constant CODERECOMMENDED.
         */
        String CODERECOMMENDED = "CodeRecommended";

        /**
         * The constant PAYMENTGATEWAY.
         */
        String PAYMENTGATEWAY = "PaymentGateway";

        /**
         * The constant LOYALTY.
         */
        String LOYALTY = "Loyalty";

        /**
         * The constant SECURETOKEN.
         */
        String SECURETOKEN = "SecureToken";

        /**
         * The constant BANKNAME.
         */
        String BANKNAME = "BankName";

        /**
         * The constant FBEMAILID.
         */
        String FBEMAILID = "FBEmailId";

        /**
         * The constant AGENTCODE.
         */
        String AGENTCODE = "AgentCode";

        /**
         * The constant COUNTRY_CODE.
         */
        String COUNTRY_CODE = "CountryCode";


        /**
         * The constant COUNTRY_Of_CITIZEN.
         */
        String COUNTRY_Of_CITIZEN = "CountryOfCitizen";

        /**
         * The constant ID_PHOTO.
         */
        String ID_PHOTO = "IDPhoto";

        /**
         * The constant ID_PHOTO1.
         */
        String ID_PHOTO1 = "IDPhoto1";

        /**
         * The constant ID_EXP_DATE.
         */
        String ID_EXP_DATE = "IDExpiryDate";

        /**
         * The constant HOUSE_BLOCK_NO.
         */
        String HOUSE_BLOCK_NO = "HouseBlockNo";

        /**
         * The constant FLOOR_NO.
         */
        String FLOOR_NO = "HouseFloorNo";

        /**
         * The constant ROOM_NO.
         */
        String ROOM_NO = "HouseRoomNo";

        /**
         * The constant ZONE_NAME.
         */
        String ZONE_NAME = "HouseName";

        /**
         * The constant VILLAGE_NAME.
         */
        String VILLAGE_NAME = "VillageName";

        /**
         * The constant BUSINESS_OPENTIME.
         */
        String BUSINESS_OPENTIME = "OpenTime";

        /**
         * The constant BUSINESS_CLOSETIME.
         */
        String BUSINESS_CLOSETIME = "CloseTime";

        /**
         * The constant BUSINESS_CLOSE_DAYS.
         */
        String BUSINESS_CLOSE_DAYS = "ClosedDays";

        /**
         * The constant BUSINESS_LICENSE_PIC1.
         */
        String BUSINESS_LICENSE_PIC1 = "BusinessIdPhoto1";

        /**
         * The constant BUSINESS_LICENSE_PIC2.
         */
        String BUSINESS_LICENSE_PIC2 = "BusinessIdPhoto2";

        /**
         * The constant SECURITY_QUESTION.
         */
        String SECURITY_QUESTION = "SecurityQuestionCode";

        /**
         * The constant SECURITY_ANSWER.
         */
        String SECURITY_ANSWER = "SecurityQuestionAnswer";

        /**
         * The constant REGISTRATION_STATUS.
         */
        String REGISTRATION_STATUS = "RegistrationStatus";

        /**
         * The constant LANGUAGE.
         */
        String LANGUAGE = "RegistedLanguage";

        /**
         * The constant PASSWORD_TYPE.
         */
        String PASSWORD_TYPE = "PasswordType";

        /**
         * The constant ENCRYPTED_MNO.
         */
        String ENCRYPTED_MNO = "encrypt_mno";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_USER_PROFILE.
         */
        String PATH_PROFILE = "/PROFILE";

        String CREATED_DATE = "created_date";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_PROFILE);

    }

    /**
     * The interface Send report.
     */
    interface SendReport extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "SendReport";


        /**
         * The constant KEY_NAME.
         */
        String KEY_NAME = "name";

        /**
         * The constant KEY_COMMEENT.
         */
        String KEY_COMMEENT = "comment";

        /**
         * The constant KEY_AMOUNT.
         */
        String KEY_AMOUNT = "amount";

        /**
         * The constant KEY_DATE.
         */
        String KEY_DATE = "date";

        /**
         * The constant KEY_TRANSID.
         */
        String KEY_TRANSID = "transid";

        /**
         * The constant KEY_DESTINATION_NUM.
         */
        String KEY_DESTINATION_NUM = "destination";

        /**
         * The constant KEY_TRANSCATION_DATE.
         */
        String KEY_TRANSCATION_DATE = "transcationDate";

        /**
         * The constant KEY_TRANSCATION_WALLET_BALANCE.
         */
        String KEY_TRANSCATION_WALLET_BALANCE = "walletbalance";


        /**
         * The constant COLUMN_TRANSACTION_DATE.
         */
        String COLUMN_TRANSACTION_DATE = "Dateonly";

        /**
         * The constant COLUMN_TOPUP_DAY_OF_MONTH.
         */
        String COLUMN_TOPUP_DAY_OF_MONTH = "DAY";

        /**
         * The constant COLUMN_TOPUP_MONTH_OF_YEAR.
         */
        String COLUMN_TOPUP_MONTH_OF_YEAR = "MONTH";

        /**
         * The constant COLUMN_TOPUP_YEAR.
         */
        String COLUMN_TOPUP_YEAR = "YEAR";


        /**
         * The constant STATE.
         */
        String STATE = "State";

        /**
         * The constant TOWNSHIP.
         */
        String TOWNSHIP = "Township";

        /**
         * The constant NRC.
         */
        String NRC = "NRC";

        /**
         * The constant IDTYPE.
         */
        String IDTYPE = "IDType";

        /**
         * The constant ACCOUNTTYPE.
         */
        String ACCOUNTTYPE = "AccountType";

        /**
         * The constant ACCOUNTNUMBER.
         */
        String ACCOUNTNUMBER = "AccountNumber";

        /**
         * The constant BANKNAME.
         */
        String BANKNAME = "BankName";


        /**
         * The constant BRANCH.
         */
        String BRANCH = "Branch";

        /**
         * The constant BRANCH_ADDRESS.
         */
        String BRANCH_ADDRESS = "Address";

        /**
         * The constant BANK_PHONE.
         */
        String BANK_PHONE = "Phone";

        /**
         * The constant IDNO.
         */
        String IDNO = "ID_NO";

        /**
         * The constant BRANCH_ID.
         */
        String BRANCH_ID = "Branch_Id";

        /**
         * The constant BANKNAME_BURMESE.
         */
        String BANKNAME_BURMESE = "BankNameBurmese";

        /**
         * The constant BRANCHNAME_BURMESE.
         */
        String BRANCHNAME_BURMESE = "BranchNameBurmese";


        /**
         * The constant KEY_ACCOUNT_MODEL.
         */
        String KEY_ACCOUNT_MODEL = "account_model";

        /**
         * The constant BRANCH_ADDRESS2.
         */
        String BRANCH_ADDRESS2 = "branch_address2";

        /**
         * The constant BANK_ID.
         */
        String BANK_ID = "BankId";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_SEND_REPORT.
         */
        String PATH_SEND_REPORT = "/SendReport";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_SEND_REPORT);

    }

    /**
     * The interface Air time.
     */
    interface AirTime extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "AirTimeReports";


        /**
         * The constant KEY_PRODUCT_ID.
         */
        String KEY_PRODUCT_ID = "ProductId";

        /**
         * The constant KEY_PHONE_NUMBER.
         */
        String KEY_PHONE_NUMBER = "PhoneNumber";

        /**
         * The constant KEY_AIRTIME_AMOUNT.
         */
        String KEY_AIRTIME_AMOUNT = "AirtimeAmount";

        /**
         * The constant KEY_STATUS.
         */
        String KEY_STATUS = "Status";

        /**
         * The constant KEY_CREATE_DATE.
         */
        String KEY_CREATE_DATE = "CreateDate";

        /**
         * The constant KEY_CUSTOMER_ID.
         */
        String KEY_CUSTOMER_ID = "CustomerId";

        /**
         * The constant KEY_TELCO_ID.
         */
        String KEY_TELCO_ID = "TelcoId";

        /**
         * The constant KEY_TELCO_NAME.
         */
        String KEY_TELCO_NAME = "TelcoName";

        /**
         * The constant KEY_SELLING_AMOUNT.
         */
        String KEY_SELLING_AMOUNT = "SellingAmount";

        /**
         * The constant KEY_CUSTOMER_PHN_NO.
         */
        String KEY_CUSTOMER_PHN_NO = "CustomerPhoneNumber";

        /**
         * The constant KEY_GCM_REG_ID.
         */
        String KEY_GCM_REG_ID = "GcmRegistrationId";


        /**
         * The constant KEY_DESTINATION_NUM.
         */
        String KEY_DESTINATION_NUM = "destination";


        /**
         * The constant COLUMN_TRANSACTION_DATE.
         */
        String COLUMN_TRANSACTION_DATE = "Dateonly";

        /**
         * The constant COLUMN_TOPUP_DAY_OF_MONTH.
         */
        String COLUMN_TOPUP_DAY_OF_MONTH = "DAY";

        /**
         * The constant COLUMN_TOPUP_MONTH_OF_YEAR.
         */
        String COLUMN_TOPUP_MONTH_OF_YEAR = "MONTH";

        /**
         * The constant COLUMN_TOPUP_YEAR.
         */
        String COLUMN_TOPUP_YEAR = "YEAR";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_AIR_TIME.
         */
        String PATH_AIR_TIME = "/AirTimeReports";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_AIR_TIME);

    }

    /**
     * The interface Device information.
     */
    interface DeviceInformation extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "DeviceInformation";


        /**
         * The constant COLUMN_DEVICE_ID.
         */
        String COLUMN_DEVICE_ID = "device_id";

        /**
         * The constant COLUMN_MOBILE_NO.
         */
        String COLUMN_MOBILE_NO = "mobile_no";

        /**
         * The constant COLUMN_SIM_ID.
         */
        String COLUMN_SIM_ID = "SIM_ID";

        /**
         * The constant COLUMN_CUS_ID.
         */
        String COLUMN_CUS_ID = "customerID";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_DEVICE_INFO.
         */
        String PATH_DEVICE_INFO = "/DeviceInformation";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_DEVICE_INFO);

    }


    /**
     * The interface Bill payment.
     */
    interface BillPayment extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "BillPayment";

        /**
         * The constant COLUMN_TRANS_DATE.
         */
        String COLUMN_TRANS_DATE = "trans_date";


        /**
         * The constant KEY_NAME.
         */
        String KEY_NAME = "name";

        /**
         * The constant KEY_COMMEENT.
         */
        String KEY_COMMEENT = "comment";

        /**
         * The constant KEY_AMOUNT.
         */
        String KEY_AMOUNT = "amount";

        /**
         * The constant KEY_CAR_NUMBER.
         */
        String KEY_CAR_NUMBER = "carNo";

        /**
         * The constant KEY_MESSAGE.
         */
        String KEY_MESSAGE = "message";

        /**
         * The constant KEY_DATE.
         */
        String KEY_DATE = "date";

        /**
         * The constant KEY_TRANACATION_TYPE.
         */
        String KEY_TRANACATION_TYPE = "tType";

        /**
         * The constant KEY_TRANSTYPE.
         */
        String KEY_TRANSTYPE = "istrans";

        /**
         * The constant KEY_HOURS.
         */
        String KEY_HOURS = "hours";

        /**
         * The constant KEY_BUSINESS_TYPE_LOGO.
         */
        String KEY_BUSINESS_TYPE_LOGO = "businesstype_logo";

        /**
         * The constant KEY_TRANSID.
         */
        String KEY_TRANSID = "transid";

        /**
         * The constant KEY_DESCRIPTION.
         */
        String KEY_DESCRIPTION = "description";

        /**
         * The constant KEY_AGENT_NUM.
         */
        String KEY_AGENT_NUM = "agentnumber";

        /**
         * The constant KEY_FEE.
         */
        String KEY_FEE = "fee";

        /**
         * The constant KEY_SERVICE_TAX.
         */
        String KEY_SERVICE_TAX = "servicetax";

        /**
         * The constant KEY_DESTINATION_NUM.
         */
        String KEY_DESTINATION_NUM = "destination";

        /**
         * The constant KEY_OLD_AMOUNT.
         */
        String KEY_OLD_AMOUNT = "oldamount";

        /**
         * The constant KEY_NEW_AMOUNT.
         */
        String KEY_NEW_AMOUNT = "newamount";

        /**
         * The constant KEY_AGENT_NAME.
         */
        String KEY_AGENT_NAME = "agentname";

        /**
         * The constant KEY_TRANSCATION_DATE.
         */
        String KEY_TRANSCATION_DATE = "transcationDate";

        /**
         * The constant KEY_TRANSCATION_SURVERY.
         */
        String KEY_TRANSCATION_SURVERY = "survery";

        /**
         * The constant KEY_TRANSCATION_WALLET_BALANCE.
         */
        String KEY_TRANSCATION_WALLET_BALANCE = "walletbalance";

        /**
         * The constant KEY_TRANSCATION_RECEIVED_KIKBACK.
         */
        String KEY_TRANSCATION_RECEIVED_KIKBACK = "receivedKickabck";

        /**
         * The constant KEY_TRANSACTION_OPERATOR.
         */
        String KEY_TRANSACTION_OPERATOR = "operator";


        /**
         * The constant KEY_CITY_CODE.
         */
        String KEY_CITY_CODE = "city_code";

        /**
         * The constant COLUMN_TRANSACTION_DATE.
         */
        String COLUMN_TRANSACTION_DATE = "Dateonly";

        /**
         * The constant COLUMN_TOPUP_DAY_OF_MONTH.
         */
        String COLUMN_TOPUP_DAY_OF_MONTH = "DAY";

        /**
         * The constant COLUMN_TOPUP_MONTH_OF_YEAR.
         */
        String COLUMN_TOPUP_MONTH_OF_YEAR = "MONTH";

        /**
         * The constant COLUMN_TOPUP_YEAR.
         */
        String COLUMN_TOPUP_YEAR = "YEAR";


        /**
         * The constant KEY_IMAGE_PATH.
         */
        String KEY_IMAGE_PATH = "imagePath";

        /**
         * The constant KEY_LAT_LONG.
         */
        String KEY_LAT_LONG = "lat_long";

        /**
         * The constant KEY_STATUS_PENDING.
         */
        String KEY_STATUS_PENDING = "status";


        /**
         * The constant GENDER.
         */
        String GENDER = "Gender";

        /**
         * The constant CELLTOWERID.
         */
        String CELLTOWERID = "cell_id";

        /**
         * The constant AGE.
         */
        String AGE = "age";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_BILL_PAYMENT.
         */
        String PATH_BILL_PAYMENT = "/BillPayment";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_BILL_PAYMENT);

    }


    /**
     * The interface Received money.
     */
    interface ReceivedMoney extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "ReceivedMoney";

        /**
         * The constant COLUMN_TRANS_DATE.
         */
        String COLUMN_TRANS_DATE = "trans_date";

        /**
         * The constant KEY_CAR_NUMBER.
         */
        String KEY_CAR_NUMBER = "carNo";

        /**
         * The constant KEY_MESSAGE.
         */
        String KEY_MESSAGE = "message";

        /**
         * The constant KEY_NAME.
         */
        String KEY_NAME = "name";

        /**
         * The constant KEY_KEY_COUNT.
         */
        String KEY_KEY_COUNT = "count";

        /**
         * The constant KEY_COMMEENT.
         */
        String KEY_COMMEENT = "comment";

        /**
         * The constant KEY_AMOUNT.
         */
        String KEY_AMOUNT = "amount";

        /**
         * The constant KEY_BUSINESS_TYPE_LOGO.
         */
        String KEY_BUSINESS_TYPE_LOGO = "businesstype_logo";

        /**
         * The constant KEY_DATE.
         */
        String KEY_DATE = "date";

        /**
         * The constant KEY_TRANACATION_TYPE.
         */
        String KEY_TRANACATION_TYPE = "tType";

        /**
         * The constant KEY_TRANSTYPE.
         */
        String KEY_TRANSTYPE = "istrans";

        /**
         * The constant KEY_HOURS.
         */
        String KEY_HOURS = "hours";

        /**
         * The constant KEY_TRANSID.
         */
        String KEY_TRANSID = "transid";

        /**
         * The constant KEY_DESCRIPTION.
         */
        String KEY_DESCRIPTION = "description";

        /**
         * The constant KEY_AGENT_NUM.
         */
        String KEY_AGENT_NUM = "agentnumber";

        /**
         * The constant KEY_FEE.
         */
        String KEY_FEE = "fee";

        /**
         * The constant KEY_SERVICE_TAX.
         */
        String KEY_SERVICE_TAX = "servicetax";

        /**
         * The constant KEY_DESTINATION_NUM.
         */
        String KEY_DESTINATION_NUM = "destination";

        /**
         * The constant KEY_OLD_AMOUNT.
         */
        String KEY_OLD_AMOUNT = "oldamount";

        /**
         * The constant KEY_NEW_AMOUNT.
         */
        String KEY_NEW_AMOUNT = "newamount";

        /**
         * The constant KEY_AGENT_NAME.
         */
        String KEY_AGENT_NAME = "agentname";

        /**
         * The constant KEY_TRANSCATION_DATE.
         */
        String KEY_TRANSCATION_DATE = "transcationDate";

        /**
         * The constant KEY_TRANSCATION_SURVERY.
         */
        String KEY_TRANSCATION_SURVERY = "survery";

        /**
         * The constant KEY_TRANSCATION_WALLET_BALANCE.
         */
        String KEY_TRANSCATION_WALLET_BALANCE = "walletbalance";

        /**
         * The constant KEY_TRANSCATION_RECEIVED_KIKBACK.
         */
        String KEY_TRANSCATION_RECEIVED_KIKBACK = "receivedKickabck";

        /**
         * The constant KEY_TRANSACTION_OPERATOR.
         */
        String KEY_TRANSACTION_OPERATOR = "operator";


        /**
         * The constant KEY_CITY_CODE.
         */
        String KEY_CITY_CODE = "city_code";

        /**
         * The constant COLUMN_TRANSACTION_DATE.
         */
        String COLUMN_TRANSACTION_DATE = "Dateonly";

        /**
         * The constant COLUMN_TOPUP_DAY_OF_MONTH.
         */
        String COLUMN_TOPUP_DAY_OF_MONTH = "DAY";

        /**
         * The constant COLUMN_TOPUP_MONTH_OF_YEAR.
         */
        String COLUMN_TOPUP_MONTH_OF_YEAR = "MONTH";

        /**
         * The constant COLUMN_TOPUP_YEAR.
         */
        String COLUMN_TOPUP_YEAR = "YEAR";


        /**
         * The constant KEY_IMAGE_PATH.
         */
        String KEY_IMAGE_PATH = "imagePath";

        /**
         * The constant KEY_LAT_LONG.
         */
        String KEY_LAT_LONG = "lat_long";

        /**
         * The constant KEY_STATUS_PENDING.
         */
        String KEY_STATUS_PENDING = "status";


        /**
         * The constant GENDER.
         */
        String GENDER = "Gender";

        /**
         * The constant CELLTOWERID.
         */
        String CELLTOWERID = "cell_id";

        /**
         * The constant AGE.
         */
        String AGE = "age";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_REVEIEVEDMONEY.
         */
        String PATH_REVEIEVEDMONEY = "/ReceivedMoney";


        String SUCCESS_SENDER_BUSINESS = "sender_business";


        String SUCCESS_BUSINESS = "businessname";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_REVEIEVEDMONEY);

    }


    /**
     * The interface Transaction.
     */
    interface Transaction extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "transation_table";

        /**
         * The constant COLUMN_TRANS_DATE.
         */
        String COLUMN_TRANS_DATE = "trans_date";


        /**
         * The constant KEY_NAME.
         */
        String KEY_NAME = "name";

        /**
         * The constant KEY_COMMEENT.
         */
        String KEY_COMMEENT = "comment";

        /**
         * The constant KEY_COMMEENT1.
         */
        String KEY_COMMEENT1 = "comment1";

        /**
         * The constant KEY_AMOUNT.
         */
        String KEY_AMOUNT = "amount";

        /**
         * The constant KEY_DATE.
         */
        String KEY_DATE = "date";

        /**
         * The constant KEY_TRANACATION_TYPE.
         */
        String KEY_TRANACATION_TYPE = "tType";

        /**
         * The constant KEY_CAR_NUMBER.
         */
        String KEY_CAR_NUMBER = "carNo";

        /**
         * The constant KEY_BUSINESS_TYPE_LOGO.
         */
        String KEY_BUSINESS_TYPE_LOGO = "businesstype_logo";

        /**
         * The constant KEY_MESSAGE.
         */
        String KEY_MESSAGE = "message";

        /**
         * The constant KEY_TRANSTYPE.
         */
        String KEY_TRANSTYPE = "istrans";

        /**
         * The constant KEY_HOURS.
         */
        String KEY_HOURS = "hours";

        /**
         * The constant KEY_TRANSID.
         */
        String KEY_TRANSID = "transid";

        /**
         * The constant KEY_DESCRIPTION.
         */
        String KEY_DESCRIPTION = "description";

        /**
         * The constant KEY_AGENT_NUM.
         */
        String KEY_AGENT_NUM = "agentnumber";

        /**
         * The constant KEY_FEE.
         */
        String KEY_FEE = "fee";

        /**
         * The constant KEY_SERVICE_TAX.
         */
        String KEY_SERVICE_TAX = "servicetax";

        /**
         * The constant KEY_DESTINATION_NUM.
         */
        String KEY_DESTINATION_NUM = "destination";

        /**
         * The constant KEY_OLD_AMOUNT.
         */
        String KEY_OLD_AMOUNT = "oldamount";

        /**
         * The constant KEY_NEW_AMOUNT.
         */
        String KEY_NEW_AMOUNT = "newamount";

        /**
         * The constant KEY_AGENT_NAME.
         */
        String KEY_AGENT_NAME = "agentname";

        /**
         * The constant KEY_TRANSCATION_DATE.
         */
        String KEY_TRANSCATION_DATE = "transcationDate";

        /**
         * The constant KEY_TRANSCATION_SURVERY.
         */
        String KEY_TRANSCATION_SURVERY = "survery";

        /**
         * The constant KEY_TRANSCATION_WALLET_BALANCE.
         */
        String KEY_TRANSCATION_WALLET_BALANCE = "walletbalance";

        /**
         * The constant KEY_TRANSCATION_RECEIVED_KIKBACK.
         */
        String KEY_TRANSCATION_RECEIVED_KIKBACK = "receivedKickabck";

        /**
         * The constant KEY_TRANSACTION_OPERATOR.
         */
        String KEY_TRANSACTION_OPERATOR = "operator";


        /**
         * The constant KEY_CITY_CODE.
         */
        String KEY_CITY_CODE = "city_code";

        /**
         * The constant COLUMN_TRANSACTION_DATE.
         */
        String COLUMN_TRANSACTION_DATE = "Dateonly";

        /**
         * The constant COLUMN_TOPUP_DAY_OF_MONTH.
         */
        String COLUMN_TOPUP_DAY_OF_MONTH = "DAY";

        /**
         * The constant COLUMN_TOPUP_MONTH_OF_YEAR.
         */
        String COLUMN_TOPUP_MONTH_OF_YEAR = "MONTH";

        /**
         * The constant COLUMN_TOPUP_YEAR.
         */
        String COLUMN_TOPUP_YEAR = "YEAR";

        /**
         * The constant COLUMN_LOCAL_TRANSACTION_TYPE.
         */
        String COLUMN_LOCAL_TRANSACTION_TYPE = "local_transaction";


        /**
         * The constant KEY_IMAGE_PATH.
         */
        String KEY_IMAGE_PATH = "imagePath";

        /**
         * The constant KEY_LAT_LONG.
         */
        String KEY_LAT_LONG = "lat_long";

        /**
         * The constant KEY_STATUS_PENDING.
         */
        String KEY_STATUS_PENDING = "status";


        /**
         * The constant GENDER.
         */
        String GENDER = "Gender";

        /**
         * The constant CELLTOWERID.
         */
        String CELLTOWERID = "cell_id";

        /**
         * The constant AGE.
         */
        String AGE = "age";


        /**
         * The constant SUCCESS_MERCHANT.
         */
        String SUCCESS_MERCHANT = "merchantname";

        /**
         * The constant SUCCESS_BUSINESS.
         */
        String SUCCESS_BUSINESS = "businessname";


        /**
         * The constant KEY_DESTINATION_NAME.
         */
        String KEY_DESTINATION_NAME = "destinationName";


        String SUCCESS_SENDER_BUSINESS = "sender_business";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_TRANSACTION.
         */
        String PATH_TRANSACTION = "/transation_table";


        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_TRANSACTION);

    }

    /**
     * The interface Update profile commission charge status.
     */
    interface UpdateProfileCommissionChargeStatus extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileCommissionChargeStatus";


        /**
         * The constant COMMISSION_CHARGE_STATUS_CODE.
         */
        String COMMISSION_CHARGE_STATUS_CODE = "CommissionChargeStatusCode";
        /**
         * The constant COMMISSION_CHARGE_STATUS_NAME.
         */
        String COMMISSION_CHARGE_STATUS_NAME = "CommissionChargeStatusName";
        /**
         * The constant COMMISSION_CHARGE_STATUS_BNAME.
         */
        String COMMISSION_CHARGE_STATUS_BNAME = "CommissionChargeStatusBName";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileCommissionChargeStatus.
         */
        String PATH_UpdateProfileCommissionChargeStatus = "/UpdateProfileCommissionChargeStatus";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileCommissionChargeStatus);

    }

    /**
     * The interface Update profile day list.
     */
    interface UpdateProfileDayList extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileDayList";


        /**
         * The constant DAY_POSITION.
         */
        String DAY_POSITION = "DayPosition";
        /**
         * The constant DAY_CODE.
         */
        String DAY_CODE = "DayCode";
        /**
         * The constant DAY_NAME.
         */
        String DAY_NAME = "DayName";
        /**
         * The constant DAY_SHORT_NAME.
         */
        String DAY_SHORT_NAME = "DayShortName";
        /**
         * The constant DAY_BNAME.
         */
        String DAY_BNAME = "DayBName";
        /**
         * The constant DAY_SHORT_BNAME.
         */
        String DAY_SHORT_BNAME = "DayShortBName";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileDayList.
         */
        String PATH_UpdateProfileDayList = "/UpdateProfileDayList";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileDayList);

    }

    /**
     * The interface Update profile ok agent.
     */
    interface UpdateProfileOKAgent extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileOKAgent";

        /**
         * The constant NAME.
         */
        String NAME = "Name";

        /**
         * The constant AGENT_STATUS.
         */
        String AGENT_STATUS = "Agent_Status";

        /**
         * The constant BUSINESSNAME.
         */
        String BUSINESSNAME = "BusinessName";


        /**
         * The constant ID_PHOTO.
         */
        String ID_PHOTO = "IDPhoto";

        /**
         * The constant ID_PHOTO1.
         */
        String ID_PHOTO1 = "IDPhoto1";

        /**
         * The constant REGISTER_NUMBER.
         */
        String REGISTER_NUMBER = "Register_number";
        /**
         * The constant REGISTER_EXP_DATE.
         */
        String REGISTER_EXP_DATE = "Register_exp_date";
        /**
         * The constant REGISTER_AUTHORITY.
         */
        String REGISTER_AUTHORITY = "Register_authority";
        /**
         * The constant LICENSE_NUMBER.
         */
        String LICENSE_NUMBER = "License_number";
        /**
         * The constant LICENSE_EXP_DATE.
         */
        String LICENSE_EXP_DATE = "License_exp_date";
        /**
         * The constant LICENSE_AUTHORITY.
         */
        String LICENSE_AUTHORITY = "License_Authority";
        /**
         * The constant REGISTER_IMAGE1.
         */
        String REGISTER_IMAGE1 = "Register_Image1";
        /**
         * The constant REGISTER_IMAGE2.
         */
        String REGISTER_IMAGE2 = "Register_Image2";
        /**
         * The constant REGISTER_IMAGE3.
         */
        String REGISTER_IMAGE3 = "Register_Image3";
        /**
         * The constant REGISTER_IMAGE4.
         */
        String REGISTER_IMAGE4 = "Register_Image4";
        /**
         * The constant LICENSE_IMAGE1.
         */
        String LICENSE_IMAGE1 = "License_Image1";
        /**
         * The constant LICENSE_IMAGE2.
         */
        String LICENSE_IMAGE2 = "License_Image2";
        /**
         * The constant LICENSE_IMAGE3.
         */
        String LICENSE_IMAGE3 = "License_Image3";
        /**
         * The constant LICENSE_IMAGE4.
         */
        String LICENSE_IMAGE4 = "License_Image4";
        /**
         * The constant SINCE_DATE.
         */
        String SINCE_DATE = "Since_Date";
        /**
         * The constant DOCUMENT_STATUS.
         */
        String DOCUMENT_STATUS = "Document_status";
        /**
         * The constant AGENT_LEVEL.
         */
//String AGENT_STATUS = "Agent_status";
        String AGENT_LEVEL = "Agent_Level";
        /**
         * The constant VERIFY_DATE.
         */
        String VERIFY_DATE = "Verify_Date";
        /**
         * The constant EXPIRE_DATE.
         */
        String EXPIRE_DATE = "Expire_Date";
        /**
         * The constant EXPIRE_STATUS.
         */
        String EXPIRE_STATUS = "Expire_Status";
        /**
         * The constant DOC_IS_ATTACHED.
         */
        String DOC_IS_ATTACHED = "Is_Attached";
        /**
         * The constant ATTACHMENT_PATH.
         */
        String ATTACHMENT_PATH = "Attach_Path";
        /**
         * The constant STATUS.
         */
        String STATUS = "Status";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileOKAgent.
         */
        String PATH_UpdateProfileOKAgent = "/UpdateProfileOKAgent";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileOKAgent);

    }

    /**
     * The interface Update profile ok merchant.
     */
    interface UpdateProfileOKMerchant extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileOKMerchant";

        /**
         * The constant NAME.
         */
        String NAME = "Name";

        /**
         * The constant BUSINESSNAME.
         */
        String BUSINESSNAME = "BusinessName";


        /**
         * The constant ID_PHOTO.
         */
        String ID_PHOTO = "IDPhoto";

        /**
         * The constant ID_PHOTO1.
         */
        String ID_PHOTO1 = "IDPhoto1";

        /**
         * The constant REGISTER_NUMBER.
         */
        String REGISTER_NUMBER = "Register_number";
        /**
         * The constant REGISTER_EXP_DATE.
         */
        String REGISTER_EXP_DATE = "Register_exp_date";
        /**
         * The constant REGISTER_AUTHORITY.
         */
        String REGISTER_AUTHORITY = "Register_authority";
        /**
         * The constant LICENSE_NUMBER.
         */
        String LICENSE_NUMBER = "License_number";
        /**
         * The constant LICENSE_EXP_DATE.
         */
        String LICENSE_EXP_DATE = "License_exp_date";
        /**
         * The constant LICENSE_AUTHORITY.
         */
        String LICENSE_AUTHORITY = "License_Authority";
        /**
         * The constant REGISTER_IMAGE1.
         */
        String REGISTER_IMAGE1 = "Register_Image1";
        /**
         * The constant REGISTER_IMAGE2.
         */
        String REGISTER_IMAGE2 = "Register_Image2";
        /**
         * The constant REGISTER_IMAGE3.
         */
        String REGISTER_IMAGE3 = "Register_Image3";
        /**
         * The constant REGISTER_IMAGE4.
         */
        String REGISTER_IMAGE4 = "Register_Image4";
        /**
         * The constant LICENSE_IMAGE1.
         */
        String LICENSE_IMAGE1 = "License_Image1";
        /**
         * The constant LICENSE_IMAGE2.
         */
        String LICENSE_IMAGE2 = "License_Image2";
        /**
         * The constant LICENSE_IMAGE3.
         */
        String LICENSE_IMAGE3 = "License_Image3";
        /**
         * The constant LICENSE_IMAGE4.
         */
        String LICENSE_IMAGE4 = "License_Image4";
        /**
         * The constant SINCE_DATE.
         */
        String SINCE_DATE = "Since_Date";
        /**
         * The constant DOCUMENT_STATUS.
         */
        String DOCUMENT_STATUS = "Document_status";
        /**
         * The constant VERIFY_DATE.
         */
        String VERIFY_DATE = "Verify_Date";
        /**
         * The constant EXPIRE_DATE.
         */
        String EXPIRE_DATE = "Expire_Date";
        /**
         * The constant EXPIRE_STATUS.
         */
        String EXPIRE_STATUS = "Expire_Status";
        /**
         * The constant DOC_IS_ATTACHED.
         */
        String DOC_IS_ATTACHED = "Is_Attached";
        /**
         * The constant ATTACHMENT_PATH.
         */
        String ATTACHMENT_PATH = "Attach_Path";
        /**
         * The constant STATUS.
         */
        String STATUS = "Status";
        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileOKMerchant.
         */
        String PATH_UpdateProfileOKMerchant = "/UpdateProfileOKMerchant";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileOKMerchant);

    }

    /**
     * The interface Update profile commission charge mode.
     */
    interface UpdateProfileCommissionChargeMode extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileCommissionChargeMode";

        /**
         * The constant COMMISSION_CHARGE_MODE_CODE.
         */
        String COMMISSION_CHARGE_MODE_CODE = "CommissionChargeModeCode";
        /**
         * The constant COMMISSION_CHARGE_MODE_NAME.
         */
        String COMMISSION_CHARGE_MODE_NAME = "CommissionChargeModeName";
        /**
         * The constant COMMISSION_CHARGE_MODE_BNAME.
         */
        String COMMISSION_CHARGE_MODE_BNAME = "CommissionChargeModeBName";


        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileCommissionChargeMode.
         */
        String PATH_UpdateProfileCommissionChargeMode = "/UpdateProfileCommissionChargeMode";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileCommissionChargeMode);

    }

    /**
     * The interface Update profile commission charge fees.
     */
    interface UpdateProfileCommissionChargeFees extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileCommissionChargeFees";

        /**
         * The constant COMMISSION_CHARGE_FEES_CODE.
         */
        String COMMISSION_CHARGE_FEES_CODE = "CommissionChargeModeCode";
        /**
         * The constant COMMISSION_CHARGE_FEES_MIN.
         */
        String COMMISSION_CHARGE_FEES_MIN = "CommissionChargeModeName";
        /**
         * The constant COMMISSION_CHARGE_FEES_MAX.
         */
        String COMMISSION_CHARGE_FEES_MAX = "CommissionChargeModeBName";
        /**
         * The constant COMMISSION_CHARGE_FEES_CHARGE.
         */
        String COMMISSION_CHARGE_FEES_CHARGE = "CommissionChargeModeChargeValue";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileCommissionChargeFees.
         */
        String PATH_UpdateProfileCommissionChargeFees = "/UpdateProfileCommissionChargeFees";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileCommissionChargeFees);

    }

    /**
     * The interface Update profile telco cash back.
     */
    interface UpdateProfileTelcoCashBack extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileTelcoCashBack";

        /**
         * The constant TELCO_CODE.
         */
        String TELCO_CODE = "TelcoCode";
        /**
         * The constant TELCO_NAME.
         */
        String TELCO_NAME = "TelcoName";
        /**
         * The constant TELCO_CASHBACK.
         */
        String TELCO_CASHBACK = "TelcoCashBack";
        /**
         * The constant TELCO_OFFER.
         */
        String TELCO_OFFER = "TelcoOffer";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileTelcoCashBack.
         */
        String PATH_UpdateProfileTelcoCashBack = "/UpdateProfileTelcoCashBack";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileTelcoCashBack);

    }

    /**
     * The interface Update profile other service.
     */
    interface UpdateProfileOtherService {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileOtherService";

        /**
         * The constant SERVICE_CODE.
         */
        String SERVICE_CODE = "ServiceCode";
        /**
         * The constant SERVICE_STATUS.
         */
        String SERVICE_STATUS = "ServiceStatus";
        /**
         * The constant SERVICE_FEES.
         */
        String SERVICE_FEES = "ServiceFees";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileOtherService.
         */
        String PATH_UpdateProfileOtherService = "/UpdateProfileOtherService";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileOtherService);

    }

    /**
     * The interface Update profile cash out.
     */
    interface UpdateProfileCashOut extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileCashOut";

        /**
         * The constant CASH_IN_STATUS.
         */
        String CASH_IN_STATUS = "optionStatus";
        /**
         * The constant CASH_IN_BUSINESS_NAME.
         */
        String CASH_IN_BUSINESS_NAME = "BusinessName";
        /**
         * The constant CASH_IN_MIN_AMOUNT.
         */
        String CASH_IN_MIN_AMOUNT = "Minimum_Amount";
        /**
         * The constant CASH_IN_MAX_AMOUNT.
         */
        String CASH_IN_MAX_AMOUNT = "Maximum_Amount";
        /**
         * The constant CASH_IN_CHARGE_STATUS_TYPE.
         */
        String CASH_IN_CHARGE_STATUS_TYPE = "ChargeStatusType";
        /**
         * The constant CASH_IN_CHARGE_STATUS_NAMES.
         */
        String CASH_IN_CHARGE_STATUS_NAMES = "ChargeStatusNames";
        /**
         * The constant CASH_IN_CHARGE_STATUS_NAME_CODE.
         */
        String CASH_IN_CHARGE_STATUS_NAME_CODE = "ChargeStatusNameCode";
        /**
         * The constant CASH_IN_CHARGE_STATUS_MODE_NAMES.
         */
        String CASH_IN_CHARGE_STATUS_MODE_NAMES = "ChargeStatusModeNames";
        /**
         * The constant CASH_IN_CHARGE_STATUS_MODE_CODE.
         */
        String CASH_IN_CHARGE_STATUS_MODE_CODE = "ChargeStatusModeCode";
        /**
         * The constant CASH_IN_FIRST_AMT.
         */
        String CASH_IN_FIRST_AMT = "First_Amount_Rate";
        /**
         * The constant CASH_IN_SECOND_AMT.
         */
        String CASH_IN_SECOND_AMT = "Second_Amount_Rate";
        /**
         * The constant CASH_IN_THIRD_AMT.
         */
        String CASH_IN_THIRD_AMT = "Third_Amount_Rate";
        /**
         * The constant CASH_IN_LAST_AMT.
         */
        String CASH_IN_LAST_AMT = "Last_Amount_Percent";
        /**
         * The constant CASH_IN_SERVICE_DAY.
         */
        String CASH_IN_SERVICE_DAY = "Service_Day";
        /**
         * The constant CASH_IN_SERVICE_TIME.
         */
        String CASH_IN_SERVICE_TIME = "Service_Time";
        /**
         * The constant CASH_IN_SAME_BUSINESS_TIME.
         */
        String CASH_IN_SAME_BUSINESS_TIME = "IsSameBusinessTime";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileCashOut.
         */
        String PATH_UpdateProfileCashOut = "/UpdateProfileCashOut";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileCashOut);

    }

    /**
     * The interface Update profile cash in.
     */
    interface UpdateProfileCashIn extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileCashIn";


        /**
         * The constant CASH_IN_STATUS.
         */
        String CASH_IN_STATUS = "optionStatus";
        /**
         * The constant CASH_IN_BUSINESS_NAME.
         */
        String CASH_IN_BUSINESS_NAME = "BusinessName";
        /**
         * The constant CASH_IN_MIN_AMOUNT.
         */
        String CASH_IN_MIN_AMOUNT = "Minimum_Amount";
        /**
         * The constant CASH_IN_MAX_AMOUNT.
         */
        String CASH_IN_MAX_AMOUNT = "Maximum_Amount";
        /**
         * The constant CASH_IN_CHARGE_STATUS_TYPE.
         */
        String CASH_IN_CHARGE_STATUS_TYPE = "ChargeStatusType";
        /**
         * The constant CASH_IN_CHARGE_STATUS_NAMES.
         */
        String CASH_IN_CHARGE_STATUS_NAMES = "ChargeStatusNames";
        /**
         * The constant CASH_IN_CHARGE_STATUS_NAME_CODE.
         */
        String CASH_IN_CHARGE_STATUS_NAME_CODE = "ChargeStatusNameCode";
        /**
         * The constant CASH_IN_CHARGE_STATUS_MODE_NAMES.
         */
        String CASH_IN_CHARGE_STATUS_MODE_NAMES = "ChargeStatusModeNames";
        /**
         * The constant CASH_IN_CHARGE_STATUS_MODE_CODE.
         */
        String CASH_IN_CHARGE_STATUS_MODE_CODE = "ChargeStatusModeCode";
        /**
         * The constant CASH_IN_FIRST_AMT.
         */
        String CASH_IN_FIRST_AMT = "First_Amount_Rate";
        /**
         * The constant CASH_IN_LAST_AMT.
         */
        String CASH_IN_LAST_AMT = "Last_Amount_Percent";
        /**
         * The constant CASH_IN_SERVICE_DAY.
         */
        String CASH_IN_SERVICE_DAY = "Service_Day";
        /**
         * The constant CASH_IN_SERVICE_TIME.
         */
        String CASH_IN_SERVICE_TIME = "Service_Time";
        /**
         * The constant CASH_IN_SAME_BUSINESS_TIME.
         */
        String CASH_IN_SAME_BUSINESS_TIME = "IsSameBusinessTime";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileCashIn.
         */
        String PATH_UpdateProfileCashIn = "/UpdateProfileCashIn";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileCashIn);

    }

    /**
     * The interface Update business profile.
     */
    interface UpdateBusinessProfile extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateBusinessProfile";

        /**
         * The constant SUCCESS_STATUS.
         */
        String SUCCESS_STATUS = "status";

        /**
         * The constant PROMOTION_WEBSITE.
         */
        String PROMOTION_WEBSITE = "pWebsite";

        /**
         * The constant STATE.
         */
        String STATE = "State";

        /**
         * The constant TOWNSHIP.
         */
        String TOWNSHIP = "Township";

        /**
         * The constant BUSINESSTYPE.
         */
        String BUSINESSTYPE = "BusinessType";

        /**
         * The constant BUSINESSCATEGORY.
         */
        String BUSINESSCATEGORY = "BusinessCategory";

        /**
         * The constant LATITUDE.
         */
        String LATITUDE = "Latitude";

        /**
         * The constant LONGITUDE.
         */
        String LONGITUDE = "Longitude";

        /**
         * The constant ADDRESS1.
         */
        String ADDRESS1 = "Address1";

        /**
         * The constant ADDRESS2.
         */
        String ADDRESS2 = "Address2";

        /**
         * The constant EMAILID.
         */
        String EMAILID = "EmailID";

        /**
         * The constant ADDRESSTYPE.
         */
        String ADDRESSTYPE = "AddressType";

        /**
         * The constant BUSINESSNAME.
         */
        String BUSINESSNAME = "BusinessName";

        /**
         * The constant FBEMAILID.
         */
        String FBEMAILID = "FBEmailId";


        /**
         * The constant HOUSE_BLOCK_NO.
         */
        String HOUSE_BLOCK_NO = "HouseBlockNo";

        /**
         * The constant FLOOR_NO.
         */
        String FLOOR_NO = "HouseFloorNo";

        /**
         * The constant ROOM_NO.
         */
        String ROOM_NO = "HouseRoomNo";

        /**
         * The constant ZONE_NAME.
         */
        String ZONE_NAME = "HouseName";

        /**
         * The constant VILLAGE_NAME.
         */
        String VILLAGE_NAME = "VillageName";

        /**
         * The constant BUSINESS_CLOSE_DAYS.
         */
        String BUSINESS_CLOSE_DAYS = "ClosedDays";

        /**
         * The constant MEMBERSHIP.
         */
        String MEMBERSHIP = "Membership";
        /**
         * The constant BRANCHOUTLET.
         */
        String BRANCHOUTLET = "BranchOutlet";
        /**
         * The constant OWNERSHIP_TYPE.
         */
        String OWNERSHIP_TYPE = "OwnershipType";
        /**
         * The constant REGISTER_NUMBER.
         */
        String REGISTER_NUMBER = "Register_number";
        /**
         * The constant REGISTER_EXP_DATE.
         */
        String REGISTER_EXP_DATE = "Register_exp_date";
        /**
         * The constant REGISTER_AUTHORITY.
         */
        String REGISTER_AUTHORITY = "Register_authority";
        /**
         * The constant LICENSE_NUMBER.
         */
        String LICENSE_NUMBER = "License_number";
        /**
         * The constant LICENSE_EXP_DATE.
         */
        String LICENSE_EXP_DATE = "License_exp_date";
        /**
         * The constant LICENSE_AUTHORITY.
         */
        String LICENSE_AUTHORITY = "License_Authority";
        /**
         * The constant REGISTER_IMAGE1.
         */
        String REGISTER_IMAGE1 = "Register_Image1";
        /**
         * The constant REGISTER_IMAGE2.
         */
        String REGISTER_IMAGE2 = "Register_Image2";
        /**
         * The constant REGISTER_IMAGE3.
         */
        String REGISTER_IMAGE3 = "Register_Image3";
        /**
         * The constant REGISTER_IMAGE4.
         */
        String REGISTER_IMAGE4 = "Register_Image4";
        /**
         * The constant LICENSE_IMAGE1.
         */
        String LICENSE_IMAGE1 = "License_Image1";
        /**
         * The constant LICENSE_IMAGE2.
         */
        String LICENSE_IMAGE2 = "License_Image2";
        /**
         * The constant LICENSE_IMAGE3.
         */
        String LICENSE_IMAGE3 = "License_Image3";
        /**
         * The constant LICENSE_IMAGE4.
         */
        String LICENSE_IMAGE4 = "License_Image4";
        /**
         * The constant LOGO_IMAGE.
         */
        String LOGO_IMAGE = "Logo_Image";
        /**
         * The constant PRIMARY_IMAGE.
         */
        String PRIMARY_IMAGE = "Primary_Image";
        /**
         * The constant BUSINESS_IMAGE1.
         */
        String BUSINESS_IMAGE1 = "Business_Image1";
        /**
         * The constant BUSINESS_IMAGE2.
         */
        String BUSINESS_IMAGE2 = "Business_Image2";
        /**
         * The constant BUSINESS_IMAGE3.
         */
        String BUSINESS_IMAGE3 = "Business_Image3";
        /**
         * The constant BUSINESS_IMAGE4.
         */
        String BUSINESS_IMAGE4 = "Business_Image4";
        /**
         * The constant WORKING_DAYS.
         */
        String WORKING_DAYS = "Working_Days";
        /**
         * The constant WORKING_TIME.
         */
        String WORKING_TIME = "Working_Time";
        /**
         * The constant ADDITIONAL_MOBILE_LIST.
         */
        String ADDITIONAL_MOBILE_LIST = "Additional_Mobile_List";
        /**
         * The constant ADDITIONAL_MOBILE_TYPE_LIST.
         */
        String ADDITIONAL_MOBILE_TYPE_LIST = "Additional_Mobile_Type_List";
        /**
         * The constant IMAGE_STATUS.
         */
        String IMAGE_STATUS = "Image_Status";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateBusinessProfile.
         */
        String PATH_UpdateBusinessProfile = "/UpdateBusinessProfile";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateBusinessProfile);

    }

    /**
     * The interface Update personal profile.
     */
    interface UpdatePersonalProfile extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdatePersonalProfile";

        /**
         * The constant QUALIFICATION.
         */
        String QUALIFICATION = "Qualification";
        /**
         * The constant OCCUPATION.
         */
        String OCCUPATION = "Occupation";
        /**
         * The constant STUDENT_INFO.
         */
        String STUDENT_INFO = "Student_Info";
        /**
         * The constant LICENSE_INFO.
         */
        String LICENSE_INFO = "License_Info";
        /**
         * The constant PASSPORT_INFO.
         */
        String PASSPORT_INFO = "Passport_Info";

        /**
         * The constant MOBILENUMBER.
         */
        String MOBILENUMBER = "MobileNumber";

        /**
         * The constant NAME.
         */
        String NAME = "Name";

        /**
         * The constant CREATED_DATE.
         */
        String CREATED_DATE = "Created_Date";

        /**
         * The constant SUCCESS_STATUS.
         */
        String SUCCESS_STATUS = "status";

        /**
         * The constant RECOMMENDED.
         */
        String RECOMMENDED = "Recommended";

        /**
         * The constant STATE.
         */
        String STATE = "State";

        /**
         * The constant TOWNSHIP.
         */
        String TOWNSHIP = "Township";

        /**
         * The constant FATHER.
         */
        String FATHER = "Father";

        /**
         * The constant GENDER.
         */
        String GENDER = "Gender";

        /**
         * The constant DATEOFBIRTH.
         */
        String DATEOFBIRTH = "DateOfBirth";

        /**
         * The constant NRC.
         */
        String NRC = "NRC";

        /**
         * The constant IDTYPE.
         */
        String IDTYPE = "IDType";

        /**
         * The constant LATITUDE.
         */
        String LATITUDE = "Latitude";

        /**
         * The constant LONGITUDE.
         */
        String LONGITUDE = "Longitude";

        /**
         * The constant ADDRESS1.
         */
        String ADDRESS1 = "Address1";

        /**
         * The constant ADDRESS2.
         */
        String ADDRESS2 = "Address2";

        /**
         * The constant ACCOUNTTYPE.
         */
        String ACCOUNTTYPE = "AccountType";

        /**
         * The constant PROFILEPIC.
         */
        String PROFILEPIC = "ProfilePic";

        /**
         * The constant EMAILID.
         */
        String EMAILID = "EmailID";

        /**
         * The constant ADDRESSTYPE.
         */
        String ADDRESSTYPE = "AddressType";

        /**
         * The constant FBEMAILID.
         */
        String FBEMAILID = "FBEmailId";

        /**
         * The constant COUNTRY_CODE.
         */
        String COUNTRY_CODE = "CountryCode";


        /**
         * The constant COUNTRY_Of_CITIZEN.
         */
        String COUNTRY_Of_CITIZEN = "CountryOfCitizen";

        /**
         * The constant ID_PHOTO.
         */
        String ID_PHOTO = "IDPhoto";

        /**
         * The constant ID_PHOTO1.
         */
        String ID_PHOTO1 = "IDPhoto1";

        /**
         * The constant HOUSE_BLOCK_NO.
         */
        String HOUSE_BLOCK_NO = "HouseBlockNo";

        /**
         * The constant FLOOR_NO.
         */
        String FLOOR_NO = "HouseFloorNo";

        /**
         * The constant ROOM_NO.
         */
        String ROOM_NO = "HouseRoomNo";

        /**
         * The constant ZONE_NAME.
         */
        String ZONE_NAME = "HouseName";

        /**
         * The constant VILLAGE_NAME.
         */
        String VILLAGE_NAME = "VillageName";

        /**
         * The constant SECURITY_QUESTION.
         */
        String SECURITY_QUESTION = "SecurityQuestionCode";

        /**
         * The constant SECURITY_ANSWER.
         */
        String SECURITY_ANSWER = "SecurityQuestionAnswer";

        /**
         * The constant LANGUAGE.
         */
        String LANGUAGE = "RegistedLanguage";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdatePersonalProfile.
         */
        String PATH_UpdatePersonalProfile = "/UpdatePersonalProfile";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdatePersonalProfile);

    }

    /**
     * The interface Update profile menu.
     */
    interface UpdateProfileMenu extends BaseColumns {

        /**
         * The constant TABLE_NAME.
         */
        String TABLE_NAME = "UpdateProfileMenu";
        /**
         * The constant MOBILENUMBER.
         */
        String MOBILENUMBER = "MobileNumber";
        /**
         * The constant ALL_STATUS.
         */
        String ALL_STATUS = "Full_Menu_Status";
        /**
         * The constant PERSONAL_STATUS.
         */
        String PERSONAL_STATUS = "Personal_Status";
        /**
         * The constant BUSINESS_STATUS.
         */
        String BUSINESS_STATUS = "Business_Status";
        /**
         * The constant CASHIN_STATUS.
         */
        String CASHIN_STATUS = "CashIn_Status";
        /**
         * The constant CASHOUT_STATUS.
         */
        String CASHOUT_STATUS = "CashOut_Status";
        /**
         * The constant OTHER_SERVICE_STATUS.
         */
        String OTHER_SERVICE_STATUS = "OtherService_Status";
        /**
         * The constant MERCHANT_STATUS.
         */
        String MERCHANT_STATUS = "Merchant_Status";
        /**
         * The constant AGENT_STATUS.
         */
        String AGENT_STATUS = "Agent_Status";
        /**
         * The constant BUSINESS_TEMP_CLOSE_STATUS.
         */
        String BUSINESS_TEMP_CLOSE_STATUS = "Business_Temp_Status";
        /**
         * The constant IS_BUSINESS_STATUS.
         */
        String IS_BUSINESS_STATUS = "IsBusinessRegistered";

        /**
         * The constant SCHEME.
         */
        String SCHEME = "content://";

        /**
         * The constant PATH_UpdateProfileMenu.
         */
        String PATH_UpdateProfileMenu = "/UpdateProfileMenu";

        /**
         * The constant CONTENT_URI.
         */
        Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_UpdateProfileMenu);

    }
}
