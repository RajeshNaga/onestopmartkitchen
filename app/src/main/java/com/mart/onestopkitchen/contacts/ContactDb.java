package com.mart.onestopkitchen.contacts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;

import com.mart.onestopkitchen.contacts.model.ConstactModel;
import com.mart.onestopkitchen.data.sqldb.DBHelper;
import com.mart.onestopkitchen.service.PreferenceService;

import java.util.ArrayList;

public class ContactDb implements OneStopKitchenListener.Contacts {

    @SuppressLint("StaticFieldLeak")
    private static ContactDb instance;
    private PreferenceService mPrefHelper;
    private DBHelper mDbHelper;
    private Context mContext;

    public static ContactDb getInstance(Context mContext) {
        if (null == instance)
            instance = new ContactDb();
        instance.init(mContext);
        return instance;
    }

    private void init(Context mContext) {
        this.mContext = mContext;
        mDbHelper = DBHelper.getDataHelper(mContext);
        mPrefHelper = PreferenceService.getInstance();
    }

    public ArrayList<ConstactModel> getContactsDetailsDB() {
        ArrayList<ConstactModel> list = new ArrayList<>(0);
        Cursor cursor = mDbHelper.rawQuery("SELECT DISTINCT * from " + TABLE_NAME, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                list = new ArrayList<>(cursor.getCount());
                do {
                    list.add(new ConstactModel(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }


    public void deleteParticularContacts(String mobile_number) {
        if (isContactsNameExist(mobile_number))
            mDbHelper.rawDelete(TABLE_NAME, COLUMN_MOBILE_NUMBER + "='" + mobile_number + "'", null);
    }

    public boolean isContactsNameExist(String search_str) {
        return isValid("SELECT * FROM " + TABLE_NAME + " where " + COLUMN_MOBILE_NUMBER + " like '%" + search_str + "%'");
    }

    public boolean isValid(String query) {
        boolean check = false;
        Cursor cursor = mDbHelper.rawQuery(query, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                check = true;
            }
            cursor.close();
        }
        return check;
    }

    public ArrayList<ConstactModel> getContactsDetails() {
        ArrayList<ConstactModel> list = new ArrayList<>(0);
        Cursor cursor = null;
        try {
            cursor = mDbHelper.rawQuery("SELECT * FROM "
                    + TABLE_NAME +
                    " where " + COLUMN_ISCONTACT_UPLOAED + "='" + "false" + "'" + " limit 1000", null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    list = new ArrayList<>(cursor.getCount());
                    do {
                        list.add(new ConstactModel(cursor));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
        }
        return list;
    }

    public ArrayList<ConstactModel> getAllContactsNotUploaded() {
        ArrayList<ConstactModel> list = new ArrayList<>(0);
        Cursor cursor = null;
        try {
            cursor = mDbHelper.rawQuery("SELECT * FROM "
                    + TABLE_NAME +
                    " where " + COLUMN_ISCONTACT_UPLOAED + "='" + "false" + "'", null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    list = new ArrayList<>(cursor.getCount());
                    do {
                        list.add(new ConstactModel(cursor));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
        }
        return list;
    }
}
