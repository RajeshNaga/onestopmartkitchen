package com.mart.onestopkitchen.contacts;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mart.onestopkitchen.contacts.model.ConstactModel;
import com.mart.onestopkitchen.contacts.model.ResponseContact;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mart.onestopkitchen.service.PreferenceService.CURRENT_DATE_UPLOAD_CONTACT;
import static com.mart.onestopkitchen.service.PreferenceService.USER_MOBILE_NUMBER;
import static com.mart.onestopkitchen.service.PreferenceService.USER_MSID;
import static com.mart.onestopkitchen.service.PreferenceService.USER_SIM_ID;


public class UpdateAllContactsOnceSynced extends Service {

    public static boolean isRequestedSuccess(String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            String statusCode = jsonObject.getString("Code");
            String msg = jsonObject.getString("Msg");
            return statusCode.equalsIgnoreCase("200") || statusCode.equalsIgnoreCase("0");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        uploadAndVerifyData();
        return START_STICKY;
    }

    private void uploadAndVerifyData() {
        if (AppUtils.isReallyConnectedToInternet()) {
            ArrayList<ConstactModel> listOfContacts = ContactDb.getInstance(getApplicationContext()).getContactsDetails();
            if (listOfContacts.size() > 0) {
                // TODO : Next day call
                uploadContacts(listOfContacts);

            } else {
                stopSelf();
            }
        } else {
            stopSelf();
        }
    }

    private void uploadContacts(final ArrayList<ConstactModel> contactsServices) {
        Api service = RetroClient.getApi();
        Call<ResponseContact> jsonObjectCall = service.MultiUpdateContacts(makeDataForContacts(contactsServices));
        jsonObjectCall.enqueue(new Callback<ResponseContact>() {
            @Override
            public void onResponse(Call<ResponseContact> call, Response<ResponseContact> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getStatusCode() == 200) {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
                        String formattedDate = df.format(c.getTime());
                        PreferenceService.getInstance().SetPreferenceValue(CURRENT_DATE_UPLOAD_CONTACT, formattedDate);
                        new UpdateDataBaseContacts(contactsServices).execute();
                    } else {
                        stopSelf();
                    }
                } else stopSelf();
            }

            @Override
            public void onFailure(Call<ResponseContact> call, Throwable t) {
                stopSelf();
            }
        });
    }

    private JsonObject makeDataForContacts(ArrayList<ConstactModel> pojo) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("MobileNumber", PreferenceService.getInstance().GetPreferenceValue(USER_MOBILE_NUMBER));//.getPrefsHelper().getPref(AppPreference.getPrefsHelper().MOBILE_NO, ""));
        jsonObject.addProperty("Simid", PreferenceService.getInstance().GetPreferenceValue(USER_SIM_ID));
        jsonObject.addProperty("Msid", PreferenceService.getInstance().GetPreferenceValue(USER_MSID));
        jsonObject.addProperty("Ostype", 0);
        jsonObject.addProperty("Otp", PreferenceService.getInstance().GetPreferenceValue(PreferenceService.OTP_MESSAGE));
        Gson gson = new GsonBuilder().create();
        JsonArray jsonArray = gson.toJsonTree(pojo).getAsJsonArray();
        JsonObject mainJson = new JsonObject();
        mainJson.addProperty("AgentNumber", PreferenceService.getInstance().GetPreferenceValue(USER_MOBILE_NUMBER));
        mainJson.add("Login", jsonObject);
        mainJson.add("ContactDetailses", jsonArray);
        return mainJson;
    }

    private void updateData(ConstactModel contactModel) {
        ContentValues values = new ContentValues();
        values.put(OneStopKitchenListener.Contacts._ID, contactModel.get_id());
        values.put(OneStopKitchenListener.Contacts.COLUMN_NAME, contactModel.getName());
        values.put(OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID, contactModel.getEmail());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ID, contactModel.getId());
        values.put(OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL, contactModel.getPhotoUrl());
        values.put(OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER, contactModel.getMobileNo());
        values.put(OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED, contactModel.getIsContactUpload());
        values.put(OneStopKitchenListener.Contacts.COLUMN_VCFURI, contactModel.getVcfUri());
        values.put(OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY, contactModel.getIsVerified());

        getContentResolver().update(OneStopKitchenListener.Contacts.CONTENT_URI, values,
                OneStopKitchenListener.Contacts._ID + " = '"
                        + values.get(OneStopKitchenListener.Contacts._ID) + "'", null);
    }

    @SuppressLint("StaticFieldLeak")
    class UpdateDataBaseContacts extends AsyncTask<Void, Void, Boolean> {

        ArrayList<ConstactModel> iconLists;

        UpdateDataBaseContacts(ArrayList<ConstactModel> iconLists) {
            this.iconLists = iconLists;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            try {
                for (ConstactModel model : iconLists) {
                    model.setIsContactUpload("true");
                    updateData(model);
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            stopSelf();
        }
    }
}