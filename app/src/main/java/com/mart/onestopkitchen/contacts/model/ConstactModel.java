package com.mart.onestopkitchen.contacts.model;

import android.database.Cursor;

import com.mart.onestopkitchen.data.sqldb.DBHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;


public class ConstactModel implements Serializable {

    String Name = "";
    String PhoneNumber = "";
    String EmailID = "";
    String PhotoUri = "";
    String IsOkDollarAcc = "";
    String isContactUpload = "";
    String VcardUri = "";

    String isVerified = "false";
    boolean expand = false;
    int _id = 0;
    ArrayList<ConstactModel> mContactModel = new ArrayList<ConstactModel>();
    private int type = 0;

    public ConstactModel(String name, String moNo, String Email) {
        setName(name);
        setMobileNo(moNo);
        setEmail(Email);
    }
    public ConstactModel() {

    }

    public ConstactModel(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("Name"))
            setName(jsonObject.getString("Name"));
        if (jsonObject.has("PhoneNumber"))
            setMobileNo(jsonObject.getString("PhoneNumber"));
        if (jsonObject.has("EmailID"))
            setEmail(jsonObject.getString("EmailID"));
    }

    public ConstactModel(Cursor c) {
        if (checkColumnExist(DBHelper.COLUMN_PRIMARY_KEY_ID, c))
            set_id(c.getInt(c.getColumnIndex(DBHelper.COLUMN_PRIMARY_KEY_ID)));
        if (checkColumnExist(DBHelper.KEY_NAME, c))
            setName(c.getString(c.getColumnIndex(DBHelper.KEY_NAME)));
        if (checkColumnExist(DBHelper.KEY_MOBILE_NUMBER, c))
            setMobileNo(c.getString(c.getColumnIndex(DBHelper.KEY_MOBILE_NUMBER)));
        if (checkColumnExist(DBHelper.KEY_EMAIL_ID, c))
            setEmail(c.getString(c.getColumnIndex(DBHelper.KEY_EMAIL_ID)));
        if (checkColumnExist(DBHelper.COLUMN_ID, c))
            setId(c.getString(c.getColumnIndex(DBHelper.COLUMN_ID)));
        if (checkColumnExist(DBHelper.COLUMN_PHOTO_URL, c))
            setPhotoUrl(c.getString(c.getColumnIndex(DBHelper.COLUMN_PHOTO_URL)));
        if (checkColumnExist(DBHelper.KEY_ISCONTACT_UPLOAED, c))
            setIsContactUpload(c.getString(c.getColumnIndex(DBHelper.KEY_ISCONTACT_UPLOAED)));
        if (checkColumnExist(DBHelper.KEY_VCFURI, c))
            setVcfUri(c.getString(c.getColumnIndex(DBHelper.KEY_VCFURI)));
        if (checkColumnExist(DBHelper.KEY_IS_VERIFY, c))
            setIsVerified(c.getString(c.getColumnIndex(DBHelper.KEY_IS_VERIFY)));
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public boolean isExpand() {
        return expand;
    }

    public void setExpand(boolean expand) {
        this.expand = expand;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getVcfUri() {
        return VcardUri;
    }

    public void setVcfUri(String vcfUri) {
        this.VcardUri = vcfUri;
    }

    public String getIsContactUpload() {
        return isContactUpload;
    }

    public void setIsContactUpload(String isContactUpload) {
        this.isContactUpload = isContactUpload;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getMobileNo() {
        return PhoneNumber;
    }

    public void setMobileNo(String mobileNo) {
        this.PhoneNumber = mobileNo;
    }

    public String getEmail() {
        return EmailID;
    }

    public void setEmail(String email) {
        this.EmailID = email;
    }

    public String getPhotoUrl() {
        return PhotoUri;
    }

    public void setPhotoUrl(String photoUrl) {
        this.PhotoUri = photoUrl;
    }

    public String getId() {
        return IsOkDollarAcc;
    }

    public void setId(String id) {
        this.IsOkDollarAcc = id;
    }


    public ArrayList<ConstactModel> getmTransationModel() {
        return mContactModel;
    }

    public void setmCantactModel(ConstactModel mContactModel) {
        this.mContactModel.add(mContactModel);
    }

    public boolean checkColumnExist(String columnName, Cursor cursor) {
        if (null != cursor)
            return cursor.getColumnIndex(columnName) >= 0;
        return false;
    }

}
