package com.mart.onestopkitchen.expandableLayout;

/**
 * Created by nidhin on 29/09/2018.
 */

public class Settings {
    static final int EXPAND_DURATION = 300;
    int expandDuration = EXPAND_DURATION;
    boolean expandWithParentScroll;
    boolean expandScrollTogether;
}
