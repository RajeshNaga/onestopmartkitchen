package com.mart.onestopkitchen.expandableLayout;

/**
 * Created by nidhin on 29/09/2018.
 */

class ExpandState {
    static final int PRE_INIT = -1;
    static final int CLOSED = 0;
    static final int EXPANDED = 1;
    static final int EXPANDING = 2;
    static final int CLOSING = 3;
}
