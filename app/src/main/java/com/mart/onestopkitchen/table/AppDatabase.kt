package com.mart.onestopkitchen.table

import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import android.content.Context
import com.mart.onestopkitchen.model.creditcarddao.CardDetailModel
import com.mart.onestopkitchen.model.creditcarddao.CreditCardDao
import com.mart.onestopkitchen.utils.Keys

/**
 * Created by Akash Garg on 18/01/2019.
 */

@Database(entities = [CardDetailModel::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cardDetails(): CreditCardDao

    companion object {
        private var Instance: AppDatabase? = null
        private const val DB_NAME = Keys.DATABASE_NAME

        fun getInstance(context: Context): AppDatabase =
                Instance ?: synchronized(AppDatabase::class) {
                    Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DB_NAME).allowMainThreadQueries().build()
                }.apply {
                    Instance = this
                }

        val mygration1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE cardDetailTable_NEW AS SELECT cardHolderName, cardNumber,cardCvv,cardExpMonth,cardExpYear from cardDetailTable")
                database.execSQL("DROP TABLE cardDetailTable")
                database.execSQL("ALTER TABLE cardDetailTable_NEW  RENAME TO cardDetailTable")
//                database.execSQL("ALTER TABLE cardDetailTable ADD COLUMN userId 'INTEGER' PRIMARY KEY AUTOINCREMENT DEFAULT NULL")
            }

        }

        val mygration2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE cardDetailTable_NEW AS SELECT cardHolderName, cardNumber,cardCvv,cardExpMonth,cardExpYear from cardDetailTable")
                database.execSQL("DROP TABLE cardDetailTable")
                database.execSQL("ALTER TABLE cardDetailTable_NEW  RENAME TO cardDetailTable")
//                database.execSQL("ALTER TABLE cardDetailTable ADD COLUMN userId 'INTEGER' PRIMARY KEY AUTOINCREMENT DEFAULT NULL")
                database.execSQL("INSERT INTO cardDetailTable (cardHolderName) values ('')")
            }
        }
    }

    fun destroyInstance() {
        Instance = null
    }


}
