package com.mart.onestopkitchen.mvp.ui.cashin;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.cashin.cashinresponsemodel.ContentItem;
import com.mart.onestopkitchen.mvp.ui.cashin.map.NearByAgentMapActivity;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.fragment.BaseFragment;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.callback.CurrentLocationCallback;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NearByOk$ServiceFragment extends BaseFragment implements NearByListener, FragmentListener, CurrentLocationCallback, AgentListAdapter.OnViewClicked {
    public final static String[] MULTI_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    @BindView(R.id.rel_agent_list)
    RecyclerView relAgentList;

    @BindView(R.id.fab_btn)
    FloatingActionButton fabBtn;

    @BindView(R.id.txtNoRecord)
    TextView txtNoRecord;
    // @BindView(R.id.fragment_navigation_drawer)

    @BindView(R.id.drawer_filter)
    DrawerLayout drawerFilter;
    private Unbinder unbinder;
    private NearByPresenter nearByPresenter;
    private double latitude = 0.0, longitude = 0.0, amount = 0.0;
    private AgentListAdapter agentListAdapter;
    private MainActivity mainActivity;
    private boolean isFirstTime;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cash_in, container, false);
        unbinder = ButterKnife.bind(this, view);
        txtNoRecord.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getActivity().getResources().getString(R.string.near_by_service));
        initUi();
    }

    private void initUi() {
        mainActivity = (MainActivity) getActivity();
        Objects.requireNonNull(mainActivity).getMapper().disbleTracking(mainActivity);
        checkPermission();
        nearByPresenter = new NearByPresenter(this);
        intRv();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !AppUtils.chekcPermission(MULTI_PERMISSIONS, getActivity())) {
            if (AppUtils.shouldShowPermissionRationale(MULTI_PERMISSIONS, getActivity())) {
                if (isAdded())
                    Objects.requireNonNull(getActivity()).onBackPressed();
                //AppUtils.goToSettings(getActivity());
            } else
                ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), MULTI_PERMISSIONS, 100);
        } else {
            locationEnable();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100 || requestCode == 501) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                /*nearByPresenter.apiCall(1);*/
                locationEnable();
            } else {
                if (AppUtils.verifyPermissions(grantResults)) {
                    locationEnable();
                } else checkPermission();
            }
        }
    }

    private void locationEnable() {
        fetchingLocations();
    }

    private void fetchingLocations() {
        mainActivity.getMapper().enableLocationTracking(this);
        showLoading();
    }

    private void intRv() {
        agentListAdapter = new AgentListAdapter(getContext(), this);
        relAgentList.setLayoutManager(new LinearLayoutManager(getContext()));
        relAgentList.setAdapter(agentListAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public double amount() {
        // return amount;
        return 1000;
    }

    @Override
    public double latitude() {
        return latitude;
    }

    @Override
    public double longitude() {
        return longitude;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void successFromServer(final List<ContentItem> content) {
        Instance.listener = NearByOk$ServiceFragment.this;
        hideLoading();
        if (content != null && content.size() > 0) {
            txtNoRecord.setVisibility(View.GONE);
            fabBtn.setVisibility(View.GONE);
//            drawerFilter.openDrawer(GravityCompat.START);
            fabBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (content.size() > 0)
                        drawerFilter.openDrawer(GravityCompat.END);
                }
            });
            agentListAdapter.loadData(content);
        } else noRecordFound();
    }

    @SuppressLint("RestrictedApi")
    private void noRecordFound() {
        drawerFilter.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        txtNoRecord.setVisibility(View.VISIBLE);
        fabBtn.setVisibility(View.GONE);
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showLoading() {
        AppDialogs.loadingDialog(getContext(), true, "");
    }

    @Override
    public void hideLoading() {
        AppDialogs.dismissDialog();
    }

    @Override
    public void showError(String s) {
        hideLoading();
        mainActivity.showNormalToast(s);
    }

    @OnClick(R.id.fab_btn)
    public void onViewClicked() {

    }

    @Override
    public void apiCall(boolean fullService, boolean opened, boolean recentClose, boolean noFilter) {
        drawerFilter.closeDrawers();
        showLoading();
        nearByPresenter.filterApi(fullService, opened, recentClose, noFilter);
    }

    @Override
    public void currentLocation(@NotNull Location mLocation, @org.jetbrains.annotations.Nullable Location oldLocation, double mDistanceFromFirstLocation) {
        if (!isFirstTime) {
            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();
            isFirstTime = true;
            nearByPresenter.apiCall(1);
        }
    }

    @Override
    public void onViewClicked(ContentItem contentItem) {
        contentItem.setUserLatitude(latitude);
        contentItem.setUserLongitude(longitude);
        Intent intent = new Intent(getContext(), NearByAgentMapActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", contentItem);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void onBackPressed() {
        if (drawerFilter.isDrawerOpen(Gravity.RIGHT)) {
            drawerFilter.closeDrawers();
        } else Objects.requireNonNull(super.getActivity()).onBackPressed();
    }


    public static class Instance {
        public static FragmentListener listener;
    }
}
