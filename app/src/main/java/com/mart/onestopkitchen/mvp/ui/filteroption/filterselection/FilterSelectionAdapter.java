package com.mart.onestopkitchen.mvp.ui.filteroption.filterselection;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.filter.filternew.FilterOptionItemNew;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterSelectionAdapter extends RecyclerView.Adapter<FilterSelectionAdapter.ViewHolder> {


    private OnViewClickedListener onViewClickedListener;
    private Context mContext;
    private List<FilterOptionItemNew> dataItem;

    public FilterSelectionAdapter(OnViewClickedListener onViewClickedListener, Context context, List<FilterOptionItemNew> dataItem) {
        this.onViewClickedListener = onViewClickedListener;
        this.mContext = context;
        this.dataItem = dataItem;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_filters_selections, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FilterOptionItemNew filterOptionItem = dataItem.get(position);
        holder.onItemBind(filterOptionItem);
        holder.cheSelectedItem.setChecked(filterOptionItem.isSelectedItem());
        onViewClickedListener.onViewClicked(filterOptionItem, position, filterOptionItem.isSelectedItem());
        holder.txtTitle.setText(filterOptionItem.getItemSubName());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean status = false;
                if (holder.cheSelectedItem.isChecked()) {
                    status = false;
                    holder.cheSelectedItem.setChecked(false);
                } else {
                    holder.cheSelectedItem.setChecked(true);
                    status = true;
                }
                holder.cheSelectedItem.setChecked(status);
            }
        });
        holder.cheSelectedItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                onViewClickedListener.onViewClicked(filterOptionItem, position, b);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataItem != null ? dataItem.size() : 0;
    }

    public void loadItem(List<FilterOptionItemNew> filterOption) {
        this.dataItem = filterOption;
        notifyDataSetChanged();
    }

    public interface OnViewClickedListener {
        void onViewClicked(FilterOptionItemNew filterOptionItem, int pos, boolean b);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.che_selected_item)
        CheckBox cheSelectedItem;

        @BindView(R.id.rel_root)
        RelativeLayout relativeLayout;

        private FilterOptionItemNew filterOptionItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void onItemBind(FilterOptionItemNew filterOptionItem) {
            this.filterOptionItem = filterOptionItem;
        }
    }
}
