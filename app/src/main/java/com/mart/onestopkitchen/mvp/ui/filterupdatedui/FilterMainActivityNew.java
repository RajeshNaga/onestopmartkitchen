package com.mart.onestopkitchen.mvp.ui.filterupdatedui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.filter.filternew.DataItemFilterMain;
import com.mart.onestopkitchen.mvp.ui.filteroption.FilterMainActivityListener;
import com.mart.onestopkitchen.mvp.ui.filteroption.FilterMainActivityPresenter;
import com.mart.onestopkitchen.mvp.ui.filteroption.filter.FilterAdapter;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.fragments.FilterFragmentUpdate;
import com.mart.onestopkitchen.networking.response.ProductsResponse;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FilterMainActivityNew extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener, FragmentListenerNew, FilterMainActivityListener {

    public DataItemFilterMain dateSelectedItem;
    @BindView(R.id.tool_filter)
    Toolbar toolFilter;
    @BindView(R.id.txt_toolbar)
    TextView txtToolbar;
    @BindView(R.id.frame_filter)
    FrameLayout frameFilter;
    private String[] arrayLength;
    private FilterAdapter filterAdapter;
    private Context mContext;
    private int categoryId = 0;
    private FilterMainActivityPresenter presenter;
    private ProductsResponse productsResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_filter_updated);
        ButterKnife.bind(this);
        initUi();
    }

    private void initUi() {
        Intent intent = getIntent();
        if (intent != null) {
            productsResponse = (ProductsResponse) Objects.requireNonNull(intent.getExtras()).getSerializable("categoryId");
        }
        presenter = new FilterMainActivityPresenter(this);
        //presenter.apiCall(1);
        initToolbar();
        initFragment();

    }

    private void initToolbar() {
        setSupportActionBar(toolFilter);
        txtToolbar.setText(getString(R.string.filter));
        toolFilter.setNavigationIcon(this.getResources().getDrawable(R.drawable.ic_action_nav_arrow_back));
        toolFilter.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public ProductsResponse getDetails() {
        return productsResponse;
    }

    private void initFragment() {
        FilterFragmentUpdate filterFragment = new FilterFragmentUpdate();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_filter, filterFragment, filterFragment.getClass().getName())
                .addToBackStack(filterFragment.getClass().getName())
                .commitAllowingStateLoss();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        // AppUtils.changeFont(this);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_filter);
        if (fragment instanceof FilterFragmentUpdate) {
            ((FilterFragmentUpdate) fragment).applyAction();
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_filter);
        if (fragment != null)
            fragment.onResume();
    }

    @Override
    public void setTitle(CharSequence title) {
        txtToolbar.setText(title);
    }

    @Override
    public void nextFragment(Fragment fragment, boolean addToBackStack, Integer... animations) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment available = fragmentManager.findFragmentByTag(fragment.getClass().getName());
        if (available != null) {
            int stackPosition = isFragmentInBackStack(fragmentManager, fragment.getClass().getName());
            if (stackPosition != -1 && fragmentManager.getBackStackEntryCount() > stackPosition) {
                FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(stackPosition);
                fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } else {
            if (animations.length == 2)
                fragmentTransaction.setCustomAnimations(animations[0], animations[1]);
            else if (animations.length == 4)
                fragmentTransaction.setCustomAnimations(animations[0], animations[1], animations[2], animations[3]);
            fragmentTransaction
                    .add(R.id.frame_filter, fragment, fragment.getClass().getName());
            if (addToBackStack)
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    private int isFragmentInBackStack(FragmentManager fragmentManager, String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount() - 1; entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return entry + 1;
            }
        }
        return -1;
    }

    public void hideToolBar() {
        toolFilter.setNavigationIcon(null);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String s) {

    }

    @Override
    public int getCategoryId() {
        return categoryId;
    }

    public void setCureentItem(DataItemFilterMain dataItem) {
        this.dateSelectedItem = dataItem;
    }
}
