package com.mart.onestopkitchen.mvp.ui.filteroption.filter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.filter.filternew.DataItemFilterMain;
import com.mart.onestopkitchen.model.filter.filternew.FilterOptionItemNew;
import com.mart.onestopkitchen.model.filter.selected.FilterItem;
import com.mart.onestopkitchen.model.filter.selected.ItemsItem;
import com.mart.onestopkitchen.model.filter.selected.SelectedModel;
import com.mart.onestopkitchen.mvp.ui.filteroption.FilterMainActivity;
import com.mart.onestopkitchen.mvp.ui.filteroption.FragmentListener;
import com.mart.onestopkitchen.mvp.ui.filteroption.filterselection.FilterSelectionFragment;
import com.mart.onestopkitchen.networking.response.ProductsResponse;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.mart.onestopkitchen.utils.AppConstants.FILTER_OPTION_INTENT;


/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends Fragment implements FilterAdapter.FilterViewClick {


    @BindView(R.id.range_seek_bar)
    RangeSeekBar rangeSeekBar;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;

    @BindView(R.id.lin_cate_filter)
    LinearLayout linCateFilter;

    @BindView(R.id.txt_min_price)
    TextView txtMinPrice;
    @BindView(R.id.txt_max_price)
    TextView txtMaxPrice;
    @BindView(R.id.lin_seek_bar)
    LinearLayout linSeekBar;
    @BindView(R.id.btn_addtoCart)
    Button btnAddtoCart;
    @BindView(R.id.btn_buy_now)
    Button btnBuyNow;
    @BindView(R.id.view_lin)
    View viewLin;
    @BindView(R.id.addtoCartLayout)
    LinearLayout addtoCartLayout;
    @BindView(R.id.fixedLayout)
    CardView fixedLayout;
    Unbinder unbinder;
    private String[] arrayLength;
    private FilterAdapter filterAdapter;
    private Context mContext;
    private FragmentListener mListener;
    private int currentPos;
    private int categoryId = 0;
    private ProductsResponse productDetailResponse;
    private FilterMainActivity filterMainActivity;
    private Double minPrice = 0.0, maxPrice = 0.0;
    private DataItemFilterMain currentFilter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filters, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setTitle(getString(R.string.filter));
        // Bundle bundle = this.getArguments();
        /*if (bundle != null) {
            DataItem dataItems = (DataItem) bundle.getSerializable("selectedItem");
            filterAdapter.changeData(dataItems,currentPos);
        }*/
        if (filterMainActivity.dateSelectedItem != null)
            filterAdapter.changeData(filterMainActivity.dateSelectedItem, currentPos);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        filterMainActivity = (FilterMainActivity) getActivity();
        productDetailResponse = filterMainActivity.getDetails();
        initRv();
        uiListener();
        setSeekBar();
    }

    private void initRv() {
        filterAdapter = new FilterAdapter(this, mContext);
        rvCategories.setLayoutManager(new LinearLayoutManager(mContext));
        rvCategories.setAdapter(filterAdapter);
        loadData();
    }

    private void loadData() {
        /// filterAdapter.loadData(getDate());
        filterAdapter.loadDataToAdapter(getDateFilterData());
    }

    private List<DataItemFilterMain> getDateFilterData() {
        List<FilterOptionItemNew> dataItems = new ArrayList<>(0);
        LinkedHashSet<String> getSpecificationAttributeName = new LinkedHashSet<String>();
        List<DataItemFilterMain> mainList = new ArrayList<>(0);
        if (productDetailResponse.getNotFilteredItems() != null && productDetailResponse.getNotFilteredItems().size() > 0) {
            linCateFilter.setVisibility(View.VISIBLE);
            for (com.mart.onestopkitchen.model.FilterItem itemNew : productDetailResponse.getNotFilteredItems()) {
                getSpecificationAttributeName.add(itemNew.getSpecificationAttributeName());
            }
            for (String s : getSpecificationAttributeName) {
                DataItemFilterMain filterMain = new DataItemFilterMain();
                filterMain.setItemName(s);
                List<FilterOptionItemNew> itemNew1 = new ArrayList<>();
                for (com.mart.onestopkitchen.model.FilterItem itemNew : productDetailResponse.getNotFilteredItems()) {
                    if (s.equalsIgnoreCase(itemNew.getSpecificationAttributeName())) {
                        FilterOptionItemNew filterOptionItemNew = new FilterOptionItemNew();
                        filterOptionItemNew.setItemSubName(itemNew.getSpecificationAttributeOptionName());
                        filterOptionItemNew.setFilterId(itemNew.getFilterId());
                        filterOptionItemNew.setProductId(itemNew.getProductId());
                        itemNew1.add(filterOptionItemNew);
                        filterMain.setFilterOption(itemNew1);
                    }
                }
                mainList.add(filterMain);
            }
        } else {
            linCateFilter.setVisibility(View.GONE);
        }
        return mainList;
    }


    private void uiListener() {
        btnAddtoCart.setText(getString(R.string.reset));
        btnBuyNow.setText(getString(R.string.apply));
    }

    private void setSeekBar() {


        if (productDetailResponse != null && productDetailResponse.getPriceRange() != null) {
            minPrice = productDetailResponse.getPriceRange().getFrom();
            maxPrice = productDetailResponse.getPriceRange().getTo();
            setTextValues(100, 0);
            rangeSeekBar.setRange(0, 100);
            rangeSeekBar.setValue(0, 100);
        } else {
            linSeekBar.setVisibility(View.GONE);
        }
        //  if (productDetailResponse.getFilterItems() != null && productDetailResponse.getFilterItems().size() == 0) {
        //      viewLin.setVisibility(View.GONE);
        //  }
        rangeSeekBar.setIndicatorTextDecimalFormat("0");
        rangeSeekBar.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                setTextValues(rightValue, leftValue);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });
    }

    private void setTextValues(float rightValue, float leftValue) {
        try {
            int leftvalInt = Math.round(leftValue);
            int rightValInt = Math.round(rightValue);
            Double aDouble = Double.parseDouble(String.valueOf(leftValue));
            Double ri = Double.parseDouble(String.valueOf(rightValue));
            Double aDouble1 = 100 / ri;
            Double aDouble2 = maxPrice / aDouble1;
            Double right = 100 / aDouble;
            // Double rightSeek = maxPrice / right;
            double ans = maxPrice - minPrice;
            double aans = ans / 10;
            double mulvalues = leftvalInt / 10;
            double mulvalues2 = rightValInt / 10;
            Double rightSeek = minPrice + (aans * mulvalues);
            Double maxSeek = minPrice + (aans * mulvalues2);
            // if (rightSeek == 0.0)
            //     txtMinPrice.setText(cutNull(minPrice));
            // else

            txtMinPrice.setText(cutNull(AppUtils.getMMKString(txtMinPrice.getTextSize(), String.valueOf(rightSeek.intValue()), 0)));
            txtMaxPrice.setText(cutNull(AppUtils.getMMKString(txtMaxPrice.getTextSize(), String.valueOf(maxSeek.intValue()), 0)));

            /*   txtMinPrice.setText(cutNull("" + Integer.parseInt(String.valueOf(rightSeek.intValue()))));
            txtMaxPrice.setText(cutNull("" + Integer.parseInt(String.valueOf(maxSeek.intValue()))));
            txtMinPrice.append(" MMK");
            txtMaxPrice.append(" MMK");*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String[] getStringLength() {
        arrayLength = new String[10];
        for (int i = 0; i < 10; i++) {
            arrayLength[i] = "|";
        }
        return arrayLength;
    }

    @Override
    public void viewClickFilterItem(DataItemFilterMain model, int pos) {
        this.currentPos = pos;
        this.currentFilter = model;
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", model);
        FilterSelectionFragment filterSelectionFragment = new FilterSelectionFragment();
        filterSelectionFragment.setArguments(bundle);
        mListener.nextFragment(filterSelectionFragment);
    }

    private String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_addtoCart, R.id.btn_buy_now})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_addtoCart:
                resetAction();
                break;
            case R.id.btn_buy_now:
                applyAction();
                break;
        }
    }

    private void resetAction() {
        loadData();
        rangeSeekBar.setValue(0, 100);
        txtMinPrice.setText(cutNull(AppUtils.getMMKString(txtMinPrice.getTextSize(), String.valueOf(minPrice), 0)));
        txtMaxPrice.setText(cutNull(AppUtils.getMMKString(txtMaxPrice.getTextSize(), String.valueOf(maxPrice), 0)));
    }

    private void applyAction() {
        String price;

        try {
            Double minValueUserSelected = Double.parseDouble(txtMinPrice.getText().toString().replace(" MMK", "").replace(",", ""));
            Double maxValueUserSelected = Double.parseDouble(txtMaxPrice.getText().toString().replace(" MMK", "").replace(",", ""));
            // if (minPrice.equals(minValueUserSelected) && maxPrice.equals(maxValueUserSelected)) {
            //     price = "0-0";
            //   } else {
            price = minValueUserSelected + "-" + maxValueUserSelected;
            //    }
        } catch (Exception e) {
            e.printStackTrace();
            price = "0-0";
        }
        List<DataItemFilterMain> dataItems = filterAdapter.dataItemSlected;
        SelectedModel selectedModel = new SelectedModel();
        List<FilterItem> items = new ArrayList<>();
        if (dataItems != null && dataItems.size() > 0) {
            for (DataItemFilterMain item : dataItems) {
                FilterItem filterItem = new FilterItem();
                filterItem.setCategoryName(item.getItemName());
                if (item.getSelectedName().size() > 0) {
                    List<ItemsItem> itemsItems = new ArrayList<>(0);
                    for (int i = 0; i < item.getSelectedName().size(); i++) {
                        for (FilterOptionItemNew optionItem : item.getFilterOption()) {
                            if (item.getSelectedName().get(i).equalsIgnoreCase(optionItem.getItemSubName())) {
                                ItemsItem itemsItem = new ItemsItem();
                                itemsItem.setSubItme(optionItem.getItemSubName());
                                itemsItem.setId(optionItem.getFilterId());
                                itemsItem.setProductId(optionItem.getProductId());
                                itemsItems.add(itemsItem);
                            }
                        }
                    }
                    filterItem.setItems(itemsItems);
                    items.add(filterItem);
                }
            }
        }
        selectedModel.setFilter(items);
        Intent intent = new Intent();
        intent.putExtra("price", price);
        intent.putExtra("category", selectedModel);
        filterMainActivity.setResult(FILTER_OPTION_INTENT, intent);
        getActivity().finish();
    }


}
