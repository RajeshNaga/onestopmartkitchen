package com.mart.onestopkitchen.mvp.ui.filteroption.filterselection;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.filter.filternew.DataItemFilterMain;
import com.mart.onestopkitchen.model.filter.filternew.FilterOptionItemNew;
import com.mart.onestopkitchen.mvp.ui.filteroption.FilterMainActivity;
import com.mart.onestopkitchen.mvp.ui.filteroption.FragmentListener;
import com.mart.onestopkitchen.mvp.ui.filteroption.filter.FilterFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class FilterSelectionFragment extends Fragment implements FilterSelectionAdapter.OnViewClickedListener {


    Unbinder unbinder;
    @BindView(R.id.rv_filter_selection)
    RecyclerView rvFilterSelection;
    @BindView(R.id.btn_selection)
    Button btnSelection;
    private FragmentListener mListener;
    private DataItemFilterMain dataItem;
    private FilterMainActivity filterMainActivity;
    private FilterSelectionAdapter adapter;
    private Context mContext;
    private ArrayList<String> selectedItem = new ArrayList<>(0);
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataItem = (DataItemFilterMain) getArguments().getSerializable("item");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter_selection, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
        uiListener();
    }

    private void uiListener() {
        btnSelection.setText(getString(R.string.apply));
    }

    private void intListData() {
        adapter = new FilterSelectionAdapter(this, mContext, dataItem.getFilterOption());
        rvFilterSelection.setLayoutManager(new LinearLayoutManager(mContext));
        rvFilterSelection.setAdapter(adapter);
    }

    private void initUi() {
        mContext = getContext();
        filterMainActivity = (FilterMainActivity) getActivity();
        filterMainActivity.hideToolBar();
        intListData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setTitle(dataItem.getItemName());
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_filer_selection, menu);
        final View view = menu.findItem(R.id.menu_item_filter_clear).getActionView();
        textView = view.findViewById(R.id.ab_txt_filter);
        textView.setAlpha(0.5f);
        textView.setEnabled(false);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedItem.clear();
                for (int i = 0; i < dataItem.getFilterOption().size(); i++) {
                    dataItem.getFilterOption().get(i).setSelectedItem(false);
                }
                adapter.loadItem(dataItem.getFilterOption());
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            mListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_selection)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_selection:
                btnApplyAction();
                break;
        }
    }

    private void btnApplyAction() {
        dataItem.setSelectedName(selectedItem);
        filterMainActivity.setCureentItem(dataItem);
        FilterFragment filterFragment = new FilterFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("selectedItem", dataItem);
        filterFragment.setArguments(bundle);
        mListener.nextFragment(filterFragment);
    }

    @Override
    public void onViewClicked(FilterOptionItemNew filterOptionItem, int pos, boolean b) {
        if (b) {
            dataItem.getFilterOption().get(pos).setSelectedItem(true);
            selectedItem.add(filterOptionItem.getItemSubName());
        } else {
            dataItem.getFilterOption().get(pos).setSelectedItem(false);
            selectedItem.remove(filterOptionItem.getItemSubName());
        }

        if (selectedItem.size() > 0) {
            textView.setAlpha(1);
            textView.setEnabled(true);
        } else {
            textView.setAlpha(0.5f);
            textView.setEnabled(false);
        }

    }
}
