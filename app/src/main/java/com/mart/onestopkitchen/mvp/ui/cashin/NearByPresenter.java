package com.mart.onestopkitchen.mvp.ui.cashin;

import android.content.Context;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.baseview.ResponseHandler;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.CashInRequestModel;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.Location;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.filter.Filter;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.filter.FilterRequestModel;
import com.mart.onestopkitchen.model.cashin.cashinresponsemodel.NearbyResponseModel;
import com.mart.onestopkitchen.networking.ApiBroker;

import java.util.ArrayList;
import java.util.List;

public class NearByPresenter implements ResponseHandler {
    private NearByListener inListener;
    private Context mContext;
    private ApiBroker apiBroker;

    NearByPresenter(NearByListener inListener) {
        this.inListener = inListener;
        this.mContext = inListener.getActivityContext();
        this.apiBroker = new ApiBroker(this);
    }

    public void apiCall(int i) {
        //inListener.showLoading();
        switch (i) {
            case 1:
                apiBroker.nearByCashInList(i, getPostData());
                break;
        }
    }

    private CashInRequestModel getPostData() {
        CashInRequestModel cashInRequestModel = new CashInRequestModel();
        cashInRequestModel.setAmount(inListener.amount());
        List<Double> doubles = new ArrayList<>();
        doubles.add(inListener.longitude());
        doubles.add(inListener.latitude());
        Location location = new Location();
        location.setCoordinates(doubles);
        location.setLatitude(inListener.latitude());
        location.setLongitude(inListener.longitude());
        cashInRequestModel.setLocation(location);
        return cashInRequestModel;
    }

    @Override
    public void onSuccess(Object o, int api) {
        try {
            inListener.hideLoading();
            if (o != null) {
                switch (api) {
                    case 1:
                        cashList(o);
                        break;
                }
            } else inListener.showError(mContext.getString(R.string.some_thing_went));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cashList(Object o) {
        try {
            NearbyResponseModel nearbyResponse = (NearbyResponseModel) o;
            if (nearbyResponse != null && nearbyResponse.getStatusCode() == 200) {
                inListener.successFromServer(nearbyResponse.getContent());
            } else {
                inListener.showError(mContext.getString(R.string.please_try_again));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorBody(Object s, int api) {
        inListener.showError(mContext.getString(R.string.please_try_again));
    }

    @Override
    public void onFailure(String s) {
        inListener.showError(mContext.getString(R.string.please_try_again));
    }


    void filterApi(boolean fullService, boolean opened, boolean recentClose, boolean noFilter) {
        FilterRequestModel cashInRequestModel = new FilterRequestModel();
        cashInRequestModel.setAmount(inListener.amount());
        List<Double> doubles = new ArrayList<>();
        doubles.add(inListener.longitude());
        doubles.add(inListener.latitude());
        Location location = new Location();
        location.setCoordinates(doubles);
        location.setLatitude(inListener.latitude());
        location.setLongitude(inListener.longitude());
        cashInRequestModel.setLocation(location);
        Filter filter = new Filter();
        if (!noFilter) {
            filter.setIncludeMobileAgent(false);
            filter.setIncludeStationaryAgent(false);
            filter.setFullDayWorking(fullService);
            filter.setOpened(opened);
            filter.setRecentlyClosing(recentClose);
        } else filter = null;
        cashInRequestModel.setFilter(filter);
        apiBroker.nearByCashFilter(1, cashInRequestModel);
    }
}
