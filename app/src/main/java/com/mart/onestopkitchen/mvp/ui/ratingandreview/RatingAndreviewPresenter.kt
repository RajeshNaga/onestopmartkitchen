package com.mart.onestopkitchen.mvp.ui.ratingandreview

import android.annotation.SuppressLint
import android.content.Context
import com.mart.onestopkitchen.baseview.AbstractBasePresenter
import com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel
import com.mart.onestopkitchen.model.ratingreview.RatingGettingModel
import com.mart.onestopkitchen.model.ratingreview.RatingRequestModel
import com.mart.onestopkitchen.rxjava.RxJavaUtils


class RatingAndReviewPresenter(val mContext: Context) : AbstractBasePresenter<ReviewListener>() {
    private var reviewListener: ReviewListener? = null
    override fun setView(getView: ReviewListener) {
        super.setView(getView)
        this.reviewListener = getView
    }

    @SuppressLint("CheckResult")
    fun callReviewApi() {
        showLoading(true)
        apiHelper.getRatingAndReviewProduct(getProductId(), reviewListener!!.review()).compose(RxJavaUtils.applySchedulers()).subscribe({ t: RatingRequestModel? ->
            showLoading(false)
            reviewListener?.successFromServer(t)
        }, { throwable: Throwable -> exceptionHandler(throwable) })
    }

    @SuppressLint("CheckResult")
    fun getcallReviewApi() {
        showLoading(true)
        apiHelper.getRatingAndReviewOrder(getProductId()).compose(RxJavaUtils.applySchedulers()).subscribe({ rating: RatingGettingModel? ->
            showLoading(false)
            reviewListener?.getSuccessFromServer(rating)
        }, { throwable: Throwable -> exceptionHandler(throwable) })
    }

    private fun getProductId(): Int {
        return reviewListener!!.productId()
    }

    private fun showLoading(status: Boolean) {
        if (status)
            reviewListener?.showLoading()
        else reviewListener?.hideLoading()
    }

    @SuppressLint("CheckResult")
    fun callReviewApiNew() {
        showLoading(true)
        apiHelper.getRatingAndReviewByOrder(reviewListener!!.reviewByOrder()).compose(RxJavaUtils.applySchedulers()).subscribe({ model: AppRatingResponseModel? ->
            showLoading(false)
            reviewListener?.orderReviewAndRating(model)
        }, { throwable: Throwable -> exceptionHandler(throwable) })

    }
}