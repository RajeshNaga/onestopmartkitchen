package com.mart.onestopkitchen.mvp.ui.filterupdatedui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.FilterSubDataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoriesListAdapter extends RecyclerView.Adapter<CategoriesListAdapter.ViewHolder> {

    public List<FilterSubDataModel> productDetailResponse;
    public int selectedPos = -1;
    private OnSubClick onSubClick;
    private Context mContext;
    private String attributeName;
    private List<FilterSubDataModel> getGroupValues;

    public CategoriesListAdapter(Context context, OnSubClick onSubClick) {
        this.onSubClick = onSubClick;
        this.productDetailResponse = new ArrayList<>();
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.lite_item_categery_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final FilterSubDataModel dataModel = productDetailResponse.get(position);
        holder.onBindItem(dataModel);
        holder.txtSubTitle.setText(dataModel.getOptionName());
        if (dataModel.isSelected()) {
            holder.imgTick.setImageResource(R.drawable.ic_round_check_blue);
            holder.txtSubTitle.setTextColor(mContext.getResources().getColor(R.color.accent));
        } else {
            holder.imgTick.setImageResource(R.drawable.ic_round_check_gray);
            holder.txtSubTitle.setTextColor(mContext.getResources().getColor(R.color.defaultColor));
        }
    }

    @Override
    public int getItemCount() {
        return productDetailResponse == null ? 0 : productDetailResponse.size();
    }

    public void loadData(List<FilterSubDataModel> subDataModels, String specificationAttributeName) {
        this.attributeName = specificationAttributeName;
        this.productDetailResponse = subDataModels;
        notifyDataSetChanged();
    }

    public void loadDatas(List<FilterSubDataModel> getGroupValuess, String s) {
        this.attributeName = s;
        this.productDetailResponse = getGroupValuess;
        notifyDataSetChanged();
    }


    public interface OnSubClick {
        void onSubViewClicked(FilterSubDataModel filterSubDataModel, String attributeName, int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_tick)
        ImageView imgTick;
        @BindView(R.id.txt_sub_title)
        TextView txtSubTitle;
        @BindView(R.id.lin_full)
        LinearLayout linFull;
        private FilterSubDataModel subDataModel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.lin_full)
        public void onViewClicked() {
            onSubClick.onSubViewClicked(subDataModel, attributeName, getAdapterPosition());
            // selectedPos = getAdapterPosition();
            // notifyDataSetChanged();
        }

        public void onBindItem(FilterSubDataModel dataModel) {
            this.subDataModel = dataModel;
        }
    }
}
