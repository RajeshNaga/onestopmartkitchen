package com.mart.onestopkitchen.mvp.ui.filteroption;

import androidx.fragment.app.Fragment;

public interface FragmentListener {
    void setTitle(CharSequence title);

    void nextFragment(Fragment fragment);
}
