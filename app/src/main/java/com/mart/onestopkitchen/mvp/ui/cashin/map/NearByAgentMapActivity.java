package com.mart.onestopkitchen.mvp.ui.cashin.map;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.cashin.cashinresponsemodel.ContentItem;
import com.mart.onestopkitchen.ui.activity.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class NearByAgentMapActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.tool_filter_map)
    androidx.appcompat.widget.Toolbar toolFilterMap;
    private ContentItem intentProfile;
    private GoogleMap googleMap;
    private Marker markerAgentMobile;
    private Polyline polylineDriverToDest;
    GoogleDirectionsAPI.OnDirectionResult callbackDriverToDest = new GoogleDirectionsAPI.OnDirectionResult() {
        @Override
        public void onSuccess(GoogleDirectionsAPI.PlaceApiResult placeApiResult) {
            plotDriverToDestPath(placeApiResult.getEncodedPolyline());
        }

        @Override
        public void onFailure(Throwable exception) {

        }
    };
    private String TAG = NearByAgentMapActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_cash_in_map);
        ButterKnife.bind(this);
        iniUi();
    }

    private void iniUi() {
        Intent bundle = getIntent();
        intentProfile = (ContentItem) bundle.getSerializableExtra("data");
        try {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
        toolFilterMap.setNavigationIcon(this.getResources().getDrawable(R.drawable.ic_action_nav_arrow_back));
        toolFilterMap.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.googleMap.setMyLocationEnabled(true);

        initData();
    }

    private void initData() {
        try {
            if (googleMap == null) {
                showNormalToast(getString(R.string.please_update_play));
                finish();
                return;
            }
            if (intentProfile != null /*&& LocationService.location != null*/) {
                addMarker(getDestLocation());
                LatLng myLoc = new LatLng(intentProfile.getUserLatitude(), intentProfile.getUserLongitude());
                plotPath(myLoc, getDestLocation());
            } else {
                Log.e(TAG, "---------- OKDollar Agent Location Lat Lng Not Found ---------------");
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    private void plotPath(LatLng from, LatLng to) {
        if (from == null || to == null)
            return;
        GoogleDirectionsAPI googleDirectionsAPI = new GoogleDirectionsAPI(this, from, to, null, callbackDriverToDest);
        googleDirectionsAPI.requestAPI();
    }

    private void plotDriverToDestPath(String encodedPolyline) {
        final List<LatLng> list = PolylineEncoding.decode(encodedPolyline);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final PolylineOptions polylineOptions = buildPolyline(list);
                if (googleMap != null) {
                    if (polylineOptions != null) {
                        if (polylineDriverToDest != null)
                            polylineDriverToDest.remove();
                        polylineDriverToDest = googleMap.addPolyline(polylineOptions);
                    }
                }
            }
        });
    }

    private PolylineOptions buildPolyline(List<LatLng> list) {
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.addAll(list);
        polylineOptions.width(10);
        polylineOptions.geodesic(true);
        polylineOptions.color(getResources().getColor(R.color.polyline_color));
        return polylineOptions;
    }

    private void addMarker(LatLng agnt) {
        if (agnt == null)
            return;
        if (markerAgentMobile != null)
            markerAgentMobile.remove();
        MarkerOptions options = new MarkerOptions();
        options.position(agnt);
        options.draggable(false);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_black));
        if (markerAgentMobile == null)
            markerAgentMobile = googleMap.addMarker(options);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(agnt, 16));
    }

    private LatLng getDestLocation() {
        if (intentProfile == null)
            return null;
        if (intentProfile.getCurrentLocation() != null) {
            return new LatLng(intentProfile.getCurrentLocation().getLatitude(), intentProfile.getCurrentLocation().getLongitude());
        }
        if (intentProfile.getHomeLocation() != null) {
            return new LatLng(intentProfile.getHomeLocation().getLatitude(), intentProfile.getHomeLocation().getLongitude());
        }
        return null;
    }
}
