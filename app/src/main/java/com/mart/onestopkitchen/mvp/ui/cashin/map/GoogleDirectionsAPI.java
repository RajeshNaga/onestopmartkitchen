package com.mart.onestopkitchen.mvp.ui.cashin.map;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by NickX on 12-10-2016.
 */
public class GoogleDirectionsAPI {

    private static final String TAG = GoogleDirectionsAPI.class.getSimpleName() + " : ";
    /**
     * The constant distance.
     */
    public long distance = 0;
    /**
     * The constant duration.
     */
    public long duration = 0;
    /**
     * The Waypoints.
     */
    ArrayList<LatLng> waypoints;
    /**
     * The Source.
     */
    LatLng source = null;
    /**
     * The Destination.
     */
    LatLng destination = null;
    OnDirectionResult onDirectionResult;
    private Context context;

    /**
     * Instantiates a new Google place api.
     *
     * @param context     the context
     * @param source      the source
     * @param destination the destination
     * @param waypoints   the waypoints
     */
    public GoogleDirectionsAPI(Context context, LatLng source, LatLng destination, ArrayList<LatLng> waypoints, OnDirectionResult onDirectionResult) {
        this.context = context;
        this.waypoints = waypoints;
        this.source = source;
        this.destination = destination;
        this.onDirectionResult = onDirectionResult;
        distance = 0;
        duration = 0;
    }

    private String getMapsApiDirectionsUrl() {
        StringBuilder builder = new StringBuilder();
        if (waypoints != null && waypoints.size() > 0)
            for (LatLng latLng : waypoints) {
                builder.append("|");
                builder.append(latLng.latitude);
                builder.append(",");
                builder.append(latLng.longitude);
            }
        String endpoints = "origin=" + source.latitude + "," + source.longitude
                + "&destination=" + destination.latitude + "," + destination.longitude;
        String key = "&key=" + context.getString(R.string.google_map_api_key);
        String waypointstring = "";
        if (waypoints != null && waypoints.size() > 0)
            waypointstring = "waypoints=optimize:true" + builder.toString();
        String sensor = "sensor=true";
        String params = waypointstring + (!waypointstring.isEmpty() ? "&" : "") + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + endpoints + "&" + params + key;
        return url;
    }

    /**
     * Gets place data.
     */
    public void requestAPI() {
        new ReadTask().execute();
    }

    /**
     * Parse list.
     *
     * @param jObject the j object
     * @return the list
     */
    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;
        Integer[] waypointOrder;
        try {
            jRoutes = jObject.getJSONArray("routes");
            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {

                if (i > 0)
                    continue;

                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

                // The commented code for way point ordering
//                try {
//                    if (_wayPointChildList != null && _wayPointChildList.size()>0) {
//                        JSONArray array = ((JSONObject) jRoutes.get(i)).getJSONArray("waypoint_order");
//                        String str = array.getString(0);
//                        orderedList = new ArrayList<>();
//                        for (int k = 0; k < array.length(); k++) {
//                            orderedList.add(_wayPointChildList.get(array.getInt(k)));
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
                    try {
                        JSONObject distanceObject = ((JSONObject) jLegs.get(j)).getJSONObject("distance");
                        JSONObject durationObject = ((JSONObject) jLegs.get(j)).getJSONObject("duration");
                        distance += distanceObject.getLong("value");
                        duration += durationObject.getLong("value");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /** Traversing all steps */
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps
                                .get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);

                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat",
                                    Double.toString(list.get(l).latitude));
                            hm.put("lng",
                                    Double.toString(list.get(l).longitude));
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (onDirectionResult != null)
                onDirectionResult.onFailure(e);
        } catch (Exception e) {
            if (onDirectionResult != null)
                onDirectionResult.onFailure(e);
        }
        return routes;
    }

    /**
     * Method Courtesy :
     * jeffreysambells.com/2010/05/27
     * /decoding-polylines-from-google-maps-direction-api-with-java
     *
     * @param encoded the encoded
     * @return the list
     */
    public List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    public int[] stringToIntArray(String arr) {
        String[] items = arr.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");

        int[] results = new int[items.length];

        for (int i = 0; i < items.length; i++) {
            try {
                results[i] = Integer.parseInt(items[i]);
            } catch (NumberFormatException nfe) {
                //NOTE: write something here if you need to recover from formatting errors
            }
        }
        return results;
    }

    public interface OnDirectionResult {
        void onSuccess(PlaceApiResult placeApiResult);

        void onFailure(Throwable exception);
    }

    /**
     * The type Read task.
     */
    public class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(getMapsApiDirectionsUrl());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                routes = parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
                if (onDirectionResult != null)
                    onDirectionResult.onFailure(e);
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            if (routes == null) {
                if (onDirectionResult != null)
                    onDirectionResult.onFailure(new NullPointerException("Routes null..."));
                return;
            }

            // traversing through routes
//            List<LatLng> allPointsPoly = new ArrayList<>();
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(10);
                polyLineOptions.color(context.getResources().getColor(R.color.polyline_color));
            }


            if (polyLineOptions == null) {
                if (onDirectionResult != null)
                    onDirectionResult.onFailure(new NullPointerException("Polyline null..."));
                return;
            }

            PlaceApiResult placeApiResult = new PlaceApiResult();
            placeApiResult.setEncodedPolyline(PolylineEncoding.encode(polyLineOptions.getPoints()));
            placeApiResult.setDuration(duration);
            placeApiResult.setDistance(distance);
            placeApiResult.setSource(source);
            placeApiResult.setDestination(destination);

            if (onDirectionResult != null)
                onDirectionResult.onSuccess(placeApiResult);
        }
    }

    /**
     * The type Http connection.
     */
    public class HttpConnection {
        /**
         * Read url string.
         *
         * @param mapsApiDirectionsUrl the maps api directions url
         * @return the string
         * @throws IOException the io exception
         */
        public String readUrl(String mapsApiDirectionsUrl) throws IOException {
            String data = "";
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(mapsApiDirectionsUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                iStream = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        iStream));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                data = sb.toString();
                br.close();
            } catch (Exception e) {
                Log.d("Exception reading url", e.toString());
            } finally {
                if (iStream != null) {
                    iStream.close();
                }
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return data;
        }
    }

    public class PlaceApiResult {

        @SerializedName("EncodedPolyline")
        private String encodedPolyline;

        @SerializedName("Duration")
        private long duration;

        @SerializedName("Distance")
        private long distance;

        @SerializedName("Source")
        private LatLng source;

        @SerializedName("Destination")
        private LatLng destination;

        public String getEncodedPolyline() {
            return encodedPolyline;
        }

        public void setEncodedPolyline(String encodedPolyline) {
            this.encodedPolyline = encodedPolyline;
        }

        public long getDuration() {
            return duration;
        }

        public void setDuration(long duration) {
            this.duration = duration;
        }

        public long getDistance() {
            return distance;
        }

        public void setDistance(long distance) {
            this.distance = distance;
        }

        public LatLng getSource() {
            return source;
        }

        public void setSource(LatLng source) {
            this.source = source;
        }

        public LatLng getDestination() {
            return destination;
        }

        public void setDestination(LatLng destination) {
            this.destination = destination;
        }
    }
}
