package com.mart.onestopkitchen.mvp.ui.cashin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.cashin.cashinresponsemodel.ContentItem;
import com.mart.onestopkitchen.utils.AppUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AgentListAdapter extends RecyclerView.Adapter<AgentListAdapter.ViewHolder> {
    private List<ContentItem> item;
    private String phone_number = "";
    private Context context;
    private OnViewClicked onViewClicked;

    AgentListAdapter(Context cashInFragment, OnViewClicked onViewClicked) {
        item = new ArrayList<>(0);
        this.context = cashInFragment;
        this.onViewClicked = onViewClicked;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.c_item_agent_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        final ContentItem profile = item.get(i);
        holder.onItemBind(profile);
        StringBuilder stringBuilderName = new StringBuilder();
        if (profile.getBusinessName() != null && !profile.getBusinessName().isEmpty()) {
            stringBuilderName.append(profile.getBusinessName());
        } else {
            if (profile.getFirstName() != null && !profile.getFirstName().isEmpty()) {
                stringBuilderName.append(profile.getFirstName());
            } else {
                stringBuilderName.append("-------");
            }
        }
        holder.textName.setText(stringBuilderName.toString());
        stringBuilderName = new StringBuilder();
        if (profile.getPhoneNumber() != null && !profile.getPhoneNumber().isEmpty()) {
            phone_number = profile.getPhoneNumber();
            if (phone_number.startsWith("0095")) {
                phone_number = phone_number.replaceFirst("00", "+");
            }
            stringBuilderName.append(" ").append(phone_number);
        }

        holder.textPhone.setText(stringBuilderName.toString());

        String textAddress = "Address not found";
        StringBuilder stringBuilderAddress = new StringBuilder();
        if (profile.getLine1() != null && !profile.getLine1().isEmpty()) {
            stringBuilderAddress.append(profile.getLine1()).append(",");
        }
        if (profile.getLine2() != null && !profile.getLine2().isEmpty()) {
            stringBuilderAddress.append(profile.getLine2()).append(",");
        }
        if (profile.getCityName() != null && !profile.getCityName().isEmpty() && !profile.getCityName().equalsIgnoreCase("na")) {
            stringBuilderAddress.append(profile.getCityName()).append(",");
        }
        if (profile.getCountry() != null && !profile.getCountry().isEmpty()) {
            stringBuilderAddress.append(profile.getCountry());
        }

        if (stringBuilderAddress.toString().length() > 3)
            textAddress = stringBuilderAddress.toString();

        // holder.textAddress.setMyanmarText(textAddress);

        holder.textAddress.setText(textAddress);

        if (profile.getRating() != null) {
            holder.textRating.setText(String.valueOf(round(profile.getRating(), 2)));
        }

        if (profile.isIsOpen()) {
            // #747474
            holder.textOpenClose.setTextColor(Color.parseColor("#c14646"));
            try {
                DateTime dateTime = DateTime.parse(profile.getClosingTime());
                dateTime = dateTime.withZone(DateTimeZone.getDefault());
                DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");
                holder.textOpenClose.setText(String.format(Locale.ENGLISH, context.getString(R.string.label_open_until), formatter.print(dateTime)));
            } catch (Exception e) {
                e.printStackTrace();
                holder.textOpenClose.setText(String.format(Locale.ENGLISH, context.getString(R.string.label_open_until), profile.getClosingTime()));
            }
        } else if (profile.isIsOpenAlways()) {
            holder.textOpenClose.setTextColor(Color.parseColor("#c14646"));
            try {
//                DateTime dateTime = DateTime.parse(profile.getClosingTime());
//                dateTime = dateTime.withZone(DateTimeZone.getDefault());
//                DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");
//                holder.textOpenClose.setText(String.format(Locale.ENGLISH, context.getString(R.string.label_open_until), formatter.print(dateTime)));
                holder.textOpenClose.setText(context.getString(R.string.open_always));
            } catch (Exception e) {
                e.printStackTrace();
//                holder.textOpenClose.setText(String.format(Locale.ENGLISH, context.getString(R.string.label_open_until), profile.getClosingTime()));
                holder.textOpenClose.setText(context.getString(R.string.open_always));
            }
        } else {
            holder.textOpenClose.setTextColor(Color.RED);
            holder.textOpenClose.setText(context.getString(R.string.label_closed));
        }

        holder.imageCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profile.getPhoneNumber() == null || profile.getCountryCode() == null)
                    return;
                String num = profile.getPhoneNumber();
                num = num.startsWith("0095") ? num.replace("0095", "0") : num;
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + num));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

      /*  if (LocationSettings.location != null && profile.getCurrentLocation() != null) {
            Location location = new Location(LocationService.location);
            location.setLatitude(LocationService.location.getLatitude());
            location.setLongitude(LocationService.location.getLatitude());

            LatLng latLng1 = new LatLng(LocationService.location.getLatitude(), LocationService.location.getLongitude());
            LatLng latLng2 = new LatLng(profile.getCurrentLocation().getLatitude(), profile.getCurrentLocation().getLongitude());

            Location locationAgent = new Location("agent");
            locationAgent.setLatitude(profile.getCurrentLocation().getLatitude());
            locationAgent.setLongitude(profile.getCurrentLocation().getLongitude());
            double dist = SphericalUtil.computeDistanceBetween(latLng1, latLng2);

            String timeTaken = (dist / (8.33 * 60)) >= 60 ? String.valueOf(Math.round(dist / (8.33 * 60 * 60))) + context.getString(R.string.hr)
                    : String.valueOf((int) (Math.ceil(dist / (8.33 * 60))) + " " + context.getString(R.string.min));

            holder.textTime.setText(timeTaken);
        } else {
            holder.textTime.setText("---");
        }*/

        if (profile.getType() == UserType.AGENT.ordinal() || profile.getType() == UserType.USER.ordinal()) {
            holder.imageMobileAgent.setVisibility(View.VISIBLE);
            if (profile.getAgentType() == AgentType.MOBILE.ordinal()) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) AppUtils.convertDpToPixel(59f, context), (int) AppUtils.convertDpToPixel(59f, context));
                holder.imageMobileAgent.setLayoutParams(layoutParams);
                GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(holder.imageMobileAgent);
                Glide.with(context).load(R.drawable.bike_animation)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageViewTarget);
            } else {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) AppUtils.convertDpToPixel(38f, context), (int) AppUtils.convertDpToPixel(38f, context));
                holder.imageMobileAgent.setLayoutParams(layoutParams);
                holder.imageMobileAgent.setImageResource(R.drawable.ic_agent_started);
            }
        } /*else if (profile.getType() == Constants.UserType.USER.ordinal()) {
            holder.imageMobileAgent.setVisibility(View.GONE);
        } */ else {
            holder.imageMobileAgent.setVisibility(View.GONE);
        }
    }

    public void loadData(List<ContentItem> items) {
        this.item = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return item == null ? 0 : item.size();
    }

    public enum UserType {
        SUPERADMIN, AGENT, USER, ADMIN, AdvMer, Dummy, Subscriber
    }

    public enum AgentType {
        MOBILE, STATIONARY
    }

    public interface OnViewClicked {
        void onViewClicked(ContentItem contentItem);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_name)
        TextView textName;
        @BindView(R.id.text_address)
        TextView textAddress;
        @BindView(R.id.text_status)
        TextView textOpenClose;
        @BindView(R.id.layout_info)
        LinearLayout layoutInfo;
        @BindView(R.id.image_mobile_agent)
        ImageView imageMobileAgent;
        @BindView(R.id.image_call)
        ImageView imageCall;
        @BindView(R.id.layout_actions)
        LinearLayout layoutActions;
        @BindView(R.id.layout_top)
        LinearLayout layoutTop;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.text_rating)
        TextView textRating;
        @BindView(R.id.text_time)
        TextView textTime;
        @BindView(R.id.text_min_fee)
        TextView textMinFee;
        @BindView(R.id.layout_footer)
        LinearLayout layoutFooter;
        @BindView(R.id.text_phone)
        TextView textPhone;
        private ContentItem contentItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /* @OnClick(R.id.card_total)
         public void onViewClicked() {
             onViewClicked.onViewClicked(contentItem);
         }
 */
        void onItemBind(ContentItem profile) {
            this.contentItem = profile;
        }
    }
}
