package com.mart.onestopkitchen.mvp.ui.filterupdatedui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.FilterNewUpDateModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    public int selectedPosition = -1;
    private List<FilterNewUpDateModel> productDetailResponse;
    private OnViewClicked onViewClicked;
    private Context mContext;

    public CategoriesAdapter(Context context, OnViewClicked viewClicked, List<FilterNewUpDateModel> filterNewUpDateModels) {
        this.productDetailResponse = filterNewUpDateModels;
        this.onViewClicked = viewClicked;
        this.mContext = context;
    }

    public void loadCateList(List<FilterNewUpDateModel> filterNewUpDateModels) {
        this.productDetailResponse = filterNewUpDateModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_list_new, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.onItemBind(productDetailResponse.get(position));
        holder.txtCate.setText(productDetailResponse.get(position).getSpecificationAttributeName());
        if (position == selectedPosition) {
            holder.imgArrow.setVisibility(View.VISIBLE);
            holder.txtCate.setTextColor(mContext.getResources().getColor(R.color.accent));

        } else {
            holder.txtCate.setTextColor(mContext.getResources().getColor(R.color.black));
            holder.imgArrow.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return productDetailResponse == null ? 0 : productDetailResponse.size();
    }


    public interface OnViewClicked {
        void onCateClicked(FilterNewUpDateModel productDetailResponse);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_cate)
        TextView txtCate;
        @BindView(R.id.img_arrow)
        ImageView imgArrow;
        private FilterNewUpDateModel item;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.txt_cate)
        public void onViewClicked() {
            onViewClicked.onCateClicked(item);
            selectedPosition = getAdapterPosition();
            notifyDataSetChanged();
        }

        public void onItemBind(FilterNewUpDateModel filterItem) {
            this.item = filterItem;
        }
    }
}
