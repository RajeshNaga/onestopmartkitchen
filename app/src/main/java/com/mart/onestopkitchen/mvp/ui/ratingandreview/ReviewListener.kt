package com.mart.onestopkitchen.mvp.ui.ratingandreview

import basicsetup.me.com.base.LoadBaseView
import com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel
import com.mart.onestopkitchen.model.appratingandreview.AppReviewAndRating
import com.mart.onestopkitchen.model.ratingreview.RatingGettingModel
import com.mart.onestopkitchen.model.ratingreview.RatingRequestModel
import com.mart.onestopkitchen.model.ratingreview.RatingReviewRequestModel

interface ReviewListener : LoadBaseView {
    fun successFromServer(t: RatingRequestModel?)
    fun getSuccessFromServer(rating: RatingGettingModel?)
    fun review(): RatingReviewRequestModel?
    fun productId(): Int
    fun reviewByOrder(): AppReviewAndRating?
    fun orderReviewAndRating(model: AppRatingResponseModel?)


}