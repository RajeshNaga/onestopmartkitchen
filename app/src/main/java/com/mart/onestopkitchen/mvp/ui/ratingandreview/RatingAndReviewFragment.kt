package com.mart.onestopkitchen.mvp.ui.ratingandreview


import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.application.MyApplication
import com.mart.onestopkitchen.baseview.AppErrorResponseModel
import com.mart.onestopkitchen.loginuser.DeviceInformation
import com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel
import com.mart.onestopkitchen.model.appratingandreview.AppReviewAndRating
import com.mart.onestopkitchen.model.ratingreview.RatingGettingModel
import com.mart.onestopkitchen.model.ratingreview.RatingRequestModel
import com.mart.onestopkitchen.model.ratingreview.RatingReviewRequestModel
import com.mart.onestopkitchen.ratingview.BaseRating
import com.mart.onestopkitchen.ratingview.SmileRating
import com.mart.onestopkitchen.service.PreferenceService
import com.mart.onestopkitchen.ui.activity.MainActivity
import com.mart.onestopkitchen.ui.fragment.BaseFragment
import com.mart.onestopkitchen.utils.AppUtils
import com.mart.onestopkitchen.utils.Language
import com.mart.onestopkitchen.utils.dialog.AppDialogs
import kotlinx.android.synthetic.main.fragment_rating_and_review.*
import java.util.*


@SuppressLint("ValidFragment")
class RatingAndReviewFragment(private val fromWhere: String, val orderId: Int?, val productId: Int, var orderIds: String?) : BaseFragment(), ReviewListener, SmileRating.OnSmileySelectionListener, View.OnClickListener {
    private lateinit var getRating: String
    private lateinit var ratingAndReviewPresenter: RatingAndReviewPresenter
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rating_and_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun uiListener() {
        ratingView.selectedSmile = BaseRating.GOOD
        ratingView.setOnSmileySelectionListener(this)
        btnContinue.setOnClickListener(this)
        setTextToView(getString(R.string.smile_rating_good), "4")
    }

    private fun initUi() {
        mainActivity = activity as MainActivity
        ratingAndReviewPresenter = RatingAndReviewPresenter(mainActivity)
        ratingAndReviewPresenter.setView(this)
        uiListener()
        ((context?.applicationContext) as MyApplication).fromWhere = fromWhere
        if (fromWhere.equals("UserReview", ignoreCase = true))
            ratingGetAPIAction()

        if (fromWhere == "UserReview") {
            mainActivity.setTitle(R.string.rate_our_product)
        } else {
            mainActivity.setTitle(R.string.rate_our_service)
        }
    }

    override fun reviewByOrder(): AppReviewAndRating? {
        val appReviewAndRating = AppReviewAndRating()
        appReviewAndRating.orderNo = AppUtils.cutNull(orderIds)
        resetLanguage(false)
        appReviewAndRating.rating = getRating.toInt()
        appReviewAndRating.reviewText = raEdtComments.text.toString()
        appReviewAndRating.reviewType = 1
        return appReviewAndRating
    }

    private fun resetLanguage(b: Boolean) {
        if (b)
            setEngLang(Language.ENGLISH)
        else
            setEngLang(preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE))
    }

    private fun setEngLang(engLang: String) {
        val myLocale = Locale(engLang)
        Locale.setDefault(myLocale)
        val config = Configuration(resources.configuration)
        config.setLocale(myLocale)
        resources.updateConfiguration(config, resources.displayMetrics)
    }

    override fun orderReviewAndRating(model: AppRatingResponseModel?) {
        model?.let {
            if (it.statusCode == 200) {
                Snackbar.make(raRelRoot, R.string.thanks_for_review, Snackbar.LENGTH_SHORT).show()
                moveToHomeFragment()
            } else if (it.statusCode == 404) {
                mainActivity.showNormalToast(getString(R.string.some_thing_went))
                moveToHomeFragment()
            }
        } ?: run {
            mainActivity.showNormalToast(getString(R.string.some_thing_went))
            moveToHomeFragment()
        }
    }

    override fun showServerError(msg: AppErrorResponseModel?) {
        msg?.message?.let {
            if (msg.message.contains("index") || msg.message.contains("invalid"))
                return
            mainActivity.showNormalToast(msg.message)
        }
    }

    override fun productId(): Int {
        return when {
            fromWhere.equals("UserReview", ignoreCase = true) -> productId
            else -> ((context?.applicationContext) as MyApplication).orderItems.let { it[0].productId }
        }
    }

    override fun review(): RatingReviewRequestModel {
        val review = RatingReviewRequestModel()
        review.rating = getRating
        review.reviewText = raEdtComments.text.toString()
        return review
    }

    override fun onClick(id: View?) {
        if (DeviceInformation().deviceInfo.checkInterNet) {
            when (id?.id) {
                R.id.btnContinue -> continueAction()
                R.id.btnSkip -> skipAction()
            }
        }
    }

    private fun skipAction() {
        moveToHomeFragment()
    }

    private fun continueAction() {
        if (validation())
            if (fromWhere.equals("paymentdone"))
                ratingAndReviewPresenter.callReviewApiNew()
            else
                ratingAndReviewPresenter.callReviewApi()
    }

    private fun ratingGetAPIAction() {
        ratingAndReviewPresenter.getcallReviewApi()
    }

    private fun validation(): Boolean {
        if (raEdtComments.text.toString().isEmpty()) {
            Toast.makeText(context, getString(R.string.enter_comments), Toast.LENGTH_SHORT).show()
            return false
        }
        if (txtReviewStatus.text.isNullOrEmpty()) {
            Toast.makeText(context, getString(R.string.select_rating), Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    override fun onSmileySelected(smiley: Int, reselected: Boolean) {
        when (smiley) {
            SmileRating.NONE -> setTextToView("", "0")
            SmileRating.TERRIBLE -> setTextToView(getString(R.string.smile_rating_terrible), "1")
            SmileRating.BAD -> setTextToView(getString(R.string.smile_rating_bad), "2")
            SmileRating.OKAY -> setTextToView(getString(R.string.smile_rating_okay), "3")
            SmileRating.GOOD -> setTextToView(getString(R.string.smile_rating_good), "4")
            SmileRating.GREAT -> setTextToView(getString(R.string.smile_rating_great), "5")
        }
    }

    private fun setTextToView(string: String, str: String) {
        getRating = str
        txtReviewStatus.text = string
    }

    override fun getSuccessFromServer(rating: RatingGettingModel?) {
        if (rating != null && rating.statusCode == 200) {
            rating.Items?.get(0)?.title
            when {
                rating.Items?.get(0)?.rating.equals("1") -> {
                    ratingView.selectedSmile = BaseRating.TERRIBLE
                    setTextToView(getString(R.string.smile_rating_terrible), "1")
                }
                rating.Items?.get(0)?.rating.equals("2") -> {
                    ratingView.selectedSmile = BaseRating.BAD
                    setTextToView(getString(R.string.smile_rating_bad), "2")
                }
                rating.Items?.get(0)?.rating.equals("3") -> {
                    ratingView.selectedSmile = BaseRating.OKAY
                    setTextToView(getString(R.string.smile_rating_okay), "3")
                }
                rating.Items?.get(0)?.rating.equals("4") -> {
                    ratingView.selectedSmile = BaseRating.GOOD
                    setTextToView(getString(R.string.smile_rating_good), "4")
                }
                rating.Items?.get(0)?.rating.equals("5") -> {
                    ratingView.selectedSmile = BaseRating.GREAT
                    setTextToView(getString(R.string.smile_rating_great), "5")
                }
            }
            raEdtComments.setText(rating.Items?.get(0)?.reviewText)
        } else mainActivity.showNormalToast(getString(R.string.please_try_again))
    }

    @SuppressLint("LongLogTag")
    override fun successFromServer(t: RatingRequestModel?) {
        if (t != null && t.statusCode == 200) {
            Snackbar.make(raRelRoot, R.string.thanks_for_review, Snackbar.LENGTH_SHORT).show()
            if (fromWhere.equals("UserReview", ignoreCase = true)) {
                mainActivity.onBackPressed()
            } else {
                moveToHomeFragment()
            }

        } else
            mainActivity.showNormalToast(getString(R.string.please_try_again))
    }

    override fun showLoading() {
        AppDialogs.loadingDialog(activity, false, "")
    }

    override fun hideLoading() {
        AppDialogs.dismissDialog()
    }

    override fun showError(msg: String) {
        if (msg.toLowerCase().contains("index") || msg.toLowerCase().contains("invalid"))
            return
        mainActivity.showNormalToast(msg)
    }


    override fun getActContext(): Context {
        return context!!
    }

    override fun onDestroy() {
        ratingAndReviewPresenter.destroyView()
        super.onDestroy()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.menu_cart)?.isVisible = false
        menu.findItem(R.id.menu_search)?.isVisible = false
        menu.findItem(R.id.menu_invoice_share)?.isVisible = false

    }
}
