package com.mart.onestopkitchen.mvp.ui.filteroption.filter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.filter.filternew.DataItemFilterMain;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {

    List<DataItemFilterMain> dataItemSlected;
    private Context context;
    private FilterViewClick filterViewClick;
    private List<DataItemFilterMain> dataItem;

    public FilterAdapter(FilterViewClick filterViewClick, Context context) {
        this.context = context;
        this.filterViewClick = filterViewClick;
        this.dataItem = new ArrayList<>(0);
        this.dataItemSlected = new ArrayList<>(0);
    }

    public void loadData(List<DataItemFilterMain> dataItem) {
        this.dataItemSlected.clear();
        this.dataItem = dataItem;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_filters, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DataItemFilterMain item = dataItem.get(position);

        holder.onItemBind(item, position);
        holder.txtFilterTitle.setText(cutNull(item.getItemName()));
        holder.txtSubItem.setVisibility(View.GONE);
        if (item.getSelectedName() != null && item.getSelectedName().size() > 0) {
            holder.txtSubItem.setVisibility(View.VISIBLE);
            StringBuilder sf = new StringBuilder("");
            for (int i = 0; i < item.getSelectedName().size(); i++) {
                if (i == 0)
                    if (item.getSelectedName().size() == 1)
                        sf = new StringBuilder(item.getSelectedName().get(i));
                    else sf = new StringBuilder(item.getSelectedName().get(i) + ",");
                else if (i == 1)
                    sf.append(item.getSelectedName().get(i));
                else {
                    sf.append(" & ").append(item.getSelectedName().size() - 2).append(" more");
                    break;
                }
            }
            holder.txtSubItem.setText(sf);
        }
        /*if (item.getSelectedName() != null && !cutNull(item.getSelectedName()).isEmpty()) {
            holder.txtSubItem.setVisibility(View.VISIBLE);
            String[] strings = item.getSelectedName().split("_");
            if (strings.length > 2)
                holder.txtSubItem.setText(strings[0] + " , " + strings[1] + " & " + (strings.length - 2) + context.getResources().getString(R.string.more));
            else holder.txtSubItem.setText(Arrays.toString(strings).replace("_", ","));
        }*/
    }

    @Override
    public int getItemCount() {
        return dataItem != null ? dataItem.size() : 0;
    }

    public void changeData(DataItemFilterMain dataItems, int pos) {
        //this.dataItemSlected.clear();
        dataItemSlected.add(dataItems);
        notifyItemChanged(pos, dataItems);
    }

    public void loadDataToAdapter(List<DataItemFilterMain> dateFilterData) {
        this.dataItemSlected.clear();
        this.dataItem = dateFilterData;
        notifyDataSetChanged();
    }

    private String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }

    public interface FilterViewClick {
        void viewClickFilterItem(DataItemFilterMain model, int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_filter_title)
        TextView txtFilterTitle;
        @BindView(R.id.txt_sub_item)
        TextView txtSubItem;

        @BindView(R.id.rel_root)
        RelativeLayout relRoot;
        private DataItemFilterMain dataItem;
        private int pos;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void onItemBind(DataItemFilterMain item, int position) {
            this.dataItem = item;
            this.pos = position;
        }

        @OnClick(R.id.rel_root)
        public void onViewClicked() {
            filterViewClick.viewClickFilterItem(dataItem, pos);
        }
    }
}
