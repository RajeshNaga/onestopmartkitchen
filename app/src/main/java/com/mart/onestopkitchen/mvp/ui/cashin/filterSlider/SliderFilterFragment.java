package com.mart.onestopkitchen.mvp.ui.cashin.filterSlider;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.mvp.ui.cashin.FragmentListener;
import com.mart.onestopkitchen.mvp.ui.cashin.NearByOk$ServiceFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SliderFilterFragment extends Fragment {


    @BindView(R.id.textDone)
    TextView textDone;
    @BindView(R.id.checkbox_full)
    CheckBox texcheckboxFull;
    @BindView(R.id.checkbox_opend)
    CheckBox check_open;
    @BindView(R.id.checkbox_closing)
    CheckBox check_close;
    @BindView(R.id.checkbox_slider)
    CheckBox all;
    Unbinder unbinder;
    private FilterSliderAdapter filterSliderAdapter;

    // @SuppressLint("ValidFragment")
    //  public SliderFilterFragment(FragmentListener fragmentListener) {
    //  fragmentListener = fragmentListener;
    //}

    private FragmentListener fragmentListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement fragmentListener");
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_slider_filter, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iniUi();
    }

    private void iniUi() {
        // fragmentListener = NearByOk$ServiceFragment.Instance.listener;
        all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    texcheckboxFull.setChecked(false);
                    check_open.setChecked(false);
                    check_close.setChecked(false);
                }
            }
        });
        check_open.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    all.setChecked(false);
                }
            }
        });
        check_open.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    all.setChecked(false);
                }
            }
        });
        check_close.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    all.setChecked(false);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.textDone)
    public void onViewClicked() {
        fragmentListener = (FragmentListener) NearByOk$ServiceFragment.Instance.listener;
        fragmentListener.apiCall(texcheckboxFull.isChecked(), check_open.isChecked(), check_close.isChecked(), all.isChecked());
    }
}
