package com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel;

import java.io.Serializable;
import java.util.List;

public class UserSelectedList implements Serializable {

    private List<SelectedModel> selectedModelList;

    public List<SelectedModel> getSelectedModelList() {
        return selectedModelList;
    }

    public void setSelectedModelList(List<SelectedModel> selectedModelList) {
        this.selectedModelList = selectedModelList;
    }
}
