package com.mart.onestopkitchen.mvp.ui.cashin;

import com.mart.onestopkitchen.baseview.LoadView;
import com.mart.onestopkitchen.model.cashin.cashinresponsemodel.ContentItem;

import java.util.List;

public interface NearByListener extends LoadView {
    double amount();

    double latitude();

    double longitude();

    void successFromServer(List<ContentItem> content);
}
