package com.mart.onestopkitchen.mvp.ui.filterupdatedui.model;

import java.util.List;

public class FilterNewUpDateModel {
    private String SpecificationAttributeName;
    private List<FilterSubDataModel> subDataModels;

    public String getSpecificationAttributeName() {
        return SpecificationAttributeName;
    }

    public void setSpecificationAttributeName(String specificationAttributeName) {
        SpecificationAttributeName = specificationAttributeName;
    }

    public List<FilterSubDataModel> getSubDataModels() {
        return subDataModels;
    }

    public void setSubDataModels(List<FilterSubDataModel> subDataModels) {
        this.subDataModels = subDataModels;
    }
}
