package com.mart.onestopkitchen.mvp.ui.filteroption;

import android.content.Context;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.baseview.ResponseHandler;
import com.mart.onestopkitchen.networking.ApiBroker;
import com.mart.onestopkitchen.networking.response.ProductsResponse;

public class FilterMainActivityPresenter implements ResponseHandler {
    private Context mContext;
    private FilterMainActivityListener listener;
    private ApiBroker apiBroker;

    public FilterMainActivityPresenter(FilterMainActivityListener listener) {
        this.mContext = listener.getActivityContext();
        this.listener = listener;
        this.apiBroker = new ApiBroker(this);
    }

    public void apiCall(int whichApi) {
        switch (whichApi) {
            case 1:
                apiBroker.categoryFilter(whichApi, listener.getCategoryId());
                break;
        }
    }

    @Override
    public void onSuccess(Object o, int api) {
        if (o != null) {
            switch (api) {
                case 1:
                    getFilterOption(o);
                    break;
            }
        } else listener.showError(mContext.getString(R.string.some_thing_went));
    }

    private void getFilterOption(Object o) {
        ProductsResponse productsResponse;
        if (o instanceof ProductsResponse)
            productsResponse = (ProductsResponse) o;

    }

    @Override
    public void onErrorBody(Object s, int api) {

    }

    @Override
    public void onFailure(String s) {

    }
}
