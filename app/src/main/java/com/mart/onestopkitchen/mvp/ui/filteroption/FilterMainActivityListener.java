package com.mart.onestopkitchen.mvp.ui.filteroption;

import com.mart.onestopkitchen.baseview.LoadView;

public interface FilterMainActivityListener extends LoadView {

    int getCategoryId();

}
