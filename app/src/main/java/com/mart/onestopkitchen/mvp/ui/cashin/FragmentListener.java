package com.mart.onestopkitchen.mvp.ui.cashin;

public interface FragmentListener {
    void apiCall(boolean fullService, boolean opened, boolean recentClose, boolean noFilter);
}
