package com.mart.onestopkitchen.mvp.ui.filterupdatedui.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.model.FilterItem;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.FilterMainActivityNew;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.FragmentListenerNew;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.adapter.CategoriesAdapter;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.adapter.CategoriesListAdapter;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.FilterNewUpDateModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.FilterSubDataModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.UserSelectedList;
import com.mart.onestopkitchen.networking.response.ProductsResponse;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.mart.onestopkitchen.utils.AppConstants.FILTER_OPTION_INTENT;


/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragmentUpdate extends Fragment implements CategoriesAdapter.OnViewClicked, CategoriesListAdapter.OnSubClick {


    @BindView(R.id.rv_cate)
    RecyclerView rvCate;
    @BindView(R.id.btn_reset)
    Button btnReset;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.rel_search)
    RelativeLayout relSearch;
    @BindView(R.id.rv_cate_list)
    RecyclerView rvCateList;
    @BindView(R.id.btn_apply)
    Button btnApply;

    Unbinder unbinder;
    @BindView(R.id.txt_cate)
    TextView txtCate;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.imgh_view)
    ImageView imghView;
    @BindView(R.id.img_arrow)
    ImageView imgArrow;
    @BindView(R.id.lin_price)
    LinearLayout linPrice;
    @BindView(R.id.edt_cate_search)
    EditText edtCateSearch;
    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.txt_min_price)
    TextView txtMinPrice;
    @BindView(R.id.txt_max_price)
    TextView txtMaxPrice;
    @BindView(R.id.range_seek_bar)
    RangeSeekBar rangeSeekBar;
    @BindView(R.id.lin_seek_bar)
    LinearLayout linSeekBar;
    @BindView(R.id.lin_price_data)
    LinearLayout linPriceData;
    @BindView(R.id.txt_no_record)
    TextView noRecordFound;
    @BindView(R.id.rel_root)
    RelativeLayout relRoot;

    boolean subViewClicked = false;
    private FilterMainActivityNew filterMainActivity;
    private FragmentListenerNew mListener;
    private ProductsResponse productDetailResponse;
    private CategoriesAdapter categoriesAdapter;
    private CategoriesListAdapter listAdapter;
    private Context mContext;
    private ArrayList<FilterSubDataModel> selectedModelList = new ArrayList<>(0);
    private Double minPrice = 0.0, maxPrice = 0.0;
    private List<FilterSubDataModel> getGroupValuess;
    private List<FilterSubDataModel> dataModels = new CopyOnWriteArrayList<>();
    private MyApplication myApplication;
    private boolean isAlreadyAdded;
    private int leftvalInt = 0, rightValInt = 0;
    private ViewTreeObserver observer;
    private boolean isFirst = false;
    private boolean isPreviousDataReset;
    private int count = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filters_update, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListenerNew) {
            mListener = (FragmentListenerNew) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.setTitle(getString(R.string.filter));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        filterMainActivity = (FilterMainActivityNew) getActivity();
        myApplication = (MyApplication) getContext().getApplicationContext();
        uiListener();
        hideKeyboard();
        // observer = relRoot.getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    private void initRv() {
        listAdapter = new CategoriesListAdapter(getContext(), this);
        rvCateList.setLayoutManager(new LinearLayoutManager(mContext));
        rvCateList.setAdapter(listAdapter);
        categoriesAdapter = new CategoriesAdapter(getContext(), this, filterNewUpDateModel(productDetailResponse.getNotFilteredItems()));
        rvCate.setLayoutManager(new LinearLayoutManager(mContext));
        rvCate.setAdapter(categoriesAdapter);
    }

    private void uiListener() {
        imgClose.setVisibility(View.GONE);
        productDetailResponse = filterMainActivity.getDetails();
        if (productDetailResponse != null && productDetailResponse.getPriceRange() != null) {
            minPrice = productDetailResponse.getPriceRange().getFrom();
            maxPrice = productDetailResponse.getPriceRange().getTo();
        } else {
            minPrice = 0.0;
            maxPrice = 0.0;
            linPrice.setVisibility(View.GONE);
        }
        initRv();
        priceAction();
        if (myApplication.leftValInt != 0 || myApplication.rightValInt != 0) {
            rangeSeekBar.setValue(myApplication.leftValInt, myApplication.rightValInt);
            setTextValues(myApplication.rightValInt, myApplication.leftValInt);
        } else {
            setTextValues(100, 0);
            rangeSeekBar.setValue(0, 100);
            // rangeSeekBar.setValue(myApplication.leftValInt, myApplication.rightValInt);
        }
        rangeSeekBar.setRange(0, 100);

        rangeSeekBar.setIndicatorTextDecimalFormat("0");
        rangeSeekBar.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                setTextValues(rightValue, leftValue);
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });

        edtCateSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() > 0) {
                    imgClose.setVisibility(View.VISIBLE);
                    ArrayList<FilterSubDataModel> searchList = new ArrayList<>(0);
                    for (FilterSubDataModel model : getGroupValuess) {
                        if (model.getOptionName().toLowerCase().contains(editable.toString().toLowerCase())) {
                            searchList.add(model);
                        }
                    }
                    if (searchList.size() > 0) {
                        noRecord(false);
                        listAdapter.loadDatas(searchList, "");
                    } else {
                        noRecord(true);
                    }
                } else {
                    imgClose.setVisibility(View.GONE);
                    noRecord(false);
                    listAdapter.loadDatas(getGroupValuess, "");
                }
            }

        });
    }

    private void noRecord(boolean s) {
        if (s) {
            noRecordFound.setVisibility(View.VISIBLE);
            rvCateList.setVisibility(View.GONE);
        } else {
            noRecordFound.setVisibility(View.GONE);
            rvCateList.setVisibility(View.VISIBLE);
        }
    }

    private void setTextValues(float rightValue, float leftValue) {
        try {
            leftvalInt = Math.round(leftValue);
            rightValInt = Math.round(rightValue);
            Double aDouble = Double.parseDouble(String.valueOf(leftValue));
            Double ri = Double.parseDouble(String.valueOf(rightValue));
            Double aDouble1 = 100 / ri;
            Double aDouble2 = maxPrice / aDouble1;
            Double right = 100 / aDouble;
            // Double rightSeek = maxPrice / right;
            double ans = maxPrice - minPrice;
            double aans = ans / 10;
            double mulvalues = leftvalInt / 10;
            double mulvalues2 = rightValInt / 10;
            Double rightSeek = minPrice + (aans * mulvalues);
            Double maxSeek = minPrice + (aans * mulvalues2);
            txtMinPrice.setText(cutNull(AppUtils.getMMKString(txtMinPrice.getTextSize(), String.valueOf(rightSeek.intValue()), 0)));
            txtMaxPrice.setText(cutNull(AppUtils.getMMKString(txtMaxPrice.getTextSize(), String.valueOf(maxSeek.intValue()), 0)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private List<FilterNewUpDateModel> filterNewUpDateModel(List<FilterItem> notFilteredItems) {
        List<FilterNewUpDateModel> dateModels = new ArrayList<>();
        LinkedHashSet<String> types = new LinkedHashSet();
        for (FilterItem item : notFilteredItems) {
            types.add(item.getSpecificationAttributeName());
        }
        List<String> strings = new ArrayList<>(types);
        for (int i = 0; i < strings.size(); i++) {
            List<FilterSubDataModel> filterSubDataModels = new ArrayList<>();
            FilterNewUpDateModel filterNewUpDateModel = new FilterNewUpDateModel();
            filterNewUpDateModel.setSpecificationAttributeName(strings.get(i));
            for (FilterItem item : notFilteredItems) {
                if (strings.get(i).equalsIgnoreCase(item.getSpecificationAttributeName())) {
                    FilterSubDataModel filterSubDataModel = new FilterSubDataModel();
                    filterSubDataModel.setFilterId(item.getFilterId());
                    filterSubDataModel.setProductId(item.getProductId());
                    filterSubDataModel.setAttributeName(strings.get(i));
                    filterSubDataModel.setOptionName(item.getSpecificationAttributeOptionName());
                    filterSubDataModels.add(filterSubDataModel);
                    filterNewUpDateModel.setSubDataModels(filterSubDataModels);
                }
            }
            dateModels.add(filterNewUpDateModel);
        }
        return dateModels;
    }

    @OnClick({R.id.btn_reset, R.id.btn_apply, R.id.lin_price, R.id.img_close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_reset:
                resetFilterOption();
                break;
            case R.id.btn_apply:
                applyAction();
                break;
            case R.id.lin_price:
                priceAction();
                break;
            case R.id.img_close:
                searchCloseAction();
                break;
        }
    }

    private void searchCloseAction() {
        edtCateSearch.setText("");
    }

    private void priceAction() {
        edtCateSearch.setText("");
        imgArrow.setVisibility(View.VISIBLE);
        relSearch.setVisibility(View.GONE);
        categoriesAdapter.selectedPosition = -1;
        categoriesAdapter.notifyDataSetChanged();
        if (minPrice != 0.0 &&
                maxPrice != 0.0)
            linPriceData.setVisibility(View.VISIBLE);
        txtCate.setTextColor(getResources().getColor(R.color.accent));
        rvCateList.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                edtCateSearch.clearFocus();
                // if (isFirst)
                //     showKeyboard();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtCateSearch.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                //  isFirst=true;
            }
        }, 100);

    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void applyAction() {
        try {
            String price;
            if (dataModels.size() > 0) {
                if (myApplication.alreadySelected == null)
                    myApplication.alreadySelected = new ArrayList<>();
                else {
                    if (!subViewClicked) {
                        removeItemFromPreviousList(dataModels);
                    }
                    dataModels.addAll(myApplication.alreadySelected);
                    myApplication.alreadySelected.clear();
                }
                // myApplication.alreadySelected.addAll(dataModels);}
            } else {
                if (myApplication.alreadySelected == null)
                    myApplication.alreadySelected = new ArrayList<>();
                dataModels = myApplication.alreadySelected;
                myApplication.alreadySelected = new ArrayList<>(0);
            }
            List<FilterSubDataModel> dataModelsa = groupData(dataModels);
            for (FilterSubDataModel filterSubDataModel : dataModelsa) {
                if (filterSubDataModel.isSelected()) {
                    myApplication.alreadySelected.add(filterSubDataModel);
                }

            }
            /*if (myApplication.alreadySelected.size() == 0)
                isPreviousDataReset = true;*/
            myApplication.rightValInt = rightValInt;
            myApplication.leftValInt = leftvalInt;
            try {
                Double minValueUserSelected = Double.parseDouble(txtMinPrice.getText().toString().replace(" MMK", "").replace(",", ""));
                Double maxValueUserSelected = Double.parseDouble(txtMaxPrice.getText().toString().replace(" MMK", "").replace(",", ""));
                price = minValueUserSelected + "-" + maxValueUserSelected;
                myApplication.alreadySelectedPrice = price;
            } catch (Exception e) {
                e.printStackTrace();
                price = "0-0";
                myApplication.alreadySelectedPrice = price;
            }

            UserSelectedList selectedItems = new UserSelectedList();
            List<SelectedModel> selectedModelLis = new ArrayList<>();
            if (dataModelsa.size() > 0) {
                for (FilterSubDataModel item : dataModelsa) {
                    for (FilterItem item1 : productDetailResponse.getNotFilteredItems()) {
                        if (item.getOptionName().equalsIgnoreCase(item1.getSpecificationAttributeOptionName())) {
                            if (item.isSelected()) {
                                SelectedModel selectedModel = new SelectedModel();
                                selectedModel.setCateName(item1.getSpecificationAttributeName());
                                selectedModel.setOptionName(item1.getSpecificationAttributeOptionName());
                                selectedModel.setSelected(true);
                                selectedModel.setProductId(item1.getProductId());
                                selectedModel.setFilterId(item1.getFilterId());
                                selectedModelLis.add(selectedModel);
                            }
                            //break;
                        }
                    }
                }
            }
            selectedItems.setSelectedModelList(selectedModelLis);
            if (subViewClicked)
                myApplication.selectedModelLis = selectedItems;
            Intent intent = new Intent();
            intent.putExtra("price", price);
            intent.putExtra("resetData", isPreviousDataReset);
      /*  if (myApplication.fromWhere.equalsIgnoreCase("search")) {
            intent.putExtra("category", selectedItems);
        } else {*/
            if (!subViewClicked)
                intent.putExtra("category", selectedItems);
            else
                intent.putExtra("category", myApplication.selectedModelLis);
            // }
            filterMainActivity.setResult(FILTER_OPTION_INTENT, intent);
            Objects.requireNonNull(getActivity()).finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<FilterSubDataModel> groupData(List<FilterSubDataModel> dataModels) {
        LinkedHashSet<FilterSubDataModel> stringLinkedHashSet = new LinkedHashSet<FilterSubDataModel>(dataModels);
        return new ArrayList<>(stringLinkedHashSet);
    }

    private List<FilterSubDataModel> getGroupValuess(ArrayList<FilterSubDataModel> selectedModelList, boolean isDataAdded) {
        LinkedHashSet<String> types = new LinkedHashSet();
        for (FilterSubDataModel dataModel : selectedModelList) {
            types.add(dataModel.getOptionName().substring(0, 1).toUpperCase() + dataModel.getOptionName().substring(1));
        }
        selectedModelList.clear();
        List<String> strings = new ArrayList<>(types);
        for (String s : strings) {
            FilterSubDataModel dataModel = new FilterSubDataModel();
            dataModel.setOptionName(s);
            for (FilterSubDataModel dataModel1 : getMerchedData(s, isDataAdded)) {
                if (s.equalsIgnoreCase(dataModel1.getOptionName())) {
                    dataModel.setSelected(dataModel1.isSelected());
                }
            }
            selectedModelList.add(dataModel);
        }
        return selectedModelList;
    }

    private List<FilterSubDataModel> getMerchedData(String s, boolean isDataAdded) {
        if (myApplication.alreadySelected != null) {
            //if (isDataAdded)dataModels.clear();
            for (FilterSubDataModel dataModel1 : myApplication.alreadySelected) {
                // for (FilterSubDataModel dataModelsa : dataModels) {
                if (s.equalsIgnoreCase(dataModel1.getOptionName())) {
                    if (isDataAdded) {
                        //checkAlreadyAdded(dataModel1);
                        dataModels.add(dataModel1);
                    }
                }
                //   }
            }
        }
        // }
        return !isDataAdded ? dataModels : myApplication.alreadySelected == null ? new ArrayList<>() : myApplication.alreadySelected;
    }

    private void checkAlreadyAdded(FilterSubDataModel dataModel1) {
        if (myApplication.alreadySelected != null)
            for (FilterSubDataModel filterSubDataModel1 : dataModels) {
                for (int i = 0; i < dataModels.size(); i++) {
                    if (filterSubDataModel1.getOptionName().equalsIgnoreCase(dataModel1.getOptionName())) {
                        dataModels.remove(i);
                    }
                }
            }


    }

    private List<FilterSubDataModel> getAlreadySelectGroupValues(ArrayList<FilterSubDataModel> selectedModelList) {
        LinkedHashSet<String> types = new LinkedHashSet();
        for (FilterSubDataModel dataModel : selectedModelList) {
            types.add(dataModel.getOptionName().substring(0, 1).toUpperCase() + dataModel.getOptionName().substring(1));
        }
        selectedModelList.clear();
        List<String> strings = new ArrayList<>(types);
        for (String s : strings) {
            FilterSubDataModel dataModel = new FilterSubDataModel();
            dataModel.setOptionName(s);
            for (FilterSubDataModel dataModel1 : myApplication.alreadySelected) {
                if (s.equalsIgnoreCase(dataModel1.getOptionName())) {
                    dataModel.setSelected(dataModel1.isSelected());
                }
            }
            selectedModelList.add(dataModel);
        }
        return selectedModelList;
    }

    /*private void resetFilterOption() {
        rangeSeekBar.setRange(0, 100);
        rangeSeekBar.setValue(0, 100);
        priceAction();
        categoriesAdapter.loadCateList(filterNewUpDateModel(filterMainActivity.getDetails().getNotFilteredItems()));
        dataModels.clear();
    }*/

    private void resetFilterOption() {
        rangeSeekBar.setRange(0, 100);
        rangeSeekBar.setValue(0, 100);
        priceAction();
        isPreviousDataReset = true;
        if (myApplication.alreadySelected != null) {
            myApplication.alreadySelected.clear();
            dataModels.clear();
            selectedModelList.clear();
            myApplication.selectedModelLis = null;
            listAdapter.selectedPos = -1;
            categoriesAdapter.loadCateList(filterNewUpDateModel(productDetailResponse.getNotFilteredItems()));
        } else {
            myApplication.selectedModelLis = new UserSelectedList();
            categoriesAdapter.loadCateList(filterNewUpDateModel(filterMainActivity.getDetails().getNotFilteredItems()));
            dataModels.clear();
        }
    }


    @Override
    public void onCateClicked(FilterNewUpDateModel productDetailResponse) {
        count++;
        linPriceData.setVisibility(View.GONE);
        txtCate.setTextColor(getResources().getColor(R.color.black));
        imgArrow.setVisibility(View.GONE);
        rvCateList.setVisibility(View.VISIBLE);
        relSearch.setVisibility(View.VISIBLE);
        selectedModelList.clear();
        listAdapter.selectedPos = -1;
        edtCateSearch.setText("");
        //   if (myApplication.alreadySelected == null) {
        selectedModelList.addAll(productDetailResponse.getSubDataModels());
        getGroupValuess = getGroupValuess(selectedModelList, true);
        listAdapter.loadData(getGroupValuess(selectedModelList, false), productDetailResponse.getSpecificationAttributeName());
      /*  } else {
            selectedModelList.addAll(productDetailResponse.getSubDataModels());
            getGroupValuess = getAlreadySelectGroupValues(selectedModelList);
            listAdapter.loadData(getAlreadySelectGroupValues(selectedModelList), productDetailResponse.getSpecificationAttributeName());
*/
        // }
        //  }
    }

    @Override
    public void onSubViewClicked(FilterSubDataModel filterSubDataModel, String attributeName, int pos) {
        subViewClicked = true;
        int posString = 0;
        //edtCateSearch.setText("");
        //myApplication.alreadySelected = null;
        if (edtCateSearch.getText().toString().length() > 0 && listAdapter.productDetailResponse.size() > 0) {

            for (int i = 0; i < getGroupValuess.size(); i++) {
                if (getGroupValuess.get(i).getOptionName().equalsIgnoreCase(filterSubDataModel.getOptionName())) {
                    posString = i;
                }
            }
            removeItemFromPreviousList(dataModels);
            if (filterSubDataModel.isSelected()) {
                filterSubDataModel.setSelected(false);
                for (int i = 0; i < dataModels.size(); i++) {
                    if (dataModels.get(i).getOptionName().equalsIgnoreCase(filterSubDataModel.getOptionName())) {
                        FilterSubDataModel dataModel = dataModels.get(i);
                        dataModel.setSelected(false);
                        dataModels.remove(i);
                        //   dataModels.remove(i, dataModel);
                    }
                }
            } else {
                filterSubDataModel.setSelected(true);
                dataModels.add(filterSubDataModel);
            }
            getGroupValuess.set(posString, filterSubDataModel);
            listAdapter.notifyDataSetChanged();
            //listAdapter.loadDatas(listAdapter.productDetailResponse, "");
        } else {
            for (int i = 0; i < getGroupValuess.size(); i++) {
                if (getGroupValuess.get(i).getOptionName().equalsIgnoreCase(filterSubDataModel.getOptionName())) {
                    posString = i;
                }
            }
            // addRemoveItemFromPreviousList(dataModels,filterSubDataModel);
            removeItemFromPreviousList(dataModels);

            if (filterSubDataModel.isSelected()) {
                filterSubDataModel.setSelected(false);
                for (int i = 0; i < dataModels.size(); i++) {
                    if (dataModels.get(i).getOptionName().equalsIgnoreCase(filterSubDataModel.getOptionName())) {
                        FilterSubDataModel dataModel = dataModels.get(i);
                        dataModel.setSelected(false);
                        dataModels.remove(i);
                        //   dataModels.remove(i, dataModel);
                    }
                }
            } else {
                filterSubDataModel.setSelected(true);
                dataModels.add(filterSubDataModel);
            }

            getGroupValuess.set(posString, filterSubDataModel);
            listAdapter.loadDatas(getGroupValuess, "");
        }
    }


    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void removeItemFromPreviousList(List<FilterSubDataModel> dataModels) {
        if (myApplication.alreadySelected != null)
            for (FilterSubDataModel filterSubDataModel1 : dataModels) {
                for (int i = 0; i < myApplication.alreadySelected.size(); i++) {
                    if (filterSubDataModel1.getOptionName().equalsIgnoreCase(myApplication.alreadySelected.get(i).getOptionName())) {
                        myApplication.alreadySelected.remove(i);
                    }
                }
            }
    }

    private void addRemoveItemFromPreviousList(List<FilterSubDataModel> dataModels, FilterSubDataModel filterSubDataModel) {
        if (myApplication.alreadySelected != null)
            for (FilterSubDataModel filterSubDataModel1 : dataModels) {
                for (int i = 0; i < myApplication.alreadySelected.size(); i++) {
                    if (filterSubDataModel1.getOptionName().equalsIgnoreCase(myApplication.alreadySelected.get(i).getOptionName())) {
                        myApplication.alreadySelected.remove(i);
                    }
                }
            }
    }

}
