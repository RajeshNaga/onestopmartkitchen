package com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.newmodel;

import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.FilterSubDataModel;

import java.util.List;

public class SelectedUpdateModel {

    private String optionName;
    private List<FilterSubDataModel> dataModels;

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public List<FilterSubDataModel> getDataModels() {
        return dataModels;
    }

    public void setDataModels(List<FilterSubDataModel> dataModels) {
        this.dataModels = dataModels;
    }
}
