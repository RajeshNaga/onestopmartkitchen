package com.mart.onestopkitchen.mvp.ui.cashin.filterSlider;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterSliderAdapter extends RecyclerView.Adapter<FilterSliderAdapter.ViewHolder> {


    private List<String> list;

    public FilterSliderAdapter(List<String> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_slider, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtCheckTitle.setText(list.get(i));
        viewHolder.checkboxSlider.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @OnClick(R.id.rel_check_box)
    public void onViewClicked() {
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_check_title)
        TextView txtCheckTitle;
        @BindView(R.id.checkbox_slider)
        CheckBox checkboxSlider;
        @BindView(R.id.rel_check_box)
        RelativeLayout relCheckBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
