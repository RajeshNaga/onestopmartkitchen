package com.mart.onestopkitchen.mvp.ui.filterupdatedui;

import androidx.fragment.app.Fragment;

public interface FragmentListenerNew {
    void setTitle(CharSequence title);

    void nextFragment(Fragment fragment, boolean isBackStack, Integer... animations);
}
