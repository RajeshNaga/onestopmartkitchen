package com.mart.onestopkitchen.base64imge;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImageUrlRequestModel {

    @SerializedName("MobileNumber")
    private String mobileNumber;

    @SerializedName("Base64String")
    private List<String> base64String;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public List<String> getBase64String() {
        return base64String;
    }

    public void setBase64String(List<String> base64String) {
        this.base64String = base64String;
    }
}