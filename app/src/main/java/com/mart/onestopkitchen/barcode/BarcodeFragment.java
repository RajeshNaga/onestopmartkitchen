package com.mart.onestopkitchen.barcode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.google.zxing.Result;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.ui.fragment.BaseFragment;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hemnath on 10/10/2018.
 */

public class BarcodeFragment extends BaseFragment implements ZXingScannerView.ResultHandler {
    private Context context;
    private ZXingScannerView mScannerView;
    private Toast toast;

    public static BarcodeFragment getInstance(Context context) {
        BarcodeFragment barcodeFragment = new BarcodeFragment();
        barcodeFragment.context = context;
        return barcodeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mScannerView = new ZXingScannerView(getContext());
        mScannerView.setBorderColor(getResources().getColor(R.color.bt_blue));
        mScannerView.setBorderStrokeWidth(15);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.scan_barcode));
        return mScannerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.scan_barcode));
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {
        mScannerView.resumeCameraPreview(this);
        if (result != null && !result.equals("")) {
            callWebserviceForProductId(result.getText());
        } else {
            if (null != getActivity())
                getActivity().onBackPressed();
        }
    }

    private void callWebserviceForProductId(String scannedValue) {
       /* RetroClient.getApi().getScannedProductId(scannedValue)
                .enqueue(new CustomCB<BarcodeResponse>());*/
        RetroClient.getApi().getScannedProductId(scannedValue)
                .enqueue(new Callback<BarcodeResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<BarcodeResponse> call, @NotNull Response<BarcodeResponse> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().getProductId() != 0) {
                                    ProductModel productModel = new ProductModel();
                                    productModel.setId(response.body().getProductId());
                                    productModel.setName("");
                                    ProductDetailFragment.productModel = productModel;

                                    FragmentManager fragmentManager = getFragmentManager();
                                    if (fragmentManager != null) {
                                        fragmentManager.popBackStack();
                                        fragmentManager.beginTransaction()
                                                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                                                .replace(R.id.container, new ProductDetailFragment()).addToBackStack(null).commit();
                                    }

                                } else {
                                    showMyToast(response.body().getMessage());
                                }
                            }
                        } catch (NullPointerException e) {
                            if (null != getActivity())
                                showMyToast(getActivity().getResources().getString(R.string.no_product_for_barcode));
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<BarcodeResponse> call, @NotNull Throwable t) {
                        if (null != getActivity())
                            showMyToast(getActivity().getResources().getString(R.string.no_product_for_barcode));
                    }
                });
    }

    /*public void onEvent(BarcodeResponse response) {
        if(response.getProductId() != 0){
            ProductModel productModel = new ProductModel();
            productModel.setId(response.getProductId());
            productModel.setName("");
            ProductDetailFragment.productModel = productModel;
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.popBackStack();
            fragmentManager.beginTransaction().replace(R.id.container, new ProductDetailFragment()).addToBackStack(null).commit();
        }else{
            showMyToast(response.getMessage());
        }
    }*/

    @SuppressLint("ShowToast")
    private void showMyToast(String st) {
        try {
            if (toast != null) {
                toast.getView().isShown();
                toast.setText(st);
            }
        } catch (Exception e) {
            toast = Toast.makeText(context, st, Toast.LENGTH_SHORT);
        }
        if (null != toast)
            toast.show();
    }
}
