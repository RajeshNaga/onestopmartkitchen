package com.mart.onestopkitchen.barcode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hemnath on 1/31/2019.
 */
public class BarcodeResponse {

    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("Message")
    @Expose
    private String message;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
