package com.mart.onestopkitchen.service;

import android.content.Context;

import com.binjar.prefsdroid.Preference;

/**
 * Created by Ashraful on 11/25/2015.
 */

public class PreferenceService {
    public static final String FIRST_RUN = "firstRun";
    public static String SHARED_PREF_KEY = "1STOP_KITCHEN_PREF";
    public static String IS_LOGEDIN = "isLoggedIn";
    public static String LOGGED_PREFER_KEY = "isLoggedIn";
    public static String TOKEN_KEY = "token";
    public static String URL_PREFER_KEY = "URL_CHANGE_KEY";
    public static String DO_USE_NEW_URL = "URL_CHANGE_KEY_BOOLEAN";
    public static String APP_VERSION_CODE_KEY = "VERSION_CODE";
    public static String SENT_TOKEN_TO_SERVER = "SENT_TOKEN_TO_SERVER_KEY";
    public static String REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE_KEY";
    public static String taxShow = "taxShow";
    public static String discuntShow = "discuntShow";
    public static String CURRENT_LANGUAGE = "current_language";
    public static String CURRENT_LANGUAGE_ID = "current_language_id";
    public static String NST = "nst";
    public static String VERIFIED_DONE = "VERIFIED_DONE";
    public static String REGISTER_DONE = "REGISTER_DONE";
    public static String ADDRESS_DONE = "ADDRESS_DONE";
    public static String AUTO_REGISTER_DATA = "AUTO_REGISTER_DATA";
    public static String USER_OK_DATA = "USER_OK_DATA";
    public static String USER_MOBILE_NUMBER = "USER_MOBILE_NUMBER";
    public static String OPERATOR_MOBILE_NUMBER = "OPERATOR_MOBILE_NUMBER";
    public static String USER_COUNTRY_CODE = "USER_COUNTRY_CODE";
    public static String USER_SIM_ID = "USER_SIM_ID";
    public static String USER_SIM_ID1 = "USER_SIM_ID1";
    public static String USER_DEVICE_ID = "USER_DEVICE_ID";
    public static String USER_MSID = "USER_MSID";
    public static String SLOT_NAME = "SLOT_NAME";
    public static String FB_ACCOUNT = "FB_ACCOUNT";
    public static String EMAIL_ACCOUNT = "EMAIL_ACCOUNT";
    public static String OTP_MESSAGE = "OTP_MESSAGE";
    public static String Validate_Contact_Uploaded = "Validate_Contact_Uploaded";
    public static String CONTACTS_SCHEDULE_FUNCTION_WEEK = "Contacts_Date_new";
    public static String CONTACTS_UPLOAD_FUNCTION = "Contacts_Upload_New_new";
    public static String PASSWORD = "password";
    public static String USERNAME = "username";
    public static String USER_NAME_TXT = "txt_username";
    public static String PROCEED_AS_GUEST = "PROCEED_AS_GUEST";
    public static String PROFILE_PIC = "PROFILE_PIC";
    public static String CURRENT_DATE_UPLOAD_CONTACT = "CURRENT_DATE_UPLOAD_CONTACT";
    public static String USER_SEARCH = "USER_SEARCH";
    public static String NAVIGATION_CLASS_NAME = "NAVIGATION_ONCLICK_CLASS_NAME";
    public static String internetChecking = "internetchecking";
    public static String PICKUP_POINT_ADDRESS = "Pickup_Point_Address";
    public static String PICKUP_POINT_ADDRESS_ID = "Pickup_Point_Address_Id";
    public static String FCM_TOKEN = "fcm_token";
    public static String USER_PROFILE = "USER_PROFILE";
    public static String PICKUP_POINT_NAME = "pickup_point_name";
    public static String LOCATIONS_LAT = "LOCATION_LAT";
    public static String LOCATIONS_LONG = "LOCATION_LONG";
    public static String RECENT_VIEW_PRODUCTS = "RECENT_VIEW_PRODUCTS";
    public static String RECENT_SEARCH_PRODUCTS = "RECENT_SEARCH_PRODUCTS";
    public static String IS_PAYMENT_DONE = "is_payment_done";
    private static PreferenceService preferenceService;

    private PreferenceService() {
    }

    public static PreferenceService getInstance() {
        if (preferenceService == null) {
            preferenceService = new PreferenceService();
        }
        return preferenceService;
    }

    public String GetPreferenceValue(String key) {
        return Preference.getString(key, "");
    }

    public int GetPreferenceIntValue(String key) {
        return Preference.getInt(key, -1);
    }

    public Boolean GetPreferenceBooleanValue(String key) {
        return Preference.getBoolean(key, false);
    }

    public void SetPreferenceValue(String key, String value) {
        Preference.putString(key, value);
    }

    public void SetPreferenceValue(String key, Boolean value) {
        Preference.putBoolean(key, value);
    }

    public void SetPreferenceValue(String key, int value) {
        Preference.putInt(key, value);
    }

    public String getCountryCode(Context context) {
        return GetPreferenceValue("COUNTRY_CODE");
    }

    public void clearAppPreference(Context context) {
        if (preferenceService != null) {
            Preference.clear();
        }
    }

    public boolean getInternetChecking() {
        return GetPreferenceBooleanValue(internetChecking);
    }

    public void setInternetChecking(Context context, boolean bool) {
        SetPreferenceValue(internetChecking, bool);
    }

    public void removekey(String key) {
        if (preferenceService != null) {
            Preference.remove(key);
        }
    }

    public boolean getTotalContactSync() {
        return GetPreferenceBooleanValue("totalContactSync");
    }

    public void setTotalContactSync(Context context, boolean bool) {
        SetPreferenceValue("totalContactSync", bool);
    }

    public void setTotalPhoneContact(Context context, int total) {
        SetPreferenceValue("totalContact", total);
    }

    public void setTotalPhoneCursorContact(Context context, int total) {
        SetPreferenceValue("totalContactCursor", total);
    }

    public int getTotalPhoneCursorContact(Context context) {

        return GetPreferenceIntValue("totalContactCursor");
    }
}
