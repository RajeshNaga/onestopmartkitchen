package com.mart.onestopkitchen.loginuser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegisterRequestModel {

    @SerializedName("SimId")
    @Expose
    private String simId;

    @SerializedName("Email")
    @Expose
    private List<String> email;

    @SerializedName("Username")
    @Expose
    private String username;

    @SerializedName("Phone")
    @Expose
    private String phone;

    @SerializedName("DOB")
    @Expose
    private String dOB;

    @SerializedName("DeviceId")
    @Expose
    private String deviceId;

    @SerializedName("Gender")
    @Expose
    private String gender;

    @SerializedName("FaceBook")
    @Expose
    private String faceBook;

    @SerializedName("CountryCode")
    @Expose
    private String countryCode;

    @SerializedName("Address")
    @Expose
    private Address address;


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getSimId() {
        return simId;
    }

    public void setSimId(String simId) {
        this.simId = simId;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDOB() {
        return dOB;
    }

    public void setDOB(String dOB) {
        this.dOB = dOB;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFaceBook() {
        return faceBook;
    }

    public void setFaceBook(String faceBook) {
        this.faceBook = faceBook;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}