package com.mart.onestopkitchen.loginuser.modelnew.requestmodel.login;

import com.google.gson.annotations.SerializedName;

public class LoginRequestModel {

    @SerializedName("Email")
    private String email;

    @SerializedName("DeviceID")
    private String deviceID;

    @SerializedName("Lastname")
    private String lastname;

    @SerializedName("Address2")
    private String address2;

    @SerializedName("Gender")
    private String gender;

    @SerializedName("Address1")
    private String address1;

    @SerializedName("City")
    private String city;

    @SerializedName("DateOfBirthYear")
    private String dateOfBirthYear;

    @SerializedName("DateOfBirthDay")
    private String dateOfBirthDay;

    @SerializedName("OtherEmail")
    private String otherEmail;

    @SerializedName("Firstname")
    private String firstname;

    @SerializedName("Simid")
    private String simid;

    @SerializedName("MobileNumber")
    private String mobileNumber;

    @SerializedName("Username")
    private String username;

    @SerializedName("State")
    private String state;

    @SerializedName("DeviceInfo")
    private DeviceInfo deviceInfo;

    @SerializedName("Country")
    private String country;

    @SerializedName("DateofBirthMonth")
    private String dateofBirthMonth;

    @SerializedName("Password")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDateOfBirthYear() {
        return dateOfBirthYear;
    }

    public void setDateOfBirthYear(String dateOfBirthYear) {
        this.dateOfBirthYear = dateOfBirthYear;
    }

    public String getDateOfBirthDay() {
        return dateOfBirthDay;
    }

    public void setDateOfBirthDay(String dateOfBirthDay) {
        this.dateOfBirthDay = dateOfBirthDay;
    }

    public String getOtherEmail() {
        return otherEmail;
    }

    public void setOtherEmail(String otherEmail) {
        this.otherEmail = otherEmail;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSimid() {
        return simid;
    }

    public void setSimid(String simid) {
        this.simid = simid;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDateofBirthMonth() {
        return dateofBirthMonth;
    }

    public void setDateofBirthMonth(String dateofBirthMonth) {
        this.dateofBirthMonth = dateofBirthMonth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}