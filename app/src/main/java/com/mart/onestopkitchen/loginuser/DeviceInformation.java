package com.mart.onestopkitchen.loginuser;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;

import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.loginuser.loginnewmodel.DeviceInfo;
import com.mart.onestopkitchen.utils.AppUtils;

import java.lang.reflect.Method;

import static android.content.Context.TELEPHONY_SERVICE;
import static android.content.Context.TELEPHONY_SUBSCRIPTION_SERVICE;


public class DeviceInformation {
    private String getDeviceSoftwareVersion(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            return tm.getDeviceSoftwareVersion();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getNetworkCountryIso(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getNetworkCountryIso();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getNetworkOperator(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getNetworkOperator();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getNetworkOperatorName(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getNetworkOperatorName();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getNetworkType(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            switch (tm.getNetworkType()) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "Unknown";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getPhoneType(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            int i = tm.getPhoneType();
            if (i == TelephonyManager.PHONE_TYPE_NONE)
                return "NONE";
            else if (i == TelephonyManager.PHONE_TYPE_GSM)
                return "GSM";
            else if (i == TelephonyManager.PHONE_TYPE_CDMA)
                return "CDMA";
            else if (i == TelephonyManager.PHONE_TYPE_SIP)
                return "SIP";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return "";
    }

    private String getSimCountryIso(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getSimCountryIso();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getSimOperator(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getSimOperator();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getSimOperatorName(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getSimOperatorName();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getVoiceMailNumber(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            return tm.getVoiceMailNumber();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private boolean isNetworkRoaming(Context mContext) {
        try {
            TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.isNetworkRoaming();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    private String getBSSID(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return wifiInfo.getBSSID();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @SuppressLint("HardwareIds")
    private String getMacAddress(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return (wifiInfo.getMacAddress() != null) ? wifiInfo.getMacAddress() : "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private boolean getHiddenSSID(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return wifiInfo.getHiddenSSID();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private String getIpAddress(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return Formatter.formatIpAddress(wifiInfo.getIpAddress());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private int getLinkSpeed(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return wifiInfo.getLinkSpeed();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private int getNetworkId(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return wifiInfo.getNetworkId();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private String getSSID(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return wifiInfo.getSSID();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private int getNetworkSignal(Context mContext) {
        try {
            WifiManager wm = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wm.getConnectionInfo();
            return WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 5);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @SuppressLint("HardwareIds")
    private String getBluetoothAddress(Context mContext) {
        try {
            BluetoothManager wm = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
            if (null != wm.getAdapter() && null != wm.getAdapter().getAddress())
                return wm.getAdapter().getAddress();
            else
                return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getBuletoothName(Context mContext) {
        try {
            BluetoothManager wm = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
            if (null != wm.getAdapter() && null != wm.getAdapter().getName())
                return wm.getAdapter().getName();
            else
                return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    @SuppressLint("HardwareIds")
    private String getMsidNew(Context mContext, int slot) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                try {
                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return "";
                    }
                    Method getSubId = TelephonyManager.class.getMethod("getSubscriberId", int.class);
                    SubscriptionManager sm = (SubscriptionManager) mContext.getSystemService(TELEPHONY_SUBSCRIPTION_SERVICE);
                    if (sm != null && sm.getActiveSubscriptionInfoCount() >= 2) {
                        TelephonyInfNew telephonyInfo = TelephonyInfNew.getInstance(mContext);
                        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
                        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
                        String msid = "";
                        String msid2 = "";
                        if (isSIM1Ready && slot == 1) {
                            msid = (String) getSubId.invoke(telephonyManager, sm.getActiveSubscriptionInfoForSimSlotIndex(0).getSubscriptionId()); // Sim slot 1 IMSI
                        }
                        if (isSIM2Ready && slot == 2) {
                            msid2 = (String) getSubId.invoke(telephonyManager, sm.getActiveSubscriptionInfoForSimSlotIndex(1).getSubscriptionId()); // Sim slot 1 IMSI
                            Log.d("cccc", msid + "  second");
                        }
                        return msid + "," + msid2;
                    } else {
                        return slot == 1 ? AppUtils.cutNull(telephonyManager.getSubscriberId()) : "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                return AppUtils.cutNull(telephonyManager.getSubscriberId());
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return "";
    }


    private boolean isNetworkConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    private String getConnectedNetworkType(Context mContext) {
        ConnectivityManager connMgr = (ConnectivityManager)
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = networkInfo.isConnected();
        networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isNetworkConn = networkInfo.isConnected();
        if (isWifiConn)
            return "WIFI";
        if (isNetworkConn)
            return "NETWORK DATA";
        return "";
    }

    public DeviceInfo getDeviceInfo() {
        Context mContext = MyApplication.getAppContext();
        DeviceInfo deviceInformationModel = new DeviceInfo();
        deviceInformationModel.setDeviceSoftwareVersion(getDeviceSoftwareVersion(mContext));
        deviceInformationModel.setNetworkCountryIso(getNetworkCountryIso(mContext));
        deviceInformationModel.setNetworkOperator(getNetworkOperator(mContext));
        deviceInformationModel.setNetworkOperatorName(getNetworkOperatorName(mContext));
        deviceInformationModel.setNetworkType(getNetworkType(mContext));
        deviceInformationModel.setPhoneType(getPhoneType(mContext));
        deviceInformationModel.setSIMCountryIso(getSimCountryIso(mContext));
        deviceInformationModel.setSIMOperator(getSimOperator(mContext));
        deviceInformationModel.setSIMOperatorName(getSimOperatorName(mContext));
        deviceInformationModel.setVoiceMailNo(getVoiceMailNumber(mContext));
        deviceInformationModel.setBSSID(getBSSID(mContext));
        deviceInformationModel.setMACAddress(getMacAddress(mContext));
        deviceInformationModel.setIPAddress(getIpAddress(mContext));
        deviceInformationModel.setHiddenSSID(getHiddenSSID(mContext));
        deviceInformationModel.setIsNetworkRoaming(isNetworkRoaming(mContext));
        deviceInformationModel.setLinkSpeed(getLinkSpeed(mContext));
        deviceInformationModel.setNetworkID(getNetworkId(mContext));
        deviceInformationModel.setSSID(getSSID(mContext));
        deviceInformationModel.setNetworkSignal(getNetworkSignal(mContext));
        deviceInformationModel.setBluetoothAddress(getBluetoothAddress(mContext));
        deviceInformationModel.setBluetoothName(getBuletoothName(mContext));
        deviceInformationModel.setConnectedNetworkType(getConnectedNetworkType(mContext));
        deviceInformationModel.setCellid(AppUtils.getDeviceId(mContext));
        deviceInformationModel.setInternetCheckin(isNetworkConnected(mContext));
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                deviceInformationModel.setSimid1(AppUtils.getDualSimId(mContext, 0));
            } else {
                deviceInformationModel.setSimid1(AppUtils.getSimID(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
            deviceInformationModel.setSimid1("123456");
        }
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                deviceInformationModel.setSimid2(AppUtils.getDualSimId(mContext, 1));
            } else {
                deviceInformationModel.setSimid2(AppUtils.getSimID(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
            deviceInformationModel.setSimid2("123456");
        }
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                deviceInformationModel.setMsid1(getMsidNew(mContext, 1));
            } else {
                deviceInformationModel.setMsid1(AppUtils.getMsId(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
            deviceInformationModel.setMsid1("123456");
        }
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                deviceInformationModel.setMsid2(getMsidNew(mContext, 2));
            } else deviceInformationModel.setMsid1(AppUtils.getMsId(mContext));
        } catch (Exception e) {
            e.printStackTrace();
            deviceInformationModel.setMsid2("123456");
        }
        return deviceInformationModel;
    }

}