package com.mart.onestopkitchen.loginuser.loginnewmodel;

import android.app.ActivityManager;
import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.OfflineActivity;

import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;

public class DeviceInfo {
    @SerializedName("BSSID")
    @Expose
    private String bSSID;
    @SerializedName("BluetoothAddress")
    @Expose
    private String bluetoothAddress;
    @SerializedName("BluetoothName")
    @Expose
    private String bluetoothName;
    @SerializedName("Cellid")
    @Expose
    private String cellid;
    @SerializedName("ConnectedNetworkType")
    @Expose
    private String connectedNetworkType;
    @SerializedName("DeviceSoftwareVersion")
    @Expose
    private String deviceSoftwareVersion;
    @SerializedName("HiddenSSID")
    @Expose
    private Boolean hiddenSSID;
    @SerializedName("IPAddress")
    @Expose
    private String iPAddress;
    @SerializedName("LinkSpeed")
    @Expose
    private Integer linkSpeed;
    @SerializedName("MACAddress")
    @Expose
    private String mACAddress;
    @SerializedName("Msid1")
    @Expose
    private String msid1;
    @SerializedName("Msid2")
    @Expose
    private String msid2;
    @SerializedName("NetworkCountryIso")
    @Expose
    private String networkCountryIso;
    @SerializedName("NetworkID")
    @Expose
    private Integer networkID;
    @SerializedName("NetworkOperator")
    @Expose
    private String networkOperator;
    @SerializedName("NetworkOperatorName")
    @Expose
    private String networkOperatorName;
    @SerializedName("NetworkSignal")
    @Expose
    private Integer networkSignal;
    @SerializedName("NetworkType")
    @Expose
    private String networkType;
    @SerializedName("PhoneType")
    @Expose
    private String phoneType;
    @SerializedName("SIMCountryIso")
    @Expose
    private String sIMCountryIso;
    @SerializedName("SIMOperator")
    @Expose
    private String sIMOperator;
    @SerializedName("SIMOperatorName")
    @Expose
    private String sIMOperatorName;
    @SerializedName("SSID")
    @Expose
    private String sSID;
    @SerializedName("Simid1")
    @Expose
    private String simid1;
    @SerializedName("Simid2")
    @Expose
    private String simid2;
    @SerializedName("VoiceMailNo")
    @Expose
    private String voiceMailNo;
    @SerializedName("isNetworkRoaming")
    @Expose
    private Boolean isNetworkRoaming;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("InternetCheckin")
    @Expose
    private Boolean InternetCheckin;

    public Boolean getInternetCheckin() {
        String currentActivity = getCurrentActivityName();
        if (!InternetCheckin) {
            if (!currentActivity.equals(".ui.activity.OfflineActivity")) {
                if (!PreferenceService.getInstance().getInternetChecking()) {
                    PreferenceService.getInstance().setInternetChecking(MyApplication.getAppContext(), true);
                    Intent i = new Intent(MyApplication.getAppContext(), OfflineActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApplication.getAppContext().startActivity(i);
                }
            }
        } else
            PreferenceService.getInstance().setInternetChecking(MyApplication.getAppContext(), false);
        return InternetCheckin;
    }

    public void setInternetCheckin(Boolean internetCheckin) {
        InternetCheckin = internetCheckin;
    }


    public boolean getCheckInterNet() {
        if (!InternetCheckin) {
            Intent i = new Intent(MyApplication.getAppContext(), OfflineActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            MyApplication.getAppContext().startActivity(i);
        }
        return InternetCheckin;
    }

    public String getCurrentActivityName() {
        ActivityManager am = (ActivityManager) MyApplication.getAppContext().getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        String currentActivity = taskInfo.get(0).topActivity.getShortClassName();
        return currentActivity;
    }

    public String getBSSID() {
        return bSSID;
    }

    public void setBSSID(String bSSID) {
        this.bSSID = bSSID;
    }

    public String getBluetoothAddress() {
        return bluetoothAddress;
    }

    public void setBluetoothAddress(String bluetoothAddress) {
        this.bluetoothAddress = bluetoothAddress;
    }

    public String getBluetoothName() {
        return bluetoothName;
    }

    public void setBluetoothName(String bluetoothName) {
        this.bluetoothName = bluetoothName;
    }

    public String getCellid() {
        return cellid;
    }

    public void setCellid(String cellid) {
        this.cellid = cellid;
    }

    public String getConnectedNetworkType() {
        return connectedNetworkType;
    }

    public void setConnectedNetworkType(String connectedNetworkType) {
        this.connectedNetworkType = connectedNetworkType;
    }

    public String getDeviceSoftwareVersion() {
        return deviceSoftwareVersion;
    }

    public void setDeviceSoftwareVersion(String deviceSoftwareVersion) {
        this.deviceSoftwareVersion = deviceSoftwareVersion;
    }

    public Boolean getHiddenSSID() {
        return hiddenSSID;
    }

    public void setHiddenSSID(Boolean hiddenSSID) {
        this.hiddenSSID = hiddenSSID;
    }

    public String getIPAddress() {
        return iPAddress;
    }

    public void setIPAddress(String iPAddress) {
        this.iPAddress = iPAddress;
    }

    public Integer getLinkSpeed() {
        return linkSpeed;
    }

    public void setLinkSpeed(Integer linkSpeed) {
        this.linkSpeed = linkSpeed;
    }

    public String getMACAddress() {
        return mACAddress;
    }

    public void setMACAddress(String mACAddress) {
        this.mACAddress = mACAddress;
    }

    public String getMsid1() {
        return msid1;
    }

    public void setMsid1(String msid1) {
        this.msid1 = msid1;
    }

    public String getMsid2() {
        return msid2;
    }

    public void setMsid2(String msid2) {
        this.msid2 = msid2;
    }

    public String getNetworkCountryIso() {
        return networkCountryIso;
    }

    public void setNetworkCountryIso(String networkCountryIso) {
        this.networkCountryIso = networkCountryIso;
    }

    public Integer getNetworkID() {
        return networkID;
    }

    public void setNetworkID(Integer networkID) {
        this.networkID = networkID;
    }

    public String getNetworkOperator() {
        return networkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        this.networkOperator = networkOperator;
    }

    public String getNetworkOperatorName() {
        return networkOperatorName;
    }

    public void setNetworkOperatorName(String networkOperatorName) {
        this.networkOperatorName = networkOperatorName;
    }

    public Integer getNetworkSignal() {
        return networkSignal;
    }

    public void setNetworkSignal(Integer networkSignal) {
        this.networkSignal = networkSignal;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getSIMCountryIso() {
        return sIMCountryIso;
    }

    public void setSIMCountryIso(String sIMCountryIso) {
        this.sIMCountryIso = sIMCountryIso;
    }

    public String getSIMOperator() {
        return sIMOperator;
    }

    public void setSIMOperator(String sIMOperator) {
        this.sIMOperator = sIMOperator;
    }

    public String getSIMOperatorName() {
        return sIMOperatorName;
    }

    public void setSIMOperatorName(String sIMOperatorName) {
        this.sIMOperatorName = sIMOperatorName;
    }

    public String getSSID() {
        return sSID;
    }

    public void setSSID(String sSID) {
        this.sSID = sSID;
    }

    public String getSimid1() {
        return simid1;
    }

    public void setSimid1(String simid1) {
        this.simid1 = simid1;
    }

    public String getSimid2() {
        return simid2;
    }

    public void setSimid2(String simid2) {
        this.simid2 = simid2;
    }

    public String getVoiceMailNo() {
        return voiceMailNo;
    }

    public void setVoiceMailNo(String voiceMailNo) {
        this.voiceMailNo = voiceMailNo;
    }

    public Boolean getIsNetworkRoaming() {
        return isNetworkRoaming;
    }

    public void setIsNetworkRoaming(Boolean isNetworkRoaming) {
        this.isNetworkRoaming = isNetworkRoaming;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
