package com.mart.onestopkitchen.loginuser.modelnew.error;

import com.google.gson.annotations.SerializedName;

public class RegisterResponseModel {

    @SerializedName("SuccessMessage")
    private String successMessage;

    @SerializedName("ErrorList")
    private String[] ErrorList;
    @SerializedName("StatusCode")
    private int statusCode;

    public String[] getErrorList() {
        return ErrorList;
    }

    public void setErrorList(String[] errorList) {
        ErrorList = errorList;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}