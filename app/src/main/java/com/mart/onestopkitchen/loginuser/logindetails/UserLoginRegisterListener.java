package com.mart.onestopkitchen.loginuser.logindetails;

import com.mart.onestopkitchen.baseview.LoadView;
import com.mart.onestopkitchen.loginuser.modelnew.error.CommonError;
import com.mart.onestopkitchen.loginuser.modelnew.responsemodel.LoginResponseModelNew;
import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;

public interface UserLoginRegisterListener extends LoadView {
    String getUserMobileNumber();

    void otpSuccessFromServer(OtpResponseModel model);

    void errorFromServer(CommonError model, int api);

    void userLoginIn(LoginResponseModelNew modelNew);

    String getUserCountryCode();


    void noAccountFromVmartAndOkDollar();

    String getUserDob();

    String getUserName();

    String getUserMailId();

    String getUserGender();
}
