package com.mart.onestopkitchen.loginuser;

public class DeviceInformationModel {

    private String DeviceSoftwareVersion = "";
    private String NetworkCountryIso = "";
    private String NetworkOperator = "";
    private String NetworkOperatorName = "";
    private String NetworkType = "";
    private String PhoneType = "";
    private String SIMCountryIso = "";
    private String SIMOperator = "";
    private String SIMOperatorName = "";
    private String VoiceMailNo = "";
    private boolean isNetworkRoaming;
    private String BSSID = "";
    private String MACAddress = "";
    private boolean HiddenSSID;
    private String IPAddress = "";
    private int LinkSpeed;
    private int NetworkID;
    private String SSID = "";
    private int NetworkSignal;
    private String BluetoothAddress = "";
    private String BluetoothName = "";
    private String ConnectedNetworkType = "";
    private String CellId = "";
    private String SimID1 = "";
    private String SimID2 = "";
    private String Msid1 = "";
    private String Msid2 = "";

    public String getCellId() {
        return CellId;
    }

    public void setCellId(String cellId) {
        CellId = cellId;
    }

    public String getSimID1() {
        return SimID1;
    }

    public void setSimID1(String simID1) {
        SimID1 = simID1;
    }

    public String getSimID2() {
        return SimID2;
    }

    public void setSimID2(String simID2) {
        SimID2 = simID2;
    }

    public String getMsid1() {
        return Msid1;
    }

    public void setMsid1(String msid1) {
        Msid1 = msid1;
    }

    public String getMsid2() {
        return Msid2;
    }

    public void setMsid2(String msid2) {
        Msid2 = msid2;
    }

    public String getDeviceSoftwareVersion() {
        return DeviceSoftwareVersion;
    }

    public void setDeviceSoftwareVersion(String deviceSoftwareVersion) {
        DeviceSoftwareVersion = deviceSoftwareVersion;
    }

    public String getNetworkCountryIso() {
        return NetworkCountryIso;
    }

    public void setNetworkCountryIso(String networkCountryIso) {
        NetworkCountryIso = networkCountryIso;
    }

    public String getNetworkOperator() {
        return NetworkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        NetworkOperator = networkOperator;
    }

    public String getNetworkOperatorName() {
        return NetworkOperatorName;
    }

    public void setNetworkOperatorName(String networkOperatorName) {
        NetworkOperatorName = networkOperatorName;
    }

    public String getNetworkType() {
        return NetworkType;
    }

    public void setNetworkType(String networkType) {
        NetworkType = networkType;
    }

    public String getPhoneType() {
        return PhoneType;
    }

    public void setPhoneType(String phoneType) {
        PhoneType = phoneType;
    }

    public String getSIMCountryIso() {
        return SIMCountryIso;
    }

    public void setSIMCountryIso(String SIMCountryIso) {
        this.SIMCountryIso = SIMCountryIso;
    }

    public String getSIMOperator() {
        return SIMOperator;
    }

    public void setSIMOperator(String SIMOperator) {
        this.SIMOperator = SIMOperator;
    }

    public String getSIMOperatorName() {
        return SIMOperatorName;
    }

    public void setSIMOperatorName(String SIMOperatorName) {
        this.SIMOperatorName = SIMOperatorName;
    }

    public String getVoiceMailNo() {
        return VoiceMailNo;
    }

    public void setVoiceMailNo(String voiceMailNo) {
        VoiceMailNo = voiceMailNo;
    }

    public boolean isNetworkRoaming() {
        return isNetworkRoaming;
    }

    public void setNetworkRoaming(boolean networkRoaming) {
        isNetworkRoaming = networkRoaming;
    }

    public void setIsNetworkRoaming(boolean isNetworkRoaming) {
        this.isNetworkRoaming = isNetworkRoaming;
    }

    public String getBSSID() {
        return BSSID;
    }

    public void setBSSID(String BSSID) {
        this.BSSID = BSSID;
    }

    public String getMACAddress() {
        return MACAddress;
    }

    public void setMACAddress(String MACAddress) {
        this.MACAddress = MACAddress;
    }

    public boolean isHiddenSSID() {
        return HiddenSSID;
    }

    public void setHiddenSSID(boolean hiddenSSID) {
        HiddenSSID = hiddenSSID;
    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public int getLinkSpeed() {
        return LinkSpeed;
    }

    public void setLinkSpeed(int linkSpeed) {
        LinkSpeed = linkSpeed;
    }

    public int getNetworkID() {
        return NetworkID;
    }

    public void setNetworkID(int networkID) {
        NetworkID = networkID;
    }

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public int getNetworkSignal() {
        return NetworkSignal;
    }

    public void setNetworkSignal(int networkSignal) {
        NetworkSignal = networkSignal;
    }

    public String getBluetoothAddress() {
        return BluetoothAddress;
    }

    public void setBluetoothAddress(String bluetoothAddress) {
        BluetoothAddress = bluetoothAddress;
    }

    public String getBluetoothName() {
        return BluetoothName;
    }

    public void setBluetoothName(String bluetoothName) {
        BluetoothName = bluetoothName;
    }

    public String getConnectedNetworkType() {
        return ConnectedNetworkType;
    }

    public void setConnectedNetworkType(String connectedNetworkType) {
        ConnectedNetworkType = connectedNetworkType;
    }

}
