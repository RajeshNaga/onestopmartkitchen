package com.mart.onestopkitchen.loginuser.loginnew;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.SmsSendAndReadWithoutPermission.AutoReadIncomingSmsReceiver;
import com.mart.onestopkitchen.base64imge.ImageUrlRequestModel;
import com.mart.onestopkitchen.base64imge.ImageUrlResponseModel;
import com.mart.onestopkitchen.baseview.ResponseHandler;
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.loginuser.loginnewmodel.DeviceInfo;
import com.mart.onestopkitchen.loginuser.loginnewmodel.LoginOrRegistrationModel;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;
import com.mart.onestopkitchen.loginuser.modelnew.error.CommonError;
import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;
import com.mart.onestopkitchen.model.simUtils.SimModels;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.ApiBroker;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.fragment.LoginFragment;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.greenrobot.event.EventBus;

public class VerifyUserPresenterNew implements ResponseHandler {
    public boolean isImageApiCalled;
    private VerifyUserListenerNew listener;
    private Context mContext;
    private ApiBroker apiBroker;
    private LoginFragment loginFragment;
    private PreferenceService preferenceService;
    private String imageUrl = "";


    public VerifyUserPresenterNew(VerifyUserListenerNew userLoginRegisterListener, LoginFragment fragment) {
        this.listener = userLoginRegisterListener;
        this.mContext = listener.getActivityContext();
        this.apiBroker = new ApiBroker(this);
        this.loginFragment = fragment;
        preferenceService = PreferenceService.getInstance();
    }

    /* 1 => otp received
     * 2 ==>login and registration
     * 3 => getImageUrl
     * */
    public void apiCall(int whichApi) {
        if (!isImageApiCalled)
            loadingDialog(true);
        switch (whichApi) {
            case 1:
                apiBroker.getOtp(whichApi, getOtpData());
                break;
            case 2:
                apiBroker.userLoginOrRegister(whichApi, getLoginData());
                break;
            case 3:
                if (!cutNull(imageUrl).isEmpty()) {
                    loadingDialog(false);
                    apiCall(2);
                } else
                    apiBroker.getImageUrl(whichApi, getBase64Image());
                break;
            case 4:
                apiBroker.getUserAddress(whichApi);
                break;
        }
    }

    private ImageUrlRequestModel getBase64Image() {
        ImageUrlRequestModel imageUrlRequestModel = new ImageUrlRequestModel();
        String mobileNumber = listener.getUserMobileNumber();
        String code = listener.getUserCountryCode();
        String mobile = "";
        if (code.startsWith("+95")) {
            mobile = "00959" + mobileNumber.substring(2);
        } else mobile = mobileNumber;
        List<String> list = new ArrayList<>(0);
        list.add(listener.getProfilePicture());
        imageUrlRequestModel.setMobileNumber(mobile);
        imageUrlRequestModel.setBase64String(list);
        return imageUrlRequestModel;
    }

    private LoginOrRegistrationModel getLoginData() {
        LoginOrRegistrationModel loginRequestModel = new LoginOrRegistrationModel();
        String opr = "";
        if (!TextUtils.isEmpty(preferenceService.GetPreferenceValue(PreferenceService.OPERATOR_MOBILE_NUMBER))) {
            opr = preferenceService.GetPreferenceValue(PreferenceService.OPERATOR_MOBILE_NUMBER);
            System.out.println("OPR::" + opr);
            validateSimIdAndMsId(opr);
        }
        if (listener.getVisibility()) {
            try {
                loginRequestModel.setFirstname(listener.getUserName());
                loginRequestModel.setLastname(listener.getUserName());
                loginRequestModel.setPassword(cutNull(getPassword()));
                loginRequestModel.setSimid(cutNull(getSimIds()));
                String mobileNumber = listener.getUserMobileNumber();
                String code = listener.getUserCountryCode();
                String mobile = "";
                if (code.startsWith("+95")) {
                    mobile = "00959" + mobileNumber.substring(2);
                } else mobile = code.replace("+", "00") + mobileNumber;
                loginRequestModel.setCountry(code);
                loginRequestModel.setCountryCode(code);
                loginRequestModel.setUsername(cutNull(mobile));
                //  loginRequestModel.setUsername(cutNull(getUserName()));
                loginRequestModel.setEmail(listener.getUserMailId());
                loginRequestModel.setMode("register");
                loginRequestModel.setGender(listener.getUserGender());
                loginRequestModel.setUserLanguage(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE));
                listener.resetLocale(true);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
                Date date = null;
                try {
                    date = format1.parse(listener.getUserDob());
                    String day = (String) DateFormat.format("dd", date); // 20
                    String monthNumber = (String) DateFormat.format("MM", date); // 06
                    String year = (String) DateFormat.format("yyyy", date); // 2013
                    loginRequestModel.setDateOfBirthYear(year);
                    loginRequestModel.setDateOfBirthDay(day);
                    loginRequestModel.setDateofBirthMonth(monthNumber);
                    loginRequestModel.setVersionCode(Api.VersionCode);
                } catch (ParseException e) {
                    e.printStackTrace();

                }
                listener.resetLocale(false);
                List<String> emails = AppUtils.getDefaultEmails();
                //    (listener.getUserMailId().isEmpty() ? "" : listener.getUserMailId() + ",") +
                if (emails.size() > 0)
                    loginRequestModel.setOtherEmail(emails.toString().replace("[", "").replace("]", "") + (AppUtils.getDefaultFacebook().isEmpty() ? "" : "," + AppUtils.getDefaultFacebook()));
                else loginRequestModel.setOtherEmail(AppUtils.getDefaultFacebook());

                /*String mobileNumber = listener.getUserMobileNumber();
                String code = listener.getUserCountryCode();
                String mobile = "";*///ok dollar like 00959966810921    00959966810921

                loginRequestModel.setMobileNumber(mobile);
                loginRequestModel.setDeviceID(AppUtils.getDeviceId(mContext));

                String simId = getSimIds();
                if (simId.length() > 5)
                    loginRequestModel.setPassword(simId.substring(simId.length() - 6));
                else
                    loginRequestModel.setPassword("123456");
                loginRequestModel.setViberNumber(AppUtils.getViberNumber(mContext));
                loginRequestModel.setMaritalStatus(1);
                loginRequestModel.setDisplayAvatar(listener.getDisplayAvatar());
                loginRequestModel.setProfilePictureUrl(imageUrl);
                DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
                deviceInfo.setLatitude(listener.getLat());
                deviceInfo.setLongitude(listener.getLong());
                loginRequestModel.setDeviceInfo(deviceInfo);
                System.out.println("Visble log::" + new Gson().toJson(loginRequestModel));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                String mobileNumber = listener.getUserMobileNumber();
                String code = listener.getUserCountryCode();
                String mobile = "";
                if (code.startsWith("+95")) {
                    mobile = "00959" + mobileNumber.substring(2);
                } else mobile = code.replace("+", "00") + mobileNumber;

                loginRequestModel.setUsername(mobile);
                loginRequestModel.setPassword(cutNull(getPassword()));
                loginRequestModel.setSimid(cutNull(getSimIds()));
                loginRequestModel.setDisplayAvatar(false);
                loginRequestModel.setMaritalStatus(0);
                // loginRequestModel.setSimid(cutNull(getSimIds()));
                loginRequestModel.setMode("login");
               /* String mobile = "";
                String code = listener.getUserCountryCode();
                String mobileNumber = listener.getUserMobileNumber();
                if (code.startsWith("+95")) {
                    mobile = "00959" + mobileNumber.substring(2);
                } else mobile = mobileNumber;
                loginRequestModel.setMobileNumber(mobile);*/
                DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
                deviceInfo.setLatitude(listener.getLat());
                deviceInfo.setLongitude(listener.getLong());
                loginRequestModel.setDeviceInfo(deviceInfo);


                System.out.println("Manual log::" + new Gson().toJson(loginRequestModel));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return loginRequestModel;
    }

    private String getSimIds() {
        try {
            String DualSimValues = "";
            String opr = "";
            // System.out.println("SIM SIZE:::"+     AppUtils.getAllSimInfo(mContext).size());
            if (!TextUtils.isEmpty(preferenceService.GetPreferenceValue(PreferenceService.OPERATOR_MOBILE_NUMBER))) {
                opr = preferenceService.GetPreferenceValue(PreferenceService.OPERATOR_MOBILE_NUMBER);
            }

            if (AppUtils.getAllSimInfo(mContext).size() > 1) {
                for (SimModels simModels : AppUtils.getAllSimInfo(mContext)) {
                    if (simModels != null && !TextUtils.isEmpty(simModels.getSimId()) && !TextUtils.isEmpty(simModels.getOperator().toLowerCase().trim()) && !TextUtils.isEmpty(opr) && simModels.getOperator().toLowerCase().trim().contains(opr.toLowerCase().trim())) {
                        System.out.println(" getSimId v ::" + simModels.getSimId());
                        System.out.println("OPR NAme::" + opr);
                        DualSimValues = simModels.getSimId();
                        break;
                    }
                }
            } else {
                for (SimModels simModels : AppUtils.getAllSimInfo(mContext)) {
                    if (simModels != null && !TextUtils.isEmpty(simModels.getSimId())) {
                        DualSimValues = simModels.getSimId();
                        break;
                    }
                }
            }
            return DualSimValues;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "123456789";
    }

    private String getPassword() {
        try {
            String DualSimValues = "";
            System.out.println("SIM SIZE:::" + AppUtils.getAllSimInfo(mContext).size());
            for (SimModels simModels : AppUtils.getAllSimInfo(mContext)) {
                if (simModels != null && !TextUtils.isEmpty(simModels.getSimId())) {
                    DualSimValues = DualSimValues + simModels.getSimId();
                    break;
                }
            }
            return DualSimValues.substring(DualSimValues.length() - 6);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "123456";
    }

    private String getUserName() {
        String mobile = listener.getUserMobileNumber();
        String countryCode = listener.getUserCountryCode();
        if (countryCode.startsWith("+95"))
            return mobile.substring(2);
        else {
            return mobile;
        }
    }

    /* 1 => otp received
     * 2 ==>login and registration
     * 3 => getImageUrl
     * */
    @Override
    public void onSuccess(Object object, int api) {
        try {

            if (object != null) {
                switch (api) {
                    case 1:
                        loadingDialog(false);
                        otpReceived(object);
                        break;
                    case 2:
                        loadingDialog(false);
                        userLoginOrRegister(object);
                        break;
                    case 3:
                        imageUrl(object);
                        break;
                    case 4:
                        loadingDialog(false);
                        address(object);
                        break;

                }
            } else {
                loadingDialog(false);
                failureAction(mContext.getString(R.string.some_thing_went));
            }
        } catch (Exception e) {
            e.printStackTrace();
            loadingDialog(false);
        }
    }

    private void address(Object object) {
        isImageApiCalled = false;
        BillingAddressResponse addressResponse = (BillingAddressResponse) object;
        listener.addressSucess(addressResponse);
    }

    private void imageUrl(Object object) {
        try {
            ImageUrlResponseModel imageUrlResponseModel = (ImageUrlResponseModel) object;
            if (imageUrlResponseModel.getCode() == 200 && !cutNull(imageUrlResponseModel.getData()).isEmpty()) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                List<String> list = new Gson().fromJson(imageUrlResponseModel.getData(), listType);
                if (list != null && list.size() > 0) {
                    for (String s : list)
                        imageUrl = s;
                    if (!cutNull(imageUrl).isEmpty()) {
                        isImageApiCalled = true;
                        apiCall(2);
                        return;
                    } else failureAction(mContext.getString(R.string.some_thing_went));
                } else failureAction(mContext.getString(R.string.some_thing_went));
            } else failureAction(mContext.getString(R.string.some_thing_went));
            loadingDialog(false);
            isImageApiCalled = false;
        } catch (Exception e) {
            e.printStackTrace();
            failureAction(mContext.getString(R.string.some_thing_went));
            loadingDialog(false);
        }

    }

    private void userLoginOrRegister(Object object) {
        try {
            LoginResponseModel loginOrRegistrationModel = (LoginResponseModel) object;
            if (loginOrRegistrationModel != null)
                if (loginOrRegistrationModel.getStatusCode() == 200) {
                    if (loginOrRegistrationModel.getData() != null && loginOrRegistrationModel.getData().getToken() != null && !cutNull(loginOrRegistrationModel.getData().getToken()).isEmpty()) {
                        preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, loginOrRegistrationModel.getData().getToken());
                        preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
                        EventBus.getDefault().post("1"); // 1 for Logout -: user registered @implement at CategoryNewFragment
                        NetworkUtil.setToken(loginOrRegistrationModel.getData().getToken());
                        listener.userLoginIn(loginOrRegistrationModel);
                        SharedPrefsHelper.getInstance().clearAllData();

                    } else failureAction(mContext.getString(R.string.some_thing_went));
                } else if (loginOrRegistrationModel.getStatusCode() == 400) {
                    listener.noAccountFromVmartAndOkDollar();
                } else {
                    failureAction(mContext.getString(R.string.some_thing_went));
                }
            else failureAction(mContext.getString(R.string.some_thing_went));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void otpReceived(Object object) {
        try {
            loadingDialog(false);
            OtpResponseModel model = (OtpResponseModel) object;
            if (listener != null && model != null && model.getOTP() != null) {
                System.out.println("OTP::" + model.getOTP());
                AutoReadIncomingSmsReceiver.bindData(loginFragment, model.getOTP());
                listener.otpSuccessFromServer(model);
            } else failureAction(mContext.getResources().getString(R.string.please_try_again));
        } catch (Exception e) {
            e.printStackTrace();
            failureAction(mContext.getResources().getString(R.string.please_try_again));
        }
    }

    @Override
    public void onErrorBody(Object object, int api) {
        try {
            loadingDialog(false);
            if (object != null)
                switch (api) {
                    case 1:
                        otpFailure(object);
                        break;
                    case 3:
                        base64Failed(object);
                        break;
                    default:
                        failureAction(mContext.getString(R.string.some_thing_went));
                }
            else failureAction(mContext.getString(R.string.some_thing_went));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void base64Failed(Object object) {
        failureAction(mContext.getString(R.string.please_try_again));
    }

    private void otpFailure(Object object) {
        try {
            CommonError commonError = (CommonError) object;
            if (commonError != null)
                listener.errorFromServer(commonError, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String s) {
        loadingDialog(false);
        failureAction(s);
    }


    private void failureAction(String s) {
        if (listener != null) {
            listener.showError(s);
        }
    }

    private void loadingDialog(boolean status) {
        if (listener != null) {
            if (status)
                listener.showLoading();
            else listener.hideLoading();
        }
    }

    private HashMap<String, String> getOtpData() {

        Log.e("VerifyUser::## ", "---BuildConfig.BUILD_TYPE:: " + BuildConfig.BUILD_TYPE);
        String mobile = "";
        if (listener.getUserCountryCode().startsWith("+95")) {
            mobile = "00959" + listener.getUserMobileNumber().substring(2);
        } else
            mobile = listener.getUserCountryCode().replace("+", "00") + listener.getUserMobileNumber().substring(1);
        HashMap<String, String> map = new HashMap<>();
        map.put("Application", "OneStopKitchen");
        map.put("DestinationNumber", mobile);
        map.put("Operator", preferenceService.GetPreferenceValue(PreferenceService.OPERATOR_MOBILE_NUMBER));
        map.put("AppVersionName", BuildConfig.VERSION_NAME);
        map.put("DeviceTypeId", "10");
        map.put("BuildType", BuildConfig.BUILD_TYPE);
        //  AutoReadIncomingSmsReceiver.bindData(loginFragment, smsBody);
        return map;
    }

    private String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }

    private void validateSimIdAndMsId(String opr) {
        System.out.println("operators::" + opr);
        if (AppUtils.removeNoServiceOperator(AppUtils.getAllSimInfo(mContext), opr).size() > 1) {
            AppUtils.getSimID(mContext);
        }
    }

}
