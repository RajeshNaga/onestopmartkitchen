package com.mart.onestopkitchen.loginuser.loginnewmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginOrRegistrationModel {
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("City")
    @Expose
    private Object city;
    @SerializedName("Country")
    @Expose
    private Object country;
    @SerializedName("DateOfBirthDay")
    @Expose
    private String dateOfBirthDay;
    @SerializedName("DateOfBirthYear")
    @Expose
    private String dateOfBirthYear;
    @SerializedName("DateofBirthMonth")
    @Expose
    private String dateofBirthMonth;
    @SerializedName("DeviceID")
    @Expose
    private String deviceID;
    @SerializedName("ProfilePictureUrl")
    @Expose
    private String profilePictureUrl;
    @SerializedName("ViberNumber")
    @Expose
    private String viberNumber;
    @SerializedName("MaritalStatus")
    @Expose
    private Integer maritalStatus;
    @SerializedName("DisplayAvatar")
    @Expose
    private Boolean displayAvatar;
    @SerializedName("DeviceInfo")
    @Expose
    private DeviceInfo deviceInfo;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Firstname")
    @Expose
    private String firstname;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("Lastname")
    @Expose
    private String lastname;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("OtherEmail")
    @Expose
    private String otherEmail;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("Simid")
    @Expose
    private String simid;
    @SerializedName("State")
    @Expose
    private Object state;
    @SerializedName("Token")
    @Expose
    private Object token;
    @SerializedName("Username")
    @Expose
    private String username;


    @SerializedName("RoomNo")
    @Expose
    private String RoomNo;

    @SerializedName("FloorNo")
    @Expose
    private String FloorNo;

    @SerializedName("HouseNo")
    @Expose
    private String HouseNo;

    @SerializedName("CountryCode")
    @Expose
    private String countryCode;

    @SerializedName("VersionCode")
    @Expose
    private int VersionCode;
    @SerializedName("UserLanguage")
    @Expose
    private String userLanguage;

    public String getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    public int getVersionCode() {
        return VersionCode;
    }

    public void setVersionCode(int versionCode) {
        VersionCode = versionCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRoomNo() {
        return RoomNo;
    }

    public void setRoomNo(String roomNo) {
        RoomNo = roomNo;
    }

    public String getFloorNo() {
        return FloorNo;
    }

    public void setFloorNo(String floorNo) {
        FloorNo = floorNo;
    }

    public String getHouseNo() {
        return HouseNo;
    }

    public void setHouseNo(String houseNo) {
        HouseNo = houseNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public String getDateOfBirthDay() {
        return dateOfBirthDay;
    }

    public void setDateOfBirthDay(String dateOfBirthDay) {
        this.dateOfBirthDay = dateOfBirthDay;
    }

    public String getDateOfBirthYear() {
        return dateOfBirthYear;
    }

    public void setDateOfBirthYear(String dateOfBirthYear) {
        this.dateOfBirthYear = dateOfBirthYear;
    }

    public String getDateofBirthMonth() {
        return dateofBirthMonth;
    }

    public void setDateofBirthMonth(String dateofBirthMonth) {
        this.dateofBirthMonth = dateofBirthMonth;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getViberNumber() {
        return viberNumber;
    }

    public void setViberNumber(String viberNumber) {
        this.viberNumber = viberNumber;
    }

    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Boolean getDisplayAvatar() {
        return displayAvatar;
    }

    public void setDisplayAvatar(Boolean displayAvatar) {
        this.displayAvatar = displayAvatar;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getOtherEmail() {
        return otherEmail;
    }

    public void setOtherEmail(String otherEmail) {
        this.otherEmail = otherEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSimid() {
        return simid;
    }

    public void setSimid(String simid) {
        this.simid = simid;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getToken() {
        return token;
    }

    public void setToken(Object token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
