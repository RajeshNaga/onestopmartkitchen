package com.mart.onestopkitchen.loginuser.modelnew.responsemodel;

import com.google.gson.annotations.SerializedName;

public class LoginResponseModelNew {

    @SerializedName("CountryId")
    private String countryId;

    @SerializedName("Email")
    private String email;

    @SerializedName("StateProvinceId")
    private int stateProvinceId;

    @SerializedName("FirstName")
    private String firstName;

    @SerializedName("Token")
    private String token;

    @SerializedName("City")
    private String city;

    @SerializedName("CustomerId")
    private int customerId;

    @SerializedName("StatusCode")
    private int statusCode;

    @SerializedName("StreetAddress2")
    private String streetAddress2;

    @SerializedName("StreetAddress")
    private String streetAddress;

    @SerializedName("SuccessMessage")
    private String successMessage;

    @SerializedName("ErrorList")
    private String[] errorList;
    @SerializedName("Username")
    private String username;

    @SerializedName("Phone")
    private String phone;

    @SerializedName("LastName")
    private String lastName;

    public String[] getErrorList() {
        return errorList;
    }

    public void setErrorList(String[] errorList) {
        this.errorList = errorList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStateProvinceId() {
        return stateProvinceId;
    }

    public void setStateProvinceId(int stateProvinceId) {
        this.stateProvinceId = stateProvinceId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Object getStreetAddress2() {
        return streetAddress2;
    }


    public Object getStreetAddress() {
        return streetAddress;
    }


    public Object getSuccessMessage() {
        return successMessage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getPhone() {
        return phone;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}