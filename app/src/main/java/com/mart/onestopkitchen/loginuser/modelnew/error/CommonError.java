package com.mart.onestopkitchen.loginuser.modelnew.error;

import com.google.gson.annotations.SerializedName;

public class CommonError {
    @SerializedName("Message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
