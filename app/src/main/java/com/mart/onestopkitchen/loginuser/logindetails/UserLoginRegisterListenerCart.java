package com.mart.onestopkitchen.loginuser.logindetails;

import com.mart.onestopkitchen.baseview.LoadView;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;

public interface UserLoginRegisterListenerCart extends LoadView {
    void loginSuccessFromServer(LoginResponseModel loginOrRegistrationModel);

    void loginFailedFromServer(String string);
}
