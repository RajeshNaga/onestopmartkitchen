package com.mart.onestopkitchen.loginuser;

import com.mart.onestopkitchen.baseview.LoadView;
import com.mart.onestopkitchen.model.LoginResponse;
import com.mart.onestopkitchen.model.okdollar.OKDollarLoginInfo;
import com.mart.onestopkitchen.model.okdollar.model.ProfileModel;
import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;

public interface VerifyUserListener extends LoadView {
    void successFromServer(OtpResponseModel responseModel);

    void registration();

    void noAccountFromVmart();

    void noAccountFromOkDollarAndVMart();

    void userHaveOkDollarAccount(ProfileModel profileModel);

    String getMobileNumber();

    String getSimId();

    String getCountryCode();

    String getUsername();

    String getDob();

    String getEmail();

    String getGender();

    OKDollarLoginInfo getOkDollarApp();

    void userLogin();

    void userLoginFromOkDollar();

    void failedUserLoginFromOkDollar();

    void billingAddress();

    void loginSucess(LoginResponse loginResponse);

}
