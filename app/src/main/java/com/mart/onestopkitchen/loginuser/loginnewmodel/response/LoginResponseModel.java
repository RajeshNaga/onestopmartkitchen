package com.mart.onestopkitchen.loginuser.loginnewmodel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponseModel {

    @SerializedName("Data")
    @Expose
    private Data data;
    @SerializedName("SuccessMessage")
    @Expose
    private String successMessage;
    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("ErrorList")
    @Expose
    private List<Object> errorList = null;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Object> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Object> errorList) {
        this.errorList = errorList;
    }
}
