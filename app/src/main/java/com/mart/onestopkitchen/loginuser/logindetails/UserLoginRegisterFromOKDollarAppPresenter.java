package com.mart.onestopkitchen.loginuser.logindetails;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.text.format.DateFormat;
import android.util.Log;

import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.baseview.ResponseHandler;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.loginuser.loginnewmodel.DeviceInfo;
import com.mart.onestopkitchen.loginuser.loginnewmodel.LoginOrRegistrationModel;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;
import com.mart.onestopkitchen.loginuser.modelnew.error.RegisterResponseModel;
import com.mart.onestopkitchen.model.okdollar.OKDollarLoginInfo;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.ApiBroker;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.Language;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;

import static com.mart.onestopkitchen.utils.AppUtils.getStateName;
import static com.mart.onestopkitchen.utils.AppUtils.getTownShipName;

public class UserLoginRegisterFromOKDollarAppPresenter implements ResponseHandler {
    private ApiBroker apiBroker;
    private PreferenceService preferenceService;
    private OKDollarLoginInfo loginInfo;
//    private boolean isRegisterData = false;
//    private LoginRequestModel loginRequestModel;
    private UserLoginRegisterListenerCart listenerCart;
    private boolean isFirstTime;
    private Context mContext;
    private String userLat = "", userLong = "";
//    private boolean isAlreadyUserRegister;

//    public UserLoginRegisterFromOKDollarAppPresenter(OKDollarLoginInfo loginDetails) {
//        this.apiBroker = new ApiBroker(this);
//        preferenceService = PreferenceService.getInstance();
//        this.loginInfo = loginDetails;
//        apiCall(1);
//        isFirstTime = false;
//        listenerCart = null;
//        mContext = null;
//    }

    public UserLoginRegisterFromOKDollarAppPresenter(UserLoginRegisterListenerCart listener, Context context) {
        this.apiBroker = new ApiBroker(this);
        preferenceService = PreferenceService.getInstance();
        this.loginInfo = null;

        isFirstTime = false;
        this.listenerCart = listener;
        this.mContext = context;
    }

    public void apiData(OKDollarLoginInfo okDollarLoginInfo, String lat, String aLong) {
        this.loginInfo = okDollarLoginInfo;
        this.userLong = aLong;
        this.userLat = lat;
        //this.isAlreadyUserRegister=isAlreadyRegister;
        apiCall(1);
    }

    public void apiCall(int whichApi) {
        loadingDialog(true);
        if (whichApi == 1) {
            apiBroker.userLoginOrRegister(whichApi, getLoginData());
        }
    }

    private void loadingDialog(boolean status) {
        if (listenerCart != null) {
            if (status)
                listenerCart.showLoading();
            else listenerCart.hideLoading();
        }
    }

    @Override
    public void onSuccess(Object object, int api) {
        loadingDialog(false);
        try {
            if (object != null) {
                if (api == 1) {
                    userLogin(object);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private LoginRequestModel getRegistrationData() {
//        isRegisterData = true;
//        loginRequestModel = new LoginRequestModel();
//        loginRequestModel.setFirstname(cutNull(loginInfo.getName()));
//        loginRequestModel.setLastname(cutNull(loginInfo.getName()));
//        String[] email = loginInfo.getEmail().split(",");//venkat@okdollar.com,manikandan@okdollar.com
//        if (email.length > 0)
//            loginRequestModel.setEmail(cutNull(email[0]));
//        String mobile = cutNull(loginInfo.getMobilNumber());
//        if (mobile.startsWith("09")) {
//            mobile = mobile.substring(2);
//            loginRequestModel.setUsername(cutNull(mobile.substring(2)));
//        } else {
//            loginRequestModel.setUsername(cutNull(mobile));
//        }
//
//        if (loginInfo.getGender().equalsIgnoreCase("1"))
//            loginRequestModel.setGender("M");
//        else loginRequestModel.setGender("F");
//        SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
//        Date date = null;
//        try {
//            date = format1.parse(cutNull(loginInfo.getDob()));
//            String day = (String) DateFormat.format("dd", date); // 20
//            String monthNumber = (String) DateFormat.format("MM", date); // 06
//            String year = (String) DateFormat.format("yyyy", date); // 2013
//            loginRequestModel.setDateOfBirthYear(year);
//            loginRequestModel.setDateOfBirthDay(day);
//            loginRequestModel.setDateofBirthMonth(monthNumber);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            loginRequestModel.setDateOfBirthYear("01");
//            loginRequestModel.setDateOfBirthDay("01");
//            loginRequestModel.setDateofBirthMonth("2000");
//        }
//        loginRequestModel.setAddress1(cutNull(loginInfo.getAddress1()));
//        String house = cutNull(loginInfo.getHouseBlockNo()).isEmpty() ? "" : loginInfo.getHouseBlockNo() + ",";
//        String village = cutNull(loginInfo.getVillageName()).isEmpty() ? "" : loginInfo.getVillageName() + ",";
//        String floor = cutNull(loginInfo.getFloorNumber()).isEmpty() ? "" : loginInfo.getFloorNumber() + ",";
//        String roomNumber = cutNull(loginInfo.getRoomNumber()).isEmpty() ? "" : loginInfo.getRoomNumber() + ",";
//        String address2 = cutNull(loginInfo.getAddress2()).isEmpty() ? "" : loginInfo.getAddress2();
//        String address = address2 + "" + house + "" + village + "" + floor + "" + roomNumber;
//        loginRequestModel.setAddress2(address);
//        loginRequestModel.setCity(loginInfo.getTownShip());
//        loginRequestModel.setState(loginInfo.getState());
//        loginRequestModel.setCountry(loginInfo.getCountry());
//        List<String> emails = AppUtils.getDefaultEmails();
//        if (emails.size() > 0)
//            loginRequestModel.setOtherEmail(cutNull(email.toString()).isEmpty() ? "" : email.toString() + loginInfo.getFBEmailId() + "," + emails.toString().replace("[", "").replace("]", ""));
//        else
//            loginRequestModel.setOtherEmail(cutNull(email.toString()).isEmpty() ? "" : email.toString() + loginInfo.getFBEmailId());
//        loginRequestModel.setMobileNumber(loginInfo.getMobilNumber());
//        loginRequestModel.setDeviceID(loginInfo.getDeviceId());
//        loginRequestModel.setSimid(loginInfo.getSimId());
//        loginRequestModel.setPassword(AppUtils.getSimId().substring(AppUtils.getSimId().length() - 6));
//        //  DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
//        //   loginRequestModel.setDeviceInfo(deviceInfo);
//        return loginRequestModel;
//    }


    private void registrationSuccess(Object object) {
        try {
            RegisterResponseModel responseModel = (RegisterResponseModel) object;
            if (responseModel != null) {
                if (responseModel.getStatusCode() == 200) {
                    apiCall(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userLogin(Object object) {
        try {
            LoginResponseModel loginOrRegistrationModel = (LoginResponseModel) object;
            if (loginOrRegistrationModel != null)
                if (loginOrRegistrationModel.getStatusCode() == 200) {
                    if (loginOrRegistrationModel.getData() != null && loginOrRegistrationModel.getData().getToken() != null && !cutNull(loginOrRegistrationModel.getData().getToken()).isEmpty()) {
                        preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, loginOrRegistrationModel.getData().getToken());
                        preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
                        NetworkUtil.setToken(loginOrRegistrationModel.getData().getToken());
                        preferenceService.SetPreferenceValue(PreferenceService.REGISTER_DONE, true);
                        Log.e("UserLoginReg", "-------otpReceived-------");
                        EventBus.getDefault().post("1");
                        if (listenerCart != null)
                            listenerCart.loginSuccessFromServer(loginOrRegistrationModel);
                    } else {
                        if (listenerCart != null)
                            listenerCart.loginFailedFromServer(mContext.getString(R.string.some_thing_went));
                    }
                } else if (/*!isFirstTime &&*/ loginOrRegistrationModel.getStatusCode() == 400) {

                    System.out.println("Registeration::");
                    isFirstTime = true;
                    apiCall(1);
                } else {
                    isFirstTime = false;
                    if (listenerCart != null)
                        listenerCart.loginFailedFromServer(mContext.getString(R.string.some_thing_went));
                }
            else {
                if (listenerCart != null)
                    listenerCart.loginFailedFromServer(mContext.getString(R.string.some_thing_went));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    private LoginOrRegistrationModel getLoginData() {
        LoginOrRegistrationModel loginRequestModel = new LoginOrRegistrationModel();
        //  if (isFirstTime) {
        try {
            loginRequestModel.setFirstname(loginInfo.getName());
            loginRequestModel.setLastname(loginInfo.getName());
            loginRequestModel.setPassword(cutNull(getPassword()));
            loginRequestModel.setSimid(cutNull(getSimIds()));
            loginRequestModel.setUsername(cutNull(loginInfo.getMobilNumber()));
            loginRequestModel.setUserLanguage(cutNull(loginInfo.getRegisterdLang()));
            String[] email = loginInfo.getEmail().split(",");
            if (email.length > 0) {
                loginRequestModel.setEmail(loginInfo.getEmail().isEmpty() ? getUserName() + "@gmail.com" : email[0]);
            } else
                loginRequestModel.setEmail(loginInfo.getEmail().isEmpty() ? getUserName() + "@gmail.com" : loginInfo.getEmail());
            loginRequestModel.setMode("register");

            if (loginInfo.getGender().equalsIgnoreCase("1"))
                loginRequestModel.setGender("M");
            else loginRequestModel.setGender("F");
            resetLocale(true);
            SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
            Date date = null;
            try {
                date = format1.parse(cutNull(loginInfo.getDob()));
                String day = (String) DateFormat.format("dd", date); // 20
                String monthNumber = (String) DateFormat.format("MM", date); // 06
                String year = (String) DateFormat.format("yyyy", date); // 2013
                loginRequestModel.setDateOfBirthYear(year);
                loginRequestModel.setDateOfBirthDay(day);
                loginRequestModel.setDateofBirthMonth(monthNumber);
                loginRequestModel.setVersionCode(Api.VersionCode);
            } catch (ParseException e) {
                e.printStackTrace();
                   /* loginRequestModel.setDateOfBirthYear("01");
                    loginRequestModel.setDateOfBirthDay("01");
                    loginRequestModel.setDateofBirthMonth("2000");*/
            }

            resetLocale(false);
            loginRequestModel.setAddress1(cutNull(/*loginInfo.getTownShip()+","+*/cutNull(loginInfo.getAddress1())));
            //   String house = cutNull(loginInfo.getHouseBlockNo()).isEmpty() ? "" : loginInfo.getHouseBlockNo() + "!";
            //    String village = cutNull(loginInfo.getVillageName()).isEmpty() ? "" : loginInfo.getVillageName() + "!";
            //   String floor = cutNull(loginInfo.getFloorNumber()).isEmpty() ? "" : loginInfo.getFloorNumber() + "!";
            //   String roomNumber = cutNull(loginInfo.getRoomNumber()).isEmpty() ? "" : loginInfo.getRoomNumber() + "!";
            //   String address2 = cutNull(loginInfo.ge    tAddress2()).isEmpty() ? "" : loginInfo.getAddress2()+"!";
            //  String address =g address2 + "" + house + "" + village + "" + floor + "" + roomNumber;
            //  String address =   village  ;
            loginRequestModel.setHouseNo(cutNull(loginInfo.getHouseBlockNo()));
            loginRequestModel.setRoomNo(cutNull(loginInfo.getRoomNumber()));
            loginRequestModel.setFloorNo(cutNull(loginInfo.getFloorNumber()));
            loginRequestModel.setAddress2(cutNull(loginInfo.getAddress2()));
            loginRequestModel.setCity(getTownShipName(mContext, loginInfo.getTownShip()));//KARYOU
            loginRequestModel.setState(getStateName(mContext, loginInfo.getState()));//YANGON
            loginRequestModel.setCountry(loginInfo.getCountry());
            List<String> emails = AppUtils.getDefaultEmails();
            if (emails.size() > 0)
                loginRequestModel.setOtherEmail(cutNull(loginInfo.getFBEmailId()).isEmpty() ? "" : loginInfo.getFBEmailId() + "," + emails.toString().replace("[", "").replace("]", ""));
            else
                loginRequestModel.setOtherEmail(loginInfo.getFBEmailId());
            loginRequestModel.setMobileNumber(loginInfo.getMobilNumber());
            loginRequestModel.setDeviceID(loginInfo.getDeviceId());
            loginRequestModel.setSimid(loginInfo.getSimId());
            loginRequestModel.setMaritalStatus(1);
            loginRequestModel.setDisplayAvatar(true);
            loginRequestModel.setCountryCode(loginInfo.getCountyCode());
            loginRequestModel.setPassword(loginInfo.getSimId().substring(loginInfo.getSimId().length() - 6));
            loginRequestModel.setProfilePictureUrl(loginInfo.getProfilePicture());
            DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
            deviceInfo.setLatitude(userLat);
            deviceInfo.setLongitude(userLong);
            //  if (mContext != null)
            ///      deviceInfo.setViberNumber(AppUtils.getViberNumber(mContext));
            //  else deviceInfo.setViberNumber("");

            loginRequestModel.setDeviceInfo(deviceInfo);
            System.out.println("isFirstTime::" + new Gson().toJson(loginRequestModel));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //modified..
      /*  } else {
            try {

                loginRequestModel.setFirstname(loginInfo.getName());
                loginRequestModel.setUsername(loginInfo.getMobilNumber());
                loginRequestModel.setPassword(cutNull(getPassword()));
                loginRequestModel.setSimid(cutNull(getSimIds()));
                loginRequestModel.setMode("login");
                loginRequestModel.setDisplayAvatar(false);
                loginRequestModel.setMaritalStatus(0);
                loginRequestModel.setMobileNumber(loginInfo.getMobilNumber());
                DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
               *//* deviceInfo.setLatitude("");
                deviceInfo.setLongitude("");*//*
                deviceInfo.setLatitude(userLat);
                deviceInfo.setLongitude(userLong);
                loginRequestModel.setDeviceInfo(deviceInfo);
                System.out.println("isFirstTime1111::" + new Gson().toJson(loginRequestModel));
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        // }
        return loginRequestModel;

    }

    private void resetLocale(boolean b) {
        Locale myLocale;
        if (b)
            myLocale = new Locale(Language.ENGLISH);
        else
            myLocale = new Locale(preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE));
        Locale.setDefault(myLocale);
        Configuration config = new Configuration(MyApplication.getAppContext().getResources().getConfiguration());
        config.setLocale(myLocale);
        MyApplication.getAppContext().getResources().updateConfiguration(config, MyApplication.getAppContext().getResources().getDisplayMetrics());

    }

    private String getUserName() {
        String mobile = loginInfo.getMobilNumber(); //00959766378289
        if (mobile.startsWith("0095"))
            return mobile.replace("00959", "");
        else {
            return mobile;
        }
    }

    private String getMobileNumber() {
        String mobile = loginInfo.getMobilNumber();
        if (mobile.startsWith("09"))
            return mobile.substring(2);
        else {
            return mobile;
        }
    }

    private String getSimIds() {
        try {

            return loginInfo.getSimId();
        } catch (Exception e) {
            e.printStackTrace();
            return "1234567";
        }
    }

    private String getPassword() {
        try {

            return loginInfo.getSimId().substring(loginInfo.getSimId().length() - 6);
        } catch (Exception e) {
            e.printStackTrace();
            return "1234567";
        }
    }

    @Override
    public void onErrorBody(Object s, int api) {
        loadingDialog(false);
        if (listenerCart != null)
            listenerCart.loginFailedFromServer(mContext.getString(R.string.some_thing_went));
    }

    @Override
    public void onFailure(String s) {
        loadingDialog(false);
        if (listenerCart != null)
            listenerCart.loginFailedFromServer(mContext.getString(R.string.some_thing_went));
    }


    private String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }
}
