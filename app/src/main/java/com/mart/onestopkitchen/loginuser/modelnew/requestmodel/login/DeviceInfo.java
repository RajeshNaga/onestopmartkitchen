package com.mart.onestopkitchen.loginuser.modelnew.requestmodel.login;

import com.google.gson.annotations.SerializedName;

public class DeviceInfo {

    @SerializedName("BluetoothAddress")
    private String BluetoothAddress;

    @SerializedName("Simid1")
    private String SimID1;

    @SerializedName("Msid1")
    private String Msid1;

    @SerializedName("Msid2")
    private String Msid2;

    @SerializedName("SIMOperator")
    private String SIMOperator;

    @SerializedName("NetworkOperatorName")
    private String NetworkOperatorName;

    @SerializedName("Cellid")
    private String CellId;

    @SerializedName("NetworkID")
    private int NetworkID;

    @SerializedName("isNetworkRoaming")
    private boolean isNetworkRoaming;

    @SerializedName("NetworkType")
    private String NetworkType;

    @SerializedName("NetworkOperator")
    private String NetworkOperator;

    @SerializedName("NetworkSignal")
    private int NetworkSignal;

    @SerializedName("SSID")
    private String SSID;

    @SerializedName("VoiceMailNo")
    private String VoiceMailNo;

    @SerializedName("DeviceSoftwareVersion")
    private String DeviceSoftwareVersion;

    @SerializedName("HiddenSSID")
    private boolean HiddenSSID;

    @SerializedName("MACAddress")
    private String MACAddress;

    @SerializedName("BSSID")
    private String BSSID;

    @SerializedName("PhoneType")
    private String PhoneType;

    @SerializedName("BluetoothName")
    private String BluetoothName;

    @SerializedName("LinkSpeed")
    private int LinkSpeed;

    @SerializedName("IPAddress")
    private String IPAddress;

    @SerializedName("NetworkCountryIso")
    private String NetworkCountryIso;

    @SerializedName("ConnectedNetworkType")
    private String ConnectedNetworkType;

    @SerializedName("Simid2")
    private String SimID2;

    @SerializedName("SIMOperatorName")
    private String SIMOperatorName;

    @SerializedName("SIMCountryIso")
    private String SIMCountryIso;

    public String getBluetoothAddress() {
        return BluetoothAddress;
    }

    public void setBluetoothAddress(String bluetoothAddress) {
        BluetoothAddress = bluetoothAddress;
    }

    public String getSimID1() {
        return SimID1;
    }

    public void setSimID1(String simID1) {
        SimID1 = simID1;
    }

    public String getMsid1() {
        return Msid1;
    }

    public void setMsid1(String msid1) {
        Msid1 = msid1;
    }

    public String getMsid2() {
        return Msid2;
    }

    public void setMsid2(String msid2) {
        Msid2 = msid2;
    }

    public String getSIMOperator() {
        return SIMOperator;
    }

    public void setSIMOperator(String SIMOperator) {
        this.SIMOperator = SIMOperator;
    }

    public String getNetworkOperatorName() {
        return NetworkOperatorName;
    }

    public void setNetworkOperatorName(String networkOperatorName) {
        NetworkOperatorName = networkOperatorName;
    }

    public String getCellId() {
        return CellId;
    }

    public void setCellId(String cellId) {
        CellId = cellId;
    }

    public int getNetworkID() {
        return NetworkID;
    }

    public void setNetworkID(int networkID) {
        NetworkID = networkID;
    }

    public boolean isNetworkRoaming() {
        return isNetworkRoaming;
    }

    public void setNetworkRoaming(boolean networkRoaming) {
        isNetworkRoaming = networkRoaming;
    }

    public String getNetworkType() {
        return NetworkType;
    }

    public void setNetworkType(String networkType) {
        NetworkType = networkType;
    }

    public String getNetworkOperator() {
        return NetworkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        NetworkOperator = networkOperator;
    }

    public int getNetworkSignal() {
        return NetworkSignal;
    }

    public void setNetworkSignal(int networkSignal) {
        NetworkSignal = networkSignal;
    }

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public String getVoiceMailNo() {
        return VoiceMailNo;
    }

    public void setVoiceMailNo(String voiceMailNo) {
        VoiceMailNo = voiceMailNo;
    }

    public String getDeviceSoftwareVersion() {
        return DeviceSoftwareVersion;
    }

    public void setDeviceSoftwareVersion(String deviceSoftwareVersion) {
        DeviceSoftwareVersion = deviceSoftwareVersion;
    }

    public boolean isHiddenSSID() {
        return HiddenSSID;
    }

    public void setHiddenSSID(boolean hiddenSSID) {
        HiddenSSID = hiddenSSID;
    }

    public String getMACAddress() {
        return MACAddress;
    }

    public void setMACAddress(String MACAddress) {
        this.MACAddress = MACAddress;
    }

    public String getBSSID() {
        return BSSID;
    }

    public void setBSSID(String BSSID) {
        this.BSSID = BSSID;
    }

    public String getPhoneType() {
        return PhoneType;
    }

    public void setPhoneType(String phoneType) {
        PhoneType = phoneType;
    }

    public String getBluetoothName() {
        return BluetoothName;
    }

    public void setBluetoothName(String bluetoothName) {
        BluetoothName = bluetoothName;
    }

    public int getLinkSpeed() {
        return LinkSpeed;
    }

    public void setLinkSpeed(int linkSpeed) {
        LinkSpeed = linkSpeed;
    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public String getNetworkCountryIso() {
        return NetworkCountryIso;
    }

    public void setNetworkCountryIso(String networkCountryIso) {
        NetworkCountryIso = networkCountryIso;
    }

    public String getConnectedNetworkType() {
        return ConnectedNetworkType;
    }

    public void setConnectedNetworkType(String connectedNetworkType) {
        ConnectedNetworkType = connectedNetworkType;
    }

    public String getSimID2() {
        return SimID2;
    }

    public void setSimID2(String simID2) {
        SimID2 = simID2;
    }

    public String getSIMOperatorName() {
        return SIMOperatorName;
    }

    public void setSIMOperatorName(String SIMOperatorName) {
        this.SIMOperatorName = SIMOperatorName;
    }

    public String getSIMCountryIso() {
        return SIMCountryIso;
    }

    public void setSIMCountryIso(String SIMCountryIso) {
        this.SIMCountryIso = SIMCountryIso;
    }
}