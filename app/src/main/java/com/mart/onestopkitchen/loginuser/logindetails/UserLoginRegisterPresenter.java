//package com.mart.onestopkitchen.loginuser.logindetails;
//
//import android.content.Context;
//import android.text.format.DateFormat;
//
//import com.mart.onestopkitchen.R;
//import com.mart.onestopkitchen.baseview.ResponseHandler;
//import com.mart.onestopkitchen.loginuser.modelnew.error.CommonError;
//import com.mart.onestopkitchen.loginuser.modelnew.error.RegisterResponseModel;
//import com.mart.onestopkitchen.loginuser.modelnew.requestmodel.login.LoginRequestModel;
//import com.mart.onestopkitchen.loginuser.modelnew.responsemodel.LoginResponseModelNew;
//import com.mart.onestopkitchen.model.okdollar.model.ProfileModel;
//import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;
//import com.mart.onestopkitchen.networking.ApiBroker;
//import com.mart.onestopkitchen.networking.NetworkUtil;
//import com.mart.onestopkitchen.service.PreferenceService;
//import com.mart.onestopkitchen.ui.fragment.LoginFragment;
//import com.mart.onestopkitchen.utils.AppUtils;
//import com.mart.onestopkitchen.utils.Keys;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//
//public class UserLoginRegisterPresenter implements ResponseHandler {
//    private UserLoginRegisterListener listener;
//    private Context mContext;
//    private ApiBroker apiBroker;
//    private LoginFragment loginFragment;
//    private LoginRequestModel loginRequestModel;
//    private PreferenceService preferenceService;
//    private boolean isOkDollarUser = false;
//    private boolean firstTime = false;
//    private boolean firstTimeNormalLogin = false;
//
//    public UserLoginRegisterPresenter(UserLoginRegisterListener userLoginRegisterListener, LoginFragment fragment) {
//        this.listener = userLoginRegisterListener;
//        this.mContext = listener.getActivityContext();
//        this.apiBroker = new ApiBroker(this);
//        this.loginFragment = fragment;
//        preferenceService = PreferenceService.getInstance();
//    }
//
//    /*
//     *
//     * 1 getOtp otp
//     * 2 login api
//     * 3 ok dollar server login
//     *
//     * */
//    public void apiCall(int whichApi) {
//        loadingDialog(true);
//        switch (whichApi) {
//            case 1:
////                apiBroker.getOtp(whichApi, getOtpData());
//                break;
//            case 2:
//                apiBroker.userLogin(whichApi, getLoginData());
//                break;
//            case 3:
//                apiBroker.getProfile(whichApi, getProfileData());
//                break;
//            case 4:
//                apiBroker.userRegister(whichApi, getRegistrationData());
//                break;
//        }
//    }
//
//    @Override
//    public void onSuccess(Object object, int api) {
//        try {
//            loadingDialog(false);
//            if (object != null) {
//                switch (api) {
//                    case 1:
//                        otpReceived(object);
//                        break;
//                    case 2:
//                        userLogin(object);
//                        break;
//                    case 3:
//                        okDollarLogin(object);
//                        break;
//                    case 4:
//                        registrationSuccess(object);
//                        break;
//                }
//            } else failureAction(mContext.getString(R.string.some_thing_went));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void registrationSuccess(Object object) {
//        try {
//            RegisterResponseModel responseModel = (RegisterResponseModel) object;
//            if (responseModel != null) {
//                if (responseModel.getStatusCode() == 200) {
//                    apiCall(2);
//                } else if (responseModel.getStatusCode() == 400) {
//                    //listener.noAccountFromVmartAndOkDollar();
//                    failureAction(mContext.getString(R.string.some_thing_went));
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            failureAction(mContext.getString(R.string.some_thing_went));
//        }
//    }
//
//    private LoginRequestModel getRegistrationData() {
//        if (isOkDollarUser)
//            return loginRequestModel;
//        LoginRequestModel loginRequestModel = new LoginRequestModel();
//        loginRequestModel.setFirstname(listener.getUserName());
//        loginRequestModel.setLastname(listener.getUserName());
//        loginRequestModel.setEmail(listener.getUserMailId().isEmpty() ? "12345@gmail.com" : listener.getUserMailId());
//        String mobile = listener.getUserMobileNumber();
//        if (mobile.startsWith("09"))
//            mobile = mobile.substring(2);
//        else {
//            mobile = listener.getUserMobileNumber();
//        }
//        loginRequestModel.setUsername(mobile);
//        loginRequestModel.setGender(listener.getUserGender());
//
//        SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
//        Date date = null;
//        try {
//            date = format1.parse(listener.getUserDob());
//            String day = (String) DateFormat.format("dd", date); // 20
//            String monthNumber = (String) DateFormat.format("MM", date); // 06
//            String year = (String) DateFormat.format("yyyy", date); // 2013
//            loginRequestModel.setDateOfBirthYear(year);
//            loginRequestModel.setDateOfBirthDay(day);
//            loginRequestModel.setDateofBirthMonth(monthNumber);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            loginRequestModel.setDateOfBirthYear("01");
//            loginRequestModel.setDateOfBirthDay("01");
//            loginRequestModel.setDateofBirthMonth("2000");
//        }
//       /* loginRequestModel.setAddress1(profileModel.getAddress1());
//        loginRequestModel.setAddress2(profileModel.getAddress2());
//        loginRequestModel.setCity(profileModel.getTownship());
//        loginRequestModel.setState(profileModel.getState());
//        loginRequestModel.setCountry(profileModel.getCountry());*/
//        List<String> emails = AppUtils.getDefaultEmails();
//        if (emails.size() > 0)
//            loginRequestModel.setOtherEmail((listener.getUserMailId().isEmpty() ? "" : listener.getUserMailId() + ",") + emails.toString().replace("[", "").replace("]", "") + (AppUtils.getDefaultFacebook().isEmpty() ? "" : "," + AppUtils.getDefaultFacebook()));
//        else loginRequestModel.setOtherEmail(AppUtils.getDefaultFacebook());
//        loginRequestModel.setMobileNumber(mobile);
//        loginRequestModel.setDeviceID(AppUtils.getDeviceId(mContext));
//        loginRequestModel.setSimid(cutNull(AppUtils.getSimId()));
//        loginRequestModel.setPassword(AppUtils.getSimId().substring(AppUtils.getSimId().length() - 6));
//        //DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
//      //  loginRequestModel.setDeviceInfo(deviceInfo);
//        return loginRequestModel;
//    }
//
//    private void okDollarLogin(Object object) {
//        try {
//            ProfileModel profileModel = new ProfileModel();
//            profileModel = profileModel.getDataIntoModel(object.toString(), mContext);
//            if (profileModel != null &&
//                    !cutNull(profileModel.getMobileNumber()).trim().isEmpty()
//                    && !cutNull(profileModel.getSimID()).trim().isEmpty()) {
//                isOkDollarUser = true;
//                loginRequestModel = new LoginRequestModel();
//                loginRequestModel.setFirstname(profileModel.getName());
//                loginRequestModel.setLastname(profileModel.getName());
//                try {
//                    String[] emailss = new String[0];
//                    if (!cutNull(profileModel.getEmailID()).isEmpty()) {
//                        emailss = profileModel.getEmailID().split(",");
//                    }
//                    loginRequestModel.setEmail(cutNull(profileModel.getEmailID()).isEmpty() ? "12345@gmail.com" : emailss.length > 0 ? emailss[0] : "");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    loginRequestModel.setEmail("12345@gmail.com");
//                }
//                String mobile = loginRequestModel.getMobileNumber();
//                if (mobile.startsWith("0095")) {
//                    loginRequestModel.setUsername(mobile.replace("0095", ""));
//                } else {
//                    loginRequestModel.setUsername(mobile);
//                }
//                loginRequestModel.setUsername(profileModel.getMobileNumber());
//                if (profileModel.getGender())
//                    loginRequestModel.setGender("M");
//                else loginRequestModel.setGender("F");
//                SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
//                Date date = null;
//                try {
//                    date = format1.parse(profileModel.getDateOfBirth());
//                    String day = (String) DateFormat.format("dd", date); // 20
//                    String monthNumber = (String) DateFormat.format("MM", date); // 06
//                    String year = (String) DateFormat.format("yyyy", date); // 2013
//                    loginRequestModel.setDateOfBirthYear(year);
//                    loginRequestModel.setDateOfBirthDay(day);
//                    loginRequestModel.setDateofBirthMonth(monthNumber);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                    loginRequestModel.setDateOfBirthYear("01");
//                    loginRequestModel.setDateOfBirthDay("01");
//                    loginRequestModel.setDateofBirthMonth("2000");
//                }
//                loginRequestModel.setAddress1(profileModel.getAddress1());
//                loginRequestModel.setAddress2(profileModel.getAddress2());
//                loginRequestModel.setCity(profileModel.getTownship());
//                loginRequestModel.setState(profileModel.getState());
//                loginRequestModel.setCountry(profileModel.getCountry());
//                List<String> emails = AppUtils.getDefaultEmails();
//                if (emails.size() > 0)
//                    loginRequestModel.setOtherEmail(profileModel.getFBEmailId() + "," + emails.toString().replace("[", "").replace("]", ""));
//                else loginRequestModel.setOtherEmail(profileModel.getFBEmailId());
//                loginRequestModel.setMobileNumber(profileModel.getMobileNumber());
//                loginRequestModel.setDeviceID(profileModel.getDeviceId());
//                loginRequestModel.setSimid(profileModel.getSimID());
//                String password;
//                if (profileModel.getSimID().length() > 5)
//                    password = profileModel.getSimID().substring(profileModel.getSimID().length() - 6);
//                else password = AppUtils.getSimId().substring(AppUtils.getSimId().length() - 6);
//                loginRequestModel.setPassword(password);
//                //DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
//            //    loginRequestModel.setDeviceInfo(deviceInfo);
//                apiCall(4);
//            } else {
//                listener.noAccountFromVmartAndOkDollar();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            failureAction(mContext.getString(R.string.some_thing_went));
//        }
//    }
//
//    private HashMap<String, String> getProfileData() {
//        HashMap<String, String> query = new HashMap<>();
//        String mobile = listener.getUserMobileNumber();
//        if (mobile.startsWith("09"))
//            mobile = "0095" + mobile.substring(1);
//        else {
//            mobile = listener.getUserCountryCode().replace("+", "00") + listener.getUserMobileNumber();
//        }
//        query.put("mobilenumber", mobile);
//        query.put("simid", AppUtils.getSimId());
//        return query;
//    }
//
//    private LoginRequestModel getLoginData() {
//        LoginRequestModel loginRequestModel = new LoginRequestModel();
//        try {
//            loginRequestModel.setUsername(getUserName());
//            loginRequestModel.setPassword(cutNull(getPassword()));
//            loginRequestModel.setSimid(cutNull(getSimIds()));
//            //DeviceInfo deviceInfo = new DeviceInformation().getDeviceInfo();
//          //  loginRequestModel.setDeviceInfo(deviceInfo);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return loginRequestModel;
//    }
//
//    private String getUserName() {
//        if (isOkDollarUser) {
//            String mobile = loginRequestModel.getUsername();
//            if (mobile.startsWith("0095")) {
//                return mobile.replace("0095", "");
//            } else {
//                return mobile;
//            }
//        }
//        String mobile = listener.getUserMobileNumber();
//        if (mobile.startsWith("09"))
//            return mobile.substring(2);
//        else {
//            return mobile;
//        }
//    }
//
//    private Object getSimIds() {
//        if (isOkDollarUser)
//            return loginRequestModel.getSimid();
//        return AppUtils.getSimId();
//    }
//
//    private String getPassword() {
//        if (isOkDollarUser)
//            return loginRequestModel.getPassword();
//        return AppUtils.getSimId().substring(AppUtils.getSimId().length() - 6);
//    }
//
//
//    private void userLogin(Object object) {
//        try {
//            LoginResponseModelNew modelNew = (LoginResponseModelNew) object;
//            if (modelNew != null)
//                if (modelNew.getStatusCode() == 200) {
//                    if (modelNew.getToken() != null) {
//                        preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, modelNew.getToken());
//                        preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
//                        NetworkUtil.setToken(modelNew.getToken());
//                    }
//                    listener.userLoginIn(modelNew);
//                } else if (!firstTime && modelNew.getStatusCode() == 400) {
//                    firstTime = true;
//                    apiCall(3);
//                } else if (firstTimeNormalLogin) {
//                    firstTimeNormalLogin = true;
//                    apiCall(2);
//                } else
//                    failureAction(mContext.getString(R.string.some_thing_went));
//
//        } catch (Exception e) {
//            failureAction(mContext.getString(R.string.some_thing_went));
//        }
//
//    }
//
//    private void otpReceived(Object object) {
//        try {
//            loadingDialog(false);
//            OtpResponseModel model = (OtpResponseModel) object;
//            if (listener != null && model != null && model.getStatus() != null)
//                listener.otpSuccessFromServer(model);
//            else failureAction(mContext.getResources().getString(R.string.please_try_again));
//        } catch (Exception e) {
//            e.printStackTrace();
//            failureAction(mContext.getResources().getString(R.string.please_try_again));
//        }
//    }
//
//    @Override
//    public void onErrorBody(Object object, int api) {
//        try {
//            loadingDialog(false);
//            if (object != null)
//                switch (api) {
//                    case 1:
//                        otpFailure(object);
//                        break;
//                }
//            else failureAction(mContext.getString(R.string.some_thing_went));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void otpFailure(Object object) {
//        try {
//            CommonError commonError = (CommonError) object;
//            if (commonError != null)
//                listener.errorFromServer(commonError, 1);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onFailure(String s) {
//        loadingDialog(false);
//        failureAction(s);
//    }
//
//    private void failureAction(String s) {
//        if (listener != null)
//            listener.showError(s);
//    }
//
//    private void loadingDialog(boolean status) {
//        if (listener != null) {
//            if (status)
//                listener.showLoading();
//            else listener.hideLoading();
//        }
//    }
//
//  /*  private HashMap<String, String> getOtpData() {
//        String msg =mContext.getResources().getString(R.string.kitchen_app_name)+"-OTP is-" + AppUtils.randomGeneratorNumber();
//        String smsBody = "<#> ".concat(msg) + " y/ZzOB/+ID6"; //Debug - working
//        HashMap<String, String> map = new HashMap<>();
//        map.put("Message", smsBody);
//        map.put("DestinationNumber", listener.getUserMobileNumber());
//        map.put("Application", "V-Mart");
//        AppUtils.appLog("map", map.toString());
//        AutoReadIncomingSmsReceiver.bindData(loginFragment, smsBody);
//        return map;
//    }*/
//
//    private String cutNull(Object o) {
//        return AppUtils.cutNull(o);
//    }
//}
