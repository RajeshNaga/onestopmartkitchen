package com.mart.onestopkitchen.loginuser.loginnew;

import com.mart.onestopkitchen.baseview.LoadView;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;
import com.mart.onestopkitchen.loginuser.modelnew.error.CommonError;
import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;

public interface VerifyUserListenerNew extends LoadView {
    String getUserMobileNumber();

    void otpSuccessFromServer(OtpResponseModel model);

    void errorFromServer(CommonError model, int api);

    void userLoginIn(LoginResponseModel modelNew);

    String getUserCountryCode();


    void noAccountFromVmartAndOkDollar();

    String getUserDob();

    String getUserName();

    String getUserMailId();

    String getUserGender();

    String getLat();

    String getLong();

    boolean getVisibility();

    String getProfilePicture();

    boolean getDisplayAvatar();

    void addressSucess(BillingAddressResponse addressResponse);

    void resetLocale(boolean b);

/*
    void loadManualSms();
*/
}
