package com.mart.onestopkitchen.loginuser.loginnewmodel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("DateOfBirthDay")
    @Expose
    private Integer dateOfBirthDay;
    @SerializedName("DateofBirthMonth")
    @Expose
    private Integer dateofBirthMonth;
    @SerializedName("DateOfBirthYear")
    @Expose
    private Integer dateOfBirthYear;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("OtherEmail")
    @Expose
    private String otherEmail;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("DeviceID")
    @Expose
    private String deviceID;
    @SerializedName("Simid")
    @Expose
    private String simid;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("ProfilePictureUrl")
    @Expose
    private String profilePictureUrl;
    @SerializedName("ViberNumber")
    @Expose
    private String viberNumber;
    @SerializedName("MaritalStatus")
    @Expose
    private Integer maritalStatus;
    @SerializedName("DisplayAvatar")
    @Expose
    private Boolean displayAvatar;
    @SerializedName("DeviceInfo")
    @Expose
    private Object deviceInfo;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getDateOfBirthDay() {
        return dateOfBirthDay;
    }

    public void setDateOfBirthDay(Integer dateOfBirthDay) {
        this.dateOfBirthDay = dateOfBirthDay;
    }

    public Integer getDateofBirthMonth() {
        return dateofBirthMonth;
    }

    public void setDateofBirthMonth(Integer dateofBirthMonth) {
        this.dateofBirthMonth = dateofBirthMonth;
    }

    public Integer getDateOfBirthYear() {
        return dateOfBirthYear;
    }

    public void setDateOfBirthYear(Integer dateOfBirthYear) {
        this.dateOfBirthYear = dateOfBirthYear;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOtherEmail() {
        return otherEmail;
    }

    public void setOtherEmail(String otherEmail) {
        this.otherEmail = otherEmail;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getSimid() {
        return simid;
    }

    public void setSimid(String simid) {
        this.simid = simid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getViberNumber() {
        return viberNumber;
    }

    public void setViberNumber(String viberNumber) {
        this.viberNumber = viberNumber;
    }

    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Boolean getDisplayAvatar() {
        return displayAvatar;
    }

    public void setDisplayAvatar(Boolean displayAvatar) {
        this.displayAvatar = displayAvatar;
    }

    public Object getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(Object deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}

