//package com.mart.onestopkitchen.loginuser;
//
//import android.content.Context;
//import android.text.format.DateFormat;
//
//import com.mart.onestopkitchen.R;
//import com.mart.onestopkitchen.baseview.ResponseHandler;
//import com.mart.onestopkitchen.loginuser.model.Address;
//import com.mart.onestopkitchen.loginuser.model.RegisterRequestModel;
//import com.mart.onestopkitchen.model.CustomerRegistrationInfo;
//import com.mart.onestopkitchen.model.ForgetResponse;
//import com.mart.onestopkitchen.model.LoginData;
//import com.mart.onestopkitchen.model.LoginResponse;
//import com.mart.onestopkitchen.model.RegistrationResponse;
//import com.mart.onestopkitchen.model.okdollar.OKDollarLoginInfo;
//import com.mart.onestopkitchen.model.okdollar.model.ProfileModel;
//import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;
//import com.mart.onestopkitchen.networking.ApiBroker;
//import com.mart.onestopkitchen.networking.NetworkUtil;
//import com.mart.onestopkitchen.service.PreferenceService;
//import com.mart.onestopkitchen.ui.fragment.LoginFragment;
//import com.mart.onestopkitchen.model.AutoRegisterokDollar;
//import com.mart.onestopkitchen.model.ResponseModelAutoLogin;
//import com.mart.onestopkitchen.utils.AppUtils;
//import com.mart.onestopkitchen.utils.Keys;
//import com.mart.onestopkitchen.utils.sms.SMSReceiverMobile;
//import com.google.gson.Gson;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Date;
//import java.util.HashMap;
//
//import static com.mart.onestopkitchen.service.PreferenceService.USER_MOBILE_NUMBER;
//
//public class  VerifyUserPresenter implements ResponseHandler {
//    private VerifyUserListener userListener;
//    private Context mContext;
//    private LoginFragment loginFragment;
//    private ApiBroker apiBroker;
//    private RegisterRequestModel okDollarUser;
//    private boolean isOkDollarUser = false;
//    private AutoRegisterokDollar dollar;
//    private PreferenceService preferenceService;
//    private String userName = "", password = "";
//
//    public VerifyUserPresenter(VerifyUserListener userListener, LoginFragment loginFragment) {
//        this.userListener = userListener;
//        this.mContext = userListener.getActivityContext();
//        this.apiBroker = new ApiBroker(this);
//        this.loginFragment = loginFragment;
//        preferenceService = PreferenceService.getInstance();
//    }
//
//    /**
//     * 1 ==>otp verify
//     * 2 ==>Register Api
//     * 3 ==>Ok Dollar profile
//     * 4 ==>checkUserAlreadyRegister
//     * 5 ==>through ok dollar app check user available or not
//     */
//
//    public void apiCall(int whichApi) {
//        loadingDialog(true);
//        switch (whichApi) {
//            case 1:
//                apiBroker.getOtp(whichApi, getOtpData());
//                break;
//            case 2:
//                apiBroker.registration(whichApi, getRegistration());
//                break;
//            case 3:
//                apiBroker.getProfile(whichApi, getProfileData());
//                break;
//            case 4:
//                apiBroker.checkUserAlreadyRegister(whichApi, getUserData());
//                break;
//            case 5:
//                apiBroker.checkOkDollarUser(whichApi, getOkDollarAppData());
//                break;
//            case 6:
//                apiBroker.loginApi(whichApi, getMobileNmber());
//                break;
//            case 7:
//                apiBroker.okDollarAutoRegistration(whichApi, dollar);
//                break;
//            case 8:
//                apiBroker.normalRegistrationLocal(whichApi, getNormalRegistration());
//                break;
//            case 9:
//                apiBroker.localServerLogin(whichApi, getLoginDetails());
//                break;
//            case 10:
//                apiBroker.checkLoginApi(whichApi, getUserLogin());
//                break;
//
//        }
//    }
//
//    private LoginData getUserLogin() {
//        return new LoginData(getMobileNumer(), getPassword());
//    }
//
//    private String getPassword() {
//      String password =AppUtils.getSimId().substring(AppUtils.getSimId().length() - 6);
//       if(password!=null && !password.isEmpty()) {
//           preferenceService.SetPreferenceValue(PreferenceService.PASSWORD, password);
//           return password;
//       }
//       return "000000";
//    }
//
//    private String getMobileNumer() {
//        return preferenceService.GetPreferenceValue(USER_MOBILE_NUMBER);
//    }
//
//   /* private HashMap<String, String> getUserLogin() {
//        HashMap<String, String> query = new HashMap<>();
//        String mobile = userListener.getMobileNumber();
//        query.put("UserName", mobile);
//        query.put("Email", "");
//        String s = AppUtils.getSimId().substring(AppUtils.getSimId().length() - 6);
//        query.put("Password", s);
//        return query;
//    }*/
//
//    private LoginData getLoginDetails() {
//        return new LoginData(userName, password);
//    }
//
//    private CustomerRegistrationInfo getNormalRegistration() {
//
//        CustomerRegistrationInfo customerInfo = new CustomerRegistrationInfo();
//        customerInfo.setFirstName(userListener.getUsername());
//        customerInfo.setLastName(userListener.getUsername());
//        customerInfo.setEmail(userListener.getEmail());
//        // String[] getDate = userListener.getDob().split("-");
//        // customerInfo.setDateOfBirthYear(Integer.parseInt(getDate[2]));
//        // customerInfo.setDateOfBirthDay(Integer.parseInt(getDate[0]));
//        // Date date = null;
//        try {
//            SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
//            Date date = format1.parse(userListener.getDob());
//            String day = (String) DateFormat.format("dd", date); // 20
//            String monthNumber = (String) DateFormat.format("MM", date); // 06
//            String year = (String) DateFormat.format("yyyy", date); // 2013
//            customerInfo.setDateOfBirthYear(Integer.parseInt(year));
//            customerInfo.setDateOfBirthDay(Integer.parseInt(day));
//            customerInfo.setDateOfBirthMonth(Integer.parseInt(monthNumber));
//            //  date = new SimpleDateFormat("MMM").parse(getDate[1]);
//            //  Calendar cal = Calendar.getInstance();
//            //   cal.setTime(date);
//            //    customerInfo.setDateOfBirthMonth(cal.get(Calendar.MONTH) + 1);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            customerInfo.setDateOfBirthMonth(0);
//        }
//        customerInfo.setPassword(getPassword());
//        customerInfo.setConfirmPassword(getPassword());
//        customerInfo.setUsername(userListener.getMobileNumber());
//
//
//        userName = userListener.getMobileNumber();
//        password = getPassword();
//        return customerInfo;
//    }
//
//    private HashMap<String, String> getMobileNmber() {
//        HashMap<String, String> query = new HashMap<>();
//        query.put("Email", userListener.getMobileNumber());
//        return query;
//    }
//
//    private RegisterRequestModel getOkDollarAppData() {
//        RegisterRequestModel model = new RegisterRequestModel();
//        OKDollarLoginInfo appInfo = userListener.getOkDollarApp();
//        model.setPhone(appInfo.getMobilNumber());
//        model.setCountryCode(appInfo.getCountyCode());
//        model.setUsername(appInfo.getUserName());
//        if (cutNull(appInfo.getGender()).equals("1"))
//            model.setGender("M");
//        else model.setGender("F");
//        model.setSimId(appInfo.getSimId());
//        model.setDeviceId(appInfo.getDeviceId());
//        ArrayList<String> strings = new ArrayList<>(0);
//        String[] mails = AppUtils.getDefaultEmail().split(",");
//        Collections.addAll(strings, mails);
//        strings.removeAll(Arrays.asList(null, ""));
//        model.setEmail(strings);
//        model.setFaceBook(AppUtils.getDefaultFacebook());
//        Address address = new Address();
//        address.setName(appInfo.getUserName());
//        address.setCountry(appInfo.getCountry());
//        address.setState(appInfo.getState());
//        address.setTownship(appInfo.getTownShip());
//        address.setAddress1(appInfo.getAddress1());
//        address.setAddress2(appInfo.getAddress2());
//        address.setHouseBlockNo(appInfo.getHouseBlockNo());
//        address.setVillageName(appInfo.getVillageName());
//        address.setFloorNumber(appInfo.getFloorNumber());
//        address.setRoomNumber(appInfo.getRoomNumber());
//        model.setAddress(address);
//        return model;
//    }
//
//    private RegisterRequestModel getRegistration() {
//        if (isOkDollarUser)
//            return okDollarUser;
//        else {
//            RegisterRequestModel model = new RegisterRequestModel();
//            model.setPhone(userListener.getMobileNumber());
//            model.setCountryCode(userListener.getCountryCode());
//            model.setUsername(userListener.getUsername());
//            model.setDOB(userListener.getDob());
//            model.setGender(userListener.getGender());
//            model.setSimId(AppUtils.getSimId());
//            model.setDeviceId(AppUtils.getDeviceId(mContext));
//            ArrayList<String> arrayList = new ArrayList<>();
//            arrayList.add(cutNull(userListener.getEmail()));
//            String[] mails = AppUtils.getDefaultEmail().split(",");
//            Collections.addAll(arrayList, mails);
//            arrayList.removeAll(Arrays.asList(null, ""));
//            model.setEmail(arrayList);
//            model.setFaceBook(cutNull(AppUtils.getDefaultFacebook()));
//            model.setAddress(null);
//            return model;
//        }
//
//    }
//
//    private HashMap getUserData() {
//        return null;
//    }
//
//    private HashMap<String, String> getProfileData() {
//        HashMap<String, String> query = new HashMap<>();
//        String mobile = userListener.getMobileNumber();
//        if (mobile.startsWith("09"))
//            mobile = "0095" + mobile.substring(1);
//        else {
//            mobile = userListener.getCountryCode().replace("+", "00") + userListener.getMobileNumber();
//        }
//        query.put("mobilenumber", mobile);
//        query.put("simid", AppUtils.getSimId());
//        return query;
//    }
//
//    private HashMap<String, String> getOtpData() {
//        String msg = mContext.getResources().getString(R.string.zayok_app_name)+"-OTP is-" + AppUtils.randomGeneratorNumber();
//        HashMap<String, String> map = new HashMap<>();
//        map.put("Message", msg);
//        map.put("DestinationNumber", userListener.getMobileNumber());
//        map.put("Application", "V-Mart");
//        AppUtils.appLog("map", map.toString());
//        SMSReceiverMobile.bindData(loginFragment, msg);
//        return map;
//    }
//
//    private void loginApiCall() {
//
//    }
//
//    public void onDestroyView() {
//        userListener = null;
//    }
//
//    private void loadingDialog(boolean status) {
//        if (userListener != null) {
//            if (status)
//                userListener.showLoading();
//            else userListener.hideLoading();
//        }
//    }
//
//    private void failureAction(String s) {
//        loadingDialog(false);
//        if (userListener != null)
//            userListener.showError(s);
//    }
//
//    /**
//     * 1 ==>otp verify
//     * 2 ==>Register Api
//     * 3 ==>Ok Dollar profile
//     * 4 ==>checkUserAlreadyRegister
//     */
//    @Override
//    public void onSuccess(Object o, int api) {
//        try {
//            loadingDialog(false);
//            if (o == null) {
//                failureAction(mContext.getResources().getString(R.string.some_thing_went));
//                return;
//            }
//            switch (api) {
//                case 1:
//                    otpVerified(o);
//                    break;
//                case 4:
//                    userListener.noAccountFromVmart();
//                    break;
//                case 3:
//                    okDollarProfile(o);
//                    break;
//                case 6:
//                    checkUserAlreadyregister(o);
//                    break;
//                case 7:
//                    autoRegister(o);
//                    break;
//                case 8:
//                    normalRegister(o);
//                    break;
//                case 9:
//                    userLogin(o);
//                    break;
//                case 10:
//                    userCheckAlreadyExit(o);
//                    break;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            failureAction(mContext.getResources().getString(R.string.some_thing_went));
//        }
//        // if (o instanceof OtpResponseModel)
//        //     otpVerified(o);
//    }
//
//    private void userCheckAlreadyExit(Object o) {
//        LoginResponse loginResponse = (LoginResponse) o;
//        if (loginResponse.getStatusCode().equalsIgnoreCase("200")) {
//            userListener.loginSucess(loginResponse);
//        } else {
//            apiCall(3);
//        }
//    }
//
//    private void userLogin(Object o) {
//        LoginResponse loginResponse = (LoginResponse) o;
//        if (loginResponse.getToken() != null) {
//            preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, loginResponse.getToken());
//            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
//            NetworkUtil.setToken(loginResponse.getToken());
//        }
//        userListener.billingAddress();
//
//    }
//
//    private void normalRegister(Object o) {
//        RegistrationResponse response = (RegistrationResponse) o;
//
//        if (response == null) {
//            failureAction(mContext.getResources().getString(R.string.some_thing_went));
//            return;
//        }
//        if (response.getStatusCode() == 200) {
//            preferenceService.SetPreferenceValue(PreferenceService.REGISTER_DONE, true);
//            apiCall(9);
//        }
//        if (response.getStatusCode() == 400) {
//            String errors = "";
//            if (response.getErrorList().length > 0) {
//                for (int i = 0; i < response.getErrorList().length; i++) {
//                    errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
//                }
//
//                if (errors.contains("The specified email already exists")) {
//                    preferenceService.SetPreferenceValue(PreferenceService.AUTO_REGISTER_DATA, "Register");
//                    userListener.userLoginFromOkDollar();
//                } else {
//                    preferenceService.SetPreferenceValue(PreferenceService.AUTO_REGISTER_DATA, "");
//                    userListener.failedUserLoginFromOkDollar();
//                }
//            }
//        }
//    }
//
//    private void autoRegister(Object o) {
//        ResponseModelAutoLogin autoLogin = (ResponseModelAutoLogin) o;
//        if (autoLogin == null) {
//            failureAction(mContext.getResources().getString(R.string.some_thing_went));
//            return;
//        }
//          if (autoLogin.getStatusCode() == 200) {
//            userListener.userLoginFromOkDollar();
//            if (autoLogin != null)
//                preferenceService.SetPreferenceValue(PreferenceService.USER_OK_DATA, new Gson().toJson(autoLogin));
//        } else {
//            if (autoLogin.getStatusCode() == 400) {
//                String errors = "";
//                if (autoLogin.getErrorList().length > 0) {
//                    for (int i = 0; i < autoLogin.getErrorList().length; i++) {
//                        errors += "  " + (i + 1) + ": " + autoLogin.getErrorList()[i] + " \n";
//                    }
//                }
//                if (errors.contains("The specified username already exists")) {
//                    preferenceService.SetPreferenceValue(PreferenceService.AUTO_REGISTER_DATA, "Register");
//                    userListener.userLoginFromOkDollar();
//                } else {
//                    preferenceService.SetPreferenceValue(PreferenceService.AUTO_REGISTER_DATA, "");
//                    userListener.failedUserLoginFromOkDollar();
//                }
//            }
//        }
//    }
//
//    private void checkUserAlreadyregister(Object o) {
//        ForgetResponse model = (ForgetResponse) o;
//        if (model != null)
//            if (model.getStatusCode() == 400) {
//                userListener.noAccountFromVmart();
//
//            } else if (model.getStatusCode() == 200)
//                userListener.userLogin();
//
//    }
//
//    private void okDollarProfile(Object o) {
//
//        ProfileModel profileModel = new ProfileModel();
//        profileModel = profileModel.getDataIntoModel(o.toString(), mContext);
//        if (profileModel != null &&
//                !cutNull(profileModel.getMobileNumber()).trim().isEmpty()
//                && !cutNull(profileModel.getSimID()).trim().isEmpty()
//                ) {
//           /* isOkDollarUser = true;
//            okDollarUser = new RegisterRequestModel();
//            okDollarUser.setPhone(profileModel.getMobileNumber());
//            okDollarUser.setCountryCode(profileModel.getCountryCode());
//            okDollarUser.setUsername(profileModel.getName());
//            okDollarUser.setDOB(cutNull(profileModel.getDateOfBirth()));
//            if (profileModel.getGender())
//                okDollarUser.setGender("M");
//            else okDollarUser.setGender("F");
//            okDollarUser.setSimId(profileModel.getSimID());
//            okDollarUser.setDeviceId(profileModel.getDeviceId());
//            ArrayList<String> email = new ArrayList<>(0);
//            if (!cutNull(profileModel.getEmailID()).trim().isEmpty()) {
//                email.add(profileModel.getEmailID());
//            }
//            String[] mails = AppUtils.getDefaultEmail().split(",");
//            Collections.addAll(email, mails);
//            email.removeAll(Arrays.asList(null, ""));
//            okDollarUser.setDeviceId(profileModel.getDeviceId());
//            okDollarUser.setEmail(email);
//            okDollarUser.setFaceBook(cutNull(profileModel.getFBEmailId()).isEmpty() ? AppUtils.getDefaultFacebook() : cutNull(profileModel.getFBEmailId()));
//            Address address = new Address();
//            address.setName(cutNull(profileModel.getName()));
//            address.setCountry(cutNull(profileModel.getCountry()));
//            address.setState(cutNull(profileModel.getState()));
//            address.setTownship(cutNull(profileModel.getTownship()));
//            address.setAddress1(cutNull(profileModel.getAddress1()));
//            address.setAddress2(cutNull(profileModel.getAddress2()));
//            address.setHouseBlockNo(cutNull(profileModel.getHouseBlockNo()));
//            address.setVillageName(cutNull(profileModel.getVillageName()));
//            address.setFloorNumber(cutNull(profileModel.getFloorNumber()));
//            address.setRoomNumber(cutNull(profileModel.getRoomNumber()));
//            okDollarUser.setAddress(address);*/
//            //new code
//            getData(profileModel);
//            // OkDollarRequestModel okDollarRequestModel = new OkDollarRequestModel();
//
//
////            okDollarRequestModel.set
//            //do registration api
//            //apiCall(2);
//        } else userListener.failedUserLoginFromOkDollar();
//        /*if (true)
//                        userListener.userHaveOkDollarAccount(null);
//                    else userListener.noAccountFromOkDollarAndVMart();*/
//
//    }
//
//
//    private AutoRegisterokDollar getData(ProfileModel profileModel) {
//        dollar = new AutoRegisterokDollar();
//        dollar.setFirstname(profileModel.getName());
//        dollar.setLastname(profileModel.getName());
//        String email[] = profileModel.getEmailID().split(",");
//        if (email.length == 1) {
//            dollar.setEmail(profileModel.getEmailID());
//        } else if (email.length > 1) {
//            dollar.setEmail(email[0]);
//        }
//        // dollar.setEmail(okDollarLoginInfo.getEmail());
//        dollar.setUsername(profileModel.getName());
//        if (cutNull(profileModel.getGender()).equals("1"))
//            dollar.setGender("M");
//        else dollar.setGender("F");
//        try {
//            String[] dateOfBirth = profileModel.getDateOfBirth().split("-");
//            dollar.setDateOfBirthDay(Integer.parseInt(dateOfBirth[0]));
//            dollar.setDateOfBirthYear(Integer.parseInt(dateOfBirth[2]));
//            Date date = new SimpleDateFormat("MMM").parse(dateOfBirth[1]);
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(date);
//            dollar.setDateofBirthMonth(cal.get(Calendar.MONTH) + 1);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        dollar.setAddress1(profileModel.getAddress1());
//        dollar.setAddress2(profileModel.getAddress2());
//        dollar.setCity(profileModel.getTownship());
//        dollar.setState(profileModel.getState());
//        dollar.setCountry(profileModel.getCountry());
//        dollar.setOtherEmail(profileModel.getFBEmailId());
//        dollar.setMobileNumber(profileModel.getMobileNumber());
//        String s = profileModel.getSimID().substring(profileModel.getSimID().length() - 6);
//        dollar.setPassword(s);
//        dollar.setDeviceID(profileModel.getDeviceId());
//        dollar.setSimid(profileModel.getSimID());
//        apiCall(7);
//        return dollar;
//    }
//
//    private void otpVerified(Object o) {
//        try {
//            loadingDialog(false);
//            OtpResponseModel model = (OtpResponseModel) o;
//            if (userListener != null && model != null && model.getStatus() != null)
//                userListener.successFromServer(model);
//            else failureAction(mContext.getResources().getString(R.string.please_try_again));
//        } catch (Exception e) {
//            e.printStackTrace();
//            failureAction(mContext.getResources().getString(R.string.please_try_again));
//        }
//
//    }
//
//    /**
//     * 1 ==>otp verify
//     * 2 ==>Register Api
//     * 3 ==>Ok Dollar profile
//     * 4 ==>checkUserAlreadyRegister
//     */
//    @Override
//    public void onErrorBody(Object s, int api) {
//        loadingDialog(false);
//    }
//
//    @Override
//    public void onFailure(String s) {
//        failureAction(s);
//    }
//
//    private String cutNull(Object o) {
//        return AppUtils.cutNull(o);
//    }
//}
