package com.mart.onestopkitchen.SmsSendAndReadWithoutPermission;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.mart.onestopkitchen.listener.OtpListener;
import com.mart.onestopkitchen.service.PreferenceService;


public class AutoReadIncomingSmsReceiver extends BroadcastReceiver {
    private static OtpListener otpListener;
    private static String otpMessage = "";
    public AutoReadSMSListener listener;

    public static void bindData(OtpListener otpListeners, String otpString) {
        otpListener = otpListeners;
        otpMessage = otpString;
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.OTP_MESSAGE, otpMessage);
        Log.e("AutoReomingReceiver", "--### bindData--otpMessage----------: " + otpMessage);

    }

    public void setListener(AutoReadSMSListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent bundle) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(bundle.getAction())) {
            Bundle extras = bundle.getExtras();
            if (extras != null) {
                Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
                if (status != null) {
                    switch (status.getStatusCode()) {
                        case CommonStatusCodes.SUCCESS:
                            String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                            Log.e("AutoRemingReceiver", "--### --onReceive----------: " + message);
                            if (listener != null) {
                                int slotName = extras.getInt("slot", -1);
                                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.SLOT_NAME, slotName);
                                if (otpListener != null) {
                                    if (message != null && message.contains(otpMessage))
                                        otpListener.otpReceived();
                                }
                            }
                            break;
                        case CommonStatusCodes.TIMEOUT:
                            if (listener != null) {
                                listener.onTimeOutReadSms(null);
                            }
                            break;
                    }
                }
            }
        }
    }


    public interface AutoReadSMSListener {
        void onReadSMS(String otp);

        void onTimeOutReadSms(String otp);
    }
}