/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mart.onestopkitchen.camera;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.permission.PermissionListener;
import com.mart.onestopkitchen.permission.PermissionUtils;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mart.onestopkitchen.utils.AppConstants.FROM_SIGN_UP_SELECT_CAPTURE;


/**
 * Activity for the face tracker app.  This app detects faces with the rear facing camera, and draws
 * overlay graphics to indicate the position, size, and ID of each face.
 */
public final class FaceTrackerActivity extends AppCompatActivity implements CameraSource.PictureCallback, AppDialogs.AlertDialogOkClick {
    private static final String TAG = "FaceTracker";

    @BindView(R.id.toolbar1)
    Toolbar toolbar1;
    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    @BindView(R.id.capture)
    TextView capture;
    @BindView(R.id.img_preview)
    ImageView imgPreview;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;

    private ConstraintLayout relativeLayout;
    private CameraSource mCameraSource = null;
    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;

    private boolean isNoSpance = false;
    private File mPhotoFile = null;
    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256


    private FaceDetector.Detections<Face> detectionResults;
    private FaceDetector detector;
    private boolean safeToTakePicture;
    public final static String[] MULTI_PERMISSIONS = new String[]{
            PermissionUtils.Companion.getREAD_PHONE_STATE(),
            PermissionUtils.Companion.getREAD_EXTERNAL_STORAGE(),
            PermissionUtils.Companion.getWRITE_EXTERNAL_STORAGE(), PermissionUtils.Companion.getCAMERA()};
    private PermissionUtils permissionUtils = null;

    public FaceTrackerActivity() {
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_camera_viewer_new);
        ButterKnife.bind(this);
        createCameraSource();
        permissionUtils = PermissionUtils.Companion.getInstance().with(this).setListener(new PermissionListener() {
            @Override
            public void allPermissionGranted() {
                showAlert(getString(R.string.photo_msg), false);
                createCameraSource();
            }

            @Override
            public void onNeverAskAgainPermission(@NotNull String[] string) {
                AppUtils.goToSettings(FaceTrackerActivity.this);

            }

            @Override
            public void onDenied(@NotNull String[] string) {
                permissionUtils.requestPermission(MULTI_PERMISSIONS);
            }
        });
        permissionUtils.requestPermission(MULTI_PERMISSIONS);
        getIntentData();
        setupToolBar();
        //Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(this));

        //  validatePermission();

    }

    public void showAlert(String message, final boolean status) {
        AppDialogs.showAlertFinish(FaceTrackerActivity.this, "splash", false, "", message, R.drawable.app_icon, 1);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onPermissionRequestResult(requestCode, permissions, grantResults);
    }

    private int type;
    private String token = "";
    private String typeId = "depositor";

    private void getIntentData() {
        //Intent intent = getIntent();
        // type = Integer.parseInt(intent.getStringExtra("type"));
        //  token = intent.getStringExtra("token");
        //  if (type == 1111)
        ///     typeId = "withdrawal";

    }

    private Handler taken = new Handler();
    private Runnable takenRunnable = () -> {
        if (mCameraSource != null)
            mCameraSource.takePicture(null, FaceTrackerActivity.this);
    };

    private void takePhtot() {
        if (!safeToTakePicture) {
            capture.setEnabled(false);
            capture.setClickable(false);
            taken.removeCallbacks(takenRunnable);
            taken.postDelayed(takenRunnable, 500);
        }
    }

    private void setupToolBar() {
        toolbar1.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_nav_arrow_back));
        toolbar1.setNavigationOnClickListener(v -> onBackPressed());
    }

    private boolean isCamaradectionFailed = false;

    private void createCameraSource() {
        relativeLayout = findViewById(R.id.photo_view);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCamaradectionFailed) {
                    takePhtot();
                    return;
                }
                if (faceIsThere)
                    takePhtot();
                else
                    AppUtils.showCustomToastMsg(FaceTrackerActivity.this, "", getString(R.string.no_face_detected_more), false);

            }
        });
        mPreview = findViewById(R.id.preview);
        mGraphicOverlay = findViewById(R.id.faceOverlay);
        Context context = getApplicationContext();
        detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!detector.isOperational()) {
            isCamaradectionFailed = true;
        } else isCamaradectionFailed = false;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        mCameraSource = new CameraSource.Builder(context, detector)
                //.setRequestedPreviewSize(width, height)
                .setRequestedPreviewSize(displayMetrics.widthPixels, displayMetrics.heightPixels)
                .setAutoFocusEnabled(true)
                //  .setRequestedPreviewSize(100, 100)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build();

    }


    @Override
    protected void onResume() {
        super.onResume();

        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) mPreview.stop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }


    private void startCameraSource() {
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;//190
        int height = size.y;
        if (mCameraSource != null) {
            try {
                mPreview.setSizes(height, width);
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }

        Log.d("height", height + "" + width);
    }

    @Override
    public void onPictureTaken(byte[] data) {
        safeToTakePicture = true;
        isNoSpance = false;
        mPreview.stop();

        try {
            if (data != null) {
                Bitmap uploadSize = BitmapFactory.decodeByteArray(data, 0, data.length);
                //    Frame frame = new Frame.Builder().setBitmap(uploadSize).build();
                //        SparseArray<Face> faces = detector.detect(frame);

                // restartCamera();

                //     if (faces.size() > 1) {
                //     AppUtils.showCustomToastMsg(FaceTrackerActivity.this, "", getString(R.string.more_face), false);
                //        safeToTakePicture = false;
                // /       retakeView();
                //       return;
                //   }
                //AppDialogs.loadingDialog(FaceTrackerActivity.this, false, "");
                int orientation = DeviceOrientation.getOrientation(data);
                Matrix mtx = new Matrix();
                mtx.postRotate(orientation);
                try {
                   /* if (orientation == 0) {
                        //Bitmap scaledBitmap = Bitmap.createScaledBitmap(uploadSize, 300, 450, true);
                        Bitmap scaledBitmap = Bitmap.createBitmap(uploadSize, 0, 0, uploadSize.getWidth(), uploadSize.getHeight(), mtx, true);
                        // bm = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), mtx, true);
                        try {
                            uploadSize = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), mtx, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                            uploadSize = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), mtx,
                                    true);
                        }
                    } else {
                         uploadSize = Bitmap.createBitmap(uploadSize, 0, 0, uploadSize.getWidth(), uploadSize.getHeight(), mtx, true);
                    }*/
                    uploadSize = Bitmap.createBitmap(uploadSize, 0, 0, uploadSize.getWidth(), uploadSize.getHeight(), mtx, true);

                } catch (Exception e) {
                    e.printStackTrace();
                    // bm = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), mtx, true);
                    uploadSize = Bitmap.createBitmap(uploadSize, 0, 0, uploadSize.getWidth(), uploadSize.getHeight(), mtx, true);

                }
                //  relativeLayout.setVisibility(View.GONE);
                // mCamera.stopPreview();surfaceview.setVisibility(View.GONE);
                //   imgPreview.setVisibility(View.VISIBLE);
                // //   imgPreview.setImageBitmap(uploadSize);

                //set photo
                capture.setEnabled(true);
                capture.setClickable(true);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                uploadSize.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                try {
                    Date currentTime = Calendar.getInstance().getTime();
                    //Log.i("---------currentTime--------",currentTime.toString());
                    mPhotoFile = new File(Environment.getExternalStorageDirectory() + File.separator + currentTime.getTime() + ".jpg");
                    mPhotoFile.createNewFile();
                    FileOutputStream fo = new FileOutputStream(mPhotoFile);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    isNoSpance = true;
                }

                AppDialogs.dismissDialog();
                if (isNoSpance) {
                    Intent i = getIntent();
                    setResult(FROM_SIGN_UP_SELECT_CAPTURE, i);
                    FaceTrackerActivity.this.finish();
                } else {
                    Intent i = getIntent();
                    i.putExtra(Keys.IMAGE_PATH, mPhotoFile.getAbsolutePath());
                    setResult(FROM_SIGN_UP_SELECT_CAPTURE, i);
                    FaceTrackerActivity.this.finish();

                }
                safeToTakePicture = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = getIntent();
        setResult(FROM_SIGN_UP_SELECT_CAPTURE, i);
        finish();
    }

    private void retakeView() {
        imgPreview.setVisibility(View.GONE);
        capture.setClickable(true);
        capture.setEnabled(true);
        try {
            mPreview.start(mCameraSource, mGraphicOverlay);
        } catch (IOException e) {
            e.printStackTrace();
        }
        relativeLayout.setVisibility(View.VISIBLE);
    }


    @Override
    public void clickOk(int fromWhichDialog) {

    }

    @Override
    public Context getActivityContext() {
        return this;
    }


    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */

    private boolean faceIsThere = false;

    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detections, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
            detectionResults = detections;
            faceIsThere = true;
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detections) {
            mOverlay.remove(mFaceGraphic);
            detectionResults.getDetectedItems().clear();
            faceIsThere = false;
        }

        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }
}
