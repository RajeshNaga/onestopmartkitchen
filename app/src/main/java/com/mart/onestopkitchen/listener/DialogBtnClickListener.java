package com.mart.onestopkitchen.listener;

import android.app.Dialog;

public interface DialogBtnClickListener {
    void yesBtnClick(Dialog dialog);

    void cancelBtnClick();
}
