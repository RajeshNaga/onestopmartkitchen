package com.mart.onestopkitchen.listener;

public interface BackPressedListener {
    void onBackPressed();
}
