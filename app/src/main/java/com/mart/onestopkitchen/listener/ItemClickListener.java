package com.mart.onestopkitchen.listener;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}

