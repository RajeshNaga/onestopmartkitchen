package com.mart.onestopkitchen.listener;

import androidx.fragment.app.Fragment;

public interface FragmentListener {

    void addFragmentWithAnimation(Fragment fragment, int enterAnimationResource,
                                  int endAnimationResource, int popEnterAnimationResource,
                                  int popEndAnimationResource, Boolean stacked, Boolean addToBackStack);

    void disableNavigation(boolean disabled);
}
