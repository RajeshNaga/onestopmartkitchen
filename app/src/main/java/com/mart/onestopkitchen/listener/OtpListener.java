package com.mart.onestopkitchen.listener;

public interface OtpListener {
    void otpReceived();

    void otpNotWorkingLowEndDevices();
}
