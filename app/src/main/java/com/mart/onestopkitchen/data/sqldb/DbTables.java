package com.mart.onestopkitchen.data.sqldb;

import com.mart.onestopkitchen.contacts.OneStopKitchenListener;

/**
 *
 */

class DbTables {

    String getContactTb() {
        return "CREATE TABLE IF NOT EXISTS " + OneStopKitchenListener.Contacts.TABLE_NAME + " (" +
                OneStopKitchenListener.Contacts._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                OneStopKitchenListener.Contacts.COLUMN_NAME + ", " +
                OneStopKitchenListener.Contacts.COLUMN_EMAIL_ID + ", " +
                OneStopKitchenListener.Contacts.COLUMN_ID + ", " +
                OneStopKitchenListener.Contacts.COLUMN_PHOTO_URL + ", " +
                OneStopKitchenListener.Contacts.COLUMN_MOBILE_NUMBER + ", " +
                OneStopKitchenListener.Contacts.COLUMN_ISCONTACT_UPLOAED + ", " +
                OneStopKitchenListener.Contacts.COLUMN_VCFURI + ", " +
                OneStopKitchenListener.Contacts.COLUMN_IS_VERIFY + ");";
    }
}
