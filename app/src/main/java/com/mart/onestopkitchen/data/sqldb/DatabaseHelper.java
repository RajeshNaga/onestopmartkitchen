package com.mart.onestopkitchen.data.sqldb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mart.onestopkitchen.contacts.OneStopKitchenListener;

/**
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private DbTables dbTables;

    public DatabaseHelper(Context context) {
        super(context, OneStopKitchenListener.DATABASE_NAME, null, OneStopKitchenListener.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        initTable();
        db.execSQL(dbTables.getContactTb());
        dbTables = null;
    }

    private void initTable() {
        if (null == dbTables)
            dbTables = new DbTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        initTable();
        String DROP_SYNTAX = "DROP TABLE IF EXISTS ";
        dbTables = null;
    }

}