package com.mart.onestopkitchen.data.sqldb;

public interface DBConstant {


    String KEY_VCFURI = "vcfUri";

    String KEY_IS_VERIFY = "IsVerify";
    String KEY_NAME = "name";

    String KEY_EMAIL_ID = "email";
    String KEY_MOBILE_NUMBER = "mobileNumber";

    String KEY_ISCONTACT_UPLOAED = "isContactUpload";
    String COLUMN_PRIMARY_KEY_ID = "_id";


    String COLUMN_ID = "id";

    String COLUMN_PHOTO_URL = "photourl";

    String PHONE = "Phone";


}
