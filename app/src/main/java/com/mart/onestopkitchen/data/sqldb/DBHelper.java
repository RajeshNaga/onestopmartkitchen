package com.mart.onestopkitchen.data.sqldb;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


public class DBHelper implements DBConstant {

    public static DatabaseHelper dm;
    private static SQLiteDatabase db;
    private static DBHelper instance;
    private Context mContext = null;

    public DBHelper(Context c) {
        instance = this;
        if (null != dm) {
            dm.close();
            dm = null;

        }

        if (null != db && db.isOpen())
            db.close();

        dm = new DatabaseHelper(c);
        this.mContext = c;
        open();
    }

    public static DBHelper getDataHelper(Context context) {
        if (null == instance)
            instance = new DBHelper(context);
        return instance;
    }

    private boolean validateDbInstance() {
        if (null != db) {
            if (!db.isOpen() && null != dm) {
                db = dm.getWritableDatabase();
                return true;
            }
            if (db.isOpen()) {
                return true;
            }
        }
        return false;
    }

    private DBHelper open() throws SQLException {
        if (db == null || !db.isOpen()) {
            db = dm.getWritableDatabase();
            dm.setWriteAheadLoggingEnabled(true);
        }
        return this;
    }

    public Cursor rawQuery(String sql, String[] selectionArgs) {
        if (!validateDbInstance())
            return null;
        return db.rawQuery(sql, selectionArgs);
    }


    public int rawDelete(String table, String whereClause, String[] whereArgs) {
        if (!validateDbInstance())
            return -1;
        return db.delete(table, whereClause, whereArgs);
    }

}