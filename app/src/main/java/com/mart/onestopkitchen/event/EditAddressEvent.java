package com.mart.onestopkitchen.event;

import com.mart.onestopkitchen.model.CustomerAddress;

/**
 * Created by mart-110 on 12/17/2015.
 */
public class EditAddressEvent {
    private int index;
    private CustomerAddress address;

    public EditAddressEvent(int index, CustomerAddress address) {
        this.index = index;
        this.address = address;
    }

    public CustomerAddress getAddress() {
        return address;
    }

    public void setAddress(CustomerAddress address) {
        this.address = address;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
