package com.mart.onestopkitchen.event;

/**
 * Created by mart-110 on 12/18/2015.
 */
public class OrderDetailsEvent {
    int id;

    public OrderDetailsEvent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
