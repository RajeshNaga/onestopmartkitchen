package com.mart.onestopkitchen.update

import com.google.gson.annotations.SerializedName
import com.mart.onestopkitchen.networking.BaseResponse

data class AppUpdateModel(

        @SerializedName("apkName")
        val apkName: String? = null,

        @SerializedName("AppURL")
        val appURL: String? = null,

        @SerializedName("IsBackgroundDownload")
        val isBackgroundDownload: Boolean = false,

        @SerializedName("IsUpdateRequired")
        val isUpdateRequired: Boolean = false,

        @SerializedName("message")
        val message: String? = null,

        @SerializedName("IsProduction")
        val isProduction: Boolean = false,

        @SerializedName("IsUpdateForceRequired")
        val isUpdateForceRequired: Boolean = false,

        @SerializedName("AppLatestVersionName")
        val appLatestVersionName: String? = null,

        @SerializedName("AppID")
        val appID: Int? = null,

        @SerializedName("SuccessMessage")
        val successMessage: Any? = null,

        @SerializedName("playStoreUrl")
        val playStoreUrl: String? = null,

        @SerializedName("AppLatestVersionCode")
        val appLatestVersionCode: Int? = null

) : BaseResponse()