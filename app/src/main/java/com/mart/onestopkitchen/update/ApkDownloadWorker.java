package com.mart.onestopkitchen.update;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.mart.onestopkitchen.utils.AppConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class ApkDownloadWorker extends Worker {
    private LiveDataHelper liveDataHelper;

    public ApkDownloadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        liveDataHelper = LiveDataHelper.getInstance();
    }

    @NonNull
    @Override
    public Result doWork() {
        try {

            File apkFilePath = AppConstants.APkFilePath;

            if (!apkFilePath.exists())
                apkFilePath.mkdirs();

            File myFile = new File(apkFilePath, AppConstants.LATEST_APK_NAME);

            //  if (myFile.exists())
            //  myFile.delete();

            URL url = new URL(AppConstants.APK_URL);
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();
            int fileLength = urlConnection.getContentLength();
            FileOutputStream fos = new FileOutputStream(myFile);
            InputStream inputStream = urlConnection.getInputStream();
            byte[] buffer = new byte[1024];
            int len1;
            long total = 0;
            while ((len1 = inputStream.read(buffer)) > 0) {
                total += len1;
                int percentage = (int) ((total * 100) / fileLength);
                liveDataHelper.updateDownloadPer(percentage);
                fos.write(buffer, 0, len1);
            }

            fos.close();
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return Result.failure();
        }
        return Result.success();
    }
}