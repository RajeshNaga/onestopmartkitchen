package com.mart.onestopkitchen.update;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public class LiveDataHelper {
    private LiveDataHelper() {
    }

    private static LiveDataHelper liveDataHelper;
    private MediatorLiveData<Integer> downloadPercent = new MediatorLiveData<>();

    synchronized public static LiveDataHelper getInstance() {
        if (liveDataHelper == null)
            liveDataHelper = new LiveDataHelper();
        return liveDataHelper;
    }

    void updateDownloadPer(int percentage) {
        downloadPercent.postValue(percentage);
    }

    public LiveData<Integer> observePercentage() {
        return downloadPercent;
    }


}