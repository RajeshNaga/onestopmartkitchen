package com.mart.onestopkitchen.update

import android.content.Context
import com.mart.onestopkitchen.baseview.AppUpdateView
import com.mart.onestopkitchen.networking.RetroClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

/**
 * @author AKASH GARG -: 9 JAN-2020.
 * */

class AppUpdatePresenter(val mContext: Context, var listener: AppUpdateView) {
    private var disposableObserver: DisposableObserver<AppUpdateModel>? = null

    init {
        getAppUpdate()
    }

    private fun getAppUpdate() {
        disposableObserver?.let {
            it.dispose().takeIf {
                !disposableObserver!!.isDisposed
            }
        }

        disposableObserver = getAppUpdateObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver())
    }

    private fun getAppUpdateObservable(): Observable<AppUpdateModel> {
        return RetroClient.getApi().appUpdate
    }

    private fun getObserver(): DisposableObserver<AppUpdateModel> {
        return object : DisposableObserver<AppUpdateModel>() {
            override fun onComplete() {
                if (!(disposableObserver!!.isDisposed)) {
                    disposableObserver?.dispose()
                }
            }

            override fun onNext(response: AppUpdateModel) {
                listener.let { listener.appUpdateData(response) }
            }

            override fun onError(e: Throwable) {
                listener.let { listener.error() }
            }
        }
    }
}