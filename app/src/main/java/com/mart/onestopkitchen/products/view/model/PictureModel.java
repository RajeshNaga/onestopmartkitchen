package com.mart.onestopkitchen.products.view.model;

/**
 * Created by Hemnath on 9/22/2018.
 */

public class PictureModel {


    private String imageUrl;

    private Object thumbImageUrl;

    private Object fullSizeImageUrl;

    private Object title;

    private Object alternateText;

    private Object form;

    private CustomProperties customProperties;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Object getThumbImageUrl() {
        return thumbImageUrl;
    }

    public void setThumbImageUrl(Object thumbImageUrl) {
        this.thumbImageUrl = thumbImageUrl;
    }

    public Object getFullSizeImageUrl() {
        return fullSizeImageUrl;
    }

    public void setFullSizeImageUrl(Object fullSizeImageUrl) {
        this.fullSizeImageUrl = fullSizeImageUrl;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public Object getAlternateText() {
        return alternateText;
    }

    public void setAlternateText(Object alternateText) {
        this.alternateText = alternateText;
    }

    public Object getForm() {
        return form;
    }

    public void setForm(Object form) {
        this.form = form;
    }

    public CustomProperties getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }
}
