package com.mart.onestopkitchen.products.view.model;

/**
 * Created by Hemnath on 9/29/2018.
 */

public class Navigation {

    private int id;

    private NavigationCategory navigationCategory;

    public Navigation(int id, NavigationCategory navigationCategory) {
        this.id = id;
        this.navigationCategory = navigationCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NavigationCategory getNavigationCategory() {
        return navigationCategory;
    }

    public void setNavigationCategory(NavigationCategory navigationCategory) {
        this.navigationCategory = navigationCategory;
    }

    public enum NavigationCategory {
        CATEGORY, SUB_CATEGORY, ITEM
    }
}
