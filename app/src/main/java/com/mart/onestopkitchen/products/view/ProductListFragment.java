package com.mart.onestopkitchen.products.view;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.constants.ProductSort;
import com.mart.onestopkitchen.listener.FragmentListener;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.AvailableSortOption;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.SubCategoryProductDetails.SubCategoryProduct;
import com.mart.onestopkitchen.model.filterslection.GroupDataItem;
import com.mart.onestopkitchen.model.filterslection.UserSelectionResponseModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.FilterMainActivityNew;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.UserSelectedList;
import com.mart.onestopkitchen.networking.BaseResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.AddtoCartResponse;
import com.mart.onestopkitchen.networking.response.ProductsResponse;
import com.mart.onestopkitchen.products.view.model.Navigation;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.fragment.BaseFragment;
import com.mart.onestopkitchen.ui.fragment.HomePageFragment;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;
import com.mart.onestopkitchen.utils.AppUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.mart.onestopkitchen.utils.AppConstants.FILTER_OPTION_INTENT;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends BaseFragment implements ProductNavigationProduct.OnLoadDataListener, ProductsFromSubcategoryAdapter.OnItemClickListener, SelectedProductAdapter.OnSubCategoryListener {

    private static final String CATEGORY_ID = "categoryId";

    @BindView(R.id.sub_filter)
    View sortFilterView;
    @BindView(R.id.rv_filter_list)
    RecyclerView listProduct;
    @BindView(R.id.ll_product_list)
    RelativeLayout llProductList;
    @BindView(R.id.rl_sortby)
    RelativeLayout rl_sortby;
    @BindView(R.id.rl_filter)
    RelativeLayout rl_filter;
    @BindView(R.id.bottomsheet)
    BottomSheetLayout bottomSheetLayout;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerFrameLayout;
    @BindView(R.id.grid_view)
    ImageView gridView;
    @BindView(R.id.grid_view_border)
    View gridViewBorder;
    @BindView(R.id.txt_no_record_found)
    TextView noRecordFound;

    private GridLayoutManager layoutManager;
    private ProductsFromSubcategoryAdapter productsFromSubcategoryAdapter;
    private Map<String, String> queryMapping = new HashMap<>();
    protected Unbinder unbinder;
    private DrawerLayout drawer;
    private FragmentListener fragmentListener;
    private ProductNavigationProduct productNavigationProduct;
    private ArrayList<SubCategoryProduct.SubCategory> subCategories;
    private List<Navigation> backContainer;
    private int totalItemCount, lastVisibleItem;
    private boolean isLoading;
    private int categoryId;
    private View view;
    private List<AvailableSortOption> availableSortOptions = new ArrayList<>();
    private ArrayAdapter<String> sortAdapter;
    private int selectedPosition = -1;
    private ProductsResponse productsResponse;
    private String fromWhere;
    private String categoryName;
    private List<ProductModel> productListFromServer;
    private ArrayList<Integer> productIds;
    private LinearLayoutManager mLayoutManager;
    private int visibleThreshold = 5;
    private MyApplication myApplication;
    private int alreadySortClicked = -1;
    private boolean isFilterClicked;
    private int minPriceSel = 0, maxPriceSel;
    private boolean isFilterClickSingleTime;
    private boolean needToCallApi = true;
    private boolean exitForLoop;
    private String titleToolbar = "";
    private boolean isListView;

    public ProductListFragment() {
        // Required empty public constructor
    }

    public static ProductListFragment getInstance(int id, String categoryName, int param) {
        ProductListFragment productListFragment = new ProductListFragment();
        productListFragment.categoryId = id;
        productListFragment.categoryName = categoryName;
        return productListFragment;
    }

    public static ProductListFragment getInstance(int id, String whereFrom, String s) {
        ProductListFragment productListFragment = new ProductListFragment();
        productListFragment.categoryId = id;
        productListFragment.categoryName = s;
        productListFragment.fromWhere = whereFrom;
        return productListFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_product_list, container, false);
        Objects.requireNonNull(getActivity()).setTitle(categoryName);
        unbinder = ButterKnife.bind(this, view);
        listProduct.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        listProduct.setLayoutManager(mLayoutManager);


        filterReset();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (needToCallApi)
            setNavigationDrawer();

    }

    @Override
    public void onStop() {
        super.onStop();
        needToCallApi = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(getActivity()).setTitle(titleToolbar.isEmpty() ? categoryName : titleToolbar);
        //   setNavigationDrawer();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // It may useful in future
       /* MenuItem item = menu.findItem(R.id.menu_search);
        item.setVisible(true);*/
    }

    private void filterReset() {
        myApplication = ((MyApplication) getActivity().getApplicationContext());
        myApplication.alreadySelected = null;
        myApplication.alreadySelectedPrice = "0-0";
        myApplication.rightValInt = 0;
        myApplication.leftValInt = 0;

    }

    private void setNavigationDrawer() {
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            myApplication.toolBarTitle.add(categoryName);
            rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            drawer = view.findViewById(R.id.dl_subcategory);
            drawer.setScrimColor(getResources().getColor(android.R.color.transparent));
            if (AppUtils.cutNull(fromWhere).contains("feature")) {
                callWebServiceFeatureMoreLoad();
            } else
                callWebServiceMoreLoad();
            productNavigationProduct = ProductNavigationProduct.newInstance(categoryId, this);
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.fl_subcategory_menu, productNavigationProduct, "Navigation").commit();

            if (backContainer == null) {
                drawer.openDrawer(GravityCompat.START);
                if (AppUtils.cutNull(fromWhere).equalsIgnoreCase(HomePageFragment.class.getSimpleName()) || AppUtils.cutNull(fromWhere).contains("feature")) {
                    closeDrawer();
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                backContainer = new ArrayList<>();
                backContainer.add(new Navigation(categoryId, Navigation.NavigationCategory.CATEGORY));
            }


            listProduct.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = mLayoutManager.getItemCount();
                    lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        loadData();
                        isLoading = true;
                    }

                }
            });

        }
        drawerListener(drawer);
    }

    private void drawerListener(DrawerLayout drawer) {
        DrawerLayout mDrawer = (DrawerLayout) drawer;
        mDrawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {
                if (bottomSheetLayout != null && bottomSheetLayout.isSheetShowing())
                    bottomSheetLayout.dismissSheet();
            }

            @Override
            public void onDrawerClosed(View view) {
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });
    }

    private void loadData() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*tempCardItem.remove(tempCardItem.size() - 1);
                mAdapter.notifyItemRemoved(tempCardItem.size());
                int Total = cardItem.size();
                int start = tempCardItem.size();
                int end = start + 20;
                int size = (Total > end) ? end : Total;

                for (int i = start; i < size; i++) {
                    tempCardItem.add(cardItem.get(i));
                }
                isLoading = (Total == size);
                mAdapter.refreshAdapter(isLoading,tempCardItem);*/
            }
        }, 20);


    }

    private void callWebServiceFeatureMoreLoad() {
        RetroClient.getApi().getProductListByManufacturerNew(categoryId)
                .enqueue(new CustomCB<ProductsResponse>());
    }

    public void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        }
    }

    /**
     * This method helps to generate subcategories. If subcategories does not have child it will call the products for subcategory
     */
    public void callWebServiceMoreLoad() {
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmer();
        listProduct.setVisibility(View.GONE);
        RetroClient.getApi().getSubcategoryNew(categoryId)
                .enqueue(new CustomCB<SubCategoryProduct>());
    }


    public void onEvent(final SubCategoryProduct response) {
        shimmerFrameLayout.setVisibility(View.GONE);
        shimmerFrameLayout.stopShimmer();
        listProduct.setVisibility(View.VISIBLE);
        SelectedProductAdapter productAdapter = new SelectedProductAdapter(getContext(), response.getSubCategories(), this);
        layoutManager = new GridLayoutManager(getContext(), 2);
        listProduct.setHasFixedSize(true);
        listProduct.setLayoutManager(layoutManager);
        listProduct.setAdapter(productAdapter);
        needToCallApi = true;

        if (response.getSubCategories().size() > 0) {
            subCategories = response.getSubCategories();
            productNavigationProduct.loadSubCategory(subCategories, categoryId);
            if (!checkAlreadyExist())
                backContainer.add(new Navigation(categoryId, Navigation.NavigationCategory.SUB_CATEGORY));
        } else {
            if (subCategories != null) {
                productNavigationProduct.loadSubCategory(subCategories, categoryId);
            }


            if (!checkAlreadyExist())
                backContainer.add(new Navigation(categoryId, Navigation.NavigationCategory.ITEM));
            productNavigationProduct.showSortFilterLayout();
            closeDrawer();
            // It will load the products in main recyclerview when subcategory have no child
            callWebServiceListOfProducts(categoryId);
        }

    }

    private boolean checkAlreadyExist() {
        for (Navigation navigation : backContainer) {
            if (navigation.getId() == categoryId)
                return true;
        }
        return false;
    }

    @OnClick({R.id.rl_sortby, R.id.rl_filter, R.id.grid_view})
    protected void showSortByView(View view) {
        switch (view.getId()) {
            case R.id.rl_sortby:
                if (productsFromSubcategoryAdapter != null && productsFromSubcategoryAdapter.getItemCount() > 0)
                    sortAction();
                else
                    Toast.makeText(getContext(), getString(R.string.no_records), Toast.LENGTH_SHORT).show();
                break;
            case R.id.rl_filter:
                filterAction();
                break;
            case R.id.grid_view:
                gridAndListOption();
                break;
        }

    }

    private void gridAndListOption() {
        if (!isListView) {
            gridView.setImageResource(R.drawable.grid);
            isListView = true;
            listProduct.setLayoutManager(new LinearLayoutManager(getActivity()));
            if (productsFromSubcategoryAdapter != null)
                productsFromSubcategoryAdapter.notifyDataSetChanged();
        } else {
            listProduct.setLayoutManager(layoutManager);
            if (productsFromSubcategoryAdapter != null)
                productsFromSubcategoryAdapter.notifyDataSetChanged();
            gridView.setImageResource(R.drawable.list);
            isListView = false;
        }
    }

    private void filterAction() {
        //  Intent intent = new Intent(getContext(), FilterMainActivity.class);
        if (productsResponse != null && productsResponse.getProducts() != null && productsResponse.getProducts().size() > 0 && !isFilterClickSingleTime) {
            Intent intent = new Intent(getContext(), FilterMainActivityNew.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(CATEGORY_ID, productsResponse);
            intent.putExtras(bundle);
            ((MainActivity) getActivity()).hideKeyboard();
            startActivityForResult(intent, FILTER_OPTION_INTENT);
            isFilterClickSingleTime = true;
          /*  minPriceSel = Integer.parseInt(String.valueOf(productsResponse.getPriceRange().getFrom()).replaceAll("[a-zA-Z(),]", ""));
            maxPriceSel = Integer.parseInt(String.valueOf(productsResponse.getPriceRange().getTo()).replaceAll("[a-zA-Z(),]", ""));
     */
            minPriceSel = (int) Math.round(Double.parseDouble(String.valueOf(productsResponse.getPriceRange().getFrom()).replaceAll("[a-zA-Z(),]", "")));
            maxPriceSel = (int) Math.round(Double.parseDouble(String.valueOf(productsResponse.getPriceRange().getTo()).replaceAll("[a-zA-Z(),]", "")));

        } /*else {
            Toast.makeText(getContext(), getString(R.string.no_records), Toast.LENGTH_LONG).show();}*/
    }

    private void sortAction() {
        if (availableSortOptions != null) {
            LinearLayout sortLinearLayout = (LinearLayout) getLayoutInflater().
                    inflate(R.layout.list_sort_by, bottomSheetLayout, false);
            ListView sortListView = sortLinearLayout.findViewById(R.id.lv_sortby);
            bottomSheetLayout.showWithSheetView(sortLinearLayout);
            sortListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            bottomSheetLayout.setFocusable(true);
            sortAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
                    R.layout.simple_list_item_single_choice, ProductSort.getSortOptionTextList(availableSortOptions));

            sortListView.setAdapter(sortAdapter);
            if (selectedPosition >= 0) {
                sortListView.setItemChecked(selectedPosition, true);
            }

            sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    String value = availableSortOptions.get(position).getValue();
                    selectedPosition = position;
                    alreadySortClicked = position;
                    // queryMapping.put("orderBy", value);
                    productsFromSubcategoryAdapter.loadData(doSortOption(position));
                    bottomSheetLayout.dismissSheet();
                    sortAndFilterColorChanged();
                }
            });
        }
    }


    private List<ProductModel> doSortOption(int value) {
        Log.d("TAG", "" + value);
        switch (value) {
            case 0:
                sortByPosition();
                break;
            case 1:
                sortByAtoZ(true);
                break;
            case 2:
                sortByAtoZ(false);
                break;
            case 3:
                sortByLowToHigh(true);
                break;
            case 4:
                sortByLowToHigh(false);
                break;
            case 5:
                sortByDate();
                break;
        }
        return productsFromSubcategoryAdapter.products;// productsFromSubcategoryAdapter.loadData(productModels);
    }

    private void sortByDate() {
        try {
            Collections.sort(productsFromSubcategoryAdapter.products, new Comparator<ProductModel>() {
                @SuppressLint("SimpleDateFormat")
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null && productModel.getProductPrice() == null)
                        return 0;
                    String sDate1 = productModel.getCreatedOn().replace("T", " ");//2018-02-19T10:35:25
                    String date2 = t1.getCreatedOn().replace("T", " ");
                    java.util.Date date1 = null;
                    Date date2s = null;
                    try {
                        date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(sDate1);
                        date2s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(date2);//2018-11-26T06:44:58.0620544
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (date2s != null && date1 != null) {
                        return date2s.compareTo(date1);
                    } else return 0;
                }
            });
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    private void sortByLowToHigh(final boolean b) {
        try {
            Collections.sort(productsFromSubcategoryAdapter.products, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null && productModel.getProductPrice() == null)
                        return 0;
                    if (b)
                        return Double.compare(productModel.getProductPrice().getPriceValue(),
                                (t1.getProductPrice().getPriceValue()));
                    else return Double.compare(t1.getProductPrice().getPriceValue(),
                            (productModel.getProductPrice().getPriceValue()));
                }
            });
        } catch (Exception ignored) {

        }
    }


    private void sortByAtoZ(final boolean b) {
        try {
            Collections.sort(productsFromSubcategoryAdapter.products, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null)
                        return 0;
                    if (b)
                        return productModel.getName().compareToIgnoreCase(t1.getName());
                    else return t1.getName().compareToIgnoreCase(productModel.getName());
                }
            });
        } catch (Exception ignored) {

        }
    }

    private void sortByPosition() {
        try {
            Collections.sort(productsFromSubcategoryAdapter.products, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null)
                        return 0;
                    return Integer.compare(productModel.getDisplayOrder(),
                            (t1.getDisplayOrder()));
/*
                    return productModel.getDisplayOrder() - t1.getDisplayOrder();
*/
                }
            });
        } catch (Exception ignored) {

        }
    }


    public void callWebService() {
        shimmerFrameLayout.startShimmer();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        listProduct.setVisibility(View.GONE);
        RetroClient.getApi().getProductList(categoryId, queryMapping)
                .enqueue(new CustomCB<ProductsResponse>());
    }

    public void onEvent(final ProductsResponse response) {
        shimmerFrameLayout.setVisibility(View.GONE);
        shimmerFrameLayout.stopShimmer();
        listProduct.setVisibility(View.VISIBLE);
        alreadySortClicked = -1;
        isFilterClicked = false;
        rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.white));

        if (response != null && response.getStatusCode() == 200) {
            //  boolean isPostionAllZero=true;
            //  for (ProductModel model:response.getProducts()){
            //      if (model.getDisplayOrder() != 0) {
            //          isPostionAllZero = false;
            //          break;
            //       }
            //  }
            //  if (isPostionAllZero) {
            for (int i = 0; i < response.getProducts().size(); i++) {
                response.getProducts().get(i).setDisplayOrder(i);
            }
            //  }


            setSelected();
            if (productIds != null && productIds.size() > 0) {
                this.productsResponse = response;
                availableSortOptions.clear();
                availableSortOptions.addAll(response.getAvailableSortOptions());
                productsFromSubcategoryAdapter = new ProductsFromSubcategoryAdapter(getContext(), this);
                productListFromServer = response.getProducts();
                List<ProductModel> productModels = new ArrayList<>();

                for (ProductModel model : response.getProducts()) {
                    for (Integer i : productIds) {
                        if (i == model.getId()) {
                            productModels.add(model);
                        }
                    }
                }
                productsFromSubcategoryAdapter.loadData(productModels);
                listProduct.setAdapter(productsFromSubcategoryAdapter);
            } else {
                this.productsResponse = response;
                availableSortOptions.clear();
                availableSortOptions.addAll(response.getAvailableSortOptions());
                productsFromSubcategoryAdapter = new ProductsFromSubcategoryAdapter(getContext(), this);
                productListFromServer = response.getProducts();
                productsFromSubcategoryAdapter.loadData(response.getProducts());
                listProduct.setAdapter(productsFromSubcategoryAdapter);
                if (AppUtils.cutNull(fromWhere).contains("feature")) {
                    sortFilterLayout(0);
                }
            }
            titleToolbar = response.getName();
            Objects.requireNonNull(getActivity()).setTitle(response.getName());
            Log.d("response", response.getName());
        }

    }


    public void callWebServiceListOfProducts(int id) {
        shimmerFrameLayout.startShimmer();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        RetroClient.getApi().getProductListFromSubcategory(id)
                .enqueue(new CustomCB<ProductsResponse>());
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentListener.disableNavigation(true);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentListener = (FragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }

    @Override
    public void onNavigationBackPressed() {
        if (backContainer.size() > 1) {
            try {
                if (myApplication.toolBarTitle.size() > 1) {
                    myApplication.toolBarTitle.remove(myApplication.toolBarTitle.size() - 1);
                    LinkedHashSet<String> stringss = new LinkedHashSet<>(myApplication.toolBarTitle);
                    List<String> stringLists = new ArrayList<>(stringss);
                    if (stringLists.size() > 1)
                        Objects.requireNonNull(getActivity()).setTitle(stringLists.get(stringLists.size() - 1));
                    else Objects.requireNonNull(getActivity()).setTitle(stringLists.get(0));
                }
            } catch (Exception ignored) {
            }
            Navigation navigation = backContainer.get(backContainer.size() - 1);
            if (backContainer.contains(navigation)) {
                backContainer.remove(navigation);
            }
            Navigation previousNavigation = backContainer.get(backContainer.size() - 1);
            int id = previousNavigation.getId();
            if (previousNavigation.getNavigationCategory().equals(Navigation.NavigationCategory.ITEM)) {
                categoryId = id;
                callWebServiceListOfProducts(id);
            } else {
                categoryId = id;
                callWebServiceMoreLoad();
            }
        } else {
            closeDrawer();
            Objects.requireNonNull(getActivity()).onBackPressed();
        }

      /*  LinkedHashSet<String>strings=new LinkedHashSet<>(myApplication.toolBarTitle);
        List<String> stringList=new ArrayList<>(strings);
        if (stringList.size() > 1) {
            myApplication.toolBarTitle.remove(myApplication.toolBarTitle.size() - 1);
            getActivity().setTitle(stringList.get(stringList.size() - 1));
        }*/


    }

    @Override
    public void onDataClick(int id) {
        categoryId = id;
        productIds = null;
        callWebServiceMoreLoad();
    }

    @Override
    public void sortFilterLayout(int status) {
        sortFilterView.setVisibility(status);
        if (status == 0) {
            gridView.setVisibility(View.GONE);
            gridViewBorder.setVisibility(View.GONE);
            closeDrawer();
        }
    }


    @Override
    public void onItemClick(int id) {
        ProductModel productModel = new ProductModel();
        productModel.setId(id);
        productModel.setName("");
        //  productIds = null;
        // productsResponse = null;
        ProductDetailFragment.productModel = productModel;
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new ProductDetailFragment()).addToBackStack(null).commit();
    }

    @Override
    public void getSubcategoryList(int id, String title) {
        Objects.requireNonNull(getActivity()).setTitle(title);
        categoryId = id;
        callWebServiceMoreLoad();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        isFilterClickSingleTime = false;
        switch (resultCode) {
            case FILTER_OPTION_INTENT:
                filterApiCall(data);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void filterApiCall(Intent data) {
        needToCallApi = false;
        if (data != null) {
            String price = data.getStringExtra("price");
            String priceSelected[] = price.split("-");
            double minPrice = Double.parseDouble(priceSelected[0]);
            double maxPrice = Double.parseDouble(priceSelected[1]);
            int minPriceInt = (int) Math.round(minPrice);
            int maxPriceInt = (int) Math.round(maxPrice);
            UserSelectedList model = (UserSelectedList) data.getSerializableExtra("category");
            List<UserSelectionResponseModel> userSelectionResponseModelList;

            if (model != null && null != model.getSelectedModelList() && model.getSelectedModelList().size() > 0) {
                isFilterClicked = true;
                LinkedHashSet<String> cateName = new LinkedHashSet();
                LinkedHashSet<String> optionName = new LinkedHashSet();
                for (com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel selectedModel : model.getSelectedModelList()) {
                    cateName.add(selectedModel.getCateName());
                }
                for (com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel selectedModel : model.getSelectedModelList()) {
                    optionName.add(selectedModel.getOptionName().substring(0, 1).toUpperCase() + selectedModel.getOptionName().substring(1));
                }

                userSelectionResponseModelList = new ArrayList<>();
                for (String integer : cateName) {
                    UserSelectionResponseModel userSelectionResponseModel = new UserSelectionResponseModel();
                    userSelectionResponseModel.setId(integer);
                    List<GroupDataItem> listGroupDataItem = new ArrayList<>();
                    for (SelectedModel model1 : model.getSelectedModelList()) {
                        if (integer.equalsIgnoreCase(model1.getCateName())) {
                            GroupDataItem groupDataItem = new GroupDataItem();
                            groupDataItem.setCateName(model1.getCateName());
                            groupDataItem.setOptionName(model1.getOptionName());
                            groupDataItem.setFilterId(model1.getFilterId());
                            groupDataItem.setProductId(model1.getProductId());
                            listGroupDataItem.add(groupDataItem);
                        }
                    }
                    userSelectionResponseModel.setGroupData(listGroupDataItem);
                    userSelectionResponseModelList.add(userSelectionResponseModel);
                }
                Log.d("userSelection", new Gson().toJson(userSelectionResponseModelList));
                productIds = new ArrayList<>();
            } else {
                userSelectionResponseModelList = new ArrayList<>(0);
                productIds = new ArrayList<>();
                isFilterClicked = maxPriceSel != maxPriceInt || minPriceSel != minPriceInt;
            }
            int countUserSelection = 0;
            LinkedHashSet<Integer> pId = new LinkedHashSet<>();
            ArrayList<Integer> pIdMulSelection = new ArrayList<>();
            List<ProductModel> selectedItems = new ArrayList<>(0);
            List<Integer> ids = new ArrayList<>();
            try {
                if (userSelectionResponseModelList.size() == 1) {
                    for (UserSelectionResponseModel userSelectionResponseModel : userSelectionResponseModelList) {

                        for (GroupDataItem groupDataItem : userSelectionResponseModel.getGroupData()) {
                            pId.add(groupDataItem.getProductId());
                        }
                    }
                } else {
                    for (UserSelectionResponseModel userSelectionResponseModel : userSelectionResponseModelList) {
                        LinkedHashSet<Integer> integers = new LinkedHashSet<>();
                        for (GroupDataItem groupDataItem : userSelectionResponseModel.getGroupData()) {
                            integers.add(groupDataItem.getProductId());
                        }
                        if (integers.size() > 0) {
                            countUserSelection++;
                            ids.addAll(integers);
                        }
                    }
                }
                if (userSelectionResponseModelList.size() == 1) {
                    ArrayList<Integer> integers = new ArrayList<>(pId);
                    for (ProductModel response : productsResponse.getProducts()) {
                        double productAmount = response.getProductPrice().getPriceValue();
                        if (minPrice <= productAmount && maxPrice >= productAmount) {
                            if (integers.size() > 0) {
                                for (int i = 0; i < integers.size(); i++) {
                                    if (integers.get(i) == response.getId()) {
                                        selectedItems.add(response);
                                    }
                                }
                            }
                        }
                    }
                } else if (userSelectionResponseModelList.size() == 0) {
                    for (ProductModel response : productsResponse.getProducts()) {
                        double productAmount = response.getProductPrice().getPriceValue();
                        if (minPrice <= productAmount && maxPrice >= productAmount) {
                            selectedItems.add(response);
                        }
                    }
                } else {
                    ArrayList<Integer> listIds = new ArrayList<>();
                    Set<Integer> unique = new HashSet<Integer>(ids);
                    for (Integer key : unique) {
                        if (Collections.frequency(ids, key) == countUserSelection) {
                            listIds.add(key);
                        }
                    }
                    for (ProductModel responsea : productsResponse.getProducts()) {
                        double productAmounat = responsea.getProductPrice().getPriceValue();
                        if (minPrice <= productAmounat && maxPrice >= productAmounat) {
                            if (listIds.size() > 0) {
                                for (int i = 0; i < listIds.size(); i++) {
                                    if (listIds.get(i) == responsea.getId()) {
                                        selectedItems.add(responsea);
                                    }
                                }
                            }
                        }
                    }
                }

                if (null != productsFromSubcategoryAdapter) {
                    if (alreadySortClicked == -1)
                        productsFromSubcategoryAdapter.loadData(selectedItems);
                    else {
                        productsFromSubcategoryAdapter.products = selectedItems;
                        alReadySortClicked();
                    }
                    if (selectedItems.size() > 0) {
                        for (ProductModel model1 : selectedItems) {
                            productIds.add(Integer.parseInt(String.valueOf(model1.getId())));
                        }
                        noRecordFound.setVisibility(View.GONE);
                        listProduct.setVisibility(View.VISIBLE);
                    } else {
                        noRecordFound.setVisibility(View.VISIBLE);
                        listProduct.setVisibility(View.GONE);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (productListFromServer != null && productListFromServer.size() > 0)
                    productsFromSubcategoryAdapter.loadData(productListFromServer);
            }
        } else {
            isFilterClicked = false;
        }
        sortAndFilterColorChanged();

    }

    private Set<Integer> findDuplicates(List<Integer> listContainingDuplicates) {
        final Set<Integer> setToReturn = new HashSet<>();
        final Set<Integer> set1 = new HashSet<>();

        for (Integer yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    private void sortAndFilterColorChanged() {
        if (alreadySortClicked == -1/*||productsFromSubcategoryAdapter.getItemCount()==0*/)
            rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        else rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));

        if (isFilterClicked/*&&productsFromSubcategoryAdapter.getItemCount()>0*/)
            rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));
        else rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.white));

    }

    private void setSelected() {
        if (productIds != null && productIds.size() > 0)
            rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));
        else rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.white));

        if (alreadySortClicked == -1)
            rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        else rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));


    }

    public Set<Integer> getMultiSelection(List<Integer> listContainingDuplicates) {

        final Set<Integer> setToReturn = new HashSet<Integer>();
        final Set<Integer> set1 = new HashSet<Integer>();

        for (Integer yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    @Override
    public void callApiForWishList(long pId, int cartTypeId) {

        List<KeyValuePair> productAttributes = new ArrayList<>();

        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey("addtocart_" + pId + ".EnteredQuantity");
        keyValuePair.setValue("1");
        productAttributes.add(keyValuePair);

        RetroClient.getApi()
                .addProductIntoCart(pId, cartTypeId, productAttributes)
                .enqueue(new CustomCB<AddtoCartResponse>(getView(), 0));
    }

    @Override
    public void removeWishlist(long pId, int cartId) {
        RetroClient.getApi()
                .removeProductFromWishList(String.valueOf(pId), "2")
                .enqueue(new CustomCB<BaseResponse>(getView(), 0));
    }

    @Override
    public void noRecordFound(boolean b) {
        if (b)
            noRecordFound.setVisibility(View.VISIBLE);
        else noRecordFound.setVisibility(View.GONE);
    }

    public void onEvent(BaseResponse response) {
        String errors = "";
        if (response.getErrorList().length > 0) {
            for (int i = 0; i < response.getErrorList().length; i++) {
                errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
            }
            showSnackBar(errors);
        }

    }

    public void onEvent(AddtoCartResponse response) {
        String errors = "";
        if (response.getErrorList().length > 0) {
            for (int i = 0; i < response.getErrorList().length; i++) {
                errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
            }
            showSnackBar(errors);
        }

    }

    public void showSnackBar(String message) {
        Snackbar.make(Objects.requireNonNull(getView()), message, Snackbar.LENGTH_LONG).show();
    }

    private void alReadySortClicked() {
        productsFromSubcategoryAdapter.loadData(doSortOption(alreadySortClicked));
    }

    public interface SetTitle {
        void setToolbarTitle();
    }
}

