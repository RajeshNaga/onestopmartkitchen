package com.mart.onestopkitchen.products.view.model;

/**
 * Created by Hemnath on 9/22/2018.
 */

public class SubCategory {

    private Integer id;

    private String name;

    private PictureModel pictureModel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PictureModel getPictureModel() {
        return pictureModel;
    }

    public void setPictureModel(PictureModel pictureModel) {
        this.pictureModel = pictureModel;
    }
}
