package com.mart.onestopkitchen.products.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Updated by Akash Garg on 2/2/2019.
 */

public class ProductsFromSubcategoryAdapter extends RecyclerView.Adapter<ProductsFromSubcategoryAdapter.ShowCategoryProductsHolder> {
    public List<ProductModel> products;
    private Context context;
    private OnItemClickListener itemClickListener;

    ProductsFromSubcategoryAdapter(Context context, OnItemClickListener itemClickListener) {
        this.context = context;
        this.products = new ArrayList<>(0);
        this.itemClickListener = itemClickListener;
    }

    public void loadData(List<ProductModel> productModelList) {
        if (productModelList == null || productModelList.size() == 0)
            itemClickListener.noRecordFound(true);
        else itemClickListener.noRecordFound(false);
        this.products = productModelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ShowCategoryProductsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_category_selected_products, parent, false);
        return new ShowCategoryProductsHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ShowCategoryProductsHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.pName.setText(products.get(position).getName());

        if (null != products.get(position).getProductPrice().getPriceWithDiscount() && !products.get(position).getProductPrice().getPriceWithDiscount().isEmpty()) {
            holder.productOldPrice.setVisibility(View.VISIBLE);
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), products.get(position).getProductPrice().getPriceWithDiscount(), 0));
            holder.productOldPrice.setText(products.get(position).getProductPrice().getPrice());

        } else if (null != products.get(position).getProductPrice().getOldPrice()) {
            holder.productOldPrice.setVisibility(View.VISIBLE);
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), products.get(position).getProductPrice().getPrice(), 0));
            holder.productOldPrice.setText(products.get(position).getProductPrice().getOldPrice());

        } else {
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), products.get(position).getProductPrice().getPrice(), 0));
            holder.productOldPrice.setText(products.get(position).getProductPrice().getOldPrice());
            holder.productOldPrice.setVisibility(View.GONE);
        }
        holder.productOldPrice.setPaintFlags(holder.productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        Glide
                .with(context)
                .load(products.get(position).getDefaultPictureModel().getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.pImage);

        if (null != holder.gif_mark_new) {
            if (products.get(position).getMarkAsNew()) {
                holder.gif_mark_new.setVisibility(View.VISIBLE);
                Glide
                        .with(context)
                        .load(R.drawable.new_3)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.gif_mark_new);
            } else {
                holder.gif_mark_new.setVisibility(View.GONE);
            }
        }

        holder.itemView.setOnClickListener(v -> {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet())
                itemClickListener.onItemClick((int) products.get(position).getId());
        });

        if (products.get(position).getProductPrice().getDiscountPercentage() != 0) {
            holder.tvDiscountPercentage.setVisibility(View.VISIBLE);
            holder.tvDiscountPercentage.setText(products.get(position).getProductPrice().getDiscountPercentage() + " %");
        } else holder.tvDiscountPercentage.setVisibility(View.GONE);

        holder.wishList.setChecked(products.get(position).getWishList());

        holder.wishList.setOnClickListener(view -> {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                if (holder.wishList.isChecked()) {
                    products.get(position).setWishList(true);
                    itemClickListener.callApiForWishList(products.get(position).getId(), 2);
                } else {
                    if (products.get(position).getId() != 0L) {
                        products.get(position).setWishList(false);
                        itemClickListener.removeWishlist(products.get(position).getId(), 2);
                    }
                }
            } else {
                holder.wishList.setChecked(false);
            }
        });
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int id);

        void callApiForWishList(long pId, int cartTypeId);

        void removeWishlist(long pId, int cartId);

        void noRecordFound(boolean b);
    }

    class ShowCategoryProductsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_productImage)
        ImageView pImage;
        @BindView(R.id.tv_products_name)
        TextView pName;
        @BindView(R.id.tv_product_Price)
        TextView productPrice;
        @BindView(R.id.tv_product_OldPrice)
        TextView productOldPrice;
        @BindView(R.id.tv_discount_percentage)
        TextView tvDiscountPercentage;
        @BindView(R.id.button_favorite_category)
        ToggleButton wishList;
        @BindView(R.id.gif_mark_new)
        ImageView gif_mark_new;

        ShowCategoryProductsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
