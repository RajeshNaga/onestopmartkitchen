package com.mart.onestopkitchen.products.view.model;

import java.util.List;

/**
 * Created by Hemnath on 9/22/2018.
 */

public class SubcategoryResponse {


    private int statusCode;
    private List<SubCategory> subCategories = null;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }


}
