package com.mart.onestopkitchen.products.view;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.model.SubCategoryProductDetails.SubCategoryProduct;
import com.mart.onestopkitchen.ui.adapter.SubCategoryDrawerAdapter;
import com.mart.onestopkitchen.ui.customview.CustomLinearLayoutManager;
import com.mart.onestopkitchen.ui.fragment.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hemnath on 9/22/2018.
 */

public class ProductNavigationProduct extends BaseFragment implements SubCategoryDrawerAdapter.OnItemSubcategoryListener, View.OnClickListener {

    static RelativeLayout rootview;
    int categoryId;
    SubCategoryDrawerAdapter subCategoryDrawerAdapter;
    @BindView(R.id.rv_subcategory)
    RecyclerView subCategoryRecyclerList;
    CustomLinearLayoutManager layoutManager;
    OnLoadDataListener onLoadDataListener;
    @BindView(R.id.ll_back)
    LinearLayout back;
    ArrayList<SubCategoryProduct.SubCategory> subCategories;
    private MyApplication myApplication;

    public static ProductNavigationProduct newInstance(int id, ProductListFragment productListFragment) {
        ProductNavigationProduct navigationFragment = new ProductNavigationProduct();
        navigationFragment.onLoadDataListener = productListFragment;
        Bundle args = new Bundle();
        navigationFragment.setArguments(args);
        navigationFragment.categoryId = id;
        return navigationFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subcategory, container, false);
        ButterKnife.bind(this, view);
        rootview = view.findViewById(R.id.rl_rootView);
        back.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        subCategoryDrawerAdapter = new SubCategoryDrawerAdapter(getContext(), this);

        layoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        subCategoryRecyclerList.setHasFixedSize(true);
        subCategoryRecyclerList.setLayoutManager(layoutManager);

        subCategoryRecyclerList.setAdapter(subCategoryDrawerAdapter);
        myApplication = (MyApplication) getActivity().getApplicationContext();
    }

    public void loadSubCategory(ArrayList<SubCategoryProduct.SubCategory> subCategories, int categoryId) {
        this.categoryId = categoryId;
        this.subCategories = subCategories;
        subCategoryDrawerAdapter.loadData(subCategories);
        if (subCategories.size() > 0) {
            onLoadDataListener.sortFilterLayout(8);
        } else {
            onLoadDataListener.sortFilterLayout(0);
        }
    }


    public void showSortFilterLayout() {
        onLoadDataListener.sortFilterLayout(0);
    }

    @Override
    public void onItemClick(int pId, String s) {
        getActivity().setTitle(s);
        myApplication.toolBarTitle.add(s);
        // Navigation item click in navigation drawer
        onLoadDataListener.onDataClick(pId);
       /* if(!new DeviceInformation().getDeviceInfo().getInternetCheckin()==true){
            startActivity(new Intent(getActivity(),OfflineActivity.class));
        }
        else{
            onLoadDataListener.onDataClick(pId);
        }*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                handleNavigationBack();
                break;
        }
    }

    private void handleNavigationBack() {
        /*if (myApplication.toolBarTitle.size() > 1) {
            getActivity().setTitle(myApplication.toolBarTitle.get(myApplication.toolBarTitle.size() - 1));
            myApplication.toolBarTitle.remove(myApplication.toolBarTitle.size() - 1);
        }*/
        onLoadDataListener.onNavigationBackPressed();
    }


    public interface OnLoadDataListener {

        void onNavigationBackPressed();

        void onDataClick(int id);

        void sortFilterLayout(int status);

    }

}
