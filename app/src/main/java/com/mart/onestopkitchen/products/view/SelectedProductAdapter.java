package com.mart.onestopkitchen.products.view;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.SubCategoryProductDetails.SubCategoryProduct;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class SelectedProductAdapter extends RecyclerView.Adapter<SelectedProductAdapter.SelectedProductHolder> {

    public List<ProductModel> products;

    Context context;

    List<SubCategoryProduct.SubCategory> productList;

    OnSubCategoryListener onSubCategoryListener;

    public SelectedProductAdapter(Context context, ArrayList<SubCategoryProduct.SubCategory> subCategories, OnSubCategoryListener onSubCategoryListener) {

        this.context = context;
        this.products = new ArrayList<>();
        this.productList = new ArrayList<>();
        this.onSubCategoryListener = onSubCategoryListener;
        this.productList = subCategories;

    }

    public void loadData(List<ProductModel> productList) {
        this.products = productList;
        notifyDataSetChanged();
    }

    public void loadSubcategoryData(List<SubCategoryProduct.SubCategory> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SelectedProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_selected_product, parent, false);
        SelectedProductHolder sph = new SelectedProductHolder(v);
        return sph;
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedProductHolder holder, final int position) {

        holder.productName.setText(AppUtils.capitalizeWords(productList.get(position).getName()));

        SubCategoryProduct.PictureModel pictureModel = productList.get(position).getPictureModel();


        Glide
                .with(context)
                .load(pictureModel.getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.productImage);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new DeviceInformation().getDeviceInfo().getCheckInterNet())
                    onSubCategoryListener.getSubcategoryList(productList.get(position).getId(), productList.get(position).getName());
            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public interface OnSubCategoryListener {
        void getSubcategoryList(int id, String s);
    }

    class SelectedProductHolder extends RecyclerView.ViewHolder {
        ImageView productImage;
        TextView productName;

        SelectedProductHolder(View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.img_productImage);
            productName = itemView.findViewById(R.id.tv_productName);

        }

    }
}
