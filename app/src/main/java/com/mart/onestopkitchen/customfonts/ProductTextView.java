package com.mart.onestopkitchen.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Hemnath on 12/13/2018.
 */
public class ProductTextView extends androidx.appcompat.widget.AppCompatTextView {

    public ProductTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ProductTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProductTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/MavenPro-Regular.ttf");
            setTypeface(tf);
        }
    }
}
