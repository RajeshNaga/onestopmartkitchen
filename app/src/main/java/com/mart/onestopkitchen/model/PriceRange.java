package com.mart.onestopkitchen.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ashraful on 11/11/2015.
 */
public class PriceRange implements Serializable {
    @SerializedName("From")
    private double from;
    @SerializedName("To")
    private double to;

    public double getFrom() {
        return from;
    }

    public void setFrom(double from) {
        this.from = from;
    }

    public double getTo() {
        return to;
    }

    public void setTo(double to) {
        this.to = to;
    }
}
