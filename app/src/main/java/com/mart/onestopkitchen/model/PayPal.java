package com.mart.onestopkitchen.model;

/**
 * Created by Ashraful on 12/21/2015.
 */
public class PayPal {
    private String ClientId;

    public String getClientId() {
        return ClientId;
    }

    public void setClientId(String clientId) {
        ClientId = clientId;
    }
}