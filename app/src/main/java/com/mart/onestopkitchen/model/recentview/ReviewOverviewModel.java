package com.mart.onestopkitchen.model.recentview;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReviewOverviewModel implements Serializable {

    @SerializedName("RatingSum")
    private double ratingSum;

    @SerializedName("TotalReviews")
    private double totalReviews;

    @SerializedName("RatingUrlFromGSMArena")
    private String ratingUrlFromGSMArena;

    @SerializedName("ProductId")
    private int productId;

    @SerializedName("AllowCustomerReviews")
    private boolean allowCustomerReviews;

    public double getRatingSum() {
        return ratingSum;
    }

    public void setRatingSum(double ratingSum) {
        this.ratingSum = ratingSum;
    }

    public double getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(double totalReviews) {
        this.totalReviews = totalReviews;
    }

    public String getRatingUrlFromGSMArena() {
        return ratingUrlFromGSMArena;
    }

    public void setRatingUrlFromGSMArena(String ratingUrlFromGSMArena) {
        this.ratingUrlFromGSMArena = ratingUrlFromGSMArena;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public boolean isAllowCustomerReviews() {
        return allowCustomerReviews;
    }

    public void setAllowCustomerReviews(boolean allowCustomerReviews) {
        this.allowCustomerReviews = allowCustomerReviews;
    }
}