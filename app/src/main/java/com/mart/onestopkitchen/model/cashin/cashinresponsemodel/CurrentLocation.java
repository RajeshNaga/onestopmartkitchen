package com.mart.onestopkitchen.model.cashin.cashinresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CurrentLocation implements Serializable {

    @SerializedName("Accuracy")
    private int accuracy;

    @SerializedName("CreatedTime")
    private String createdTime;

    @SerializedName("Angle")
    private int angle;

    @SerializedName("coordinates")
    private List<Double> coordinates;

    @SerializedName("Time")
    private String time;

    @SerializedName("Latitude")
    private double latitude;

    @SerializedName("type")
    private String type;

    @SerializedName("Longitude")
    private double longitude;

    @SerializedName("Source")
    private Object source;

    @SerializedName("Name")
    private Object name;

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }
}