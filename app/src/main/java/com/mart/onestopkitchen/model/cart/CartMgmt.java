package com.mart.onestopkitchen.model.cart;

import com.mart.onestopkitchen.model.ProductDetail;
import com.mart.onestopkitchen.networking.response.CartProductListResponse;

public class CartMgmt {

    CartProductListResponse mCartProduct = null;
    ProductDetail mBuyNowProduct = null;

    public CartProductListResponse getmCartProduct() {
        return mCartProduct;
    }

    public void setmCartProduct(CartProductListResponse mCartProduct) {
        this.mCartProduct = mCartProduct;
    }

    public ProductDetail getmBuyNowProduct() {
        return mBuyNowProduct;
    }

    public void setmBuyNowProduct(ProductDetail mBuyNowProduct) {
        this.mBuyNowProduct = mBuyNowProduct;
    }

    public Boolean isBuyNow() {
        return mBuyNowProduct != null;
    }
}
