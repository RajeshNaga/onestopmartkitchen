package com.mart.onestopkitchen.model;

/**
 * Created by Ashraful on 11/20/2015.
 */
public class AboutUsModel {
    private String TopicTitle;
    private String TopicBody;

    public String getTopicTitle() {
        return TopicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        TopicTitle = topicTitle;
    }

    public String getTopicBody() {
        return TopicBody;
    }

    public void setTopicBody(String topicBody) {
        TopicBody = topicBody;
    }
}

