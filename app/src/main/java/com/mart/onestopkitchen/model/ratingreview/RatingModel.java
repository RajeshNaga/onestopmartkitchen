package com.mart.onestopkitchen.model.ratingreview;

public class RatingModel {
    private String RatingSum="";
    private String TotalReviews="";

    public String getRatingSum() {
        return RatingSum;
    }

    public void setRatingSum(String ratingSum) {
        RatingSum = ratingSum;
    }

    public String getTotalReviews() {
        return TotalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        TotalReviews = totalReviews;
    }


}
