package com.mart.onestopkitchen.model.addsubmenu;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SubMenuResponseModel implements Serializable {

    @SerializedName("HomeData")
    private List<HomeDataItem> homeData;

    @SerializedName("MainItems")
    private List<MainItemsItem> mainItems;

    @SerializedName("MainItemName")
    private String mainItemName;

    public List<HomeDataItem> getHomeData() {
        return homeData;
    }

    public void setHomeData(List<HomeDataItem> homeData) {
        this.homeData = homeData;
    }

    public List<MainItemsItem> getMainItems() {
        return mainItems;
    }

    public void setMainItems(List<MainItemsItem> mainItems) {
        this.mainItems = mainItems;
    }

    public String getMainItemName() {
        return mainItemName;
    }

    public void setMainItemName(String mainItemName) {
        this.mainItemName = mainItemName;
    }
}