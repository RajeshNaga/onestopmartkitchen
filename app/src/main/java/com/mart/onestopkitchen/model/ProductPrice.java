package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.products.view.model.CustomProperties;

import java.io.Serializable;

/**
 * updated by AkashGarg  on 29/12/2018.
 */
public class ProductPrice implements Serializable {

    @SerializedName("Form")
    @Expose
    private Object Form;

    @SerializedName("CustomerEntersPrice")
    @Expose
    private boolean CustomerEntersPrice;

    @SerializedName("ProductId")
    @Expose
    private int ProductId;

    @SerializedName("RentalPrice")
    @Expose
    private Object RentalPrice;

    @SerializedName("DiscountOfferName")
    @Expose
    private String DiscountOfferName;

    @SerializedName("CurrencyCode")
    @Expose
    private String CurrencyCode;

    @SerializedName("DisplayTaxShippingInfo")
    @Expose
    private boolean DisplayTaxShippingInfo;

    @SerializedName("PriceWithDiscount")
    @Expose
    private String PriceWithDiscount;

    @SerializedName("HidePrices")
    @Expose
    private boolean HidePrices;

    @SerializedName("DiscountPercentage")
    @Expose
    private int DiscountPercentage;

    @SerializedName("Price")
    @Expose
    private String Price;

    @SerializedName("CallForPrice")
    @Expose
    private boolean CallForPrice;

    @SerializedName("BasePricePAngV")
    @Expose
    private Object BasePricePAngV;

    @SerializedName("PriceWithDiscountValue")
    @Expose
    private double PriceWithDiscountValue;

    @SerializedName("PriceValue")
    @Expose
    private double PriceValue;

    @SerializedName("IsRental")
    @Expose
    private boolean IsRental;

    @SerializedName("OldPrice")
    @Expose
    private String OldPrice;

    @SerializedName("CustomProperties")
    @Expose
    private CustomProperties customProperties;


    public CustomProperties getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }

    public Object getForm() {
        return Form;
    }

    public void setForm(Object form) {
        Form = form;
    }

    public boolean isCustomerEntersPrice() {
        return CustomerEntersPrice;
    }

    public void setCustomerEntersPrice(boolean customerEntersPrice) {
        CustomerEntersPrice = customerEntersPrice;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public Object getRentalPrice() {
        return RentalPrice;
    }

    public void setRentalPrice(Object rentalPrice) {
        RentalPrice = rentalPrice;
    }

    public String getDiscountOfferName() {
        return DiscountOfferName;
    }

    public void setDiscountOfferName(String discountOfferName) {
        DiscountOfferName = discountOfferName;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        CurrencyCode = currencyCode;
    }

    public boolean isDisplayTaxShippingInfo() {
        return DisplayTaxShippingInfo;
    }

    public void setDisplayTaxShippingInfo(boolean displayTaxShippingInfo) {
        DisplayTaxShippingInfo = displayTaxShippingInfo;
    }

    public String getPriceWithDiscount() {
        return PriceWithDiscount;
    }

    public void setPriceWithDiscount(String priceWithDiscount) {
        PriceWithDiscount = priceWithDiscount;
    }

    public boolean isHidePrices() {
        return HidePrices;
    }

    public void setHidePrices(boolean hidePrices) {
        HidePrices = hidePrices;
    }

    public int getDiscountPercentage() {
        return DiscountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        DiscountPercentage = discountPercentage;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public boolean isCallForPrice() {
        return CallForPrice;
    }

    public void setCallForPrice(boolean callForPrice) {
        CallForPrice = callForPrice;
    }

    public Object getBasePricePAngV() {
        return BasePricePAngV;
    }

    public void setBasePricePAngV(Object basePricePAngV) {
        BasePricePAngV = basePricePAngV;
    }

    public double getPriceWithDiscountValue() {
        return PriceWithDiscountValue;
    }

    public void setPriceWithDiscountValue(double priceWithDiscountValue) {
        PriceWithDiscountValue = priceWithDiscountValue;
    }

    public double getPriceValue() {
        return PriceValue;
    }

    public void setPriceValue(double priceValue) {
        PriceValue = priceValue;
    }

    public boolean isRental() {
        return IsRental;
    }

    public void setRental(boolean rental) {
        IsRental = rental;
    }

    public String getOldPrice() {
        return OldPrice;
    }

    public void setOldPrice(String oldPrice) {
        OldPrice = oldPrice;
    }


    /* private int PriceValue;
    private int ProductId;
    private String OldPrice = "";
    private String Price = "";
    private String PriceWithDiscount = "";
    private String DiscountPercentage = "0";
    private String DiscountOfferName = "";


    public int getPriceValue() {
        return PriceValue;
    }

    public void setPriceValue(int priceValue) {
        this.PriceValue = priceValue;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        this.ProductId = productId;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getOldPrice() {
        return OldPrice;
    }

    public void setOldPrice(String oldPrice) {
        OldPrice = oldPrice;
    }


    public String getPriceWithDiscount() {
        return PriceWithDiscount;
    }

    public void setPriceWithDiscount(String priceWithDiscount) {
        PriceWithDiscount = priceWithDiscount;
    }

    public String getDiscountPercentage() {
        return DiscountPercentage;
    }

    public void setDiscountPercentage(String DiscountPercentage) {
        this.DiscountPercentage = DiscountPercentage;
    }

    public String getDiscountOfferName() {
        return DiscountOfferName;
    }

    public void setDiscountOfferName(String discountOfferName) {
        DiscountOfferName = discountOfferName;
    }*/
}
