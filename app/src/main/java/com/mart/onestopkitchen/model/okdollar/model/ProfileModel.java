package com.mart.onestopkitchen.model.okdollar.model;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProfileModel implements Serializable {

    private String MobileNumber = "";
    private String Name = "";
    private String GcmID = "";
    private String Encrypted = "";
    private String SimID = "";
    private String MSID = "";
    private String IMEI = "";
    private String lType = "";
    private String AppID = "";
    private String Recommended = "";
    private String State = "";
    private String Township = "";
    private String Father = "";
    private Boolean Gender = false;
    private String DateOfBirth = "";
    private String NRC = "";
    private String IDType = "";
    private String Phone = "";
    private String BusinessType = "";
    private String BusinessCategory = "";
    private String Car = "";
    private int CarType = -1;
    private String Latitude = "0.0";
    private String Longitude = "0.0";
    private String Address1 = "";
    private String Address2 = "";
    private int AccountType = -1;
    private int OSType = -1;
    private String ProfilePic = "";
    private String ProfilePicAwsUrl = "";
    private String signature = "";
    private String signatureAwsUrl = "";
    private String Password = "";
    private String EmailId = "";
    private String AddressType = "";
    private String CellTowerID = "";
    private String BusinessName = "";
    private boolean Kickback = false;
    private String CodeAlternate = "";
    private String Country = "";
    private String ParentAccount = "";
    private String CodeRecommended = "";
    private List<BankDetailsModel> BankDetails = new ArrayList<>();
    private List<CashBankOutNoModel> Msisdn = new ArrayList<>();
    private boolean PaymentGateway = false;
    private boolean Loyalty = true;
    private String SecureToken = "";
    private String Language = "";
    private String FBEmailId = "";
    private String AgentAuthCode = "";
    private String CountryCode = "";
    private String DeviceId = "";
    private String PhoneModel = "";
    private String Phonebrand = "";
    private String OsVersion = "";
    private DeviceInformationModel DeviceInfo;
    // ADDED FOR NEW UPDATED REGISTRATION
    private String IdPhoto = "";
    private String IdPhoto1 = "";
    private String IdPhotoAwsUrl = "";
    private String IdPhoto1AwsUrl = "";
    private String CountryOfCitizen = "";
    private String IdExpDate = "";
    private String HouseBlockNo = ""; // House No
    private String FloorNumber = "";
    private String RoomNumber = "";
    private String HouseName = "";
    private String VillageName = "";
    private List<String> ClosedDays = new ArrayList<>();
    private String BusinessIdPhoto1 = "";
    private String BusinessIdPhoto2 = "";
    private String BusinessIdPhoto1AwsUrl = "";
    private String BusinessIdPhoto2AwsUrl = "";
    private String OpenTime = "00:00";
    private String CloseTime = "00:00";
    private String SecurityQuestionCode = "";
    private String SecurityQuestionAnswer = "";
    private int RegistrationStatus = -1;
    private String PasswordType = "";  // ADDED ALREADY
    private String LoginIdentityType = "";
    private String LoginAppKey = "";
    private String CreatedDate = "";
    public ProfileModel() {
    }

    public String getProfilePicAwsUrl() {
        return ProfilePicAwsUrl;
    }

    public void setProfilePicAwsUrl(String profilePicAwsUrl) {
        ProfilePicAwsUrl = profilePicAwsUrl;
    }

    public String getSignatureAwsUrl() {
        return signatureAwsUrl;
    }

    public void setSignatureAwsUrl(String signatureAwsUrl) {
        this.signatureAwsUrl = signatureAwsUrl;
    }

    public String getIdPhotoAwsUrl() {
        return IdPhotoAwsUrl;
    }

    public void setIdPhotoAwsUrl(String idPhotoAwsUrl) {
        IdPhotoAwsUrl = idPhotoAwsUrl;
    }

    public String getIdPhoto1AwsUrl() {
        return IdPhoto1AwsUrl;
    }

    public void setIdPhoto1AwsUrl(String idPhoto1AwsUrl) {
        IdPhoto1AwsUrl = idPhoto1AwsUrl;
    }

    public String getBusinessIdPhoto1AwsUrl() {
        return BusinessIdPhoto1AwsUrl;
    }

    public void setBusinessIdPhoto1AwsUrl(String businessIdPhoto1AwsUrl) {
        BusinessIdPhoto1AwsUrl = businessIdPhoto1AwsUrl;
    }

    public String getBusinessIdPhoto2AwsUrl() {
        return BusinessIdPhoto2AwsUrl;
    }


    // ADDED THIS FOR NEW UPDATED REGISTRATION

    public void setBusinessIdPhoto2AwsUrl(String businessIdPhoto2AwsUrl) {
        BusinessIdPhoto2AwsUrl = businessIdPhoto2AwsUrl;
    }

    public ProfileModel getDataIntoModel(String response, Context context) {
        ProfileModel model = null;
        try {
            //response = Utils.readTextFileFromAssets(context, "update_profile.txt");
            JSONObject jsonObject = new JSONObject(response);
            String status = jsonObject.getString("Code");
            if (status.equals("200")) {
                // Utils.showToast(context, status);
                String data = jsonObject.getString("Data");
                JSONObject jsonObjectData = new JSONObject(data);
                JSONArray jsonObjectTable = jsonObjectData.getJSONArray("ProfileDetails");
                for (int i = 0; i < jsonObjectTable.length(); i++) {
                    model = new ProfileModel();
                    JSONObject jsonObject1 = jsonObjectTable.getJSONObject(i);
                    model.setMobileNumber(jsonObject1.getString("Mobilenumber"));
                    model.setName(jsonObject1.getString("Name"));
                    model.setGcmID(jsonObject1.getString("GCMID"));
                    model.setEncrypted(jsonObject1.getString("EncryptedKey"));
                    model.setSimID(jsonObject1.getString("SimID"));
                    model.setMSID(jsonObject1.getString("MSID"));
                    model.setIMEI(jsonObject1.getString("IMEI"));
                    model.setlType(jsonObject1.getString("AgentType"));
                    model.setAppID(jsonObject1.getString("AppID"));
                    model.setRecommended(jsonObject1.getString("Recommendedby"));
                    model.setState(jsonObject1.getString("State"));
                    model.setTownship(jsonObject1.getString("Township"));
                    model.setFather(jsonObject1.getString("FatherName"));
                    model.setGender(jsonObject1.getBoolean("Gender"));
                    model.setDateOfBirth(jsonObject1.getString("DOB"));
                    model.setNRC(jsonObject1.getString("NRC"));
                    model.setIDType(jsonObject1.getString("IDType"));
                    model.setPhone(jsonObject1.getString("PhoneNumber"));
                    model.setBusinessType(jsonObject1.getString("BusinessType"));
                    model.setBusinessCategory(jsonObject1.getString("BusinessCategory"));
                    model.setCar(jsonObject1.getString("Car"));
                    model.setCarType(jsonObject1.getInt("CarType"));
                    model.setLatitude(jsonObject1.getString("Latitude"));
                    model.setLongitude(jsonObject1.getString("Longitude"));
                    model.setAddress1(jsonObject1.getString("Address1"));
                    model.setAddress2(jsonObject1.getString("Address2"));
                    model.setAccountType(jsonObject1.getInt("AccountType"));
                    model.setOSType(jsonObject1.getInt("OSType"));
                    model.setProfilePic(jsonObject1.getString("ProfilePic"));
                    model.setsignature(jsonObject1.getString("SignaturePic"));
                    model.setFBEmailId(jsonObject1.getString("FBEmailId"));
                    model.setEmailID(jsonObject1.getString("EmailId"));
                    model.setAddressType(jsonObject1.getString("AddressType"));
                    model.setCellTowerID(jsonObject1.getString("CellTowerID"));
                    model.setBusinessName(jsonObject1.getString("BusinessName"));

                    model.setCodeAlternate(jsonObject1.getString("CodeAlternate"));
                    model.setCountry(jsonObject1.getString("Country"));
                    model.setParentAccount(jsonObject1.getString("ParentAccount"));
                    model.setCodeRecommended(jsonObject1.getString("CodeRecommended"));
                    model.setCountryCode(jsonObject1.getString("CountryCode"));

                    // ADDED FOR NEW UPDATED REGISTRATION
                    model.setCountryOfCitizen(jsonObject1.getString("CountryOfCitizen"));
                    model.setIdPhoto(jsonObject1.getString("IdPhoto"));
                    model.setIdPhoto1(jsonObject1.optString("IdPhoto1"));
                    model.setIdExpDate(jsonObject1.optString("IdExpDate"));

                    model.setHouseBlockNo(jsonObject1.getString("HouseBlockNo"));
                    model.setHouseName(jsonObject1.optString("HouseName"));
                    model.setFloorNumber(jsonObject1.optString("FloorNumber"));
                    model.setRoomNumber(jsonObject1.optString("RoomNumber"));
                    model.setVillageName(jsonObject1.optString("VillageName"));

                    model.setCreatedDate(jsonObject1.getString("CreatedDate"));

                    if (!jsonObject1.optString("OpenTime").equals("null") || jsonObject1.optString("OpenTime") != null) {
                        model.setOpenTime(jsonObject1.optString("OpenTime"));
                    } else {
                        model.setOpenTime("");
                    }

                    if (!jsonObject1.optString("CloseTime").equals("null") || jsonObject1.optString("CloseTime") != null) {
                        model.setCloseTime(jsonObject1.optString("CloseTime"));
                    } else {
                        model.setCloseTime("");
                    }
                    List<String> dayslist = new ArrayList<String>();
                    JSONArray jarr = jsonObjectData.optJSONArray("ClosedDays");
                    if (jarr != null) {
                        int len = jarr.length();
                        if (len > 0) {
                            for (int j = 0; j < len; j++) {
                                dayslist.add(jarr.getJSONObject(j).optString("ClosedDays"));
                            }
                            model.setClosedDays(dayslist);
                        } else {
                            dayslist.add("");
                            model.setClosedDays(dayslist);
                        }
                    } else {
                        dayslist.add("");
                        model.setClosedDays(dayslist);
                    }


                    model.setBusinessIdPhoto1(jsonObject1.optString("BusinessIdPhoto1"));
                    model.setBusinessIdPhoto2(jsonObject1.optString("BusinessIdPhoto2"));

                    JSONArray jarrnew = jsonObjectData.optJSONArray("QuestionCode");
                    if (jarrnew != null) {
                        if (jarrnew.length() > 0) {
                            model.setSecurityQuestionCode(jarrnew.getJSONObject(0).optString("QuestionCode"));
                            model.setSecurityQuestionAnswer(jarrnew.getJSONObject(0).optString("SecurityAnswer"));
                        } else {
                            model.setSecurityQuestionCode("");
                            model.setSecurityQuestionAnswer("");
                        }
                    } else {
                        model.setSecurityQuestionCode("");
                        model.setSecurityQuestionAnswer("");
                    }


                    model.setRegistrationStatus(jsonObject1.optInt("RegistrationStatus"));
                    model.setPasswordType(String.valueOf(jsonObject1.getInt("PasswordType")));
                    model.setLanguage(String.valueOf(jsonObject1.optString("Language")));
                    System.out.println("IN Model : From REsponse : PasswordType : " + String.valueOf(jsonObject1.getInt("PasswordType")));


                    JSONArray msidnArray = jsonObjectData.getJSONArray("MsisdnDetails");
                    for (int j = 0; j < msidnArray.length(); j++) {
                        CashBankOutNoModel msidn = new CashBankOutNoModel();
                        JSONObject bankObject = msidnArray.getJSONObject(j);
                        msidn.setMsisidn(bankObject.getString("Msisdn"));
                        msidn.setCountrycode(bankObject.getString("CountryCode"));
                        Msisdn.add(msidn);
                        Log.v("MSISDN", bankObject.getString("Msisdn"));
                    }
                    model.setMsisdn(Msisdn);

                    JSONArray bankArray = jsonObjectData.getJSONArray("BankDetails");
                    for (int j = 0; j < bankArray.length(); j++) {
                        BankDetailsModel bankDetailsModel = new BankDetailsModel();
                        JSONObject bankObject = bankArray.getJSONObject(j);
                        bankDetailsModel.setAccountNumber(bankObject.getString("AccountNumber"));
                        bankDetailsModel.setAccountType(bankObject.getString("AccountType"));
                        bankDetailsModel.setBankName(bankObject.getString("BankName"));
                        bankDetailsModel.setBranch(bankObject.getString("Branch"));
                        bankDetailsModel.setBranchAddress(bankObject.getString("BranchAddress"));
                        bankDetailsModel.setBankPhoneNumber(bankObject.getString("BankPhone"));
                        bankDetailsModel.setIDType(bankObject.getString("IDType"));
                        bankDetailsModel.setIDNo(bankObject.getString("IDNo"));
                        bankDetailsModel.setBranchAddress2(bankObject.getString("BranchAddress2"));
                        bankDetailsModel.setState(bankObject.getString("State"));
                        bankDetailsModel.setTownship(bankObject.getString("TownShip"));
                        bankDetailsModel.setBranchId(bankObject.getInt("BranchId"));
                        bankDetailsModel.setBankId(bankObject.getInt("BankId"));
                        bankDetailsModel.setBankBurmeseName(bankObject.getString("BankBurmeseName"));
                        bankDetailsModel.setBranchBurmeseName(bankObject.getString("BranchBurmeseName"));
                        BankDetails.add(bankDetailsModel);
                    }
                    model.setBankDetails(BankDetails);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.v("Exception", e.toString());
        }

        return model;
    }

    public String getCellTowerID() {
        return CellTowerID;
    }

    public void setCellTowerID(String cellTowerID) {
        CellTowerID = cellTowerID;
    }

    public String getAddressType() {

        return AddressType;
    }

    public void setAddressType(String addressType) {

        AddressType = addressType;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getEmailID() {
        return EmailId;
    }

    public void setEmailID(String emailID) {
        EmailId = emailID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    public String getsignature() {
        return signature;
    }

    public void setsignature(String signature) {
        this.signature = signature;
    }

    public String getGcmID() {
        return GcmID;
    }

    public void setGcmID(String gcmID) {
        GcmID = gcmID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEncrypted() {
        return Encrypted;
    }

    public void setEncrypted(String encrypted) {
        Encrypted = encrypted;
    }

    public String getSimID() {
        return SimID;
    }

    public void setSimID(String simID) {
        SimID = simID;
    }

    public String getMSID() {
        return MSID;
    }

    public void setMSID(String MSID) {
        this.MSID = MSID;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getlType() {
        return lType;
    }

    public void setlType(String lType) {
        this.lType = lType;
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String appID) {
        AppID = appID;
    }

    public String getRecommended() {
        return Recommended;
    }

    public void setRecommended(String recommended) {
        Recommended = recommended;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getFather() {
        return Father;
    }

    public void setFather(String father) {
        Father = father;
    }

    public Boolean getGender() {
        return Gender;
    }

    public void setGender(Boolean gender) {
        Gender = gender;
    }

    public String getNRC() {
        return NRC;
    }

    public void setNRC(String NRC) {
        this.NRC = NRC;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getBusinessCategory() {
        return BusinessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        BusinessCategory = businessCategory;
    }

    public String getCar() {
        return Car;
    }

    public void setCar(String car) {
        Car = car;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public int getCarType() {
        return CarType;
    }

    public void setCarType(int carType) {
        CarType = carType;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public int getAccountType() {
        return AccountType;
    }

    public void setAccountType(int accountType) {
        AccountType = accountType;
    }

    public int getOSType() {
        return OSType;
    }

    public void setOSType(int OSType) {
        this.OSType = OSType;
    }

    public void setKickback(Boolean kickback) {
        Kickback = kickback;
    }

    public Boolean getKickback() {
        return Kickback;
    }


    public boolean isKickback() {
        return Kickback;
    }

    public void setKickback(boolean kickback) {
        Kickback = kickback;
    }

    public String getCodeAlternate() {
        return CodeAlternate;
    }

    public void setCodeAlternate(String codeAlternate) {
        CodeAlternate = codeAlternate;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getParentAccount() {
        return ParentAccount;
    }

    public void setParentAccount(String parentAccount) {
        ParentAccount = parentAccount;
    }

    public String getCodeRecommended() {
        return CodeRecommended;
    }

    public void setCodeRecommended(String codeRecommended) {
        CodeRecommended = codeRecommended;
    }

    public List<BankDetailsModel> getBankDetails() {
        return BankDetails;
    }

    public void setBankDetails(List<BankDetailsModel> bankDetails) {
        BankDetails = bankDetails;
    }

    public List<CashBankOutNoModel> getMsisdn() {
        return Msisdn;
    }

    public void setMsisdn(List<CashBankOutNoModel> msisdn) {
        Msisdn = msisdn;
    }

    public boolean isPaymentGateway() {
        return PaymentGateway;
    }

    public void setPaymentGateway(boolean paymentGateway) {
        PaymentGateway = paymentGateway;
    }

    public boolean isLoyalty() {
        return Loyalty;
    }

    public void setLoyalty(boolean loyalty) {
        Loyalty = loyalty;
    }

    public String getSecureToken() {
        return SecureToken;
    }

    public void setSecureToken(String secureToken) {
        SecureToken = secureToken;
    }


    public String getFBEmailId() {
        return FBEmailId;
    }

    public void setFBEmailId(String FBEmailId) {
        this.FBEmailId = FBEmailId;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getAgentAuthCode() {
        return AgentAuthCode;
    }

    public void setAgentAuthCode(String agentAuthCode) {
        AgentAuthCode = agentAuthCode;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getPhoneModel() {
        return PhoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        PhoneModel = phoneModel;
    }

    public String getPhonebrand() {
        return Phonebrand;
    }

    public void setPhonebrand(String phonebrand) {
        Phonebrand = phonebrand;
    }

    public String getOsVersion() {
        return OsVersion;
    }

    public void setOsVersion(String osVersion) {
        OsVersion = osVersion;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public DeviceInformationModel getDeviceInformation() {
        return DeviceInfo;
    }

    public void setDeviceInformation(DeviceInformationModel deviceInformation) {
        DeviceInfo = deviceInformation;
    }

    // ADDED FOR NEW UPDATED REGISTRATION

    public String getIdPhoto() {
        return IdPhoto;
    }

    public void setIdPhoto(String idPhoto) {
        IdPhoto = idPhoto;
    }

    public String getIdPhoto1() {
        return IdPhoto1;
    }

    public void setIdPhoto1(String idPhoto1) {
        IdPhoto1 = idPhoto1;
    }

    public String getCountryOfCitizen() {
        return CountryOfCitizen;
    }

    public void setCountryOfCitizen(String countryOfCitizen) {
        CountryOfCitizen = countryOfCitizen;
    }

    public String getIdExpDate() {
        return IdExpDate;
    }

    public void setIdExpDate(String idExpDate) {
        this.IdExpDate = idExpDate;
    }

    public String getHouseBlockNo() {
        return HouseBlockNo;
    }

    public void setHouseBlockNo(String houseBlockNo) {
        HouseBlockNo = houseBlockNo;
    }

    public String getFloorNumber() {
        return FloorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        FloorNumber = floorNumber;
    }

    public String getRoomNumber() {
        return RoomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        RoomNumber = roomNumber;
    }

    public String getHouseName() {
        return HouseName;
    }

    public void setHouseName(String houseName) {
        HouseName = houseName;
    }

    public String getVillageName() {
        return VillageName;
    }

    public void setVillageName(String villageName) {
        VillageName = villageName;
    }

    public List<String> getClosedDays() {
        return ClosedDays;
    }

    public void setClosedDays(List<String> closedDays) {
        ClosedDays = closedDays;
    }

    public String getBusinessIdPhoto1() {
        return BusinessIdPhoto1;
    }

    public void setBusinessIdPhoto1(String businessIdPhoto1) {
        BusinessIdPhoto1 = businessIdPhoto1;
    }

    public String getBusinessIdPhoto2() {
        return BusinessIdPhoto2;
    }

    public void setBusinessIdPhoto2(String businessIdPhoto2) {
        BusinessIdPhoto2 = businessIdPhoto2;
    }

    public String getOpenTime() {
        return OpenTime;
    }

    public void setOpenTime(String openTime) {
        OpenTime = openTime;
    }

    public String getCloseTime() {
        return CloseTime;
    }

    public void setCloseTime(String closeTime) {
        CloseTime = closeTime;
    }

    public String getSecurityQuestionCode() {
        return SecurityQuestionCode;
    }

    public void setSecurityQuestionCode(String securityQuestionCode) {
        SecurityQuestionCode = securityQuestionCode;
    }

    public String getSecurityQuestionAnswer() {
        return SecurityQuestionAnswer;
    }

    public void setSecurityQuestionAnswer(String securityQuestionAnswer) {
        SecurityQuestionAnswer = securityQuestionAnswer;
    }

    public int getRegistrationStatus() {
        return RegistrationStatus;
    }

    public void setRegistrationStatus(int registrationStatus) {
        RegistrationStatus = registrationStatus;
    }

    // ADDED ALREADY
    public String getPasswordType() {
        return PasswordType;
    }

    public void setPasswordType(String passwordType) {
        PasswordType = passwordType;
    }

    public String getLoginIdentityType() {
        return LoginIdentityType;
    }

    public void setLoginIdentityType(String loginIdentityType) {
        LoginIdentityType = loginIdentityType;
    }

    public String getLoginAppKey() {
        return LoginAppKey;
    }

    public void setLoginAppKey(String loginAppKey) {
        LoginAppKey = loginAppKey;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}