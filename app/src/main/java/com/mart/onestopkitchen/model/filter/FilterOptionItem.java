package com.mart.onestopkitchen.model.filter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilterOptionItem implements Serializable {

    @SerializedName("ItemSubName")
    private String itemSubName;
    @SerializedName("SelectedItem")
    private boolean selectedItem = false;

    public boolean isSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(boolean selectedItem) {
        this.selectedItem = selectedItem;
    }

    public String getItemSubName() {
        return itemSubName;
    }

    public void setItemSubName(String itemSubName) {
        this.itemSubName = itemSubName;
    }
}