package com.mart.onestopkitchen.model.okdollar.model;

/**
 * Created by kapil on 24/10/15.
 */
public class CashBankOutNoModel {

    private String Msisidn = "";
    private String Countrycode = "";

    public String getMsisidn() {
        return Msisidn;
    }

    public void setMsisidn(String msisidn) {
        Msisidn = msisidn;
    }

    public String getCountrycode() {
        return Countrycode;
    }

    public void setCountrycode(String countrycode) {
        Countrycode = countrycode;
    }
}
