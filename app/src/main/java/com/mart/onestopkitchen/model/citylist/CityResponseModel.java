package com.mart.onestopkitchen.model.citylist;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityResponseModel {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("status")
    private Status status;

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}