package com.mart.onestopkitchen.model.appratingandreview

import com.google.gson.annotations.SerializedName

data class AppReviewAndRating(

        @field:SerializedName("Rating")
        var rating: Int? = null,

        @field:SerializedName("ReviewText")
        var reviewText: String? = null,

        @field:SerializedName("OrderNo")
        var orderNo: String? = null,

        @field:SerializedName("ReviewType")
        var reviewType: Int? = null
)