package com.mart.onestopkitchen.model.featureproduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataItem implements Serializable {

    @SerializedName("ReviewOverviewModel")
    private ReviewOverviewModel reviewOverviewModel;

    @SerializedName("DefaultPictureModel")
    private DefaultPictureModel defaultPictureModel;

    @SerializedName("Form")
    private Object form;

    @SerializedName("ProductPrice")
    @Expose
    private ProductPrice productPrice;

    @SerializedName("Id")
    private int id;

    @SerializedName("Sku")
    private String sku;

    @SerializedName("Name")
    private String name;

    @SerializedName("ShortDescription")
    private String shortDescription;

    @SerializedName("CustomProperties")
    private CustomProperties customProperties;

    public ReviewOverviewModel getReviewOverviewModel() {
        return reviewOverviewModel;
    }

    public void setReviewOverviewModel(ReviewOverviewModel reviewOverviewModel) {
        this.reviewOverviewModel = reviewOverviewModel;
    }

    public DefaultPictureModel getDefaultPictureModel() {
        return defaultPictureModel;
    }

    public void setDefaultPictureModel(DefaultPictureModel defaultPictureModel) {
        this.defaultPictureModel = defaultPictureModel;
    }

    public Object getForm() {
        return form;
    }

    public void setForm(Object form) {
        this.form = form;
    }

    public ProductPrice getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        this.productPrice = productPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public CustomProperties getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }
}