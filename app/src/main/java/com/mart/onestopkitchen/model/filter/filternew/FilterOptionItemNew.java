package com.mart.onestopkitchen.model.filter.filternew;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilterOptionItemNew implements Serializable {

    @SerializedName("ItemSubName")
    private String itemSubName = "";
    @SerializedName("SelectedItem")
    private boolean selectedItem = false;

    @SerializedName("FilterId")
    private int filterId = 0;

    @SerializedName("ProductId")
    private int productId = 0;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getFilterId() {
        return filterId;
    }

    public void setFilterId(int filterId) {
        this.filterId = filterId;
    }

    public boolean isSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(boolean selectedItem) {
        this.selectedItem = selectedItem;
    }

    public String getItemSubName() {
        return itemSubName;
    }

    public void setItemSubName(String itemSubName) {
        this.itemSubName = itemSubName;
    }
}