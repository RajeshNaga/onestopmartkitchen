package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Updated by AkashGarg on 25/12/2018.
 */
public class ReviewModel implements Serializable {

    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("RatingSum")
    @Expose
    private Float ratingSum = 0.0F;
    @SerializedName("AllowCustomerReviews")
    @Expose
    private Boolean allowCustomerReviews;
    @SerializedName("TotalReviews")
    @Expose
    private Integer totalReviews;

    @SerializedName("RatingUrlFromGSMArena")
    @Expose
    private String ratingFromGSMArena;

    public String getRatingFromGSMArena() {
        return ratingFromGSMArena;
    }

    public void setRatingFromGSMArena(String ratingFromGSMArena) {
        this.ratingFromGSMArena = ratingFromGSMArena;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Float getRatingSum() {
        return ratingSum;
    }

    public void setRatingSum(Float ratingSum) {
        this.ratingSum = ratingSum;
    }

    public Boolean getAllowCustomerReviews() {
        return allowCustomerReviews;
    }

    public void setAllowCustomerReviews(Boolean allowCustomerReviews) {
        this.allowCustomerReviews = allowCustomerReviews;
    }

    public Integer getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(Integer totalReviews) {
        this.totalReviews = totalReviews;
    }

}


