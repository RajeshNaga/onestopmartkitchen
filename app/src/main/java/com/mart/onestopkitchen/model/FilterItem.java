package com.mart.onestopkitchen.model;

import java.io.Serializable;

/**
 * Created by Ashraful on 12/15/2015.
 */
public class FilterItem implements Serializable {
    private String SpecificationAttributeName;
    private String SpecificationAttributeOptionName;
    private int FilterId;
    private int ProductId;

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public int getFilterId() {
        return FilterId;
    }

    public void setFilterId(int filterId) {
        FilterId = filterId;
    }

    public String getSpecificationAttributeName() {
        return SpecificationAttributeName;
    }

    public void setSpecificationAttributeName(String specificationAttributeName) {
        SpecificationAttributeName = specificationAttributeName;
    }

    public String getSpecificationAttributeOptionName() {
        return SpecificationAttributeOptionName;
    }

    public void setSpecificationAttributeOptionName(String specificationAttributeOptionName) {
        SpecificationAttributeOptionName = specificationAttributeOptionName;
    }


}
