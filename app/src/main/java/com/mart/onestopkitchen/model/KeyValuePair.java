package com.mart.onestopkitchen.model;

/**
 * Created by Ashraful on 11/27/2015.
 */
public class KeyValuePair {
    private String Value;
    private String Key;

    public KeyValuePair(String key, String value) {
        this.Value = value;
        this.Key = key;
    }

    public KeyValuePair() {

    }

    public KeyValuePair(String value) {
        this.Value = value;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    @Override
    public String toString() {
        return Key + ":" + Value;
    }
}
