package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Updated by AkashGarg on 10/22/2018.
 */

public class PickupPointItem {

    @SerializedName("Id")
    private long id;

    @SerializedName("Name")
    private String name = "";

    @SerializedName("Description")
    private String description = "";

    @SerializedName("ProviderSystemName")
    private String providerSystemName;

    @SerializedName("Address")
    private String address = "";

    @SerializedName("City")
    private String city = "";


    @SerializedName("CountryName")
    private String countryName = "";

    @SerializedName("ZipPostalCode")
    private String zipPostalCode = "";

    @SerializedName("PickupFee")
    private String pickupFee = "";

    @SerializedName("Latitude")
    @Expose
    private Object latitude;
    @SerializedName("Longitude")
    @Expose
    private Object longitude;
    @SerializedName("OpeningHours")
    @Expose
    private String openingHours = "";

    @SerializedName("Distance")
    @Expose
    private String distance = "";

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProviderSystemName() {
        return providerSystemName;
    }

    public void setProviderSystemName(String providerSystemName) {
        this.providerSystemName = providerSystemName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getZipPostalCode() {
        return zipPostalCode;
    }

    public void setZipPostalCode(String zipPostalCode) {
        this.zipPostalCode = zipPostalCode;
    }

    public String getPickupFee() {
        return pickupFee;
    }

    public void setPickupFee(String pickupFee) {
        this.pickupFee = pickupFee;
    }

    @Override
    public String toString() {
        return
                "id=" + id +
                        ", name='" + name + '\'' +
                        ", description='" + description + '\'' +
                        ", providerSystemName='" + providerSystemName + '\'' +
                        ", address='" + address + '\'' +
                        ", city='" + city + '\'' +
                        ", countryName='" + countryName + '\'' +
                        ", zipPostalCode='" + zipPostalCode + '\'' +
                        ", pickupFee='" + pickupFee + '\'' +
                        ", latitude=" + latitude +
                        ", longitude=" + longitude +
                        ", openingHours='" + openingHours;
    }
}
