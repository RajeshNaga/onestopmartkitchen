package com.mart.onestopkitchen.model.cashin.cashinresponsemodel;

import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.Location;

import java.io.Serializable;

public class ContentItem implements Serializable {

    private double userLatitude;
    private double userLongitude;
    @SerializedName("Email")
    private String email;
    @SerializedName("Category")
    private String category;
    @SerializedName("Address")
    private String address;
    @SerializedName("IsActive")
    private boolean isActive;
    @SerializedName("Rating")
    private Double rating;
    @SerializedName("OpeningTime")
    private String openingTime;
    @SerializedName("Gender")
    private int gender;
    @SerializedName("ModifiedDate")
    private String modifiedDate;
    @SerializedName("OkDollarBalance")
    private int okDollarBalance;
    @SerializedName("TotalRateCount")
    private int totalRateCount;
    @SerializedName("AppVersion")
    private String appVersion;
    @SerializedName("HomeLocation")
    private Location homeLocation;
    @SerializedName("CountryCode")
    private String countryCode;
    @SerializedName("CurrentLocation")
    private CurrentLocation currentLocation;
    @SerializedName("Password")
    private Object password;
    @SerializedName("ClosingTime")
    private String closingTime;
    @SerializedName("IsDeleted")
    private boolean isDeleted;
    @SerializedName("DivisionName")
    private Object divisionName;
    @SerializedName("UserName")
    private Object userName;
    @SerializedName("CreatedBy")
    private String createdBy;
    @SerializedName("BusinessName")
    private String businessName;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("CityName")
    private String cityName;
    @SerializedName("CellId")
    private String cellId;
    @SerializedName("TransactionId")
    private String transactionId;
    @SerializedName("MessageRequestRejectedTime")
    private String messageRequestRejectedTime;
    @SerializedName("AgentType")
    private int agentType;
    @SerializedName("Type")
    private int type;
    @SerializedName("IsOpenAlways")
    private boolean isOpenAlways;
    @SerializedName("State")
    private String state;
    @SerializedName("CashOut")
    private boolean cashOut;
    @SerializedName("SubCategory")
    private String subCategory;
    @SerializedName("CreatedDate")
    private String createdDate;
    @SerializedName("PhoneNumber")
    private String phoneNumber;
    @SerializedName("CashIn")
    private boolean cashIn;
    @SerializedName("Country")
    private String country;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("Id")
    private String id;
    @SerializedName("TownShipName")
    private String townShipName;
    @SerializedName("Line1")
    private String line1;
    @SerializedName("Line2")
    private String line2;
    @SerializedName("IsOpen")
    private boolean isOpen;

    public double getUserLatitude() {
        return userLatitude;
    }

    public void setUserLatitude(double userLatitude) {
        this.userLatitude = userLatitude;
    }

    public double getUserLongitude() {
        return userLongitude;
    }

    public void setUserLongitude(double userLongitude) {
        this.userLongitude = userLongitude;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public int getOkDollarBalance() {
        return okDollarBalance;
    }

    public void setOkDollarBalance(int okDollarBalance) {
        this.okDollarBalance = okDollarBalance;
    }

    public int getTotalRateCount() {
        return totalRateCount;
    }

    public void setTotalRateCount(int totalRateCount) {
        this.totalRateCount = totalRateCount;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Location getHomeLocation() {
        return homeLocation;
    }

    public void setHomeLocation(Location homeLocation) {
        this.homeLocation = homeLocation;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public CurrentLocation getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(CurrentLocation currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Object getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(Object divisionName) {
        this.divisionName = divisionName;
    }

    public Object getUserName() {
        return userName;
    }

    public void setUserName(Object userName) {
        this.userName = userName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCellId() {
        return cellId;
    }

    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMessageRequestRejectedTime() {
        return messageRequestRejectedTime;
    }

    public void setMessageRequestRejectedTime(String messageRequestRejectedTime) {
        this.messageRequestRejectedTime = messageRequestRejectedTime;
    }

    public int getAgentType() {
        return agentType;
    }

    public void setAgentType(int agentType) {
        this.agentType = agentType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isIsOpenAlways() {
        return isOpenAlways;
    }

    public void setIsOpenAlways(boolean isOpenAlways) {
        this.isOpenAlways = isOpenAlways;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isCashOut() {
        return cashOut;
    }

    public void setCashOut(boolean cashOut) {
        this.cashOut = cashOut;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isCashIn() {
        return cashIn;
    }

    public void setCashIn(boolean cashIn) {
        this.cashIn = cashIn;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTownShipName() {
        return townShipName;
    }

    public void setTownShipName(String townShipName) {
        this.townShipName = townShipName;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public boolean isIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }
}