package com.mart.onestopkitchen.model;

import com.mart.onestopkitchen.networking.BaseResponse;


public class ForgetResponse extends BaseResponse {
    private String SuccessMessage;

    public String getSuccessMessage() {
        return SuccessMessage;
    }

    public void setSuccessMessage(String successMessage) {
        SuccessMessage = successMessage;
    }
}
