package com.mart.onestopkitchen.model.filter.selected;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SelectedModel implements Serializable {

    @SerializedName("Filter")
    private List<FilterItem> filter;

    public List<FilterItem> getFilter() {
        return filter;
    }

    public void setFilter(List<FilterItem> filter) {
        this.filter = filter;
    }
}