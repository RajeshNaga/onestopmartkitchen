package com.mart.onestopkitchen.model;

import java.io.Serializable;

/**
 * Created by Ashraful on 11/9/2015.
 */
public class ImageModel implements Serializable {

    private int IsProduct;
    private int ProdOrCatId;
    private String CategoryName;
    private String ImageUrl;

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public int getIsProduct() {
        return IsProduct;
    }

    public void setIsProduct(int isProduct) {
        IsProduct = isProduct;
    }

    public int getProdOrCatId() {
        return ProdOrCatId;
    }

    public void setProdOrCatId(int prodOrCatId) {
        ProdOrCatId = prodOrCatId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }
}
