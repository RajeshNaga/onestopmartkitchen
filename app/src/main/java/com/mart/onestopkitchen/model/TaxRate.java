package com.mart.onestopkitchen.model;

/**
 * Created by Ashraful on 12/4/2015.
 */
public class TaxRate {
    public String Rate;
    public String Value;

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
