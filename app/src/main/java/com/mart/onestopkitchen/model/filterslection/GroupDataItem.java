package com.mart.onestopkitchen.model.filterslection;

import com.google.gson.annotations.SerializedName;

public class GroupDataItem {

    @SerializedName("IsSelected")
    private boolean isSelected;

    @SerializedName("ProductId")
    private int productId;

    @SerializedName("CateName")
    private String cateName;

    @SerializedName("FilterId")
    private int filterId;

    @SerializedName("OptionName")
    private String optionName;

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public int getFilterId() {
        return filterId;
    }

    public void setFilterId(int filterId) {
        this.filterId = filterId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }
}