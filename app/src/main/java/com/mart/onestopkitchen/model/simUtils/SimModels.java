package com.mart.onestopkitchen.model.simUtils;

public class SimModels {
    String Msids = "";
    String SimId = "";
    String operator = "";

    public String getMsids() {
        return Msids;
    }

    public void setMsids(String msids) {
        Msids = msids;
    }

    public String getSimId() {
        return SimId;
    }

    public void setSimId(String simId) {
        SimId = simId;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
