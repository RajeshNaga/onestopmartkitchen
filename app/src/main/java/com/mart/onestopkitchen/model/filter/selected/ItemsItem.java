package com.mart.onestopkitchen.model.filter.selected;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemsItem implements Serializable {

    @SerializedName("subItme")
    private String subItme;

    @SerializedName("id")
    private int id;
    @SerializedName("ProductId")
    private int productId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubItme() {
        return subItme;
    }

    public void setSubItme(String subItme) {
        this.subItme = subItme;
    }
}