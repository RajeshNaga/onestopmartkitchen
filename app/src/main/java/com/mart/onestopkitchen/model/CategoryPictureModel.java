package com.mart.onestopkitchen.model;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Hemnath on 9/24/2018.
 */

public class CategoryPictureModel extends BaseResponse {


    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
