package com.mart.onestopkitchen.model;

/**
 * Created by Ashraful on 12/8/2015.
 */
public class AvailableState {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
