package com.mart.onestopkitchen.model.okdollar;

import java.io.Serializable;

public class OKDollarLoginInfo implements Serializable {
    private String userName;
    private String passWord;
    private String mobilNumber;
    private String name;
    private String countyCode;
    private String state;
    private String townShip;
    private String gender;
    private String dob;
    private String address1;
    private String address2;
    private String email;
    private String password;
    private String villageName;
    private String profilePicture;
    private String simId;
    private String deviceId;
    private String FBEmailId;
    private String Country;
    private String HouseBlockNo;
    private String FloorNumber;
    private String RoomNumber;
    private String registerdLang;

    public String getRegisterdLang() {
        return registerdLang;
    }

    public void setRegisterdLang(String registerdLang) {
        this.registerdLang = registerdLang;
    }

    public String getFBEmailId() {
        return FBEmailId;
    }

    public void setFBEmailId(String FBEmailId) {
        this.FBEmailId = FBEmailId;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }


    public String getHouseBlockNo() {
        return HouseBlockNo;
    }

    public void setHouseBlockNo(String houseBlockNo) {
        HouseBlockNo = houseBlockNo;
    }

    public String getFloorNumber() {
        return FloorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        FloorNumber = floorNumber;
    }

    public String getRoomNumber() {
        return RoomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        RoomNumber = roomNumber;
    }


    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSimId() {
        return simId;
    }

    public void setSimId(String simId) {
        this.simId = simId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getMobilNumber() {
        return mobilNumber;
    }

    public void setMobilNumber(String mobilNumber) {
        this.mobilNumber = mobilNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTownShip() {
        return townShip;
    }

    public void setTownShip(String townShip) {
        this.townShip = townShip;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
