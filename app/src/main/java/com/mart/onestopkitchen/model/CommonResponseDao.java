package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AkashGarg on 5/28/2018.
 */

public class CommonResponseDao {
    @SerializedName("Code")
    @Expose
    private Integer code;
    @SerializedName("Data")
    @Expose
    private String data;
    @SerializedName("Msg")
    @Expose
    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
