package com.mart.onestopkitchen.model.ratingreview

import com.google.gson.annotations.SerializedName

data class RatingReviewRequestModel(

        @field:SerializedName("Rating")
        var rating: String? = null,

        @field:SerializedName("ReviewText")
        var reviewText: String? = null,

        @field:SerializedName("Title")
        var title: String? = null
)