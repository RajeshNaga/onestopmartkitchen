package com.mart.onestopkitchen.model.otpreceived;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtpResponseModel implements Serializable {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Status")
    @Expose
    private String status;

    @SerializedName("OTP")
    @Expose
    private String OTP;

    @SerializedName("StatusCode")
    @Expose
    private int StatusCode;

    @SerializedName("IsUserRegistered")
    @Expose
    private boolean isUserRegistered;

    @SerializedName("HashKey")
    @Expose
    private String HashKey = "";

    @SerializedName("DestinationNumber")
    @Expose
    private String DestinationNumber="";

    @SerializedName("AppVersionName")
    @Expose
    private String AppVersionName="";



    public boolean isUserRegistered() {
        return isUserRegistered;
    }

    public void setUserRegistered(boolean userRegistered) {
        isUserRegistered = userRegistered;
    }

    public String getHashKey() {
        return HashKey;
    }

    public void setHashKey(String hashKey) {
        HashKey = hashKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDestinationNumber() {
        return DestinationNumber;
    }

    public void setDestinationNumber(String destinationNumber) {
        DestinationNumber = destinationNumber;
    }

    public String getAppVersionName() {
        return AppVersionName;
    }

    public void setAppVersionName(String appVersionName) {
        AppVersionName = appVersionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

}
