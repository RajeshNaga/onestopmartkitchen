package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Updated by AkashGarg on 25/12/2018.
 */
public class BaseProductModel implements Serializable {

    @SerializedName("Name")
    @Expose
    private String Name = "";

    @SerializedName("DefaultPictureModel")
    @Expose
    private DefaultPictureModel DefaultPictureModel;

    @SerializedName("Id")
    @Expose
    private long Id = 0L;

    private int count;

    private int ProductCount = 0;

    @SerializedName("Sku")
    @Expose
    private String sku;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public DefaultPictureModel getDefaultPictureModel() {
        return DefaultPictureModel;
    }

    public void setDefaultPictureModel(DefaultPictureModel defaultPictureModel) {
        DefaultPictureModel = defaultPictureModel;
    }

    public int getProductCount() {
        return ProductCount;
    }

    public void setProductCount(int productCount) {
        ProductCount = productCount;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }


}
