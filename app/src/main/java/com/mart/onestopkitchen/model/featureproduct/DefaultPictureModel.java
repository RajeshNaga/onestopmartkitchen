package com.mart.onestopkitchen.model.featureproduct;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DefaultPictureModel implements Serializable {

    @SerializedName("FullSizeImageUrl")
    private Object fullSizeImageUrl;

    @SerializedName("Form")
    private Object form;

    @SerializedName("AlternateText")
    private Object alternateText;

    @SerializedName("ImageUrl")
    private String imageUrl;

    @SerializedName("Title")
    private Object title;

    @SerializedName("CustomProperties")
    private CustomProperties customProperties;

    public Object getFullSizeImageUrl() {
        return fullSizeImageUrl;
    }

    public void setFullSizeImageUrl(Object fullSizeImageUrl) {
        this.fullSizeImageUrl = fullSizeImageUrl;
    }

    public Object getForm() {
        return form;
    }

    public void setForm(Object form) {
        this.form = form;
    }

    public Object getAlternateText() {
        return alternateText;
    }

    public void setAlternateText(Object alternateText) {
        this.alternateText = alternateText;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public CustomProperties getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }
}