package com.mart.onestopkitchen.model.addsubmenu;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubItemsItem implements Serializable {

    @SerializedName("SubItemName")
    private String subItemName;

    @SerializedName("SubItemId")
    private int subItemId;

    public String getSubItemName() {
        return subItemName;
    }

    public void setSubItemName(String subItemName) {
        this.subItemName = subItemName;
    }

    public int getSubItemId() {
        return subItemId;
    }

    public void setSubItemId(int subItemId) {
        this.subItemId = subItemId;
    }
}