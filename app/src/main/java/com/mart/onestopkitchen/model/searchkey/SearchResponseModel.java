package com.mart.onestopkitchen.model.searchkey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResponseModel {

    @SerializedName("SuccessMessage")
    private String successMessage;

    @SerializedName("ErrorList")
    //private List<Object> errorList;
    private String[] errorList;
    @SerializedName("Data")

    private List<String> data;
    @SerializedName("StatusCode")
    private int statusCode;

    public String[] getErrorList() {
        return errorList;
    }

    public void setErrorList(String[] errorList) {
        this.errorList = errorList;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}