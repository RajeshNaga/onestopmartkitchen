package com.mart.onestopkitchen.model.okdollar.model;

/**
 * Created by LENOVO on 7/29/2016.
 */
public class DeviceInformationModel {

    private String DeviceSoftwareVersion = "";
    private String NetworkCountryIso = "";
    private String NetworkOperator = "";
    private String NetworkOperatorName = "";
    private String NetworkType = "";
    private String PhoneType = "";
    private String SIMCountryIso = "";
    private String SIMOperator = "";
    private String SIMOperatorName = "";
    private String VoiceMailNo = "";
    private boolean isNetworkRoaming;
    private String BSSID = "";
    private String MACAddress = "";
    private boolean HiddenSSID;
    private String IPAddress = "";
    private int LinkSpeed;
    private int NetworkID;
    private String SSID = "";
    private int NetworkSignal;
    private String BluetoothAddress = "";
    private String BluetoothName = "";
    private String ConnectedNetworkType = "";

    public String getDeviceSoftwareVersion() {
        return DeviceSoftwareVersion;
    }

    public void setDeviceSoftwareVersion(String deviceSoftwareVersion) {
        DeviceSoftwareVersion = deviceSoftwareVersion;
    }

    public String getNetworkCountryIso() {
        return NetworkCountryIso;
    }

    public void setNetworkCountryIso(String networkCountryIso) {
        NetworkCountryIso = networkCountryIso;
    }

    public String getNetworkOperator() {
        return NetworkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        NetworkOperator = networkOperator;
    }

    public String getNetworkOperatorName() {
        return NetworkOperatorName;
    }

    public void setNetworkOperatorName(String networkOperatorName) {
        NetworkOperatorName = networkOperatorName;
    }

    public String getNetworkType() {
        return NetworkType;
    }

    public void setNetworkType(String networkType) {
        NetworkType = networkType;
    }

    public String getPhoneType() {
        return PhoneType;
    }

    public void setPhoneType(String phoneType) {
        PhoneType = phoneType;
    }

    public String getSIMCountryIso() {
        return SIMCountryIso;
    }

    public void setSIMCountryIso(String SIMCountryIso) {
        this.SIMCountryIso = SIMCountryIso;
    }

    public String getSIMOperator() {
        return SIMOperator;
    }

    public void setSIMOperator(String SIMOperator) {
        this.SIMOperator = SIMOperator;
    }

    public String getSIMOperatorName() {
        return SIMOperatorName;
    }

    public void setSIMOperatorName(String SIMOperatorName) {
        this.SIMOperatorName = SIMOperatorName;
    }

    public String getVoiceMailNo() {
        return VoiceMailNo;
    }

    public void setVoiceMailNo(String voiceMailNo) {
        VoiceMailNo = voiceMailNo;
    }

    public boolean isNetworkRoaming() {
        return isNetworkRoaming;
    }

    public void setIsNetworkRoaming(boolean isNetworkRoaming) {
        this.isNetworkRoaming = isNetworkRoaming;
    }

    public String getBSSID() {
        return BSSID;
    }

    public void setBSSID(String BSSID) {
        this.BSSID = BSSID;
    }

    public String getMACAddress() {
        return MACAddress;
    }

    public void setMACAddress(String MACAddress) {
        this.MACAddress = MACAddress;
    }

    public boolean isHiddenSSID() {
        return HiddenSSID;
    }

    public void setHiddenSSID(boolean hiddenSSID) {
        HiddenSSID = hiddenSSID;
    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public int getLinkSpeed() {
        return LinkSpeed;
    }

    public void setLinkSpeed(int linkSpeed) {
        LinkSpeed = linkSpeed;
    }

    public int getNetworkID() {
        return NetworkID;
    }

    public void setNetworkID(int networkID) {
        NetworkID = networkID;
    }

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public int getNetworkSignal() {
        return NetworkSignal;
    }

    public void setNetworkSignal(int networkSignal) {
        NetworkSignal = networkSignal;
    }

    public String getBluetoothAddress() {
        return BluetoothAddress;
    }

    public void setBluetoothAddress(String bluetoothAddress) {
        BluetoothAddress = bluetoothAddress;
    }

    public String getBluetoothName() {
        return BluetoothName;
    }

    public void setBluetoothName(String bluetoothName) {
        BluetoothName = bluetoothName;
    }

    public String getConnectedNetworkType() {
        return ConnectedNetworkType;
    }

    public void setConnectedNetworkType(String connectedNetworkType) {
        ConnectedNetworkType = connectedNetworkType;
    }

}
