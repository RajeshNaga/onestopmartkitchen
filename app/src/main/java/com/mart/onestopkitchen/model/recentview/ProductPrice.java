package com.mart.onestopkitchen.model.recentview;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductPrice implements Serializable {

    @SerializedName("Form")
    private Object form;

    @SerializedName("CustomerEntersPrice")
    private boolean customerEntersPrice;

    @SerializedName("ProductId")
    private int productId;

    @SerializedName("RentalPrice")
    private Object rentalPrice;

    @SerializedName("DiscountOfferName")
    private String discountOfferName;

    @SerializedName("CurrencyCode")
    private String currencyCode;

    @SerializedName("DisplayTaxShippingInfo")
    private boolean displayTaxShippingInfo;

    @SerializedName("PriceWithDiscount")
    private String priceWithDiscount;

    @SerializedName("HidePrices")
    private boolean hidePrices;

    @SerializedName("DiscountPercentage")
    private int discountPercentage;

    @SerializedName("Price")
    private String price;

    @SerializedName("CallForPrice")
    private boolean callForPrice;

    @SerializedName("BasePricePAngV")
    private Object basePricePAngV;

    @SerializedName("PriceWithDiscountValue")
    private int priceWithDiscountValue;

    @SerializedName("PriceValue")
    private int priceValue;

    @SerializedName("IsRental")
    private boolean isRental;

    @SerializedName("OldPrice")
    private String oldPrice;

    @SerializedName("CustomProperties")
    private CustomProperties customProperties;

    public Object getForm() {
        return form;
    }

    public void setForm(Object form) {
        this.form = form;
    }

    public boolean isCustomerEntersPrice() {
        return customerEntersPrice;
    }

    public void setCustomerEntersPrice(boolean customerEntersPrice) {
        this.customerEntersPrice = customerEntersPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Object getRentalPrice() {
        return rentalPrice;
    }

    public void setRentalPrice(Object rentalPrice) {
        this.rentalPrice = rentalPrice;
    }

    public String getDiscountOfferName() {
        return discountOfferName;
    }

    public void setDiscountOfferName(String discountOfferName) {
        this.discountOfferName = discountOfferName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public boolean isDisplayTaxShippingInfo() {
        return displayTaxShippingInfo;
    }

    public void setDisplayTaxShippingInfo(boolean displayTaxShippingInfo) {
        this.displayTaxShippingInfo = displayTaxShippingInfo;
    }

    public String getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(String priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public boolean isHidePrices() {
        return hidePrices;
    }

    public void setHidePrices(boolean hidePrices) {
        this.hidePrices = hidePrices;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isCallForPrice() {
        return callForPrice;
    }

    public void setCallForPrice(boolean callForPrice) {
        this.callForPrice = callForPrice;
    }

    public Object getBasePricePAngV() {
        return basePricePAngV;
    }

    public void setBasePricePAngV(Object basePricePAngV) {
        this.basePricePAngV = basePricePAngV;
    }

    public int getPriceWithDiscountValue() {
        return priceWithDiscountValue;
    }

    public void setPriceWithDiscountValue(int priceWithDiscountValue) {
        this.priceWithDiscountValue = priceWithDiscountValue;
    }

    public int getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(int priceValue) {
        this.priceValue = priceValue;
    }

    public boolean isIsRental() {
        return isRental;
    }

    public void setIsRental(boolean isRental) {
        this.isRental = isRental;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public CustomProperties getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }
}