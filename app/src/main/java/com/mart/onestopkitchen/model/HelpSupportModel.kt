package com.mart.onestopkitchen.model

import java.io.Serializable

data class HelpSupportModel(val Data: List<HelpSupportDataItem>? = null) : Serializable
