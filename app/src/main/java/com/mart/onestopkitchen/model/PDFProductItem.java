package com.mart.onestopkitchen.model;

import com.mart.onestopkitchen.networking.BaseResponse;

import java.io.Serializable;

public class PDFProductItem extends BaseResponse implements Serializable {

    private int Id;
    private String OrderItemGuid;
    private String Sku;
    private String ProductId;
    private String ProductName;
    private String ProductSeName;
    private String UnitPrice;
    private String SubTotal;
    private String Quantity;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getOrderItemGuid() {
        return OrderItemGuid;
    }

    public void setOrderItemGuid(String orderItemGuid) {
        OrderItemGuid = orderItemGuid;
    }

    public String getSku() {
        return Sku;
    }

    public void setSku(String sku) {
        Sku = sku;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductSeName() {
        return ProductSeName;
    }

    public void setProductSeName(String productSeName) {
        ProductSeName = productSeName;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }


}

