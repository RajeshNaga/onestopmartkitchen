package com.mart.onestopkitchen.model.featureproduct;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReviewOverviewModel implements Serializable {

    @SerializedName("RatingSum")
    private Float ratingSum;

    @SerializedName("TotalReviews")
    private int totalReviews;

    @SerializedName("ProductId")
    private int productId;

    @SerializedName("AllowCustomerReviews")
    private boolean allowCustomerReviews;

    public Float getRatingSum() {
        return ratingSum;
    }

    public void setRatingSum(Float ratingSum) {
        this.ratingSum = ratingSum;
    }

    public int getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(int totalReviews) {
        this.totalReviews = totalReviews;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public boolean isAllowCustomerReviews() {
        return allowCustomerReviews;
    }

    public void setAllowCustomerReviews(boolean allowCustomerReviews) {
        this.allowCustomerReviews = allowCustomerReviews;
    }
}