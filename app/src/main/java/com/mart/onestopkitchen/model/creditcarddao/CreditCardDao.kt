package com.mart.onestopkitchen.model.creditcarddao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update


@Dao
interface CreditCardDao {

    @Insert(onConflict = REPLACE)
    fun insertCardDetails(card: CardDetailModel)

    @Query("Select * From cardDetailTable order by userId desc Limit 1 ")
    fun getCardDetails(): CardDetailModel

//    @Query("UPDATE cardDetailTable SET cardHolderName =:cardHolderName, cardNumber =:cardNumber, cardCvv =:cardCvv , cardExpMonth=:cardExpMonth , cardExpYear=:cardExpYear WHERE userId =:userId")
//    fun updateCardDetails(userId: Int, cardHolderName: String, cardNumber: String, cardCvv: String, cardExpMonth: String, cardExpYear: String)

    @Update
    fun updateCardDetails(card: CardDetailModel): Int

}