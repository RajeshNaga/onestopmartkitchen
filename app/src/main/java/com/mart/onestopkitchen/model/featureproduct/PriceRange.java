package com.mart.onestopkitchen.model.featureproduct;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PriceRange implements Serializable {

    @SerializedName("From")
    private int from;

    @SerializedName("To")
    private int to;

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}