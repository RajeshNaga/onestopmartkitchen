package com.mart.onestopkitchen.model;

import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 5/11/2016.
 */
public class AppThemeResponse extends BaseResponse {
    @SerializedName("Data")
    private AppTheme Data;

    public AppTheme getData() {
        return Data;
    }

    public void setData(AppTheme data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "Data=" + Data;
    }
}
