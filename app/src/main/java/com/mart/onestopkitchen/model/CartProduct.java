package com.mart.onestopkitchen.model;


import com.mart.onestopkitchen.model.ratingreview.RatingModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 11/30/2015.
 */
public class CartProduct {
    private Object Sku;
    private int ProductId;
    private String ProductName;
    private String ProductSeName;
    private String UnitPrice;
    private String DiscountPrice;
    private String SubTotal;
    private String Discount;
    private String Rating;
    private String RatingCount;
    private String DiscountPercentage;
    private String DiscountAmount;
    private String DiscountOfferName;
    private String PriceAfterDiscount;
    private String OldPrice;
    private String ActualPrice;
    private int AvailableQuantity;
    private int Quantity;
    private List<Object> AllowedQuantities = new ArrayList<Object>();
    private String AttributeInfo;
    private boolean AllowItemEditing;
    private List<Object> Warnings = new ArrayList<>();
    private int Id;
    private PictureModel Picture;
    private RatingModel ProductReviewOverview;
    private String DeliveryDays;
    private String Price;

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public int getAvailableQuantity() {
        return AvailableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        AvailableQuantity = availableQuantity;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getRatingCount() {
        return RatingCount;
    }

    public void setRatingCount(String ratingCount) {
        RatingCount = ratingCount;
    }

    public String getPriceAfterDiscount() {
        return PriceAfterDiscount;
    }

    public void setPriceAfterDiscount(String priceAfterDiscount) {
        PriceAfterDiscount = priceAfterDiscount;
    }

    public String getOldPrice() {
        return OldPrice;
    }

    public void setOldPrice(String oldPrice) {
        OldPrice = oldPrice;
    }

    public String getActualPrice() {
        return ActualPrice;
    }

    public void setActualPrice(String actualPrice) {
        ActualPrice = actualPrice;
    }

    public String getDiscountPrice() {
        return DiscountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        DiscountPrice = discountPrice;
    }

    public String getDeliveryDays() {
        return DeliveryDays;
    }

    public void setDeliveryDays(String deliveryDate) {
        this.DeliveryDays = deliveryDate;
    }

    public PictureModel getPicture() {
        return Picture;
    }

    public void setPicture(PictureModel picture) {
        Picture = picture;
    }


    public RatingModel getRatingModel() {
        return ProductReviewOverview;
    }

    public void setRatingModel(RatingModel productReviewOverview) {
        ProductReviewOverview = productReviewOverview;
    }


    public String getDiscountPercentage() {
        return DiscountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        DiscountPercentage = discountPercentage;
    }


    public String getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        DiscountAmount = discountAmount;
    }

    public String getDiscountOfferName() {
        return DiscountOfferName;
    }

    public void setDiscountOfferName(String discountOfferName) {
        DiscountOfferName = discountOfferName;
    }

    /**
     * @return The Sku
     */
    public Object getSku() {
        return Sku;
    }

    /**
     * @param Sku The Sku
     */
    public void setSku(Object Sku) {
        this.Sku = Sku;
    }

    /**
     * @return The ProductId
     */
    public int getProductId() {
        return ProductId;
    }

    /**
     * @param ProductId The ProductId
     */
    public void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }

    /**
     * @return The ProductName
     */
    public String getProductName() {
        return ProductName;
    }

    /**
     * @param ProductName The ProductName
     */
    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    /**
     * @return The ProductSeName
     */
    public String getProductSeName() {
        return ProductSeName;
    }

    /**
     * @param ProductSeName The ProductSeName
     */
    public void setProductSeName(String ProductSeName) {
        this.ProductSeName = ProductSeName;
    }

    /**
     * @return The UnitPrice
     */
    public String getUnitPrice() {
        return UnitPrice;
    }

    /**
     * @param UnitPrice The UnitPrice
     */
    public void setUnitPrice(String UnitPrice) {
        this.UnitPrice = UnitPrice;
    }

    /**
     * @return The SubTotal
     */
    public String getSubTotal() {
        return SubTotal;
    }

    /**
     * @param SubTotal The SubTotal
     */
    public void setSubTotal(String SubTotal) {
        this.SubTotal = SubTotal;
    }

    /**
     * @return The Discount
     */
    public String getDiscount() {
        return Discount;
    }

    /**
     * @param Discount The Discount
     */
    public void setDiscount(String Discount) {
        this.Discount = Discount;
    }

    /**
     * @return The Quantity
     */
    public int getQuantity() {
        return Quantity;
    }

    /**
     * @param Quantity The Quantity
     */
    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    /**
     * @return The AllowedQuantities
     */
    public List<Object> getAllowedQuantities() {
        return AllowedQuantities;
    }

    /**
     * @param AllowedQuantities The AllowedQuantities
     */
    public void setAllowedQuantities(List<Object> AllowedQuantities) {
        this.AllowedQuantities = AllowedQuantities;
    }

    /**
     * @return The AttributeInfo
     */
    public String getAttributeInfo() {
        return AttributeInfo;
    }

    /**
     * @param AttributeInfo The AttributeInfo
     */
    public void setAttributeInfo(String AttributeInfo) {
        this.AttributeInfo = AttributeInfo;
    }

    /**
     * @return The AllowItemEditing
     */
    public boolean isAllowItemEditing() {
        return AllowItemEditing;
    }

    /**
     * @param AllowItemEditing The AllowItemEditing
     */
    public void setAllowItemEditing(boolean AllowItemEditing) {
        this.AllowItemEditing = AllowItemEditing;
    }

    /**
     * @return The Warnings
     */
    public List<Object> getWarnings() {
        return Warnings;
    }

    /**
     * @param Warnings The Warnings
     */
    public void setWarnings(List<Object> Warnings) {
        this.Warnings = Warnings;
    }

    /**
     * @return The Id
     */
    public int getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(int Id) {
        this.Id = Id;
    }
}
