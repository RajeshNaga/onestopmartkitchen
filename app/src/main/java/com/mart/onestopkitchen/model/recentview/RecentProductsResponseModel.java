package com.mart.onestopkitchen.model.recentview;

import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.ProductModel;

import java.io.Serializable;
import java.util.List;

public class RecentProductsResponseModel implements Serializable {

    @SerializedName("VersionCode")
    private int versionCode;

    @SerializedName("SuccessMessage")
    private Object successMessage;

    @SerializedName("ErrorList")
    private List<Object> errorList;

    @SerializedName("Data")
    private List<ProductModel> data;

    @SerializedName("StatusCode")
    private int statusCode;

    @SerializedName("IsDeliveryAllowed")
    private boolean isDeliveryAllowed;

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public Object getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(Object successMessage) {
        this.successMessage = successMessage;
    }

    public List<Object> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Object> errorList) {
        this.errorList = errorList;
    }

    public List<ProductModel> getData() {
        return data;
    }

    public void setData(List<ProductModel> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isIsDeliveryAllowed() {
        return isDeliveryAllowed;
    }

    public void setIsDeliveryAllowed(boolean isDeliveryAllowed) {
        this.isDeliveryAllowed = isDeliveryAllowed;
    }
}