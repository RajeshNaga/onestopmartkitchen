package com.mart.onestopkitchen.model.subCategoryModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Hemnath on 9/24/2018.
 */

public class SubCategoryResponse implements Serializable {

    @Expose
    @SerializedName("SubCategories")
    List<SubCategoriesItem> subCategoriesItems;

    public List<SubCategoriesItem> getSubCategoriesItems() {
        return subCategoriesItems;
    }

    public void setSubCategoriesItems(List<SubCategoriesItem> subCategoriesItems) {
        this.subCategoriesItems = subCategoriesItems;
    }


}
