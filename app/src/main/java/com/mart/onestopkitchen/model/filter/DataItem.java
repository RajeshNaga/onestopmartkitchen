package com.mart.onestopkitchen.model.filter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DataItem implements Serializable {

    @SerializedName("FilterOption")
    private List<FilterOptionItem> filterOption;

    @SerializedName("ItemName")
    private String itemName;
    @SerializedName("SelectedList")
    private List<String> selectedName;

    public List<String> getSelectedName() {
        return selectedName;
    }

    public void setSelectedName(List<String> selectedName) {
        this.selectedName = selectedName;
    }

    public List<FilterOptionItem> getFilterOption() {
        return filterOption;
    }

    public void setFilterOption(List<FilterOptionItem> filterOption) {
        this.filterOption = filterOption;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}