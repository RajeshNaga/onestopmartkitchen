package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Updated by AkashGarg on 25/12/2018.
 */
public class ProductModel extends BaseProductModel implements Serializable {
    @SerializedName("ShortDescription")
    @Expose
    private String ShortDescription = "";

    @SerializedName("DisplayOrder")
    @Expose
    private int displayOrder = 0;

    @SerializedName("CreatedOn")
    @Expose
    private String createdOn = "";

    @SerializedName("ProductReviewOverview") //ReviewOverviewModel
    @Expose
    private ReviewModel ProductReviewOverview;

    @SerializedName("ProductPrice")
    @Expose
    private ProductPrice ProductPrice;

    @SerializedName("IsWishList")
    @Expose
    private Boolean isWishList = false;

    @SerializedName("MarkAsNew")
    @Expose
    private Boolean markAsNew = false;

    private String DeliveryDate = "";
    private String Url = "";

    public String getUrl() {
        return Url;
    }


    public void setUrl(String url) {
        Url = url;
    }

    public Boolean getMarkAsNew() {
        return markAsNew;
    }

    public void setMarkAsNew(Boolean markAsNew) {
        this.markAsNew = markAsNew;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }


    public Boolean getWishList() {
        return isWishList;
    }

    public void setWishList(Boolean wishList) {
        isWishList = wishList;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public ProductPrice getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        ProductPrice = productPrice;
    }


    public ReviewModel getReviewOverviewModel() {
        return ProductReviewOverview;
    }

    public void setReviewOverviewModel(ReviewModel reviewOverviewModel) {
        ProductReviewOverview = reviewOverviewModel;
    }

    public String getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        DeliveryDate = deliveryDate;
    }
}
