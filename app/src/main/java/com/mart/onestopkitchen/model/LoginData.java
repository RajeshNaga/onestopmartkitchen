package com.mart.onestopkitchen.model;

/**
 * Created by mart-110 on 12/9/2015.
 */
public class LoginData {
    public String Username;
    private String email;
    private String password;

    public LoginData(String username, String password) {
        this.email = username;
        this.Username = username;
        this.password = password;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }
}
