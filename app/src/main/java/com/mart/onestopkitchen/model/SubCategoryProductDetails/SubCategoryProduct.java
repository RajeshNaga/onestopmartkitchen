package com.mart.onestopkitchen.model.SubCategoryProductDetails;

import java.util.ArrayList;

/**
 * Created by Hemnath on 9/24/2018.
 */

public class SubCategoryProduct {
    private ArrayList<SubCategory> SubCategories;
    private ArrayList<Object> FeaturedProducts;
    private Object SuccessMessage;
    private int StatusCode;
    private ArrayList<Object> ErrorList;

    public ArrayList<SubCategory> getSubCategories() {
        return this.SubCategories;
    }

    public void setSubCategories(ArrayList<SubCategory> SubCategories) {
        this.SubCategories = SubCategories;
    }

    public ArrayList<Object> getFeaturedProducts() {
        return this.FeaturedProducts;
    }

    public void setFeaturedProducts(ArrayList<Object> FeaturedProducts) {
        this.FeaturedProducts = FeaturedProducts;
    }

    public Object getSuccessMessage() {
        return this.SuccessMessage;
    }

    public void setSuccessMessage(Object SuccessMessage) {
        this.SuccessMessage = SuccessMessage;
    }

    public int getStatusCode() {
        return this.StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public ArrayList<Object> getErrorList() {
        return this.ErrorList;
    }

    public void setErrorList(ArrayList<Object> ErrorList) {
        this.ErrorList = ErrorList;
    }

    public class CustomProperties {
    }

    public class PictureModel {
        private String ImageUrl;
        private Object ThumbImageUrl;
        private Object FullSizeImageUrl;
        private Object Title;
        private Object AlternateText;
        private Object Form;
        private CustomProperties CustomProperties;

        public String getImageUrl() {
            return this.ImageUrl;
        }

        public void setImageUrl(String ImageUrl) {
            this.ImageUrl = ImageUrl;
        }

        public Object getThumbImageUrl() {
            return this.ThumbImageUrl;
        }

        public void setThumbImageUrl(Object ThumbImageUrl) {
            this.ThumbImageUrl = ThumbImageUrl;
        }

        public Object getFullSizeImageUrl() {
            return this.FullSizeImageUrl;
        }

        public void setFullSizeImageUrl(Object FullSizeImageUrl) {
            this.FullSizeImageUrl = FullSizeImageUrl;
        }

        public Object getTitle() {
            return this.Title;
        }

        public void setTitle(Object Title) {
            this.Title = Title;
        }

        public Object getAlternateText() {
            return this.AlternateText;
        }

        public void setAlternateText(Object AlternateText) {
            this.AlternateText = AlternateText;
        }

        public Object getForm() {
            return this.Form;
        }

        public void setForm(Object Form) {
            this.Form = Form;
        }

        public CustomProperties getCustomProperties() {
            return this.CustomProperties;
        }

        public void setCustomProperties(CustomProperties CustomProperties) {
            this.CustomProperties = CustomProperties;
        }
    }

    public class SubCategory {
        private int Id;
        private String Name;
        private PictureModel PictureModel;

        public int getId() {
            return this.Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

        public String getName() {
            return this.Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public PictureModel getPictureModel() {
            return this.PictureModel;
        }

        public void setPictureModel(PictureModel PictureModel) {
            this.PictureModel = PictureModel;
        }
    }
}
