package com.mart.onestopkitchen.model.featureproduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.FilterItem;

import java.io.Serializable;
import java.util.List;

public class FeatureProductResponseModel implements Serializable {

    @SerializedName("NotFilteredItems")
    @Expose
    private List<FilterItem> filterItems;

    @SerializedName("AvailableSortOptions")
    @Expose
    private List<AvailableSortOptionsItem> availableSortOptions;

    @SerializedName("SuccessMessage")
    private Object successMessage;

    @SerializedName("ErrorList")
    @Expose
    private List<Object> errorList;

    @SerializedName("Data")
    @Expose
    private List<DataItem> data;

    @SerializedName("StatusCode")
    @Expose
    private int statusCode;

    @SerializedName("PriceRange")
    @Expose
    private PriceRange priceRange;

    @SerializedName("IsDeliveryAllowed")
    @Expose
    private boolean isDeliveryAllowed;

    public List<FilterItem> getFilterItems() {
        return filterItems;
    }

    public void setFilterItems(List<FilterItem> filterItems) {
        this.filterItems = filterItems;
    }

    public List<AvailableSortOptionsItem> getAvailableSortOptions() {
        return availableSortOptions;
    }

    public void setAvailableSortOptions(List<AvailableSortOptionsItem> availableSortOptions) {
        this.availableSortOptions = availableSortOptions;
    }

    public Object getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(Object successMessage) {
        this.successMessage = successMessage;
    }

    public List<Object> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Object> errorList) {
        this.errorList = errorList;
    }

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public PriceRange getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    public boolean isIsDeliveryAllowed() {
        return isDeliveryAllowed;
    }

    public void setIsDeliveryAllowed(boolean isDeliveryAllowed) {
        this.isDeliveryAllowed = isDeliveryAllowed;
    }
}