package com.mart.onestopkitchen.model;

import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.List;

/**
 * Created by mart-110 on 1/7/2016.
 */
public class CategoryFeaturedProductAndSubcategoryResponse extends BaseResponse {
    String Name;

    int id;
    private List<Category> SubCategories;
    private List<ProductModel> FeaturedProducts;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<ProductModel> getFeaturedProducts() {
        return FeaturedProducts;
    }

    public void setFeaturedProducts(List<ProductModel> featuredProducts) {
        FeaturedProducts = featuredProducts;
    }

    public List<Category> getSubCategories() {
        return SubCategories;
    }

    public void setSubCategories(List<Category> subCategories) {
        SubCategories = subCategories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
