package com.mart.onestopkitchen.model.subCategoryModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoryDetails {

    @SerializedName("SuccessMessage")
    private Object successMessage;

    @SerializedName("ErrorList")
    private List<Object> errorList;

    @SerializedName("SubCategories")
    private List<SubCategoriesItem> subCategories;

    @SerializedName("FeaturedProducts")
    private List<Object> featuredProducts;

    @SerializedName("StatusCode")
    private int statusCode;

    public Object getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(Object successMessage) {
        this.successMessage = successMessage;
    }

    public List<Object> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Object> errorList) {
        this.errorList = errorList;
    }

    public List<SubCategoriesItem> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategoriesItem> subCategories) {
        this.subCategories = subCategories;
    }

    public List<Object> getFeaturedProducts() {
        return featuredProducts;
    }

    public void setFeaturedProducts(List<Object> featuredProducts) {
        this.featuredProducts = featuredProducts;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}