package com.mart.onestopkitchen.model.appratingandreview

import com.google.gson.annotations.SerializedName

data class AppRatingResponseModel(
        @field:SerializedName("SuccessMessage")
        val successMessage: Any? = null,
        @field:SerializedName("ErrorList")
        val errorList: List<Any?>? = null,
        @field:SerializedName("StatusCode")
        val statusCode: Int? = 0


)
//{"SuccessMessage":null,"StatusCode":200,"ErrorList":[]}