package com.mart.onestopkitchen.model;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by mart-110 on 12/22/2015.
 */
public class ConfirmPayPalCheckoutResponse extends BaseResponse {
    long OrderId;

    public long getOrderId() {
        return OrderId;
    }

    public void setOrderId(long orderId) {
        OrderId = orderId;
    }
}
