package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.products.view.model.CustomProperties;

import java.util.List;

/**
 * Created by mart-110 on 12/18/2015.
 */
public class CustomerOrder {

    @SerializedName("CustomOrderNumber")
    @Expose
    private String customOrderNumber;


    @SerializedName("ExpectedDeliveryDate")
    @Expose
    private String expectedDeliveryDate;
    @SerializedName("OrderTotal")
    @Expose
    private String orderTotal;
    @SerializedName("IsReturnRequestAllowed")
    @Expose
    private Boolean isReturnRequestAllowed;
    @SerializedName("OrderStatusEnum")
    @Expose
    private Integer orderStatusEnum;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("PaymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("ShippingStatus")
    @Expose
    private String shippingStatus;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Form")
    @Expose
    private Object form;
    @SerializedName("CustomProperties")
    @Expose
    private CustomProperties customProperties;
    private List<CartProduct> Items;


    public String getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(String expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public String getCustomOrderNumber() {
        return customOrderNumber;
    }

    public void setCustomOrderNumber(String customOrderNumber) {
        this.customOrderNumber = customOrderNumber;
    }


    public List<CartProduct> getItems() {
        return Items;
    }

    public void setItems(List<CartProduct> items) {
        Items = items;
    }


    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public Boolean getReturnRequestAllowed() {
        return isReturnRequestAllowed;
    }

    public void setReturnRequestAllowed(Boolean returnRequestAllowed) {
        isReturnRequestAllowed = returnRequestAllowed;
    }

    public Integer getOrderStatusEnum() {
        return orderStatusEnum;
    }

    public void setOrderStatusEnum(Integer orderStatusEnum) {
        this.orderStatusEnum = orderStatusEnum;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getForm() {
        return form;
    }

    public void setForm(Object form) {
        this.form = form;
    }

    public CustomProperties getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }
}
