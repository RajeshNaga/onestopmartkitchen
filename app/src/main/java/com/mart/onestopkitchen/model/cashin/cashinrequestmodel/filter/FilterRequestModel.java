package com.mart.onestopkitchen.model.cashin.cashinrequestmodel.filter;

import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.Location;

public class FilterRequestModel {

    @SerializedName("Filter")
    private Filter filter;

    @SerializedName("Amount")
    private double amount;

    @SerializedName("CashType")
    private int cashType;

    @SerializedName("Commission")
    private int commission;

    @SerializedName("Location")
    private Location location;

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCashType() {
        return cashType;
    }

    public void setCashType(int cashType) {
        this.cashType = cashType;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}