package com.mart.onestopkitchen.model.filterslection;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserSelectionResponseModel {

    @SerializedName("Id")
    private String cateName;

    @SerializedName("GroupData")
    private List<GroupDataItem> groupData;

    public String getId() {
        return cateName;
    }

    public void setId(String id) {
        this.cateName = id;
    }

    public List<GroupDataItem> getGroupData() {
        return groupData;
    }

    public void setGroupData(List<GroupDataItem> groupData) {
        this.groupData = groupData;
    }
}