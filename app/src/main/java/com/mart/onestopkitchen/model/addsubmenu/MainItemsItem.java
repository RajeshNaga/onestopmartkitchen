package com.mart.onestopkitchen.model.addsubmenu;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MainItemsItem implements Serializable {

    @SerializedName("CategeryId")
    private int categeryId;

    @SerializedName("CategeryItems")
    private List<CategeryItemsItem> categeryItems;

    @SerializedName("CategeryName")
    private String categeryName;

    public int getCategeryId() {
        return categeryId;
    }

    public void setCategeryId(int categeryId) {
        this.categeryId = categeryId;
    }

    public List<CategeryItemsItem> getCategeryItems() {
        return categeryItems;
    }

    public void setCategeryItems(List<CategeryItemsItem> categeryItems) {
        this.categeryItems = categeryItems;
    }

    public String getCategeryName() {
        return categeryName;
    }

    public void setCategeryName(String categeryName) {
        this.categeryName = categeryName;
    }
}