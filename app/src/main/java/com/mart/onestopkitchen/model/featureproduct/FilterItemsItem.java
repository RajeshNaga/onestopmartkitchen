package com.mart.onestopkitchen.model.featureproduct;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilterItemsItem implements Serializable {

    @SerializedName("SpecificationAttributeOptionName")
    private String specificationAttributeOptionName;

    @SerializedName("SpecificationAttributeName")
    private String specificationAttributeName;

    @SerializedName("ProductId")
    private int productId;

    @SerializedName("FilterId")
    private int filterId;

    public String getSpecificationAttributeOptionName() {
        return specificationAttributeOptionName;
    }

    public void setSpecificationAttributeOptionName(String specificationAttributeOptionName) {
        this.specificationAttributeOptionName = specificationAttributeOptionName;
    }

    public String getSpecificationAttributeName() {
        return specificationAttributeName;
    }

    public void setSpecificationAttributeName(String specificationAttributeName) {
        this.specificationAttributeName = specificationAttributeName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getFilterId() {
        return filterId;
    }

    public void setFilterId(int filterId) {
        this.filterId = filterId;
    }
}