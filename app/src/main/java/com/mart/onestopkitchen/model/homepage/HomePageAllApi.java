package com.mart.onestopkitchen.model.homepage;

import com.mart.onestopkitchen.networking.response.FeaturedCategoryResponse;
import com.mart.onestopkitchen.networking.response.HomePageBannerResponse;
import com.mart.onestopkitchen.networking.response.HomePageManufacturerResponse;
import com.mart.onestopkitchen.networking.response.HomePageProductResponse;

public class HomePageAllApi {
    private HomePageBannerResponse homePageBannerResponse;
    private HomePageProductResponse homePageProductResponse;
    private FeaturedCategoryResponse featuredCategoryResponse;
    private HomePageManufacturerResponse homePageManufacturerResponse;

    public HomePageAllApi(HomePageBannerResponse homePageBannerResponse, HomePageProductResponse homePageProductResponse, FeaturedCategoryResponse featuredCategoryResponse, HomePageManufacturerResponse homePageManufacturerResponse) {
        this.homePageBannerResponse = homePageBannerResponse;
        this.homePageProductResponse = homePageProductResponse;
        this.featuredCategoryResponse = featuredCategoryResponse;
        this.homePageManufacturerResponse = homePageManufacturerResponse;
    }

    public HomePageBannerResponse getHomePageBannerResponse() {
        return homePageBannerResponse;
    }

    public void setHomePageBannerResponse(HomePageBannerResponse homePageBannerResponse) {
        this.homePageBannerResponse = homePageBannerResponse;
    }

    public HomePageProductResponse getHomePageProductResponse() {
        return homePageProductResponse;
    }

    public void setHomePageProductResponse(HomePageProductResponse homePageProductResponse) {
        this.homePageProductResponse = homePageProductResponse;
    }

    public FeaturedCategoryResponse getFeaturedCategoryResponse() {
        return featuredCategoryResponse;
    }

    public void setFeaturedCategoryResponse(FeaturedCategoryResponse featuredCategoryResponse) {
        this.featuredCategoryResponse = featuredCategoryResponse;
    }

    public HomePageManufacturerResponse getHomePageManufacturerResponse() {
        return homePageManufacturerResponse;
    }

    public void setHomePageManufacturerResponse(HomePageManufacturerResponse homePageManufacturerResponse) {
        this.homePageManufacturerResponse = homePageManufacturerResponse;
    }
}
