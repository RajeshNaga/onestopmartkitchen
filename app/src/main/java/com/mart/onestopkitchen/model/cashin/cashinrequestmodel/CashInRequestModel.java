package com.mart.onestopkitchen.model.cashin.cashinrequestmodel;

import com.google.gson.annotations.SerializedName;

public class CashInRequestModel {

    @SerializedName("Amount")
    private double amount;

    @SerializedName("CashType")
    private int cashType;

    @SerializedName("Commission")
    private int commission;

    @SerializedName("Location")
    private Location location;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCashType() {
        return cashType;
    }

    public void setCashType(int cashType) {
        this.cashType = cashType;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}