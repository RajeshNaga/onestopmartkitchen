package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class PaymentResponseModel extends BaseResponse implements Serializable {
    @SerializedName("OrderNumber")
    @Expose
    private Object orderNumber;

    @SerializedName("ReferenceNumber")
    @Expose
    private String referenceNumber;

    @SerializedName("ShippingAddress")
    @Expose
    private String shippingAddress;

    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;

    @SerializedName("ProductAmount")
    @Expose
    private String productAmount;

    @SerializedName("TotalTax")
    @Expose
    private String TotalTax;

    @SerializedName("ShippingCharges")
    @Expose
    private String shippingCharges;

    @SerializedName("TotalPaidAmount")
    @Expose
    private String totalPaidAmount;

    @SerializedName("CustomOrderNumbers")
    @Expose
    private List<String> customOrderNumbers = null;

    @SerializedName("Orders")
    @Expose
    private List<OrdersModel> orders = null;

    @SerializedName("SuccessMessage")
    @Expose
    private Object successMessage;

    @SerializedName("transactionId")
    @Expose
    private String transactionId;

    @SerializedName("transactionStatus")
    @Expose
    private String transactionStatus;

    @SerializedName("orderId")
    @Expose
    private Integer orderId = 0;


    private int Id;
    private boolean PrintMode;
    private boolean PdfInvoiceDisabled;
    private String CreatedOn;
    private String OrderStatus;
    private boolean IsReOrderAllowed;
    private boolean IsReturnRequestAllowed;
    private boolean IsShippable;
    private boolean PickUpInStore;
    private CustomerAddress BillingAddress;
    private CustomerAddress PickupAddress;
    private String OrderSubtotal;
    private String OrderSubTotalDiscount;
    private String OrderShipping;
    private String PaymentMethodAdditionalFee;
    private String CheckoutAttributeInfo;
    private boolean PricesIncludeTax;
    private boolean DisplayTaxShippingInfo;
    private String Tax;
    private boolean DisplayTax;
    private boolean DisplayTaxRates;
    private String OrderTotalDiscount;
    private int RedeemedRewardPoints;
    private String RedeemedRewardPointsAmount;
    private String OrderTotal;
    private String PaymentMethod;
    private String PaymentMethodStatus;
    private String ShippingMethod;
    private String ShippingStatus;
    private List<CartProduct> Items;

    public String getTotalTax() {
        return TotalTax;
    }

    public void setTotalTax(String totalTax) {
        TotalTax = totalTax;
    }

    public List<String> getCustomOrderNumbers() {
        return customOrderNumbers;
    }

    public void setCustomOrderNumbers(List<String> customOrderNumbers) {
        this.customOrderNumbers = customOrderNumbers;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    public String getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(String shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    public List<OrdersModel> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersModel> orders) {
        this.orders = orders;
    }

    public Object getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(Object successMessage) {
        this.successMessage = successMessage;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public boolean isPrintMode() {
        return PrintMode;
    }

    public void setPrintMode(boolean printMode) {
        PrintMode = printMode;
    }

    public boolean isPdfInvoiceDisabled() {
        return PdfInvoiceDisabled;
    }

    public void setPdfInvoiceDisabled(boolean pdfInvoiceDisabled) {
        PdfInvoiceDisabled = pdfInvoiceDisabled;
    }

  /*  public String getcustomOrderNumber() {
        return CustomOrderNumber;
    }
    public void setcustomOrderNumber(String custom_OrderNumber) {
        CustomOrderNumber = custom_OrderNumber;
    }*/


    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }


    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public boolean isReOrderAllowed() {
        return IsReOrderAllowed;
    }

    public void setReOrderAllowed(boolean reOrderAllowed) {
        IsReOrderAllowed = reOrderAllowed;
    }

    public boolean isReturnRequestAllowed() {
        return IsReturnRequestAllowed;
    }

    public void setReturnRequestAllowed(boolean returnRequestAllowed) {
        IsReturnRequestAllowed = returnRequestAllowed;
    }

    public boolean isShippable() {
        return IsShippable;
    }

    public void setShippable(boolean shippable) {
        IsShippable = shippable;
    }

    public boolean isPickUpInStore() {
        return PickUpInStore;
    }

    public void setPickUpInStore(boolean pickUpInStore) {
        PickUpInStore = pickUpInStore;
    }

    public String getShippingStatus() {
        return ShippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        ShippingStatus = shippingStatus;
    }



   /* public CustomerAddress getShippingAddress() {
        return ShippingAddress;
    }
    public void setShippingAddress(CustomerAddress shippingAddress) {
        ShippingAddress = shippingAddress;
    }*/


    public CustomerAddress getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(CustomerAddress billingAddress) {
        BillingAddress = billingAddress;
    }

    public String getOrderSubtotal() {
        return OrderSubtotal;
    }

    public void setOrderSubtotal(String orderSubtotal) {
        OrderSubtotal = orderSubtotal;
    }


    public String getOrderShipping() {
        return OrderShipping;
    }

    public void setOrderShipping(String orderShipping) {
        OrderShipping = orderShipping;
    }


    public String getCheckoutAttributeInfo() {
        return CheckoutAttributeInfo;
    }

    public void setCheckoutAttributeInfo(String checkoutAttributeInfo) {
        CheckoutAttributeInfo = checkoutAttributeInfo;
    }

    public boolean isPricesIncludeTax() {
        return PricesIncludeTax;
    }

    public void setPricesIncludeTax(boolean pricesIncludeTax) {
        PricesIncludeTax = pricesIncludeTax;
    }

    public boolean isDisplayTaxShippingInfo() {
        return DisplayTaxShippingInfo;
    }

    public void setDisplayTaxShippingInfo(boolean displayTaxShippingInfo) {
        DisplayTaxShippingInfo = displayTaxShippingInfo;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public boolean isDisplayTax() {
        return DisplayTax;
    }

    public void setDisplayTax(boolean displayTax) {
        DisplayTax = displayTax;
    }

    public boolean isDisplayTaxRates() {
        return DisplayTaxRates;
    }

    public void setDisplayTaxRates(boolean displayTaxRates) {
        DisplayTaxRates = displayTaxRates;
    }

    public String getOrderTotalDiscount() {
        return OrderTotalDiscount;
    }

    public void setOrderTotalDiscount(String orderTotalDiscount) {
        OrderTotalDiscount = orderTotalDiscount;
    }

    public int getRedeemedRewardPoints() {
        return RedeemedRewardPoints;
    }

    public void setRedeemedRewardPoints(int redeemedRewardPoints) {
        RedeemedRewardPoints = redeemedRewardPoints;
    }


    public String getOrderTotal() {
        return OrderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        OrderTotal = orderTotal;
    }

    public List<CartProduct> getItems() {
        return Items;
    }

    public void setItems(List<CartProduct> items) {
        Items = items;
    }


    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public String getPaymentMethodStatus() {
        return PaymentMethodStatus;
    }

    public void setPaymentMethodStatus(String paymentMethodStatus) {
        PaymentMethodStatus = paymentMethodStatus;
    }

    public String getShippingMethod() {
        return ShippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        ShippingMethod = shippingMethod;
    }


    public String getOrderSubTotalDiscount() {
        return OrderSubTotalDiscount;
    }

    public void setOrderSubTotalDiscount(String orderSubTotalDiscount) {
        OrderSubTotalDiscount = orderSubTotalDiscount;
    }

    public String getPaymentMethodAdditionalFee() {
        return PaymentMethodAdditionalFee;
    }

    public void setPaymentMethodAdditionalFee(String paymentMethodAdditionalFee) {
        PaymentMethodAdditionalFee = paymentMethodAdditionalFee;
    }

    public String getRedeemedRewardPointsAmount() {
        return RedeemedRewardPointsAmount;
    }

    public void setRedeemedRewardPointsAmount(String redeemedRewardPointsAmount) {
        RedeemedRewardPointsAmount = redeemedRewardPointsAmount;
    }

    public CustomerAddress getPickupAddress() {
        return PickupAddress;
    }

    public void setPickupAddress(CustomerAddress pickupAddress) {
        PickupAddress = pickupAddress;
    }

    @Override
    public String toString() {
        return "PaymentResponseModel{" +
                "orderNumber=" + orderNumber +
                ", referenceNumber='" + referenceNumber + '\'' +
                ", shippingAddress='" + shippingAddress + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", productAmount='" + productAmount + '\'' +
                ", TotalTax='" + TotalTax + '\'' +
                ", shippingCharges='" + shippingCharges + '\'' +
                ", totalPaidAmount='" + totalPaidAmount + '\'' +
                ", customOrderNumbers=" + customOrderNumbers +
                ", orders=" + orders +
                ", successMessage=" + successMessage +
                ", transactionId='" + transactionId + '\'' +
                ", transactionStatus='" + transactionStatus + '\'' +
                ", orderId=" + orderId +
                ", Id=" + Id +
                ", PrintMode=" + PrintMode +
                ", PdfInvoiceDisabled=" + PdfInvoiceDisabled +
                ", CreatedOn='" + CreatedOn + '\'' +
                ", OrderStatus='" + OrderStatus + '\'' +
                ", IsReOrderAllowed=" + IsReOrderAllowed +
                ", IsReturnRequestAllowed=" + IsReturnRequestAllowed +
                ", IsShippable=" + IsShippable +
                ", PickUpInStore=" + PickUpInStore +
                ", BillingAddress=" + BillingAddress +
                ", PickupAddress=" + PickupAddress +
                ", OrderSubtotal='" + OrderSubtotal + '\'' +
                ", OrderSubTotalDiscount='" + OrderSubTotalDiscount + '\'' +
                ", OrderShipping='" + OrderShipping + '\'' +
                ", PaymentMethodAdditionalFee='" + PaymentMethodAdditionalFee + '\'' +
                ", CheckoutAttributeInfo='" + CheckoutAttributeInfo + '\'' +
                ", PricesIncludeTax=" + PricesIncludeTax +
                ", DisplayTaxShippingInfo=" + DisplayTaxShippingInfo +
                ", Tax='" + Tax + '\'' +
                ", DisplayTax=" + DisplayTax +
                ", DisplayTaxRates=" + DisplayTaxRates +
                ", OrderTotalDiscount='" + OrderTotalDiscount + '\'' +
                ", RedeemedRewardPoints=" + RedeemedRewardPoints +
                ", RedeemedRewardPointsAmount='" + RedeemedRewardPointsAmount + '\'' +
                ", OrderTotal='" + OrderTotal + '\'' +
                ", PaymentMethod='" + PaymentMethod + '\'' +
                ", PaymentMethodStatus='" + PaymentMethodStatus + '\'' +
                ", ShippingMethod='" + ShippingMethod + '\'' +
                ", ShippingStatus='" + ShippingStatus + '\'' +
                ", Items=" + Items +
                '}';
    }
}

