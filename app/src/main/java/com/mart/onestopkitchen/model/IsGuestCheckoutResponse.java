package com.mart.onestopkitchen.model;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by mart-110 on 12/23/2015.
 */
public class IsGuestCheckoutResponse extends BaseResponse {
    boolean Data;

    public boolean isData() {
        return Data;
    }

    public void setData(boolean data) {
        Data = data;
    }
}
