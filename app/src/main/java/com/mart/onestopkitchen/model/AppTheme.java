package com.mart.onestopkitchen.model;

import java.util.List;

/**
 * Created by Ashraful on 5/11/2016.
 */
public class AppTheme {
    private String AppImgUrl;
    private String AppLogoUrl;
    private Object GcmApiKey;
    private Object AppName;
    private Object AppKey;
    private Object LicenceType;
    private Object CreatedDate;
    private Object AndroidAppStatus;
    private Object DownloadUrl;
    private Object iOsAPPUDID;
    private Object MobilWebsiteURL;
    private String HeaderBackgroundColor;
    private String HeaderFontandIconColor;
    private String HighlightedTextColor;
    private String PrimaryTextColor;
    private String SecondaryTextColor;
    private String BackgroundColorofPrimaryButton;
    private String TextColorofPrimaryButton;
    private String BackgroundColorofSecondaryButton;
    private boolean DefaultNopFlowSameAs;
    private Object PushNotificationHeading;
    private Object PushNotificationMessage;
    private boolean Willusedefaultnopcategory;
    private boolean ActivatePushNotification;
    private boolean SandboxMode;
    private Object GoogleApiProjectNumber;
    private int UploudeIOSPEMFile;
    private Object PEMPassword;
    private Object AppNameOnGooglePlayStore;
    private Object AppUrlOnGooglePlayStore;
    private Object AppNameOnAppleStore;
    private Object AppUrlonAppleStore;
    private Object AppDescription;
    private List<BannerModel> BannerModel;

    public String getBackgroundColorofSecondaryButton() {
        return BackgroundColorofSecondaryButton;
    }

    public void setBackgroundColorofSecondaryButton(String backgroundColorofSecondaryButton) {
        BackgroundColorofSecondaryButton = backgroundColorofSecondaryButton;
    }

    public boolean isActivatePushNotification() {
        return ActivatePushNotification;
    }

    public void setActivatePushNotification(boolean activatePushNotification) {
        ActivatePushNotification = activatePushNotification;
    }

    public Object getAndroidAppStatus() {
        return AndroidAppStatus;
    }

    public void setAndroidAppStatus(Object androidAppStatus) {
        AndroidAppStatus = androidAppStatus;
    }

    public Object getAppDescription() {
        return AppDescription;
    }

    public void setAppDescription(Object appDescription) {
        AppDescription = appDescription;
    }

    public String getAppImgUrl() {
        return AppImgUrl;
    }

    public void setAppImgUrl(String appImgUrl) {
        AppImgUrl = appImgUrl;
    }

    public Object getAppKey() {
        return AppKey;
    }

    public void setAppKey(Object appKey) {
        AppKey = appKey;
    }

    public String getAppLogoUrl() {
        return AppLogoUrl;
    }

    public void setAppLogoUrl(String appLogoUrl) {
        AppLogoUrl = appLogoUrl;
    }

    public Object getAppName() {
        return AppName;
    }

    public void setAppName(Object appName) {
        AppName = appName;
    }

    public Object getAppNameOnAppleStore() {
        return AppNameOnAppleStore;
    }

    public void setAppNameOnAppleStore(Object appNameOnAppleStore) {
        AppNameOnAppleStore = appNameOnAppleStore;
    }

    public Object getAppNameOnGooglePlayStore() {
        return AppNameOnGooglePlayStore;
    }

    public void setAppNameOnGooglePlayStore(Object appNameOnGooglePlayStore) {
        AppNameOnGooglePlayStore = appNameOnGooglePlayStore;
    }

    public Object getAppUrlonAppleStore() {
        return AppUrlonAppleStore;
    }

    public void setAppUrlonAppleStore(Object appUrlonAppleStore) {
        AppUrlonAppleStore = appUrlonAppleStore;
    }

    public Object getAppUrlOnGooglePlayStore() {
        return AppUrlOnGooglePlayStore;
    }

    public void setAppUrlOnGooglePlayStore(Object appUrlOnGooglePlayStore) {
        AppUrlOnGooglePlayStore = appUrlOnGooglePlayStore;
    }

    public String getBackgroundColorofPrimaryButton() {
        return BackgroundColorofPrimaryButton;
    }

    public void setBackgroundColorofPrimaryButton(String backgroundColorofPrimaryButton) {
        BackgroundColorofPrimaryButton = backgroundColorofPrimaryButton;
    }

    public List<com.mart.onestopkitchen.model.BannerModel> getBannerModel() {
        return BannerModel;
    }

    public void setBannerModel(List<com.mart.onestopkitchen.model.BannerModel> bannerModel) {
        BannerModel = bannerModel;
    }

    public Object getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Object createdDate) {
        CreatedDate = createdDate;
    }

    public boolean isDefaultNopFlowSameAs() {
        return DefaultNopFlowSameAs;
    }

    public void setDefaultNopFlowSameAs(boolean defaultNopFlowSameAs) {
        DefaultNopFlowSameAs = defaultNopFlowSameAs;
    }

    public Object getDownloadUrl() {
        return DownloadUrl;
    }

    public void setDownloadUrl(Object downloadUrl) {
        DownloadUrl = downloadUrl;
    }

    public Object getGcmApiKey() {
        return GcmApiKey;
    }

    public void setGcmApiKey(Object gcmApiKey) {
        GcmApiKey = gcmApiKey;
    }

    public Object getGoogleApiProjectNumber() {
        return GoogleApiProjectNumber;
    }

    public void setGoogleApiProjectNumber(Object googleApiProjectNumber) {
        GoogleApiProjectNumber = googleApiProjectNumber;
    }

    public String getHeaderBackgroundColor() {
        return HeaderBackgroundColor;
    }

    public void setHeaderBackgroundColor(String headerBackgroundColor) {
        HeaderBackgroundColor = headerBackgroundColor;
    }

    public String getHeaderFontandIconColor() {
        return HeaderFontandIconColor;
    }

    public void setHeaderFontandIconColor(String headerFontandIconColor) {
        HeaderFontandIconColor = headerFontandIconColor;
    }

    public String getHighlightedTextColor() {
        return HighlightedTextColor;
    }

    public void setHighlightedTextColor(String highlightedTextColor) {
        HighlightedTextColor = highlightedTextColor;
    }

    public Object getiOsAPPUDID() {
        return iOsAPPUDID;
    }

    public void setiOsAPPUDID(Object iOsAPPUDID) {
        this.iOsAPPUDID = iOsAPPUDID;
    }

    public Object getLicenceType() {
        return LicenceType;
    }

    public void setLicenceType(Object licenceType) {
        LicenceType = licenceType;
    }

    public Object getMobilWebsiteURL() {
        return MobilWebsiteURL;
    }

    public void setMobilWebsiteURL(Object mobilWebsiteURL) {
        MobilWebsiteURL = mobilWebsiteURL;
    }

    public Object getPEMPassword() {
        return PEMPassword;
    }

    public void setPEMPassword(Object PEMPassword) {
        this.PEMPassword = PEMPassword;
    }

    public String getPrimaryTextColor() {
        return PrimaryTextColor;
    }

    public void setPrimaryTextColor(String primaryTextColor) {
        PrimaryTextColor = primaryTextColor;
    }

    public Object getPushNotificationHeading() {
        return PushNotificationHeading;
    }

    public void setPushNotificationHeading(Object pushNotificationHeading) {
        PushNotificationHeading = pushNotificationHeading;
    }

    public Object getPushNotificationMessage() {
        return PushNotificationMessage;
    }

    public void setPushNotificationMessage(Object pushNotificationMessage) {
        PushNotificationMessage = pushNotificationMessage;
    }

    public boolean isSandboxMode() {
        return SandboxMode;
    }

    public void setSandboxMode(boolean sandboxMode) {
        SandboxMode = sandboxMode;
    }

    public String getSecondaryTextColor() {
        return SecondaryTextColor;
    }

    public void setSecondaryTextColor(String secondaryTextColor) {
        SecondaryTextColor = secondaryTextColor;
    }

    public String getTextColorofPrimaryButton() {
        return TextColorofPrimaryButton;
    }

    public void setTextColorofPrimaryButton(String textColorofPrimaryButton) {
        TextColorofPrimaryButton = textColorofPrimaryButton;
    }

    public int getUploudeIOSPEMFile() {
        return UploudeIOSPEMFile;
    }

    public void setUploudeIOSPEMFile(int uploudeIOSPEMFile) {
        UploudeIOSPEMFile = uploudeIOSPEMFile;
    }

    public boolean isWillusedefaultnopcategory() {
        return Willusedefaultnopcategory;
    }

    public void setWillusedefaultnopcategory(boolean willusedefaultnopcategory) {
        Willusedefaultnopcategory = willusedefaultnopcategory;
    }

    @Override
    public String toString() {
        return "AppImgUrl='" + AppImgUrl + '\'' +
                ", AppLogoUrl='" + AppLogoUrl + '\'' +
                ", GcmApiKey=" + GcmApiKey +
                ", AppName=" + AppName +
                ", AppKey=" + AppKey +
                ", LicenceType=" + LicenceType +
                ", CreatedDate=" + CreatedDate +
                ", AndroidAppStatus=" + AndroidAppStatus +
                ", DownloadUrl=" + DownloadUrl +
                ", iOsAPPUDID=" + iOsAPPUDID +
                ", MobilWebsiteURL=" + MobilWebsiteURL +
                ", HeaderBackgroundColor='" + HeaderBackgroundColor + '\'' +
                ", HeaderFontandIconColor='" + HeaderFontandIconColor + '\'' +
                ", HighlightedTextColor='" + HighlightedTextColor + '\'' +
                ", PrimaryTextColor='" + PrimaryTextColor + '\'' +
                ", SecondaryTextColor='" + SecondaryTextColor + '\'' +
                ", BackgroundColorofPrimaryButton='" + BackgroundColorofPrimaryButton + '\'' +
                ", TextColorofPrimaryButton='" + TextColorofPrimaryButton + '\'' +
                ", BackgroundColorofSecondaryButton='" + BackgroundColorofSecondaryButton + '\'' +
                ", DefaultNopFlowSameAs=" + DefaultNopFlowSameAs +
                ", PushNotificationHeading=" + PushNotificationHeading +
                ", PushNotificationMessage=" + PushNotificationMessage +
                ", Willusedefaultnopcategory=" + Willusedefaultnopcategory +
                ", ActivatePushNotification=" + ActivatePushNotification +
                ", SandboxMode=" + SandboxMode +
                ", GoogleApiProjectNumber=" + GoogleApiProjectNumber +
                ", UploudeIOSPEMFile=" + UploudeIOSPEMFile +
                ", PEMPassword=" + PEMPassword +
                ", AppNameOnGooglePlayStore=" + AppNameOnGooglePlayStore +
                ", AppUrlOnGooglePlayStore=" + AppUrlOnGooglePlayStore +
                ", AppNameOnAppleStore=" + AppNameOnAppleStore +
                ", AppUrlonAppleStore=" + AppUrlonAppleStore +
                ", AppDescription=" + AppDescription +
                ", BannerModel=" + BannerModel;
    }
}
