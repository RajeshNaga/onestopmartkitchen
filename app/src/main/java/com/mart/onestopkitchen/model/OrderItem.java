package com.mart.onestopkitchen.model;

/**
 * Created by mart-110 on 12/18/2015.
 */
public class OrderItem {
    private String OrderItemGuid;
    private Object Sku;
    private int ProductId;
    private String ProductName;
    private String ProductSeName;
    private String UnitPrice;
    private String SubTotal;
    private int Quantity;
    private String AttributeInfo;
    private String RentalInfo;
    private int DownloadId;
    private int LicenseId;
    private int Id;

}
