package com.mart.onestopkitchen.model.cashin.cashinresponsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearbyResponseModel {

    @SerializedName("Status")
    private int status;

    @SerializedName("Comment")
    private Object comment;

    @SerializedName("Content")
    private List<ContentItem> content;

    @SerializedName("StatusCode")
    private int statusCode;

    @SerializedName("Exception")
    private Object exception;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public List<ContentItem> getContent() {
        return content;
    }

    public void setContent(List<ContentItem> content) {
        this.content = content;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public Object getException() {
        return exception;
    }

    public void setException(Object exception) {
        this.exception = exception;
    }
}