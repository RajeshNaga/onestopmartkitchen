package com.mart.onestopkitchen.model.ratingreview

import com.google.gson.annotations.SerializedName

data class RatingGettingModel(

        @field:SerializedName("ProductId")
        val productId: String? = null,

        @field:SerializedName("ProductName")
        val productName: String? = null,

        @field:SerializedName("ProductSeName")
        val productSeName: String? = null,

        @field:SerializedName("StatusCode")
        val statusCode: Int? = null,
        var Items: List<RatingReviewRequestModel>? = null

)