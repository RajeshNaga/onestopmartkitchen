package com.mart.onestopkitchen.model;


public class LanguageDM {
    private int CurrentLanguageId;

    public int getCurrentLanguageId() {
        return CurrentLanguageId;
    }

    public void setCurrentLanguageId(int currentLanguageId) {
        CurrentLanguageId = currentLanguageId;
    }
}
