package com.mart.onestopkitchen.model.subCategoryModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubCategoriesItem implements Serializable {

    @SerializedName("Id")
    private int id;

    @SerializedName("Name")
    private String name;

    @SerializedName("PictureModel")
    private PictureModel pictureModel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PictureModel getPictureModel() {
        return pictureModel;
    }

    public void setPictureModel(PictureModel pictureModel) {
        this.pictureModel = pictureModel;
    }
}