package com.mart.onestopkitchen.model.ratingreview

import com.google.gson.annotations.SerializedName

data class RatingRequestModel(

        @field:SerializedName("SuccessMessage")
        val successMessage: String? = null,

        @field:SerializedName("ErrorList")
        val errorList: List<Any?>? = null,

        @field:SerializedName("Data")
        val data: Boolean? = null,

        @field:SerializedName("StatusCode")
        val statusCode: Int? = null,

        @field:SerializedName("IsDeliveryAllowed")
        val isDeliveryAllowed: Boolean? = null
)