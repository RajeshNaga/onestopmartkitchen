package com.mart.onestopkitchen.model.okdollar.model;


import com.google.gson.GsonBuilder;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BankDetailsModel implements Comparable<BankDetailsModel>, Serializable {

    final String TAG_AGENTCODE = "agentcode";
    final String TAG_AGENT_NAME = "agentname";
    final String TAG_SOURCE = "source";
    final String TAG_DESTINATION = "destination";
    final String TAG_AMOUNT = "amount";
    final String TAG_TRANSID = "transid";
    final String TAG_RESULTP_CODE = "resultcode";
    final String TAG_DESCRIPTION = "resultdescription";
    final String TAG_DATE = "responsects";
    final String TAG_WALLET_BALANCE = "walletbalance";
    final String TAG_COMMENTS = "comments";
    public String AccountName = "";
    public String AccountModel = "";
    public String Nrcno = "";
    public String AccountNumber = "";
    public String BankName = "";
    public String Branch = "";
    public String AccountType = "";
    public String BranchAddress = "";
    public String IDType = "";
    public String IDNo = "";
    public String BankPhoneNumber = "";
    public String BranchAddress2 = "";
    public String State = "";
    public String Township = "";
    public int BranchId;
    public int BankId;
    public String BankBurmeseName = "";
    public String BranchBurmeseName = "";
    public String BankState = "";
    public String BankTownship = "";
    public String SimId = "";
    public String MsId = "";
    public int Ostype;
    public int Otp;
    public String phonenumber = "";
    public String beneficiaryMobNumber = "";
    public String Remarks = "";
    public String email_address = "";
    public int bankBranchId;
    public long TransactionDate;
    public String Comments = "";
    public String Balance = "";
    public String PhoneNo = "";
    public String Amount = "";
    public String TransactionId;
    String TAG_RESPONSE_TYPE = "responsetype";
    boolean disableSave;
    String newTownshipBurmese;
    String newDivisionBurmese;
    String newBankNameBurmese;
    private String BankPhone;
    private String date;

    public BankDetailsModel() {


    }

    public BankDetailsModel(String response) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = factory.newPullParser();
        myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream stream = new ByteArrayInputStream(response.getBytes());
        myparser.setInput(stream, null);
        parseXML(myparser);
        stream.close();
    }

    public static BankDetailsModel fromJson(JSONObject jsonObject) {
        BankDetailsModel b = new BankDetailsModel();
        // Deserialize json into object fields
        try {
            b.setAccountName(jsonObject.optString("AccountName"));
            b.setAccountModel(jsonObject.optString("AccountModel"));
            b.setNrcno(jsonObject.optString("Nrcno"));
            b.setAccountNumber(jsonObject.optString("AccountNumber"));
            b.setBankName(jsonObject.optString("BankName"));
            b.setBranch(jsonObject.optString("Branch"));
            b.setAccountType(jsonObject.optString("AccountType"));
            b.setBranchAddress(jsonObject.optString("BranchAddress"));
            b.setIDType(jsonObject.optString("IDType"));
            b.setIDNo(jsonObject.optString("IDNo"));
            b.setBankPhoneNumber(jsonObject.optString("BankPhoneNumber"));
            b.setBranchAddress2(jsonObject.optString("BranchAddress2"));
            b.setState(jsonObject.optString("State"));
            b.setTownship(jsonObject.optString("Township"));
            b.setBranchId(jsonObject.optInt("BranchId"));
            b.setBankId(jsonObject.optInt("BankId"));
            b.setBankBurmeseName(jsonObject.optString("BankBurmeseName"));
            b.setBranchBurmeseName(jsonObject.optString("BranchBurmeseName"));
            b.setBankState(jsonObject.optString("BankState"));
            b.setBankTownship(jsonObject.optString("BankTownship"));
            b.setBankPhone(jsonObject.optString("BankPhone"));
            b.setSimId(jsonObject.optString("SimId"));
            b.setMsId(jsonObject.optString("MsId"));
            b.setOstype(jsonObject.optInt("Ostype"));
            b.setOtp(jsonObject.optInt("Otp"));
            b.setPhonenumber(jsonObject.optString("phonenumber"));
            b.setBankBranchId(jsonObject.optInt("bankBranchId"));


        } catch (Exception e) {
            e.printStackTrace();
            return b;
        }
        // Return new object
        return b;
    }

    public String getBankPhoneNumber() {
        return BankPhoneNumber;
    }

    public void setBankPhoneNumber(String bankPhoneNumber) {
        BankPhoneNumber = bankPhoneNumber;
    }

    public Date getTransactionDate() {
        return new Date(TransactionDate);
    }

    public void setTransactionDate(Date transactionDate) {
        TransactionDate = transactionDate.getTime();
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    void parseXML(XmlPullParser myParser) throws XmlPullParserException, IOException {
        int event;
        String text = null;
        event = myParser.getEventType();
        while (event != XmlPullParser.END_DOCUMENT) {
            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.START_TAG:
                    break;
                case XmlPullParser.TEXT:
                    text = myParser.getText();
                    break;
                case XmlPullParser.END_TAG:

                    switch (name) {
                        case TAG_TRANSID:
                            setIDNo(text);
                            break;
                        case TAG_DATE:
                            String text1 = text;
                            setDate(text);
                            try {
                                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
                                setTransactionDate(dateFormatter.parse(text));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        case TAG_AGENT_NAME:
                            setAccountName(text);
                            break;
                        case TAG_WALLET_BALANCE:
                            setBalance(text);
                            break;
                        case TAG_AMOUNT:
                            setAmount(text);
                            break;
                        case TAG_SOURCE:
                            setPhoneNo(text);
                            break;
                        case TAG_COMMENTS:
                            setComments(text);
                            String[] account = text.split("");
                            setAccountNumber(account[account.length - 1]);
                            break;
                        default:
                            break;
                    }

                    break;
            }
            event = myParser.next();
        }
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getNrcno() {
        return Nrcno;
    }

    public void setNrcno(String nrcno) {
        Nrcno = nrcno;
    }

    public String getAccountName() {
        return AccountName;
    }

    public void setAccountName(String accountName) {
        AccountName = accountName;
    }

    public String getAccountModel() {
        return AccountModel;
    }

    public void setAccountModel(String accountModel) {
        AccountModel = accountModel;
    }

    public String getBankPhone() {
        return BankPhone;
    }

    public void setBankPhone(String bankPhone) {
        BankPhone = bankPhone;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    public String getAccountType() {
        return AccountType;
    }

    public void setAccountType(String accountType) {
        AccountType = accountType;
    }

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        BranchAddress = branchAddress;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getBranchAddress2() {
        return BranchAddress2;
    }

    public void setBranchAddress2(String branchAddress2) {
        BranchAddress2 = branchAddress2;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int branchId) {
        BranchId = branchId;
    }

    public int getBankId() {
        return BankId;
    }

    public void setBankId(int bankId) {
        BankId = bankId;
    }

    public String getBankBurmeseName() {
        return BankBurmeseName;
    }

    public void setBankBurmeseName(String bankBurmeseName) {
        BankBurmeseName = bankBurmeseName;
    }

    public String getBranchBurmeseName() {
        return BranchBurmeseName;
    }

    public void setBranchBurmeseName(String branchBurmeseName) {
        BranchBurmeseName = branchBurmeseName;
    }

    public String getBankState() {
        return BankState;
    }

    public void setBankState(String bankState) {
        BankState = bankState;
    }

    public String getBankTownship() {
        return BankTownship;
    }

    public void setBankTownship(String bankTownship) {
        BankTownship = bankTownship;
    }

    public int getOstype() {
        return Ostype;
    }

    public void setOstype(int ostype) {
        Ostype = ostype;
    }

    public int getOtp() {
        return Otp;
    }

    public void setOtp(int otp) {
        Otp = otp;
    }

    public String getSimId() {
        return SimId;
    }

    public void setSimId(String simId) {
        SimId = simId;
    }

    public String getMsId() {
        return MsId;
    }

    public void setMsId(String msId) {
        MsId = msId;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getBankBranchId() {
        return bankBranchId;
    }

    public void setBankBranchId(int bankBranchId) {
        this.bankBranchId = bankBranchId;
    }

    public String getBeneficiaryMobNumber() {
        return beneficiaryMobNumber;
    }

    public void setBeneficiaryMobNumber(String beneficiaryMobNumber) {
        this.beneficiaryMobNumber = beneficiaryMobNumber;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public String getEmailAddress() {
        return email_address;
    }

    public void setEmailAddress(String email_ad) {
        email_address = email_ad;
    }

    public boolean isDisableSave() {
        return disableSave;
    }

    public void setDisableSave(boolean disableSave) {
        this.disableSave = disableSave;
    }

    @Override
    public int compareTo(BankDetailsModel o) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
        Date date1 = null;
        Date date2 = null;
        try {
            if (getDate() != null) {
                if (!getDate().equals("")) {
                    date1 = dateFormatter.parse(getDate());
                    date2 = dateFormatter.parse(o.getDate());
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (Exception e) {
        }

        if (date1 != null && date2 != null) {
            return date2.compareTo(date1);
        } else {
            return 0;
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, BankDetailsModel.class);
    }

    public String getNewBankNameBurmese() {
        return newBankNameBurmese;
    }

    public void setNewBankNameBurmese(String newBankNameBurmese) {
        this.newBankNameBurmese = newBankNameBurmese;
    }

    public String getNewTownshipBurmese() {
        return newTownshipBurmese;
    }

    public void setNewTownshipBurmese(String newTownshipBurmese) {
        this.newTownshipBurmese = newTownshipBurmese;
    }

    public String getNewDivisionBurmese() {
        return newDivisionBurmese;
    }

    public void setNewDivisionBurmese(String newDivisionBurmese) {
        this.newDivisionBurmese = newDivisionBurmese;
    }
}



