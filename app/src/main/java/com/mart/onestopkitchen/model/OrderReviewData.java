package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Updated by AKashGarg on 12/11/2015.
 */
public class OrderReviewData {

    @Expose
    @SerializedName("BillingAddress")
    private BillingAddress BillingAddress;

    @Expose
    @SerializedName("ShippingAddress")
    private BillingAddress ShippingAddress;

    @Expose
    @SerializedName("PickupAddress")
    private BillingAddress PickupAddress;

    private String ShippingMethod;

    @Expose
    @SerializedName("PaymentMethod")
    private String PaymentMethod;

    private boolean SelectedPickUpInStore = false;

    public BillingAddress getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(com.mart.onestopkitchen.model.BillingAddress billingAddress) {
        BillingAddress = billingAddress;
    }

    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public com.mart.onestopkitchen.model.BillingAddress getShippingAddress() {
        return ShippingAddress;
    }

    public void setShippingAddress(com.mart.onestopkitchen.model.BillingAddress shippingAddress) {
        ShippingAddress = shippingAddress;
    }

    public String getShippingMethod() {
        return ShippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        ShippingMethod = shippingMethod;
    }


    public com.mart.onestopkitchen.model.BillingAddress getPickupAddress() {
        return PickupAddress;
    }

    public void setPickupAddress(com.mart.onestopkitchen.model.BillingAddress pickupAddress) {
        PickupAddress = pickupAddress;
    }

    public boolean isSelectedPickUpInStore() {
        return SelectedPickUpInStore;
    }

    public void setSelectedPickUpInStore(boolean selectedPickUpInStore) {
        SelectedPickUpInStore = selectedPickUpInStore;
    }
}
