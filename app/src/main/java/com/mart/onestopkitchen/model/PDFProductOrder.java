package com.mart.onestopkitchen.model;

import com.mart.onestopkitchen.networking.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class PDFProductOrder extends BaseResponse implements Serializable {


    private int Id;
    private String CustomOrderNumber;
    private String CreatedOn;
    private String ExpectedDeliveryDate;
    private String OrderStatus;
    private String PaymentMethod;
    private String PaymentMethodStatus;
    private String OrderSubtotal;
    private String OrderShipping;
    private String Tax;
    private String RedeemedRewardPoints;
    private String OrderTotal;
    private List<PDFProductItem> Items;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCustomOrderNumber() {
        return CustomOrderNumber;
    }

    public void setCustomOrderNumber(String customOrderNumber) {
        CustomOrderNumber = customOrderNumber;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getExpectedDeliveryDate() {
        return ExpectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(String expectedDeliveryDate) {
        ExpectedDeliveryDate = expectedDeliveryDate;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public String getPaymentMethodStatus() {
        return PaymentMethodStatus;
    }

    public void setPaymentMethodStatus(String paymentMethodStatus) {
        PaymentMethodStatus = paymentMethodStatus;
    }

    public String getOrderSubtotal() {
        return OrderSubtotal;
    }

    public void setOrderSubtotal(String orderSubtotal) {
        OrderSubtotal = orderSubtotal;
    }

    public String getOrderShipping() {
        return OrderShipping;
    }

    public void setOrderShipping(String orderShipping) {
        OrderShipping = orderShipping;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getRedeemedRewardPoints() {
        return RedeemedRewardPoints;
    }

    public void setRedeemedRewardPoints(String redeemedRewardPoints) {
        RedeemedRewardPoints = redeemedRewardPoints;
    }

    public String getOrderTotal() {
        return OrderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        OrderTotal = orderTotal;
    }


    public List<PDFProductItem> getItems() {
        return Items;
    }

    public void setItems(List<PDFProductItem> items) {
        Items = items;
    }


}
