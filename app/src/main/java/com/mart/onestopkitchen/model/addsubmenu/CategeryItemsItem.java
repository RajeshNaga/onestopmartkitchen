package com.mart.onestopkitchen.model.addsubmenu;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategeryItemsItem implements Serializable {

    //@SerializedName("SubItems")
    //private List<Object> subItems;

    @SerializedName("ItemName")
    private String itemName;

    //public void setSubItems(List<Object> subItems){
    //	this.subItems = subItems;
    //}

    //public List<Object> getSubItems(){
    //	return subItems;
    //}

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}