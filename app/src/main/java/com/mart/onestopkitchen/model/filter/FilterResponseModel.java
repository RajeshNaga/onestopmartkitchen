package com.mart.onestopkitchen.model.filter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FilterResponseModel implements Serializable {

    @SerializedName("MinPrize")
    private int minPrize;

    @SerializedName("SuccessMessage")
    private Object successMessage;

    @SerializedName("ErrorList")
    private List<Object> errorList;

    @SerializedName("Data")
    private List<DataItem> data;

    @SerializedName("MaxPrize")
    private int maxPrize;

    @SerializedName("StatusCode")
    private int statusCode;

    public int getMinPrize() {
        return minPrize;
    }

    public void setMinPrize(int minPrize) {
        this.minPrize = minPrize;
    }

    public Object getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(Object successMessage) {
        this.successMessage = successMessage;
    }

    public List<Object> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Object> errorList) {
        this.errorList = errorList;
    }

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public int getMaxPrize() {
        return maxPrize;
    }

    public void setMaxPrize(int maxPrize) {
        this.maxPrize = maxPrize;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}