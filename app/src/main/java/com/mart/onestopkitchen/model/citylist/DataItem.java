package com.mart.onestopkitchen.model.citylist;

import com.google.gson.annotations.SerializedName;

public class DataItem {

    @SerializedName("State Name")
    private String stateName;

    @SerializedName("isState")
    private String isState;

    @SerializedName("DivNrcCode")
    private String divNrcCode;

    @SerializedName("State BName")
    private String stateBName;

    @SerializedName("DivNrcCodeMyan")
    private String divNrcCodeMyan;

    @SerializedName("State Code")
    private String stateCode;

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getIsState() {
        return isState;
    }

    public void setIsState(String isState) {
        this.isState = isState;
    }

    public String getDivNrcCode() {
        return divNrcCode;
    }

    public void setDivNrcCode(String divNrcCode) {
        this.divNrcCode = divNrcCode;
    }

    public String getStateBName() {
        return stateBName;
    }

    public void setStateBName(String stateBName) {
        this.stateBName = stateBName;
    }

    public String getDivNrcCodeMyan() {
        return divNrcCodeMyan;
    }

    public void setDivNrcCodeMyan(String divNrcCodeMyan) {
        this.divNrcCodeMyan = divNrcCodeMyan;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
}