package com.mart.onestopkitchen.model.filter.selected;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FilterItem implements Serializable {

    @SerializedName("categoryName")
    private String categoryName;

    @SerializedName("items")
    private List<ItemsItem> items;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<ItemsItem> getItems() {
        return items;
    }

    public void setItems(List<ItemsItem> items) {
        this.items = items;
    }
}