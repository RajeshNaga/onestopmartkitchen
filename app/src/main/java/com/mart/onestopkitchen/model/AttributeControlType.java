package com.mart.onestopkitchen.model;

/**
 * Created by Ashraful on 11/25/2015.
 */
public class AttributeControlType {

    public static int DropdownList = 1;

    public static int RadioList = 2;

    public static int Checkboxes = 3;

    public static int TextBox = 4;
    public static int MultilineTextbox = 10;

    public static int Datepicker = 20;

    public static int FileUpload = 30;

    public static int ColorSquares = 40;

    public static int ReadonlyCheckboxes = 50;
}
