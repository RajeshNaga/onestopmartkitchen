package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ashraful on 11/20/2015.
 */
public class AddToCart {

    @SerializedName("IsWishList")
    @Expose
    private Boolean isWishList;

    @SerializedName("AllowedQuantities")
    @Expose
    private List<AllowedQuantity> allowedQuantities = null;

    public Boolean getWishList() {
        return isWishList;
    }

    public void setWishList(Boolean wishList) {
        isWishList = wishList;
    }

    public List<AllowedQuantity> getAllowedQuantities() {
        return allowedQuantities;
    }

    public void setAllowedQuantities(List<AllowedQuantity> allowedQuantities) {
        this.allowedQuantities = allowedQuantities;
    }


}
