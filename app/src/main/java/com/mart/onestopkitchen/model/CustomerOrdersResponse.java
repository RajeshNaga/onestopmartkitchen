package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mart-110 on 12/18/2015.
 */


public class CustomerOrdersResponse extends BaseResponse implements Serializable {

    @SerializedName("Orders")
    @Expose
    private ArrayList<CustomerOrder> orders = null;

    @SerializedName("RecurringOrders")
    @Expose
    private List<Object> recurringOrders = null;

    @SerializedName("CancelRecurringPaymentErrors")
    @Expose
    private List<Object> cancelRecurringPaymentErrors = null;

    @SerializedName("SuccessMessage")
    @Expose
    private String successMessage="";

    public ArrayList<CustomerOrder> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<CustomerOrder> orders) {
        this.orders = orders;
    }

    public List<Object> getRecurringOrders() {
        return recurringOrders;
    }

    public void setRecurringOrders(List<Object> recurringOrders) {
        this.recurringOrders = recurringOrders;
    }

    public List<Object> getCancelRecurringPaymentErrors() {
        return cancelRecurringPaymentErrors;
    }

    public void setCancelRecurringPaymentErrors(List<Object> cancelRecurringPaymentErrors) {
        this.cancelRecurringPaymentErrors = cancelRecurringPaymentErrors;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }
}
