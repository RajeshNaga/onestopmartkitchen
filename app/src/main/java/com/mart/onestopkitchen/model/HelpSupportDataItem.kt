/*
 * Copyright (c) : mart , Created By: Akash Garg.
 */

package com.mart.onestopkitchen.model

class HelpSupportDataItem(
        val Value: String? = null,
        val Title: String? = null) {
}