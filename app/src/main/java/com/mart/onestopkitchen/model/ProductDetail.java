package com.mart.onestopkitchen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ashraful on 11/20/2015.
 */
public class ProductDetail extends ProductModel {

    List<PictureModel> PictureModels;
    String FullDescription;
    String StockAvailability;
    List<ProductDetail> AssociatedProducts;


    @SerializedName("ProductSpecifications")
    List<ProductSpecification> productSpecifications;
    List<ProductAttribute> ProductAttributes;
    @SerializedName("Quantity")
    private Quantity quantity = new Quantity();
    @SerializedName("AddToCart")
    @Expose
    private AddToCart addToCart;
    @SerializedName("AvailableStartDateTimeUtc")
    @Expose
    private String availableStartDateTimeUtc;
    @SerializedName("AvailableEndDateTimeUtc")
    @Expose
    private String availableEndDateTimeUtc;

    @SerializedName("ProductCategoryName")
    @Expose
    private String ProductCategoryName = "";


    public String getProductCategoryName() {
        return ProductCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        ProductCategoryName = productCategoryName;
    }

    public String getAvailableStartDateTimeUtc() {
        return availableStartDateTimeUtc;
    }

    public void setAvailableStartDateTimeUtc(String availableStartDateTimeUtc) {
        this.availableStartDateTimeUtc = availableStartDateTimeUtc;
    }

    public String getAvailableEndDateTimeUtc() {
        return availableEndDateTimeUtc;
    }

    public void setAvailableEndDateTimeUtc(String availableEndDateTimeUtc) {
        this.availableEndDateTimeUtc = availableEndDateTimeUtc;
    }

    public List<ProductAttribute> getProductAttributes() {
        return ProductAttributes;
    }

    public void setProductAttributes(List<ProductAttribute> productAttributes) {
        ProductAttributes = productAttributes;
    }

    public String getStockAvailability() {
        return StockAvailability;
    }

    public void setStockAvailability(String stockAvailability) {
        StockAvailability = stockAvailability;
    }


    public String getFullDescription() {
        return FullDescription;
    }

    public void setFullDescription(String fullDescription) {
        FullDescription = fullDescription;
    }

    public List<PictureModel> getPictureModels() {
        return PictureModels;
    }

    public void setPictureModels(List<PictureModel> pictureModels) {
        PictureModels = pictureModels;
    }

    public List<ProductDetail> getAssociatedProducts() {
        return AssociatedProducts;
    }

    public void setAssociatedProducts(List<ProductDetail> associatedProducts) {
        AssociatedProducts = associatedProducts;
    }

    public List<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }

    public void setProductSpecifications(List<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public AddToCart getAddToCart() {
        return addToCart;
    }

    public void setAddToCart(AddToCart addToCart) {
        this.addToCart = addToCart;
    }
}
