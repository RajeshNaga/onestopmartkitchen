package com.mart.onestopkitchen.model.okdollar;

import android.content.Context;

import com.mart.onestopkitchen.ui.AppConstant;
import com.mart.onestopkitchen.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by kapil on 15/07/15.
 */
public class NetworkOperatorModel {

    public static NetworkOperatorModel networkOperatorModel = new NetworkOperatorModel();
    String countryCode = "", countryName = "", MCC = "", MNC = "", networkName = "", operatorName = "", status = "", countryFlagCode = "";
    int simMCC, simMNC, imageIndex = -1;
    int countryImageId = -1;
    ArrayList<String> listOfDialingCodes;

    public static NetworkOperatorModel getInstance() {
        return networkOperatorModel;
    }

    public int getCountryImageId() {
        return countryImageId;
    }

    public void setCountryImageId(int countryImageId) {
        this.countryImageId = countryImageId;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public void setImageIndex(int imageIndex) {
        this.imageIndex = imageIndex;
    }

    public ArrayList<String> getListOfDialingCodes() {
        return listOfDialingCodes;
    }

    public void setListOfDialingCodes(ArrayList<String> listOfDialingCodes) {
        this.listOfDialingCodes = listOfDialingCodes;
    }

    public NetworkOperatorModel getNetworkOperatorModel(Context context, String MCC_MNC) {

        try {

            simMCC = Integer.parseInt(MCC_MNC.substring(0, 3));
            simMNC = Integer.parseInt(MCC_MNC.substring(3));

            JSONArray array = null;
            array = varifyArrayResponse(AppUtils.readTextFileFromAssets(context, "mcc_mnc.txt"));
            for (int i = 0; i < array.length(); i++) {
                NetworkOperatorModel model = new NetworkOperatorModel();
                JSONObject jsonObject = (JSONObject) array.get(i);
                MCC = jsonObject.getString("MCC");
                if (Integer.parseInt(MCC) == simMCC) {
                    countryCode = jsonObject.getString("CountryCode");
                    countryName = jsonObject.getString("CountryName");
                    countryFlagCode = jsonObject.getString("CountryFlagCode");
                    JSONArray networkArray = jsonObject.getJSONArray("networks");
                    for (int j = 0; j < networkArray.length(); j++) {
                        JSONObject jsonObjectNetWork = (JSONObject) networkArray.get(j);
                        MNC = jsonObjectNetWork.getString("MNC");
                        if (Integer.parseInt(MNC) == simMNC) {
                            networkName = jsonObjectNetWork.getString("NetworkName");
                            operatorName = jsonObjectNetWork.getString("OperatorName");
                            status = jsonObjectNetWork.getString("Status");
                            model.setCountryCode(countryCode);
                            model.setCountryName(countryName);
                            model.setMCC(MCC);
                            model.setMNC(MNC);
                            model.setNetworkName(networkName);
                            model.setOperatorName(operatorName);
                            model.setStatus(status);
                            model.setCountryFlagCode(countryFlagCode);
                            model.setImageIndex(i);

                            return model;
                        }
                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public ArrayList<NetworkOperatorModel> getListOfNetworkOperatorModel(Context context) {
        ArrayList<NetworkOperatorModel> list = new ArrayList<>();
        String prevCountryCode = "";
        try {
            JSONArray array = null;
            array = varifyArrayResponse(AppUtils.readTextFileFromAssets(context, "mcc_mnc.txt"));
            for (int i = 0; i < array.length(); i++) {
                NetworkOperatorModel model = new NetworkOperatorModel();
                JSONObject jsonObject = (JSONObject) array.get(i);
                countryCode = jsonObject.getString("CountryCode");
                countryName = jsonObject.getString("CountryName");

                if (countryName.equalsIgnoreCase("Canada")) {
                    listOfDialingCodes = new ArrayList<>();
                    JSONArray dailingCodes = jsonObject.getJSONArray("DialingCodes");
                    for (int j = 0; j < dailingCodes.length(); j++) {
                        JSONObject jsonObjectDialing = (JSONObject) dailingCodes.get(j);
                        String dialingCode = jsonObjectDialing.getString("Code");
                        listOfDialingCodes.add(dialingCode);
                    }
                    model.setListOfDialingCodes(listOfDialingCodes);
                }

                if (countryName.equalsIgnoreCase("UnitedStates")) {
                    listOfDialingCodes = new ArrayList<>();
                    JSONArray dailingCodes = jsonObject.getJSONArray("DialingCodes");
                    for (int j = 0; j < dailingCodes.length(); j++) {
                        JSONObject jsonObjectDialing = (JSONObject) dailingCodes.get(j);
                        String dialingCode = jsonObjectDialing.getString("Code");
                        listOfDialingCodes.add(dialingCode);
                    }
                    model.setListOfDialingCodes(listOfDialingCodes);
                }

                model.setCountryImageId(AppConstant.listOfFlags[i]);
                model.setCountryCode(countryCode);
                model.setCountryName(countryName);
                if (!prevCountryCode.equalsIgnoreCase(countryCode)) {
                    list.add(model);
                }
                prevCountryCode = countryCode;
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    public String getCountryFlagCode() {
        return countryFlagCode;
    }

    public void setCountryFlagCode(String countryFlagCode) {
        this.countryFlagCode = countryFlagCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getMNC() {
        return MNC;
    }

    public void setMNC(String MNC) {
        this.MNC = MNC;
    }

    public String getMCC() {
        return MCC;
    }

    public void setMCC(String MCC) {
        this.MCC = MCC;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JSONArray varifyArrayResponse(String jsonString) throws Exception {
        try {
            JSONObject resp = new JSONObject(jsonString);
            JSONObject status = resp.getJSONObject("status");
            JSONArray data = null;

            String message = status.getString("msg");
            int status_code = status.getInt("code");
            switch (status_code) {
                case 200:
                    if (resp.has("data")) {
                        data = resp.getJSONArray("data");
                    }


                    break;
                case 400:
                    throw new Exception(message);
                case 401:
                    throw new Exception(message);
                case 402:
                    throw new Exception(message);
                case 405:
                    throw new Exception(message);

                default:
                    break;
            }
            return data;
        } catch (JSONException e) {
            throw new Exception("Invalid response format.");
        }
    }
}
