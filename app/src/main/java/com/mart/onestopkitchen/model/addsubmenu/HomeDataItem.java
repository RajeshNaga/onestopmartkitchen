package com.mart.onestopkitchen.model.addsubmenu;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HomeDataItem implements Serializable {

    @SerializedName("Extension")
    private Object extension;

    @SerializedName("IconPath")
    private String iconPath;

    @SerializedName("DisplayOrder")
    private int displayOrder;

    @SerializedName("ProductCount")
    private int productCount;

    @SerializedName("Children")
    private List<Object> children;

    @SerializedName("Id")
    private int id;

    @SerializedName("ParentCategoryId")
    private int parentCategoryId;

    @SerializedName("Name")
    private String name;

    public Object getExtension() {
        return extension;
    }

    public void setExtension(Object extension) {
        this.extension = extension;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public int getProductCount() {
        return productCount;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    public List<Object> getChildren() {
        return children;
    }

    public void setChildren(List<Object> children) {
        this.children = children;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(int parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}