package com.mart.onestopkitchen.model.creditcarddao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mart.onestopkitchen.utils.AppConstants.TABLE_CARD_DETAIL

/**
 * Created by Akash Garg on 16/01/2019.
 */

@Entity(tableName = TABLE_CARD_DETAIL)
class CardDetailModel {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "userId")
    var userId: Int? = null

    @ColumnInfo(name = "cardHolderName")
    var cardHolderName: String = ""

    @ColumnInfo(name = "cardNumber")
    var cardNumber: String = ""

    @ColumnInfo(name = "cardCvv")
    var cardCvv: String = ""

    @ColumnInfo(name = "cardExpMonth")
    var cardExpMonth: String = ""

    @ColumnInfo(name = "cardExpYear")
    var cardExpYear: String = ""
}