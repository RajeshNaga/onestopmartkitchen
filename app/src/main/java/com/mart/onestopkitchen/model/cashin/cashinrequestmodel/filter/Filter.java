package com.mart.onestopkitchen.model.cashin.cashinrequestmodel.filter;

import com.google.gson.annotations.SerializedName;

public class Filter {

    @SerializedName("Opened")
    private boolean opened;

    @SerializedName("FullDayWorking")
    private boolean fullDayWorking;

    @SerializedName("IncludeStationaryAgent")
    private boolean includeStationaryAgent;

    @SerializedName("IncludeMobileAgent")
    private boolean includeMobileAgent;

    @SerializedName("RecentlyClosing")
    private boolean recentlyClosing;

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public boolean isFullDayWorking() {
        return fullDayWorking;
    }

    public void setFullDayWorking(boolean fullDayWorking) {
        this.fullDayWorking = fullDayWorking;
    }

    public boolean isIncludeStationaryAgent() {
        return includeStationaryAgent;
    }

    public void setIncludeStationaryAgent(boolean includeStationaryAgent) {
        this.includeStationaryAgent = includeStationaryAgent;
    }

    public boolean isIncludeMobileAgent() {
        return includeMobileAgent;
    }

    public void setIncludeMobileAgent(boolean includeMobileAgent) {
        this.includeMobileAgent = includeMobileAgent;
    }

    public boolean isRecentlyClosing() {
        return recentlyClosing;
    }

    public void setRecentlyClosing(boolean recentlyClosing) {
        this.recentlyClosing = recentlyClosing;
    }
}