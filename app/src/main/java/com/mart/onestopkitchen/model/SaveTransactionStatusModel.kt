package com.mart.onestopkitchen.model

import com.mart.onestopkitchen.networking.BaseResponse

data class SaveTransactionStatusModel(
        val transactionAmount: Double? = null,
        val paymentMethod: String? = null,
        val transactionDescription: String? = null,
        val transactionId: String? = null,
        val transactionStatusCode: String? = null
) : BaseResponse() {
    override fun toString(): String {
        return "transactionAmount=$transactionAmount, paymentMethod=$paymentMethod, transactionStatus=$transactionDescription, transactionId=$transactionId"
    }
}
