package com.mart.onestopkitchen.model;

/**
 * Created by Ashraful on 11/20/2015.
 */
public class PictureModel extends ImageModel {
    private String FullSizeImageUrl = "";
    private int Id;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFullSizeImageUrl() {
        return FullSizeImageUrl;
    }

    public void setFullSizeImageUrl(String fullSizeImageUrl) {
        FullSizeImageUrl = fullSizeImageUrl;
    }
}
