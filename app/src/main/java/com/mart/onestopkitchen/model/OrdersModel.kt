package com.mart.onestopkitchen.model

import com.mart.onestopkitchen.networking.BaseResponse

data class OrdersModel(
        val CustomOrderNumber: String? = null,
        val ExpectedDeliveryDate: String? = null,
        val OrderId: Int? = null,

        val Id: Int? = null,
        val CreatedOn: String? = null,
        val PickUpInStore: Boolean? = null,
        val OrderStatus: String? = null,
        val PaymentMethod: String? = null,
        val PaymentMethodStatus: String? = null,
        val OrderSubtotal: String? = null,
        val OrderShipping: String? = null,
        val Tax: String? = null,
        val RedeemedRewardPoints: String? = null,
        val OrderTotal: String? = null,
        var Items: List<PDFProductItem>? = null
) : BaseResponse()
