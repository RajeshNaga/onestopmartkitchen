package com.mart.onestopkitchen.model.recentview;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataItem implements Serializable {

    @SerializedName("DisplayOrder")
    private int displayOrder;

    @SerializedName("DefaultPictureModel")
    private DefaultPictureModel defaultPictureModel;

    @SerializedName("Form")
    private Object form;

    @SerializedName("ProductPrice")
    private ProductPrice productPrice;

    @SerializedName("DisableWishlistButton")
    private boolean disableWishlistButton;

    @SerializedName("MarkAsNew")
    private boolean markAsNew;

    @SerializedName("Name")
    private String name;

    @SerializedName("ShortDescription")
    private String shortDescription;

    @SerializedName("ReviewOverviewModel")
    private ReviewOverviewModel reviewOverviewModel;

    @SerializedName("IsWishList")
    private boolean isWishList;

    @SerializedName("CreatedOn")
    private String createdOn;

    @SerializedName("Id")
    private int id;

    @SerializedName("Sku")
    private String sku;

    @SerializedName("CustomProperties")
    private CustomProperties customProperties;

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public DefaultPictureModel getDefaultPictureModel() {
        return defaultPictureModel;
    }

    public void setDefaultPictureModel(DefaultPictureModel defaultPictureModel) {
        this.defaultPictureModel = defaultPictureModel;
    }

    public Object getForm() {
        return form;
    }

    public void setForm(Object form) {
        this.form = form;
    }

    public ProductPrice getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        this.productPrice = productPrice;
    }

    public boolean isDisableWishlistButton() {
        return disableWishlistButton;
    }

    public void setDisableWishlistButton(boolean disableWishlistButton) {
        this.disableWishlistButton = disableWishlistButton;
    }

    public boolean isMarkAsNew() {
        return markAsNew;
    }

    public void setMarkAsNew(boolean markAsNew) {
        this.markAsNew = markAsNew;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public ReviewOverviewModel getReviewOverviewModel() {
        return reviewOverviewModel;
    }

    public void setReviewOverviewModel(ReviewOverviewModel reviewOverviewModel) {
        this.reviewOverviewModel = reviewOverviewModel;
    }

    public boolean isIsWishList() {
        return isWishList;
    }

    public void setIsWishList(boolean isWishList) {
        this.isWishList = isWishList;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public CustomProperties getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(CustomProperties customProperties) {
        this.customProperties = customProperties;
    }
}