package com.mart.onestopkitchen.presenter

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.util.Log
import com.mart.onestopkitchen.networking.RetroClient
import com.mart.onestopkitchen.networking.response.PickupPointAddressResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.net.ConnectException

/**
 * Created by Akash Garg
 */

@SuppressLint("StaticFieldLeak")
open class PickupPointPresenter {
    private var data: MutableLiveData<PickupPointAddressResponse> = MutableLiveData()

    init {
        getPickUpList()
    }

    fun getResponse(): LiveData<PickupPointAddressResponse>? {
        return data
    }


    @SuppressLint("CheckResult")
    fun getPickUpList() {
        getPickupPointObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver())
    }

    private fun getObserver(): DisposableObserver<PickupPointAddressResponse> {
        return object : DisposableObserver<PickupPointAddressResponse>() {
            override fun onComplete() {
            }

            override fun onNext(result: PickupPointAddressResponse) {
                data.postValue(result)
            }

            override fun onError(e: Throwable) {
                if (e is ConnectException) {
                    val error = PickupPointAddressResponse()
                    error.statusCode = 401
                    data.postValue(error)
                }
                Log.e("----", "#No Internet Connectoion -----: ${e.message}")
            }
        }
    }

    private fun getPickupPointObservable(): Observable<PickupPointAddressResponse> {
        return RetroClient.getApi().pickupPoints
    }
}