package com.mart.onestopkitchen.presenter

import android.content.Context
import com.mart.onestopkitchen.baseview.NotificationView
import com.mart.onestopkitchen.model.NotificationModel
import com.mart.onestopkitchen.networking.RetroClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class NotificationPresenter(val mContext: Context, var listener: NotificationView) {
    lateinit var disposableObserver: DisposableObserver<NotificationModel>

    init {
        getNotificationList()
    }

    private fun getNotificationList() {
        disposableObserver.let {
            it.dispose().takeIf {
                !disposableObserver.isDisposed
            }
        }
        disposableObserver = getNotificationObservable().subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(getObserver())
    }

    private fun getNotificationObservable(): Observable<NotificationModel> {
        return RetroClient.getApi().notificationsList
    }

    private fun getObserver(): DisposableObserver<NotificationModel> {
        return object : DisposableObserver<NotificationModel>() {
            override fun onComplete() {

            }

            override fun onNext(t: NotificationModel) {
                listener.let { listener.notificationsList() }
            }

            override fun onError(e: Throwable) {
                listener.let { listener.error() }
            }
        }
    }
}