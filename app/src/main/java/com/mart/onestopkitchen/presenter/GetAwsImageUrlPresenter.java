package com.mart.onestopkitchen.presenter;

import com.google.gson.JsonObject;
import com.mart.onestopkitchen.model.CommonResponseDao;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.utils.ApisModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class GetAwsImageUrlPresenter {

    private ImageResponseListener listener;
    private DisposableObserver<CommonResponseDao> disposableObserver;

    public GetAwsImageUrlPresenter(ImageResponseListener listener, JsonObject jsonObj) {
        this.listener = listener;
        getObservable(jsonObj);
    }

    private void getObservable(JsonObject jsonObj) {
        if (null != disposableObserver && !disposableObserver.isDisposed()) {
            disposableObserver.dispose();
            disposableObserver = null;
        }
        disposableObserver = getImageUrlObservable(jsonObj)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getImageUrlObserver());
    }

    private DisposableObserver<CommonResponseDao> getImageUrlObserver() {
        return new DisposableObserver<CommonResponseDao>() {

            @Override
            public void onNext(CommonResponseDao commonResponseDao) {
                if (commonResponseDao.getCode() == 200) {
                    String url = commonResponseDao.getData().substring(2, commonResponseDao.getData().length() - 2);
                    if (url.contains(" ")) {
                        url = url.replace(" ", "%20");
                    }
                    if (null != listener) {
                        listener.imageOnSuccess(url);
                        listener.hideLoading();
                    }
                } else {
                    if (null != listener) {
                        listener.onFailure(commonResponseDao.getMsg());
                        listener.hideLoading();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (null != listener) {
                    listener.onFailure(e.getMessage());
                }
                onDispose();
            }

            @Override
            public void onComplete() {
                onDispose();
            }
        };
    }

    public void onDispose() {
        if (null != disposableObserver && !disposableObserver.isDisposed()) {
            disposableObserver.dispose();
            disposableObserver = null;
        }
        if (null != listener) {
            listener = null;
        }

    }

    private Observable<CommonResponseDao> getImageUrlObservable(JsonObject jsonObj) {
        return RetroClient.getDynamicUrlApi(ApisModel.Companion.getInstance().getOK_DOLLAR_URL()).getImageUrlByBase64String(1, jsonObj);
    }

    public interface ImageResponseListener {
        void imageOnSuccess(String imgUrl);

        void onFailure(String message);

        void showLoading();

        void hideLoading();
    }
}
