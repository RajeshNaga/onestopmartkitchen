package com.mart.onestopkitchen.presenter

import android.annotation.SuppressLint
import android.content.Context
import com.mart.onestopkitchen.networking.RetroClient
import com.mart.onestopkitchen.networking.response.StoreSaveResponse
import com.mart.onestopkitchen.service.PreferenceService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class PickupPointSelectedPresenter(val mContext: Context, private val pId: String, val listener: SavePickUpPoint) {
    init {
        setPickupPoint()
    }

    @SuppressLint("CheckResult")
    private fun setPickupPoint() {
        getObservable().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver())
    }

    private fun getObserver(): DisposableObserver<StoreSaveResponse> {
        return object : DisposableObserver<StoreSaveResponse>() {
            override fun onComplete() {

            }

            override fun onNext(result: StoreSaveResponse) {
                listener.savedPickupPoint(result.isData)
            }

            override fun onError(e: Throwable) {
                listener.errorPickupPoint()
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, "")
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0")
            }
        }
    }

    private fun getObservable(): Observable<StoreSaveResponse> {
        return RetroClient.getApi().saveStoreAddress(pId)
    }

    interface SavePickUpPoint {
        fun savedPickupPoint(isSave: Boolean)
        fun errorPickupPoint()
    }
}