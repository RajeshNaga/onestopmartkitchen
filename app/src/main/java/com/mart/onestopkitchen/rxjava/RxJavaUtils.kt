package com.mart.onestopkitchen.rxjava

import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Thavam on 10-Nov-18.
 */
class RxJavaUtils {
    companion object {
        fun <T> applySchedulers(): ObservableTransformer<T, T> {
            return ObservableTransformer { upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
        }
    }
}
