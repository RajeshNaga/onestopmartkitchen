package com.mart.onestopkitchen.rxjava

import com.mart.onestopkitchen.application.MyApplication
import com.mart.onestopkitchen.model.OrderDetailsResponse
import com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel
import com.mart.onestopkitchen.model.appratingandreview.AppReviewAndRating
import com.mart.onestopkitchen.model.featureproduct.FeatureProductResponseModel
import com.mart.onestopkitchen.model.ratingreview.RatingGettingModel
import com.mart.onestopkitchen.model.ratingreview.RatingRequestModel
import com.mart.onestopkitchen.model.ratingreview.RatingReviewRequestModel
import com.mart.onestopkitchen.networking.Api
import io.reactivex.Observable

class ApiHelper {
    companion object {
        private var serviceFactory: Api? = MyApplication.getApi()
        private var helper: ApiHelper? = null
        fun getDataHelper() = helper ?: synchronized(this) {
            ApiHelper().also { helper = it }

        }
    }

    fun getRatingAndReviewProduct(request: Int, review: RatingReviewRequestModel?): Observable<RatingRequestModel> {
        return serviceFactory!!.ratingAndReview(request, review)
    }

    fun getRatingAndReviewOrder(request: Int): Observable<RatingGettingModel> {
        return serviceFactory!!.getratingAndReview(request)
    }

    fun setUserLanguage(request: Int): Observable<OrderDetailsResponse> {
        return serviceFactory!!.setUserLanguage(request)
    }

    fun availableSort(request: String, size: String): Observable<FeatureProductResponseModel> {
        return serviceFactory!!.featureProductSort(size, request)
    }

    fun getRatingAndReviewByOrder(review: AppReviewAndRating?): Observable<AppRatingResponseModel> {
        return serviceFactory!!.ratingAndReview(review)
    }
}