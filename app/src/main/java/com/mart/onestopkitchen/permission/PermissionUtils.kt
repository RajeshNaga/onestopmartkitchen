package com.mart.onestopkitchen.permission

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat


class PermissionUtils {

    private var loopExecute: Boolean = false
    private val permission = ArrayList<String>(0)
    private var mContext: Context? = null
    private var mListener: PermissionListener? = null
    private var deniedPermission = false

    companion object {
        // var ACCESS_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
        //  var WRITE_CONTACTS = Manifest.permission.WRITE_CONTACTS
        //  var CALL_PHONE = Manifest.permission.CALL_PHONE
        var ACCESS_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
        var READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE
        var CAMERA = Manifest.permission.CAMERA
        var RECORD_AUDIO = Manifest.permission.RECORD_AUDIO
        var GET_ACCOUNTS = Manifest.permission.GET_ACCOUNTS
        var WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
        var READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE
        var READ_CONTACTS = Manifest.permission.READ_CONTACTS
        var REQUEST_CODE = 10

        @SuppressLint("StaticFieldLeak")
        val instance = PermissionUtils()
    }

    fun with(context: Context): PermissionUtils {
        this.mContext = context
        loopExecute = false
        return instance
    }

    fun setListener(mListener: PermissionListener): PermissionUtils {
        this.mListener = mListener
        return instance
    }

    private fun isAvailableAllPermission(): Boolean {
        var isAllowed = true
        for (item in permission) {
            isAllowed = ActivityCompat.checkSelfPermission(mContext!!, item) == PackageManager.PERMISSION_GRANTED
            if (!isAllowed)
                break
        }
        return isAllowed
    }

    fun requestPermission(permissions: Array<String>) {
        if (mListener == null)
            return
        if (mContext == null)
            return
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mListener!!.allPermissionGranted()
            return
        }
        if (permissions.isNotEmpty()) {
            this.permission.clear()
            checkPermissionAlReadyGranted(permissions)
        }
        if (permission.size > 0) {
            if (mContext is Activity)
                ActivityCompat.requestPermissions(mContext as Activity, this.permission.toArray(arrayOfNulls(0)), REQUEST_CODE)
        } else mListener!!.allPermissionGranted()
    }

    private fun checkPermissionAlReadyGranted(permission: Array<String>) {
        for (item in permission) {
            if (ActivityCompat.checkSelfPermission(mContext!!, item) != PackageManager.PERMISSION_GRANTED) {
                this.permission.addAll(permission)
            }
        }
    }

    fun onPermissionRequestResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE -> {
                if (!isAvailableAllPermission()) {
                    var count = 0
                    for (name in permissions) {
                        count++
                        deniedPermission = ActivityCompat.shouldShowRequestPermissionRationale(mContext as Activity, name)
                        if (deniedPermission) {
                            loopExecute = true
                            break
                        }
                    }
                    if (count == permission.size || loopExecute)
                        if (!deniedPermission)
                            mListener!!.onNeverAskAgainPermission(permissions)
                        else
                            mListener!!.onDenied(permissions)

                } else
                    mListener!!.allPermissionGranted()
            }
        }
    }
}

interface PermissionListener {
    fun allPermissionGranted()

    fun onNeverAskAgainPermission(string: Array<String>)

    fun onDenied(string: Array<String>)
}