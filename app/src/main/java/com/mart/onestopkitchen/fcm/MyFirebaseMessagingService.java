package com.mart.onestopkitchen.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.firebase.messaging.RemoteMessage;
import com.quickblox.messages.services.fcm.QBFcmPushListenerService;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.chat.services.LoginService;
import com.mart.onestopkitchen.chat.ui.activity.DialogsActivity;
import com.mart.onestopkitchen.chat.utils.ActivityLifecycle;
import com.mart.onestopkitchen.chat.utils.NotificationUtils;
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;

import java.io.IOException;
import java.util.Map;

/**
 * Updated by AkashGarg on 24-nov-18.
 */

public class MyFirebaseMessagingService extends QBFcmPushListenerService {
    public static final String TITLE = "title";
    public static final String BODY = "body";
    public static final String BANNER = "banner";
    public static final String MESSAGE = "message";

    public static final String ITEM_TYPE = "itemType";
    public static final String ITEM_ID = "itemId";
    public static final int ITEM_PRODUCT = 1;
    public static final int ITEM_CATEGORY = 2;
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private static final int REQUEST_CODE = 0;
    private static final int NOTIFICATION_ID = 1;
    public String NOTIFICATION_CHANNEL_ID = "10001";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e(TAG, "###------------------------onMessageReceived----------:");

        if (remoteMessage.getData().size() > 0) {
            if (remoteMessage.getData().get(BODY) != null) {
                sendNotification(remoteMessage.getData());
            }
        }
        if (SharedPrefsHelper.getInstance().hasQbUser()) {
            QBUser qbUser = SharedPrefsHelper.getInstance().getQbUser();
            if (qbUser != null) {
//                if (AppUtils.isMyServiceRunning(LoginService.class, this)) {
//                    Log.e(TAG, "###------------------------LoginService is already running -------------:" + remoteMessage.getData().get(BODY));
//                    stopService(new Intent(this, LoginService.class));
//                }
                Log.e(TAG, "----------App has logged user" + qbUser.getId() + " : --------------");
                LoginService.start(this, qbUser);
            }
        }
    }


    @Override
    protected void sendPushMessage(@NonNull Map data, String from, String message) {
        super.sendPushMessage(data, from, message);
        Log.e(TAG, "###------------------------sendPushMessage--------------:" + message); //data:{user_id=5796, badge=1, message=You have 1 new message, dialog_id=5d9c319294a9681311bd6ca4}
        Log.e(TAG, "##data:" + data.toString());
        Log.e(TAG, "##From:" + from);
        Log.e(TAG, "###Message: " + message);

        if (ActivityLifecycle.getInstance().isBackground()) {
            showNotification(message);
        }
    }

    private void showNotification(String message) {
        NotificationUtils.INSTANCE.showNotification(this, DialogsActivity.class,
                getString(R.string.notification_title), message,
                R.mipmap.app_icon, NOTIFICATION_ID);
    }


    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        Log.d(TAG, "######onNewTokenRefreshed token ----:" + refreshedToken);
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.FCM_TOKEN, refreshedToken);
    }

    private void sendNotification(Map<String, String> data) {
        String title = "1 Stop Kitchen";
        String body = data.get(BODY) != null ? data.get(BODY) : data.get(MESSAGE);
        String bigPicture = data.get(BANNER);

        Intent intent = new Intent(this, MainActivity.class);
        Bundle bundle = new Bundle();
        for (String key : data.keySet()) {
            bundle.putString(key, data.get(key));
        }
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(R.mipmap.app_icon)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.app_icon))
                .setColor(getResources().getColor(R.color.accent))
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        Bitmap bitmap = null;
        try {
            bitmap = Picasso.with(this).load(bigPicture).get();
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }

        if (bitmap != null) {
            NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
            style.setBigContentTitle(title);
            style.setSummaryText(body);
            style.bigPicture(bitmap);
            mBuilder.setStyle(style);
        }
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            if (mNotificationManager != null) {
                mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
        }
        if (mNotificationManager != null)
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
