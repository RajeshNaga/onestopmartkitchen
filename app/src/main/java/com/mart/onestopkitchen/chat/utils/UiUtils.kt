package com.mart.onestopkitchen.chat.utils

import android.graphics.drawable.Drawable
import androidx.annotation.*
import androidx.annotation.IntRange
import com.mart.onestopkitchen.application.MyApplication

private const val RANDOM_COLOR_START_RANGE = 0
private const val RANDOM_COLOR_END_RANGE = 9


/*fun getColorCircleDrawable(colorPosition: Int): Drawable {
    return getColoredCircleDrawable(getCircleColor(colorPosition % RANDOM_COLOR_END_RANGE))
}

fun getColoredCircleDrawable(@ColorInt color: Int): Drawable {
    val drawable = getDrawable(R.drawable.shape_circle) as GradientDrawable
    drawable.setColor(color)
    return drawable
}*/

fun getCircleColor(@IntRange(from = RANDOM_COLOR_START_RANGE.toLong(), to = RANDOM_COLOR_END_RANGE.toLong())
                   colorPosition: Int): Int {
    val colorIdName = String.format("random_color_%d", colorPosition + 1)
    val colorId = MyApplication.getAppContext().resources
            .getIdentifier(colorIdName, "color", MyApplication.getAppContext().packageName)

    return getColor(colorId)
}

fun getString(@StringRes stringId: Int): String {
    return MyApplication.getAppContext().getString(stringId)
}

fun getDrawable(@DrawableRes drawableId: Int): Drawable {
    return MyApplication.getAppContext().resources.getDrawable(drawableId)
}

fun getColor(@ColorRes colorId: Int): Int {
    return MyApplication.getAppContext().resources.getColor(colorId)
}

fun getDimen(@DimenRes dimenId: Int): Int {
    return MyApplication.getAppContext().resources.getDimension(dimenId).toInt()
}