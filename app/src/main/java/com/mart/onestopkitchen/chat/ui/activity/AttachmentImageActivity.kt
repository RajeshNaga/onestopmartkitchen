package com.mart.onestopkitchen.chat.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.chat.utils.PREFERRED_IMAGE_SIZE_FULL
import com.mart.onestopkitchen.ui.activity.BaseActivity


private const val EXTRA_URL = "url"

class AttachmentImageActivity : BaseActivity() {

    private lateinit var imageView: ImageView
    private lateinit var progressBar: ProgressBar
    private lateinit var img_back: ImageView

    companion object {
        fun start(context: Context, url: String) {
            val intent = Intent(context, AttachmentImageActivity::class.java)
            intent.putExtra(EXTRA_URL, url)
            context.startActivity(intent)
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_image)
        initUI()
        loadImage()
        hideKeyboard()
    }

    private fun initUI() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        imageView = findViewById(com.mart.onestopkitchen.R.id.image_full_view)
        progressBar = findViewById(com.mart.onestopkitchen.R.id.progress_bar_show_image)
        img_back = findViewById(com.mart.onestopkitchen.R.id.img_back)
        img_back.setOnClickListener { super.onBackPressed() }
    }

    private fun loadImage() {
        val url = intent.getStringExtra(EXTRA_URL)
        if (TextUtils.isEmpty(url)) {
            imageView.setImageResource(com.mart.onestopkitchen.R.drawable.placeholder)
        } else {
            progressBar.visibility = View.VISIBLE
            Glide.with(this)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(DrawableListener(progressBar))
                    .error(com.mart.onestopkitchen.R.drawable.placeholder)
                    .dontTransform()
                    .override(PREFERRED_IMAGE_SIZE_FULL, PREFERRED_IMAGE_SIZE_FULL)
                    .into(imageView)
        }
    }

    private inner class DrawableListener(private val progressBar: ProgressBar) : RequestListener<String, GlideDrawable> {

        override fun onException(e: Exception?, model: String, target: Target<GlideDrawable>,
                                 isFirstResource: Boolean): Boolean {
            e?.printStackTrace()
            showErrorSnackbar(com.mart.onestopkitchen.R.string.error_load_image, null, null)
            progressBar.visibility = View.GONE
            return false
        }

        override fun onResourceReady(resource: GlideDrawable, model: String, target: Target<GlideDrawable>,
                                     isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
            progressBar.visibility = View.GONE
            return false
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}