package com.mart.onestopkitchen.chat;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.GenericQueryRule;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.users.model.QBUser;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.chat.db.QbUsersDbManager;
import com.mart.onestopkitchen.chat.services.CallService;
import com.mart.onestopkitchen.chat.services.LoginService;
import com.mart.onestopkitchen.chat.ui.activity.CallActivity;
import com.mart.onestopkitchen.chat.utils.QBResRequestExecutorKt;
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper;
import com.mart.onestopkitchen.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import static com.okdollar.paymentgateway.utils.Utils.hideProgressDialog;
import static com.okdollar.paymentgateway.utils.Utils.showProgressDialog;
import static com.mart.onestopkitchen.chat.utils.ConstsKt.EXTRA_IS_INCOMING_CALL;

public class CallHandlePresenter {
    private static final String TAG = CallHandlePresenter.class.getSimpleName();
    private Context context = null;
    private static final int PER_PAGE_SIZE_100 = 100;
    private static final String ORDER_RULE = "order";
    private static final String ORDER_DESC_UPDATED = "desc date updated_at";

    private QBUser currentUser;
    private QbUsersDbManager dbManager;
    protected QBResRequestExecutorKt requestExecutor;

    public CallHandlePresenter(Context context) {
        this.context = context;
        initData();
        startLoginService();
    }

    private void initData() {
        currentUser = SharedPrefsHelper.getInstance().getQbUser();
        dbManager = QbUsersDbManager.INSTANCE;


        boolean isIncomingCall = SharedPrefsHelper.getInstance().get(EXTRA_IS_INCOMING_CALL, false);
        if (isCallServiceRunning(CallService.class)) {
            Log.d(TAG, "CallService is running now");
            CallActivity.Companion.start(context, isIncomingCall);
        }
        clearAppNotifications();
        loadUsers();
    }

    private void clearAppNotifications() {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }

    private void loadUsers() {
        showProgressDialog(context, context.getResources().getString(R.string.dlg_loading_opponents), false);
        ArrayList<GenericQueryRule> rules = new ArrayList<>();
        rules.add(new GenericQueryRule(ORDER_RULE, ORDER_DESC_UPDATED));

        QBPagedRequestBuilder qbPagedRequestBuilder = new QBPagedRequestBuilder();
        qbPagedRequestBuilder.setRules(rules);
        qbPagedRequestBuilder.setPerPage(PER_PAGE_SIZE_100);
        String chatTagName = ((MyApplication) context.getApplicationContext()).PRODUCT_CATEGORY_NAME;

        QBResRequestExecutorKt.loadUsersByTag(chatTagName, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
//                Log.e(TAG, "@@@@Successfully loaded Last 100 created users");
                dbManager.saveAllUsers(qbUsers, true);
                initUsersList();
                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(TAG, "Error load users" + e.getMessage());
                hideProgressDialog();
                ((BaseActivity) context).showErrorSnackbar(R.string.loading_users_error, e, v -> loadUsers());
            }
        });

    }

    private void initUsersList() {
        List<QBUser> currentOpponentsList = dbManager.getAllUsers();
        Log.d(TAG, "initUsersList currentOpponentsList= " + currentOpponentsList);
        currentOpponentsList.remove(SharedPrefsHelper.getInstance().getQbUser());
    }


    private void startLoginService() {
        if (SharedPrefsHelper.getInstance().hasQbUser()) {
            LoginService.start(context, SharedPrefsHelper.getInstance().getQbUser());
        }
    }


    private boolean isCallServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
