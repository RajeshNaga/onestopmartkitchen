package com.mart.onestopkitchen.chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.chat.ui.activity.DialogsActivity;
import com.mart.onestopkitchen.chat.utils.ChatHelper;
import com.mart.onestopkitchen.chat.utils.ConstsKt;
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper;
import com.mart.onestopkitchen.ui.activity.BaseActivity;

import static com.okdollar.paymentgateway.utils.Utils.hideProgressDialog;
import static com.okdollar.paymentgateway.utils.Utils.showProgressDialog;

public class LoginToMsgChat {
    private static final int UNAUTHORIZED = 401;
    private String TAG = "--LoginToMsgChat-";
    private Context mContext;
    @SuppressLint("StaticFieldLeak")
    private static LoginToMsgChat instance;

    private LoginToMsgChat(Context context) {
        this.mContext = context;
    }

    public static LoginToMsgChat getInstance(Context context) {
        if (instance == null)
            instance = new LoginToMsgChat(context);
        return instance;
    }

    public void msgChat(QBUser user) {
//        if (SharedPrefsHelper.getInstance().hasQbUser()) {
//            restoreChatSession(user);
//        } else {
        signIn(user);
//        }
    }

    private void restoreChatSession(QBUser user) {
        if (ChatHelper.getInstance().isLogged()) {
            DialogsActivity.start(mContext);
        } else {
            QBUser currentUser = getUserFromSession();
            if (currentUser == null) {
                signIn(user);
            } else {
                loginToChat(currentUser);

            }
        }
    }

    private QBUser getUserFromSession() {
        QBUser user = SharedPrefsHelper.getInstance().getQbUser();
        QBSessionManager qbSessionManager = QBSessionManager.getInstance();
        if (qbSessionManager.getSessionParameters() == null) {
            ChatHelper.getInstance().destroy();
            return null;
        }
        int userId = qbSessionManager.getSessionParameters().getUserId();
        user.setId(userId);
        return user;
    }

    private void signIn(final QBUser user) {
        showProgressDialog(mContext, mContext.getResources().getString(R.string.login), false);
        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getFullName().equals(user.getFullName())) {
                    loginToChat(user);
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null/*ConstsKt.USER_DEFAULT_PASSWORD*/);
                    updateUser(user);
                }
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getHttpStatusCode() == UNAUTHORIZED) {
                    signUp(user);
                } else {
                    hideProgressDialog();
                    ((BaseActivity) mContext).showErrorSnackbar(R.string.login_chat_login_error, e, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            signIn(user);
                        }
                    });
                }
            }
        });
    }

    private void updateUser(final QBUser user) {
        ChatHelper.getInstance().updateUser(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle bundle) {
                loginToChat(user);
            }

            @Override
            public void onError(QBResponseException e) {
                hideProgressDialog();
                ((BaseActivity) mContext).showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(ConstsKt.USER_DEFAULT_PASSWORD);
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                SharedPrefsHelper.getInstance().saveQbUser(user);
                DialogsActivity.start(mContext);
                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getMessage().equals("You have already logged in chat")) {
                    loginToChat(user);
                } else {
                    Log.e(TAG, "Chat login onError(): " + e);
                    ((BaseActivity) mContext).showErrorSnackbar(R.string.error_recreate_session, e,
                            v -> loginToChat(user));
                }
            }
        });
    }

    private void signUp(final QBUser newUser) {
        SharedPrefsHelper.getInstance().removeQbUser();
        QBUsers.signUp(newUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle bundle) {
                hideProgressDialog();
                signIn(newUser);
            }

            @Override
            public void onError(QBResponseException e) {
                hideProgressDialog();
                ((BaseActivity) mContext).showErrorSnackbar(R.string.login_sign_up_error, e, null);
            }
        });
    }

}
