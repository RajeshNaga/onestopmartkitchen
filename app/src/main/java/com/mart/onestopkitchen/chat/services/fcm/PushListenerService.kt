package com.mart.onestopkitchen.chat.services.fcm

import android.util.Log
import com.google.firebase.messaging.RemoteMessage
import com.quickblox.messages.services.fcm.QBFcmPushListenerService
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.chat.services.LoginService
import com.mart.onestopkitchen.chat.utils.ActivityLifecycle
import com.mart.onestopkitchen.chat.utils.NotificationUtils
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper
import com.mart.onestopkitchen.ui.activity.SplashScreenActivity

private const val NOTIFICATION_ID = 1

class PushListenerService : QBFcmPushListenerService() {
    private val TAG = PushListenerService::class.java.simpleName
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)


        Log.e(TAG, "### PushListenerService:: " + remoteMessage!!.data)

        if (SharedPrefsHelper.getInstance().hasQbUser()) {
            val qbUser = SharedPrefsHelper.getInstance().getQbUser()
            qbUser?.let {
                Log.d(TAG, "App has logged user" + qbUser.id)
                LoginService.start(this, qbUser)
            }
        }
    }

    override fun sendPushMessage(data: MutableMap<Any?, Any?>?, from: String?, message: String?) {
        super.sendPushMessage(data, from, message)
        Log.e(TAG, "From: $from")
        Log.e(TAG, "Message: $message")
        if (ActivityLifecycle.getInstance().isBackground) {
            showNotification(message ?: " ")
        }
    }

    private fun showNotification(message: String) {
        NotificationUtils.showNotification(this, SplashScreenActivity::class.java,
                getString(R.string.notification_title), message,
                R.mipmap.app_icon, NOTIFICATION_ID)
    }
}