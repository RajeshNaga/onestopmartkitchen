//package com.mart.onestopkitchen.chat
//
//import android.annotation.SuppressLint
//import android.app.Activity
//import android.content.Context
//import android.content.Intent
//import android.os.Bundle
//import android.util.Log
//import com.okdollar.paymentgateway.utils.Utils.hideProgressDialog
//import com.okdollar.paymentgateway.utils.Utils.showProgressDialog
//import com.quickblox.auth.session.QBSessionManager
//import com.quickblox.core.QBEntityCallback
//import com.quickblox.core.exception.QBResponseException
//import com.quickblox.users.QBUsers
//import com.quickblox.users.model.QBUser
//import com.mart.onestopkitchen.R
//import com.mart.onestopkitchen.chat.services.LoginService
//import com.mart.onestopkitchen.chat.ui.activity.DialogsActivity
//import com.mart.onestopkitchen.chat.ui.activity.ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS
//import com.mart.onestopkitchen.chat.ui.activity.OpponentsActivity
//import com.mart.onestopkitchen.chat.utils.*
//import com.mart.onestopkitchen.ui.activity.BaseActivity
//import com.mart.onestopkitchen.ui.fragment.Utility.activity
//
//@SuppressLint("StaticFieldLeak")
//public class LoginUserForChat {
//    private val UNAUTHORIZED = 401
//    lateinit var user: QBUser
//    var context: Context? = null
//    private val MIN_LENGTH = 3
//
//    companion object {
//        @Volatile
//        private var INSTANCE: LoginUserForChat? = null
//
//        fun getInstance(): LoginUserForChat {
//            return INSTANCE ?: synchronized(this) {
//                LoginUserForChat().also { INSTANCE = it }
//            }
//        }
//    }
//
//    fun videoChat(mContext: Context, user: QBUser) {
//        if (context == null)
//            this.context = mContext
//        if (SharedPrefsHelper.hasQbUser()) {
//            LoginService.start(activity!!, SharedPrefsHelper.getQbUser()!!)
//            OpponentsActivity.start(activity!!)
//        } else {
//            signUpNewUser(user)
//        }
//    }
//
//    /*private fun signUpNewUser(newUser: QBUser) {
//        showProgressDialog(activity, activity.getString(R.string.dlg_creating_new_user), false)
//        signUp(newUser, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(result: QBUser, params: Bundle) {
//                SharedPrefsHelper.saveQbUser(newUser)
//                loginToChat(result)
//                hideProgressDialog()
//                OpponentsActivity.start(activity)
//                Log.e("LoginUserForChat@@11", "#### signUpNewUser Successfully# ")
//            }
//
//            override fun onError(e: QBResponseException) {
//                if (e.httpStatusCode == ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
//                    signInCreatedUser(newUser)
//                    Log.e("LoginUserForChat@@22", "#### onError if # $ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS")
//                } else {
//                    Log.e("LoginUserForChat33", "#### onError else¬ # ")
//                    hideProgressDialog()
//                    longToast(R.string.sign_up_error)
//                }
//            }
//        })
//    }
//
//    private fun loginToChat(qbUser: QBUser) {
//        qbUser.password = USER_DEFAULT_PASSWORD
//        user = qbUser
//        startLoginService(qbUser)
//    }
//
//    private fun startLoginService(qbUser: QBUser) {
//        val tempIntent = Intent(activity, LoginService::class.java)
//        val pendingIntent = activity.createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0)
//        LoginService.start(activity, qbUser, pendingIntent)
//    }
//
//    fun signInCreatedUser(user: QBUser) {
//        signInUser(user, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(result: QBUser, params: Bundle) {
//                SharedPrefsHelper.saveQbUser(user)
//                updateUserOnServer(user)
//            }
//
//            override fun onError(responseException: QBResponseException) {
//                hideProgressDialog()
//                longToast(R.string.sign_in_error)
//            }
//        })
//    }
//
//    private fun updateUserOnServer(user: QBUser) {
//
//        Log.e("LoginUserForChat", "@@ UpdateUser@@@")
//        user.password = null
//        QBUsers.updateUser(user).performAsync(object : QBEntityCallback<QBUser> {
//            override fun onSuccess(updUser: QBUser?, params: Bundle?) {
//                Log.e("LoginUserForChat", "@@ UpdateUser@@@ onSuccess")
//                hideProgressDialog()
//                OpponentsActivity.start(activity)
////                finish()
//            }
//
//            override fun onError(responseException: QBResponseException?) {
//                hideProgressDialog()
//                Log.e("LoginUserForChat", "@@ UpdateUser@@@ onError : " + responseException.toString())
//                longToast(R.string.update_user_error)
//            }
//        })
//    }*/
//
//
//    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == EXTRA_LOGIN_RESULT_CODE) {
//            hideProgressDialog()
//
//            var isLoginSuccess = false
//            data?.let {
//                isLoginSuccess = it.getBooleanExtra(EXTRA_LOGIN_RESULT, false)
//            }
//
//            var errorMessage = getString(R.string.unknown_error)
//            data?.let {
//                errorMessage = it.getStringExtra(EXTRA_LOGIN_ERROR_MESSAGE)
//            }
//
//            if (isLoginSuccess) {
//                SharedPrefsHelper.saveQbUser(user)
//                signInCreatedUser(user)
//            } else {
//                longToast(getString(R.string.login_chat_login_error) + errorMessage)
////                userLoginEditText.setText(user.login)
////                userFullNameEditText.setText(user.fullName)
//            }
//        }
//    }*/
//
//
//    fun msgChat(mContext: Context, user: QBUser) {  //msg
//        if (context == null)
//            this.context = mContext
//        restoreChatSession(user)
//    }
//
//    private fun restoreChatSession(user: QBUser) { // msg
//        if (ChatHelper.isLogged()) {
//            DialogsActivity.start(context!!)
//        } else {
//            val currentUser = getUserFromSession()
//            if (currentUser == null) {
//                signIn(user)
////                LoginActivity.start(context)
//            } else {
//                loginToChat2(currentUser)
//            }
//        }
//    }
//
//    private fun signIn(user: QBUser) { //msg
//        showProgressDialog(context!!, getString(R.string.login), false)
//        ChatHelper.login(user, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(userFromRest: QBUser, bundle: Bundle?) {
//                if (userFromRest.fullName == user.fullName) {
//                    loginToChat(user)
//                } else {
//                    //Need to set password NULL, because server will update user only with NULL password
//                    user.password = null
//                    updateUser(user)
//                }
//            }
//
//            override fun onError(e: QBResponseException) {
//                if (e.httpStatusCode == UNAUTHORIZED) {
////                    signUp(user);
//                    signUpNewUser(user)
//                } else {
//                    hideProgressDialog()
//                    (activity as BaseActivity).showErrorSnackbar(R.string.login_chat_login_error, e) { signIn(user) }
//                }
//            }
//        })
//    }
//
//    private fun updateUser(user: QBUser) { //msg
//        ChatHelper.updateUser(user, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(qbUser: QBUser, bundle: Bundle?) {
//                loginToChat(user)
//            }
//
//            override fun onError(e: QBResponseException) {
//                hideProgressDialog()
//                (activity as BaseActivity).showErrorSnackbar(R.string.login_chat_login_error, e) { signIn(user) }
//            }
//        })
//    }
//
//    private fun signUpNewUser(newUser: QBUser) { //msg
//        showProgressDialog(activity, activity.getString(R.string.dlg_creating_new_user), false)
//        signUp(newUser, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(result: QBUser, params: Bundle) {
//                SharedPrefsHelper.saveQbUser(newUser)
//                loginToChat(result)
//            }
//
//            override fun onError(e: QBResponseException) {
//                if (e.httpStatusCode == ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
//                    signInCreatedUser(newUser)
//                } else {
//                    hideProgressDialog()
//                    longToast(R.string.sign_up_error)
//                }
//            }
//        })
//    }
//
//    private fun loginToChat(qbUser: QBUser) { //msg
//        qbUser.password = USER_DEFAULT_PASSWORD
//        user = qbUser
//        startLoginService(qbUser)
//    }
//
//    fun signInCreatedUser(user: QBUser) { //msg
//        signInUser(user, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(result: QBUser, params: Bundle) {
//                SharedPrefsHelper.saveQbUser(user)
//                updateUserOnServer(user)
//            }
//
//            override fun onError(responseException: QBResponseException) {
//                hideProgressDialog()
//                longToast(R.string.sign_in_error)
//            }
//        })
//    }
//
//    private fun updateUserOnServer(user: QBUser) { //msg
//        user.password = null
//        QBUsers.updateUser(user).performAsync(object : QBEntityCallback<QBUser> {
//            override fun onSuccess(updUser: QBUser?, params: Bundle?) {
//                hideProgressDialog()
//                OpponentsActivity.start(activity)
//            }
//
//            override fun onError(responseException: QBResponseException?) {
//                hideProgressDialog()
//                longToast(R.string.update_user_error)
//            }
//        })
//    }
//
//    private fun startLoginService(qbUser: QBUser) { //msg
//        val tempIntent = Intent(activity, LoginService::class.java)
//        val pendingIntent = (activity as Activity).createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0)
//        LoginService.start(activity!!, qbUser, pendingIntent)
//
//    }
//
//    private fun getUserFromSession(): QBUser? { //msg
//        val user = SharedPrefsHelper.getQbUser()
//        val qbSessionManager = QBSessionManager.getInstance()
//        qbSessionManager.sessionParameters?.let {
//            val userId = qbSessionManager.sessionParameters.userId
//            user?.id = userId
//            return user
//        } ?: run {
//            ChatHelper.destroy()
//            return null
//        }
//    }
//
//    private fun loginToChat2(user: QBUser) { //msg
//
//        showProgressDialog(context!!, getString(R.string.dlg_restoring_chat_session), false)
//
//        ChatHelper.loginToChat(user, object : QBEntityCallback<Void> {
//            override fun onSuccess(result: Void?, bundle: Bundle?) {
//                Log.v("--------", "Chat login onSuccess()")
//                hideProgressDialog()
//                DialogsActivity.start(context!!)
//            }
//
//
//            override fun onError(e: QBResponseException) {
//                hideProgressDialog()
//                Log.w("---------", "Chat login onError(): $e")
//
//                (context!! as BaseActivity).showErrorSnackbar(R.string.error_recreate_session, e)
//                {
//                    loginToChat(user)
//                }
//            }
//        })
//    }
//}
