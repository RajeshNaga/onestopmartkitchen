package com.mart.onestopkitchen.chat.ui.fragments


interface IncomeCallFragmentCallbackListener {

    fun onAcceptCurrentSession()

    fun onRejectCurrentSession()
}