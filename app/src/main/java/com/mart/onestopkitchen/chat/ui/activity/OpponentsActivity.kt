package com.mart.onestopkitchen.chat.ui.activity

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.quickblox.chat.QBChatService
import com.quickblox.core.QBEntityCallback
import com.quickblox.core.exception.QBResponseException
import com.quickblox.core.request.GenericQueryRule
import com.quickblox.core.request.QBPagedRequestBuilder
import com.quickblox.users.model.QBUser
import com.quickblox.videochat.webrtc.QBRTCClient
import com.quickblox.videochat.webrtc.QBRTCTypes
import com.mart.onestopkitchen.application.MyApplication
import com.mart.onestopkitchen.chat.db.QbUsersDbManager
import com.mart.onestopkitchen.chat.services.CallService
import com.mart.onestopkitchen.chat.services.LoginService
import com.mart.onestopkitchen.chat.ui.adapters.UsersAdapter
import com.mart.onestopkitchen.chat.utils.*
import com.mart.onestopkitchen.ui.activity.BaseActivity
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


private const val PER_PAGE_SIZE_100 = 100
private const val ORDER_RULE = "order"
private const val ORDER_DESC_UPDATED = "desc date updated_at"

class OpponentsActivity : BaseActivity() {
    private val TAG = OpponentsActivity::class.java.simpleName

    private lateinit var usersRecyclerView: RecyclerView
    private lateinit var currentUser: QBUser

    private var usersAdapter: UsersAdapter? = null

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, OpponentsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.mart.onestopkitchen.R.layout.activity_select_users)
        currentUser = SharedPrefsHelper.getInstance().qbUser!!
        initDefaultActionBar()
        initUI()
        startLoginService()
    }

    override fun onResume() {
        super.onResume()
        if (isCallServiceRunning(CallService::class.java)) {
            Log.d(TAG, "CallService is running now")
            CallActivity.start(this, false)
        }
        loadUsers()
    }

    private fun isCallServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val services = manager.getRunningServices(Integer.MAX_VALUE)
        for (service in services) {
            if (CallService::class.java.name == service.service.className) {
                return true
            }
        }
        return false
    }

    /*private fun startPermissionsActivity(checkOnlyAudio: Boolean) {
//        PermissionsActivity.startForResult(this, checkOnlyAudio, PERMISSIONS)
    }*/

    private fun loadUsers() {
        showProgressDialog(com.mart.onestopkitchen.R.string.dlg_loading_opponents)

        val rules = ArrayList<GenericQueryRule>()
        rules.add(GenericQueryRule(ORDER_RULE, ORDER_DESC_UPDATED))
        val requestBuilder = QBPagedRequestBuilder()
        requestBuilder.rules = rules
        requestBuilder.perPage = PER_PAGE_SIZE_100

        /*loadUsersByPagedRequestBuilder(object : QBEntityCallback<java.util.ArrayList<QBUser>> {
            override fun onSuccess(result: ArrayList<QBUser>, params: Bundle) {
                QbUsersDbManager.saveAllUsers(result, true)
                initUsersList()
                hideProgressDialog()
            }

            override fun onError(responseException: QBResponseException) {
                hideProgressDialog()
                showErrorSnackbar(R.string.loading_users_error, responseException)
            }
        }, requestBuilder)
       */

        val chatTagName = (application as MyApplication).PRODUCT_CATEGORY_NAME
        loadUsersByTag(chatTagName, object : QBEntityCallback<java.util.ArrayList<QBUser>> {
            @SuppressLint("SimpleDateFormat")
            override fun onSuccess(result: ArrayList<QBUser>, params: Bundle) {

//                Log.d(TAG, "#### saveAllUsers: $result")
//                Log.e(TAG, "#### saveAllUsers:" + result[0].lastRequestAt)
                /*val sdf = SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
                val givenDateString = result[0].lastRequestAt
                sdf.timeZone = TimeZone.getDefault()
                val dateString = sdf.format(givenDateString)

                try {
                    val mDate = sdf.parse(dateString)
                    Log.e(TAG, "#### mDate-->  $mDate")
                    val timeInMilliseconds = mDate.time
                    Log.e(TAG, "#### timeInMilliseconds-->$timeInMilliseconds")
                    println("Date in milli :: $timeInMilliseconds")
                } catch (e: ParseException) {
                    e.printStackTrace()
                }*/

                result.sortByDescending { it.customData }

                QbUsersDbManager.saveAllUsers(result, true)
                initUsersList()
                hideProgressDialog()
            }

            override fun onError(responseException: QBResponseException) {
                hideProgressDialog()
                showErrorSnackbar(com.mart.onestopkitchen.R.string.loading_users_error, responseException)
            }
        })
    }

    fun uTCToLocal(dateFormatInPut: String, dateFomratOutPut: String, datesToConvert: String): String {

        var dateToReturn = datesToConvert
        val sdf = SimpleDateFormat(dateFormatInPut)
        sdf.timeZone = TimeZone.getTimeZone("UTC")

        var gmt: Date? = null

        val sdfOutPutToSend = SimpleDateFormat(dateFomratOutPut)
        sdfOutPutToSend.timeZone = TimeZone.getDefault()

        try {

            gmt = sdf.parse(datesToConvert)
            dateToReturn = sdfOutPutToSend.format(gmt)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return dateToReturn
    }


    private fun initUI() {
        usersRecyclerView = findViewById(com.mart.onestopkitchen.R.id.list_select_users)
    }

    private fun initUsersList() {
        val currentOpponentsList = QbUsersDbManager.allUsers
        currentOpponentsList.remove(SharedPrefsHelper.getInstance().qbUser)
        if (usersAdapter == null) {
            usersAdapter = UsersAdapter(this, currentOpponentsList)
            usersAdapter!!.setSelectedItemsCountsChangedListener(object : UsersAdapter.SelectedItemsCountsChangedListener {
                override fun onCountSelectedItemsChanged(count: Int) {
                    updateActionBar(count)
                }
            })
            usersRecyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
            usersRecyclerView.adapter = this.usersAdapter
        } else {
            usersAdapter!!.updateUsersList(currentOpponentsList)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(com.mart.onestopkitchen.R.menu.menu_main, menu)
        menu.findItem(com.mart.onestopkitchen.R.id.menu_cart).isVisible = false
        menu.findItem(com.mart.onestopkitchen.R.id.menu_search).isVisible = false

        if (usersAdapter != null && usersAdapter!!.selectedUsers.isNotEmpty()) {
            menuInflater.inflate(com.mart.onestopkitchen.R.menu.activity_selected_opponents, menu)
        } else {
            menuInflater.inflate(com.mart.onestopkitchen.R.menu.activity_opponents, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            com.mart.onestopkitchen.R.id.update_opponents_list -> {
                loadUsers()
                return true
            }
            com.mart.onestopkitchen.R.id.settings -> {
                SettingsActivity.start(this)
                return true
            }
            com.mart.onestopkitchen.R.id.log_out -> {
//                logout()
                return true
            }
            com.mart.onestopkitchen.R.id.start_video_call -> {
//                PermissionUtils.instance.requestPermission(ff)
                if (checkIsLoggedInChat()) {
                    startCall(true)
                }
//                if (checkPermissions(PERMISSIONS)) {
//                    startPermissionsActivity(false)
//                }
                return true
            }
            com.mart.onestopkitchen.R.id.start_audio_call -> {
                if (checkIsLoggedInChat()) {
                    startCall(false)
                }
//                if (checkPermission(PERMISSIONS[1])) {
//                    startPermissionsActivity(true)
//                }
                return true
            }
            com.mart.onestopkitchen.R.id.appinfo -> {
//                AppInfoActivity.start(this)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun checkIsLoggedInChat(): Boolean {
        if (!QBChatService.getInstance().isLoggedIn) {
            startLoginService()
            shortToast(com.mart.onestopkitchen.R.string.login_chat_retry)
            return false
        }
        return true
    }

    private fun startLoginService() {
        if (SharedPrefsHelper.getInstance().hasQbUser()) {
            LoginService.start(this, SharedPrefsHelper.getInstance().qbUser!!)
        }
    }

    private fun startCall(isVideoCall: Boolean) {
        val usersCount = usersAdapter!!.selectedUsers.size
        if (usersCount > MAX_OPPONENTS_COUNT) {
            longToast(String.format(getString(com.mart.onestopkitchen.R.string.error_max_opponents_count), MAX_OPPONENTS_COUNT))
            return
        }

        val opponentsList = getIdsSelectedOpponents(usersAdapter!!.selectedUsers)
        val conferenceType = if (isVideoCall) {
            QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
        } else {
            QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO
        }
        val qbrtcClient = QBRTCClient.getInstance(applicationContext)

        val newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType)

        WebRtcSessionManager.getInstance(this@OpponentsActivity).setCurrentSession(newQbRtcSession)

        sendPushMessage(opponentsList, currentUser.fullName)

        CallActivity.start(this, false)
    }

    private fun getIdsSelectedOpponents(selectedUsers: Collection<QBUser>): ArrayList<Int> {
        val opponentsIds = ArrayList<Int>()
        if (!selectedUsers.isEmpty()) {
            for (qbUser in selectedUsers) {
                opponentsIds.add(qbUser.id)
            }
        }
        return opponentsIds
    }

    private fun updateActionBar(countSelectedUsers: Int) {
        if (countSelectedUsers < 1) {
            initDefaultActionBar()
        } else {
            val title = if (countSelectedUsers > 1) {
                com.mart.onestopkitchen.R.string.tile_many_users_selected
            } else {
                com.mart.onestopkitchen.R.string.title_one_user_selected
            }
            supportActionBar?.title = getString(title, countSelectedUsers)
            supportActionBar?.subtitle = null
        }
        invalidateOptionsMenu()
    }

    private fun initDefaultActionBar() {
        showHideCart(false)
        val toolbar = findViewById<Toolbar>(com.mart.onestopkitchen.R.id.app_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            val currentUserFullName = SharedPrefsHelper.getInstance().qbUser!!.fullName
            supportActionBar?.title = currentUserFullName
//            supportActionBar?.subtitle = getString(R.string.subtitle_text_logged_in_as, currentUserFullName)
            toolbar.setNavigationIcon(com.mart.onestopkitchen.R.drawable.ic_action_nav_arrow_back)
            toolbar.setNavigationOnClickListener { finish() }
        }
    }


//    private fun logout() {
//        SubscribeService.unSubscribeFromPushes(this)
//        LoginService.logout(this)
//        removeAllUserData()
//        startLoginActivity()
//    }
//
//    private fun removeAllUserData() {
//        SharedPrefsHelper.clearAllData()
//        QbUsersDbManager.clearDB()
//        signOut()
//    }
//
//    private fun startLoginActivity() {
//        LoginActivity.start(this)
//        finish()
//    }
}