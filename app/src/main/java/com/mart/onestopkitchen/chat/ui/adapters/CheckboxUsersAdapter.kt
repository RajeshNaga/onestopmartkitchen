package com.mart.onestopkitchen.chat.ui.adapters

import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.quickblox.users.model.QBUser
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper
import com.mart.onestopkitchen.chat.utils.getColorCircleGrayDrawable
import com.mart.onestopkitchen.utils.showToast


class CheckboxUsersAdapter(context: Context, users: List<QBUser>) : MsgUsersAdapter(context, users as MutableList<QBUser>) {

    private val initiallySelectedUsers: MutableList<Int> = ArrayList()
    private val _selectedUsers: MutableSet<QBUser> = HashSet()
    val selectedUsers: ArrayList<*>
        get() = ArrayList<QBUser>(_selectedUsers)

    fun addSelectedUsers(userIdList: List<Int>) {
        for (user in userList) {
            for (userId in userIdList) {
                if (user.id == userId) {
                    _selectedUsers.add(user)
                    initiallySelectedUsers.add(user.id)
                    break
                }
            }
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)

        val user = getItem(position)
        val holder = view.tag as ViewHolder

        view.setOnClickListener(View.OnClickListener {
            if (!isAvailableForSelection(user)) {
                return@OnClickListener
            }

            holder.userCheckBox.isChecked = !holder.userCheckBox.isChecked
            if (holder.userCheckBox.isChecked) {
                if (_selectedUsers.size < 1) {
                    _selectedUsers.add(user)
                    SharedPrefsHelper.getInstance().save(SharedPrefsHelper.QB_OPPONENTS_NAME, user.fullName)

                } else {
                    showToast(context.getString(R.string.one_one_chat_only), context)
                    holder.userCheckBox.isChecked = false
                }
            } else {
                _selectedUsers.remove(user)
            }
        })

        holder.userCheckBox.visibility = View.VISIBLE
        val containsUser = _selectedUsers.contains(user)
        holder.userCheckBox.isChecked = containsUser

        if (isUserMe(user)) {
            holder.userCheckBox.isChecked = true
        }

        val userAvailableStatus = user.customData
        Log.e("MsgAdaper-->", ": #### CustomData : $userAvailableStatus")

        if (null != userAvailableStatus) {
            if (userAvailableStatus.isEmpty() || userAvailableStatus == "1") {
                holder.status.setImageResource(R.drawable.ic_offline)
                holder.tvOpponentsStatus.text = context.getString(R.string.offline)
            } else if (userAvailableStatus == "3") {
                holder.status.setImageResource(R.drawable.ic_online)
                holder.tvOpponentsStatus.text = context.getString(R.string.online)
            } else if (userAvailableStatus == "2") {
                holder.status.setImageResource(R.drawable.ic_busy)
                holder.tvOpponentsStatus.text = context.getString(R.string.busy_call)
            } else {
                holder.status.setImageResource(R.drawable.ic_offline)
                holder.tvOpponentsStatus.text = context.getString(R.string.offline)
            }
        }

//        holder.opponentIcon.setBackgroundDrawable(getColorCircleDrawable(user.id))

        holder.opponentIcon.setBackgroundDrawable(getColorCircleGrayDrawable(user.id))
        holder.opponentIcon.setImageResource(R.drawable.ic_person)


        return view
    }

    override fun getItem(position: Int): QBUser {
        return userList[position]
    }

    override fun isAvailableForSelection(user: QBUser): Boolean {
        return super.isAvailableForSelection(user) && !initiallySelectedUsers.contains(user.id)
    }
}