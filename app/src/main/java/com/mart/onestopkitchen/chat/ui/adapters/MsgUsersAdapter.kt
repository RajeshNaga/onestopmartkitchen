package com.mart.onestopkitchen.chat.ui.adapters

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.quickblox.chat.QBChatService
import com.quickblox.users.model.QBUser
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.chat.utils.getColorCircleDrawable
import com.mart.onestopkitchen.utils.CustomTextView
import de.hdodenhof.circleimageview.CircleImageView


open class MsgUsersAdapter(val context: Context, val userList: MutableList<QBUser>) : BaseAdapter() {
    private var currentUser: QBUser? = QBChatService.getInstance().user

    init {
        currentUser?.let {
            if (!userList.contains(it)) {
                (userList as ArrayList).add(it)
            }
        }
    }

    fun addUserToUserList(user: QBUser) {
        if (!userList.contains(user)) {
            userList.add(user)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: ViewHolder
        var modifiedView = convertView
        val user = getItem(position)

        if (modifiedView == null) {
            modifiedView = LayoutInflater.from(context).inflate(R.layout.list_item_user, parent, false) as View
            holder = ViewHolder()
            holder.opponentIcon = modifiedView.findViewById(R.id.image_opponent_icon)
            holder.loginTextView = modifiedView.findViewById(R.id.text_user_login)
            holder.userCheckBox = modifiedView.findViewById(R.id.checkbox_user)
            holder.linearLayoutRoot = modifiedView.findViewById(R.id.linear_layout_root)
            holder.tvOpponentsStatus = modifiedView.findViewById(R.id.tvOpponentsStatus)
            holder.status = modifiedView.findViewById(R.id.status)

            modifiedView.tag = holder
        } else {
            holder = modifiedView.tag as ViewHolder
        }

        val username = if (TextUtils.isEmpty(user.fullName)) {
            user.login
        } else {
            user.fullName
        }

        if (isUserMe(user)) {
            holder.loginTextView.text = context.getString(R.string.placeholder_username_you, username)
            holder.linearLayoutRoot.visibility = View.GONE
        } else {
            holder.linearLayoutRoot.visibility = View.VISIBLE
            holder.loginTextView.text = username
        }

        if (isAvailableForSelection(user)) {
            holder.loginTextView.setTextColor(context.resources.getColor(R.color.text_color_black))
        } else {
            holder.loginTextView.setTextColor(context.resources.getColor(R.color.text_color_medium_grey))
        }

        holder.opponentIcon.setBackgroundDrawable(getColorCircleDrawable(position))
        holder.userCheckBox.visibility = View.GONE


//        val userAvailableStatus = user.customData
//        Log.e("MsgAdaper-->", ": #### CustomData : $userAvailableStatus")
//
//        if (null != userAvailableStatus) {
//            if (userAvailableStatus.isEmpty() || userAvailableStatus == "1") {
//                holder.status.setImageResource(R.drawable.ic_offline)
//                holder.tvOpponentsStatus.text = context.getString(R.string.offline)
//            } else if (userAvailableStatus == "3") {
//                holder.status.setImageResource(R.drawable.ic_online)
//                holder.tvOpponentsStatus.text = context.getString(R.string.online)
//            } else if (userAvailableStatus == "2") {
//                holder.status.setImageResource(R.drawable.ic_busy)
//                holder.tvOpponentsStatus.text = context.getString(R.string.busy_call)
//            } else {
//                holder.status.setImageResource(R.drawable.ic_offline)
//                holder.tvOpponentsStatus.text = context.getString(R.string.offline)
//            }
//        }



        return modifiedView
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return userList.size
    }

    override fun getItem(position: Int): QBUser {
        return userList[position]
    }

    protected fun isUserMe(user: QBUser): Boolean {
        return currentUser?.id == user.id
    }

    protected open fun isAvailableForSelection(user: QBUser): Boolean {
        return currentUser?.id != user.id
    }

    protected class ViewHolder {
        lateinit var opponentIcon: ImageView
        lateinit var loginTextView: TextView
        lateinit var userCheckBox: CheckBox
        lateinit var linearLayoutRoot: LinearLayout
        lateinit var status: CircleImageView
        lateinit var tvOpponentsStatus: CustomTextView
    }
}