package com.mart.onestopkitchen.chat.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.quickblox.users.model.QBUser
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.chat.utils.getColor
import com.mart.onestopkitchen.chat.utils.getColorCircleGrayDrawable
import com.mart.onestopkitchen.chat.utils.getColoredCircleDrawable
import com.mart.onestopkitchen.chat.utils.shortToast
import kotlinx.android.synthetic.main.item_opponents_list.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class UsersAdapter(val context: Context, private var usersList: List<QBUser>) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    private val _selectedUsers: MutableList<QBUser> = ArrayList()
    private var isOnline: Boolean = false
    val selectedUsers: List<QBUser>
        get() = _selectedUsers
    private lateinit var selectedItemsCountsChangedListener: SelectedItemsCountsChangedListener

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = usersList[position]
        Log.e("#### STATUS: ,", "user :$user")
        holder.opponentName.text = user.fullName
        if (_selectedUsers.contains(user)) {
            holder.rootLayout.setBackgroundResource(R.color.background_color_selected_user_item)
            holder.opponentIcon.setBackgroundDrawable(
                    getColoredCircleDrawable(getColor(R.color.icon_background_color_selected_user)))
            holder.opponentIcon.setImageResource(R.drawable.ic_checkmark)
        } else {
//            holder.rootLayout.setBackgroundResource(R.color.background_color_normal_user_item)
//            holder.opponentIcon.setBackgroundDrawable(getColorCircleDrawable(user.id))

            holder.rootLayout.setBackgroundResource(R.color.background_color_normal_user_item)
            holder.opponentIcon.setBackgroundDrawable(getColorCircleGrayDrawable(user.id))
            holder.opponentIcon.setImageResource(R.drawable.ic_person)
        }
        holder.status.visibility = View.VISIBLE

        val userAvailableStatus = user.customData
        Log.e("#### CustomData : ,", ": $userAvailableStatus")

        if (null != userAvailableStatus) {
            if (userAvailableStatus.isEmpty() || userAvailableStatus.equals("1")) {
                holder.status.setImageResource(R.drawable.ic_offline)
                holder.opponentStatus.text = context.getString(R.string.offline)
            } else if (userAvailableStatus.equals("3")) {
                holder.status.setImageResource(R.drawable.ic_online)
                holder.opponentStatus.text = context.getString(R.string.online)
            } else if (userAvailableStatus.equals("2")) {
                holder.status.setImageResource(R.drawable.ic_busy)
                holder.opponentStatus.text = context.getString(R.string.busy_call)
            } else {
                holder.status.setImageResource(R.drawable.ic_offline)
                holder.opponentStatus.text = context.getString(R.string.offline)
            }

        }


        // if user didn't do anything last 5 minutes (5*60*1000 milliseconds)
        /*
         val currentTime = System.currentTimeMillis()
        val sdf = SimpleDateFormat("MMM dd, hh:mm a")
        Log.e("#### CustomTime : ,", ": " + getDate(userLastRequestAtTime.toLong(), "MMM dd, hh:mm a"))
        val longTime = userLastRequestAtTime.toLong()
        if (currentTime - longTime > 5 * 60 * 1000) {
            // user is offline now
            isOnline = false
            holder.status.setImageResource(com.mart.onestopkitchen.R.drawable.ic_gray)
        } else {
            //online
            holder.status.setImageResource(com.mart.onestopkitchen.R.drawable.ic_online)
            isOnline = true
        }*/


        holder.rootLayout.setOnClickListener { v ->
            toggleSelection(user)
            selectedItemsCountsChangedListener.onCountSelectedItemsChanged(_selectedUsers.size)
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getDate(milliSeconds: Long, dateFormat: String): String {
        val formatter = SimpleDateFormat(dateFormat)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }


    override fun getItemCount(): Int {
        return usersList.size
    }

    fun updateUsersList(usersList: List<QBUser>) {
        this.usersList = usersList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_opponents_list, parent, false))
    }

    private fun toggleSelection(qbUser: QBUser) {
        if (_selectedUsers.contains(qbUser)) {
            _selectedUsers.remove(qbUser)
        } else {
            if (_selectedUsers.size == 0) {
                _selectedUsers.add(qbUser)
            } else {
                shortToast(context.getString(R.string.one_one_chat_only))
            }
        }
        notifyDataSetChanged()
    }

    fun setSelectedItemsCountsChangedListener(selectedItemsCountsChangedListener: SelectedItemsCountsChangedListener) {
        this.selectedItemsCountsChangedListener = selectedItemsCountsChangedListener
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val opponentIcon = view.image_opponent_icon!!
        val opponentName = view.opponents_name!!
        val rootLayout = view.root_layout!!
        val status = view.status!!
        val opponentStatus = view.opponents_status!!
    }

    interface SelectedItemsCountsChangedListener {
        fun onCountSelectedItemsChanged(count: Int)
    }
}