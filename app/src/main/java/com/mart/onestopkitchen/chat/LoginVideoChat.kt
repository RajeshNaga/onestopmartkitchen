package com.mart.onestopkitchen.chat

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.okdollar.paymentgateway.utils.Utils.hideProgressDialog
import com.okdollar.paymentgateway.utils.Utils.showProgressDialog
import com.quickblox.core.QBEntityCallback
import com.quickblox.core.exception.QBResponseException
import com.quickblox.users.QBUsers
import com.quickblox.users.model.QBUser
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.chat.services.LoginService
import com.mart.onestopkitchen.chat.ui.activity.OpponentsActivity
import com.mart.onestopkitchen.chat.utils.*
import com.mart.onestopkitchen.ui.fragment.Utility.activity

class LoginVideoChat {
    private val UNAUTHORIZED = 401
    val ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS = 422
    lateinit var user: QBUser
    var context: Context? = null

    companion object {
        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: LoginVideoChat? = null

        fun getInstance(): LoginVideoChat {
            return INSTANCE ?: synchronized(this) {
                LoginVideoChat().also { INSTANCE = it }
            }
        }
    }

    fun videoChat(mContext: Context, user: QBUser) {
        if (context == null)
            this.context = mContext
        if (SharedPrefsHelper.getInstance().hasQbUser()) {
            LoginService.start(activity!!, SharedPrefsHelper.getInstance().qbUser!!)
            OpponentsActivity.start(activity!!)
        } else {
            signUpNewUser(user)
        }
    }

    private fun signUpNewUser(newUser: QBUser) {
        showProgressDialog(activity, activity.getString(R.string.dlg_creating_new_user), false)
        signUp(newUser, object : QBEntityCallback<QBUser> {
            override fun onSuccess(result: QBUser, params: Bundle) {
                SharedPrefsHelper.getInstance().saveQbUser(newUser)
                loginToChat(result)
                hideProgressDialog()
                signInCreatedUser(user)
                OpponentsActivity.start(activity)
                Log.e("LoginVideoChat", "#### signUpNewUser Successfully# ")
            }

            override fun onError(e: QBResponseException) {
                if (e.httpStatusCode == ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                    signInCreatedUser(newUser)
                    Log.e("LoginVideoChat", "#### onError if # $ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS")
                } else {
                    Log.e("LoginVideoChat", "#### onError else¬ # ")
                    hideProgressDialog()
                    longToast(R.string.sign_up_error)
                    SharedPrefsHelper.getInstance().clearAllData()
                }
            }
        })
    }

    private fun loginToChat(qbUser: QBUser) {
        qbUser.password = USER_DEFAULT_PASSWORD
        user = qbUser
        startLoginService(qbUser)
    }

    private fun startLoginService(qbUser: QBUser) {
        val tempIntent = Intent(activity, LoginService::class.java)
        val pendingIntent = activity.createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0)
        LoginService.start(activity, qbUser, pendingIntent)
    }

    fun signInCreatedUser(user: QBUser) {
        signInUser(user, object : QBEntityCallback<QBUser> {
            override fun onSuccess(result: QBUser, params: Bundle) {
                SharedPrefsHelper.getInstance().saveQbUser(user)
                updateUserOnServer(user)
            }

            override fun onError(responseException: QBResponseException) {
                hideProgressDialog()
                longToast(R.string.sign_in_error)
            }
        })
    }

    private fun updateUserOnServer(user: QBUser) {
        user.password = null
        QBUsers.updateUser(user).performAsync(object : QBEntityCallback<QBUser> {
            override fun onSuccess(updUser: QBUser?, params: Bundle?) {
//                Log.e("LoginVideoChat", "@@ UpdateUser@@@ onSuccess :---- " + user.toString())
                SharedPrefsHelper.getInstance().saveQbUser(user)
                hideProgressDialog()
                OpponentsActivity.start(activity)
            }

            override fun onError(responseException: QBResponseException?) {
                hideProgressDialog()
//                Log.e("LoginVideoChat", "@@ UpdateUser@@@ onError : " + responseException.toString())
                longToast(R.string.update_user_error)
            }
        })
    }

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == EXTRA_LOGIN_RESULT_CODE) {
            hideProgressDialog()

            var isLoginSuccess = false
            data?.let {
                isLoginSuccess = it.getBooleanExtra(EXTRA_LOGIN_RESULT, false)
            }

            var errorMessage = getString(R.string.unknown_error)
            data?.let {
                errorMessage = it.getStringExtra(EXTRA_LOGIN_ERROR_MESSAGE)
            }

            if (isLoginSuccess) {
                SharedPrefsHelper.saveQbUser(user)
                signInCreatedUser(user)
            } else {
                longToast(getString(R.string.login_chat_login_error) + errorMessage)
//                userLoginEditText.setText(user.login)
//                userFullNameEditText.setText(user.fullName)
            }
        }
    }*/
}