package com.mart.onestopkitchen.chat.utils

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.mart.onestopkitchen.application.MyApplication

fun showKeyboard(editText: EditText) {
    val imm = MyApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
}

fun hideKeyboard(editText: EditText) {
    val imm = MyApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(editText.windowToken, 0)
}