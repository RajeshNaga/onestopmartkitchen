package com.mart.onestopkitchen.chat.utils

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.application.MyApplication

private const val EMPTY_STRING = ""
private val NO_CONNECTION_ERROR = MyApplication.getAppContext().getString(R.string.error_connection)
private val NO_RESPONSE_TIMEOUT = MyApplication.getAppContext().getString(R.string.error_response_timeout)

private val NO_INTERNET_CONNECTION = MyApplication.getAppContext().getString(R.string.no_internet_connection)

private val NO_SERVER_CONNECTION =MyApplication.getAppContext().getString(R.string.no_server_connection)

fun showErrorSnackbar(view: View, @StringRes errorMessage: Int, e: Exception?,
                      @StringRes actionLabel: Int, clickListener: View.OnClickListener): Snackbar {
    val error = if (e == null) {
        EMPTY_STRING
    } else {
        e.message as String
    }
    val noConnection = NO_CONNECTION_ERROR == error
    val timeout = error.startsWith(NO_RESPONSE_TIMEOUT)
    val snackbar: Snackbar
    if (noConnection || timeout) {
        snackbar = showErrorSnackbar(view, NO_INTERNET_CONNECTION, actionLabel, clickListener)
    } else if (errorMessage == 0) {
        snackbar = showErrorSnackbar(view, error, actionLabel, clickListener)
    } else if (error == EMPTY_STRING) {
        snackbar = showErrorSnackbar(view, errorMessage, NO_INTERNET_CONNECTION, actionLabel, clickListener)
    } else {
        snackbar = showErrorSnackbar(view, errorMessage, error, actionLabel, clickListener)
    }
    return snackbar
}

fun showErrorSnackbar(view: View, @StringRes errorMessage: Int, error: String,
                      @StringRes actionLabel: Int, clickListener: View.OnClickListener): Snackbar {
    val errorMessageString = MyApplication.getAppContext().getString(errorMessage)
    val message = String.format("%s: %s", errorMessageString, error)
    return showErrorSnackbar(view, message, actionLabel, clickListener)
}

fun showErrorSnackbar(view: View, message: String, @StringRes actionLabel: Int,
                      clickListener: View.OnClickListener?): Snackbar {
    val snackbar = Snackbar.make(view, message.trim { it <= ' ' }, Snackbar.LENGTH_INDEFINITE)
    if (clickListener != null) {
        snackbar.setAction(actionLabel, clickListener)
    }
    snackbar.show()
    return snackbar
}

fun showSnackbar(view: View, @StringRes errorMessageResource: Int, e: Exception?,
                 @StringRes actionLabel: Int, clickListener: View.OnClickListener?): Snackbar {
    val error = if (e == null) "" else e.message
    val noConnection = NO_CONNECTION_ERROR == error
    val timeout = error!!.startsWith(NO_RESPONSE_TIMEOUT)
    return if (noConnection || timeout) {
        showSnackbar(view, NO_SERVER_CONNECTION, actionLabel, clickListener)
    } else if (errorMessageResource == 0) {
        showSnackbar(view, error, actionLabel, clickListener)
    } else if (errorMessageResource != 0) {
        showSnackbar(view,  MyApplication.getAppContext().getString(errorMessageResource), actionLabel, clickListener)
    } else if (error == "") {
        showSnackbar(view, errorMessageResource, NO_SERVER_CONNECTION, actionLabel, clickListener)
    } else {
        showSnackbar(view, errorMessageResource, error, actionLabel, clickListener)
    }
}

private fun showSnackbar(view: View, @StringRes errorMessage: Int, error: String,
                         @StringRes actionLabel: Int, clickListener: View.OnClickListener?): Snackbar {
    val errorMessageString = MyApplication.getAppContext().getString(errorMessage)
    val message = String.format("%s: %s", errorMessageString, error)
    return showSnackbar(view, message, actionLabel, clickListener)
}

private fun showSnackbar(view: View, message: String,
                         @StringRes actionLabel: Int,
                         clickListener: View.OnClickListener?): Snackbar {
    val snackbar = Snackbar.make(view, message.trim { it <= ' ' }, Snackbar.LENGTH_INDEFINITE)
    if (clickListener != null) {
        snackbar.setAction(actionLabel, clickListener)
    }
    snackbar.duration = 2000
    snackbar.show()
    return snackbar
}