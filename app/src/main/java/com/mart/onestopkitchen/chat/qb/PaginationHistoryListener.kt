package com.mart.onestopkitchen.chat.qb


interface PaginationHistoryListener {
    fun downloadMore()
}