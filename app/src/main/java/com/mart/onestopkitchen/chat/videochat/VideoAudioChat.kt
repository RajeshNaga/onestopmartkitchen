//package com.mart.onestopkitchen.chat.videochat
//
//import android.app.Activity
//import android.content.Context
//import android.content.Intent
//import android.os.Bundle
//import com.okdollar.paymentgateway.utils.Utils.hideProgressDialog
//import com.okdollar.paymentgateway.utils.Utils.showProgressDialog
//import com.quickblox.core.QBEntityCallback
//import com.quickblox.core.exception.QBResponseException
//import com.quickblox.users.QBUsers
//import com.quickblox.users.model.QBUser
//import com.mart.onestopkitchen.R
//import com.mart.onestopkitchen.chat.services.LoginService
//import com.mart.onestopkitchen.chat.ui.activity.ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS
//import com.mart.onestopkitchen.chat.ui.activity.OpponentsActivity
//import com.mart.onestopkitchen.chat.utils.*
//
//class VideoAudioChat(val mContext: Context) {
//
//    private lateinit var user: QBUser
//
//    fun signUpNewUser(newUser: QBUser) {
//        showProgressDialog(mContext, getString(R.string.dlg_creating_new_user), false)
//        signUp(newUser, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(result: QBUser, params: Bundle) {
//                SharedPrefsHelper.saveQbUser(newUser)
//                loginToChat(result)
//            }
//
//            override fun onError(e: QBResponseException) {
//                if (e.httpStatusCode == ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
//                    signInCreatedUser(newUser)
//                } else {
//                    hideProgressDialog()
//                    longToast(R.string.sign_up_error)
//                }
//            }
//        })
//    }
//
//    private fun loginToChat(qbUser: QBUser) {
//        qbUser.password = "123456"
//        user = qbUser
//        startLoginService(qbUser)
//    }
//
//    private fun startLoginService(qbUser: QBUser) {
//        val tempIntent = Intent(mContext, LoginService::class.java)
//        val pendingIntent = (mContext as Activity).createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0)
//        LoginService.start(mContext, qbUser, pendingIntent)
//    }
//
//    private fun signInCreatedUser(user: QBUser) {
//        signInUser(user, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(result: QBUser, params: Bundle) {
//                SharedPrefsHelper.saveQbUser(user)
//                updateUserOnServer(user)
//            }
//
//            override fun onError(responseException: QBResponseException) {
//                hideProgressDialog()
//                longToast(R.string.sign_in_error)
//            }
//        })
//    }
//
//    private fun updateUserOnServer(user: QBUser) {
//        user.password = null
//        QBUsers.updateUser(user).performAsync(object : QBEntityCallback<QBUser> {
//            override fun onSuccess(updUser: QBUser?, params: Bundle?) {
//                hideProgressDialog()
//                OpponentsActivity.start(mContext)
//            }
//
//            override fun onError(responseException: QBResponseException?) {
//                hideProgressDialog()
//                longToast(R.string.update_user_error)
//            }
//        })
//    }
//
//    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == EXTRA_LOGIN_RESULT_CODE) {
//            hideProgressDialog()
//
//            var isLoginSuccess = false
//            data?.let {
//                isLoginSuccess = it.getBooleanExtra(EXTRA_LOGIN_RESULT, false)
//            }
//
//            var errorMessage = getString(R.string.unknown_error)
//            data?.let {
//                errorMessage = it.getStringExtra(EXTRA_LOGIN_ERROR_MESSAGE)
//            }
//
//            if (isLoginSuccess) {
//                SharedPrefsHelper.saveQbUser(user)
//                signInCreatedUser(user)
//            } else {
//                longToast(getString(R.string.login_chat_login_error) + errorMessage)
//                userLoginEditText.setText(user.login)
//                userFullNameEditText.setText(user.fullName)
//            }
//        }
//    }*/
//
//
//}