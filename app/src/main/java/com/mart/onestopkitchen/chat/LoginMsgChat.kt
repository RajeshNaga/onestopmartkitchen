//package com.mart.onestopkitchen.chat
//
//import android.annotation.SuppressLint
//import android.content.Context
//import android.os.Bundle
//import android.util.Log
//import android.view.View
//import com.okdollar.paymentgateway.utils.Utils.hideProgressDialog
//import com.okdollar.paymentgateway.utils.Utils.showProgressDialog
//import com.quickblox.auth.session.QBSessionManager
//import com.quickblox.core.QBEntityCallback
//import com.quickblox.core.exception.QBResponseException
//import com.quickblox.users.QBUsers
//import com.quickblox.users.model.QBUser
//import com.mart.onestopkitchen.R
//import com.mart.onestopkitchen.chat.ui.activity.DialogsActivity
//import com.mart.onestopkitchen.chat.utils.ChatHelper
//import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper
//import com.mart.onestopkitchen.chat.utils.USER_DEFAULT_PASSWORD
//import com.mart.onestopkitchen.ui.activity.BaseActivity
//import com.mart.onestopkitchen.ui.fragment.Utility.activity
//
//class LoginMsgChat {
//    private val UNAUTHORIZED = 401
//    lateinit var user: QBUser
//    var context: Context? = null
//    private val MIN_LENGTH = 3
//
//    private val TAG = LoginMsgChat::class.java.simpleName
//
//    companion object {
//        @SuppressLint("StaticFieldLeak")
//        @Volatile
//        private var INSTANCE: LoginMsgChat? = null
//
//        fun getInstance(): LoginMsgChat {
//            return INSTANCE ?: synchronized(this) {
//                LoginMsgChat().also { INSTANCE = it }
//            }
//        }
//    }
//
//    fun msgChat(mContext: Context, user: QBUser) {
//        if (context == null)
//            this.context = mContext
//
//        if (SharedPrefsHelper.getInstance().hasQbUser()) {
//            restoreChatSession(user)
//        } else {
//            signIn(user)
//        }
//    }
//
//    private fun restoreChatSession(user: QBUser) {
//        if (ChatHelper.getInstance().isLogged()) {
//            DialogsActivity.start(context!!)
//        } else {
//            val currentUser = getUserFromSession()
//            if (currentUser == null) {
//                signIn(user)
//            } else {
//                loginToChat(currentUser)
//            }
//        }
//    }
//
//    private fun getUserFromSession(): QBUser? {
//        val user = SharedPrefsHelper.getInstance().qbUser
//        val qbSessionManager = QBSessionManager.getInstance()
//        qbSessionManager.sessionParameters?.let {
//            val userId = qbSessionManager.sessionParameters.userId
//            user?.id = userId
//            return user
//        } ?: run {
//            ChatHelper.getInstance().destroy()
//            return null
//        }
//    }
//
//    /* private fun loginToChat(user: QBUser) {
//         showProgressDialog(context, context!!.getString(R.string.dlg_restoring_chat_session), false)
//         ChatHelper.loginToChat(user, object : QBEntityCallback<Void> {
//             override fun onSuccess(result: Void?, bundle: Bundle?) {
//                 Log.e("LoginChaMsg::", "Chat login onSuccess()")
//                 hideProgressDialog()
//                 DialogsActivity.start(context!!)
//                //   finish()
//             }
//
//             override fun onError(e: QBResponseException) {
//                 hideProgressDialog()
//                 Log.e("LoginChaMsg::", "Chat login onError(): $e")
//                 (activity as BaseActivity).showErrorSnackbar(R.string.error_recreate_session, e, View.OnClickListener { loginToChat(user) })
//             }
//         })
//     }*/
//
//
//    //LoginActiviy
//    private fun signIn(user: QBUser) {
//        showProgressDialog(context, context!!.getString(R.string.dlg_login), false)
//        ChatHelper.getInstance().login(user, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(userFromRest: QBUser, bundle: Bundle?) {
//                if (userFromRest.fullName.equals(user.fullName)) {
//                    loginToChat(user)
//                    Log.e(TAG, "##########  signIn(): onSuccess(): ##" + user.fullName)
//                } else {
//                    //Need to set password NULL, because server will update user only with NULL password
//                    user.password = null
//                    updateUser(user)
//                }
//            }
//
//            override fun onError(e: QBResponseException) {
//                if (e.httpStatusCode == UNAUTHORIZED) {
//                    signUp(user)
//                } else {
//                    hideProgressDialog()
//                    (activity as BaseActivity).showErrorSnackbar(R.string.login_chat_login_error, e, View.OnClickListener { signIn(user) })
//                }
//                Log.e(TAG, "##########  signIn(): ex: " + e.errors + " : errorCode: " + e.httpStatusCode)
//            }
//        })
//    }
//
//    private fun loginToChat(user: QBUser) {
//        showProgressDialog(activity!!, activity!!.getString(R.string.dlg_restoring_chat_session), false)
//        //Need to set password, because the server will not register to chat without password
//        user.password = USER_DEFAULT_PASSWORD
//        ChatHelper.getInstance().loginToChat(user, object : QBEntityCallback<Void> {
//            override fun onSuccess(void: Void?, bundle: Bundle?) {
//                Log.e(TAG, "Chat login onSuccess()")
//                SharedPrefsHelper.getInstance().saveQbUser(user)
//                DialogsActivity.start(context!!)
//                hideProgressDialog()
//            }
//
//            override fun onError(e: QBResponseException) {
//                Log.w(TAG, "Chat login onError(): $e")
//                hideProgressDialog()
//                (activity as BaseActivity).showErrorSnackbar(R.string.login_chat_login_error, e, null)
//                Log.e(TAG, "############  loginToChat(): ex: " + e.errors + " : errorCode: " + e.httpStatusCode)
//            }
//        })
//    }
//
//    private fun updateUser(user: QBUser) {
//        ChatHelper.getInstance().updateUser(user, object : QBEntityCallback<QBUser> {
//            override fun onSuccess(qbUser: QBUser, bundle: Bundle?) {
//                loginToChat(user)
//            }
//
//            override fun onError(e: QBResponseException) {
//                Log.e(TAG, "Chat updateUser onError(): $e")
//                hideProgressDialog()
//                (activity as BaseActivity).showErrorSnackbar(R.string.login_chat_login_error, e, null)
//                Log.e(TAG, "############  updateUser(): ex: " + e.errors + " : errorCode: " + e.httpStatusCode)
//            }
//        })
//    }
//
//    private fun signUp(user: QBUser) {
//        SharedPrefsHelper.getInstance().removeQbUser()
//        QBUsers.signUp(user).performAsync(object : QBEntityCallback<QBUser> {
//            override fun onSuccess(p0: QBUser?, p1: Bundle?) {
//                hideProgressDialog()
//                signIn(user)
//            }
//
//            override fun onError(exception: QBResponseException?) {
//                Log.w(TAG, "Chat signUp onError(): $exception")
//                hideProgressDialog()
//                (activity as BaseActivity).showErrorSnackbar(R.string.login_sign_up_error, exception, null)
//                Log.e(TAG, "############# signUp(): ex: " + exception!!.errors + " : errorCode: " + exception.httpStatusCode)
//            }
//        })
//    }
//}
//
//
