package com.mart.onestopkitchen.chat.utils

import android.Manifest


const val EXTRA_FCM_MESSAGE: String = "message"
const val ACTION_NEW_FCM_EVENT: String = "new-push-event"

val PREFERRED_IMAGE_SIZE_FULL: Int = dpToPx(320)

val PERMISSIONS = arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)

const val MAX_OPPONENTS_COUNT = 6

const val EXTRA_LOGIN_RESULT = "login_result"

const val EXTRA_LOGIN_ERROR_MESSAGE = "login_error_message"

const val EXTRA_LOGIN_RESULT_CODE = 1002

const val EXTRA_IS_INCOMING_CALL = "conversation_reason"

const val USER_DEFAULT_PASSWORD = "quickblox"


const val CHAT_PORT = 5223
const val SOCKET_TIMEOUT = 300
const val KEEP_ALIVE: Boolean = true
const val USE_TLS: Boolean = true
const val AUTO_JOIN: Boolean = false
const val AUTO_MARK_DELIVERED: Boolean = true
const val RECONNECTION_ALLOWED: Boolean = true
const val ALLOW_LISTEN_NETWORK: Boolean = true