package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.SubCategoryProductDetails.SubCategoryProduct;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.UserSelectedList;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hemnath on 9/22/2018.
 */

public class SubCategoryDrawerAdapter extends RecyclerView.Adapter<SubCategoryDrawerAdapter.DrawerHolder> {

    private final OnItemSubcategoryListener listener;

    List<SubCategoryProduct.SubCategory> subCategories;

    Context context;


    public SubCategoryDrawerAdapter(Context context, OnItemSubcategoryListener onItemSubcategoryListener) {
        this.context = context;
        this.subCategories = new ArrayList<>();
        this.listener = onItemSubcategoryListener;

    }

    public void loadData(List<SubCategoryProduct.SubCategory> subCategories) {
        this.subCategories = subCategories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DrawerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_subcategory, parent, false);
        return new DrawerHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull DrawerHolder holder, final int position) {

        holder.subTitle.setText(AppUtils.capitalizeWords(subCategories.get(position).getName()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                    ((MyApplication) context.getApplicationContext().getApplicationContext()).alreadySelectedPrice = "0-0";
                    ((MyApplication) context.getApplicationContext()).alreadySelected = null;
                    ((MyApplication) context.getApplicationContext()).rightValInt = 0;
                    ((MyApplication) context.getApplicationContext()).leftValInt = 0;
                    ((MyApplication) context.getApplicationContext()).selectedModelLis = new UserSelectedList();
                    listener.onItemClick(subCategories.get(position).getId(), subCategories.get(position).getName());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return subCategories.size();
    }

    public interface OnItemSubcategoryListener {
        void onItemClick(int pId, String s);
    }


    public class DrawerHolder extends RecyclerView.ViewHolder {

        TextView subTitle;

        public DrawerHolder(View itemView) {
            super(itemView);
            subTitle = itemView.findViewById(R.id.sub_title);
        }

    }
}
