package com.mart.onestopkitchen.ui.adapter;

/**
 * Updated by AkashGarg on 27/12/2018.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.model.BaseProductModel;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.utils.AppUtils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class FeaturedProductAdapter extends HomePageProductAdapter {
    public FeaturedProductAdapter(Context context, List<? extends BaseProductModel> productsList) {
        super(context, productsList);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull ProductSummaryHolder holder, int position) {
        ProductModel productModel = (ProductModel) productsList.get(position);
        Glide.with(context)
                .load(productModel.getDefaultPictureModel()
                        .getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(holder.productImage);

        holder.productName.setText(productModel.getName());

        if (null != productModel.getProductPrice().getPriceWithDiscount() && !productModel.getProductPrice().getPriceWithDiscount().isEmpty()) {
            holder.productOldPrice.setVisibility(View.VISIBLE);
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPriceWithDiscount(), 0));
            holder.productOldPrice.setText(productModel.getProductPrice().getPrice());

        } else if (null != productModel.getProductPrice().getOldPrice()) {
            holder.productOldPrice.setVisibility(View.VISIBLE);
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
            holder.productOldPrice.setText(productModel.getProductPrice().getOldPrice());

        } else {
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
            holder.productOldPrice.setText(productModel.getProductPrice().getOldPrice());
            holder.productOldPrice.setVisibility(View.GONE);
        }

        if (productModel.getProductPrice().getDiscountPercentage() > 0) {
            holder.productOfferPercentage.setVisibility(View.VISIBLE);
            holder.productOfferPercentage.setText(productModel.getProductPrice().getDiscountPercentage() + "%");
        } else
            holder.productOfferPercentage.setVisibility(View.GONE);
        holder.productOldPrice.setPaintFlags(holder.productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
