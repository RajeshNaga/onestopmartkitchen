package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.ViewType;
import com.mart.onestopkitchen.utils.AppUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arif Islam on 23-Feb-17.
 */

public class ProductListAdapter extends RecyclerView.Adapter {
    private List<ProductModel> productList;
    private Context context;

    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private int currentPage = 1;
    private int totalPage = 1;

    private OnLoadMoreListener loadMoreListener;
    private OnProductClickListener productClickListener;

    public ProductListAdapter(Context context, List<ProductModel> productList, RecyclerView recyclerView, int totalProductPage) {
        this.productList = new ArrayList<>();
        this.productList.addAll(productList);
        this.context = context;
        this.totalPage = totalProductPage;

        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager layoutManager = (GridLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold) && currentPage < totalPage) {
                        // End has been reached
                        // Do something
                        currentPage++;
                        if (loadMoreListener != null) {
                            loadMoreListener.onLoadMore(currentPage);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public void showLoader() {
        productList.add(null);
    }

    public void hideLoader() {
        productList.remove(productList.size() - 1);
        notifyItemRemoved(productList.size());
    }

    public void addMoreProducts(List<ProductModel> productList) {
        if (this.productList == null) {
            this.productList = new ArrayList<>();
        }
        this.productList.addAll(productList);

    }

    public void resetList() {
        if (productList != null) {
            currentPage = 1;
            totalPage = 1;
            productList.clear();
            notifyDataSetChanged();
        }
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        if (viewType == ViewType.GRID) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_products_grid, parent, false);
            return new ProductSummaryHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_progress, parent, false);
            return new ProgressViewHolder(itemView);
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder bindViewHolder, int position) {
        try {
            if (bindViewHolder instanceof ProductSummaryHolder) {
                ProductModel productModel = productList.get(position);
                ProductSummaryHolder holder = (ProductSummaryHolder) bindViewHolder;
                holder.productName.setText(productModel.getName());


                if (null != productModel.getProductPrice().getPriceWithDiscount() && !productModel.getProductPrice().getPriceWithDiscount().isEmpty()) {
                    holder.productOldPrice.setVisibility(View.VISIBLE);
                    holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPriceWithDiscount(), 0));
                    holder.productOldPrice.setText(productModel.getProductPrice().getPrice());

                } else if (null != productModel.getProductPrice().getOldPrice()) {
                    holder.productOldPrice.setVisibility(View.VISIBLE);
                    holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                    holder.productOldPrice.setText(productModel.getProductPrice().getOldPrice());

                } else {
                    holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                    holder.productOldPrice.setText(productModel.getProductPrice().getOldPrice());
                    holder.productOldPrice.setVisibility(View.GONE);
                }
                holder.productOldPrice.setPaintFlags(holder.productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                Glide.with(context).load(productModel.getDefaultPictureModel().getImageUrl())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(holder.productImage);

                if (productModel.getProductPrice().getDiscountPercentage() > 0) {
                    holder.tvDiscountPercentage.setText(productModel.getProductPrice().getDiscountPercentage() + "%");
                }

                holder.fav.setTag(position);
            } else {
                ((ProgressViewHolder) bindViewHolder).progressBar.setIndeterminate(true);
            }

        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return productList.get(position) != null ? ViewType.GRID : ViewType.PROGRESS;
    }


    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setOnProductClickListener(OnProductClickListener productClickListener) {
        this.productClickListener = productClickListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int currentPage);
    }

    public interface OnProductClickListener {
        void onProductClick(View view, ProductModel product);
    }

    private static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar1);
        }
    }

    private class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView productImage;
        TextView productPrice;
        TextView productOldPrice;
        TextView productName;
        ImageView fav;
        TextView tvDiscountPercentage;

        ProductSummaryHolder(View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.img_productImage);
            productPrice = itemView.findViewById(R.id.tv_productPrice);
            productName = itemView.findViewById(R.id.tv_productName);
            productOldPrice = itemView.findViewById(R.id.tv_productOldPrice);
            productOldPrice.setPaintFlags(productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvDiscountPercentage = itemView.findViewById(R.id.tv_discount_percentage);
            fav = itemView.findViewById(R.id.fav);
            fav.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(this);
            fav.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.fav) {
                Toast.makeText(context, "fav product added ", Toast.LENGTH_SHORT).show();
            } else {
                if (productClickListener != null) {
                    productClickListener.onProductClick(v, productList.get(getAdapterPosition()));
                }
            }
        }
    }

}
