/*
 * Copyright (c) : mart , Created By: Akash Garg.
 */

package com.mart.onestopkitchen.ui.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.model.HelpSupportDataItem
import com.mart.onestopkitchen.ui.adapter.HelpSupportAdapter.MyHelpSupportViewHolder
import com.mart.onestopkitchen.ui.fragment.helpandsupport.HelpAndSupportFragment
import kotlinx.android.synthetic.main.row_help_support.view.*


class HelpSupportAdapter(val mContext: Context, var data: List<HelpSupportDataItem>) : RecyclerView.Adapter<MyHelpSupportViewHolder>() {
    private var inflater: LayoutInflater? = null
    private var mListener: CallToHelpSupport? = null

    init {
        inflater = LayoutInflater.from(mContext)
    }

    fun setListener(listener: HelpAndSupportFragment) {
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHelpSupportViewHolder {
        val view = inflater!!.inflate(R.layout.row_help_support, parent, false)
        return MyHelpSupportViewHolder(view, mContext)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MyHelpSupportViewHolder, position: Int) {
        data[position].let { holder.bind(it) }
        holder.itemView.btnCall.setOnClickListener {
            mListener?.let {
                it.callToHelp(data[position].let { it })
            }
        }
    }

    class MyHelpSupportViewHolder(itemView: View, var mContext: Context) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: HelpSupportDataItem?) {
            itemView.tvNumber.text = data?.Value
            when (data?.Title) {
                "Head Office" -> {
                    itemView.imgCallIcon.setImageResource(R.drawable.ic_zayok_office)
                    itemView.btnCall.visibility = View.GONE
                }
                "CALL US" -> {
                    itemView.imgCallIcon.setImageResource(R.drawable.ic_phone_icon)
                }
                "SMS US" -> {
                    itemView.imgCallIcon.setImageResource(R.drawable.ic_sms)
                    itemView.btnCall.text = mContext.getString(R.string.send)
                }
                "VIBER" -> {
                    itemView.imgCallIcon.setImageResource(R.drawable.ic_viber)
                    itemView.btnCall.text = mContext.getString(R.string.open)
                }
                "WhatsApp" -> {
                    itemView.imgCallIcon.setImageResource(R.drawable.ic_whatsapp)
                    itemView.btnCall.text = mContext.getString(R.string.open)
                }
                "EMAIL US" -> {
                    itemView.imgCallIcon.setImageResource(R.drawable.ic_mail)
                    itemView.btnCall.text = mContext.getString(R.string.open)
                }
            }
        }
    }

    interface CallToHelpSupport {
        fun callToHelp(data: HelpSupportDataItem)
    }
}