package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hemnath on 11/16/2018.
 */
public class MeasureAdapter extends RecyclerView.Adapter<MeasureAdapter.MeasureHolder> {

    Context context;

    List<String> dataList;

    public MeasureAdapter(Context context) {
        this.context = context;
        dataList = new ArrayList<>();
    }

    public void loadData(List<String> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MeasureHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_measure_view, viewGroup, false);
        return new MeasureHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MeasureHolder measureHolder, int i) {
        measureHolder.tv_measure.setText(dataList.get(i));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MeasureHolder extends RecyclerView.ViewHolder {

        TextView tv_measure;

        public MeasureHolder(View itemView) {
            super(itemView);
            tv_measure = (TextView) itemView.findViewById(R.id.tv_measure_view);
        }

    }
}
