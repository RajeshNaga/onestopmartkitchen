package com.mart.onestopkitchen.ui.fragment.submenu.adapter.fragmentadapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.addsubmenu.CategeryItemsItem;

import java.util.List;

public class AddSubMenuSecondLeftAdapter extends RecyclerView.Adapter<AddSubMenuSecondLeftAdapter.ViewHolder> {
    List<CategeryItemsItem> children;
    private OnSecondViewClick onViewClick;
    private Context mContext;

    public AddSubMenuSecondLeftAdapter(OnSecondViewClick onViewClick, Context mContext, List<CategeryItemsItem> children) {
        this.onViewClick = onViewClick;
        this.mContext = mContext;
        this.children = children;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sub_menu_new, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(children.get(position).getItemName());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onViewClick.viewSecondViewClicked(children.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    public interface OnSecondViewClick {
        void viewSecondViewClicked(CategeryItemsItem categeryItemsItem, int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt_title);
        }
    }
}
