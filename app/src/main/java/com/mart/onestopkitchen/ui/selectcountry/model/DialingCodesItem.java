package com.mart.onestopkitchen.ui.selectcountry.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DialingCodesItem implements Serializable {

    @SerializedName("City")
    private String city;

    @SerializedName("Code")
    private String code;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}