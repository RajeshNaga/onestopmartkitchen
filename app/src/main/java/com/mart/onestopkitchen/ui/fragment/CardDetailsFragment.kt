package com.mart.onestopkitchen.ui.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.application.MyApplication
import com.mart.onestopkitchen.loginuser.DeviceInformation
import com.mart.onestopkitchen.model.creditcarddao.CardDetailModel
import com.mart.onestopkitchen.table.AppDatabase
import com.mart.onestopkitchen.utils.AppUtils
import com.mart.onestopkitchen.utils.CreditCardNumberFormatWatcher
import com.mart.onestopkitchen.utils.fingerprint.FingerPrintPresenter
import com.mart.onestopkitchen.utils.fingerprint.FingerprintBaseView
import kotlinx.android.synthetic.main.layout_card_details_save.*
import kotlinx.android.synthetic.main.layout_fingerprint_dialog.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Akash Garg on 15/01/2019.
 */

open class CardDetailsFragment : BaseFragment(), FingerprintBaseView {
    private var isValid = true
    private var month = ""
    private var year = ""
    private var savesCard: CardDetailModel? = null
    private var presenter: FingerPrintPresenter? = null
    var dialog: Dialog? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity?.title = getString(R.string.card_details)
        return inflater.inflate(R.layout.layout_card_details_save, container, false)
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isValid = true
        activity?.title = getString(R.string.card_details)
        dialog = Dialog(activity!!)
        if (checkDeviceFingerPrintAvailability(activity)) {
            presenter = FingerPrintPresenter(this.activity!!, this)
        }
        setMonth()
        setYears()
        onItemSelected()
        payBtnClick()
        tvProductAmount.text = AppUtils.getMMKString(tvProductAmount.textSize, (activity!!.application as MyApplication).productAmount, 0)
        tvShippingCharge.text = AppUtils.getMMKString(tvShippingCharge.textSize, (activity!!.application as MyApplication).shippingCharges, 0)
        tvTotalAmount.text = AppUtils.getMMKString(tvTotalAmount.textSize, (activity!!.application as MyApplication).totalAmount, 0)
        btn_pay.text = getString(R.string.pay) + " " + AppUtils.getMMKString(tvTotalAmount.textSize, (activity!!.application as MyApplication).totalAmount, 0)
    }

    private fun openFingerPrintPopup() {
        dialog?.setContentView(R.layout.layout_fingerprint_dialog)
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialog?.setCancelable(false)
        dialog?.window?.setWindowAnimations(R.style.dialog_animation_fade)
//        dialog?.show()
    }

    private fun setCardData(savesCard: CardDetailModel) {
        etCardHolderName.setText(savesCard.cardHolderName)
        etCardNumber.setText(savesCard.cardNumber)
        etCvvNumber.setText(savesCard.cardCvv)
        spinner_month.setSelection(monthList.indexOf(savesCard.cardExpMonth))
        spinner_year.setSelection(yearsList.indexOf(savesCard.cardExpYear))
    }

    override fun onResume() {
        super.onResume()
        etCardNumber.addTextChangedListener(CreditCardNumberFormatWatcher())
    }

    private var monthList: ArrayList<String> = arrayListOf()

    private fun setMonth() {
        monthList = ArrayList()
        monthList.add("MM")
        for (j in 1..12) {
            monthList.add(Integer.toString(j))
        }
        val monthAdapter = ArrayAdapter<String>(activity!!, R.layout.simple_list_item_1, monthList)
        spinner_month.setSelection(0)
        monthAdapter.setDropDownViewResource(R.layout.simple_list_item_1)
        spinner_month.adapter = monthAdapter
    }

    private var yearsList: ArrayList<String> = arrayListOf()

    private fun setYears() {
        yearsList = ArrayList()
        yearsList.add("YYYY")
        val year = Calendar.getInstance()
        val currentYear = year.get(Calendar.YEAR)
        val nextCal = currentYear.plus(15)
        for (i in currentYear..nextCal) {
            yearsList.add(Integer.toString(i))
        }
        val yearAdapter = ArrayAdapter<String>(activity!!, R.layout.simple_list_item_1, yearsList)
        spinner_year.setSelection(0)
        yearAdapter.setDropDownViewResource(R.layout.simple_list_item_1)
        spinner_year.adapter = yearAdapter
    }


    private fun isFormValid(): Boolean {
        when {
            getText(etCardHolderName).isEmpty() -> {
                showSnack(getString(R.string.add_cardholder_name))
                isValid = false
            }
            getText(etCardNumber).isEmpty() || getText(etCardNumber).length < 19 -> {
                showSnack(getString(R.string.add_card_number))
                isValid = false
            }
            month.isEmpty() || month == "MM" -> {
                showSnack(getString(R.string.select_card_exp_month))
                isValid = false
            }
            year.isEmpty() || year == "YYYY" -> {
                showSnack(getString(R.string.select_card_exp_year))
                isValid = false
            }
            getText(etCvvNumber).isEmpty() -> {
                showSnack(getString(R.string.card_cvv_no))
                isValid = false
            }
            else -> {
                isValid = true
            }
        }
        return isValid
    }

    private fun onItemSelected() {
        spinner_month?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                month = "MM"
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                month = parent?.getItemAtPosition(position).toString()
            }
        }

        spinner_year?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                year = "YYYY"
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                year = parent?.getItemAtPosition(position).toString()
            }
        }
    }

    private fun payBtnClick() {
        btn_pay.setOnClickListener {
            if (DeviceInformation().deviceInfo.checkInterNet) {
                if (isFormValid()) {
                    val db = AppDatabase.getInstance(activity!!.applicationContext).cardDetails()
                    val model = savesCard ?: CardDetailModel()
                    model.cardHolderName = getText(etCardHolderName)
                    model.cardNumber = getText(etCardNumber)
                    model.cardCvv = getText(etCvvNumber)
                    model.cardExpMonth = month
                    model.cardExpYear = year
                    if (savesCard != null) {
//                    db.updateCardDetails(savesCard!!.userId, model.cardHolderName, model.cardNumber, model.cardCvv, model.cardExpMonth, model.cardExpYear)
                        db.updateCardDetails(model)
                        fragmentManager!!.beginTransaction().replace(R.id.container, ConfirmOrderFragment()).addToBackStack(null).commit()
                        return@setOnClickListener
                    }
                    db.insertCardDetails(model)
                    fragmentManager!!.beginTransaction().replace(R.id.container, ConfirmOrderFragment()).addToBackStack(null).commit()
                }
            }
        }
    }

    override fun showMsg(msg: String) {
        dialog?.dismiss()
        showToast(msg)
    }

    override fun success() {
        dialog?.ic_finger?.setImageResource(R.drawable.ic_finger_success)
        dialog?.tvStatus?.text = getString(R.string.success)
        dialog?.tvStatus?.setTextColor(Color.GREEN)
        savesCard?.run { setCardData(savesCard!!) }
        showToast(getString(R.string.success))
        Handler().postDelayed({
            dialog?.window?.setWindowAnimations(R.style.dialog_animation_fade)
            dialog?.dismiss()
        }, 600)
    }

    override fun failure() {
        dialog?.ic_finger?.setImageResource(R.drawable.ic_finger_failure)
        dialog?.tvStatus?.text = getString(R.string.failure)
        dialog?.tvStatus?.setTextColor(Color.RED)
        Handler().postDelayed({
            dialog?.window?.setWindowAnimations(R.style.dialog_animation_fade)
            dialog?.ic_finger?.setImageResource(R.drawable.ic_fingerprints)
        }, 600)
        showToast(getString(R.string.failure))
    }

    override fun error() {
        showToast("Something Error! Please Try Again.")
    }

    override fun showFingerPrintPopup() {
        val db = AppDatabase.getInstance(activity!!.applicationContext).cardDetails()
        savesCard = db.getCardDetails()
        if (savesCard != null) {
            openFingerPrintPopup()
        }
    }
}


