package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.PictureModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesSlideAdapter extends RecyclerView.Adapter<ImagesSlideAdapter.ImageHolder> {
    public static int row_index = 0;
    int val;
    private Context mContext;
    private List<PictureModel> pictureModels;
    private OnImageSelectListener listener;

    public ImagesSlideAdapter(Context mContext, List<PictureModel> pictureModels, OnImageSelectListener listener) {
        this.mContext = mContext;
        this.pictureModels = pictureModels;
        this.listener = listener;
        row_index = 0;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_imageview_slider, parent, false);
        return new ImageHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, final int position) {
        if (pictureModels.get(position).getImageUrl().contains(".gif")) {
            Glide.with(mContext)
                    .load(pictureModels.get(position).getImageUrl())
                    .asGif()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.image);
        } else {
            Glide.with(mContext)
                    .load(pictureModels.get(position).getImageUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.image);
        }

        if (position == 4) {
            if (pictureModels.size() > position) {
                holder.grayMask.setVisibility(View.VISIBLE);
                holder.count.setVisibility(View.VISIBLE);
                val = (pictureModels.size() - 1) - position;
                if (val == 0) {
                    holder.grayMask.setVisibility(View.GONE);
                    holder.count.setVisibility(View.GONE);
                }
                holder.count.setText("+" + val);
            }
        }


        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position != 4) {
                    row_index = position;
                    /*listener.onItemSelect(position, pictureModels.get(position));
                    notifyDataSetChanged();*/
                    stickWithTheScreen(position);
                } else if (position == 4 && val == 0) {
                    stickWithTheScreen(position);
                } else {
                    //EventBus.getDefault().post(pictureModels);
                    listener.OnItemMaxFour(position, pictureModels);
                }

            }
        });

        if (row_index == position) {
            holder.image.setBackground(mContext.getDrawable(R.drawable.blue_ractangle_border));
        } else {
            holder.image.setBackground(mContext.getDrawable(R.drawable.image_border));
        }

    }

    public void stickWithTheScreen(int position) {
        listener.onItemSelect(position, pictureModels.get(position));
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (pictureModels.size() >= 5) {
            return 5;
        } else {
            return pictureModels.size() > 0 ? pictureModels.size() : 0;
        }
    }

    public void changeSlide(int imgId) {
        for (int i = 0; i < pictureModels.size(); i++) {

            if (pictureModels.get(i).getId() == imgId) {
                stickWithTheScreen(i);
            }
        }
    }

    public interface OnImageSelectListener {
        void onItemSelect(int position, PictureModel img);

        void OnItemMaxFour(int position, List<PictureModel> img);
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.iv_mask)
        LinearLayout grayMask;

        @BindView(R.id.tv_count)
        TextView count;

        public ImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
