package com.mart.onestopkitchen.ui.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.ccpp.my2c2psdk.cores.My2c2pResponse;
import com.ccpp.my2c2psdk.cores.My2c2pSDK;
import com.ccpp.my2c2psdk.cores.My3DSActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.okdollar.paymentgateway.OKDollar;
import com.okdollar.paymentgateway.OKListener;
import com.okdollar.paymentgateway.OKTransactionSuccessDetails;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.listener.OkBtnClickListener;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.BillingAddress;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.model.OrderReviewData;
import com.mart.onestopkitchen.model.PaymentResponseModel;
import com.mart.onestopkitchen.model.SaveTransactionStatusModel;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.CartProductListResponse;
import com.mart.onestopkitchen.networking.response.CheckoutOrderSummaryResponse;
import com.mart.onestopkitchen.networking.response.OrderTotalResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.adapter.CheckoutOrderProductAdapter;
import com.mart.onestopkitchen.ui.customview.SimpleDividerItemDecoration;
import com.mart.onestopkitchen.ui.views.FormViews;
import com.mart.onestopkitchen.utils.AppConstants;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.ConfirmOrderViewModel;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.Language;
import com.mart.onestopkitchen.utils.TextUtils;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Updated by Akash Garg on 31/1/2019.
 */


public class ConfirmOrderFragment extends BaseFragment implements View.OnClickListener {
    static String PaymentMethodSystemName = "";
    public boolean IsPaymentApiRunning = false;
    @BindView(R.id.tv_subtotal)
    TextView subTotalTextView;
    @BindView(R.id.tv_shipping)
    TextView shippingTextView;
    @BindView(R.id.tv_tax)
    TextView taxTextView;
    @BindView(R.id.taxRow)
    TableRow taxRow;
    @BindView(R.id.tv_Total)
    TextView totalAmountTextView;
    @BindView(R.id.tv_discount)
    TextView discountTextView;
    @BindView(R.id.btn_continue)
    Button confirmButton;
    @BindView(R.id.tr_discount)
    TableRow discountTableRow;
    @BindView(R.id.rclv_chekoutProductList)
    RecyclerView checkoutProductList;
    @BindView(R.id.ll_billing_address)
    LinearLayout billingAddressLinearLayout;
    @BindView(R.id.ll_shipping_address)
    LinearLayout ll_shipping_address;
    @BindView(R.id.ll_store_address)
    LinearLayout ll_store_address;
    @BindView(R.id.shippingLayout)
    LinearLayout shippingLayout;
    @BindView(R.id.storeLayouts)
    LinearLayout storeLayouts;
    @BindView(R.id.taxKey)
    TextView taxKey;
    @BindView(R.id.tv_addresss)
    TextView tv_addresss;
    @BindView(R.id.tv_citys)
    TextView tv_citys;
    @BindView(R.id.tv_countrys)
    TextView tv_countrys;
    @BindView(R.id.label_deliveryAddress)
    TextView label_deliveryAddress;
    @BindView(R.id.progress)
    ProgressBar progressBar;


    private String TAG = ConfirmOrderFragment.class.getSimpleName();
    private ConfirmOrderViewModel confirmViewModel;
    private CheckoutOrderSummaryResponse checkoutOrderSummaryResponse;
    private String transactionId = "0";
    private int REQUEST_SDK = 101;
    private Double totalAmount = 0.0;
    private String merchantNumber = ""; //00959768077189
    private String PaymentStatus = "";
    private String transactionStatusCode = "002";

    private OKListener okListener = new OKListener() {
        @Override
        public void onSuccess(OKTransactionSuccessDetails okTransactionSuccessDetails) {
            if (okTransactionSuccessDetails != null) {
                setLocale();
                IsPaymentApiRunning = true;
                if (okTransactionSuccessDetails.getResultcode().equals("0")) { //success

                    PreferenceService.getInstance().SetPreferenceValue(PreferenceService.IS_PAYMENT_DONE, true);

                    showToast(getString(R.string.payment_done_successfully));
                    ((MyApplication) Objects.requireNonNull(getActivity()).getApplicationContext()).orderItems = checkoutOrderSummaryResponse.getShoppingCartModel().getItems();

                    if (progressBar != null)
                        progressBar.setVisibility(View.VISIBLE);

                    transactionId = okTransactionSuccessDetails.getTransatId();
                    PaymentStatus = okTransactionSuccessDetails.getDescripton();
                    transactionStatusCode = "000";

                    placedOrderConfirmed(transactionId);

//                   Log.e(TAG, "------Ok$ Payment Response- : " + okTransactionSuccessDetails.getDescripton() + "; TransactionId-: " + okTransactionSuccessDetails.getTransatId());

                } else {
                    if (getActivity() != null) {
                        AppDialogs.customDialog(getActivity(), getActivity().getResources().getString(R.string.insufficent_balance_okdollar), new OkBtnClickListener() {
                            @Override
                            public void onOkClick() {
                                showToast(getString(R.string.payment_denied));
                                IsPaymentApiRunning = false;
                            }
                        });
                    }
                }
            }
        }

        @Override
        public void onFailure(String msg) {
            setLocale();
            IsPaymentApiRunning = false;
            PaymentStatus = "Failure";
            if (null != getActivity()) {
                showToast(getString(R.string.please_try_again));
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_confirm_order, container, false);
        if (null != getActivity())
            getActivity().setTitle(getString(R.string.order_confirmation));
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        Log.e(TAG, "----------  onViewCreated()--------------");
        if (null != getActivity())
            getActivity().setTitle(getString(R.string.order_confirmation));
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        setLayoutManagerofRecyclerList();
        confirmButton.setOnClickListener(this);
        if (!PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS).isEmpty())
            label_deliveryAddress.setText(getActivity().getString(R.string.pickup_address));
    }

    @Override
    public void onStart() {
        super.onStart();
//        Log.e(TAG, "----------  onStart()--------------");
    }

    @Override
    public void onResume() {
        super.onResume();
//        Log.e(TAG, "----------  onResume()--------------");
        if (null != getActivity()) {
            ((MainActivity) getActivity()).searchBarCodeLayout(false);
            ((MainActivity) getActivity()).showHideCart(false);
            ((MainActivity) getActivity()).setArrowIconInDrawer();
            confirmButton.setOnClickListener(this);
            setLocale();
        }
        callCheckoutOrderSummaryWebservice();
    }

    @Override
    public void onPause() {
        super.onPause();
//        Log.e(TAG, "----------  onPause()--------------");
    }

    @Override
    public void onStop() {
        super.onStop();
//        Log.e(TAG, "----------  onStop()--------------");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        Log.e(TAG, "----------  onDestroyView()--------------");
        if (null != confirmViewModel)
            confirmViewModel.releaseObj();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "----------  onDestroy()--------------");
    }

    private void callCheckoutOrderSummaryWebservice() {
        RetroClient.getApi().getCheckoutOrderSummary().enqueue(new CustomCB<CheckoutOrderSummaryResponse>(this.getView()));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setLayoutManagerofRecyclerList() {
//        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        checkoutProductList.setHasFixedSize(true);
        checkoutProductList.setLayoutManager(getLinearLayoutManager(LinearLayout.VERTICAL));
    }

    public void onEvent(CheckoutOrderSummaryResponse checkoutOrderSummaryResponse) {
        try {
            if (checkoutOrderSummaryResponse != null) {
                this.checkoutOrderSummaryResponse = checkoutOrderSummaryResponse;
                processCheckoutProductList(checkoutOrderSummaryResponse.getShoppingCartModel());
                processCheckoutOrderTotal(checkoutOrderSummaryResponse.getOrderTotalModel());
                processCheckoutBillingAddress(checkoutOrderSummaryResponse.getShoppingCartModel().getOrderReviewData().getShippingAddress());
                processCheckoutShippingAddress(checkoutOrderSummaryResponse.getShoppingCartModel().getOrderReviewData());
                merchantNumber = checkoutOrderSummaryResponse.getMerchantNumber() != null ? checkoutOrderSummaryResponse.getMerchantNumber() : "";
                showHideCheckoutBtn(checkoutOrderSummaryResponse.getShoppingCartModel().isCanContinue());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showHideCheckoutBtn(boolean isShowCheckoutBtn) {
        if (isShowCheckoutBtn) {
            confirmButton.setBackground(getResources().getDrawable(R.drawable.gradient_bg));
            confirmButton.setTextColor(getResources().getColor(R.color.white));
            confirmButton.setEnabled(true);
            confirmButton.setClickable(true);
        } else {
            confirmButton.setBackgroundColor(getResources().getColor(R.color.bt_light_gray));
            confirmButton.setTextColor(getResources().getColor(R.color.white));
            confirmButton.setEnabled(false);
            confirmButton.setClickable(false);
        }
    }

    private void processCheckoutProductList(CartProductListResponse cartProductListResponse) {
        if (cartProductListResponse != null && cartProductListResponse.getItems() != null) {
            if (cartProductListResponse.getItems().size() > 0) {
                populatedDatainAdapter(cartProductListResponse.getItems());
            } else {
                if (getFragmentManager() != null) {
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    gotoNewFragment(new HomePageFragment());
                }
                Snackbar.make(Objects.requireNonNull(getView()), R.string.not_item_to_process, Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private void populatedDatainAdapter(List<CartProduct> cartProductList) {
        CheckoutOrderProductAdapter checkoutOrderProductAdapter = new CheckoutOrderProductAdapter(getActivity(), cartProductList, this, preferenceService);
        checkoutProductList.setAdapter(checkoutOrderProductAdapter);
        checkoutProductList.setNestedScrollingEnabled(false);
        checkoutProductList.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
    }

    private void processCheckoutOrderTotal(OrderTotalResponse orderTotalModel) {
        if (orderTotalModel.getStatusCode() == 200) {
            populateDataInOrderTotalLayout(orderTotalModel);
        }
    }

    @SuppressLint("RtlHardcoded")
    private void populateDataInOrderTotalLayout(OrderTotalResponse orderTotalRespons) {
        if (orderTotalRespons != null) {
            if (null != orderTotalRespons.getOrderTotal()) {
//                Log.e(TAG, "----------:totalAmountin double " + (double) orderTotalRespons.getOrderTotalInt() + " :orderTotalRespons.getOrderTotal():  " + orderTotalRespons.getOrderTotal());

                totalAmount = (double) orderTotalRespons.getOrderTotalInt();
                totalAmountTextView.setText(AppUtils.getMMKString(totalAmountTextView.getTextSize(), orderTotalRespons.getOrderTotal(), 0));

//                totalAmountTextView.setText(AppUtils.getMMKString(totalAmountTextView.getTextSize(), "1", 0));
            }

            if (null != orderTotalRespons.getSubTotal())
                subTotalTextView.setText(AppUtils.getMMKString(subTotalTextView.getTextSize(), orderTotalRespons.getSubTotal(), 0));

            if (null != orderTotalRespons.getShipping())
                shippingTextView.setText(AppUtils.getMMKString(shippingTextView.getTextSize(),
                        !orderTotalRespons.getShipping().equalsIgnoreCase("0 MMK") ? orderTotalRespons.getShipping() : getString(R.string.free), 0));

            if (null != orderTotalRespons.getTax()) {
                if (orderTotalRespons.getTax().startsWith("0")) {
                    taxRow.setVisibility(View.GONE);
                } else
                    taxTextView.setText(AppUtils.getMMKString(taxTextView.getTextSize(), orderTotalRespons.getTax(), 0));
            }

            if (orderTotalRespons.getOrderTotalDiscount() != null) {
                discountTableRow.setVisibility(View.VISIBLE);
                discountTextView.setText(orderTotalRespons.getOrderTotalDiscount());
                discountTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            } else
                discountTableRow.setVisibility(View.GONE);
        }
    }

    private void processCheckoutBillingAddress(BillingAddress billingAddress) {
        populateAddres(billingAddress, billingAddressLinearLayout);
    }

    @Override
    public void onInflate(@NotNull Context context, @NotNull AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(context, attrs, savedInstanceState);
    }

    @SuppressLint("RtlHardcoded")
    private void processCheckoutShippingAddress(OrderReviewData orderReviewData) {
        if (orderReviewData.isSelectedPickUpInStore()) {
            storeLayouts.setVisibility(View.VISIBLE);
            shippingLayout.setVisibility(View.GONE);
            populateStoreAddres(orderReviewData.getPickupAddress(), ll_store_address);
        } else {
            storeLayouts.setVisibility(View.GONE);
            shippingLayout.setVisibility(View.VISIBLE);
            ll_shipping_address.setGravity(Gravity.RIGHT);
            populateAddres(orderReviewData.getShippingAddress(), ll_shipping_address);
        }
    }

    @SuppressLint("RtlHardcoded")
    private void populateStoreAddres(BillingAddress billingAddress, LinearLayout layout) {
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            tv_addresss.setGravity(Gravity.RIGHT);
            tv_citys.setGravity(Gravity.RIGHT);
            tv_countrys.setGravity(Gravity.RIGHT);
        }
        FormViews.setText(tv_addresss.getId(), "" + TextUtils.getNullSafeString(billingAddress.getAddress1()), layout);
        FormViews.setText(tv_citys.getId(), "" + TextUtils.getNullSafeString(billingAddress.getCity() + ", " + billingAddress.getZipPostalCode()), layout);
        FormViews.setText(tv_countrys.getId(), "" + TextUtils.getNullSafeString(billingAddress.getCountryName()), layout);
    }

    @SuppressLint("RtlHardcoded")
    private void populateAddres(BillingAddress billingAddress, LinearLayout layout) {
        Log.e(TAG, "----billingAddress-----" + billingAddress.toString());
        TextView tv_name = Objects.requireNonNull(getActivity()).findViewById(R.id.tv_name);
        TextView tv_phone_number = getActivity().findViewById(R.id.tv_phone_number);
        TextView tv_street_address = getActivity().findViewById(R.id.tv_street_address);
        TextView tv_city = getActivity().findViewById(R.id.tv_city);
        TextView tv_country = getActivity().findViewById(R.id.tv_country);

        if (billingAddress != null) {
            String houseNo = (billingAddress.getHouseNo() != null) ? (!billingAddress.getHouseNo().isEmpty()) ? getString(R.string.house_no) + billingAddress.getHouseNo() + "," : "" : "";
            String roomNo = (billingAddress.getRoomNo() != null) ? (!billingAddress.getRoomNo().isEmpty() ? getString(R.string.room_no) + billingAddress.getRoomNo() + "," : "") : "";
            String floorNo = (billingAddress.getFloorNo() != null) ? (!billingAddress.getFloorNo().isEmpty() ? getString(R.string.floor_no) + billingAddress.getFloorNo() + "," : "") : "";
            String addressOne = (billingAddress.getAddress1() != null) ? (!billingAddress.getAddress1().isEmpty() ? billingAddress.getAddress1() + "," : "") : "";
            String addressTwo = (billingAddress.getAddress2() != null) ? (!billingAddress.getAddress2().isEmpty() ? billingAddress.getAddress2() + "," : "") : "";
            String cityTownship = (billingAddress.getCity() != null) ? (!billingAddress.getCity().isEmpty() ? billingAddress.getCity() + "," : "") : "";
            String stateDivision = (billingAddress.getStateProvinceName() != null) ? (!billingAddress.getStateProvinceName().isEmpty() ? billingAddress.getStateProvinceName() + "," : "") : "";

            FormViews.setText(tv_name.getId(), TextUtils.getNullSafeString(billingAddress.getFirstName()) + " ", layout);
            FormViews.setText(tv_phone_number.getId(), TextUtils.getNullSafeString(AppUtils.formattedNumber(billingAddress.getPhoneNumber())), layout);
            FormViews.setText(tv_street_address.getId(), houseNo + roomNo + floorNo + addressOne + addressTwo, layout);
            FormViews.setText(tv_city.getId(), cityTownship + stateDivision, layout);
            FormViews.setText(tv_country.getId(), TextUtils.getNullSafeString(billingAddress.getCountryName()), layout);
        }
    }

    @Override
    public void onClick(View v) {
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            if (null != checkoutOrderSummaryResponse && null != checkoutOrderSummaryResponse.getOrderTotalModel()) {
                int orderTotalAmount = checkoutOrderSummaryResponse.getOrderTotalModel().getOrderTotalInt();
                if (orderTotalAmount >= checkoutOrderSummaryResponse.getOrderTotalModel().getMinOrderTotalAmount()) {
                    if (totalAmount == null || totalAmount == 0.0) {
                        showToast(getString(R.string.payment_amount_invalid));
                        return;
                    } else {
                        redirectToPaymentGateway();
                    }
                } else {
                    AppDialogs.customDialog(getActivity(), getString(R.string.low_product_amount), () -> {
                    });
                }
            }
        }
    }

    /* open payment gateway by clients */
    private void redirectToPaymentGateway() {
        if (PaymentMethodSystemName.equalsIgnoreCase(Keys.OKDOLLAR_PAYMENT))
            okDollarPayment();
        else
            card2C2PPayment();
    }

    private void okDollarPayment() {
        confirmButton.setOnClickListener(null);

        if (merchantNumber.isEmpty()) {
            showToast(getString(R.string.merchant_number_not_valid));
            return;
        }

        Log.e(TAG, "---------------merchantNumber before --------- : " + merchantNumber);

        if (merchantNumber.startsWith("0095")) {
            merchantNumber = merchantNumber.replaceFirst("0095", "0");
        }

        Log.e(TAG, "---------------merchantNumber after --------- : " + merchantNumber);

        Log.e(TAG, "---------------totalAmount to pay --------- : " + totalAmount);
        OKDollar.init(getActivity())
                .setCountryCode(Keys.MYANMAR_COUNTRY_CODE) // e.g. +95
                .setDestinationMobileNumber("09768077189") // for testing 09768077189 //merchantNumber
                .setAmount(1.0) // e.g. 1.0 //totalAmount
                .setListener(okListener)
                .build();
    }

    public void onBackPressed() {
        // when payment is going on then back press will be disable after payment done backpress btn will be work.
        Log.e(TAG, "---------- ONBACKPRESSED() CALLED -------------");
    }

    private void setLocale() {
        String preferredLanguage = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
        if (android.text.TextUtils.isEmpty(preferredLanguage)) {
            preferredLanguage = Language.MYANMAR;
            preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, preferredLanguage);
        }
        Locale preferredLocale = new Locale(preferredLanguage);
        Locale.setDefault(preferredLocale);
        Configuration configuration = new Configuration();
        configuration.locale = preferredLocale;
        Objects.requireNonNull(getActivity()).getResources().updateConfiguration(configuration, getActivity().getResources().getDisplayMetrics());
    }

    private void card2C2PPayment() {
        My2c2pSDK sdk;
        confirmButton.setOnClickListener(null);
        if (BuildConfig.isProduction) {
            sdk = new My2c2pSDK(AppConstants.MY2C2PKEY_P);
            sdk.merchantID = AppConstants.MERCHANT_ID_P;
            sdk.secretKey = AppConstants.SECRET_KEY_P;
            sdk.productionMode = true;
        } else {
            sdk = new My2c2pSDK(AppConstants.MY2C2PKEY);
            sdk.merchantID = AppConstants.MERCHANT_ID;
            sdk.secretKey = AppConstants.SECRET_KEY;
            sdk.productionMode = false;
        }
        String phoneNumber = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_MOBILE_NUMBER);
        String transactionCode = ((phoneNumber.length() < 4 ? "" : phoneNumber.substring(phoneNumber.length() - 4)) + System.currentTimeMillis());

        sdk.uniqueTransactionCode = transactionCode.substring(transactionCode.length() - 12);  // only 12 digits (MPU Mandatory)
        sdk.desc = "1 Stop Kitchen Products Details.";
        sdk.amount = totalAmount;

        sdk.paymentUI = true;
        sdk.currencyCode = "104"; // for Myanmar


        sdk.paymentOption = My2c2pSDK.PaymentOption.CREDIT_CARD;  //Refer My2c2pSDK.PaymentOption class

        //optional details
        sdk.enableStoreCard = false;
        sdk.userDefined1 = "ref1";
        sdk.userDefined2 = "ref2";
        sdk.userDefined3 = "ref3";
        sdk.userDefined4 = "ref4";
        sdk.userDefined5 = "ref5";
        sdk.request3DS = "Y";
        Intent intent = new Intent(getActivity(), My3DSActivity.class);
        intent.putExtra(My2c2pSDK.PARAMS, sdk);
        if (null != getActivity()) {
            getActivity().startActivityForResult(intent, REQUEST_SDK);
        }
    }

    /**
     * @akash -: Only Success and pending state , we are placing the order and generate the receipt page .
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IsPaymentApiRunning = true;

        if (Activity.RESULT_CANCELED == resultCode) {
            Log.e("##########:", "------Transaction is canceled: " + ":requestCode::" + requestCode + ":");
            showToast(getString(R.string.payment_denied));
            confirmButton.setOnClickListener(this);
            IsPaymentApiRunning = false;
        } else if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                My2c2pResponse my2c2pResponse = data.getParcelableExtra(My2c2pResponse.RESPONSE);

                if (my2c2pResponse.getRespCode().equals("301")) {
                    PaymentStatus = "Failure";
                    Log.e("#######", "Transaction is canceled:");
                    showToast("Transaction is canceled");
                    IsPaymentApiRunning = false;
                    confirmButton.setOnClickListener(this);
                    return;
                }

                String response = new Gson().toJson(my2c2pResponse);
                Log.e("#######", "My2c2pResponse JSON # ->  " + response);

                if (my2c2pResponse.getStatus().equalsIgnoreCase("A") || my2c2pResponse.getStatus().equalsIgnoreCase("000")) {
                    transactionStatusCode = "000";
                } else if (my2c2pResponse.getStatus().equalsIgnoreCase("F") || my2c2pResponse.getStatus().equalsIgnoreCase("002")) {
                    transactionStatusCode = "002";
                    showToast(my2c2pResponse.getFailReason());
                    IsPaymentApiRunning = false;
                    confirmButton.setOnClickListener(this);
                    return;
                } else {
                    transactionStatusCode = my2c2pResponse.getStatus();
                }
                PaymentStatus = my2c2pResponse.getFailReason();
                transactionId = my2c2pResponse.getUniqueTransactionCode();
                if (progressBar != null)
                    progressBar.setVisibility(View.VISIBLE);

                placedOrderConfirmed(transactionId);

            } else {
                PaymentStatus = "Failure";
                Log.e(TAG, " #### Transaction is canceled Because Data is Null! #####");
                showToast(getString(R.string.payment_denied));
                IsPaymentApiRunning = false;
                confirmButton.setOnClickListener(this);
            }
        }
    }

    private void placedOrderConfirmed(String transactionId) {
        SaveTransactionStatusModel request = new SaveTransactionStatusModel(totalAmount, PaymentMethodSystemName, PaymentStatus, transactionId, transactionStatusCode);
        if (getActivity() != null) {
            confirmViewModel = ViewModelProviders.of(getActivity()).get(ConfirmOrderViewModel.class);
            Objects.requireNonNull(confirmViewModel.getPlaceOrder(request)).observe(getActivity(), this::placeOrderResponse);
        } else {
            Log.e("ConfirmOrderFragment: ", "---------- activity obj is null ---------");
        }
    }

    private void placeOrderResponse(PaymentResponseModel response) {
        if (null != response) {
            if (response.getStatusCode() == 200) {
                if (null != response.getOrders()) {
                    if (null != progressBar)
                        progressBar.setVisibility(View.GONE);
//                    Log.e(TAG, "##----------onEvent PaymentResponseModel---: " + response.toString());
                    Fragment fragment = new PaymentDoneSuccessfullyFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Keys.PAYMENT_DONE_RESPONSE, response);
                    fragment.setArguments(bundle);
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
                    }
                    Utility.setCartCounter(0);
                    IsPaymentApiRunning = false;
                    if (null != getActivity())
                        showToast(getActivity().getResources().getString(R.string.payment_done_successfully));
                } else {
                    if (null != getActivity())
                        showToast(getString(R.string.call_to_customer_care));
                }

            } else {
                if (null != progressBar)
                    progressBar.setVisibility(View.GONE);
                IsPaymentApiRunning = false;
                if (null != response.getErrorList() && response.getErrorList().length > 0) {
                    showToast(response.getErrorList()[0]);
                    AppDialogs.customDialog(getActivity(), response.getErrorList()[0], () -> {
                    });
                } else {
                    showToast(getString(R.string.call_to_customer_care));
                }
            }
        }
    }
}
