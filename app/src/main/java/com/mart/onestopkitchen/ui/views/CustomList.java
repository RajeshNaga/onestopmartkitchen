package com.mart.onestopkitchen.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ExpandableListView;


public class CustomList extends ExpandableListView {

    public CustomList(Context context) {
        super(context);
    }

    public CustomList(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}

/*   //999999 is a size in pixels. ExpandableListView requires a maximum height in order to do measurement calculations.
//            heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999, MeasureSpec.AT_MOST);

//        widthMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.EXACTLY);*/
