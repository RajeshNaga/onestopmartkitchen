package com.mart.onestopkitchen.ui.fragment.submenu.adapter.fragmentadapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.addsubmenu.CategeryItemsItem;
import com.mart.onestopkitchen.model.addsubmenu.MainItemsItem;

import java.util.List;

public class AddSubMenuLeftAdapter extends RecyclerView.Adapter<AddSubMenuLeftAdapter.ViewHolder> {
    List<MainItemsItem> children;
    private OnViewClick onViewClick;
    private Context mContext;

    public AddSubMenuLeftAdapter(OnViewClick onViewClick, Context mContext, List<MainItemsItem> children) {
        this.onViewClick = onViewClick;
        this.mContext = mContext;
        this.children = children;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sub_menu_new, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(children.get(position).getCategeryName());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onViewClick.viewClicked(children.get(position).getCategeryItems(), children.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    public interface OnViewClick {
        void viewClicked(List<CategeryItemsItem> pos, MainItemsItem mainItemsItem);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt_title);
        }
    }
}
