package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TailoringSizesItem {

    @SerializedName("size")
    private String size;

    @SerializedName("Data")
    private List<DataItem> data;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }
}