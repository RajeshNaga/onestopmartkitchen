package com.mart.onestopkitchen.ui.fragment.shareyourinfo.model;

public class ManuallSize {
    private String title = "";
    private String edtValue = "";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdtValue() {
        return edtValue;
    }

    public void setEdtValue(String edtValue) {
        this.edtValue = edtValue;
    }
}
