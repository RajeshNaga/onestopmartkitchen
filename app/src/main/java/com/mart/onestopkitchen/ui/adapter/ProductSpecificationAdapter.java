package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ProductSpecification;

import java.util.List;

/**
 * Created by Hemnath on 12/14/2018.
 */
public class ProductSpecificationAdapter extends RecyclerView.Adapter<ProductSpecificationAdapter.ProductSpecificationHolder> {

    List<ProductSpecification> productSpecifications;

    Context context;

    public ProductSpecificationAdapter(Context context, List<ProductSpecification> productSpecifications) {
        this.context = context;
        this.productSpecifications = productSpecifications;
    }

    @NonNull
    @Override
    public ProductSpecificationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_product_specification, viewGroup, false);
        return new ProductSpecificationAdapter.ProductSpecificationHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductSpecificationHolder productSpecificationHolder, int i) {
        productSpecificationHolder.tvSpecificationHeader.setText(productSpecifications.get(i).getName());
        productSpecificationHolder.tvSpecificationDescription.setText(productSpecifications.get(i).getValue().replace("&quot;", "''"));
    }

    @Override
    public int getItemCount() {
        return productSpecifications.size();
    }

    class ProductSpecificationHolder extends RecyclerView.ViewHolder {

        TextView tvSpecificationHeader;
        TextView tvSpecificationDescription;

        ProductSpecificationHolder(View itemView) {
            super(itemView);
            tvSpecificationHeader = itemView.findViewById(R.id.tv_spec_header);
            tvSpecificationDescription = itemView.findViewById(R.id.tv_spec_description);
        }
    }
}
