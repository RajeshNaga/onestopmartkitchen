package com.mart.onestopkitchen.ui.selectcountry.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NetworksItem implements Serializable {

    @SerializedName("Status")
    private String status;

    @SerializedName("NetworkName")
    private String networkName;

    @SerializedName("OperatorName")
    private String operatorName;

    @SerializedName("MNC")
    private String mNC;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getMNC() {
        return mNC;
    }

    public void setMNC(String mNC) {
        this.mNC = mNC;
    }
}