package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.event.RemoveWishlistItemEvent;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.WishistUpdateResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.CartProductListResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.adapter.WishListAdapter;
import com.mart.onestopkitchen.ui.customview.CustomLinearLayoutManager;
import com.mart.onestopkitchen.utils.Language;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mart-110 on 12/24/2015.
 */
public class WishListFragment extends BaseFragment implements WishListAdapter.OnProductSelectListener {
    @BindView(R.id.btn_add_all_items_to_cart)
    Button addAllItemsToCartBtn;

    @BindView(R.id.rclv_wish_list)
    RecyclerView wishRecyclerList;

    CustomLinearLayoutManager layoutManager;

    @BindView(R.id.ll_noWishListItemLayout)
    LinearLayout noItemLayoput;

    @BindView(R.id.btn_continue_shopping)
    Button continueShopping;

    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerFrameLayout;

    private List<CartProduct> wishListProduct = null;
    private WishListAdapter wishListAdapter = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.wishlist));
        setLayoutManagerofRecyclerList();
        checkEventBusRegistration();
        //callWebservice();
        wishRecyclerList.setNestedScrollingEnabled(false);
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            wishRecyclerList.setRotationY(180);
        }
        addAllItemsToCartBtn.setOnClickListener(view1 -> addItemsToCart());
        continueShopping.setOnClickListener(view12 -> {
            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        callWebservice();
    }

    public void onEvent(WishistUpdateResponse response) {
        if (response.getStatusCode() != 200) {
            showSnack(getString(R.string.error_removing_item));
        } else {
            showSnack(getString(R.string.item_removed));
            populatedDatainAdapter(response.getItems());
        }
    }

    public void callWebservice() {
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmer();
        RetroClient.getApi().getWishList().enqueue(new CustomCB<CartProductListResponse>(this.getView()));
    }

    public void onEvent(CartProductListResponse cartProductListResponse) {
        if (cartProductListResponse != null && cartProductListResponse.getItems() != null) {
            if (cartProductListResponse.getItems().size() > 0) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                noItemLayoput.setVisibility(View.GONE);
                populatedDatainAdapter(cartProductListResponse.getItems());
            } else {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                noItemLayoput.setVisibility(View.VISIBLE);
                wishListAdapter = new WishListAdapter(getActivity(), new ArrayList(), this, preferenceService, this);
                wishRecyclerList.setAdapter(wishListAdapter);
                wishRecyclerList.setNestedScrollingEnabled(false);
                addAllItemsToCartBtn.setVisibility(View.GONE);
                Snackbar.make(getView(), R.string.wishlist_empty, Snackbar.LENGTH_SHORT).show();
            }
            if (cartProductListResponse.getCount() > 0)
                Utility.setCartCounter(cartProductListResponse.getCount());
        }

    }

    private void addItemsToCart() {
        if (wishListProduct.size() > 0) {
            callWebService();
        }
    }

    private void callWebService() {
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        for (CartProduct cp : wishListProduct) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey("addtocart");
            keyValuePair.setValue(cp.getId() + "");
            keyValuePairs.add(keyValuePair);
        }

        RetroClient.getApi().addAllItemsToCartFromWishList(keyValuePairs)
                .enqueue(new CustomCB<CartProductListResponse>(getView()));
    }

    private void populatedDatainAdapter(List<CartProduct> cartProductList) {
        this.wishListProduct = new ArrayList<>();
        this.wishListProduct.addAll(cartProductList);
        wishListAdapter = new WishListAdapter(getActivity(), this.wishListProduct, this, preferenceService, this);
        wishRecyclerList.setAdapter(wishListAdapter);

    }

    private void setLayoutManagerofRecyclerList() {
        layoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        wishRecyclerList.setHasFixedSize(true);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        wishRecyclerList.setLayoutManager(layoutManager);
    }

    public void onEvent(RemoveWishlistItemEvent event) {
        if (event.getCount() == 0) {
            addAllItemsToCartBtn.setVisibility(View.GONE);
            showSnack(getString(R.string.wishlist_empty));
        }
    }

    @Override
    public void onItemSelect(int pId, @NotNull String name) {
        ProductModel productModel = new ProductModel();
        productModel.setId(pId);
        productModel.setName("");
        ProductDetailFragment.productModel = productModel;
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                .replace(R.id.container, new ProductDetailFragment()).addToBackStack(null).commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != wishListAdapter) {
            wishListAdapter = null;
        }
    }



    /*

    public void onEvent(AddItemsToCartFromWishlistResponse response)
    {
        if(response.getStatusCode() == 200){
            Utility.setCartCounter(response.getCount());
          //  populatedDatainAdapter(new ArrayList<CartProduct>());
           // addAllItemsToCartBtn.setVisibility(View.GONE);
            showSnack("Item added to cart");
        } else {
            showSnack("Something went wrong. Please try again later");
        }

    }

    public  void onEvent(AddAllItemsToCartFromWishlistResponse response)
    {
        if(response.getStatusCode() == 200){

            Utility.setCartCounter(response.getCount());
            showSnack("Items added to cart");
            addAllItemsToCartBtn.setVisibility(View.GONE);
            wishListAdapter=new WishListAdapter(getActivity(),new ArrayList(),this);
             wishRecyclerList.setAdapter(wishListAdapter);
            Snackbar.make(getView(), "Wishlist is Empty", Snackbar.LENGTH_SHORT).show();


        } else {
            showSnack("Something went wrong. Please try again later");
        }
    }
   @InjectView(R.id.table_orderTotal)
    TableLayout orderSummaryRelativeLayout;

    @InjectView(R.id.cv_orderTotal)
    CardView orderTotalCardView;

    @InjectView(R.id.ll_order_totla)
    LinearLayout orderToalLinearLayout;

    @InjectView(R.id.ll_cartInfoLayout)
    LinearLayout CartInfoLinearLayout;

    @InjectView(R.id.coupon_layout)
    RelativeLayout couponLayout;

    @InjectView(R.id.btn_proceed_to_Checkout)
    Button checkoutBtn;

    @InjectView(R.id.rclv_cartList)
    RecyclerView cartproductRecyclerList;

    @InjectView(R.id.cv_product_attribute)
    CardView productAttributeCardView;

    CustomLinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Wishlist");
        setLayoutManagerofRecyclerList();

        checkEventBusRegistration();
        callWebservice();

        orderTotalCardView.setVisibility(View.GONE);
        couponLayout.setVisibility(View.GONE);
        checkoutBtn.setText("ADD TO CART");

        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemsToCart();
            }
        });

    }

    public void onEvent(WishistUpdateResponse response){
        if(response.getStatusCode() != 200){
            showSnack("Error removing item. Check your internet connection.");
        } else {

        }
    }

    public void callWebservice() {
        RestClient.get().getWishlist(new CustomCallback<CartProductListResponse>(this.getView()));
    }

    public void onEvent(CartProductListResponse cartProductListResponse)
    {
        if(cartProductListResponse!=null && cartProductListResponse.getItems()!=null) {
            if (cartProductListResponse.getItems().size() > 0) {
                populatedDatainAdapter(cartProductListResponse.getItems());
                CartInfoLinearLayout.setVisibility(View.VISIBLE);
                couponLayout.setVisibility(View.GONE);
                orderTotalCardView.setVisibility(View.GONE);
                productAttributeCardView.setVisibility(View.GONE);
            } else {
                Utility.setCartCounter(0);
                Snackbar.make(getView(), "Wishlist is Empty", Snackbar.LENGTH_SHORT).show();
            }
        }

    }

    private void addItemsToCart(){
        if(wishListProduct.size() > 0) {
            callWebService();
        }
    }

    private void callWebService(){
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        for (CartProduct cp: wishListProduct) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey("addtocart");
            keyValuePair.setValue(cp.getId()+"");
            keyValuePairs.add(keyValuePair);
        }
        RestClient.get().addItemsToCartFromWishlist(keyValuePairs, new CustomCallback<AddItemsToCartFromWishlistResponse>(getView()));
    }

    public void onEvent(AddItemsToCartFromWishlistResponse response)
    {
        if(response.getStatusCode() == 200){
            Utility.setCartCounter(response.getCount());
            populatedDatainAdapter(new ArrayList<CartProduct>());
            checkoutBtn.setVisibility(View.GONE);
            showSnack("Items added to cart");
        } else {
            showSnack("Something went wrong. Please try again later");
        }

    }

    List<CartProduct> wishListProduct;
    WishListAdapter wishListAdapter;
    public void populatedDatainAdapter(List<CartProduct> wishListProduct)
    {
        this.wishListProduct = new ArrayList<>();
        this.wishListProduct.addAll(wishListProduct);
        wishListAdapter=new WishListAdapter(getActivity(),this.wishListProduct,this);
        cartproductRecyclerList.setAdapter(wishListAdapter);

    }

    private void setLayoutManagerofRecyclerList()
    {
        layoutManager=new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL,false);
        cartproductRecyclerList.setHasFixedSize(true);
        cartproductRecyclerList.setLayoutManager(layoutManager);
    }

    public void onEvent(RemoveWishlistItemEvent event){
        if(event.getCount() == 0) {
            checkoutBtn.setVisibility(View.GONE);
            showSnack("Wishlist is empty");
        }
    }
*/
}
