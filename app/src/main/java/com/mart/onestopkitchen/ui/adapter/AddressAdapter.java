package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BillingAddress;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.fragment.BillingAddressFragment;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.UiUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<BillingAddress> existingAddressList;

    public AddressAdapter(Context context, List<BillingAddress> existingAddressList) {
        mContext = context;
        this.existingAddressList = existingAddressList;
        inflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public AddressAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.address_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull AddressAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (existingAddressList != null && existingAddressList.size() > 0) {
            BillingAddress address = existingAddressList.get(position);
            if ((existingAddressList.size() - 1) == position) {
                holder.ll_address_item.setBackground(mContext.getDrawable(R.drawable.add_btn_gray_border));
                holder.tvAddress.setText(existingAddressList.get(position).getAddress1());
                holder.tvAddress.setTypeface(UiUtils.getFont(mContext), Typeface.BOLD);
                holder.tvUsername.setVisibility(View.GONE);
                holder.tvMobile.setVisibility(View.GONE);
                holder.tvAddress.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                holder.tvAddress.setVisibility(View.VISIBLE);
                holder.tvAddress.setTextSize(mContext.getResources().getDimension(R.dimen._6sdp));
//                setMargins(holder.ll_address_item, (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
//                        (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
            } else {
                if (address != null) {
                    String houseNo = !AppUtils.cutNull(address.getHouseNo()).isEmpty() ? mContext.getString(R.string.house_no) + address.getHouseNo() + "," : "";
                    String roomNo = !AppUtils.cutNull(address.getRoomNo()).isEmpty() ? mContext.getString(R.string.room_no) + address.getRoomNo() + "," : "";
                    String floorNo = !AppUtils.cutNull(address.getFloorNo()).isEmpty() ? mContext.getString(R.string.floor_no) + address.getFloorNo() + "," : "";
                    holder.tvUsername.setText(existingAddressList.get(position).getFirstName());
                    holder.tvAddress.setText(houseNo + roomNo + floorNo + address.getAddress1() + " " + address.getAddress2() + " " + address.getCity() + " " + address.getStateProvinceName());
                    holder.tvMobile.setText(AppUtils.formattedNumber(existingAddressList.get(position).getPhoneNumber()));
                }
            }

            holder.ll_address_item.setOnClickListener(v -> {
                if (existingAddressList != null) {
                    if ((existingAddressList.size() - 1) == position) {
                        EventBus.getDefault().post(new BillingAddressFragment());
                    } else {
                        if (existingAddressList.get(position).isDeliveryAllowed())
                            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, String.valueOf(existingAddressList.get(position).getId()));
                        EventBus.getDefault().post(existingAddressList.get(position));
                    }
                }
            });
        }
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            final float scale = mContext.getResources().getDisplayMetrics().density;
            int l = (int) (left * scale + 0.5f);
            int r = (int) (right * scale + 0.5f);
            int t = (int) (top * scale + 0.5f);
            int b = (int) (bottom * scale + 0.5f);
            p.setMargins(l, t, r, b);
            view.requestLayout();
        }
    }

    @Override
    public int getItemCount() {
        return existingAddressList.size() > 0 ? existingAddressList.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.tv_mobile_number)
        TextView tvMobile;
        @BindView(R.id.ll_address_item)
        LinearLayout ll_address_item;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
