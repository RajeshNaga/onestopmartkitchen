package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeProductAdapterRecent extends RecyclerView.Adapter<HomeProductAdapterRecent.ViewHolder> {
    private Context mContext;
    private List<ProductModel> recentList;
    private RecentProductClick clickEvent;

    public HomeProductAdapterRecent(Context context, RecentProductClick click) {
        this.mContext = context;
        this.recentList = new ArrayList<>();
        this.clickEvent = click;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_homepage_product_recent_home, viewGroup, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            ProductModel productModel = recentList.get(i);
            holder.onItemBind(productModel);
            holder.tvProductName.setText(AppUtils.capitalizeWords(productModel.getName()));
            if (productModel.getProductPrice().getDiscountPercentage() > 0) {
                holder.tvOfferPercentage.setVisibility(View.VISIBLE);
                holder.tvOfferPercentage.setText(productModel.getProductPrice().getDiscountPercentage() + "%");
            } else
                holder.tvOfferPercentage.setVisibility(View.GONE);

            if (null != productModel.getProductPrice().getPriceWithDiscount() && !productModel.getProductPrice().getPriceWithDiscount().isEmpty()) {
                holder.tvProductOldPrice.setVisibility(View.VISIBLE);
                holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), productModel.getProductPrice().getPriceWithDiscount(), 0));
                holder.tvProductOldPrice.setText(AppUtils.getMMKString(holder.tvProductOldPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));

            } else if (null != productModel.getProductPrice().getOldPrice()) {
                holder.tvProductOldPrice.setVisibility(View.VISIBLE);
                holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                holder.tvProductOldPrice.setText(AppUtils.getMMKString(holder.tvProductOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));

            } else {
                holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                holder.tvProductOldPrice.setText(AppUtils.getMMKString(holder.tvProductOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));
                holder.tvProductOldPrice.setVisibility(View.GONE);
            }

            if (null != holder.gifMarkNew) {
                if (productModel.getMarkAsNew()) {
                    holder.gifMarkNew.setVisibility(View.VISIBLE);
                    Glide
                            .with(mContext)
                            .load(R.drawable.new_3)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.gifMarkNew);
                } else {
                    holder.gifMarkNew.setVisibility(View.GONE);
                }
            }
            Glide
                    .with(mContext)
                    .load(productModel.getDefaultPictureModel().getImageUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProductImage);

        } catch (Exception ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return recentList == null ? 0 : recentList.size();
    }

    public void loadData(List<ProductModel> recentList) {
        this.recentList = recentList;
        notifyDataSetChanged();
    }

    public interface RecentProductClick {
        void productClick(ProductModel dataItem);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_productImage)
        ImageView imgProductImage;
        @BindView(R.id.tv_productName)
        TextView tvProductName;
        @BindView(R.id.tv_productPrice)
        TextView tvProductPrice;
        @BindView(R.id.tv_productOldPrice)
        TextView tvProductOldPrice;
        @BindView(R.id.tv_offerPercentage)
        TextView tvOfferPercentage;
        @BindView(R.id.gif_mark_new)
        ImageView gifMarkNew;

        private ProductModel dataItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvProductOldPrice.setPaintFlags(tvProductOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        @OnClick(R.id.lin_root)
        public void onViewClicked(View view) {
            if (view.getId() == R.id.lin_root) {
                clickEvent.productClick(dataItem);
            }
        }

        public void onItemBind(ProductModel productModel) {
            this.dataItem = productModel;
        }
    }
}

