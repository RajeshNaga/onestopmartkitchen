package com.mart.onestopkitchen.ui.fragment.addaddres;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.materialspinner.MaterialSpinner;
import com.mart.onestopkitchen.model.AvailableCountry;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.fragment.BaseFragment;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by
 */
public class CustomerAddAddressFragmentNew extends BaseFragment implements ViewTreeObserver.OnGlobalLayoutListener, CustomerAddressListener {


    private static final String TAG = CustomerAddAddressFragmentNew.class.getSimpleName();
    @BindView(R.id.rel_root)
    RelativeLayout relRoot;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.spinner_country)
    MaterialSpinner spinnerCountry;
    @BindView(R.id.spinner_state)
    MaterialSpinner spinnerState;
    @BindView(R.id.spinner_city)
    MaterialSpinner spinnerCity;
    @BindView(R.id.lin_enter_city)
    LinearLayout linEnterCity;
    @BindView(R.id.edt_address_one)
    EditText edtAddressOne;
    @BindView(R.id.edt_address_two)
    EditText edtAddressTwo;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    private ViewTreeObserver observer;
    private MainActivity activity;
    private Context mContext;
    private CustomerAddressPresenter presenter;

    private static String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_add_address_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        activity = (MainActivity) getActivity();
        mContext = getContext();
        presenter = new CustomerAddressPresenter(this);
        presenter.apiCall(1);
        observer = relRoot.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(this);

    }

    @Override
    public void onGlobalLayout() {
        try {
            Rect r = new Rect();
            relRoot.getWindowVisibleDisplayFrame(r);
            int screenHeight = relRoot.getRootView().getHeight();
            int keypadHeight = screenHeight - r.bottom;
            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                // keyboard is opened
                btnSubmit.setVisibility(View.GONE);

            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnSubmit.setVisibility(View.VISIBLE);

                    }
                }, 100);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (observer != null)
                observer.removeGlobalOnLayoutListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        activity.hideKeyboard();
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                btnAction();
                break;
        }
    }

    private void btnAction() {
        if (validation())
            presenter.apiCall(2);
    }

    private boolean validation() {
        if (cutNull(username.getText().toString()).isEmpty()) {
            activity.showNormalToast(getString(R.string.please_enter_name));
            return false;
        } else if (spinnerCountry.getSelectedIndex() == 0) {
            activity.showNormalToast(getString(R.string.please_select_country));
            return false;
        } else if (spinnerState.getSelectedIndex() == 0) {
            activity.showNormalToast(getString(R.string.please_select_state));
            return false;
        } else if (spinnerCity.getSelectedIndex() == 0) {
            activity.showNormalToast(getString(R.string.please_select_city));
            return false;
        } else if (cutNull(edtAddressOne.getText().toString()).isEmpty()) {
            activity.showNormalToast(getString(R.string.please_enter_address_one));
            return false;
        }
        return true;
    }

    @Override
    public Context getActivityContext() {
        return mContext;
    }

    @Override
    public void showLoading() {
        AppDialogs.loadingDialog(mContext, true, "");
    }

    @Override
    public void hideLoading() {
        AppDialogs.dismissDialog();
    }

    @Override
    public void showError(String s) {

    }


    @Override
    public void successFromServerGetAddress(BillingAddressResponse addressResponse) {
        //    if (addressResponse.getNewAddress().getAvailableCountries() != null && addressResponse.getNewAddress().getAvailableCountries().size() > 1
        //            && addressResponse.getNewAddress().getAvailableStates() != null && addressResponse.getNewAddress().getAvailableStates().size() > 1
        //            && addressResponse.getNewAddress().getAvailableCountries() != null && addressResponse.getNewAddress().getAvailableCountries().size() > 1) {
        loadSpinnerItem(addressResponse);
        //    }
    }

    private void loadSpinnerItem(BillingAddressResponse addressResponse) {
        /*ArrayList<String> city = new ArrayList<>(0);
        for (AvailableCountry get : addressResponse.getNewAddress().getAvailableCities())
            if (get.getText() != null && !get.getText().isEmpty())
                city.add(get.getText());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.simple_spinner_item_black_color, city);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCountry.setAdapter(adapter);
*/
        ArrayList<String> state = new ArrayList<>(0);
        for (AvailableCountry get : addressResponse.getNewAddress().getAvailableStates())
            if (get.getText() != null && !get.getText().isEmpty())
                state.add(get.getText());

        // ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(mContext, R.layout.simple_spinner_item_new, state);
        //  stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  state.setAdapter(stateAdapter);
        spinnerState.setItems(state);
        ArrayList<String> country = new ArrayList<>(0);
        for (AvailableCountry get : addressResponse.getNewAddress().getAvailableCountries())
            if (get.getText() != null && !get.getText().isEmpty())
                country.add(get.getText());
        spinnerCountry.setItems(country);
        //   ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(mContext, R.layout.simple_spinner_item_new, country);
        //   stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinnerCountry.setAdapter(countryAdapter);

    }

    @Override
    public String userName() {
        return cutNull(username.getText().toString());
    }

    @Override
    public String addressOne() {
        return cutNull(edtAddressOne.getText().toString());
    }

    @Override
    public String addressTwo() {
        return cutNull(edtAddressTwo.getText().toString());
    }

    @Override
    public String countryId() {
        return String.valueOf(spinnerCountry.getSelectedIndex());
    }

    @Override
    public String stateId() {
        return String.valueOf(spinnerState.getSelectedIndex());
    }

    @Override
    public String cityId() {
        return String.valueOf(spinnerCity.getSelectedIndex());
    }
}



