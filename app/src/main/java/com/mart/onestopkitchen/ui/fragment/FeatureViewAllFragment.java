package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.constants.ProductSort;
import com.mart.onestopkitchen.model.AvailableSortOption;
import com.mart.onestopkitchen.model.BaseProductModel;
import com.mart.onestopkitchen.model.DefaultPictureModel;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.ProductPrice;
import com.mart.onestopkitchen.model.featureproduct.DataItem;
import com.mart.onestopkitchen.model.featureproduct.FeatureProductResponseModel;
import com.mart.onestopkitchen.model.filterslection.GroupDataItem;
import com.mart.onestopkitchen.model.filterslection.UserSelectionResponseModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.FilterMainActivityNew;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.UserSelectedList;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.BaseResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.AddtoCartResponse;
import com.mart.onestopkitchen.networking.response.HomePageManufacturerResponse;
import com.mart.onestopkitchen.networking.response.HomePageProductResponse;
import com.mart.onestopkitchen.networking.response.ProductsResponse;
import com.mart.onestopkitchen.products.view.ProductListFragment;
import com.mart.onestopkitchen.rxjava.ApiHelper;
import com.mart.onestopkitchen.rxjava.RxJavaUtils;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.utils.AppUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.functions.Consumer;

import static com.mart.onestopkitchen.ui.fragment.ProductListFragmentFor3_8.CATEGORY_ID;
import static com.mart.onestopkitchen.utils.AppConstants.FILTER_OPTION_INTENT;


public class FeatureViewAllFragment extends Fragment implements ViewAllAdapter.OnViewClick, ViewAllAdapterFeature.OnViewClickFeature {

    @BindView(R.id.bottomsheet)
    BottomSheetLayout bottomSheetLayout;
    @BindView(R.id.rel)
    RelativeLayout rel;
    @BindView(R.id.rv_view_all)
    RecyclerView rvViewAll;
    @BindView(R.id.rl_sortby)
    RelativeLayout rl_sortby;
    @BindView(R.id.rl_filter)
    RelativeLayout rl_filter;
    @BindView(R.id.txt_norecord)
    TextView txt_norecord;

    @BindView(R.id.grid_view)
    ImageView gridView;
    @BindView(R.id.grid_view_border)
    View gridViewBorder;

    Unbinder unbinder;
    HomePageProductResponse homePageProductResponse;
    private ViewAllAdapter viewAllAdapter;
    private ViewAllAdapterFeature viewAllAdapterFeature;
    private HomePageManufacturerResponse homePageManufacturerResponse;
    private FeatureProductResponseModel responseModel;
    private List<AvailableSortOption> availableSortOptions = new ArrayList<>();
    private ArrayAdapter<String> sortAdapter;
    private int selectedPosition = -1;
    private ProductsResponse productsResponse;
    private ArrayList<Integer> productIds;
    private boolean isListView;
    private GridLayoutManager gridLayoutManager;
    private int alreadySortClicked = -1;
    private boolean isFilterClicked;
    private int minPriceSel = 0, maxPriceSel = 0;
    private boolean isFilterClickSingleTime;
    private ArrayList<UserSelectionResponseModel> userSelectionResponseModelList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Object o = (Object) getArguments().getSerializable("data");
            responseModel = (FeatureProductResponseModel) getArguments().getSerializable("filter");
            if (o instanceof HomePageProductResponse) {
                homePageProductResponse = (HomePageProductResponse) o;
                String s = new Gson().toJson(responseModel);
                productsResponse = new Gson().fromJson(s, ProductsResponse.class);
                for (int i = 0; i < productsResponse.getProducts().size(); i++) {
                    productsResponse.getProducts().get(i).setDisplayOrder(i);
                }
            } else
                homePageManufacturerResponse = (HomePageManufacturerResponse) o;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feature_view_all, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.feature_product));
        initUi();
    }

    private void initUi() {
        if (homePageProductResponse != null && homePageProductResponse.getData() != null && homePageProductResponse.getData().size() > 0) {
            viewAllAdapter = new ViewAllAdapter(this, getContext(), homePageProductResponse.getData());
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
            rvViewAll.setLayoutManager(gridLayoutManager);
            rvViewAll.setAdapter(viewAllAdapter);
            rvViewAll.setNestedScrollingEnabled(false);
        } else {
            if (null != homePageManufacturerResponse.getData()) {
                viewAllAdapterFeature = new ViewAllAdapterFeature(this, getContext(), homePageManufacturerResponse.getData());
                rvViewAll.setLayoutManager(new GridLayoutManager(getContext(), 2));
                rvViewAll.setAdapter(viewAllAdapterFeature);
                rvViewAll.setNestedScrollingEnabled(false);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void addToCart(long id) {
        List<KeyValuePair> productAttributes = new ArrayList<>();

        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey("addtocart_" + id + ".EnteredQuantity");
        keyValuePair.setValue("1");
        productAttributes.add(keyValuePair);

        RetroClient.getApi()
                .addProductIntoCart(id, 2, productAttributes)
                .enqueue(new CustomCB<AddtoCartResponse>(rel));
    }

    @Override
    public void removeCart(long id) {
        RetroClient.getApi()
                .removeProductFromWishList(String.valueOf(id), "2")
                .enqueue(new CustomCB<BaseResponse>(rel));
    }

    @Override
    public void OnViewClicked(BaseProductModel productModel) {
        if (productModel != null) {
            gotoProductListFragment(productModel);
        }
    }

    private void gotoProductListFragment(BaseProductModel baseProductModel) {
        ProductDetailFragment.productModel = (ProductModel) baseProductModel;
        getFragmentManager().beginTransaction()
//                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog,R.anim.fade_in_dialog,R.anim.fade_out_dialog)
                .add(R.id.container, new ProductDetailFragment()).
                addToBackStack(null).commit();
    }

    @Override
    public void OnViewClickedFeature(BaseProductModel productModel) {
        //  ProductDetailFragment.productModel = (ProductModel) productModel;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                .replace(R.id.container, ProductListFragment.getInstance(Integer.parseInt(String.valueOf(productModel.getId())), HomePageFragment.class.getSimpleName() + "feature", productModel.getName())).addToBackStack(null).commit();

    }

    @OnClick({R.id.rl_sortby, R.id.rl_filter, R.id.grid_view})
    public void showSortByView(View view) {
        switch (view.getId()) {
            case R.id.rl_sortby:
                if (viewAllAdapter.getItemCount() > 0)
                    sortAction();
                else
                    Toast.makeText(getContext(), getString(R.string.no_orders_found), Toast.LENGTH_SHORT).show();
                break;
            case R.id.rl_filter:
                filterAction();
                break;
            case R.id.grid_view:
                gridAndListOption();
                break;
        }

    }

    private void gridAndListOption() {
        if (!isListView) {
            gridView.setImageResource(R.drawable.grid);
            isListView = true;
            rvViewAll.setLayoutManager(new LinearLayoutManager(getActivity()));
            viewAllAdapter.notifyDataSetChanged();
        } else {
            rvViewAll.setLayoutManager(gridLayoutManager);
            viewAllAdapter.notifyDataSetChanged();
            gridView.setImageResource(R.drawable.list);
            isListView = false;
        }
    }

    private void filterAction() {

        if (productsResponse != null && productsResponse.getProducts() != null && productsResponse.getProducts().size() > 0 && !isFilterClickSingleTime) {
            Intent intent = new Intent(getContext(), FilterMainActivityNew.class);
            Bundle bundle = new Bundle();
            ((MainActivity) getActivity()).hideKeyboard();
            bundle.putSerializable(CATEGORY_ID, productsResponse);
            minPriceSel = (int) Math.round(Double.parseDouble(String.valueOf(productsResponse.getPriceRange().getFrom()).replaceAll("[a-zA-Z(),]", "")));
            maxPriceSel = (int) Math.round(Double.parseDouble(String.valueOf(productsResponse.getPriceRange().getTo()).replaceAll("[a-zA-Z(),]", "")));
            intent.putExtras(bundle);
            isFilterClickSingleTime = true;
            startActivityForResult(intent, FILTER_OPTION_INTENT);
        }
    }

    private void sortAction() {
        availableSortOptions.clear();
        availableSortOptions.addAll(productsResponse.getAvailableSortOptions());
        if (availableSortOptions != null) {
            LinearLayout sortLinearLayout = (LinearLayout) getLayoutInflater().
                    inflate(R.layout.list_sort_by, bottomSheetLayout, false);
            ListView sortListView = sortLinearLayout.findViewById(R.id.lv_sortby);
            bottomSheetLayout.showWithSheetView(sortLinearLayout);
            sortListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            sortAdapter = new ArrayAdapter<String>(getContext(),
                    R.layout.simple_list_item_single_choice, ProductSort.getSortOptionTextList(availableSortOptions));

            sortListView.setAdapter(sortAdapter);
            if (selectedPosition >= 0) {
                sortListView.setItemChecked(selectedPosition, true);
            }

            sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String value = availableSortOptions.get(position).getValue();
                    selectedPosition = position;
                    alreadySortClicked = position;
                    viewAllAdapter.loadData(doSortOption(position));
                    bottomSheetLayout.dismissSheet();
                    sortAndFilterColorChanged();

                }
            });
        }
    }

    private void sortByDate() {
        try {
            Collections.sort(viewAllAdapter.homePageProductResponses, new Comparator<ProductModel>() {
                @SuppressLint("SimpleDateFormat")
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null && productModel.getProductPrice() == null)
                        return 0;
                    String sDate1 = productModel.getCreatedOn().replace("T", " ");//2018-02-19T10:35:25
                    String date2 = t1.getCreatedOn().replace("T", " ");
                    java.util.Date date1 = null;
                    Date date2s = null;
                    try {
                        date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(sDate1);
                        date2s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(date2);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (date1 != null && date2s != null) {
                        return date2s.compareTo(date1);
                    } else return 0;
                }
            });
        } catch (Exception ignored) {

        }
    }

    private void
    sortByLowToHigh(final boolean b) {
        try {
            Collections.sort(viewAllAdapter.homePageProductResponses, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null && productModel.getProductPrice() == null)
                        return 0;
                    if (b)
                        return Double.compare(productModel.getProductPrice().getPriceValue(),
                                (t1.getProductPrice().getPriceValue()));
                    else return Double.compare(t1.getProductPrice().getPriceValue(),
                            (productModel.getProductPrice().getPriceValue()));
                }
            });
        } catch (Exception ignored) {

        }
    }


    private void sortByAtoZ(final boolean b) {
        try {
            Collections.sort(viewAllAdapter.homePageProductResponses, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null)
                        return 0;
                    if (b)
                        return productModel.getName().compareToIgnoreCase(t1.getName());
                    else return t1.getName().compareToIgnoreCase(productModel.getName());
                }
            });
        } catch (Exception ignored) {

        }
    }

    private void sortByPosition() {
        try {
            Collections.sort(viewAllAdapter.homePageProductResponses, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null)
                        return 0;
                    return Integer.compare(productModel.getDisplayOrder(),
                            (t1.getDisplayOrder()));
                }
            });
        } catch (Exception ignored) {

        }
    }

    private List<ProductModel> doSortOption(int value) {
        Log.d("TAG", "" + value);
        switch (value) {
            case 0:
                sortByPosition();
                break;
            case 1:
                sortByAtoZ(true);
                break;
            case 2:
                sortByAtoZ(false);
                break;
            case 3:
                sortByLowToHigh(true);
                break;
            case 4:
                sortByLowToHigh(false);
                break;
            case 5:
                sortByDate();
                break;
        }
        return viewAllAdapter.homePageProductResponses;// productsFromSubcategoryAdapter.loadData(productModels);
    }

    @SuppressLint("CheckResult")
    private void callWebService(String value) {
        ApiHelper.Companion.getDataHelper().availableSort(value, Api.imageSize).compose(RxJavaUtils.Companion.<FeatureProductResponseModel>applySchedulers()).subscribe(new Consumer<FeatureProductResponseModel>() {
            @Override
            public void accept(FeatureProductResponseModel productsResponse) throws Exception {
                sortByProduct(productsResponse);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                AppUtils.rxErrorHandling(getContext());
            }
        });
    }

    private void sortByProduct(FeatureProductResponseModel productResponse) {
        if (productResponse != null && productResponse.getStatusCode() == 200) {
            if (productIds != null && productIds.size() > 0) {
                HomePageProductResponse homePageProductResponse = new HomePageProductResponse();
                List<ProductModel> models = new ArrayList<>();
                for (DataItem item : productResponse.getData()) {
                    ProductModel productModel = new ProductModel();
                    for (Integer i : productIds) {
                        if (i == item.getId()) {
                            productModel.setId(item.getId());
                            productModel.setName(item.getName());
                            DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                            defaultPictureModel.setImageUrl(item.getDefaultPictureModel().getImageUrl());
                            productModel.setDefaultPictureModel(defaultPictureModel);
                            productModel.setShortDescription(item.getShortDescription());
                            ProductPrice productPrice = new ProductPrice();
                            productPrice.setDiscountPercentage(item.getProductPrice().getDiscountPercentage());
                            productPrice.setPrice(item.getProductPrice().getPrice());
                            productPrice.setPriceValue(item.getProductPrice().getPriceValue());
                            productPrice.setProductId(item.getId());
                            productModel.setProductPrice(productPrice);
                            models.add(productModel);
                        }
                    }
                    homePageProductResponse.setData(models);
                    viewAllAdapter.loadData(homePageProductResponse.getData());
                }
            } else {
                HomePageProductResponse homePageProductResponse = new HomePageProductResponse();
                List<ProductModel> models = new ArrayList<>();
                for (DataItem item : productResponse.getData()) {
                    ProductModel productModel = new ProductModel();
                    productModel.setId(item.getId());
                    productModel.setName(item.getName());
                    DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                    defaultPictureModel.setImageUrl(item.getDefaultPictureModel().getImageUrl());
                    productModel.setDefaultPictureModel(defaultPictureModel);
                    productModel.setShortDescription(item.getShortDescription());
                    ProductPrice productPrice = new ProductPrice();
                    productPrice.setDiscountPercentage(item.getProductPrice().getDiscountPercentage());
                    productPrice.setPrice(item.getProductPrice().getPrice());
                    productPrice.setPriceValue(item.getProductPrice().getPriceValue());
                    productPrice.setProductId(item.getId());
                    productModel.setProductPrice(productPrice);
                    models.add(productModel);
                }
                homePageProductResponse.setData(models);
                viewAllAdapter.loadData(homePageProductResponse.getData());
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case FILTER_OPTION_INTENT:
                isFilterClickSingleTime = false;
                filterApiCall(data);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void filterApiCall(Intent data) {
        if (data != null) {

            HashMap<String, String> map = new HashMap<>(0);
            map.put("pagenumber", "1");
            String price = data.getStringExtra("price");
            String priceSelected[] = price.split("-");
            double minPrice = Double.parseDouble(priceSelected[0]);
            double maxPrice = Double.parseDouble(priceSelected[1]);
            int minPriceInt = (int) Math.round(minPrice);
            int maxPriceInt = (int) Math.round(maxPrice);
            ///  SelectedModel model = (SelectedModel) data.getSerializableExtra("category");
            UserSelectedList model = (UserSelectedList) data.getSerializableExtra("category");
            if (model != null && model.getSelectedModelList() != null && model.getSelectedModelList().size() > 0) {
                isFilterClicked = true;
                LinkedHashSet<String> cateName = new LinkedHashSet();
                LinkedHashSet<String> optionName = new LinkedHashSet();
                for (com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel selectedModel : model.getSelectedModelList()) {
                    cateName.add(selectedModel.getCateName());
                }
                for (com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel selectedModel : model.getSelectedModelList()) {
                    optionName.add(selectedModel.getOptionName().substring(0, 1).toUpperCase() + selectedModel.getOptionName().substring(1));
                }

                userSelectionResponseModelList = new ArrayList<>();
                for (String integer : cateName) {
                    UserSelectionResponseModel userSelectionResponseModel = new UserSelectionResponseModel();
                    userSelectionResponseModel.setId(integer);
                    List<GroupDataItem> listGroupDataItem = new ArrayList<>();
                    for (SelectedModel model1 : model.getSelectedModelList()) {
                        if (integer.equalsIgnoreCase(model1.getCateName())) {
                            GroupDataItem groupDataItem = new GroupDataItem();
                            groupDataItem.setCateName(model1.getCateName());
                            groupDataItem.setOptionName(model1.getOptionName());
                            groupDataItem.setFilterId(model1.getFilterId());
                            groupDataItem.setProductId(model1.getProductId());
                            listGroupDataItem.add(groupDataItem);
                        }
                    }
                    userSelectionResponseModel.setGroupData(listGroupDataItem);
                    userSelectionResponseModelList.add(userSelectionResponseModel);
                }
                Log.d("userSelection", new Gson().toJson(userSelectionResponseModelList));


            } else {
                isFilterClicked = maxPriceSel != maxPriceInt || minPriceSel != minPriceInt;
                productIds = new ArrayList<>();
            }
            List<ProductModel> selectedItems = new ArrayList<>(0);
            LinkedHashSet<Integer> pId = new LinkedHashSet<>();
            ArrayList<Integer> pIdMulSelection = new ArrayList<>();
            int countUserSelection = 0;
            List<Integer> ids = new ArrayList<>();
            try {

                if (userSelectionResponseModelList.size() == 1) {
                    for (UserSelectionResponseModel userSelectionResponseModel : userSelectionResponseModelList) {

                        for (GroupDataItem groupDataItem : userSelectionResponseModel.getGroupData()) {
                            pId.add(groupDataItem.getProductId());
                        }
                    }
                } else {
                    for (UserSelectionResponseModel userSelectionResponseModel : userSelectionResponseModelList) {
                        LinkedHashSet<Integer> integers = new LinkedHashSet<>();
                        for (GroupDataItem groupDataItem : userSelectionResponseModel.getGroupData()) {
                            integers.add(groupDataItem.getProductId());
                        }
                        if (integers.size() > 0) {
                            countUserSelection++;
                            ids.addAll(integers);
                        }
                    }
                }

                if (userSelectionResponseModelList.size() == 1) {
                    ArrayList<Integer> integers = new ArrayList<>(pId);
                    for (DataItem response : responseModel.getData()) {
                        double productAmount = response.getProductPrice().getPriceValue();
                        if (minPrice <= productAmount && maxPrice >= productAmount) {
                            if (integers.size() > 0) {
                                for (int i = 0; i < integers.size(); i++) {
                                    if (integers.get(i) == response.getId()) {
                                        ProductModel productModel = new ProductModel();
                                        productModel.setId(response.getId());
                                        productModel.setName(response.getName());
                                        DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                                        defaultPictureModel.setImageUrl(response.getDefaultPictureModel().getImageUrl());
                                        productModel.setDefaultPictureModel(defaultPictureModel);
                                        productModel.setShortDescription(response.getShortDescription());
                                        ProductPrice productPrice = new ProductPrice();
                                        productPrice.setDiscountPercentage(response.getProductPrice().getDiscountPercentage());
                                        productPrice.setPrice(response.getProductPrice().getPrice());
                                        productPrice.setPriceValue(response.getProductPrice().getPriceValue());
                                        productPrice.setProductId(response.getId());
                                        productModel.setProductPrice(productPrice);
                                        selectedItems.add(productModel);
                                    }
                                }
                            }
                        }
                    }
                } else if (userSelectionResponseModelList.size() == 0) {
                    for (DataItem response : responseModel.getData()) {
                        double productAmount = response.getProductPrice().getPriceValue();
                        if (minPrice <= productAmount && maxPrice >= productAmount) {
                            ProductModel productModel = new ProductModel();
                            productModel.setId(response.getId());
                            productModel.setName(response.getName());
                            DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                            defaultPictureModel.setImageUrl(response.getDefaultPictureModel().getImageUrl());
                            productModel.setDefaultPictureModel(defaultPictureModel);
                            productModel.setShortDescription(response.getShortDescription());
                            ProductPrice productPrice = new ProductPrice();
                            productPrice.setDiscountPercentage(response.getProductPrice().getDiscountPercentage());
                            productPrice.setPrice(response.getProductPrice().getPrice());
                            productPrice.setPriceValue(response.getProductPrice().getPriceValue());
                            productPrice.setProductId(response.getId());
                            productModel.setProductPrice(productPrice);
                            selectedItems.add(productModel);
                        }
                    }
                } else {
                    ArrayList<Integer> listIds = new ArrayList<>();
                    Set<Integer> unique = new HashSet<Integer>(ids);
                    for (Integer key : unique) {
                        if (Collections.frequency(ids, key) == countUserSelection) {
                            listIds.add(key);
                        }

                    }
                    for (DataItem response : responseModel.getData()) {
                        double productAmount = response.getProductPrice().getPriceValue();
                        if (minPrice <= productAmount && maxPrice >= productAmount) {
                            if (listIds.size() > 0) {
                                for (int i = 0; i < listIds.size(); i++) {
                                    if (listIds.get(i) == response.getId()) {
                                        ProductModel productModel = new ProductModel();
                                        productModel.setId(response.getId());
                                        productModel.setName(response.getName());
                                        DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                                        defaultPictureModel.setImageUrl(response.getDefaultPictureModel().getImageUrl());
                                        productModel.setDefaultPictureModel(defaultPictureModel);
                                        productModel.setShortDescription(response.getShortDescription());
                                        ProductPrice productPrice = new ProductPrice();
                                        productPrice.setDiscountPercentage(response.getProductPrice().getDiscountPercentage());
                                        productPrice.setPrice(response.getProductPrice().getPrice());
                                        productPrice.setPriceValue(response.getProductPrice().getPriceValue());
                                        productPrice.setProductId(response.getId());
                                        productModel.setProductPrice(productPrice);
                                        selectedItems.add(productModel);
                                    }
                                }
                            }
                        }
                    }
                }

                for (DataItem response : responseModel.getData()) {
                    double productAmount = response.getProductPrice().getPriceValue();
                    if (minPrice <= productAmount && maxPrice >= productAmount) {

                        if (productIds == null) {
                            break;
                        } else if (productIds.size() > 0) {
                            for (int i = 0; i < productIds.size(); i++) {
                                if (productIds.get(i) == response.getId()) {
                                    ProductModel productModel = new ProductModel();
                                    productModel.setId(response.getId());
                                    productModel.setName(response.getName());
                                    DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                                    defaultPictureModel.setImageUrl(response.getDefaultPictureModel().getImageUrl());
                                    productModel.setDefaultPictureModel(defaultPictureModel);
                                    productModel.setShortDescription(response.getShortDescription());
                                    ProductPrice productPrice = new ProductPrice();
                                    productPrice.setDiscountPercentage(response.getProductPrice().getDiscountPercentage());
                                    productPrice.setPrice(response.getProductPrice().getPrice());
                                    productPrice.setPriceValue(response.getProductPrice().getPriceValue());
                                    productPrice.setProductId(response.getId());
                                    productModel.setProductPrice(productPrice);
                                    selectedItems.add(productModel);
                                }
                            }
                        } else {
                            ProductModel productModel = new ProductModel();
                            productModel.setId(response.getId());
                            productModel.setName(response.getName());
                            DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                            defaultPictureModel.setImageUrl(response.getDefaultPictureModel().getImageUrl());
                            productModel.setDefaultPictureModel(defaultPictureModel);
                            productModel.setShortDescription(response.getShortDescription());
                            ProductPrice productPrice = new ProductPrice();
                            productPrice.setDiscountPercentage(response.getProductPrice().getDiscountPercentage());
                            productPrice.setPrice(response.getProductPrice().getPrice());
                            productPrice.setPriceValue(response.getProductPrice().getPriceValue());
                            productPrice.setProductId(response.getId());
                            productModel.setProductPrice(productPrice);
                            selectedItems.add(productModel);
                        }
                    }
                }
                if (alreadySortClicked == -1)
                    viewAllAdapter.loadData(selectedItems);
                else {
                    viewAllAdapter.homePageProductResponses = selectedItems;
                    alReadySortClicked();
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (homePageProductResponse != null && homePageProductResponse.getData() != null && homePageProductResponse.getData().size() > 0)
                    viewAllAdapter.loadData(homePageProductResponse.getData());
            }
        } else {
            isFilterClicked = false;
        }
        sortAndFilterColorChanged();

    }

    private Set<Integer> findDuplicates(List<Integer> listContainingDuplicates) {
        final Set<Integer> setToReturn = new HashSet<>();
        final Set<Integer> set1 = new HashSet<>();

        for (Integer yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    public Set<Integer> getMultiSelection(List<Integer> listContainingDuplicates) {

        final Set<Integer> setToReturn = new HashSet<Integer>();
        final Set<Integer> set1 = new HashSet<Integer>();

        for (Integer yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    private void alReadySortClicked() {
        viewAllAdapter.loadData(doSortOption(alreadySortClicked));
    }

    private void sortAndFilterColorChanged() {
        if (alreadySortClicked == -1)
            rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        else rl_sortby.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));

        if (isFilterClicked)
            rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));
        else rl_filter.setBackgroundColor(getContext().getResources().getColor(R.color.white));

    }
}