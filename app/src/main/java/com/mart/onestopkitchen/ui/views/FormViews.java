package com.mart.onestopkitchen.ui.views;

import android.annotation.SuppressLint;
import com.google.android.material.textfield.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.model.KeyValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 12/8/2015.
 */
public class FormViews {

    @SuppressLint("StaticFieldLeak")
    public static View view;
    public static boolean isFormValid = true;


    public static List<KeyValuePair> getForMFieldValue(ViewGroup viewGroup, String KeyPrefixTag) {
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        try {
            for (int i = 0, count = viewGroup.getChildCount(); i < count; ++i) {
                View outerView = viewGroup.getChildAt(i);
                if (outerView instanceof EditText) {
                    EditText editText = (EditText) outerView;
                    KeyValuePair keyValuePair = new KeyValuePair();
                    keyValuePair.setKey(KeyPrefixTag + editText.getTag());
                    keyValuePair.setValue(getTextBoxFieldValue(editText));
                    keyValuePairs.add(keyValuePair);
                } else if (outerView instanceof LinearLayout) {
                    for (int j = 0; j < ((LinearLayout) outerView).getChildCount(); j++) {
                        View innerView = ((LinearLayout) outerView).getChildAt(j);
                        if (innerView instanceof TextInputLayout) {
                            EditText editText = ((TextInputLayout) innerView).getEditText();

                            if (editText.getTag().toString().equalsIgnoreCase("FloorNumber")) {
                                String floor = getTextBoxFieldValue(editText);
                                for (int k = 0; k < keyValuePairs.size(); k++) {
                                    if (keyValuePairs.get(k).getKey().equalsIgnoreCase("BillingNewAddress.Address1")) {
                                        KeyValuePair keyValuePair = new KeyValuePair();
                                        keyValuePair.setKey(keyValuePairs.get(k).getKey());
                                        keyValuePair.setValue(floor + "," + keyValuePairs.get(k).getValue());
                                        keyValuePairs.set(k, keyValuePair);
                                    }
                                }
                            } else {
                                KeyValuePair keyValuePair = new KeyValuePair();
                                keyValuePair.setKey(KeyPrefixTag + editText.getTag());
                                keyValuePair.setValue(getTextBoxFieldValue(editText));
                                System.out.println(keyValuePair.getKey() + "," + keyValuePair.getValue());
                                keyValuePairs.add(keyValuePair);
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        }
        return keyValuePairs;
    }

    public static boolean isEmpty(TextView etText) {

        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isEmpty(int resourceId) {
        TextView textView = view.findViewById(resourceId);
        if (textView.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static String getTexBoxFieldValue(TextView etText) {
        return etText.getText().toString().trim();
    }

    private static String getTextBoxFieldValue(EditText etText) {
        return etText.getText().toString().trim();
    }

    public static String getTexBoxFieldValue(int resourceId) {
        TextView textView = (TextView) view.findViewById(resourceId);
        return textView.getText().toString().trim();
    }

    public static boolean isEqual(TextView first, TextView second) {
        return first.getText().toString().trim().equals(second.getText().toString().trim());
    }


    public static boolean isValidWithMark(EditText etText, String displayText) {
        if (isEmpty(etText)) {
            etText.setError(displayText + " is Required");
            isFormValid = false;

            //etText.setError(displayText+" is Empty");
            return false;
        } else {

            return true;
        }
    }

    private static TextView getTextView(int resourceId) {
        return (TextView) view.findViewById(resourceId);
    }

    private static TextView getTextView(int resourceId, View view) {
        return (TextView) view.findViewById(resourceId);
    }

    public static void setText(int resourceId, String value, View parentView) {

        if (value == null)
            getTextView(resourceId, parentView).setText("");
        else
            getTextView(resourceId, parentView).setText(value);
    }

    public static void setTextOrHideIfEmpty(int resourceId, String value, View parentView) {
        if (value == null || value.length() == 0)
            getTextView(resourceId, parentView).setVisibility(View.GONE);
        else
            getTextView(resourceId, parentView).setText(value);
    }

    public static boolean isValidWithMark(int resourceId, String displayText) {
        if (isEmpty(resourceId)) {
            getTextView(resourceId).setError(displayText + " is Required");
            isFormValid = false;
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidWithMark(int resourceId) {
        TextView textView = getTextView(resourceId);
        if (isEmpty(textView)) {
            textView.setError(getCamelValue(textView.getTag().toString()) + " is Required");
            isFormValid = false;
            return false;
        } else {
            return true;
        }
    }

    private static String getCamelValue(String str) {
        String text = "";
        for (String w : str.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            text = text + w + " ";
        }
        return text;
    }

}
