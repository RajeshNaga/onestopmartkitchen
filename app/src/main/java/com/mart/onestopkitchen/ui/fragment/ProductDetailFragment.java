package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.quickblox.users.model.QBUser;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.chat.LoginToMsgChat;
import com.mart.onestopkitchen.chat.LoginVideoChat;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.AllowedQuantity;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.NotifyModel;
import com.mart.onestopkitchen.model.PictureModel;
import com.mart.onestopkitchen.model.ProductAttribute;
import com.mart.onestopkitchen.model.ProductDetail;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.ProductSpecification;
import com.mart.onestopkitchen.model.Quantity;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.BaseResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.AddtoCartResponse;
import com.mart.onestopkitchen.networking.response.PriceResponse;
import com.mart.onestopkitchen.networking.response.ProductDetailResponse;
import com.mart.onestopkitchen.networking.response.RelatedProductResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.FullScreenImageActivity;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.adapter.ColorViewAdapter;
import com.mart.onestopkitchen.ui.adapter.DynamicViewAdapter;
import com.mart.onestopkitchen.ui.adapter.ImagesSlideAdapter;
import com.mart.onestopkitchen.ui.adapter.MeasureAdapter;
import com.mart.onestopkitchen.ui.adapter.ProductDetailSliderAdapter;
import com.mart.onestopkitchen.ui.adapter.ProductSpecificationAdapter;
import com.mart.onestopkitchen.ui.adapter.RelatedProductAdapter;
import com.mart.onestopkitchen.ui.adapter.SizeViewAdapter;
import com.mart.onestopkitchen.ui.customview.CustomLinearLayoutManager;
import com.mart.onestopkitchen.ui.customview.RadioGridGroup;
import com.mart.onestopkitchen.ui.views.ProductAttributeViews;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.CustomTextView;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.Language;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mart.onestopkitchen.ui.adapter.ImagesSlideAdapter.row_index;


/**
 * Created by Ashraful on 11/9/2015.
 */
public class ProductDetailFragment extends BaseFragment implements View.OnClickListener,
        SizeViewAdapter.onSelectSizeAdapterListener, ColorViewAdapter.onSelectColorAdapterListener, DynamicViewAdapter.onSelectDynamicAdapterListener,
        CompoundButton.OnCheckedChangeListener, ImagesSlideAdapter.OnImageSelectListener, AdapterView.OnItemSelectedListener, View.OnTouchListener {

    @BindView(R.id.tv_product_old_price)
    TextView productOldPriceTextView;
    @BindView(R.id.tv_productName)
    TextView productNameTextview;
    @BindView(R.id.tv_productPrice)
    TextView productPriceTextview;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_expand_collaps)
    CustomTextView expandCollapseTextView;
    @BindView(R.id.btn_addtoCart)
    Button addtoCartBtn;
    @BindView(R.id.btn_buy_now)
    Button addToBuyBtn;
    @BindView(R.id.gridLayout_group_product)
    RadioGridGroup groupProducrgridLayout;
    @BindView(R.id.dynamicAttributeLayout)
    LinearLayout dynamicAttributeLayout;
    @BindView(R.id.rclv_product_list)
    RecyclerView RelatedProductList;
    @BindView(R.id.cv_category_product_container)
    LinearLayout relatedProductsCardView;
    @BindView(R.id.expand_dsc)
    ImageView expandDsc;
    @BindView(R.id.expand_view)
    View expandView;
    @BindView(R.id.tv_specifications)
    TextView specificationTextView;
    @BindView(R.id.quantitiyUp)
    ImageView quantitiyUp;
    @BindView(R.id.quantityDown)
    ImageView quantityDown;
    @BindView(R.id.textviewQuantity)
    TextView textviewQuantity;
    @BindView(R.id.quantityLayout)
    LinearLayout quantityLayout;
    @BindView(R.id.tv_price_off)
    TextView tvOfferAmount;
    @BindView(R.id.rel_total)
    RelativeLayout relTotal;
    @BindView(R.id.specificelayout)
    LinearLayout specificelayout;
    @BindView(R.id.detailsScrollview)
    NestedScrollView detailsScrollview;
    @BindView(R.id.recyclerView_image)
    RecyclerView imageShowRecyclerview;
    @BindView(R.id.tv_delivery_date)
    TextView tv_delivery_date;
    @BindView(R.id.stock_count)
    TextView stockCount;
    @BindView(R.id.tv_rating_number)
    TextView tv_rating_number;
    @BindView(R.id.tv_rating_reviews)
    TextView tv_rating_reviews;
    @BindView(R.id.measurement_view)
    LinearLayout measurementView;
    @BindView(R.id.size_main_layout)
    RelativeLayout sizeMainLayout;
    @BindView(R.id.color_main_layout)
    RelativeLayout colorMainLayout;
    @BindView(R.id.size_recyclerview)
    RecyclerView sizeRecyclerView;
    @BindView(R.id.color_recyclerview)
    RecyclerView colorRecyclerView;
    @BindView(R.id.rv_measure_view)
    RecyclerView measureView;
    @BindView(R.id.merge_recyclerview)
    LinearLayout viewLayout;
    @BindView(R.id.include_Container_Layout)
    LinearLayout containerLayout;
    @BindView(R.id.tv_size)
    TextView textSizeHeader;
    @BindView(R.id.tv_color)
    TextView textColorHeader;
    @BindView(R.id.button_favorite)
    ToggleButton toggleButton;
    @BindView(R.id.rv_spec_view)
    RecyclerView specificationView;
    @BindView(R.id.tv_short_description)
    TextView tvShortDescription;
    @BindView(R.id.lin_short_description)
    LinearLayout linShortDescription;
    @BindView(R.id.lin_exp)
    LinearLayout descLayout;
    @BindView(R.id.ll_related_products)
    LinearLayout relatedLayout;
    @BindView(R.id.spinner_quantities)
    Spinner quant;
    @BindView(R.id.layout_allowed_quantities)
    RelativeLayout qtyLayout;
    @BindView(R.id.tv_gsmreview)
    TextView gsmReview;
    @BindView(R.id.img_sharing)
    ImageView img_sharing;

    @BindView(R.id.notifyLayout)
    CardView notifyLayout;
    @BindView(R.id.btn_nofify)
    Button notify;
    @BindView(R.id.fixedLayout)
    CardView fixedLayoput;
    @BindView(R.id.tv_endDate)
    TextView endDate;
    @BindView(R.id.ll_available)
    LinearLayout availableLayout;
    @BindView(R.id.btn_chat_msg)
    FloatingActionButton btn_chat_msg;
    @BindView(R.id.btn_chat_audio_video)
    FloatingActionButton btn_chat_audio_video;
    @BindView(R.id.ll_Msg_Call)
    LinearLayout ll_Msg_Call;

    @SuppressLint("StaticFieldLeak")
    public static ProductDetailFragment self;
    public static ProductModel productModel;

    private ViewPager sliderPager = null;
    private ImagesSlideAdapter adapter = null;
    private boolean isAddedToWishlist = false;
    private ScaleAnimation scaleAnimation = null;
    private CompoundButton compoundButton = null;
    private boolean isDescriptionExpanded = false;
    ProductAttributeViews productAttributeViews;
    private List<KeyValuePair> keyValuePairs = new ArrayList<>(0);
    private Map<String, String> productPairMap;
    private MeasureAdapter measureAdapter;
    private Rect rect = null;
    private Handler handler;
    private Runnable mLongPressed;
    private int orginalQuantity = 0;
    private Quantity quantity;
    private int cartOrBuyNow = 0;
    private Dialog errorDialog;
    private int wishlist = 0;
    private List<String> qArray;
    private String gsmUrl;
    private DynamicViewAdapter viewAdapter;
    private String TAG = ProductDetailFragment.class.getSimpleName();


    private void releaseObj() {
        if (null != productPairMap) {
            productPairMap = null;
        }
        if (null != handler) {
            handler = null;
        }
        if (null != mLongPressed) {
            mLongPressed = null;
        }
        if (null != measureAdapter) {
            measureAdapter = null;
        }
        if (null != rect) {
            rect = null;
        }
        if (null != errorDialog) {
            errorDialog = null;
        }
        if (null != qArray) {
            qArray = null;
        }
        if (null != viewAdapter) {
            viewAdapter = null;
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        self = this;
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        sliderPager = view.findViewById(R.id.slider_pager);
        productPairMap = new HashMap<>();
        errorDialog = new Dialog(Objects.requireNonNull(getContext()), R.style.CustomDialog);
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.setContentView(R.layout.required_popup);
        errorDialog.setCancelable(false);
        row_index = 0;

        BounceInterpolator bounceInterpolator = new BounceInterpolator();
        scaleAnimation = new ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f, Animation.RELATIVE_TO_SELF, 0.7f, Animation.RELATIVE_TO_SELF, 0.7f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setInterpolator(bounceInterpolator);
        toggleButton.setOnCheckedChangeListener(this);
        quant.setOnItemSelectedListener(this);
        btn_chat_msg.setOnClickListener(this);
        btn_chat_audio_video.setOnClickListener(this);
        img_sharing.setOnClickListener(this);
        measurementView.setVisibility(View.GONE);
//        detailsScrollview.setOnTouchListener(this);
        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onResume() {
        super.onResume();
        if (null != getActivity()) {
            ((MainActivity) getActivity()).hideToolBar(false);
            ((MainActivity) getActivity()).hideKeyboard();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    void initializeExpandCollapseResource() {
        expandView.setOnClickListener(this);
        handler = new Handler();
        quantitiyUp.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                mLongPressed = this::increaseQuantity;
                handler.postDelayed(mLongPressed, 100);
            }
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
                    stopIncrmenting();
                }
            }
            if (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP) {
                stopIncrmenting();
            }
            return false;
        });

        quantityDown.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                mLongPressed = this::decreaseQuantity;
                handler.postDelayed(mLongPressed, 100);
            }

            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
                    stopIncrmenting();
                }
            }

            if (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP) {
                stopIncrmenting();
            }

            return false;
        });
    }

    @SuppressLint("SetTextI18n")
    private void increaseQuantity() {
        int maxValue;
        if (quantity.getOrderMaximumQuantity() > quantity.getStockQuantity()) {
            maxValue = quantity.getOrderMaximumQuantity();
        } else {
            maxValue = quantity.getStockQuantity();
        }
        if (orginalQuantity < maxValue) {
            orginalQuantity++;
            textviewQuantity.setText("" + orginalQuantity);
        }
    }

    @SuppressLint("SetTextI18n")
    private void decreaseQuantity() {
        if (orginalQuantity > quantity.getOrderMinimumQuantity()) {
            orginalQuantity--;
            textviewQuantity.setText("" + orginalQuantity);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView();
        SizeViewAdapter sizeViewAdapter = new SizeViewAdapter(getContext(), this);
        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.HORIZONTAL, false);
        sizeRecyclerView.setHasFixedSize(true);
        sizeRecyclerView.setLayoutManager(layoutManager);
        sizeRecyclerView.setAdapter(sizeViewAdapter);

        ColorViewAdapter colorViewAdapter = new ColorViewAdapter(getContext(), this);
        CustomLinearLayoutManager colorLayoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.HORIZONTAL, false);
        colorRecyclerView.setHasFixedSize(true);
        colorRecyclerView.setLayoutManager(colorLayoutManager);
        colorRecyclerView.setAdapter(colorViewAdapter);

        measureAdapter = new MeasureAdapter(getContext());
        CustomLinearLayoutManager measureLayoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        measureView.setHasFixedSize(true);
        measureView.setLayoutManager(measureLayoutManager);
        measureView.setAdapter(measureAdapter);
        //callWebService();
        addtoCartBtn.setOnClickListener(this);
        addToBuyBtn.setOnClickListener(this);
        gsmReview.setOnClickListener(this);
        notify.setOnClickListener(this);
    }

    private synchronized void stopIncrmenting() {
        handler.removeCallbacks(mLongPressed);
    }

    @Override
    public void onStart() {
        super.onStart();
        callWebService();
    }

    @SuppressLint("RtlHardcoded")
    protected void initializeView() {
        expandCollapseTextView.setGravity(View.TEXT_ALIGNMENT_CENTER);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        params.setMargins(10, 0, 10, 0);
        //  addtoCartLayout.setLayoutParams(params);
        setProduceNamePrice();
        initializeExpandCollapseResource();
        RelatedProductList.setLayoutManager(getLinearLayoutManager());

        if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            expandCollapseTextView.setGravity(Gravity.RIGHT);
            specificelayout.setGravity(Gravity.RIGHT);
            specificationTextView.setGravity(Gravity.RIGHT);
        }
        addtoCartBtn.setVisibility(View.VISIBLE);


        //  Animation slideUp = AnimationUtilss.loadAnimation(getContext(), R.anim.anim_slide_down);
        //  addtoCartBtn.startAnimation(slideUp);

    }

    protected void callWebService() {
        if (null != productModel && productModel.getId() != 0L) {
            RetroClient.getApi().getProductDetails(productModel.getId()).enqueue(new CustomCB<ProductDetailResponse>(this.getView()));
            RetroClient.getApi().getRelatedProducts(productModel.getId()).enqueue(new CustomCB<RelatedProductResponse>(getView()));
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String getNewDate(String oldDate) {
        if (oldDate == null) {
            return "";
        }

        SimpleDateFormat oldFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        oldFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date value;
        String dueDateAsNormal = "";
        try {
            value = oldFormatter.parse(oldDate);
            SimpleDateFormat newFormatter = new SimpleDateFormat("dd-MMM-yyyy");

            newFormatter.setTimeZone(TimeZone.getDefault());
            dueDateAsNormal = newFormatter.format(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dueDateAsNormal;
    }

    @SuppressLint("SetTextI18n")
    public void onEvent(@NotNull ProductDetailResponse detailResponse) {
        if (detailResponse.getData().getAvailableEndDateTimeUtc() != null && !detailResponse.getData().getAvailableEndDateTimeUtc().equals("")) {
            availableLayout.setVisibility(View.VISIBLE);
            endDate.setText(getNewDate(detailResponse.getData().getAvailableEndDateTimeUtc()));
        }
        if (detailResponse.getData().getQuantity().getStockQuantity() > 0) {
            fixedLayoput.setVisibility(View.VISIBLE);
            notifyLayout.setVisibility(View.GONE);
        } else {
            fixedLayoput.setVisibility(View.GONE);
            notifyLayout.setVisibility(View.VISIBLE);
        }

        if (null != detailResponse.getData()) {
            String productCategoryName = detailResponse.getData().getProductCategoryName();
            Log.e(TAG, "productCategoryName : " + productCategoryName);
            ((MyApplication) Objects.requireNonNull(getActivity()).getApplicationContext()).PRODUCT_CATEGORY_NAME = productCategoryName;
        }

        List<AllowedQuantity> allowedQuantityList = null;
        if (detailResponse.getData() != null) {
            allowedQuantityList = detailResponse.getData().getAddToCart().getAllowedQuantities();
        }

        if (allowedQuantityList != null && allowedQuantityList.size() > 0) {
            qtyLayout.setVisibility(View.VISIBLE);
            qArray = new ArrayList<>();
            for (int i = 0; i < allowedQuantityList.size(); i++) {
                qArray.add(allowedQuantityList.get(i).getValue());
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), android.R.layout.simple_spinner_item, qArray);
            quant.setAdapter(adapter);
        }

        detailsScrollview.smoothScrollTo(0, 0);
        productModel = detailResponse.getData();
        String descriptionText = detailResponse.getData().getFullDescription();
        quantity = detailResponse.getData().getQuantity();
        orginalQuantity = quantity.getOrderMinimumQuantity();
        textviewQuantity.setText("" + orginalQuantity);
        quantityLayout.setVisibility(View.VISIBLE);
        if (detailResponse.getData().getStockAvailability().equalsIgnoreCase("Out of stock") || detailResponse.getData().getStockAvailability().equalsIgnoreCase("ပစၥည္းပ်က္ေတာက္သည္။")) {
            addtoCartBtn.setVisibility(View.VISIBLE);
            addtoCartBtn.setClickable(false);
            addtoCartBtn.setText(R.string.product_out_of_stock);
            stockCount.setVisibility(View.GONE);
            addToBuyBtn.setVisibility(View.VISIBLE);
            addToBuyBtn.setClickable(false);
            toggleButton.setEnabled(false);
            toggleButton.setClickable(false);
            showErrorListPopup(getString(R.string.out_of_stock).split(","));
            //   relTotal.setAlpha(.7f);
            //  relTotal.setAlpha(1);
            //return;
        } else {
            //  relTotal.setAlpha(1);
            toggleButton.setEnabled(true);
            toggleButton.setClickable(true);
            if (detailResponse.getData().getQuantity() != null) {
                if (detailResponse.getData().getQuantity().getStockQuantity() > 0) {
                    if (detailResponse.getData().getQuantity().getStockQuantity() > 5) {
                        stockCount.setText(getString(R.string.availability) + " : " + getString(R.string.in_stock));
                    } else {
                        stockCount.setText(getString(R.string.availability) + " : " + detailResponse.getData().getQuantity().getStockQuantity());
                    }
                } else {
                    stockCount.setText(getString(R.string.availability) + " : " + getString(R.string.out_stock));
                }
                //stockCount.setText(getString(R.string.stock_quantity) + " : " + detailResponse.getData().getQuantity().getStockQuantity());
            }
            stockCount.setVisibility(View.VISIBLE);
            addtoCartBtn.setVisibility(View.VISIBLE);
            addToBuyBtn.setVisibility(View.VISIBLE);
        }

        if (descriptionText != null) {
            descLayout.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv_description.setText(Html.fromHtml(descriptionText, Html.FROM_HTML_MODE_COMPACT));
            } else {
                tv_description.setText(Html.fromHtml(descriptionText));
            }
        }
        if (detailResponse.getData().getShortDescription() != null) {
            linShortDescription.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvShortDescription.setText(Html.fromHtml(detailResponse.getData().getShortDescription(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                tvShortDescription.setText(Html.fromHtml(detailResponse.getData().getShortDescription()));
            }
        } else {
            linShortDescription.setVisibility(View.GONE);
        }
        if (detailResponse.getData().getAddToCart().getWishList()) {
            toggleButton.setChecked(true);
        } else {
            toggleButton.setChecked(false);
        }

      /*  if (descriptionText != null) {
            fullDescriptionView.loadDataWithBaseURL("", descriptionText, "text/html", "UTF-8", "");
        } else if (detailResponse.getData().getShortDescription() != null) {
            fullDescriptionView.loadDataWithBaseURL("", detailResponse.getData().getShortDescription(), "text/html", "UTF-8", "");
        }*/

        setProduceNamePrice();
        setProductSpecificationNew(detailResponse.getData().getProductSpecifications());
        //setProductSpecification(detailResponse.getData().getProductSpecifications());
        setImageInSlider(detailResponse.getData());
        showGroupProducts(detailResponse.getData());
        populateViewOfDynamicAttributeLayout(detailResponse.getData());

        /*Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.anim_slide_up);
        addtoCartBtn.setVisibility(View.VISIBLE);
        addtoCartBtn.startAnimation(slideUp);*/

        if (detailResponse.getData().getDeliveryDate() != null && (!detailResponse.getData().getDeliveryDate().isEmpty())) {
            tv_delivery_date.setVisibility(View.VISIBLE);
            tv_delivery_date.setText(getString(R.string.delivered_on) + " : " + detailResponse.getData().getDeliveryDate());
        }
        tv_rating_number.setText(String.valueOf(detailResponse.getData().getReviewOverviewModel().getRatingSum()));
        tv_rating_reviews.setText(detailResponse.getData().getReviewOverviewModel().getTotalReviews() + " " + getString(R.string.rating) + " & "
                + detailResponse.getData().getReviewOverviewModel().getTotalReviews() + " " + getString(R.string.review));

        if (detailResponse.getData().getReviewOverviewModel().getRatingFromGSMArena() != null &&
                (!detailResponse.getData().getReviewOverviewModel().getRatingFromGSMArena().isEmpty())) {
            gsmReview.setVisibility(View.VISIBLE);
            gsmUrl = detailResponse.getData().getReviewOverviewModel().getRatingFromGSMArena();
        }
        setRecentProduct(detailResponse);
        showCallButtons(true);
    }

    private void setRecentProduct(ProductDetailResponse detailResponse) {
        String recentData = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS);
        if (!AppUtils.cutNull(recentData).isEmpty()) {
            Type listType = new TypeToken<List<ProductModel>>() {
            }.getType();
            List<ProductModel> productModels = new Gson().fromJson(recentData, listType);
            if (productModels != null) {
                for (ProductModel model : productModels) {
                    if (model.getId() == productModel.getId()) {
                        productModels.remove(model);
                        break;
                    }
                }
                ProductDetail productDetail = detailResponse.getData();
                if (productDetail != null) {
                    ProductModel model = new ProductModel();
                    model.setName(productDetail.getName());
                    model.setShortDescription(productDetail.getShortDescription());
                    model.setSku(productDetail.getSku());
                    model.setProductPrice(productDetail.getProductPrice());
                    model.setReviewOverviewModel(productDetail.getReviewOverviewModel());
                    model.setDefaultPictureModel(productDetail.getDefaultPictureModel());
                    model.setWishList(productDetail.getWishList());
                    model.setMarkAsNew(productDetail.getMarkAsNew());
                    model.setId(productDetail.getId());
                    productModels.add(0, model);
                    PreferenceService.getInstance().SetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS, new Gson().toJson(productModels));
                }
            }
        }

    }

   /* private void setProductSpecification(List<ProductSpecification> productSpecifications) {
        if (productSpecifications != null && productSpecifications.size() > 0) {
            StringBuilder spec = new StringBuilder();

            specificationTextView.setMovementMethod(LinkMovementMethod.getInstance());

            for (ProductSpecification specification : productSpecifications) {
                if (specification.getName().equalsIgnoreCase("Link")) {
                    Matcher m = Patterns.WEB_URL.matcher(specification.getValue());
                    String url = "";
                    while (m.find()) {
                        url = m.group();
                        Log.d("Url", url);
                    }
                    spec.append(specification.getName()).append(" : ").append(url).append("<br>");
                } else {
                    spec.append(specification.getName()).append(" : ").append(specification.getValue()).append("<br>");
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                specificationTextView.setText(Html.fromHtml(spec.toString(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                specificationTextView.setText(Html.fromHtml(spec.toString()));
            }
        }
        specificationTextView.setVisibility(View.VISIBLE);
    }*/

    private void setProductSpecificationNew(@NotNull List<ProductSpecification> productSpecifications) {
        if (productSpecifications.size() > 0) {
            specificelayout.setVisibility(View.VISIBLE);
        }
        ProductSpecificationAdapter specificationAdapter = new ProductSpecificationAdapter(getContext(), productSpecifications);
        CustomLinearLayoutManager specificationLayoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        specificationView.setHasFixedSize(true);
        specificationView.setLayoutManager(specificationLayoutManager);
        specificationView.setAdapter(specificationAdapter);
    }

    public void onEvent(@NotNull RelatedProductResponse productsResponse) {
        if (productsResponse.getData() != null && productsResponse.getData().size() > 0) {
            relatedProductsCardView.setVisibility(View.VISIBLE);
            relatedLayout.setVisibility(View.VISIBLE);
            // relatedProductTitleTextView.setVisibility(View.VISIBLE);
            RelatedProductAdapter productAdapter = new RelatedProductAdapter(getActivity(), productsResponse.getData());
            RelatedProductList.setAdapter(productAdapter);
            onRelatedProductClicked(productAdapter);
        } else {
            relatedProductsCardView.setVisibility(View.GONE);
        }

    }

    @SuppressLint("SetTextI18n")
    public void onEvent(@NotNull PriceResponse priceResponse) {
        List<String> measurementList = new ArrayList<>();
        if (priceResponse.getPrice() != null) {
            productPriceTextview.setText(AppUtils.getMMKString(productPriceTextview.getTextSize(), priceResponse.getPrice(), 0));
            stockCount.setText(priceResponse.getStockQuantity().toString());
            if (priceResponse.getStockQuantity() != null) {
                if (priceResponse.getStockQuantity() > 0) {
                    if (priceResponse.getStockQuantity() > 5) {
                        stockCount.setText(getString(R.string.availability) + " : " + getString(R.string.in_stock));
                    } else {
                        stockCount.setText(getString(R.string.availability) + " : " + priceResponse.getStockQuantity());
                    }

                    fixedLayoput.setVisibility(View.VISIBLE);
                    notifyLayout.setVisibility(View.GONE);

                } else {

                    fixedLayoput.setVisibility(View.GONE);
                    notifyLayout.setVisibility(View.VISIBLE);

                    stockCount.setText(getString(R.string.availability) + " : " + getString(R.string.out_stock));
                }
                //stockCount.setText(getString(R.string.stock_quantity) + " : " + detailResponse.getData().getQuantity().getStockQuantity());
            }


            if (priceResponse.getMeasurementData() != null && !priceResponse.getMeasurementData().isEmpty() && !priceResponse.getMeasurementData().equals("")) {
                measurementView.setVisibility(View.VISIBLE);  //   measurementView.expand();
                String measureData = priceResponse.getMeasurementData();
                String nString = measureData.replace("\\n", "and");
                String[] lines = nString.split("and");
                //measurementList.addAll(Arrays.asList(lines));
                for (String line : lines) {
                    if (!line.isEmpty())
                        measurementList.add(line);
                }

                measureAdapter.loadData(measurementList);


            }
            viewAdapter.loadData(measurementList);
        }
    }

    public void onEvent(@NotNull AddtoCartResponse addtoCartResponse) {

        if (addtoCartResponse.isSuccess() && addtoCartResponse.getStatusCode() == 200 && cartOrBuyNow == 1 && wishlist == 0) {
            CartFragment fragment = new CartFragment();
            FragmentTransaction transaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment).addToBackStack(null).commit();

        } else if (addtoCartResponse.isSuccess() && addtoCartResponse.getStatusCode() == 200 && cartOrBuyNow == 0 && wishlist == 0) {
            if (isAddedToWishlist) {
                Snackbar.make(Objects.requireNonNull(getView()), R.string.product_add_to_wishlist, Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(Objects.requireNonNull(getView()), R.string.product_added_to_cart, Snackbar.LENGTH_SHORT).show();
                Utility.doAnimate();
                Utility.setCartCounter(addtoCartResponse.getCount());
            }
        } else if (addtoCartResponse.isSuccess() && addtoCartResponse.getStatusCode() == 200 && wishlist == 2) {
            if (compoundButton != null) {
                compoundButton.startAnimation(scaleAnimation);
            }

        } else if (!addtoCartResponse.getErrorList()[0].isEmpty()) {
            //Snackbar.make(Objects.requireNonNull(getView()), addtoCartResponse.getErrorList()[0], Snackbar.LENGTH_SHORT).show();
            showErrorListPopup(addtoCartResponse.getErrorList());
        }
    }

    private void showErrorListPopup(@NotNull String[] errorList) {
        TextView tvClose = errorDialog.findViewById(R.id.tv_close);
        TextView tvErrorText = errorDialog.findViewById(R.id.tv_required_text);
        tvErrorText.setText(errorList[0]);
        tvClose.setOnClickListener(view -> errorDialog.dismiss());
        Objects.requireNonNull(errorDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (!errorDialog.isShowing()) {
            errorDialog.show();
        }
    }

    private void onRelatedProductClicked(@NotNull final RelatedProductAdapter relatedProductAdapter) {
        relatedProductAdapter.SetOnItemClickListener((view, position) -> {
            ProductDetailFragment.productModel = relatedProductAdapter.products.get(position);
            callWebService();
        });
    }

    @SuppressLint("SetTextI18n")
    protected void setProduceNamePrice() {
        try {
            if (null != productModel && null != productModel.getName())
                Objects.requireNonNull(getActivity()).setTitle(productModel.getName());

            if (null != productModel && null != productModel.getName())
                productNameTextview.setText(productModel.getName());

            if (null != productModel && null != productModel.getProductPrice() && null != productModel.getProductPrice().getPriceWithDiscount()) {
                productPriceTextview.setText(AppUtils.getMMKString(productPriceTextview.getTextSize(), productModel.getProductPrice().getPriceWithDiscount(), 0));
                productOldPriceTextView.setText(productModel.getProductPrice().getPrice());
            } else {
                if (null != productModel && null != productModel.getProductPrice()) {
                    productPriceTextview.setText(AppUtils.getMMKString(productPriceTextview.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                    productOldPriceTextView.setText(AppUtils.getMMKString(productOldPriceTextView.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));
                }
            }

            productOldPriceTextView.setPaintFlags(productOldPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            if (null != productModel.getProductPrice() && productModel.getProductPrice().getDiscountPercentage() > 0) {
                tvOfferAmount.setVisibility(View.VISIBLE);
                tvOfferAmount.setText(productModel.getProductPrice().getDiscountPercentage() + " % " + "off" + "-" + productModel.getProductPrice().getDiscountOfferName());
            } else {
                tvOfferAmount.setVisibility(View.GONE);
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    private void setImageInSlider(@NotNull final ProductDetail detail) {

     /*   if (detail.getPictureModels().get(0).getImageUrl().contains(".gif"))
            Glide.with(getActivity())
                    .load(detail.getPictureModels().get(0).getImageUrl())
                    .asGif()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    // .placeholder(R.drawable.placeholder)
                    .into(firstProductImage);
        else
            Glide.with(getActivity())
                    .load(detail.getPictureModels().get(0).getImageUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    // .placeholder(R.drawable.placeholder)
                    .into(firstProductImage);
//        String productImgUrl = detail.getPictureModels().get(0).getImageUrl();
        imageShowRecyclerview.setLayoutManager(getLinearLayoutManager());
        ImagesSlideAdapter adapter = new ImagesSlideAdapter(getActivity(), detail.getPictureModels());
        imageShowRecyclerview.setItemAnimator(new DefaultItemAnimator());
        imageShowRecyclerview.setAdapter(adapter);
        imageShowRecyclerview.setNestedScrollingEnabled(false);
        firstProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(detail.getPictureModels());
            }
        });*/

        ProductDetailSliderAdapter detailsSliderAdapter = new ProductDetailSliderAdapter(getActivity(), detail.getPictureModels());
        sliderPager.setAdapter(detailsSliderAdapter);
        sliderPager.setCurrentItem(0);
        pageChangeListener();
        /*circlePageIndicator.setViewPager(viewPager);

        circlePageIndicator.setPageColor(ContextCompat.getColor(getActivity(), R.color.textHintColor));
        circlePageIndicator.setFillColor(ContextCompat.getColor(getActivity(), R.color.priceColor));*/

        imageShowRecyclerview.setLayoutManager(getLinearLayoutManager());
        adapter = new ImagesSlideAdapter(getActivity(), detail.getPictureModels(), this);
        imageShowRecyclerview.setItemAnimator(new DefaultItemAnimator());
        imageShowRecyclerview.setAdapter(adapter);
        imageShowRecyclerview.setNestedScrollingEnabled(false);

        detailsSliderAdapter.setOnSliderClickListener((view, sliderPosition) -> {
            Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
            intent.putExtra("slider", sliderPosition);
            intent.putExtra(Keys.PRODUCT_NAME, productModel.getName());
            intent.putExtra("list", (Serializable) detail.getPictureModels());
            startActivity(intent);
        });


        /* viewPager.removeAllSliders();
        productImages.stopAutoCycle();
        for (PictureModel pictureModel : detail.getPictureModels()) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            defaultSliderView.image(pictureModel.getImageUrl())
                    .setScaleType(BaseSliderView.ScaleType.CenterInside);
            onImageSliderClick(defaultSliderView,detail);

            productImages.addSlider(defaultSliderView);
        }
        productImages.setCustomIndicator((PagerIndicator) view.findViewById(R.id.custom_indicator));*/
    }


    public void onEvent(List<PictureModel> picturesModel) {
        FullScreenImageActivity.pictureModels = picturesModel;
        Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
        intent.putExtra(Keys.PRODUCT_NAME, productModel.getName());
        startActivity(intent);
    }

   /* private void onImageSliderClick(DefaultSliderView textSliderView, final ProductDetail details) {
        textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                                    @Override
                                                    public void onSliderClick(BaseSliderView baseSliderView) {
                                                        FullScreenImageActivity.pictureModels = details.getPictureModels();
                                                        Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
                                                        startActivity(intent);

                                                    }
                                                }
        );
    }*/

    private void showGroupProducts(@NotNull final ProductDetail details) {
        groupProducrgridLayout.removeAllViews();
        for (ProductDetail detail : details.getAssociatedProducts()) {

            generateViewOfSingleProductSelectorAmongGroupProduct(detail, details.getName());
        }

        groupProducrgridLayout.setOnCheckedChangeListener((group, checkedId) -> {
            productModel = searchProductById(checkedId, details);
            setProduceNamePrice();
        });
        if (details.getAssociatedProducts().size() > 0)
            groupProducrgridLayout.check((int) details.getAssociatedProducts().get(0).getId());


    }

    @SuppressLint("InflateParams")
    private void generateViewOfSingleProductSelectorAmongGroupProduct(@NotNull ProductDetail associateProduct, @NotNull String groupProductName) {
        RadioButton button = (RadioButton) getLayoutInflater().inflate
                (R.layout.radiobutton_group_product_selection, null);
        button.setText(associateProduct.getName().
                substring(groupProductName.length()));
        button.setId((int) associateProduct.getId());

        groupProducrgridLayout.addView(button);
    }


    /*protected void populateViewOfDynamicAttributeLayout(ProductDetail detail) {
        List<ProductAttribute> productAttributes = new ArrayList<>();
        containerLayout.removeAllViews();
        if (detail.getProductAttributes().size() > 0) {
            //dynamicAttributeLayout.setVisibility(View.VISIBLE);
            // productAttributeViews = new ProductAttributeViews(getActivity(), detail.getProductAttributes(), dynamicAttributeLayout);

            for (ProductAttribute productAttribute : detail.getProductAttributes()) {

                if (productAttribute.getAttributeControlType() == AttributeControlType.RadioList) {
                    sizeMainLayout.setVisibility(View.VISIBLE);
                    if(productAttribute.getTextPrompt() != null){
                        textSizeHeader.setText(productAttribute.getTextPrompt());
                    }else{
                        textSizeHeader.setText(productAttribute.getName());
                    }
                    productAttributes.add(productAttribute);
                    for (int i = 0; i < productAttributes.size(); i++) {
                        sizeViewAdapter.loadData(productAttributes.get(i).getValues(), productAttribute);
                    }

                } else if (productAttribute.getAttributeControlType() == AttributeControlType.ColorSquares) {
                    colorMainLayout.setVisibility(View.VISIBLE);
                    if(productAttribute.getTextPrompt() != null){
                        textColorHeader.setText(productAttribute.getTextPrompt());
                    }else{
                        textColorHeader.setText(productAttribute.getName());
                    }
                    productAttributes.add(productAttribute);
                    for (int i = 0; i < productAttributes.size(); i++) {
                        colorViewAdapter.loadData(productAttributes.get(i).getValues(), productAttribute);
                    }

                } else if (productAttribute.getAttributeControlType() != AttributeControlType.ColorSquares && productAttribute.getAttributeControlType() != AttributeControlType.RadioList) {
                    LayoutInflater inflater = getLayoutInflater();
                    LinearLayout linearLayout = (LinearLayout) inflater.inflate
                            (R.layout.include_view_for_productdetail, viewLayout, false);

                    linearLayout.setVisibility(View.VISIBLE);
                    RecyclerView productRecyclerlist = linearLayout.findViewById(R.id.rv_product_attributes);
                    TextView header = linearLayout.findViewById(R.id.tv_header);
                    if(productAttribute.getTextPrompt() != null){
                        header.setText(productAttribute.getTextPrompt());
                    }else{
                        header.setText(productAttribute.getName());
                    }
                    productAttributes.add(productAttribute);


                    DynamicViewAdapter viewAdapter = new DynamicViewAdapter(productAttribute.getValues(), productAttribute, getContext(), this);
                    dynamicLayoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.HORIZONTAL, false);
                    productRecyclerlist.setHasFixedSize(true);
                    productRecyclerlist.setLayoutManager(dynamicLayoutManager);
                    productRecyclerlist.setAdapter(viewAdapter);
                    containerLayout.addView(linearLayout);

                }
            }
        }

    }*/

    private void populateViewOfDynamicAttributeLayout(@NotNull ProductDetail detail) {
        List<ProductAttribute> productAttributes = new ArrayList<>();
        containerLayout.removeAllViews();
        if (detail.getProductAttributes().size() > 0) {
            //dynamicAttributeLayout.setVisibility(View.VISIBLE);
            // productAttributeViews = new ProductAttributeViews(getActivity(), detail.getProductAttributes(), dynamicAttributeLayout);

            for (ProductAttribute productAttribute : detail.getProductAttributes()) {

                LayoutInflater inflater = getLayoutInflater();
                LinearLayout linearLayout = (LinearLayout) inflater.inflate
                        (R.layout.include_view_for_productdetail, viewLayout, false);

                linearLayout.setVisibility(View.VISIBLE);
                RecyclerView productRecyclerlist = linearLayout.findViewById(R.id.rv_product_attributes);
                TextView header = linearLayout.findViewById(R.id.tv_header);
                if (productAttribute.getTextPrompt() != null) {
                    header.setText(productAttribute.getTextPrompt());
                } else {
                    header.setText(productAttribute.getName());
                }
                productAttributes.add(productAttribute);


                viewAdapter = new DynamicViewAdapter(productAttributes, productAttribute, getContext(), this);
                CustomLinearLayoutManager dynamicLayoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.HORIZONTAL, false);
                productRecyclerlist.setHasFixedSize(true);
                productRecyclerlist.setLayoutManager(dynamicLayoutManager);
                productRecyclerlist.setAdapter(viewAdapter);
                containerLayout.addView(linearLayout);


            }
        }

    }

    @Nullable
    private ProductDetail searchProductById(int id, @NotNull ProductDetail details) {
        ProductDetail associatedProducts;
        for (ProductDetail product : details.getAssociatedProducts()) {
            if (product.getId() == id) {
                associatedProducts = product;
                return associatedProducts;
            }
        }
        return null;
    }

    LinearLayoutManager getLinearLayoutManager() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return layoutManager;
    }

    @Override
    public void onClick(@NotNull View v) {
        int resourceId = v.getId();
       /* if (resourceId == R.id.expand_view) {
            //ExpandorCollapse();
        } else */
        if (resourceId == R.id.btn_chat_msg) {
            startChat("Msg");
        } else if (resourceId == R.id.btn_chat_audio_video) {
            startChat("AudioVideo");
        } else if (resourceId == R.id.btn_addtoCart) {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                isAddedToWishlist = false;
                wishlist = 0;
                cartOrBuyNow = 0;
                callApiOfAddingProductIntoCart(Api.shoppingCartTypeCart);
            }
        } else if (resourceId == R.id.btn_buy_now) {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                isAddedToWishlist = false;
                wishlist = 0;
                cartOrBuyNow = 1;
                callApiOfAddingProductIntoCart(Api.shoppingCartTypeCart);
            }
        } else if (resourceId == R.id.tv_gsmreview) {
            GsmFragment gsmFragment = new GsmFragment();
            Bundle args = new Bundle();
            args.putString("pageurl", gsmUrl);
            args.putString("title", productModel.getName() + " " + getString(R.string.gsmreview));
            gsmFragment.setArguments(args);
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction().replace(R.id.container, gsmFragment).addToBackStack(null).commit();
            }
        } else if (resourceId == R.id.img_sharing) {
            showToast("product share");
            productShare(productModel.getUrl() + "?Pid=" + productModel.getId());
        } else if (resourceId == R.id.btn_nofify) {
            callNotifyApi(productModel.getId());
        }
    }

    /*

    private void openFab() {
        if (isOpen) {
            tv_audio.setVisibility(View.INVISIBLE);
            tv_video.setVisibility(View.INVISIBLE);
            fab_Video.startAnimation(fab_close);
            fab_audio.startAnimation(fab_close);
            btn_chat_audio_video.startAnimation(fab_anticlock);
            fab_Video.setClickable(false);
            fab_audio.setClickable(false);
            isOpen = false;
        } else {
            tv_audio.setVisibility(View.VISIBLE);
            tv_video.setVisibility(View.VISIBLE);
            fab_Video.startAnimation(fab_open);
            fab_audio.startAnimation(fab_open);
            btn_chat_audio_video.startAnimation(fab_clock);
            fab_Video.setClickable(true);
            fab_audio.setClickable(true);
            isOpen = true;
        }
    }
*/

    private void callNotifyApi(Long pId) {
        RetroClient.getApi().notifyMe(pId).enqueue(new Callback<NotifyModel>() {
            @Override
            public void onResponse(@NonNull Call<NotifyModel> call, @NonNull Response<NotifyModel> response) {
                if (response.body() != null) {
                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<NotifyModel> call, @NonNull Throwable t) {

            }
        });
    }

    public void callApiOfAddingProductIntoCart(int cartTypeId) {
        List<KeyValuePair> productAttributes = getProductAttribute();
        if (null != productModel) {
            if (orginalQuantity != 0) {
                KeyValuePair keyValuePair = new KeyValuePair();
                keyValuePair.setKey("addtocart_" + productModel.getId() + ".EnteredQuantity");
                keyValuePair.setValue(textviewQuantity.getText().toString());
                productAttributes.add(keyValuePair);
            }

            RetroClient.getApi()
                    .addProductIntoCart(productModel.getId(), cartTypeId, productAttributes)
                    .enqueue(new CustomCB<AddtoCartResponse>(this.getView()));
        }
    }

    /* Expand view removed */
    protected void ExpandorCollapse() {
        isDescriptionExpanded = !isDescriptionExpanded;
//        fullDescriptionView.setVisibility(isDescriptionExpanded ? View.VISIBLE : View.GONE);
        tv_description.setVisibility(isDescriptionExpanded ? View.VISIBLE : View.GONE);

        expandDsc.setImageResource(getExpandCollapseDrawable());
        Drawable myIcon = expandDsc.getDrawable();
        ColorFilter filter = new LightingColorFilter(Color.DKGRAY, Color.DKGRAY);
        myIcon.setColorFilter(filter);
    }

    @Contract(pure = true)
    private int getExpandCollapseDrawable() {
        return isDescriptionExpanded ? R.drawable.ic_arrow_up_new : R.drawable.ic_arrow_down;
    }

    @Override
    public List<KeyValuePair> getSelectedSizeFromadapter(@NotNull Map<String, String> pairValue) {
        for (String value : pairValue.keySet()) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey(pairValue.get(value));
            keyValuePair.setValue(value);
            gatherValue(value, pairValue.get(value));
            if (null != keyValuePairs) {
                for (int i = 0; i < keyValuePairs.size(); i++) {
                    if (keyValuePairs.get(i).getKey().contains(keyValuePair.getKey())) {
                        keyValuePairs.remove(i);
                    }
                }
                keyValuePairs.add(keyValuePair);
            }
        }
        return keyValuePairs;
    }

    @Override
    public void expandView() {
        //  measurementView.expand();
    }

    @Override
    public List<KeyValuePair> getSelectedColorFromAdapter(@NotNull Map<String, String> pairValue) {
        for (String value : pairValue.keySet()) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey(pairValue.get(value));
            keyValuePair.setValue(value);
            gatherValue(value, pairValue.get(value));
            for (int i = 0; i < keyValuePairs.size(); i++) {
                if (keyValuePairs.get(i).getKey().contains(keyValuePair.getKey())) {
                    keyValuePairs.remove(i);
                }
            }
            keyValuePairs.add(keyValuePair);
        }
        return keyValuePairs;
    }

    private List<KeyValuePair> getProductAttribute() {
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        for (String value : productPairMap.keySet()) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey(productPairMap.get(value));
            keyValuePair.setValue(value);
            keyValuePairs.add(keyValuePair);
            Log.e("key,Value:", productPairMap.get(value) + "," + value);
        }
        return keyValuePairs;
    }

    private void gatherValue(String value, String key) {
        productPairMap.values().removeAll(Collections.singleton(key));
        productPairMap.put(String.valueOf(value), key);
    }

    @Override
    public List<KeyValuePair> getSelectedDynamicValue(@NotNull Map<String, String> pairValue) {

        for (String value : pairValue.keySet()) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey(pairValue.get(value));
            keyValuePair.setValue(value);
            gatherValue(value, pairValue.get(value));
            if (null != keyValuePairs) {
                for (int i = 0; i < keyValuePairs.size(); i++) {
                    if (keyValuePairs.get(i).getKey().contains(keyValuePair.getKey())) {
                        keyValuePairs.remove(i);
                    }
                }
                keyValuePairs.add(keyValuePair);
            }

        }
        return keyValuePairs;
    }

    @Override
    public void controlSliderFromColor(int imageId) {
        adapter.changeSlide(imageId);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        this.compoundButton = compoundButton;
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            // if (PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE)) {
            if (isChecked) {
                wishlist = 2;
                callApiOfAddingProductIntoCart(Api.shoppingCartTypeWishlist);
            } else {
                if (null != productModel && productModel.getId() != 0L) {
                    // removeWishListObservable(String.valueOf(productModel.getId()), "2").subscribeWith(getObserverForRemoveWishList());
                    RetroClient.getApi()
                            .removeProductFromWishList(String.valueOf(productModel.getId()), "2")
                            .enqueue(new CustomCB<BaseResponse>(getView()));
                }
            }
           /* } else {
                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).addToBackStack(null).commit();
                }
            }*/
        } else {
            toggleButton.setChecked(false);
        }
    }

    public void onEvent(@NotNull BaseResponse response) {
        StringBuilder errors = new StringBuilder();
        if (response.getErrorList().length > 0) {
            for (int i = 0; i < response.getErrorList().length; i++) {
                errors.append("  ").append(i + 1).append(": ").append(response.getErrorList()[i]).append(" \n");
            }
            //Snackbar.make(Objects.requireNonNull(getView()), errors, Snackbar.LENGTH_LONG).show();
        }
    }

    private void pageChangeListener() {
        sliderPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                row_index = position;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onItemSelect(int position, PictureModel img) {
        sliderPager.setCurrentItem(position);
    }

    @Override
    public void OnItemMaxFour(int position, List<PictureModel> img) {
        Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
        intent.putExtra("slider", position);
        intent.putExtra(Keys.PRODUCT_NAME, productModel.getName());
        intent.putExtra("list", (Serializable) img);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        releaseObj();
        super.onDestroyView();
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        textviewQuantity.setText("" + qArray.get(i));
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onPause() {
        super.onPause();
        System.gc();
    }

    private void productShare(String url) {
        Intent share_intent = new Intent();
        share_intent.setAction(Intent.ACTION_SEND);
        share_intent.setType("text/plain");
        share_intent.putExtra(Intent.EXTRA_TEXT, url);
        share_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(Intent.createChooser(share_intent, "Amazing Product..."));
    }


    public static Period getTimePassedSince(@NotNull Date initialTimestamp) {
        DateTime initDT = new DateTime(initialTimestamp.getTime());
        DateTime now = new DateTime();
        Period p = new Period(initDT, now, PeriodType.dayTime()).normalizedStandard(PeriodType.dayTime());
        return p;
    }

    @SuppressLint("RestrictedApi")
    private void showCallButtons(boolean visible) {
        if (null != ll_Msg_Call && visible) {
            ll_Msg_Call.setVisibility(View.VISIBLE);
        } else {
            if (null != ll_Msg_Call) {
                ll_Msg_Call.setVisibility(View.GONE);
            }
        }
    }

    private void startChat(String chatOption) {
        QBUser userDetails = getUserDetails();
//        Log.e(TAG, "-----userDetails-----: " + userDetails.toString());
        if (null != getActivity()) {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                if (chatOption.equals("Msg")) {
                    LoginToMsgChat.getInstance(getActivity()).msgChat(userDetails);

                } else if (chatOption.equals("AudioVideo")) {
                    LoginVideoChat.Companion.getInstance().videoChat(getActivity(), userDetails);

                } else {
                    showToast(getActivity().getResources().getString(R.string.please_try_again));
                }
            } else {
                showToast(getActivity().getResources().getString(R.string.no_internet_connection));
            }
        } else {
            showToast("Context Issue !");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                showCallButtons(false);
                return true;
            case MotionEvent.ACTION_UP:
                showCallButtons(true);
                break;
        }
        return false;
    }
}


