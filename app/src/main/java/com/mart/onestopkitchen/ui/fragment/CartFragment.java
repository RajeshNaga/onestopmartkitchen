package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.loginuser.logindetails.UserLoginRegisterListenerCart;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;
import com.mart.onestopkitchen.model.BillingAddress;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.postrequest.DiscountCouponRequest;
import com.mart.onestopkitchen.networking.postrequest.ValuePost;
import com.mart.onestopkitchen.networking.response.AddtoCartResponse;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.networking.response.BillingAddressSaveResponse;
import com.mart.onestopkitchen.networking.response.CartProductListResponse;
import com.mart.onestopkitchen.networking.response.DiscountCouponApplyResponse;
import com.mart.onestopkitchen.networking.response.OrderTotalResponse;
import com.mart.onestopkitchen.networking.response.ProductDetailResponse;
import com.mart.onestopkitchen.networking.response.ShoppingCartCheckoutAttributeApplyResponse;
import com.mart.onestopkitchen.presenter.PickupPointSelectedPresenter;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.adapter.CartAdapter;
import com.mart.onestopkitchen.ui.customview.CustomLinearLayoutManager;
import com.mart.onestopkitchen.utils.AddressBottomSheet;
import com.mart.onestopkitchen.utils.AppConstants;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.Language;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Updated by Akash Garg on 16/09/2018.
 */
public class CartFragment extends BaseFragment implements View.OnClickListener, CartAdapter.OnSaveItemClickListener, UserLoginRegisterListenerCart, PickupPointSelectedPresenter.SavePickUpPoint {
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindView(R.id.rclv_cartList)
    RecyclerView cartProductRecyclerList;
    @BindView(R.id.btn_proceed_to_Checkout)
    Button checkoutBtn;
    @BindView(R.id.btn_apply_coupon)
    Button applyCouponBtn;
    @BindView(R.id.ll_existing_address)
    LinearLayout ll_existing_address;
    @BindView(R.id.tv_subtotal)
    TextView subTotalTextView;
    @BindView(R.id.tv_shipping)
    TextView shippingTextView;
    @BindView(R.id.tv_tax)
    TextView taxTextView;
    @BindView(R.id.tv_Total)
    TextView totalAmountTextView;
    @BindView(R.id.tv_discount)
    TextView discountTextView;
    @BindView(R.id.tr_discount)
    TableRow discountTableRow;
    @BindView(R.id.taxRow)
    TableRow taxRow;
    @BindView(R.id.dynamicAttributeLayout)
    LinearLayout dynamicAttributeLayout;
    @BindView(R.id.et_coupon_code)
    EditText couponCodeEditText;
    @BindView(R.id.cv_product_attribute)
    CardView productAttributeCardView;
    @BindView(R.id.ll_cartInfoLayout)
    LinearLayout CartInfoLinearLayout;
    @BindView(R.id.ll_noCartItemLayout)
    LinearLayout NoItemCartLinearLayout;
    @BindView(R.id.btn_continue_shopping)
    Button btn_continue_shopping;
    @BindView(R.id.discountlayout)
    CardView discountLayout;
    @BindView(R.id.rb_pickup_point)
    RadioButton rb_pickup_point;
    @BindView(R.id.rb_door_address)
    RadioButton rb_door_address;
    @BindView(R.id.table_orderTotal)
    TableLayout table_orderTotal;
    @BindView(R.id.taxKey)
    TextView taxKey;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmer_view_container;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_phone_number)
    TextView tvPhoneNumber;
    @BindView(R.id.btnPickupPointLocation)
    TextView btnPickupPointLocation;
    @BindView(R.id.rg_delivery_option)
    RadioGroup rg_delivery_option;
    @BindView(R.id.tv_pickup_location)
    TextView tvPickupLocation;
    @BindView(R.id.tv_pickup_name)
    TextView tvPickupName;
    @BindView(R.id.btn_edit)
    ImageView btn_edit;
    @BindView(R.id.ll_pickup_address)
    LinearLayout ll_pickup_address;
    @BindView(R.id.img_edit_address)
    ImageView img_edit_address;
    @BindView(R.id.ll_pickupaddress_layout)
    LinearLayout pickupAddressLayout;

    private String TAG = CartFragment.class.getSimpleName();
    boolean isDataLoaded = false;
    private AddressBottomSheet addressBottomSheet;
    private List<BillingAddress> existingAddressList;
    private BillingAddress selectedAddress = null;
    private long addressID = 0;
    private boolean isAddreeApiCallAfterRegister;
    private boolean isDeliveryAllowed = false;
    private boolean pickupPointSelected = false;
    private OrderTotalResponse orderTotalModel;
    private int lastScrollPosition = 0;
    private View view = null;
    public static ProductModel productModel;
    private static int id;
    private static int count;
    private static int pid;
//    private CheckoutAttributeView checkoutAttributeView;

    public CartFragment() {
        setArguments(new Bundle());
    }

    public static void addToWishList(CartProduct cartProduct, Fragment fragment) {
        id = cartProduct.getProductId();
        pid = cartProduct.getId();
        count = cartProduct.getQuantity();
        List<KeyValuePair> productAttributes = new ArrayList<>();
        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey("addtocart_" + cartProduct.getProductId() + ".EnteredQuantity");
        keyValuePair.setValue(String.valueOf(cartProduct.getQuantity()));
        productAttributes.add(keyValuePair);
        RetroClient.getApi()
                .addProductIntoCart(cartProduct.getProductId(), 2, productAttributes).enqueue(new CustomCB<AddtoCartResponse>(fragment.getView()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_cart, container, false);
        unbinder = ButterKnife.bind(this, view);
        checkoutBtn.setEnabled(false);
        checkoutBtn.setClickable(false);
        return view;
    }

    @SuppressLint("RtlHardcoded")
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.NAVIGATION_CLASS_NAME, "5"); // need to change
        loadPage();
//        batteryPercentage();
    }

    @SuppressLint("RtlHardcoded")
    private void loadPage() {
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.my_cart));
        checkEventBusRegistration();
        setLayoutManagerofRecyclerList();
        setClickListenerOnView();
        callWebservice();

        if (PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.discuntShow)) {
            discountLayout.setVisibility(View.GONE);
        } else {
            discountLayout.setVisibility(View.GONE);
        }
        if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            table_orderTotal.setGravity(Gravity.RIGHT);
            cartProductRecyclerList.setRotationY(180);
        }
        cartProductRecyclerList.setHasFixedSize(true);
        isAddreeApiCallAfterRegister = false;
        pickupPointSelect();
        noItemFoundInCart();
        updateAddress();
    }

    private void updateAddress() {
        img_edit_address.setOnClickListener(view -> {
            if (selectedAddress != null) {
                Fragment fragment = new BillingAddressFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(Keys.BILLING_ADDRESS, selectedAddress);
                bundle.putBoolean("IS_COME_FROM_CART", true);
                fragment.setArguments(bundle);
                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, fragment).addToBackStack(null).commit();
                }
            }
        });
    }

    private void noItemFoundInCart() {
        btn_continue_shopping.setOnClickListener(view -> {
            if (getFragmentManager() != null) {
                moveToHomeFragment();
            }
        });
    }

    private void resetPickPoint() {
        rb_door_address.setChecked(true);
        btnPickupPointLocation.setVisibility(View.GONE);
        ll_pickup_address.setVisibility(View.GONE);
        pickupPointSelected = false;
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0");
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, "");
    }

    private void pickupPointSelect() {
        if (!PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS).isEmpty())
            rb_pickup_point.setChecked(true);
        else
            rb_door_address.setChecked(true);


        rg_delivery_option.setOnCheckedChangeListener((radioGroup, i) -> {
            if (radioGroup.getCheckedRadioButtonId() == R.id.rb_door_address) {
                if (nestedScrollView != null)
                    nestedScrollView.scrollTo(0, 0);
                btnPickupPointLocation.setVisibility(View.GONE);
                ll_pickup_address.setVisibility(View.GONE);
                pickupPointSelected = false;
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0");
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, "");

            } else {
                if (nestedScrollView != null)
                    nestedScrollView.smoothScrollTo(0, lastScrollPosition + 300);

                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0");
                pickupPointSelected = true;
                if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS).isEmpty())
                    btnPickupPointLocation.setVisibility(View.VISIBLE);
                else
                    ll_pickup_address.setVisibility(View.VISIBLE);
            }
        });

        if (!PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS).isEmpty()) {
            ll_pickup_address.setVisibility(View.VISIBLE);
            btnPickupPointLocation.setVisibility(View.GONE);
            tvPickupName.setText(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_NAME));
            tvPickupLocation.setText(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS));
        } else {
            rb_door_address.setChecked(true);
            btnPickupPointLocation.setVisibility(View.GONE);
            ll_pickup_address.setVisibility(View.GONE);
            pickupPointSelected = false;
        }

        btnPickupPointLocation.setOnClickListener(view ->
                goToPickupPointFragment()
        );

        btn_edit.setOnClickListener(view ->
                goToPickupPointFragment()
        );
    }

    private void goToPickupPointFragment() {
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, new PickupPointListFragment()).addToBackStack(null).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isDataLoaded) {
            shimmer_view_container.startShimmer();
            loadPage();
        }
        if (nestedScrollView != null) {
            nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(() -> {
                if (null != nestedScrollView)
                    lastScrollPosition = nestedScrollView.getScrollY();
            });
        }
    }

    @Override
    public void onPause() {
        shimmer_view_container.stopShimmer();
        super.onPause();
    }

    private void setLayoutManagerofRecyclerList() {
        CustomLinearLayoutManager cartLayoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        cartProductRecyclerList.setHasFixedSize(true);
        cartProductRecyclerList.setLayoutManager(cartLayoutManager);
    }

    private void setClickListenerOnView() {
        applyCouponBtn.setOnClickListener(this);
        checkoutBtn.setOnClickListener(this);
    }

    private void callWebservice() {
        RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
        RetroClient.getApi().getShoppingCart().enqueue(new CustomCB<CartProductListResponse>(this.getView()));


        /*if (isBuyNow()) {
            if (getArguments() != null) { String productID = getArguments().getString("productId");// call api for product details}
        } else RetroClient.getApi().getShoppingCart().enqueue(new CustomCB<CartProductListResponse>(this.getView()));*/

    }

    public void onEvent(ProductDetailResponse detailResponse) {
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey("itemquantity" + id);
        keyValuePair.setValue(String.valueOf(count));
        keyValuePairs.add(keyValuePair);
        RetroClient.getApi().updateCartProductList(keyValuePairs).enqueue(new CustomCB<CartProductListResponse>(this.getView()));
    }

    public void onEvent(BillingAddressResponse billingAddressResponse) {
        if (billingAddressResponse != null && billingAddressResponse.getExistingAddresses().size() > 0) {
            if (null != pickupAddressLayout)
                pickupAddressLayout.setVisibility(View.VISIBLE);

            if (null != ll_existing_address)
                ll_existing_address.setVisibility(View.VISIBLE);

            existingAddressList = billingAddressResponse.getExistingAddresses();
            existingAddressList.add(new BillingAddress(getString(R.string.add_address)));
            showAddress(existingAddressList.get(0));
            isDeliveryAllowed = existingAddressList.get(0).isDeliveryAllowed();
        }
        if (existingAddressList != null)
            addressBottomSheet = AddressBottomSheet.getInstance(existingAddressList);
        if (isAddreeApiCallAfterRegister) {
            if (PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE)) {
                if (billingAddressResponse != null && billingAddressResponse.getExistingAddresses().size() > 0) {
                    PreferenceService.getInstance().SetPreferenceValue(PreferenceService.ADDRESS_DONE, true);
                } else {
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                                .replace(R.id.container, new BillingAddressFragment()).addToBackStack(null).commit();
                    }
                }
            }
        }
    }

    public void onEvent(BillingAddress address) {
        addressID = address.getId();
        isDeliveryAllowed = address.isDeliveryAllowed();
        callSaveAddressFromAddressApi();
        showAddress(address);

        if (addressBottomSheet.isVisible())
            addressBottomSheet.dismiss();

        resetPickPoint();

        //update the cart data or prices
//        RetroClient.getApi().getShoppingCart().enqueue(new CustomCB<CartProductListResponse>(this.getView()));
    }

    @SuppressLint("SetTextI18n")
    private void showAddress(BillingAddress address) {
        try {
            if (null != address) {
                if ((null != address.getFirstName() && !address.getFirstName().isEmpty())) {
                    Log.d("showAddress", new Gson().toJson(address));
                    selectedAddress = address;
                    String houseNo = (address.getHouseNo() != null) ? (!address.getHouseNo().isEmpty()) ? getString(R.string.house_no) + address.getHouseNo().replace("!", "") + "," : "" : "";
                    String roomNo = (address.getRoomNo() != null) ? (!address.getRoomNo().isEmpty() ? getString(R.string.room_no) + address.getRoomNo().replace("!", "") + "," : "") : "";
                    String floorNo = (address.getFloorNo() != null) ? (!address.getFloorNo().isEmpty() ? getString(R.string.floor_no) + address.getFloorNo().replace("!", "") + "," : "") : "";
                    String address2 = (null != address.getAddress2() && !address.getAddress2().isEmpty()) ? address.getAddress2() + ", " : "";
                    tvUsername.setText(address.getFirstName());
                    tvAddress.setText(houseNo + roomNo + floorNo + address.getAddress1() + ", " + address2 + address.getCity() + ", " + address.getStateProvinceName());
                    tvPhoneNumber.setText(AppUtils.formattedNumber(address.getPhoneNumber()));
                } else {
                    ll_existing_address.setVisibility(View.GONE);

                }
            } else {
                ll_existing_address.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* private void populateViewOfDynamicAttributeLayouts(ProductDetail data) {
        if (data.getProductAttributes().size() > 0)
            dynamicAttributeLayout.setVisibility(View.VISIBLE);
            productAttributeViews = new ProductAttributeViews(getActivity(), data.getProductAttributes(), dynamicAttributeLayout);

           productAttributeViews.putEdittextValueInMap();
           List<KeyValuePair> productAttributes = productAttributeViews.getProductAttribute();
          // if (orginalQuantity != 0) {
          KeyValuePair keyValuePair = new KeyValuePair();
          keyValuePair.setKey("addtocart_" + productModel.getId() + ".EnteredQuantity");
          keyValuePair.setValue(itemCount);
          productAttributes.add(keyValuePair);
         //   }

        RetroClient.getApi()
                .addProductIntoCart(productModel.getId(), 2, productAttributes)
                .enqueue(new CustomCB<AddtoCartResponse>(this.getView()));
    }*/

    public void onEvent(Fragment addNewAddressFragment) {
        if (existingAddressList.size() > 5) {
            AppDialogs.customDialog(getActivity(), getString(R.string.no_more_address), () -> {
            });
        } else if (null != addressBottomSheet) {
            addressBottomSheet.dismiss();
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                        .replace(R.id.container, addNewAddressFragment).addToBackStack(null).commit();
            }
        }
    }

    private void callApiOfAddingProductIntoCart(CartProduct itemCount) {
        RetroClient.getApi().getProductDetails(itemCount.getId()).enqueue(new CustomCB<ProductDetailResponse>(this.getView()));
    }

    public void onEvent(CartProductListResponse cartProductListResponse) {
        if (shimmer_view_container != null) shimmer_view_container.setVisibility(View.GONE);
        AppConstants.mCartMgmt.setmCartProduct(cartProductListResponse);
        if (cartProductListResponse != null && cartProductListResponse.getItems() != null) {
            if (cartProductListResponse.getItems().size() > 0) {
                orderTotalModel = cartProductListResponse.getOrderTotalResponseModel();
                showHideCheckoutBtn(cartProductListResponse.isCanContinue());
                Utility.setCartCounter(cartProductListResponse.getCount());
                showHideView(true);
                isDataLoaded = true;
                populatedDatainAdapter(cartProductListResponse.getItems());
//                populateViewOfDynamicAttributeLayout(cartProductListResponse);
                populateDataInOrderTotalLayout(cartProductListResponse.getOrderTotalResponseModel());

            } else {
                Utility.setCartCounter(0);
                showHideView(false);
            }
        }
    }

    private void showHideView(boolean isShow) {
        if (isShow) {
            if (null != checkoutBtn)
                checkoutBtn.setVisibility(View.VISIBLE);
            CartInfoLinearLayout.setVisibility(View.VISIBLE);
            NoItemCartLinearLayout.setVisibility(View.GONE);
        } else {
            if (null != checkoutBtn)
                checkoutBtn.setVisibility(View.GONE);
            CartInfoLinearLayout.setVisibility(View.GONE);
            NoItemCartLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    private void populatedDatainAdapter(List<CartProduct> cartProductList) {
        CartAdapter cartAdapter = new CartAdapter(getActivity(), cartProductList, this, preferenceService);
        cartProductRecyclerList.setNestedScrollingEnabled(false);
        cartProductRecyclerList.setAdapter(cartAdapter);
//        cartAdapter.setCheckoutBtnListener(this);
//        makeActionOnCartItemClick(cartAdapter);
    }

    /*protected void populateViewOfDynamicAttributeLayout(CartProductListResponse cartProductListResponse) {
        if (cartProductListResponse.getCheckoutAttributes().size() > 0) {
            productAttributeCardView.setVisibility(View.VISIBLE);
            dynamicAttributeLayout.setVisibility(View.VISIBLE);
        } else
            productAttributeCardView.setVisibility(View.GONE);
        checkoutAttributeView = new CheckoutAttributeView(getActivity(), cartProductListResponse.getCheckoutAttributes(), dynamicAttributeLayout);
    }*/

    public void onEvent(OrderTotalResponse orderTotalResponse) {
        if (orderTotalResponse.getStatusCode() == 200) {
            populateDataInOrderTotalLayout(orderTotalResponse);
        }
    }

    @SuppressLint("RtlHardcoded")
    private void populateDataInOrderTotalLayout(OrderTotalResponse orderTotalRespons) {
        if (orderTotalRespons != null) {
//            Log.e(TAG, "------------Shipping Charge ---- : " + orderTotalRespons.getShipping());

            subTotalTextView.setText(AppUtils.getMMKString(subTotalTextView.getTextSize(), orderTotalRespons.getSubTotal(), 0));

            if (null != orderTotalRespons.getShipping()) {
                shippingTextView.setText(AppUtils.getMMKString(shippingTextView.getTextSize(),
                        !orderTotalRespons.getShipping().equalsIgnoreCase("0 MMK") ? orderTotalRespons.getShipping() : getString(R.string.free), 0));
            } else {
                shippingTextView.setText(AppUtils.getMMKString(shippingTextView.getTextSize(), getString(R.string.free), 0));
            }

            totalAmountTextView.setText(AppUtils.getMMKString(totalAmountTextView.getTextSize(), orderTotalRespons.getOrderTotal(), 0));

            if (orderTotalRespons.getOrderTotalDiscount() != null) {
                discountTableRow.setVisibility(View.GONE);
                discountTextView.setText(AppUtils.getMMKString(discountTextView.getTextSize(), orderTotalRespons.getOrderTotalDiscount(), 0));
            } else
                discountTableRow.setVisibility(View.GONE);

            if (orderTotalRespons.getOrderTotal() == null) {
                totalAmountTextView.setText(R.string.calculated_during_checkout);
                totalAmountTextView.setTextColor(Color.RED);
            }

            if (orderTotalRespons.getTax() != null && !orderTotalRespons.getTax().equalsIgnoreCase("0 MMK")) {
                taxRow.setVisibility(View.VISIBLE);
                taxTextView.setVisibility(View.VISIBLE);
                taxTextView.setText(AppUtils.getMMKString(taxTextView.getTextSize(), orderTotalRespons.getTax(), 0));
            } else {
                taxRow.setVisibility(View.GONE);
                taxTextView.setVisibility(View.GONE);
            }

            ((MyApplication) (Objects.requireNonNull(getActivity()).getApplication())).setProductAmount(orderTotalRespons.getSubTotal());
            ((MyApplication) (Objects.requireNonNull(getActivity()).getApplication())).setShippingCharges(orderTotalRespons.getShipping());
            ((MyApplication) (Objects.requireNonNull(getActivity()).getApplication())).setTotalAmount(orderTotalRespons.getOrderTotal());
        }
    }

    private void makeActionOnCartItemClick(final CartAdapter cartAdapter) {
        cartAdapter.SetOnItemClickListener((view, position) -> {
            CartItemEditFragment.cartProduct = cartAdapter.products.get(position);
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                        .replace(R.id.container, new CartItemEditFragment()).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int resourceId = v.getId();
        if (resourceId == R.id.btn_proceed_to_Checkout) {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                ((MyApplication) Objects.requireNonNull(getContext()).getApplicationContext()).fromMenuSelection = "";
                if (null != orderTotalModel) {
                    int orderTotalAmount = orderTotalModel.getOrderTotalInt();
                    if (orderTotalAmount < orderTotalModel.getMinOrderTotalAmount()) {
                        AppDialogs.customDialog(getActivity(), getString(R.string.low_product_amount) + AppUtils.getMMKString(subTotalTextView.getTextSize()
                                , String.valueOf(orderTotalModel.getMinOrderTotalAmount()), 0), () -> {
                        });
                        return;
                    }
                }

                if (pickupPointValidate()) {
                    if (AppUtils.isConnectedToInternet(Objects.requireNonNull(getContext()))) {
                        if (getFragmentManager() != null) {
                            getFragmentManager().beginTransaction()
                                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                                    .replace(R.id.container, getFragment()).addToBackStack(null).commit();
                        }
                    } else {
                        if (getActivity() != null)
                            ((MainActivity) getActivity()).showNormalToast(getString(R.string.no_internet));
                    }
                }
            }
        } else if (resourceId == R.id.btn_apply_coupon) {
            applyCouponApi();
        }
    }

    private boolean pickupPointValidate() {
        String pickupPointId = "0";
        if (existingAddressList == null) {
            gotoNewFragment(getFragment());
            return false;
        }

        if (existingAddressList.size() > 0 && (null == existingAddressList.get(0).getFirstName())) {
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                        .replace(R.id.container, new BillingAddressFragment()).addToBackStack(null).commit();
                return false;
            }
        }

        if (!PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID).isEmpty()) {
            pickupPointId = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID);
        }
        if (!isDeliveryAllowed) {
            if (pickupPointSelected && !pickupPointId.equalsIgnoreCase("0")) {
                new PickupPointSelectedPresenter(Objects.requireNonNull(getActivity()), pickupPointId, this);
                return true;
            } else {
                showUnDeliveryPopup(getString(R.string.delivery_not_available));
                return false;
            }
        } else {
            if (pickupPointSelected) {
                if (pickupPointId.equalsIgnoreCase("0")) {
                    showUnDeliveryPopup(getString(R.string.select_pickup_point));
                    return false;
                } else {
                    new PickupPointSelectedPresenter(Objects.requireNonNull(getActivity()), pickupPointId, this);
                    return true;
                }
            } else {
                new PickupPointSelectedPresenter(Objects.requireNonNull(getActivity()), pickupPointId, this);
                return true;
            }
        }
    }

    private Fragment getFragment() {
        if (MyApplication.PROCEED_AS_GUEST) {
            if (existingAddressList != null && existingAddressList.size() > 0 && (null != existingAddressList.get(0).getFirstName()))
                return new PaymentMethodFragment();
            else
                return new BillingAddressFragment();
        }
        if (!PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE)) {
            return new LoginFragment();
        } else if (!PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.ADDRESS_DONE)) {
            if (existingAddressList != null && existingAddressList.size() > 0 && (null != existingAddressList.get(0).getFirstName()))
                return new PaymentMethodFragment();
            else return new BillingAddressFragment();
        } else {
            return new PaymentMethodFragment();
        }
    }

    private void applyCouponApi() {
        DiscountCouponRequest request = new DiscountCouponRequest();
        request.setValue(couponCodeEditText.getText().toString().trim());
        RetroClient.getApi().applyDiscountCoupon(request)
                .enqueue(new CustomCB<DiscountCouponApplyResponse>(this.getView()));
    }

    public void onEvent(DiscountCouponApplyResponse discountCouponApplyResponse) {
        if (discountCouponApplyResponse.getStatusCode() == 200) {
            showToast(getString(R.string.discount_success_msg));
            populateDataInOrderTotalLayout(discountCouponApplyResponse.getOrderTotalResponseModel());
        }
    }

    public void onEvent(ShoppingCartCheckoutAttributeApplyResponse applyResponse) {
        // populateDataInOrderTotalLayout(applyResponse.getOrderTotalResponseModel());
    }

    @Override
    public void onItemClick(CartProduct products) {
        callApiOfAddingProductIntoCart(products);
    }

    public void onEvent(AddtoCartResponse addtoCartResponse) {
        if (addtoCartResponse.isSuccess() && addtoCartResponse.getStatusCode() == 200) {
            List<KeyValuePair> keyValuePairs = new ArrayList<>();
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey("removefromcart");
            keyValuePair.setValue(String.valueOf(pid));
            keyValuePairs.add(keyValuePair);
            RetroClient.getApi().updateCartProductList(keyValuePairs).enqueue(new CustomCB<CartProductListResponse>(this.getView()));
        }
    }


    /*public boolean isBuyNow() {
        if (getArguments() != null) {
            return getArguments().getBoolean("isBuyNow", false);
        }
        return false;
    }*/

    @Override
    public void loginSuccessFromServer(LoginResponseModel loginOrRegistrationModel) {
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.REGISTER_DONE, true);
        isAddreeApiCallAfterRegister = true;
        showToast(getString(R.string.login_success_from_ok_dollar));
        RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
    }


    @Override
    public void loginFailedFromServer(String string) {
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, new LoginFragment()).addToBackStack(null).commit();
        }
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showLoading() {
        AppDialogs.loadingDialog(getContext(), false, "");
    }

    @Override
    public void hideLoading() {
        AppDialogs.dismissDialog();
    }


    @Override
    public void showError(String s) {

    }

    @Override
    public void savedPickupPoint(boolean isSave) {

    }

    @Override
    public void errorPickupPoint() {
    }


    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) Objects.requireNonNull(getActivity())).showHideCart(true);
    }

    @OnClick(R.id.btn_add_address)
    void selectOrAddAddress() {
        if (null != addressBottomSheet)
            addressBottomSheet.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), addressBottomSheet.getTag());
    }

    private void callSaveAddressFromAddressApi() {
        RetroClient.getApi().saveBillingAddressFromAddress(new ValuePost("" + addressID))
                .enqueue(new CustomCB<BillingAddressSaveResponse>(this.getView()));
    }

    public void onEvent(BillingAddressSaveResponse billingAddressSaveResponse) {
        if (billingAddressSaveResponse.isData() && billingAddressSaveResponse.getStatusCode() == 200) {
            if (!billingAddressSaveResponse.isDeliveryAllowed()) {
                showUnDeliveryPopup(getString(R.string.delivery_not_available));
            }
        }
        RetroClient.getApi().getShoppingCart().enqueue(new CustomCB<CartProductListResponse>(this.getView()));
    }

    private void showUnDeliveryPopup(String msg) {
        AppDialogs.customDialog(getActivity(), msg, () -> {
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, "");
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0");
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                        .replace(R.id.container, new PickupPointListFragment()).addToBackStack(null).commit();
            }
        });
    }

    private void showHideCheckoutBtn(boolean isShowCheckoutBtn) {
        if (null != checkoutBtn && null != getActivity()) {
            if (isShowCheckoutBtn) {
                checkoutBtn.setBackground(getActivity().getResources().getDrawable(R.drawable.gradient_bg));
                checkoutBtn.setTextColor(getResources().getColor(R.color.white));
                checkoutBtn.setEnabled(true);
                checkoutBtn.setClickable(true);
            } else {
                checkoutBtn.setBackgroundColor(getActivity().getResources().getColor(R.color.bt_light_gray));
                checkoutBtn.setTextColor(getResources().getColor(R.color.white));
                checkoutBtn.setEnabled(false);
                checkoutBtn.setClickable(false);
            }
        }
    }

    private void batteryPercentage() {
        Log.e("###", "-------- Battery Percentage have: " + getBatteryPercentage(getActivity()));
    }
}