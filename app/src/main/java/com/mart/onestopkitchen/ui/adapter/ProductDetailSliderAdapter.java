package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.PictureModel;

import java.util.List;

/**
 * Created by Hemnath on 12/17/2018.
 */
public class ProductDetailSliderAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<PictureModel> pictureModels;

    private OnSliderClickListener sliderClickListener;

    public ProductDetailSliderAdapter(Context context, List<PictureModel> imageUrl) {
        this.context = context;
        this.pictureModels = imageUrl;
    }

    @Override
    public int getCount() {
        return pictureModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (LinearLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int pos) {
        final int position = pos;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_productdetail_slider, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
        final ImageView placeholder = (ImageView) view.findViewById(R.id.placeholder);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.layout);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);


        if (pictureModels.get(position).getFullSizeImageUrl().isEmpty() ? pictureModels.get(position).getImageUrl().contains(".gif") : pictureModels.get(position).getFullSizeImageUrl().contains("gif")) {
            Glide.with(context)
                    .load(pictureModels.get(position).getFullSizeImageUrl().isEmpty() ? pictureModels.get(position).getImageUrl() : pictureModels.get(position).getFullSizeImageUrl())
                    .asGif()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //   .placeholder(R.drawable.placeholder)   .error(R.drawable.placeholder)
                    .into(imageView);
        } else {
            Glide.with(context)
                    .load(pictureModels.get(position).getFullSizeImageUrl().isEmpty() ? pictureModels.get(position).getImageUrl() : pictureModels.get(position).getFullSizeImageUrl())
                    //  .placeholder(R.drawable.placeholder)
                    // .error(R.drawable.placeholder)
                    .crossFade(1000)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            fadeOutImage(placeholder);
                            return false;
                        }
                    })
                    .into(imageView);
        }

        container.addView(view);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sliderClickListener != null) {
                    sliderClickListener.onSliderClick(v, position);
                }
            }
        });
        return view;
    }

    private void fadeOutImage(final ImageView img) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(1000);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                img.setVisibility(View.GONE);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        img.startAnimation(fadeOut);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void setOnSliderClickListener(OnSliderClickListener sliderClickListener) {
        this.sliderClickListener = sliderClickListener;
    }

    public interface OnSliderClickListener {
        void onSliderClick(View view, int position);
    }
}
