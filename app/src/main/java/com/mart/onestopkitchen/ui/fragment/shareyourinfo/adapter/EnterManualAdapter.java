package com.mart.onestopkitchen.ui.fragment.shareyourinfo.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.model.ManuallSize;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnterManualAdapter extends RecyclerView.Adapter<EnterManualAdapter.ViewHolder> {

    private List<ManuallSize> defaultData;
    private boolean edtState;

    public EnterManualAdapter() {
        this.defaultData = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_enter_size_manual, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtTitle.setText(defaultData.get(position).getTitle());
        setEdtState(holder.edtTitle, edtState);
        holder.edtTitle.setText(defaultData.get(position).getEdtValue());
    }

    private void setEdtState(EditText edtTitle, boolean edtState) {
        edtTitle.setEnabled(edtState);
        edtTitle.setCursorVisible(edtState);
    }

    @Override
    public int getItemCount() {
        return defaultData == null ? 0 : defaultData.size();
    }

    public void loadDataSizes(List<ManuallSize> defaultDatas, boolean edtStatus) {
        this.defaultData = defaultDatas;
        this.edtState = edtStatus;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title)
        public TextView txtTitle;
        @BindView(R.id.edt_title)
        public EditText edtTitle;
        @BindView(R.id.lin_root)
        LinearLayout linRoot;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
