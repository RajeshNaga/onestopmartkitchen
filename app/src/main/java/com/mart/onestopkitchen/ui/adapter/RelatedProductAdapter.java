package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BaseProductModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Ashraful on 11/27/2015.
 */
public class RelatedProductAdapter extends ProductAdapter {
    public RelatedProductAdapter(Context context, List<? extends BaseProductModel> productsList) {
        super(context, productsList);
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products_grid_related, parent, false);

        return new ProductSummaryHolder(itemView);
    }
}
