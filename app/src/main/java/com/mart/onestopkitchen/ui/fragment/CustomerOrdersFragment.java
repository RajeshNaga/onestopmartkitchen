package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.event.OrderDetailsEvent;
import com.mart.onestopkitchen.model.CustomerOrdersResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.adapter.CustomerOrderAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CustomerOrdersFragment extends BaseFragment {
    @BindView(R.id.recycler_view_orders)
    RecyclerView mRecyclerView;
    @BindView(R.id.order_not_fount)
    LinearLayout order_not_fount;
    @BindView(R.id.btn_continue_shopping)
    Button continueShopping;

    private CustomerOrderAdapter mAdapter = null;
    private View view = null;
    private String TAG = CustomerOrdersFragment.class.getSimpleName();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_customer_orders, container, false);
        unbinder = ButterKnife.bind(this, view);
//        Log.e(TAG, "-----------###onCreateView()###------");
        return view;
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkEventBusRegistration();
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.my_order));
        ((MainActivity) getActivity()).showHideCart(true);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new CustomerOrderAdapter(getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);

        continueShopping.setOnClickListener(btnView -> {
            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
            }
        });
//        Log.e(TAG, "---------###onViewCreated()###------");

        callGetCustomerOrdersWebservice();
    }

    private void callGetCustomerOrdersWebservice() {
        RetroClient.getApi().getCustomerOrders().enqueue(new CustomCB<CustomerOrdersResponse>(this.getView()));
    }

    public void onEvent(CustomerOrdersResponse response) {
        if (response.getStatusCode() == 200) {
            if (null != response.getOrders()) {
                if (response.getOrders().size() > 0) {
                    order_not_fount.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mAdapter.loadData(response.getOrders());
                    mAdapter.notifyDataSetChanged();
                } else {
                    order_not_fount.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
            } else {
                order_not_fount.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        }
    }

    public void onEvent(OrderDetailsEvent event) {
        CustomerOrderDetailsFragment fragment = new CustomerOrderDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("orderId", event.getId());
        fragment.setArguments(args);
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, fragment)
                    .addToBackStack(null).commitAllowingStateLoss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
//        Log.e(TAG, "----------###onStart()###------");
    }

    @Override
    public void onResume() {
        super.onResume();
//        Log.e(TAG, "--------###OnResume()###------");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "---------###onStop()###------");
    }

    @Override
    public void onPause() {
        super.onPause();
//        Log.e(TAG, "--------###onPause()###------");
    }

    @Override
    public void onDestroyView() {
        releaseObj();
        super.onDestroyView();
//        Log.e(TAG, "------------onDestroyView()###------");
    }

    @Override
    public void onDestroy() {
        releaseObj();
        super.onDestroy();
        Log.e(TAG, "--------onDestroy()###------");
    }

    private void releaseObj() {
        if (null != mAdapter) {
            mAdapter.releaseObj();
            mAdapter = null;
        }
        if (null != view) {
            view = null;
        }
        if (null != TAG) {
            TAG = null;
        }
    }
}
