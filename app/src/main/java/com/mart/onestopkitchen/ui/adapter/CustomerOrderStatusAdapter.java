package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.OrderNotes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("SimpleDateFormat")
public class CustomerOrderStatusAdapter extends RecyclerView.Adapter<CustomerOrderStatusAdapter.OrderNotesHolder> {
    private Context context=null;
    private List<OrderNotes> deliveryResponsesList;
    private SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private SimpleDateFormat output = new SimpleDateFormat("EEE dd MMM yyyy hh:mm:ss a");

    public CustomerOrderStatusAdapter(Context context, List<OrderNotes> searchResponsesList) {
        this.context = context;
        this.deliveryResponsesList = searchResponsesList;
    }

    @NonNull
    @Override
    public OrderNotesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_status, parent, false);
        return new OrderNotesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderNotesHolder holder, final int position) {
        holder.order_status.setText(deliveryResponsesList.get(position).getNote());
        String demoConverter = deliveryResponsesList.get(position).getCreatedOn();
        Date d = null;
        try {
         //   input.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = input.parse(demoConverter);
            String formattedDate = input.format(date);
            d = input.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formatted = output.format(d);
        holder.order_status_date.setText(formatted);
        if (position == deliveryResponsesList.size() - 1) {
            holder.lastView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return deliveryResponsesList.size();
    }


    public interface OnItemClickListener {
        void onItemClick(String pId);
    }

    class OrderNotesHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_order_status)
        TextView order_status;
        @BindView(R.id.tv_order_status_date)
        TextView order_status_date;
        @BindView(R.id.lastview)
        View lastView;

        OrderNotesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void releaseObj() {
        if (null != deliveryResponsesList) {
            deliveryResponsesList = null;
        }
        if (null != context) {
            context = null;
        }
        if (null != input) {
            input = null;
        }
        if (null != output) {
            output = null;
        }
    }
}