package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SliderMenuAdapter extends RecyclerView.Adapter<SliderMenuAdapter.ViewHolder> {
    private List<String> productList;
    private OnMenuClick menuClick;

    public SliderMenuAdapter(OnMenuClick menuClick) {
        this.productList = new ArrayList<>();
        this.menuClick = menuClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.category_custom, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.subName.setText(AppUtils.capitalizeWords(productList.get(position)));
        holder.subName.setOnClickListener(view -> {
            menuClick.onViewClicked(position);
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.NAVIGATION_CLASS_NAME, String.valueOf(position));
        });
    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    public void loadData(List<String> strings) {
        this.productList = strings;
        notifyDataSetChanged();
    }

    public interface OnMenuClick {
        void onViewClicked(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.subName)
        TextView subName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void onReleaseObj() {
        if (null != productList) {
            productList = null;
        }
        if (null != menuClick) {
            menuClick = null;
        }
    }
}
