package com.mart.onestopkitchen.ui.fragment.submenu;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.addsubmenu.CategeryItemsItem;
import com.mart.onestopkitchen.model.addsubmenu.MainItemsItem;
import com.mart.onestopkitchen.model.addsubmenu.SubMenuResponseModel;
import com.mart.onestopkitchen.mvp.ui.filteroption.FragmentListener;
import com.mart.onestopkitchen.ui.fragment.ProductListFragmentFor3_8;
import com.mart.onestopkitchen.ui.fragment.submenu.adapter.SubMenuItemAdapter;
import com.mart.onestopkitchen.ui.fragment.submenu.adapter.fragmentadapter.AddSubMenuLeftAdapter;
import com.mart.onestopkitchen.ui.fragment.submenu.adapter.fragmentadapter.AddSubMenuSecondLeftAdapter;
import com.mart.onestopkitchen.utils.AppUtils;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Ashraful on 2/11/2016.
 */
public class AddSubMenuFragmentChild extends Fragment implements SubMenuItemAdapter.ViewClick, AddSubMenuLeftAdapter.OnViewClick, AddSubMenuSecondLeftAdapter.OnSecondViewClick {


    @BindView(R.id.rv_categeries)
    RecyclerView rvCategeries;
    Unbinder unbinder;
    private FragmentListener fragmentListener;
    private String cateName = "";
    private int cateId = 0;
    private SubMenuResponseModel category;
    private AddSubMenuLeftAdapter subMenuItemAdapter;
    private AddSubMenuSecondLeftAdapter subMenuSecondLeftAdapter;
    private Context mContext;
    private List<CategeryItemsItem> categoryList;

    public static AddSubMenuFragmentChild newInstance(SubMenuResponseModel category) {
        AddSubMenuFragmentChild fragment = new AddSubMenuFragmentChild();
        Bundle args = new Bundle();
        args.putSerializable("CATEGORY", category);
        fragment.setArguments(args);
        return fragment;
    }

    public static AddSubMenuFragmentChild newInstanceSubCate(List<CategeryItemsItem> category) {
        AddSubMenuFragmentChild fragment = new AddSubMenuFragmentChild();
        Bundle args = new Bundle();
        args.putSerializable("SUBCATEGORY", (Serializable) category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            fragmentListener = (FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement fragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_menu_second, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (getArguments().getSerializable("CATEGORY") != null)
                category = (SubMenuResponseModel) getArguments().getSerializable("CATEGORY");
            if (getArguments().getSerializable("SUBCATEGORY") != null)
                categoryList = (List<CategeryItemsItem>) getArguments().getSerializable("SUBCATEGORY");
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iniUi();
    }

    private void iniUi() {
        mContext = getContext();
        initRv();
    }

    private void initRv() {
        if (category != null) {
            subMenuItemAdapter = new AddSubMenuLeftAdapter(this, mContext, category.getMainItems());
            rvCategeries.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvCategeries.setAdapter(subMenuItemAdapter);
        } else if (categoryList != null && categoryList.size() > 0) {
            subMenuSecondLeftAdapter = new AddSubMenuSecondLeftAdapter(this, mContext, categoryList);
            rvCategeries.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvCategeries.setAdapter(subMenuSecondLeftAdapter);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void viewClickEvent() {

    }

    private void logD(String tag, String msg) {
        AppUtils.appLog(tag, msg);
    }

    @Override
    public void viewClicked(List<CategeryItemsItem> pos, MainItemsItem mainItemsItem) {
        if (pos.size() > 0) {
            fragmentListener.nextFragment(AddSubMenuFragmentChild.newInstanceSubCate(pos));
        } else {
            // do
            fragmentListener.nextFragment(new ProductListFragmentFor3_8());
        }

    }


    @Override
    public void viewSecondViewClicked(CategeryItemsItem categeryItemsItem, int pos) {
        // if (pos.size() > 0) {
        //     fragmentListener.nextFragment(AddSubMenuFragmentChild.newInstanceSubCate(pos));
        //} else {
        //     // do
        //      fragmentListener.nextFragment(new ProductListFragmentFor3_8());
        //  }
    }
}
