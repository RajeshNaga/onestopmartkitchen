package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ShippingMethod;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.postrequest.ValuePost;
import com.mart.onestopkitchen.networking.response.ShippingMethodRetrievalResponse;
import com.mart.onestopkitchen.networking.response.ShippingMethodSelttingResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.adapter.FragmentClass;
import com.mart.onestopkitchen.ui.customview.CheckableLinearLayout;
import com.mart.onestopkitchen.ui.customview.RadioGridGroupforReyMaterial;
import com.mart.onestopkitchen.ui.views.MethodSelctionProcess;
import com.mart.onestopkitchen.utils.Language;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//import com.rey.material.widget.RadioButton;

/**
 * Created by Ashraful on 12/7/2015.
 */
public class ShippingMethodFragment extends BaseFragment implements View.OnClickListener {
    int id = 0;
    @BindView(R.id.rg_shipiingMethod)
    RadioGridGroupforReyMaterial radioGridGroup;
    @BindView(R.id.btn_continue)
    Button continueBtn;
    MethodSelctionProcess methodSelctionProcess;
    String shippingMethodValue = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shipping_method, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        callShippingMethodRetrievalApi();
        continueBtn.setOnClickListener(this);

    }

    private void callShippingMethodRetrievalApi() {
        RetroClient.getApi().getShippingMethod().enqueue(new CustomCB<ShippingMethodRetrievalResponse>(this.getView()));
    }

    public void onEvent(ShippingMethodRetrievalResponse shippingMethodRetrievalResponse) {
        addMethodRadioGroup(shippingMethodRetrievalResponse.getShippingMethods());
    }


    private void addMethodRadioGroup(List<ShippingMethod> shippingMethods) {
        methodSelctionProcess = new MethodSelctionProcess(radioGridGroup);
        for (ShippingMethod method : shippingMethods)
            generateRadioButton(method);
    }


    private void generateRadioButton(final ShippingMethod method) {
        CheckableLinearLayout linearLayout = (CheckableLinearLayout) getLayoutInflater().
                inflate(R.layout.item_shipping_method, radioGridGroup, false);

        TextView textView = (TextView) linearLayout.findViewById(R.id.tv_shippingMethodDescription);
        final RadioButton radioButton = (RadioButton) linearLayout.findViewById(R.id.rb_shippingChoice);
        radioButton.setText(method.getName());
        radioButton.setId(++id);
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            textView.setGravity(Gravity.RIGHT);
        }

        if (isPreselected(method)) {
            radioButton.setChecked(true);
            shippingMethodValue = method.getName() + "___" + method.getShippingRateComputationMethodSystemName();

        }
        if (method.getDescription() != null)
            textView.setText(Html.fromHtml(method.getDescription()));

        radioGridGroup.addView(linearLayout);


        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    shippingMethodValue = method.getName() + "___" + method.getShippingRateComputationMethodSystemName();
                    methodSelctionProcess.resetRadioButton(buttonView.getId());
                }
            }
        });

        linearLayout.setOnCheckedChangeListener(new CheckableLinearLayout.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(View checkableView, boolean isChecked) {
                if (isChecked)
                    radioButton.setChecked(true);
            }
        });
    }

    private boolean isPreselected(ShippingMethod shippingMethod) {
        return shippingMethod.isSelected();
    }


    @Override
    public void onClick(View v) {
        int resourceId = v.getId();
        if (resourceId == R.id.btn_continue)
            System.out.println(shippingMethodValue);
        saveShippingMethod();
    }

    private void saveShippingMethod() {
        ValuePost valuePost = new ValuePost();
        valuePost.setValue(shippingMethodValue);
        RetroClient.getApi().setShippingMethod(valuePost)
                .enqueue(new CustomCB<ShippingMethodSelttingResponse>(this.getView()));
    }

    public void onEvent(ShippingMethodSelttingResponse response) {
        ((CheckoutStepFragment) getParentFragment()).replaceFragment(FragmentClass.ShippingMethod);

    }

}
