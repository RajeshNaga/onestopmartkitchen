package com.mart.onestopkitchen.ui.adapter

import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mart.onestopkitchen.BR

import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.model.OrdersModel


open class OrderGroupReceiptPageAdapter(var mContext: Context, private val orderNumbers: MutableList<OrdersModel>) : RecyclerView.Adapter<OrderGroupReceiptPageAdapter.OrderGroupViewHolder>() {
    protected var inflater: LayoutInflater? = null

    init {
        inflater = LayoutInflater.from(mContext)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderGroupViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater!!, R.layout.layout_order_group_receipt_page, parent, false)
        return OrderGroupViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return orderNumbers.size
    }

    override fun onBindViewHolder(holder: OrderGroupViewHolder, position: Int) {
//        orderNumbers[position]?.also { holder.bind(it) }
        holder.bind(orderNumbers[position])
    }

    inner class OrderGroupViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: OrdersModel) {
            binding.setVariable(BR.orderModel, data)
            binding.setVariable(BR.position, adapterPosition)
            binding.executePendingBindings()
        }
    }


}