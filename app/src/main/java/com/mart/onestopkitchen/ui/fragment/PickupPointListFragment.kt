package com.mart.onestopkitchen.ui.fragment

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.networking.response.PickupPointAddressResponse
import com.mart.onestopkitchen.presenter.PickupPointPresenter
import com.mart.onestopkitchen.ui.adapter.PickupPointAdapter
import com.mart.onestopkitchen.utils.PickupPointViewModel
import kotlinx.android.synthetic.main.fragment_pickup_point_list.*
import kotlinx.android.synthetic.main.layout_recyclerview.*
import javax.annotation.Nullable


/**
 * Created by Akash Garg
 */

class PickupPointListFragment : BaseFragment()/*, PickupPointView*/, PickupPointAdapter.SelectedPickupPoint {
    private val TAG: String? = "PickupPointListFragment"

    private var viewBinding: ViewDataBinding? = null
    private var pickupPointPresenter: PickupPointPresenter? = null
    private lateinit var pickupPoinrViewModel: PickupPointViewModel

    @Nullable
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(com.mart.onestopkitchen.R.layout.fragment_pickup_point_list, container, false)
//        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pickup_point_list, container, false)
        activity!!.title = getString(com.mart.onestopkitchen.R.string.choose_pickup_point_location)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar?.visibility = View.VISIBLE
//        pickupPointPresenter = PickupPointPresenter(view, activity!!, this)

        activity?.let {
            pickupPoinrViewModel = ViewModelProviders.of(it).get(PickupPointViewModel::class.java)
            pickupPoinrViewModel.getPickupPointList()?.observe(it, Observer { res ->
                res?.let { response ->
                    if (res.statusCode == 401)
                        showToast(getString(R.string.no_internet))
                    else
                        updateData(response).takeIf { response.pickupPoints.size > 0 }
                                ?: noDataFound()
                }
            })
        }
    }

    private fun updateData(pickupPointList: PickupPointAddressResponse?) {
        progressBar?.visibility = View.GONE
        pickupPointList?.let { setPickupPointList(it) }
    }

    private fun noDataFound() {
        progressBar?.visibility = View.GONE
        recyclerView?.visibility = View.GONE
        tv_NoPickupPoint?.visibility = View.VISIBLE
    }

    private fun setPickupPointList(model: PickupPointAddressResponse) {
        recyclerView?.adapter = PickupPointAdapter(activity!!, model, this)
        viewBinding?.executePendingBindings()
    }

    override fun onSelectedPickupPoint() {
        fragmentManager!!.popBackStack()
    }

    override fun showLoader() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        progressBar?.visibility = View.GONE
    }

    override fun onDestroyView() {
        Log.e(TAG, "--------onDestroyView---- ")
        pickupPoinrViewModel.releaseObj()
        super.onDestroyView()
    }

    /*override fun loadData(pickupPoints: PickupPointAddressResponse) {
        progressBar?.visibility = View.GONE
        setPickupPointList(pickupPoints)
      }
       override fun noDataFound() {
          progressBar?.visibility = View.GONE
          recyclerView?.visibility = View.GONE
          tv_NoPickupPoint?.visibility = View.VISIBLE
      }
         override fun error(error: String?) {
           Log.e(TAG, "-----pickupPoinrViewModel---error----- $error")
   }*/
}