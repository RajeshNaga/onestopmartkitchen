package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.AttributeControlValue;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductAttribute;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.PriceResponse;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hemnath on 10/31/2018.
 */
public class ColorViewAdapter extends RecyclerView.Adapter<ColorViewAdapter.ColorViewHolder> {

    List<AttributeControlValue> values;
    String key;
    private int selectedIndex = -1;
    private Context context;
    private onSelectColorAdapterListener onSelectColorAdapterListener;

    public ColorViewAdapter(Context context, onSelectColorAdapterListener onSelectColorAdapterListener) {
        this.context = context;
        this.onSelectColorAdapterListener = onSelectColorAdapterListener;
        values = new ArrayList<>();
    }

    public void loadData(List<AttributeControlValue> values, ProductAttribute productAttribute) {
        key = getKey(productAttribute);
        this.values = values;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ColorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.color_linear_layout, viewGroup, false);
        return new ColorViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorViewHolder colorViewHolder, final int i) {

        if (i == selectedIndex) {
            selectColorCircle(i, 2, colorViewHolder);
        } else {
            selectColorCircle(i, 12, colorViewHolder);
        }

        colorViewHolder.iv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedIndex = i;

                Map<String, String> valueTextPairMap = new HashMap<>();
                valueTextPairMap.put(String.valueOf(values.get(i).getId()), key);

                callColorWebservice(onSelectColorAdapterListener.getSelectedColorFromAdapter(valueTextPairMap));
                notifyDataSetChanged();
            }
        });
    }

    private void selectColorCircle(int i, int strokeWidth, ColorViewHolder colorViewHolder) {
        final GradientDrawable shapeDrawable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            shapeDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable.bg_color_square, context.getTheme());
        } else {
            shapeDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable.bg_color_square);
        }

        try {
            shapeDrawable.setColor(Color.parseColor(values.get(i).getColorSquaresRgb()));
            shapeDrawable.setStroke(strokeWidth, Color.TRANSPARENT, 10, 10);
        } catch (Exception ex) {
            // who cares
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            colorViewHolder.iv_view.setBackground(shapeDrawable);
        } else {
            colorViewHolder.iv_view.setBackgroundDrawable(shapeDrawable);
        }
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public String getKey(ProductAttribute productAttribute) {
        String key = String.format("%s_%s_%s_%s", "product_attribute", productAttribute.getProductId()
                , productAttribute.getProductAttributeId(), productAttribute.getId());
        return key;
    }

    public void callColorWebservice(List<KeyValuePair> valuePair) {
        RetroClient.getApi().getUpdatedPrice(ProductDetailFragment.productModel.getId(), valuePair)
                .enqueue(new CustomCB<PriceResponse>(ProductDetailFragment.self.getView()));
    }

    public interface onSelectColorAdapterListener {
        List<KeyValuePair> getSelectedColorFromAdapter(Map<String, String> pairValue);
    }

    public class ColorViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_view;

        public ColorViewHolder(View itemView) {
            super(itemView);
            iv_view = (ImageView) itemView.findViewById(R.id.iv_color_view);
        }

    }
}
