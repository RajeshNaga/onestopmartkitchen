package com.mart.onestopkitchen.ui.fragment;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DatePickerDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.CustomerRegistrationInfo;
import com.mart.onestopkitchen.model.LoginData;
import com.mart.onestopkitchen.model.LoginResponse;
import com.mart.onestopkitchen.model.RegistrationResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.UiUtils;

import java.util.Calendar;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mart-110 on 12/9/2015.
 */
public class RegisterFragment extends BaseFragment {

   /* @BindView(R.id.religion_dropdown)
    MaterialSpinner religionDropdown;*/


    /*@BindView(R.id.language_dropdown_register)
    MaterialSpinner languageDropdown;*/

    private final int PERMISSIONS_REQUEST_GET_ACCOUNTS = 101;
    @BindView(R.id.tvName)
    TextView nameTitleTextView;
    @BindView(R.id.customer_name)
    TextView customerNameTextView;
    @BindView(R.id.dateOfBirth)
    TextView dateOfBirthTextView;

   /* @BindView(R.id.company_info)
    TextView companyInfoTextView;*/
    @BindView(R.id.customer_email)
    TextView emailTextView;
    @BindView(R.id.gender)
    TextView genderTextView;
    @BindView(R.id.et_customer_first_name)
    EditText customerFirstNameEditText;
    @BindView(R.id.et_customer_last_name)
    EditText customerLastNameEditText;

    /*@BindView(R.id.et_company_info)
    EditText companyInfoEditText;*/
    @BindView(R.id.et_customer_email)
    EditText emailEditText;
    @BindView(R.id.rb_male)
    RadioButton genderMaleRadioButton;
    @BindView(R.id.rb_female)
    RadioButton genderFemaleRadioButton;
    @BindView(R.id.genderRadioGroup)
    RadioGroup genderRadioGroup;

    /*@BindView(R.id.cb_newsletter)
    CheckBox cbNewsLetter;*/

    /*@BindView(R.id.et_password)
    EditText passwordEditText;

    @BindView(R.id.et_confirm_password)
    EditText confirmPasswordEditText;
    @BindView(R.id.tv_password)
    TextView passwordTextView;*/
    @BindView(R.id.btn_save)
    Button saveBtn;
    @BindView(R.id.customer_username)
    TextView usernameTextView;

    /*@BindView(R.id.customer_phone)
    TextView phoneTextView;
    @BindView(R.id.et_customer_phone)
    EditText phoneEditText;*/
    @BindView(R.id.et_customer_username)
    EditText usernameEditText;
    Calendar myCalendar = null;
    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            dateOfBirthTextView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            saveBtn.setVisibility(View.VISIBLE);
        }

    };
    private CustomerRegistrationInfo customerInfo;
    private String TAG = RegisterFragment.class.getSimpleName();
    ;
    private String wantPermission = Manifest.permission.GET_ACCOUNTS;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_basic_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkEventBusRegistration();
        getActivity().setTitle(getString(R.string.register));
        requestPermissions();
        clearUI();
        hideTextPanels();
        initEditButtonsAction();

//        setReligionDropdown();

//        setLanguageDropdown();

    }

    /*private void setReligionDropdown()
    {
        religionDropdown.setItems("Muslim", "Christian", "Hindu", "Bouddho");
        religionDropdown.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>()
        {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item)
            {
                //changeLanguage(item);
                //Snackbar.make(view, item + " selected", Snackbar.LENGTH_LONG).show();
            }
        });
        religionDropdown.setOnNothingSelectedListener(new MaterialSpinner.OnNothingSelectedListener()
        {
            @Override public void onNothingSelected(MaterialSpinner spinner)
            {
                //Snackbar.make(spinner, "Language Not Changed", Snackbar.LENGTH_LONG).show();
            }
        });
    }*/



   /* private void setLanguageDropdown()
    {
        languageDropdown.setItems("English", "Bengali");

        languageDropdown.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>()
        {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item)
            {

                //changeLanguage(item);
                //Snackbar.make(view, item + " selected", Snackbar.LENGTH_LONG).show();
            }
        });
        languageDropdown.setOnNothingSelectedListener(new MaterialSpinner.OnNothingSelectedListener()
        {


            @Override public void onNothingSelected(MaterialSpinner spinner)
            {
                //Snackbar.make(spinner, "Language Not Changed", Snackbar.LENGTH_LONG).show();
            }
        });
    }
*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
       /* MenuItem item = menu.findItem(R.id.menu_cart);
        item.setVisible(false);
        getActivity().invalidateOptionsMenu();*/
    }

    private void callRegisterWebService() {
        RetroClient.getApi().preformRegistration(customerInfo).enqueue(new CustomCB<RegistrationResponse>(this.getView()));
    }

    public void callLoginWebservice(LoginData loginData) {
        RetroClient.getApi().performLogin(loginData).enqueue(new CustomCB<LoginResponse>(this.getView()));
    }

    public void onEvent(LoginResponse response) {
        if (response.getToken() != null) {
            preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, response.getToken());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
            /*NetworkUtilities.token=response.getToken();*/
            NetworkUtil.setToken(response.getToken());
//            getFragmentManager().beginTransaction().replace(R.id.container, new HomePageFragment()).commit();
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, new BillingAddressFragment()).commit();
        }
    }

    public void onEvent(RegistrationResponse response) {
        if (response.getStatusCode() == 400) {
            saveBtn.setEnabled(true);
            String errors = getString(R.string.error_register_customer) + "\n";
            if (response.getErrorList().length > 0) {
                for (int i = 0; i < response.getErrorList().length; i++) {
                    errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
                }

                if (errors.contains(" The specified email already exists")) {
                    /*emailEditText.setEnabled(true);
                    emailEditText.setCursorVisible(true);*/
                }
                Toast.makeText(getActivity(), errors, Toast.LENGTH_LONG).show();
            }
        } else {
            preferenceService.SetPreferenceValue(PreferenceService.REGISTER_DONE, true);

            Toast.makeText(getActivity(), getString(R.string.customer_succes_msg), Toast.LENGTH_LONG).show();
//            callLoginWebservice(new LoginData(customerInfo.getEmail(), customerInfo.getPassword()));
        }
    }

    private void hideTextPanels() {
        nameTitleTextView.setVisibility(View.GONE);
        customerNameTextView.setVisibility(View.GONE);
        emailTextView.setVisibility(View.GONE);
//        companyInfoTextView.setVisibility(View.GONE);
        genderTextView.setVisibility(View.GONE);
        usernameTextView.setVisibility(View.GONE);
        saveBtn.setText(getString(R.string.register_new));
        dateOfBirthTextView.setText("dd/mm/yyyy");
//        passwordTextView.setVisibility(View.GONE);
//        phoneTextView.setVisibility(View.GONE);


        // TODO more work needed like kotlin

    }

    private void clearUI() {
        customerNameTextView.setText("");
        dateOfBirthTextView.setText("");
        emailTextView.setText("");
//        companyInfoTextView.setText("");
        genderTextView.setText("");
        usernameTextView.setText("");
//        phoneTextView.setText("");

        customerFirstNameEditText.setText("");
        customerLastNameEditText.setText("");
        emailEditText.setText("");
//        companyInfoEditText.setText("");
        usernameEditText.setText("");
//        phoneEditText.setText("");
        // TODO more work needed like kotlin
       /* try {
            String[] email = AppUtils.getDefaultEmail().split(",");
            if (email.length > 0) {
                emailEditText.setText(email[0]);
                emailEditText.setEnabled(false);
                emailEditText.setCursorVisible(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void initEditButtonsAction() {

        dateOfBirthTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar c = Calendar.getInstance();
                if (myCalendar != null) {
                    c = myCalendar;
                }

                new DatePickerDialog(getActivity(), dateSetListener, c
                        .get(Calendar.YEAR), c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performRegistration();
                saveBtn.setEnabled(false);
                UiUtils.hideSoftKeyboard(getActivity());
            }
        });


    }

    private void performRegistration() {
        getCustomerInfo();
        if (isValidCustomerInfo()) {
            callRegisterWebService();
        }
    }

    private boolean isValidCustomerInfo() {
        return true;
    }

    private void getCustomerInfo() {
        customerInfo = new CustomerRegistrationInfo();
        customerInfo.setFirstName(customerFirstNameEditText.getText().toString());
        customerInfo.setLastName(customerLastNameEditText.getText().toString());
        customerInfo.setEmail(emailEditText.getText().toString());
//        customerInfo.setPhone(phoneEditText.getText().toString());
//        customerInfo.setCompany(companyInfoEditText.getText().toString());

        if (myCalendar != null) {
            customerInfo.setDateOfBirthYear(myCalendar.get(Calendar.YEAR));
            customerInfo.setDateOfBirthMonth(myCalendar.get(Calendar.MONTH) + 1);
            customerInfo.setDateOfBirthDay(myCalendar.get(Calendar.DAY_OF_MONTH));
        }
        if (genderMaleRadioButton.isChecked()) {
            customerInfo.setGender("M");
        } else if (genderFemaleRadioButton.isChecked()) {
            customerInfo.setGender("F");
        }
//        customerInfo.setNewsletter(cbNewsLetter.isChecked());
        customerInfo.setPassword("1234567");
        customerInfo.setConfirmPassword("1234567");
        customerInfo.setUsername(usernameEditText.getText().toString());
    }

    private void requestPermissions() {
        if (!checkPermission(wantPermission)) {
            requestPermission(wantPermission);
        } else {
            getEmails();
        }
    }

    private void getEmails() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(getActivity()).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                Log.e(TAG, String.format("%s - %s", account.name, account.type));
            }
        }
    }

    private boolean checkPermission(String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(getActivity(), permission);
            if (result == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private void requestPermission(String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
            Toast.makeText(getActivity(), "Get account permission allows us to get your email",
                    Toast.LENGTH_LONG).show();
        }
        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, PERMISSIONS_REQUEST_GET_ACCOUNTS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_GET_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getEmails();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

}
