package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.CartProduct;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Updated By by Akash Garg on 06/07/2019.
 */
public class OrderDetailsProductAdapter extends RecyclerView.Adapter<OrderDetailsProductAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<CartProduct> cartProductList;
    private String orderStatus;
    private OnRatingClick listener;

    public OrderDetailsProductAdapter(Context context, ArrayList<CartProduct> cartProductList, String orderStatus) {
        this.mContext = context;
        this.cartProductList = cartProductList;
        this.orderStatus = orderStatus;
    }

    public void setOnRatingListener(OnRatingClick listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_item_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            CartProduct productModel = cartProductList.get(position);
            if (null != productModel) {

                if (null != productModel.getAttributeInfo()) {
                    if (productModel.getAttributeInfo().equalsIgnoreCase("")) {
                        holder.Tv_attributeInfo.setVisibility(View.GONE);
                    } else {
                        holder.Tv_attributeInfo.setVisibility(View.VISIBLE);
                        setProductAttrInfo(holder.Tv_attributeInfo, productModel.getAttributeInfo());
                    }
                } else {
                    holder.Tv_attributeInfo.setVisibility(View.GONE);
                }

                if (productModel.getQuantity() > 1) {
                    holder.tv_quantity.setText(String.valueOf(productModel.getQuantity()));
                } else {
                    holder.tv_quantity.setText(String.valueOf(productModel.getQuantity()));
                }

                if (null != productModel.getDiscountPercentage() && productModel.getDiscountPercentage().equalsIgnoreCase("0")) {
                    if (!productModel.getOldPrice().equalsIgnoreCase("0 MMK")) {
                        holder.tv_product_acturl_price.setText(productModel.getUnitPrice());
                        holder.tv_actual_price.setText(productModel.getOldPrice());
                        holder.offer.setText("");
                    } else {
                        holder.tv_product_acturl_price.setText(productModel.getUnitPrice());
                        holder.tv_actual_price.setText("");
                        holder.offer.setText("");
                    }
                } else {
                    holder.offer.setText(productModel.getDiscountPercentage() + "%");
                    holder.tv_product_acturl_price.setText(productModel.getUnitPrice());
                }

                if (null != productModel.getRatingModel() && null != productModel.getRatingModel().getRatingSum())
                    holder.tv_rating_number.setText(productModel.getRatingModel().getRatingSum());

                if (null != productModel.getRatingModel() && null != productModel.getRatingModel().getTotalReviews())
                    holder.tv_rating_reviews.setText(productModel.getRatingModel().getTotalReviews() + " Reviews");


                if (null != productModel.getPicture() && null != productModel.getPicture().getImageUrl()) {
                    Glide.with(mContext).load(productModel.getPicture().getImageUrl()).fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.productImage);
                }


                holder.tv_actual_price.setPaintFlags(holder.tv_actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.tv_actual_price.setText(productModel.getActualPrice());
                holder.productName.setText(productModel.getProductName());

                if (orderStatus.contains("ျပည့္စံု") || orderStatus.contains("Delivered")) {
                    holder.lin_write_review.setVisibility(View.VISIBLE);
                    holder.view_hide.setVisibility(View.VISIBLE);
                } else {
                    holder.view_hide.setVisibility(View.GONE);
                    holder.lin_write_review.setVisibility(View.GONE);
                }
                float rating;
                try {
                    rating = Float.parseFloat(productModel.getRatingModel().getRatingSum());
                } catch (Exception e) {
                    rating = 0;
                }
                holder.rating.setRating(rating);

                holder.lin_write_review.setOnClickListener(v -> {
                    if (null != listener) {
                        listener.OnRatingClickListener(productModel.getProductId());
                    }
                });

            }
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return cartProductList.size() > 0 ? cartProductList.size() : 0;
    }

    private void setProductAttrInfo(TextView tv_attributeInfo, String productAttributeInfo) {
        StringBuilder attributes = new StringBuilder();
        if (null != productAttributeInfo && productAttributeInfo.contains("&quot;")) {
            productAttributeInfo = productAttributeInfo.replace("&quot;", " ");
        }
        String[] attr = new String[0];
        if (productAttributeInfo != null) {
            attr = productAttributeInfo.split("<br />");
        }
        for (int i = 0; i < attr.length; i++) {
            if (i == attr.length - 1) {
                attributes.append(attr[i]).append("");
            } else attributes.append(attr[i]).append("\n");
        }
        if (!attributes.toString().trim().isEmpty()) {
            tv_attributeInfo.setVisibility(View.VISIBLE);
            tv_attributeInfo.setText(attributes.toString());
        } else {
            tv_attributeInfo.setVisibility(View.INVISIBLE);
        }
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_productImage)
        ImageView productImage;
        @BindView(R.id.offer)
        TextView offer;
        @BindView(R.id.Tv_attributeInfo)
        TextView Tv_attributeInfo;
        @BindView(R.id.tv_productName)
        TextView productName;
        @BindView(R.id.tv_product_acturl_price)
        TextView tv_product_acturl_price;
        @BindView(R.id.tv_actual_price)
        TextView tv_actual_price;
        @BindView(R.id.tv_rating_number)
        TextView tv_rating_number;
        @BindView(R.id.tv_rating_reviews)
        TextView tv_rating_reviews;
        @BindView(R.id.lin_qu)
        LinearLayout row_quantity;
        @BindView(R.id.lin_write_review)
        LinearLayout lin_write_review;
        @BindView(R.id.rating)
        AppCompatRatingBar rating;
        @BindView(R.id.view_hide)
        View view_hide;
        @BindView(R.id.tv_quantity)
        TextView tv_quantity;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnRatingClick {
        void OnRatingClickListener(int productId);
    }
}

/*

   private PreferenceService preferenceService;
    private String orderStatus;

    public OrderDetailsProductAdapter(Context context, List productsList, Fragment fragment, PreferenceService preferenceService, String status) {
        super(context, productsList, fragment, preferenceService);
        this.preferenceService = preferenceService;
        this.orderStatus = status;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = 0;
        layout = R.layout.order_details_item_list;
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(layout, parent, false);
        return new ProductSummaryHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder bindViewHolder, final int position) {
        try {
            if (bindViewHolder instanceof ProductSummaryHolder) {
                final CartProduct productModel = products.get(position);
                ProductSummaryHolder holder = (ProductSummaryHolder) bindViewHolder;
                holder.productName.setText(productModel.getProductName());
                if (null != productModel.getAttributeInfo()) {
                    if (productModel.getAttributeInfo().equalsIgnoreCase("")) {
                        holder.Tv_attributeInfo.setVisibility(View.GONE);
                    } else {
                        holder.Tv_attributeInfo.setVisibility(View.VISIBLE);
                        setProductAttrInfo(holder.Tv_attributeInfo, productModel.getAttributeInfo());
                    }
                } else {
                    holder.Tv_attributeInfo.setVisibility(View.GONE);
                }
                holder.tv_actual_price.setText(productModel.getActualPrice());

                if (productModel.getQuantity() > 1) {
                    holder.tv_quantity.setText(String.valueOf(productModel.getQuantity()));
                } else {

                    holder.tv_quantity.setText(String.valueOf(productModel.getQuantity()));
                }
                if (productModel.getDiscountPercentage().equalsIgnoreCase("0")) {
                    if (!productModel.getOldPrice().equalsIgnoreCase("0 MMK")) {
                        holder.tv_product_acturl_price.setText(productModel.getUnitPrice());
                        holder.tv_actual_price.setText(productModel.getOldPrice());
                        holder.offer.setText("");
                    } else {
                        holder.tv_product_acturl_price.setText(productModel.getUnitPrice());
                        holder.tv_actual_price.setText("");
                        holder.offer.setText("");
                    }
                } else {
                    holder.offer.setText(productModel.getDiscountPercentage() + "%");
                    holder.tv_product_acturl_price.setText(productModel.getUnitPrice());
                }
                holder.tv_rating_number.setText(productModel.getRatingModel().getRatingSum());
                holder.tv_rating_reviews.setText(productModel.getRatingModel().getTotalReviews() + " Reviews");
                holder.tv_actual_price.setPaintFlags(holder.tv_actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
                    holder.productName.setRotationY(180);
                }
                try {
                    Glide.with(context).load(productModel.getPicture().getImageUrl()).fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.productImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (orderStatus.contains("ျပည့္စံု") || orderStatus.contains("Delivered")) {
                    holder.lin_write_review.setVisibility(View.VISIBLE);
                    holder.view_hide.setVisibility(View.VISIBLE);
                } else {
                    holder.view_hide.setVisibility(View.GONE);
                    holder.lin_write_review.setVisibility(View.GONE);
                }
                float rating;
                try {
                    rating = Float.parseFloat(productModel.getRatingModel().getRatingSum());
                } catch (Exception e) {
                    rating = 0;
                }
                holder.rating.setRating(rating);
                holder.lin_write_review.setOnClickListener(v -> fragment.getFragmentManager().beginTransaction()
                        .add(R.id.container, new RatingAndReviewFragment("UserReview", 0, productModel.getProductId(), ""))
                        .addToBackStack(null)
                        .commit());
            }
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    private void setProductAttrInfo(TextView tv_attributeInfo, String productAttributeInfo) {
        StringBuilder attributes = new StringBuilder();
        if (null != productAttributeInfo && productAttributeInfo.contains("&quot;")) {
            productAttributeInfo = productAttributeInfo.replace("&quot;", " ");
        }
        String[] attr = productAttributeInfo.split("<br />");
        for (int i = 0; i < attr.length; i++) {
            if (i == attr.length - 1) {
                attributes.append(attr[i]).append("");
            } else attributes.append(attr[i]).append("\n");
        }
        if (!attributes.toString().trim().isEmpty()) {
            tv_attributeInfo.setVisibility(View.VISIBLE);
            tv_attributeInfo.setText(attributes.toString());
        } else {
            tv_attributeInfo.setVisibility(View.INVISIBLE);
        }
    }

    private void addMultiColoredTextInView(String firstWord, String secondWord, TextView textView) {
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            textView.setRotationY(180);
        }
        Spannable word = new SpannableString(firstWord);

        word.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.textPrimaryColor)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(word);
        Spannable wordTwo = new SpannableString(secondWord);

        wordTwo.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.textSecondarColor)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.append(wordTwo);
    }

//    private void addMultiColoredTextInView(String firstWord, String secondWord, TextView textView, int colorResource) {
//        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
//            textView.setRotationY(180);
//        }
//        Spannable word = new SpannableString(firstWord);
//        word.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.textPrimaryColor)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        textView.setText(word);
//        Spannable wordTwo;// = new SpannableString(secondWord);
//        wordTwo = AppUtils.getMMKString(textView.getTextSize(), secondWord, 1);
//        wordTwo.setSpan(new ForegroundColorSpan(ContextCompat.
//                getColor(context, colorResource)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        textView.append(wordTwo);
//    }

    public class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_productImage)
        ImageView productImage;
        @BindView(R.id.offer)
        TextView offer;
        @BindView(R.id.Tv_attributeInfo)
        TextView Tv_attributeInfo;
        @BindView(R.id.tv_productName)
        TextView productName;
        @BindView(R.id.tv_product_acturl_price)
        TextView tv_product_acturl_price;
        @BindView(R.id.tv_actual_price)
        TextView tv_actual_price;
        @BindView(R.id.tv_rating_number)
        TextView tv_rating_number;
        @BindView(R.id.tv_rating_reviews)
        TextView tv_rating_reviews;
        @BindView(R.id.lin_qu)
        LinearLayout row_quantity;
        @BindView(R.id.lin_write_review)
        LinearLayout lin_write_review;
        @BindView(R.id.rating)
        AppCompatRatingBar rating;
        @BindView(R.id.view_hide)
        View view_hide;
        @BindView(R.id.tv_quantity)
        TextView tv_quantity;

        public ProductSummaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }

    }


 */
