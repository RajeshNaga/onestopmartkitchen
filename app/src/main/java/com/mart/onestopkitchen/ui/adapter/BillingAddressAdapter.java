package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BillingAddress;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.greenrobot.event.EventBus;

public class BillingAddressAdapter extends RecyclerView.Adapter<BillingAddressAdapter.AddressViewHolder> {
    private Context mContext;
    private List<BillingAddress> billingAddressResponse;
    private LayoutInflater inflater;
    private Unbinder unbinder;

    public BillingAddressAdapter(Context mContext, List<BillingAddress> billingAddressResponse) {
        this.mContext = mContext;
        this.billingAddressResponse = billingAddressResponse;
        inflater = LayoutInflater.from(mContext);

    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.layout_billing_addresses, parent, false);
//        unbinder=ButterKnife.bind(this, itemView);
        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, final int position) {
        holder.tv_username.setText(billingAddressResponse.get(position).getFirstName() + " " + billingAddressResponse.get(position).getLastName());
        holder.tv_address.setText(billingAddressResponse.get(position).getAddress1() + ","
                + billingAddressResponse.get(position).getCity() + ","
                + billingAddressResponse.get(position).getStateProvinceName() + ","
                + billingAddressResponse.get(position).getCountryName());

        holder.tv_phone_number.setText(billingAddressResponse.get(position).getPhoneNumber());

        holder.ll_billing_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(billingAddressResponse.get(position));
//                listener.onAddressSelect(billingAddressResponse,position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return billingAddressResponse.size();
    }

    public interface OnBillingAddressItem {
        void onAddressSelect(List<BillingAddress> billingAddressResponse, int position);
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_billing_address)
        LinearLayout ll_billing_address;
        @BindView(R.id.tv_username)
        TextView tv_username;
        @BindView(R.id.tv_address)
        TextView tv_address;
        @BindView(R.id.tv_phone_number)
        TextView tv_phone_number;


        public AddressViewHolder(View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }
}
