package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ForgetData;
import com.mart.onestopkitchen.model.ForgetResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.forgetSend)
    Button forgetSend;

    @BindView(R.id.etForgetEmail)
    EditText etForgetEmail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forget_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        forgetSend.setOnClickListener(this);
        if (null != getActivity())
            getActivity().setTitle(getString(R.string.forgot_password));
    }

    @Override
    public void onClick(@NotNull View v) {
        if (v.getId() == forgetSend.getId()) {
            if (TextUtils.isEmpty(etForgetEmail.getText().toString())) {
                etForgetEmail.setError(getString(R.string.enter_email));
                etForgetEmail.requestFocus();
            } else if (!isValidEmail(etForgetEmail.getText().toString())) {
                etForgetEmail.setError(getString(R.string.enter_valid_email));
                etForgetEmail.requestFocus();
            } else {
                ForgetData forgetData = new ForgetData(etForgetEmail.getText().toString());
                RetroClient.getApi().forgetPassword(forgetData).enqueue(new CustomCB<ForgetResponse>(this.getView()));
            }
        }
    }

    public void onEvent(@NotNull ForgetResponse response) {
        if (response.getStatusCode() == 200) {
            etForgetEmail.setText("");
            Toast.makeText(getActivity(), response.getSuccessMessage(), Toast.LENGTH_SHORT).show();
            if (null != getActivity())
                getActivity().onBackPressed();
        } else {
            Toast.makeText(getActivity(), response.getErrorsAsFormattedString(), Toast.LENGTH_SHORT).show();

        }

    }
}
