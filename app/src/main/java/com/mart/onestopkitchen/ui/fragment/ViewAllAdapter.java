package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.BaseProductModel;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewAllAdapter extends RecyclerView.Adapter<ViewAllAdapter.ViewHolder> {
    public List<ProductModel> homePageProductResponses;
    private Context mContext;
    private OnViewClick onViewClick;

    ViewAllAdapter(OnViewClick featureViewAllFragment, Context context, List<ProductModel> homePageProductResponse) {
        this.homePageProductResponses = homePageProductResponse;
        this.mContext = context;
        this.onViewClick = featureViewAllFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_all, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        BaseProductModel product = homePageProductResponses.get(position);
        holder.onItemBind(product);

        holder.tvProductName.setText(product.getName());
        holder.tvProductPrice.setVisibility(View.VISIBLE);
        Glide
                .with(mContext)
                .load(product.getDefaultPictureModel().getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(mContext.getResources().getDrawable(R.drawable.placeholder))
                .fitCenter()
                .into(holder.imgProductImage);

        if (homePageProductResponses.get(position).getProductPrice().getDiscountPercentage() > 0) {
            holder.tvOfferPercentage.setVisibility(View.VISIBLE);
            holder.tvOfferPercentage.setText(homePageProductResponses.get(position).getProductPrice().getDiscountPercentage() + "%");
        } else
            holder.tvOfferPercentage.setVisibility(View.GONE);

        if (null != homePageProductResponses.get(position).getProductPrice().getPriceWithDiscount() && !homePageProductResponses.get(position).getProductPrice().getPriceWithDiscount().isEmpty()) {
            holder.tvProductOldPrice.setVisibility(View.VISIBLE);
            holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), homePageProductResponses.get(position).getProductPrice().getPriceWithDiscount(), 0));
            holder.tvProductOldPrice.setText(homePageProductResponses.get(position).getProductPrice().getPrice());

        } else if (null != homePageProductResponses.get(position).getProductPrice().getOldPrice()) {
            holder.tvProductOldPrice.setVisibility(View.VISIBLE);
            holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), homePageProductResponses.get(position).getProductPrice().getPrice(), 0));
            holder.tvProductOldPrice.setText(homePageProductResponses.get(position).getProductPrice().getOldPrice());

        } else {
            holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), homePageProductResponses.get(position).getProductPrice().getPrice(), 0));
            holder.tvProductOldPrice.setText(homePageProductResponses.get(position).getProductPrice().getOldPrice());
            holder.tvProductOldPrice.setVisibility(View.GONE);
        }

        holder.tvProductOldPrice.setPaintFlags(holder.tvProductOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.buttonFavoriteCategory.setChecked(homePageProductResponses.get(position).getWishList());
        holder.buttonFavoriteCategory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                    if (b) {
                        onViewClick.addToCart(homePageProductResponses.get(position).getId());
                    } else {
                        onViewClick.removeCart(homePageProductResponses.get(position).getId());
                    }
                } else {
                    holder.buttonFavoriteCategory.setChecked(false);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return homePageProductResponses != null ? homePageProductResponses.size() : 0;
    }

    public void loadData(List<ProductModel> selectedItems) {
        this.homePageProductResponses = selectedItems;
        notifyDataSetChanged();
    }


    public interface OnViewClick {
        void addToCart(long id);

        void removeCart(long id);

        void OnViewClicked(BaseProductModel productModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_productImage)
        ImageView imgProductImage;
        @BindView(R.id.tv_productName)
        TextView tvProductName;
        @BindView(R.id.tv_productPrice)
        TextView tvProductPrice;
        @BindView(R.id.tv_productOldPrice)
        TextView tvProductOldPrice;
        @BindView(R.id.button_favorite_category)
        ToggleButton buttonFavoriteCategory;
        @BindView(R.id.tv_offerPercentage)
        TextView tvOfferPercentage;
        private BaseProductModel productModel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.lin_root)
        public void onViewClicked() {
            onViewClick.OnViewClicked(productModel);
        }

        void onItemBind(BaseProductModel product) {
            this.productModel = product;
        }
    }
}
