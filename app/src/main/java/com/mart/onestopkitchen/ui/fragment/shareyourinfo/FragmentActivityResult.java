package com.mart.onestopkitchen.ui.fragment.shareyourinfo;

import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.BrandsItem;

public interface FragmentActivityResult {
    void listSelection(BrandsItem item);
}
