package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrandsItem {

    @SerializedName("Name")
    private String name;

    @SerializedName("TailoringSizes")
    private List<TailoringSizesItem> tailoringSizes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TailoringSizesItem> getTailoringSizes() {
        return tailoringSizes;
    }

    public void setTailoringSizes(List<TailoringSizesItem> tailoringSizes) {
        this.tailoringSizes = tailoringSizes;
    }
}