package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.ViewType;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 11/12/2015.
 */
public class ProductAdapter extends RecyclerView.Adapter implements Filterable {
    private static final int VIEW_PROG = 4;
    public List<ProductModel> products;
    public int ViewFormat = ViewType.GRID;
    protected Context context;
    private boolean[] checkedTracks;
    private OnItemClickListener mItemClickListener;
    private List<ProductModel> filteredData;
    private String TAG = ProductAdapter.class.getSimpleName();

    public ProductAdapter(Context context, List productsList) {
        try {
            this.products = new ArrayList<>();
            checkedTracks = new boolean[productsList.size()];
            this.products = productsList;
            this.context = context;
            Log.d("AdapterSize", "" + productsList.size());
            filteredData = new ArrayList<>();
            filteredData.addAll(productsList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ProductAdapter(Context context, List productsList, int viewType) {
        this(context, productsList);
        ViewFormat = viewType;
    }

    public void addAll(List<ProductModel> products) {
        this.products.addAll(products);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        int layout = 0;
        if (viewType == ViewType.GRID)
            layout = R.layout.item_products_grid;
        else if (viewType == ViewType.LIST)
            layout = R.layout.item_product_list;
        else if (viewType == ViewType.SINGLE)
            layout = R.layout.item_product_single;
        else if (viewType == ViewType.HOMEPAGE_VIEW)
            layout = R.layout.item_homepage_product;
        else if (viewType == VIEW_PROG) {
            layout = R.layout.item_progress;
            View itemView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
            return new ProgressViewHolder(itemView);
        }

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(layout, parent, false);

        return new ProductSummaryHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return products.get(position) != null ? ViewFormat : VIEW_PROG;

        //   return ViewFormat;
        //return super.getItemViewType(position);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder bindViewHolder, final int position) {
        try {
            if (bindViewHolder instanceof ProductSummaryHolder) {
                ProductModel productModel = products.get(position);
                ProductSummaryHolder holder = (ProductSummaryHolder) bindViewHolder;
                holder.productName.setText(AppUtils.capitalizeWords(productModel.getName()));

                if (productModel.getProductPrice().getDiscountPercentage() > 0) {
                    holder.offerPercentage.setVisibility(View.VISIBLE);
                    holder.offerPercentage.setText(productModel.getProductPrice().getDiscountPercentage() + "%");
                } else
                    holder.offerPercentage.setVisibility(View.GONE);


                if (null != productModel.getProductPrice().getPriceWithDiscount() && !productModel.getProductPrice().getPriceWithDiscount().isEmpty()) {
                    holder.productOldPrice.setVisibility(View.VISIBLE);
                    holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPriceWithDiscount(), 0));
                    holder.productOldPrice.setText(AppUtils.getMMKString(holder.productOldPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));

                } else if (null != productModel.getProductPrice().getOldPrice()) {
                    holder.productOldPrice.setVisibility(View.VISIBLE);
                    holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                    holder.productOldPrice.setText(AppUtils.getMMKString(holder.productOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));

                } else {
                    holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                    holder.productOldPrice.setText(AppUtils.getMMKString(holder.productOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));
                    holder.productOldPrice.setVisibility(View.GONE);
                }

                if (null != holder.gif_mark_new) {
                    if (productModel.getMarkAsNew()) {
                        holder.gif_mark_new.setVisibility(View.VISIBLE);
                        Glide
                                .with(context)
                                .load(R.drawable.new_3)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(holder.gif_mark_new);
                    } else {
                        holder.gif_mark_new.setVisibility(View.GONE);
                    }
                }
                Glide
                        .with(context)
                        .load(productModel.getDefaultPictureModel().getImageUrl())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.productImage);

            } else {
                ((ProgressViewHolder) bindViewHolder).progressBar.setIndeterminate(true);
            }
        } catch (ClassCastException e) {
            Log.d(TAG, "Exception---: " + e.getMessage());
        }
    }


    @Override
    public int getItemCount() {
        if (products == null)
            return 0;
        return products.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();

                // List<Object> filteredResult = getFilteredResults(charSequence);
                if (charSequence == null || charSequence.length() == 0) {
                    // No filter implemented we return all the list
                    results.values = filteredData;
                    results.count = filteredData.size();
                } else if (filteredData != null) {
                    ArrayList<ProductModel> filterResultsData = new ArrayList<>();
                    for (ProductModel data : filteredData) {
                        String price = "" + data.getProductPrice().getPrice();
                        if (data.getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || price.contains(charSequence)) {
                            filterResultsData.add(data);
                        }
                    }
                    results.values = filterResultsData;
                    results.count = filterResultsData.size();
                }
                return results;
            }


            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                products = (List<ProductModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar1);
        }
    }

    public class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView offerPercentage;
        private ImageView productImage;
        private TextView productPrice;
        private TextView productOldPrice;
        private TextView productName;
        private CheckBox fav;
        private ImageView gif_mark_new;

        public ProductSummaryHolder(View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.img_productImage);
            productPrice = itemView.findViewById(R.id.tv_productPrice);
            productName = itemView.findViewById(R.id.tv_productName);
            productOldPrice = itemView.findViewById(R.id.tv_productOldPrice);
            offerPercentage = itemView.findViewById(R.id.tv_offerPercentage);
            productOldPrice.setPaintFlags(productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemView.setOnClickListener(this);
            gif_mark_new = itemView.findViewById(R.id.gif_mark_new);

//            fav = itemView.findViewById(R.id.fav);
//            fav.setVisibility(View.GONE);
            //fav.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}

