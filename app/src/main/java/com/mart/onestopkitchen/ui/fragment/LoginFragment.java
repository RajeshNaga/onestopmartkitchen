package com.mart.onestopkitchen.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;

import com.androidhiddencamera.CameraConfig;
import com.androidhiddencamera.CameraError;
import com.androidhiddencamera.HiddenCameraFragment;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.gson.Gson;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.SmsSendAndReadWithoutPermission.AutoReadIncomingSmsReceiver;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.camera.FaceTrackerActivity;
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper;
import com.mart.onestopkitchen.listener.OtpListener;
import com.mart.onestopkitchen.loginuser.VerifyUserListener;
import com.mart.onestopkitchen.loginuser.logindetails.UserLoginRegisterFromOKDollarAppPresenter;
import com.mart.onestopkitchen.loginuser.logindetails.UserLoginRegisterListenerCart;
import com.mart.onestopkitchen.loginuser.loginnew.VerifyUserListenerNew;
import com.mart.onestopkitchen.loginuser.loginnew.VerifyUserPresenterNew;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;
import com.mart.onestopkitchen.loginuser.modelnew.error.CommonError;
import com.mart.onestopkitchen.model.LoginResponse;
import com.mart.onestopkitchen.model.okdollar.OKDollarLoginInfo;
import com.mart.onestopkitchen.model.okdollar.model.ProfileModel;
import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.permission.PermissionListener;
import com.mart.onestopkitchen.permission.PermissionUtils;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.selectcountry.CountryListener;
import com.mart.onestopkitchen.ui.selectcountry.CountrySelectionFragment;
import com.mart.onestopkitchen.ui.selectcountry.model.DataItem;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.CircleImageView;
import com.mart.onestopkitchen.utils.CustomEdittext;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.Language;
import com.mart.onestopkitchen.utils.callback.CurrentLocationCallback;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;
import com.mart.onestopkitchen.utils.mobileNumberValidation.MobileNumberValidation;
import com.mart.onestopkitchen.utils.sms.SmsValidateSms;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

import static com.mart.onestopkitchen.service.PreferenceService.LOCATIONS_LAT;
import static com.mart.onestopkitchen.service.PreferenceService.LOCATIONS_LONG;
import static com.mart.onestopkitchen.service.PreferenceService.OPERATOR_MOBILE_NUMBER;
import static com.mart.onestopkitchen.service.PreferenceService.PROCEED_AS_GUEST;
import static com.mart.onestopkitchen.service.PreferenceService.PROFILE_PIC;
import static com.mart.onestopkitchen.service.PreferenceService.REGISTER_DONE;
import static com.mart.onestopkitchen.service.PreferenceService.USER_COUNTRY_CODE;
import static com.mart.onestopkitchen.service.PreferenceService.USER_MOBILE_NUMBER;
import static com.mart.onestopkitchen.service.PreferenceService.VERIFIED_DONE;
import static com.mart.onestopkitchen.utils.AppConstants.FROM_SIGN_UP_SELECT_CAPTURE;
import static com.mart.onestopkitchen.utils.Keys.CUS_ORDER;
import static com.mart.onestopkitchen.utils.Keys.MY_ACCOUNT;

/**
 * modified thavam.
 */
public class LoginFragment extends HiddenCameraFragment implements AppDialogs.OkDollarLogin, VerifyUserListener,
        OtpListener, ViewTreeObserver.OnGlobalLayoutListener, SmsValidateSms, VerifyUserListenerNew, CurrentLocationCallback, PermissionListener, AppDialogs.AlertDialogOkClick, AutoReadIncomingSmsReceiver.AutoReadSMSListener, CountryListener, UserLoginRegisterListenerCart {

    @BindView(R.id.btn_verify)
    Button btnVerify;
    @BindView(R.id.btn_guest)
    Button btnGuest;
    @BindView(R.id.txt_country_code)
    TextView txtCountryCode;
    @BindView(R.id.edt_mobile_number)
    CustomEdittext edtMobileNumber;
    @BindView(R.id.linHeader)
    LinearLayout linHeader;
    @BindView(R.id.edt_email_id)
    EditText edtEmailId;
    @BindView(R.id.lin_email_id)
    LinearLayout linEmailId;
    @BindView(R.id.edt_dob)
    EditText edtDob;
    @BindView(R.id.rel_root)
    RelativeLayout relRoot;
    @BindView(R.id.radioMale)
    RadioButton radioMale;
    @BindView(R.id.radioFemale)
    RadioButton radioFemale;
    @BindView(R.id.radio_btn_sex)
    RadioGroup radioBtnSex;
    @BindView(R.id.lin_register_form)
    LinearLayout linRegisterForm;
    @BindView(R.id.lin_ok_dollar)
    LinearLayout linOkDollar;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.edt_pasword)
    EditText edtPasword;
    @BindView(R.id.edt_confirm_paswordpasword)
    EditText edtConfirmPaswordpasword;
    @BindView(R.id.iv_profile_image)
    CircleImageView ivProfileImage;
    @BindView(R.id.img_clear)
    ImageView imgClear;
    @BindView(R.id.lin_profile_img)
    RelativeLayout linProfileImg;
    @BindView(R.id.edt_profile_pic)
    EditText edtProfilePic;
    @BindView(R.id.img_select_country_code_verify)
    ImageView img_select_country_code_verify;
    @BindView(R.id.lin_profile_pic)
    LinearLayout linProfilePic;
    @BindView(R.id.lin_country_selection)
    LinearLayout linCountrySelection;


    //    @BindView(R.id.img_ok_dollar_login)
    //     RelativeLayout imgOkDollarLogin;
    //    @BindView(R.id.lin_user_name)
    //    LinearLayout linUserName;
    //    @BindView(R.id.lin_dob_select)
    //    LinearLayout linDobSelect;
    //    @BindView(R.id.lin_password)
    //    LinearLayout linPassword;
    //    @BindView(R.id.edt_dob_tp)
    //    TextInputLayout edtDobTp;
    //    @BindView(R.id.iv_camera)
    //    ImageView ivCamera;
    //    @BindView(R.id.lin_confirm_password)
    //    LinearLayout linConfirmPassword;
    //    @BindView(R.id.txt_number_hint)
    //    TextView txtNumberHint;
    //    @BindView(R.id.img_date_picker)
    //    ImageView imgDatePicker;
    //    @BindView(R.id.edt_profile_pic_tp)
    //    TextInputLayout edtProfilePicTp;
    //    private int readOkDollar = 0;
    //    private boolean isOkDollarLogin;
    //    private String latitude = "", longitude = "";
    //    private String OperaterName = "";
    //    private int show = 1;


    private String imageUrl = "", ServerOTP = "";
    private static final String FORMAT = "%02d";
    private static int COUNTY_SELECT = 1000;
    private OKDollarLoginInfo info;
    private MainActivity mainActivity;
    private VerifyUserPresenterNew verifyUserListenerNew;
    private Context mContext;
    private CountDownTimer countDownTimer;
    private int isFirstTime = 0;
    private boolean isUserRegister = false;
    private ViewTreeObserver observer;
    private boolean isOkDollarAppAvailable = true;
    private PreferenceService preferenceService = PreferenceService.getInstance();
    private PermissionUtils permissionUtils;
    private MobileNumberValidation mobileNumberValidation;
    private int minimumLength = 0, maximumLength = 0;
    private MyApplication myApplication;
    private AutoReadIncomingSmsReceiver autoReadIncomingSmsReceiver;
    private String TAG = LoginFragment.class.getSimpleName();
    private boolean onActivityFired;
    private boolean faceDetect;
    private boolean isAlreadyRegisterd = false;
    private boolean manualSmsSent;

    private final static String[] MULTI_PERMISSIONS = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat dateFormat = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT, Locale.ENGLISH);
            edtDob.setText(dateFormat.format(myCalendar.getTime()));
            edtDob.clearFocus();
            mainActivity.hideKeyboard();
            setEngLang(preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE));
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Log.e(TAG, "-----onCreate()------");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, rootView);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.login));
        EventBus.getDefault().register(this);
//        Log.e(TAG, "-----onCreateView()------");
        return rootView;
    }


    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
        manageReceiver();
//        Log.e(TAG, "-----onViewCreated()-------");
    }

    private void initUi() {
        mainActivity = (MainActivity) getActivity();
        verifyUserListenerNew = new VerifyUserPresenterNew(this, this);
        mContext = getContext();
        uiListener();
        img_select_country_code_verify.setImageResource(R.drawable.myanmar_flag);

    }

    private void uiListener() {
        myApplication = (MyApplication) Objects.requireNonNull(getContext()).getApplicationContext();
        permissionUtils = new PermissionUtils().with(getContext()).setListener(this);
        dobListener();
        //  keyBoardListener();
        //  checkOkDollarApp();
        linRegisterForm.setVisibility(View.GONE);
        btnStatus(false);
        mainActivity.getMapper().enableLocationTracking(this);
        if (preferenceService.GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE)) {
            imgClear.setVisibility(View.GONE);
            showRegistrationForm();
            mainActivity.hideKeyboard();
            username.requestFocus();
        } else {
            imgClear.setVisibility(View.GONE);
            edtMobileNumber.requestFocus();
            textListener();
        }
        userNameFiledValidation();

        username.setOnFocusChangeListener((view, b) -> {
            if (b) {
                username.setText(cutNull(username.getText().toString().trim()));
            }
        });

        try {
            if (!AppUtils.getViberNumber(mContext).isEmpty() && !preferenceService.GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE)) {
                showAutoPopUp();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userNameFiledValidation() {
        username.setFilters(new InputFilter[]{AppUtils.removeEmoji});
        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                username.removeTextChangedListener(this);
                String string = charSequence.toString();
                if (string.length() > 0)
                    if (string.startsWith(" ")) {
                        username.setText("");
                    } else if (string.contains("  ")) {
                        username.setText(string.replace("  ", " "));
                        username.setSelection(username.getText().toString().length());
                    } else if (string.length() >= 41) {
                        username.setText(string.substring(0, string.length() - 1));
                        username.setSelection(username.getText().toString().length());
                    }

                username.addTextChangedListener(this);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @SuppressLint("MissingPermission")
    private void startCameraListener() {
        if (null != mainActivity.cameraConfig) {
            CameraConfig mCameraConfig = mainActivity.cameraConfig;
            startCamera(mCameraConfig);
        }
    }

    private void showRegistrationForm() {
        linCountrySelection.setClickable(false);
        checkOkDollarApp();
        startCameraListener();
        isUserRegister = true;
        btnStatus(true);
        setDefaultFields();
        linRegisterForm.setVisibility(View.VISIBLE);
        txtCountryCode.setText(preferenceService.GetPreferenceValue(USER_COUNTRY_CODE));
        edtMobileNumber.setText(preferenceService.GetPreferenceValue(USER_MOBILE_NUMBER));
        txtCountryCode.setEnabled(false);
        edtMobileNumber.setEnabled(false);
        edtMobileNumber.setCursorVisible(false);
        btnVerify.setText(getString(R.string.register_news));
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.registration));
        if (!cutNull(preferenceService.GetPreferenceValue(PROFILE_PIC)).isEmpty()) {
            File imgFile = new File(preferenceService.GetPreferenceValue(PROFILE_PIC));
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                ivProfileImage.setImageBitmap(myBitmap);
                linProfileImg.setVisibility(View.VISIBLE);
                linProfilePic.setVisibility(View.GONE);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                imageUrl = Base64.encodeToString(byteArray, Base64.DEFAULT);
            }
        }
        if (myApplication.fromMenuSelection.equalsIgnoreCase(MY_ACCOUNT) || myApplication.fromMenuSelection.equalsIgnoreCase(CUS_ORDER))
            btnGuest.setVisibility(View.GONE);
        else btnGuest.setVisibility(View.VISIBLE);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void dobListener() {
        edtDob.setOnFocusChangeListener((view, b) -> {
            if (b) {
                dobSelect();
            }
        });

        edtProfilePic.setOnFocusChangeListener((view, b) -> {
            if (b) {
                takePhoto();
            }

        });

    }

    private void checkOkDollarApp() {
        if (!AppUtils.appInstalledOrNot(Objects.requireNonNull(getContext()), Keys.OK_DOLLAR_PACKAGE_NAME)) {
            linOkDollar.setVisibility(View.GONE);
            isOkDollarAppAvailable = false;
        } else {
            linOkDollar.setVisibility(View.VISIBLE);
            isOkDollarAppAvailable = true;
        }
    }

    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    private void textListener() {
        mobileNumberValidation = MobileNumberValidation.getInstance(getActivity(), "95");
        if (txtCountryCode.getText().toString().equals("+95")) {
            edtMobileNumber.setText("09");
            edtMobileNumber.setSelection(2);
        } else {
            edtMobileNumber.setText("");
        }
        edtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().startsWith("09")) {
                    if (txtCountryCode.getText().toString().equals("+95")) {
                        edtMobileNumber.setText(AppUtils.getMyanmarNumber());
                        edtMobileNumber.setSelection(edtMobileNumber.length());
                    }
                }
                int length = s.toString().length();
                if (length > 3) {
                    if (txtCountryCode.getText().toString().equals("+95")) {
                        if (length > 4)
                            imgClear.setVisibility(View.VISIBLE);
                        else
                            imgClear.setVisibility(View.GONE);

                        mobileNumberValidation.validateMobileNumber(true, "95", s.toString(), new MobileNumberValidation.MobileValidationListener() {
                            @Override
                            public void onSetValidationLimit(boolean validNumber, int minLength, int maxLength, String operator, String operatorColor) {
                                if (validNumber) {
                                    btnStatus(true);
                                    minimumLength = minLength;
                                    maximumLength = maxLength;
//                                    OperaterName = operator;
                                    preferenceService.SetPreferenceValue(OPERATOR_MOBILE_NUMBER, operator);
                                    setEditTextMaxLength(edtMobileNumber, maximumLength);
                                } else {
                                    btnStatus(false);
                                    edtMobileNumber.setText(AppUtils.getMyanmarNumber());
                                    // mainActivity.showNormalToast(getString(R.string.invalid_mobile_number));
                                    Toast.makeText(getContext(), getString(R.string.invalid_mobile_number), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onSelectedOtherCountry(boolean error) {
                            }
                        });

                    }
                }

                btnStatus(false);
                if (txtCountryCode.getText().toString().equals("+95")) {
                    if (length > 3) {
                        if (length >= minimumLength && length <= maximumLength && maximumLength == minimumLength) {
                            btnStatus(true);
                            btnVerify.performClick();
                            //--System.out.println("condition >3");
                        } else {
                            if (maximumLength != minimumLength && length >= maximumLength) {
                                btnStatus(true);
                                btnVerify.performClick();
                            }
                            if (maximumLength != minimumLength && length >= minimumLength) {
                                btnStatus(true);
                            }
                        }
                        if (!txtCountryCode.getText().toString().trim().equals("+95")) {
                            btnStatus(true);
                            // btnVerify.performClick();
                        }
                    }
                } else {
                    if (length > 3)
                        btnStatus(true);
                    if (length > 2)
                        imgClear.setVisibility(View.VISIBLE);
                    else
                        imgClear.setVisibility(View.GONE);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().startsWith("090") || s.toString().startsWith("091")) {
                    edtMobileNumber.setText(AppUtils.getMyanmarNumber());
                    edtMobileNumber.setSelection(edtMobileNumber.length());
                }
            }
        });
        edtMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(13)});
    }

    private void setEditTextMaxLength(@NotNull CustomEdittext editText, int length) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                String character = String.valueOf(source.charAt(i));
                if (character.equalsIgnoreCase("/") || character.equalsIgnoreCase("%") || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("&") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";") || character.equalsIgnoreCase("<") || character.equalsIgnoreCase(">") || character.equalsIgnoreCase("?") || character.equalsIgnoreCase("[") || character.equalsIgnoreCase("]") || character.equalsIgnoreCase("\"") || character.equalsIgnoreCase("^") || character.equalsIgnoreCase("_") || character.equalsIgnoreCase("|") || character.equalsIgnoreCase("~") || character.equalsIgnoreCase("@") || character.equalsIgnoreCase("-")) {
                    return "";
                }
            }
            return null;
        };
        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(length);
        fArray[1] = filter;
        editText.setFilters(fArray);
    }

    private void continueWithOkDollar(OKDollarLoginInfo loginDetails) {
        new AppDialogs().popUpWindow(this, loginDetails);
        if (null != mainActivity)
            mainActivity.hideKeyboard();
    }

    /*private void loginOKDollarApp() {
        mainActivity.showNormalToast(getResources().getString(R.string.please_login_ok_dollar));
        Intent launchIntent = mainActivity.getPackageManager().getLaunchIntentForPackage(Keys.OK_DOLLAR_PACKAGE_NAME);
        startActivityForResult(launchIntent, 1001);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        faceDetect = false;
        if (resultCode == 1001) {
//            readOkDollar = 1;
            showOkDollarLoginDilog();
        } else if (data != null && resultCode == COUNTY_SELECT) {
            DataItem bundle = (DataItem) data.getSerializableExtra("countrydata");
            setListData(bundle);
        }
        if (resultCode == FROM_SIGN_UP_SELECT_CAPTURE) {
            onActivityFired = true;
            faceDetect = true;
            passDataForProfilePic(data);
        }
        if (requestCode == 111) {
            permissionUtils.requestPermission(MULTI_PERMISSIONS);

            if (null != mainActivity)
                mainActivity.hideKeyboard();
        }
        if (requestCode == 101) {
            new Handler().postDelayed(() -> {
                onActivityFired = true;
                if (null != mainActivity)
                    mainActivity.hideKeyboard();
            }, 1000);//131173

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void passDataForProfilePic(Intent data) {
        try {
            if (data != null && data.hasExtra(Keys.IMAGE_PATH)) {
                Bitmap bitmap = BitmapFactory.decodeFile(data.getStringExtra(Keys.IMAGE_PATH));
                if (bitmap != null) {
                    ivProfileImage.setImageBitmap(bitmap);
                    linProfilePic.setVisibility(View.GONE);
                    linProfileImg.setVisibility(View.VISIBLE);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    imageUrl = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    preferenceService.SetPreferenceValue(PROFILE_PIC, cutNull(data.getStringExtra(Keys.IMAGE_PATH)));
                    edtEmailId.clearFocus();
                    username.clearFocus();
                }
            } else {
                edtProfilePic.clearFocus();
                edtProfilePic.setText("");
            }
        } catch (Exception e) {
            edtProfilePic.clearFocus();
            edtProfilePic.setText("");
        }
    }

    private void setListData(@NotNull DataItem bundle) {
        isFirstTime = 0;
        if (bundle.getCountryCode().contains("+95")) {
            txtCountryCode.setText(bundle.getCountryCode());
            edtMobileNumber.setText("09");
            img_select_country_code_verify.setImageResource(bundle.getLocalImg());
            edtMobileNumber.setSelection(2);
        } else {
            edtMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(13)});
            txtCountryCode.setText(bundle.getCountryCode());
            edtMobileNumber.setText("");
            img_select_country_code_verify.setImageResource(bundle.getLocalImg());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        Log.e(TAG, "-----onDetach()-----");
        // observer.removeGlobalOnLayoutListener(this);
    }

    @Override
    public void onDestroyView() {
//        Log.e(TAG, "------ onDestroyView ----------");
        releaseObj();
        super.onDestroyView();
    }

    @OnClick({R.id.lin_country_selection, R.id.img_clear, R.id.img_ok_dollar_login, R.id.lin_profile_img, R.id.iv_camera, R.id.btn_verify, R.id.lin_dob_select, R.id.img_date_picker, R.id.img_profile_select, R.id.btn_guest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_ok_dollar_login:
                showOkDollarLoginDilog();
                break;
            case R.id.btn_verify:
                btnSubmitAction();
                break;
            case R.id.img_date_picker:
                dobSelect();
                break;
            case R.id.lin_country_selection:
                countySelection();
                break;
            case R.id.btn_guest:
                proceedAsGuestLogin();
                break;
            case R.id.img_profile_select:
            case R.id.iv_camera:
            case R.id.lin_profile_img:
                takePhoto();
                break;
            case R.id.img_clear:
                clearEditText();
                break;
        }
    }

    private void clearEditText() {
        imgClear.setVisibility(View.GONE);
        textListener();
    }

    private void takePhoto() {
        if (null != mainActivity)
            mainActivity.hideKeyboard();
        Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        startActivityForResult(new Intent(getActivity(), FaceTrackerActivity.class), FROM_SIGN_UP_SELECT_CAPTURE);
    }

    private void proceedAsGuestLogin() {
        preferenceService.SetPreferenceValue(PROCEED_AS_GUEST, true);
        MyApplication.PROCEED_AS_GUEST = true;
        NetworkUtil.setToken("");
        if (getFragmentManager() != null) {
            if (Keys.WISH_LIST.equalsIgnoreCase(myApplication.fromMenuSelection)) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                        .replace(R.id.container, new WishListFragment()).commit();
                return;
            }
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, new CartFragment()).commit();
        }
    }

    private void countySelection() {
        new Handler().postDelayed(() -> {
            if (null != mainActivity)
                mainActivity.hideKeyboard();
        }, 100);
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().add(R.id.container, new CountrySelectionFragment(this)).addToBackStack(null).commit();
    }

    private void checkPermission() {
        permissionUtils.requestPermission(MULTI_PERMISSIONS);
    }

    @Override
    public void onResume() {
        super.onResume();
//        Log.e(TAG, "-----onResume()------");
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        if (PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE) || PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE))
            EventBus.getDefault().post("-1");

        if (preferenceService.GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE))
            checkOkDollarApp();
        if (onActivityFired) {
            onActivityFired = false;
            if (null != mainActivity)
                mainActivity.hideKeyboard();
            if (!faceDetect && !preferenceService.GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE))
                loadTimer();
            return;
        }
        if (AppUtils.getViberNumber(mContext).isEmpty()) {
            new Handler().postDelayed(() -> {
                if (null != mainActivity) mainActivity.showKeyboard();
            }, 100);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 10) {
            permissionUtils.onPermissionRequestResult(requestCode, permissions, grantResults);
        }
    }

    private void btnSubmitAction() {
        if (btnVerify.getResources().getString(R.string.register_news).equalsIgnoreCase(btnVerify.getText().toString().trim())) {
            if (userValidation()) {
                if (preferenceService.GetPreferenceValue(PROFILE_PIC).isEmpty())
                    takePhotoFromBackground();
                else
                    verifyUserListenerNew.apiCall(3);
            }

        } else {
            if (isFirstTime <= 1 && txtCountryCode.getText().toString().trim().equals("+95")) {
                System.out.println("isFirstTime::" + isFirstTime);
                loadTimer();
                SmsRetriever.getClient(Objects.requireNonNull(getActivity())).startSmsRetriever();
                manualSmsSent = false;
                verifyUserListenerNew.apiCall(1);

            } else {
                System.out.println("1 isFirstTime::" + isFirstTime);
                messageSendNormal();
                manualSmsSent = true;
            }
        }
    }

    private boolean userValidation() {

        if (cutNull(edtMobileNumber.getText().toString()).length() < 6) {
            if (null != mainActivity)
                mainActivity.showNormalToast(mContext.getResources().getString(R.string.please_enter_user_name));
            return false;
        }
        if (cutNull(username.getText().toString()).isEmpty()) {
            if (null != mainActivity)
                mainActivity.showNormalToast(mContext.getResources().getString(R.string.please_enter_user_name));
            return false;
        } else if (cutNull(username.getText().toString()).length() < 3) {
            if (null != mainActivity)
                mainActivity.showNormalToast(mContext.getResources().getString(R.string.please_enter_user_name_length));
            return false;

        } else if (cutNull(getEmail()).length() > 0) {
            if (!AppUtils.isValidEmail(cutNull(getEmail()))) {
                if (null != mainActivity)
                    mainActivity.showNormalToast(mContext.getResources().getString(R.string.invalid_format_email));
                return false;
            }
        }
        if (cutNull(edtDob.getText().toString()).isEmpty()) {
            if (null != mainActivity)
                mainActivity.showNormalToast(mContext.getResources().getString(R.string.select_date_of_birth));
            return false;
        }
        if (radioBtnSex.getCheckedRadioButtonId() == -1) {
            if (null != mainActivity)
                mainActivity.showNormalToast(mContext.getResources().getString(R.string.select_gender));
            return false;
        }
        return true;
    }

    private void messageSendNormal() {
        String msg = "";
        String smsBody = "";
        if (ServerOTP != null && ServerOTP.length() >= 2) {
            smsBody = "[#] " + getString(R.string.kitchen_app_name) + "-OTP -" + ServerOTP;
        } else {
            msg = getString(R.string.kitchen_app_name) + "-OTP -" + AppUtils.randomGeneratorNumber();
            smsBody = "[#] ".concat(msg) + " " + getString(R.string.sms_hash_code_release);
            if (BuildConfig.DEBUG)
                smsBody = "[#] ".concat(msg) + " " + getString(R.string.sms_hash_code_debug);
        }

        Uri uri;
        if (txtCountryCode.getText().toString().contains("+95")) {
            uri = Uri.parse("smsto:" + txtCountryCode.getText().toString().trim() + edtMobileNumber.getText().toString().trim().substring(1));
        } else {
            uri = Uri.parse("smsto:" + txtCountryCode.getText().toString().trim() + edtMobileNumber.getText().toString().trim());
        }
        if (getActivity() != null)
            SmsRetriever.getClient(getActivity()).startSmsRetriever();
        else
            Log.e(TAG, "Context null -- Did Not Started SMS API");

        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", smsBody);
        if (android.os.Build.MANUFACTURER.equalsIgnoreCase("HUAWEI"))
            intent.putExtra("exit_on_sent", false);
        else
            intent.putExtra("exit_on_sent", true);
        startActivityForResult(intent, 101);
        onActivityFired = true;
//        Log.e(TAG, "--### ------------: " + msg + " : \n smsBody: " + smsBody);

        AutoReadIncomingSmsReceiver.bindData(this, smsBody);
    }

    private void dobSelect() {
        if (null != mainActivity)
            mainActivity.hideKeyboard();
        setEngLang(Language.ENGLISH);
        Calendar c = Calendar.getInstance();
        edtDob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String charSequenc = cutNull(charSequence.toString());
                if (charSequenc.isEmpty()) {
                    edtDob.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getActivity()), dateSetListener,
                getYear(c),
                getMonth(c),
                getDay(c));//.show();
        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), (dialog, which) -> {
            if (which == DialogInterface.BUTTON_NEGATIVE) {
                if (edtDob.getText().toString().isEmpty())
                    edtDob.setText("");
                else {
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
                    Date date = null;
                    try {
                        date = format1.parse(edtDob.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String day = (String) DateFormat.format("dd", date); // 20
                    String monthNumber = (String) DateFormat.format("MM", date); // 06
                    String year = (String) DateFormat.format("yyyy", date);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, Integer.parseInt(year));
                    calendar.set(Calendar.MONTH, Integer.parseInt(monthNumber) - 1);
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
                    SimpleDateFormat dateFormat = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT, Locale.ENGLISH);
                    edtDob.setText(dateFormat.format(calendar.getTime()));
                }
                edtDob.clearFocus();
                setEngLang(preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE));
            }
        });
        datePickerDialog.setCanceledOnTouchOutside(false);

        //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 12);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @SuppressLint("SimpleDateFormat")
    private int getDay(Calendar c) {
        if (edtDob.getText().toString().isEmpty())
            return c.get(Calendar.DAY_OF_MONTH);
        else {
            SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
            Date date = null;
            try {
                date = format1.parse(edtDob.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String day = (String) DateFormat.format("dd", date); // 20

            c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
            return c.get(Calendar.DAY_OF_MONTH);
        }
    }

    @SuppressLint("SimpleDateFormat")
    private int getMonth(Calendar c) {
        if (edtDob.getText().toString().isEmpty())
            return c.get(Calendar.MONTH);
        else {
            SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
            Date date = null;
            try {
                date = format1.parse(edtDob.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String day = (String) DateFormat.format("MM", date); // 20

            c.set(Calendar.MONTH, Integer.parseInt(day) - 1);
            return c.get(Calendar.MONTH);
        }

    }

    @SuppressLint("SimpleDateFormat")
    private int getYear(Calendar c) {
        if (edtDob.getText().toString().isEmpty())
            return c.get(Calendar.YEAR);
        else {
            c.get(Calendar.YEAR);
            SimpleDateFormat format1 = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
            Date date = null;
            try {
                date = format1.parse(edtDob.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String day = (String) DateFormat.format("yyyy", date); // 20

            c.set(Calendar.YEAR, Integer.parseInt(day));
            return c.get(Calendar.YEAR);
        }

    }

    private void setEngLang(String engLang) {
        Locale myLocale = new Locale(engLang);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration(getResources().getConfiguration());
        config.setLocale(myLocale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    private void showOkDollarLoginDilog() {
        // normalOkLogin();
        if (AppUtils.appInstalledOrNot(Objects.requireNonNull(getActivity()), Keys.OK_DOLLAR_PACKAGE_NAME)) {
            OKDollarLoginInfo loginDetails = new AppUtils().validateOKAppLogin(getActivity());
            if (loginDetails != null && loginDetails.getMobilNumber() != null
                    && loginDetails.getMobilNumber().length() > 6 && !cutNull(loginDetails.getSimId()).trim().isEmpty()) {
                String mobile;
                if (loginDetails.getMobilNumber().startsWith("0095")) {
                    mobile = loginDetails.getMobilNumber().replace("00959", "09");
                    if (mobile.equalsIgnoreCase(edtMobileNumber.getText().toString())) {
                        continueWithOkDollar(loginDetails);
                    } else {
                        if (null != mainActivity)
                            mainActivity.showNormalToast(getString(R.string.verify_number_wrong));
                    }


                } else {//00959629044299
                    mobile = loginDetails.getMobilNumber();
                    if (mobile.equalsIgnoreCase(txtCountryCode.getText().toString().replace("+", "00") + edtMobileNumber.getText().toString())) {
                        continueWithOkDollar(loginDetails);
                    } else {
                        if (null != mainActivity)
                            mainActivity.showNormalToast(getString(R.string.verify_number_wrong));
                    }
                }
            } else {
                if (null != mainActivity)
                    mainActivity.showNormalToast(getString(R.string.login_ok_dollar));
            }
        } else {
            if (null != mainActivity)
                mainActivity.showNormalToast(getString(R.string.please_install_ok_dolar));
        }
    }

    /*private void normalOkLogin() {
        // txtNumberHint.setText(mContext.getResources().getString(R.string.please_enter_ok_account_number));
        //   linOkDollar.setVisibility(View.GONE);
        linRegisterForm.setVisibility(View.GONE);
        cancelTimer();
        btnVerify.setText(getResources().getString(R.string.verify));
        btnStatus(true);
    }*/

    @Override
    public void clickContinue(OKDollarLoginInfo okDollarLoginModel) {
        this.info = okDollarLoginModel;
        new UserLoginRegisterFromOKDollarAppPresenter(this, getContext()).apiData(okDollarLoginModel, getLat(), getLong());
        //   emailList.clear();
        //    emailList.add(cutNull(info.getEmail()));
        ///    String[] mails = AppUtils.getDefaultEmail().split(",");
        //    Collections.addAll(emailList, mails);
        //    isOkDollarLogin = true;
        //do check api call
        //presenter.apiCall(5);


        //    edtMobileNumber.setText(cutNull(okDollarLoginModel.getMobilNumber().replace("0095", "0")));
        //    txtCountryCode.setText(cutNull(okDollarLoginModel.getCountyCode()));
        //    linRegisterForm.setVisibility(View.VISIBLE);

        //   isUserRegister = true;
    }

   /* private void setStatus(EditText edtDob) {
        if (edtDob.getText().toString().isEmpty())
            edtDob.setEnabled(true);
        else edtDob.setEnabled(false);
    }*/

    private void btnStatus(boolean status) {
        btnVerify.setEnabled(status);
        btnVerify.setEnabled(status);
        if (status)
            btnVerify.setAlpha(1);
        else
            btnVerify.setAlpha(.5f);

    }

    @Override
    public void successFromServer(OtpResponseModel model) {
        if (model.getStatus().equalsIgnoreCase("Success"))
            loadTimer();
        else if (model.getMessage().toLowerCase().contains("should start with"))
            messageSendNormal();


        // btnVerify.setText(getResources().getString(R.string.register));

    }

    @Override
    public void noAccountFromVmart() {
        //here no account from v mart and also checking in ok dollar
        //presenter.apiCall(3);
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, new RegisterFragment())
                    .addToBackStack(null).commitAllowingStateLoss();
        }

    }

    @Override
    public void noAccountFromOkDollarAndVMart() {
        LoginFragment fragment = new LoginFragment();
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getActivity().getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                .replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    @Override
    public void userHaveOkDollarAccount(ProfileModel profileModel) {
        //do register api
        //   presenter.apiCall(2);

    }

    @Override
    public void registration() {
        //do next page
    }

    @Override
    public String getMobileNumber() {
        return cutNull(edtMobileNumber.getText().toString().trim());
       /* if (cutNull(txtCountryCode.getText().toString()).contains("+95"))
            return "0095" + cutNull(edtMobileNumber.getText().toString().trim().substring(1));
        return cutNull(txtCountryCode.getText().toString().trim().replace("+", "00") + edtMobileNumber.getText().toString().trim());
  */
    }

    @Override
    public String getSimId() {
        return cutNull(info == null ? "" : info.getSimId());
    }

    @Override
    public String getCountryCode() {
        return cutNull(txtCountryCode.getText().toString());
    }

    @Override
    public String getUsername() {
        return cutNull(username.getText().toString());
    }

    @Override
    public String getDob() {
        return cutNull(edtDob.getText().toString());
    }

    @Override
    public String getEmail() {
//        String[] mails = AppUtils.getDefaultEmail().split(",");
        // return mails.length > 0 ? mails[0] : cutNull(edtEmailId.getText().toString());
        return cutNull(edtEmailId.getText().toString());
    }

    @Override
    public String getGender() {
        if (radioMale.isChecked())
            return "M";
        return "F";

    }

    @Override
    public OKDollarLoginInfo getOkDollarApp() {
        return info;
    }

    @Override
    public void userLogin() {
        preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
        //  goMenuItemFragment(new CartFragment());
        //
        //    goMenuItemFragment(new BaseBillingAddressFragment());
    }

    private void cancelTimer() {
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void clickOk(int fromWhichDialog) {
        if (fromWhichDialog == 11) {
            String viberNumber = cutNull(AppUtils.getViberNumber(mContext));
            if (!viberNumber.trim().isEmpty()) {
                txtCountryCode.setText("+95");
                edtMobileNumber.setText("09" + viberNumber.substring(4));
                checkPermission();
            }
        }
    }

    @Override
    public Context getActivityContext() {
        return getContext();
    }

    @Override
    public void showLoading() {
        AppDialogs.loadingDialog(mContext, false, "");
    }

    @Override
    public void hideLoading() {
        AppDialogs.dismissDialog();
    }

    @Override
    public void showError(String s) {
        if (mainActivity == null) {
            mainActivity = (MainActivity) getActivity();
        }

        if (mainActivity != null) {
            mainActivity.showNormalToast(s);
        }
    }

    private void loadTimer() {
        isFirstTime = isFirstTime + 1;
        txtCountryCode.setEnabled(false);
        edtMobileNumber.setEnabled(false);
        linCountrySelection.setEnabled(false);
        linCountrySelection.setClickable(false);
        imgClear.setVisibility(View.GONE);
        btnStatus(false);
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(30000, 1000) {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onTick(long millisUntilFinished) {
                txtCountryCode.setEnabled(false);
                edtMobileNumber.setEnabled(false);
                imgClear.setVisibility(View.GONE);
                btnStatus(false);
                btnVerify.setText(getResources().getString(R.string.verifying) + " " + String.format(FORMAT,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " " + getContext().getResources().getString(R.string.seconds));
                // if (isOkDollarAppAvailable) linOkDollar.setVisibility(View.GONE);
            }

            @Override
            public void onFinish() {
                txtCountryCode.setEnabled(true);
                imgClear.setVisibility(View.VISIBLE);
                edtMobileNumber.setEnabled(true);
                btnStatus(true);
                if (isOkDollarAppAvailable) linOkDollar.setVisibility(View.GONE);
                btnVerify.setText(getResources().getString(R.string.verify));
                showVericationAlert();
                linCountrySelection.setEnabled(true);
                linCountrySelection.setClickable(true);
            }
        }.start();
    }

    private void showVericationAlert() {
        AppDialogs.showAlertFinish(this, "splash", false, getString(R.string.alert), getString(R.string.sms_verification_failed), R.drawable.app_icon, 0);
    }

    @Override
    public void otpReceived() {
        if (isAdded()) {
            cancelTimer();
            btnVerify.setText(getString(R.string.verify));
            btnStatus(true);
            preferenceService.SetPreferenceValue(USER_MOBILE_NUMBER, edtMobileNumber.getText().toString());
            preferenceService.SetPreferenceValue(USER_COUNTRY_CODE, txtCountryCode.getText().toString());
            preferenceService.SetPreferenceValue(VERIFIED_DONE, true);
            preferenceService.SetPreferenceValue(PreferenceService.USERNAME, edtMobileNumber.getText().toString());
            if (null != mainActivity)
                mainActivity.hideKeyboard();

            EventBus.getDefault().post("1");


            if (isAlreadyRegisterd && !manualSmsSent)
                verifyUserListenerNew.apiCall(2);
            else {
                if (!manualSmsSent) {
                    noAccountFromVmartAndOkDollar();
                    username.requestFocus();
                } else {
                    verifyUserListenerNew.apiCall(2);
                }
            }
        }
    }


    public void onEvent(@NotNull String event) {
        if (event.equals("1")) {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
                EventBus.getDefault().post("-1");
            } else {
                EventBus.getDefault().post("-1");
            }
        }
    }

    @Override
    public void otpNotWorkingLowEndDevices() {
        if (null != mainActivity)
            mainActivity.showNormalToast(mContext.getResources().getString(R.string.sim_swap_login_new));
        normalState();
    }

    private void setDefaultFields() {
        // emailList.clear();
        String[] mails = AppUtils.getDefaultEmail().split(",");
        //  Collections.addAll(emailList, mails);
        edtEmailId.setText(mails.length > 0 ? mails[0] : "");
        username.setText("");
        edtDob.setText("");
        edtPasword.setText("");
        edtConfirmPaswordpasword.setText("");
    }

    public void onBackPressed() {
        if (linRegisterForm.getVisibility() == View.VISIBLE) {
            // isFirstTime = 0;
            linRegisterForm.setVisibility(View.GONE);
            // edtMobileNumber.setSelection(2);
            normalState();
        }
    }

    public boolean onBackPress() {
        boolean isBackPress = false;
        if (btnVerify.getText().toString().contains(mContext.getResources().getString(R.string.seconds))) {
            if (null != mainActivity)
                mainActivity.hideKeyboard();
            if (countDownTimer != null) {
                try {
                    hideLoading();
                    if (observer != null)
                        observer.removeGlobalOnLayoutListener(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                countDownTimer.cancel();
                if (!preferenceService.GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE)) {
                    txtCountryCode.setEnabled(true);
                    imgClear.setVisibility(View.VISIBLE);
                    edtMobileNumber.setEnabled(true);
                    btnStatus(true);
                    linCountrySelection.setEnabled(true);
                    linCountrySelection.setClickable(true);
                    if (isOkDollarAppAvailable) linOkDollar.setVisibility(View.GONE);
                    btnVerify.setText(getResources().getString(R.string.verify));
                }
                isBackPress = true;
            }
        }
        return isBackPress;
    }

    @SuppressLint("SetTextI18n")
    private void normalState() {
        cancelTimer();
        btnVerify.setText(getResources().getString(R.string.verify));
        btnStatus(true);
        edtMobileNumber.setEnabled(true);
        txtCountryCode.setText("+95");
        edtMobileNumber.setText("09");
        edtMobileNumber.setSelection(2);
        username.setEnabled(true);
        txtCountryCode.setEnabled(true);
        edtEmailId.setEnabled(true);
        edtDob.setEnabled(true);
        radioMale.setEnabled(true);
        radioMale.setSelected(true);
        radioFemale.setEnabled(true);
//        isOkDollarLogin = false;
        isUserRegister = false;
        linCountrySelection.setEnabled(true);
        linCountrySelection.setClickable(true);
        // checkOkDollarApp();
    }

    /*private boolean mptNumber8digitsValidation(String number) {
        return number.trim().startsWith("0941") ||
                number.trim().startsWith("0943") ||
                number.trim().startsWith("0947") ||
                number.trim().startsWith("0949") ||
                number.trim().startsWith("0973") ||
                number.trim().startsWith("0991");
    }

    private boolean mptNumber7digitsValidation(String number) {
        return number.trim().startsWith("0920") ||
                number.trim().startsWith("0921") ||
                number.trim().startsWith("0922") ||
                number.trim().startsWith("0923") ||
                number.trim().startsWith("0924") ||
                number.trim().startsWith("095") ||
                //  number.toString().trim().startsWith("096")  ||
                number.trim().startsWith("0987") ||
                //  number.toString().trim().startsWith("0986") ||
                number.trim().startsWith("0985") ||
                number.trim().startsWith("0984") ||
                number.trim().startsWith("0983") ||
                number.trim().startsWith("0981");
    }

    public void setEditTextMaxLength(EditText editText, int length) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                String character = String.valueOf(source.charAt(i));
                if (character.equalsIgnoreCase("/") || character.equalsIgnoreCase("%") || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("&") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";") || character.equalsIgnoreCase("<") || character.equalsIgnoreCase(">") || character.equalsIgnoreCase("?") || character.equalsIgnoreCase("[") || character.equalsIgnoreCase("]") || character.equalsIgnoreCase("\"") || character.equalsIgnoreCase("^") || character.equalsIgnoreCase("_") || character.equalsIgnoreCase("|") || character.equalsIgnoreCase("~") || character.equalsIgnoreCase("@") || character.equalsIgnoreCase("-")) {
                    return "";
                }
            }
            return null;
        };
        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(length);
        fArray[1] = filter;
        editText.setFilters(fArray);
    }

    private void keyBoardListener() {
        observer = relRoot.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(this);
    }*/

    private String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }

    @Override
    public void onGlobalLayout() {
        try {
            Rect r = new Rect();
            relRoot.getWindowVisibleDisplayFrame(r);
            int screenHeight = relRoot.getRootView().getHeight();
            int keypadHeight = screenHeight - r.bottom;
            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                // keyboard is opened
                if (isUserRegister)
                    linHeader.setVisibility(View.GONE);

            } else {
                if (isUserRegister)
                    new Handler().postDelayed(() -> linHeader.setVisibility(View.VISIBLE), 100);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorSendingSms() {
        normalState();
    }

    @Override
    public void userLoginFromOkDollar() {
        preferenceService.SetPreferenceValue(REGISTER_DONE, true);
        RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
    }

    @Override
    public void failedUserLoginFromOkDollar() {
        setDefaultFields();
        isUserRegister = true;
        linRegisterForm.setVisibility(View.VISIBLE);
        btnVerify.setText(getString(R.string.register_news));
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.registration));

    }

    @Override
    public void billingAddress() {
        preferenceService.SetPreferenceValue(PreferenceService.REGISTER_DONE, true);
        RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
    }

    @Override
    public void loginSucess(LoginResponse loginResponse) {
        if (loginResponse.getStatusCode().equalsIgnoreCase("200")) {
            preferenceService.SetPreferenceValue(PreferenceService.REGISTER_DONE, true);
            preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, loginResponse.getToken());
            Toast.makeText(getContext(), getString(R.string.login_success), Toast.LENGTH_LONG).show();
            SharedPrefsHelper.getInstance().clearAllData();
            NetworkUtil.setToken(loginResponse.getToken());
            RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
        }
    }

    @Override
    public String getUserMobileNumber() {
        String outOfStartingCode;
        if (txtCountryCode.getText().toString().equals("+95"))
            return cutNull(edtMobileNumber.getText().toString().trim());
        else if (edtMobileNumber.getText().toString().startsWith("0")) {
            outOfStartingCode = "";
        } else {
            outOfStartingCode = "0";
        }
        return cutNull(outOfStartingCode + edtMobileNumber.getText().toString().trim());
    }

    @Override
    public void otpSuccessFromServer(OtpResponseModel model) {
        if (model.getStatusCode() == 200) {
            if (model.getOTP() != null) {
                this.isAlreadyRegisterd = model.isUserRegistered();
                ServerOTP = model.getOTP();
            }
        }
    }

    @Override
    public void errorFromServer(CommonError model, int api) {
        if (api == 1) {
            otpError(model);
        }
    }

    @Override
    public void addressSucess(BillingAddressResponse billingAddressResponse) {
        if (billingAddressResponse != null && billingAddressResponse.getExistingAddresses().size() > 0) {
            preferenceService.SetPreferenceValue(PreferenceService.ADDRESS_DONE, true);
            if (preferenceService.GetPreferenceValue(PreferenceService.NAVIGATION_CLASS_NAME).equals("0")) {
                if (getFragmentManager() != null) {
//                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new CustomerInfoFragment()).addToBackStack("").commitAllowingStateLoss();
                }
            } else if (preferenceService.GetPreferenceValue(PreferenceService.NAVIGATION_CLASS_NAME).equals("3")) {
                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new CustomerOrdersFragment()).commitAllowingStateLoss();
                }
            } else {
                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new CartFragment()).commitAllowingStateLoss();
                }
            }
        } else if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, new BillingAddressFragment()).commitAllowingStateLoss();
        }
    }

    @Override
    public void resetLocale(boolean b) {
        if (b)
            setEngLang(Language.ENGLISH);
        else
            setEngLang(preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE));
    }

    public void userLoginIn(LoginResponseModel modelNew) {
        preferenceService.SetPreferenceValue(PreferenceService.REGISTER_DONE, true);
        preferenceService.SetPreferenceValue(PreferenceService.USER_PROFILE, new Gson().toJson(modelNew));
        if (null != modelNew.getData())
            preferenceService.SetPreferenceValue(PreferenceService.USER_NAME_TXT, modelNew.getData().getFirstName());

        EventBus.getDefault().post("1"); // 1 for Logout -: user registered @implement at CategoryNewFragment
        if (myApplication.fromMenuSelection.equalsIgnoreCase(CUS_ORDER)) {
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction().replace(R.id.container, new CustomerOrdersFragment()).commitAllowingStateLoss();
            }
            return;
        }
        if (!preferenceService.GetPreferenceBooleanValue(PreferenceService.ADDRESS_DONE)) {
            verifyUserListenerNew.isImageApiCalled = true;
            verifyUserListenerNew.apiCall(4);
            // RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
        } else {
            if (preferenceService.GetPreferenceValue(PreferenceService.NAVIGATION_CLASS_NAME).equals("0")) {
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new CustomerInfoFragment()).addToBackStack("").commitAllowingStateLoss();

                }
            } else if (preferenceService.GetPreferenceValue(PreferenceService.NAVIGATION_CLASS_NAME).equals("3")) {
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new CustomerOrdersFragment()).commitAllowingStateLoss();
                }
            } else {
                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new CartFragment()).commitAllowingStateLoss();
                }
            }
        }
    }

    @Override
    public String getUserCountryCode() {
        return cutNull(txtCountryCode.getText().toString().trim());
    }

    @Override
    public void noAccountFromVmartAndOkDollar() {
        if (myApplication.fromMenuSelection.equalsIgnoreCase(CUS_ORDER)) {
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction().replace(R.id.container, new CustomerOrdersFragment()).commitAllowingStateLoss();
            }
            return;
        }
        checkOkDollarApp();
        startCameraListener();
        setDefaultFields();
        isUserRegister = true;
        btnStatus(true);
        linRegisterForm.setVisibility(View.VISIBLE);
        btnVerify.setText(getString(R.string.register_news));
        btnGuest.setVisibility(View.VISIBLE);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.registration));
        username.requestFocus();
        btnGuest.setVisibility(View.GONE);
        linCountrySelection.setClickable(false);
        if (myApplication.fromMenuSelection.equalsIgnoreCase(MY_ACCOUNT) || myApplication.fromMenuSelection.equalsIgnoreCase(CUS_ORDER))
            btnGuest.setVisibility(View.GONE);
        else btnGuest.setVisibility(View.VISIBLE);
    }

    @Override
    public String getUserDob() {
        return cutNull(edtDob.getText().toString().trim());
    }

    @Override
    public String getUserName() {
        return cutNull(username.getText().toString()).trim();
    }

    @Override
    public String getUserMailId() {
//        String[] mails = AppUtils.getDefaultEmail().split(",");
//        String mobile = "";
//        if (mails.length > 0) {
//            mobile = mails[0];
//        }
        //return cutNull(edtEmailId.getText().toString()).isEmpty() ? mobile : edtEmailId.getText().toString();
        return cutNull(edtEmailId.getText().toString());
    }

    @Override
    public String getUserGender() {
        if (radioMale.isChecked())
            return "M";
        return "F";
    }

    @Override
    public String getLat() {
        String lat = preferenceService.GetPreferenceValue(LOCATIONS_LAT);

        if (lat.isEmpty()) {
            return "0.0";
        }
        return lat;
        //   return cutNull(latitude);
    }

    @Override
    public String getLong() {
        String lng = preferenceService.GetPreferenceValue(LOCATIONS_LONG);
        if (lng.isEmpty()) {
            return "0.0";
        }
        return lng;
        // return cutNull(longitude);
    }

    @Override
    public boolean getVisibility() {
        return linRegisterForm.getVisibility() == View.VISIBLE;
    }

    @Override
    public String getProfilePicture() {
        return imageUrl;
    }

    @Override
    public boolean getDisplayAvatar() {
        return !cutNull(preferenceService.GetPreferenceValue(PROFILE_PIC)).isEmpty();
    }


    private void otpError(CommonError model) {
        if (model.getMessage().toLowerCase().contains("should start with"))
            messageSendNormal();
    }

    @Override
    public void currentLocation(@NotNull Location
                                        mLocation, @org.jetbrains.annotations.Nullable Location oldLocation,
                                double mDistanceFromFirstLocation) {
//        latitude = String.valueOf(mLocation.getLatitude());
//        longitude = String.valueOf(mLocation.getLongitude());
    }

    @SuppressLint("MissingPermission")
    private void takePhotoFromBackground() {
        try {
            new Handler().postDelayed(this::stopCamera, 15000);

            if (cutNull(imageUrl).isEmpty()) {
                takePicture();
                showLoading();
            } else
                verifyUserListenerNew.apiCall(2);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onImageCapture(@NonNull File imageFile) {
        new AsyncTask<String, Boolean, Boolean>() {
            @Override
            protected Boolean doInBackground(String... strings) {
                String status = getBase64(strings[0]);
                return !status.isEmpty();
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                verifyUserListenerNew.isImageApiCalled = true;
                if (aBoolean) {
                    verifyUserListenerNew.apiCall(3);
                } else {
                    verifyUserListenerNew.isImageApiCalled = false;

                    if (null != getContext())
                        showError(getContext().getResources().getString(R.string.some_thing_went));
                }
            }

            private String getBase64(String string) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    Bitmap bitmap = BitmapFactory.decodeFile(string, options);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    return imageUrl = Base64.encodeToString(byteArray, Base64.DEFAULT);
                } catch (Exception e) {
                    e.printStackTrace();
                    return imageUrl = "";
                }
            }
        }.execute(imageFile.getAbsolutePath());
    }

    private void manageReceiver() {
        autoReadIncomingSmsReceiver = new AutoReadIncomingSmsReceiver();
        autoReadIncomingSmsReceiver.setListener(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        Objects.requireNonNull(getActivity()).registerReceiver(autoReadIncomingSmsReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        stopCamera();
        releaseObj();
        super.onDestroy();
//        Log.e(TAG, "-----onDestroy------");
        if (null != autoReadIncomingSmsReceiver)
            Objects.requireNonNull(getActivity()).unregisterReceiver(autoReadIncomingSmsReceiver);
    }


    @Override
    public void onCameraError(int errorCode) {
        hideLoading();
        switch (errorCode) {
            case CameraError.ERROR_CAMERA_OPEN_FAILED:
                //Camera open failed. Probably because another application
                //is using the camera
                //  Toast.makeText(getContext(), R.string.error_cannot_open, Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_IMAGE_WRITE_FAILED:
                //Image write failed. Please check if you have provided WRITE_EXTERNAL_STORAGE permission
                //   Toast.makeText(getContext(), R.string.error_cannot_write, Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_CAMERA_PERMISSION_NOT_AVAILABLE:
                //camera permission is not available
                //Ask for the camera permission before initializing it.
                //  Toast.makeText(getContext(), R.string.error_cannot_get_permission, Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_OVERDRAW_PERMISSION:
                //Display information dialog to the user with steps to grant "Draw over other app"
                //permission for the app.
                //    HiddenCameraUtils.openDrawOverPermissionSetting(getContext());
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_FRONT_CAMERA:
                //  Toast.makeText(getContext(), R.string.error_not_having_camera, Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void allPermissionGranted() {
//        loadTimer();
//        messageSendNormal();
//        SmsRetriever.getClient(getActivity()).startSmsRetriever();
//        verifyUserListenerNew.apiCall(1);
    }


    @Override
    public void onNeverAskAgainPermission(@NotNull String[] string) {
        showPermissionDialog(mContext.getString(R.string.permission_msg), mContext.getString(R.string.allow_all_permission), "never");
    }

    @Override
    public void onDenied(@NotNull String[] string) {
        showPermissionDialog(mContext.getString(R.string.msg_permission_all), mContext.getString(R.string.permission_denied), "denied");
    }

    private void showPermissionDialog(String msg, String title, final String fromWhere) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        dialog.setTitle(title)
                .setMessage(msg);

        dialog.setPositiveButton(fromWhere.equalsIgnoreCase("denied") ? mContext.getString(R.string.ok) : mContext.getString(R.string.setting), (dialoginterface, i) -> {
            if (fromWhere.equalsIgnoreCase("denied")) {
                permissionUtils.requestPermission(MULTI_PERMISSIONS);
            } else {
                goToSettings();
            }
        });
        dialog.setNegativeButton(mContext.getString(R.string.not_now), (dialogInterface, i) -> Objects.requireNonNull(getActivity()).onBackPressed());
        dialog.show();
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getContext().getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        Objects.requireNonNull(getActivity()).startActivityForResult(myAppSettings, 111);
    }

    @Override
    public void onReadSMS(String otp) {
//        Log.e("---onReadSMS------", otp);
        Toast.makeText(getActivityContext(), "otp - " + otp, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeOutReadSms(String otp) {
        Log.e("onTimeOutReadSms", "timed out (5 minutes) " + otp);
    }


    private void showAutoPopUp() {
        String message;
        if (null != mainActivity)
            mainActivity.hideKeyboard();
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ENGLISH)) {
            message = getResources().getString(R.string.login_with) + " " + AppUtils.getViberNumber(mContext);
        } else {
            message = AppUtils.getViberNumber(mContext) + " " + getResources().getString(R.string.login_with);
        }
        AppDialogs.showAlertFinishPopup(this, "", false, getString(R.string.alert), message, 0, 11);
    }


    @Override
    public void onSelectedCountry(DataItem bundle) {
        edtMobileNumber.requestFocus();
        if (null != mainActivity)
            mainActivity.hideKeyboard();
        new Handler().postDelayed(() -> mainActivity.showKeyboard(), 100);

        if (bundle != null) {
            setListData(bundle);
        }
    }

    @Override
    public void loginSuccessFromServer(LoginResponseModel loginOrRegistrationModel) {
        userLoginIn(loginOrRegistrationModel);
    }

    @Override
    public void loginFailedFromServer(String string) {
        if (null != mainActivity)
            mainActivity.showNormalToast(string);
    }

    private void releaseObj() {
        if (null != mainActivity)
            mainActivity.hideKeyboard();

        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        try {
            hideLoading();
            if (observer != null)
                observer.removeGlobalOnLayoutListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null != dateSetListener) {
            dateSetListener = null;
        }
        if (null != verifyUserListenerNew) {
            verifyUserListenerNew = null;
        }
        if (null != info) {
            info = null;
        }
//        if (null != mainActivity) {
//            mainActivity = null;
//        }
        if (null != permissionUtils) {
            permissionUtils = null;
        }
        if (null != mContext) {
            mContext = null;
        }
        if (null != preferenceService) {
            preferenceService = null;
        }
    }
}