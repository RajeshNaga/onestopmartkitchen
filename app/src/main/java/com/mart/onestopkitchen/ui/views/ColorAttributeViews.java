package com.mart.onestopkitchen.ui.views;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import com.google.android.material.appbar.AppBarLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.AttributeControlType;
import com.mart.onestopkitchen.model.AttributeControlValue;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductAttribute;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.PriceResponse;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ColorAttributeViews extends LinearLayout {

    List<ProductAttribute> attributes;

    Context context;

    LinearLayout colorLayout;

    LinearLayout attributeLayout;

    private onSelectColorListener onSelectColorListener;

    public ColorAttributeViews(Context context) {
        super(context);
    }

    public ColorAttributeViews(Context context, List<ProductAttribute> attributes, LinearLayout layout, onSelectColorListener onSelectColorListener) {
        super(context);
        this.context = context;
        this.attributes = attributes;
        this.colorLayout = layout;
        this.onSelectColorListener = onSelectColorListener;
        generateView();

    }

    private void generateView() {
        for (ProductAttribute productAttribute : attributes) {
            if (productAttribute.getAttributeControlType() == AttributeControlType.ColorSquares) {
                if (productAttribute.getValues().size() > 0) {
                    onSelectColorListener.showColorLayout();
                    generateColorSqauares(productAttribute);
                }
            }
        }
    }

    private void generateColorSqauares(final ProductAttribute productAttribute) {
        final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.linear_layout, null);
        for (final AttributeControlValue value : productAttribute.getValues()) {

            LayoutParams imageViewParams = new AppBarLayout.LayoutParams(50, 50);
            imageViewParams.setMargins(0, 0, 10, 0);

            ImageView imageView = new ImageView(context);
            imageView.setLayoutParams(imageViewParams);
            imageView.setMaxHeight(20);
            imageView.setMaxWidth(20);

            GradientDrawable shapeDrawable;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.bg_color_square, context.getTheme());
            } else {
                shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.bg_color_square);
            }

            try {
                shapeDrawable.setColor(Color.parseColor(value.getColorSquaresRgb()));
                shapeDrawable.setStroke(5, Color.TRANSPARENT, 10, 10);
            } catch (Exception ex) {
                // who cares
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                imageView.setBackground(shapeDrawable);
            } else {
                imageView.setBackgroundDrawable(shapeDrawable);
            }

            imageView.setClickable(true);
            imageView.setFocusable(true);
            linearLayout.addView(imageView);

            if (value.isPreSelected()) {

            }

            Glide.with(context).load(value.getPictureModel().getImageUrl())
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

            imageView.setOnClickListener(v -> {
                clearSelection(linearLayout);
                selectColorCircle(v, value);
                String key = getKey(productAttribute);
                Map<String, String> valueTextPairMap = new HashMap<>();
                valueTextPairMap.put(String.valueOf(value.getId()), key);
                List<KeyValuePair> valuePair = onSelectColorListener.getSelectedColor(valueTextPairMap);
                callColorWebservice(valuePair);
            });
        }

        attributeLayout = (LinearLayout) getLayoutInflater().
                inflate(R.layout.separate_layout_each_attribute_product_details, colorLayout, false);
        attributeLayout.addView(linearLayout);
        colorLayout.addView(attributeLayout);
    }

    public void callColorWebservice(List<KeyValuePair> valuePair) {
        RetroClient.getApi().getUpdatedPrice(ProductDetailFragment.productModel.getId(), valuePair)
                .enqueue(new CustomCB<PriceResponse>(ProductDetailFragment.self.getView()));
    }

    private void selectColorCircle(View view, AttributeControlValue value) {
        GradientDrawable shapeDrawable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.bg_color_square, context.getTheme());
        } else {
            shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.bg_color_square);
        }

        try {
            shapeDrawable.setColor(Color.parseColor(value.getColorSquaresRgb()));
            shapeDrawable.setStroke(8, Color.parseColor(value.getColorSquaresRgb()));
        } catch (Exception ex) {
            // who cares
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(shapeDrawable);
        } else {
            view.setBackgroundDrawable(shapeDrawable);
        }
    }

    private LayoutInflater getLayoutInflater() {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater;
    }

    private void clearSelection(LinearLayout layout) {
        for (int i = 0; i < layout.getChildCount(); i++) {
            ImageView imageView = (ImageView) layout.getChildAt(i);

            GradientDrawable shapeDrawable = (GradientDrawable) imageView.getBackground();
            try {
                shapeDrawable.setStroke(5, Color.TRANSPARENT);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                imageView.setBackground(shapeDrawable);
            } else {
                imageView.setBackgroundDrawable(shapeDrawable);
            }
        }
    }

    public String getKey(ProductAttribute productAttribute) {
        String key = String.format("%s_%d_%d_%d", "product_attribute", productAttribute.getProductId()
                , productAttribute.getProductAttributeId(), productAttribute.getId());
        return key;
    }

    public interface onSelectColorListener {
        List<KeyValuePair> getSelectedColor(Map<String, String> pairValue);

        void showColorLayout();
    }
}
