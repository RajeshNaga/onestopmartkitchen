package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.event.OrderDetailsEvent;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.model.CustomerOrder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class CustomerOrderAdapter extends RecyclerView.Adapter<CustomerOrderAdapter.MyViewHolder> {
    private ArrayList<CustomerOrder> mDataset = null;
    private Context context = null;
    private LayoutInflater inflater = null;

    public CustomerOrderAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void loadData(ArrayList<CustomerOrder> myDataset) {
        this.mDataset = myDataset;
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public CustomerOrderAdapter.MyViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_order, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint({"SimpleDateFormat", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, final int position) {
        CustomerOrder order = mDataset.get(position);
        List<CartProduct> orderList = mDataset.get(position).getItems();
        StringBuilder csvList = new StringBuilder();
        int totalCount = 0;
        holder.setIsRecyclable(false);
        for (int i = 0; i < orderList.size(); i++) {
            if (orderList.size() > 1) {
                csvList.append(orderList.get(i).getProductName());
                csvList.append("\n");
                holder.product_Name.setText(csvList);
                if (order.getOrderStatus().equalsIgnoreCase("Complete")) {
                    holder.tv_product_acturl_price.setText(order.getExpectedDeliveryDate());
                } else {
                    holder.tv_product_acturl_price.setText(order.getOrderStatus());
                    holder.order_processing.setVisibility(View.GONE);
                    holder.tv_product_acturl_price.setTextColor(context.getResources().getColor(R.color.bonus_red));
                }
                totalCount = totalCount + orderList.get(i).getQuantity();
                holder.tv_moretext.setVisibility(View.GONE);
                holder.tv_totalPrice.setText("" + order.getOrderTotal());
                holder.sliderLayout.setVisibility(View.VISIBLE);

                try {
                    DefaultSliderView textSliderView = new DefaultSliderView(context);
                    textSliderView.image(orderList.get(i).getPicture().getImageUrl()).setScaleType(BaseSliderView.ScaleType.CenterInside);
                    holder.sliderLayout.addSlider(textSliderView);
                    holder.sliderLayout.setDuration(1100);
                } catch (Exception ignore) {
                }

                holder.productImage.setVisibility(View.GONE);


            } else {
                if (orderList.get(i).getQuantity() >= 1) {
                    totalCount = totalCount + orderList.get(i).getQuantity();
                    holder.tv_totalPrice.setText(order.getOrderTotal());
                    holder.tv_moretext.setVisibility(View.GONE);
                    if (order.getOrderStatus().equalsIgnoreCase("Complete")) {
                        holder.tv_product_acturl_price.setText(order.getExpectedDeliveryDate());
                    } else {
                        holder.tv_product_acturl_price.setText(order.getOrderStatus());
                        holder.order_processing.setVisibility(View.GONE);
                        holder.tv_product_acturl_price.setTextColor(context.getResources().getColor(R.color.bonus_red));

                    }
                    holder.product_Name.setText(orderList.get(i).getProductName());
                    holder.productImage.setVisibility(View.VISIBLE);
                    Glide.with(context).load(String.valueOf(orderList.get(i).getPicture().getImageUrl()))
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.productImage);
                    holder.sliderLayout.setVisibility(View.GONE);

                } else {
                    totalCount = totalCount + orderList.get(i).getQuantity();
                    holder.tv_moretext.setVisibility(View.GONE);
                    holder.tv_quantity.setText(context.getString(R.string.total_quantity) + "\n" + order.getOrderTotal());
                    if (order.getOrderStatus().equalsIgnoreCase("Complete")) {
                        holder.tv_product_acturl_price.setText(order.getExpectedDeliveryDate());
                    } else {
                        holder.tv_product_acturl_price.setText(order.getOrderStatus());
                        holder.tv_product_acturl_price.setTextColor(context.getResources().getColor(R.color.bonus_red));
                        holder.order_processing.setVisibility(View.GONE);
                    }
                    holder.product_Name.setText(orderList.get(i).getProductName());
                    holder.productImage.setVisibility(View.VISIBLE);
                    Glide.with(context).load(String.valueOf(orderList.get(i).getPicture().getImageUrl()))
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.productImage);
                    holder.sliderLayout.setVisibility(View.GONE);
                }
            }
            if (orderList.size() > 1) {
                holder.tv_moretext.setText(R.string.and_more);
                holder.tv_moretext.setVisibility(View.VISIBLE);
            } else {
                holder.tv_moretext.setVisibility(View.GONE);
            }
        }

        if (totalCount != 0) {
            holder.tv_quantity.setText(context.getString(R.string.total_quantity) + "\n" + totalCount);
        } else {
            holder.tv_quantity.setText(context.getString(R.string.total_quantity) + "\n" + totalCount);
            holder.tv_totalPrice.setText(order.getOrderTotal());
        }

        holder.tv_cutomer_order_number.setText(context.getString(R.string.txt_order_number) + order.getCustomOrderNumber());

        holder.row.setOnClickListener(v -> EventBus.getDefault().post(new OrderDetailsEvent(order.getId())));
    }

    @Override
    public int getItemCount() {
        return (null != mDataset && mDataset.size() > 0) ? mDataset.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.row)
        LinearLayout row;

        @BindView(R.id.img_productImage)
        ImageView productImage;

        @BindView(R.id.tv_moretext)
        TextView tv_moretext;

        @BindView(R.id.tv_product_acturl_price)
        TextView tv_product_acturl_price;

        @BindView(R.id.order_processing)
        TextView order_processing;

        @BindView(R.id.tv_productName)
        TextView product_Name;

        @BindView(R.id.tv_quantity)
        TextView tv_quantity;

        @BindView(R.id.tvquantity)
        TextView tvquantity;

        @BindView(R.id.tv_totalPrice)
        TextView tv_totalPrice;

        @BindView(R.id.tv_cutomer_order_number)
        TextView tv_cutomer_order_number;

        @BindView(R.id.slider)
        SliderLayout sliderLayout;


        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void releaseObj() {
        if (null != mDataset) {
            mDataset = null;
        }
        if (null != context) {
            context = null;
        }
        if (null != inflater) {
            inflater = null;
        }

    }

}