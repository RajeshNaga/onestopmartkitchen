package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProductAdapterRecent extends RecyclerView.Adapter<ProductAdapterRecent.ViewHolder> {

    private View view;
    private Context mContext;
    private List<ProductModel> recentList;
    private RecentProductClick clickEvent;

    public ProductAdapterRecent(Context context, RecentProductClick click, View views) {
        view= view;
        this.mContext = context;
        this.recentList = new ArrayList<>();
        this.clickEvent = click;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_homepage_product_recent, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        try {
            ProductModel productModel = recentList.get(i);
            holder.onItemBind(productModel);
            holder.tvProductName.setText(AppUtils.capitalizeWords(productModel.getName()));
            if (productModel.getProductPrice().getDiscountPercentage() > 0) {
                holder.tvOfferPercentage.setVisibility(View.VISIBLE);
                holder.tvOfferPercentage.setText(String.valueOf(productModel.getProductPrice().getDiscountPercentage()) + "%");
            } else
                holder.tvOfferPercentage.setVisibility(View.GONE);


            if (null != productModel.getProductPrice().getPriceWithDiscount() && !productModel.getProductPrice().getPriceWithDiscount().isEmpty()) {
                holder.tvProductOldPrice.setVisibility(View.VISIBLE);
                holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), productModel.getProductPrice().getPriceWithDiscount(), 0));
                holder.tvProductOldPrice.setText(AppUtils.getMMKString(holder.tvProductOldPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));

            } else if (null != productModel.getProductPrice().getOldPrice()) {
                holder.tvProductOldPrice.setVisibility(View.VISIBLE);
                holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                holder.tvProductOldPrice.setText(AppUtils.getMMKString(holder.tvProductOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));

            } else {
                holder.tvProductPrice.setText(AppUtils.getMMKString(holder.tvProductPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
                holder.tvProductOldPrice.setText(AppUtils.getMMKString(holder.tvProductOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));
                holder.tvProductOldPrice.setVisibility(View.GONE);
            }
            holder.wishList.setChecked(productModel.getWishList());
            holder.wishList.setOnClickListener(view -> {
                if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                    if (holder.wishList.isChecked()) {
                      // holder.wishList.setChecked(false);
                        clickEvent.callAddWishListApi(productModel.getId());

                     } else {
                        if (productModel.getId() != 0L) {
                          //  productModel.setWishList(false);
                            clickEvent.removeWishlist(productModel.getId());
                        }
                    }
                } else {
                    holder.wishList.setChecked(false);
                }
            });

            if (null != holder.gifMarkNew) {
                if (productModel.getMarkAsNew()) {
                    holder.gifMarkNew.setVisibility(View.VISIBLE);
                    Glide
                            .with(mContext)
                            .load(R.drawable.new_3)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.gifMarkNew);
                } else {
                    holder.gifMarkNew.setVisibility(View.GONE);
                }
            }
            Glide
                    .with(mContext)
                    .load(productModel.getDefaultPictureModel().getImageUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgProductImage);


        } catch (Exception e) {
        }
    }

    private void callApiForWishList(long id, int i) {

    }

    @Override
    public int getItemCount() {
        return recentList == null ? 0 : recentList.size();
    }

    public void loadData(List<ProductModel> recentList) {
        this.recentList = recentList;
        notifyDataSetChanged();
    }


    public interface RecentProductClick {
        void productClick(ProductModel dataItem);

        void callAddWishListApi(long id);

        void removeWishlist(long id);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_productImage)
        ImageView imgProductImage;
        @BindView(R.id.tv_productName)
        TextView tvProductName;
        @BindView(R.id.tv_productPrice)
        TextView tvProductPrice;
        @BindView(R.id.tv_productOldPrice)
        TextView tvProductOldPrice;
        @BindView(R.id.tv_discount_percentage)
        TextView tvOfferPercentage;
        @BindView(R.id.gif_mark_new)
        ImageView gifMarkNew;
        @BindView(R.id.button_favorite_category)
        ToggleButton wishList;

        private ProductModel dataItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvProductOldPrice.setPaintFlags(tvProductOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        @OnClick(R.id.card_total)
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.card_total:
                    clickEvent.productClick(dataItem);
                    break;
            }
        }


        public void onItemBind(ProductModel productModel) {
            this.dataItem = productModel;
        }
    }
}

