package com.mart.onestopkitchen.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.model.CartProduct
import com.mart.onestopkitchen.model.KeyValuePair
import com.mart.onestopkitchen.networking.CustomCB
import com.mart.onestopkitchen.networking.RetroClient
import com.mart.onestopkitchen.service.PreferenceService
import kotlinx.android.synthetic.main.item_wish_list.view.*
import java.util.*


class WishListAdapter(
        context: Context,
        productsList: MutableList<CartProduct>,
        fragment: Fragment, preferenceService: PreferenceService,
        private val clickListener: OnProductSelectListener) : CartAdapter(context, productsList, fragment, preferenceService) {

    companion object {
        var mClickListener: OnProductSelectListener? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ProductSummaryHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_wish_list, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(bindViewHolder: RecyclerView.ViewHolder, position: Int) {
        mClickListener = clickListener
        productModel = products[position]
        val holder = bindViewHolder as ProductSummaryHolder
        holder.bindData(productModel)
        holder.itemView.iconRemove?.let { onTrashClicked(it, position) }

        products[position].discountPercentage?.let {
            if (!products[position].discountPercentage.isEmpty() && products[position].discountPercentage != "0") {
                holder.itemView.tvProductOffer.visibility = View.VISIBLE
                holder.itemView.tvProductOffer.text = products[position].discountPercentage + " %"
                holder.itemView.tvOriginalPrice.visibility = View.VISIBLE
                holder.itemView.tvOldprice.visibility = View.GONE
            } else
                holder.itemView.tvProductOffer.visibility = View.GONE
        }

        //holder.itemView.iconAddCart?.let { onAddtoCartButtonClicked(it, position) }
        Glide.with(context)
                .load(products[position].picture?.imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.itemView.imgProductImage)
        holder.itemView.setOnClickListener {
            // mItemClickListener.onItemSelect(productModel.id,productModel.productName)
            if (mClickListener != null) {
                mClickListener?.onItemSelect(products[position].productId, products[position].productName)
            }

        }
    }

    private fun onAddtoCartButtonClicked(addToCartBtn: ImageView, position: Int) {
        addToCartBtn.setOnClickListener { callAddToCartWebService(position) }
    }

    private fun callAddToCartWebService(position: Int) {
        val keyValuePairs = ArrayList<KeyValuePair>()
        KeyValuePair().apply {
            key = "addtocart"
            value = products[position].id.toString() + ""
            keyValuePairs.add(this)
        }
        RetroClient.getApi().addItemsToCartFromWishList(keyValuePairs).enqueue(CustomCB(fragment!!.view))
    }

    private fun onTrashClicked(itemview: ImageView, position: Int) {
        itemview.setOnClickListener {
            if (position < products.size) {
                updateCartItem("removefromcart", "" + products[position].id)
                //  products.remove(position);
                //notifyDataSetChanged();
                /*if (getItemCount() == 0) {
        EventBus.getDefault().post(new RemoveWishlistItemEvent(0));
    }*/
            }
        }
    }

    /* fun removeItemFromList(position: Int)
      {
          products.remove(position)
          notifyDataSetChanged()
          if (itemCount == 0)
              EventBus.getDefault().post(RemoveWishlistItemEvent(0))
      }*/


    fun updateCartItem(key: String, value: String) {
        val keyValuePairs = ArrayList<KeyValuePair>()
        KeyValuePair().apply {
            this.key = key
            this.value = value
            keyValuePairs.add(this)
        }
        RetroClient.getApi().updateWishList(keyValuePairs).enqueue(CustomCB(fragment.view!!))
    }

    override fun getItemCount() = products.size

    class ProductSummaryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(data: CartProduct) = with(itemView) {
            tvProductName.text = data.productName
            tvProductPrice.text = data.unitPrice
            tvOldprice.text = data.oldPrice
            tvOldprice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            tvOriginalPrice.text = data.price
            tvOriginalPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        }
    }

    interface OnProductSelectListener {
        fun onItemSelect(pId: Int, name: String)
    }
}
