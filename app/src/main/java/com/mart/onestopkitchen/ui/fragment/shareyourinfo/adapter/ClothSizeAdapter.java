package com.mart.onestopkitchen.ui.fragment.shareyourinfo.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.TailoringSizesItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClothSizeAdapter extends RecyclerView.Adapter<ClothSizeAdapter.ViewHolder> {

    List<TailoringSizesItem> availableSizes;
    private int currentPos = 0;
    private OnSizeClicked sizeClicked;

    public ClothSizeAdapter(OnSizeClicked clicked) {
        this.sizeClicked = clicked;
        this.availableSizes = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sizes, parent, false));
    }

    public void loadData(List<TailoringSizesItem> availableSizes) {
        this.availableSizes = availableSizes;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final TailoringSizesItem sizesItem = availableSizes.get(position);
        holder.txtSizeGuide.setText(availableSizes.get(position).getSize());
        if (currentPos == position) {
            sizeClicked.onSizeClicked(sizesItem);
            holder.relSelectedPos.setVisibility(View.VISIBLE);
        } else holder.relSelectedPos.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return availableSizes == null ? 0 : availableSizes.size();
    }


    public interface OnSizeClicked {
        void onSizeClicked(TailoringSizesItem sizesItem);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rel_selected_pos)
        RelativeLayout relSelectedPos;
        @BindView(R.id.txt_size_guide)
        TextView txtSizeGuide;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.txt_size_guide)
        public void onViewClicked() {
            currentPos = getAdapterPosition();
            notifyDataSetChanged();
        }
    }
}
