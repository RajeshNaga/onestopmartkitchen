package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.mart.onestopkitchen.R;

import java.util.List;

/**
 * Created by Ashraful on 1/4/2016.
 */
public class ProductSortAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> sortOptionList;
    private LayoutInflater inflater;

    public ProductSortAdapter(Context mContext, List<String> sortOptionTextList) {
        this.mContext = mContext;
        this.sortOptionList = sortOptionTextList;
        inflater = LayoutInflater.from(mContext);
    }


    @Override
    public int getCount() {
        return sortOptionList.size();
    }


    @Override
    public Object getItem(int position) {
        return sortOptionList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.simple_list_item_single_choice, parent, false);
        }
        CheckedTextView tv = convertView.findViewById(android.R.id.text1);
        tv.setText(sortOptionList.get(position));
        return convertView;
    }
}
