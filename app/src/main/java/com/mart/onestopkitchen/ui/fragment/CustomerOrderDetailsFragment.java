package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.model.CustomerAddress;
import com.mart.onestopkitchen.model.OrderDetailsResponse;
import com.mart.onestopkitchen.model.OrderNotes;
import com.mart.onestopkitchen.mvp.ui.ratingandreview.RatingAndReviewFragment;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.adapter.CustomerOrderStatusAdapter;
import com.mart.onestopkitchen.ui.adapter.OrderDetailsProductAdapter;
import com.mart.onestopkitchen.ui.views.FormViews;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Language;
import com.mart.onestopkitchen.utils.ProductDetailsPDFInvoiceCreater;
import com.mart.onestopkitchen.utils.TextUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by mart-110 on 12/18/2015.
 */

public class CustomerOrderDetailsFragment extends BaseFragment implements OrderDetailsProductAdapter.OnRatingClick {
    @BindView(R.id.nes_screll)
    NestedScrollView nesScrell;
    @BindView(R.id.tv_subtotal)
    TextView subTotalTextView;
    @BindView(R.id.tv_shipping)
    TextView shippingTextView;
    @BindView(R.id.tv_tax)
    TextView taxTextView;
    @BindView(R.id.tv_Total)
    TextView totalAmountTextView;
    @BindView(R.id.tv_discount)
    TextView discountTextView;
    @BindView(R.id.rclv_orderProductList)
    RecyclerView orderProductList;
    @BindView(R.id.rclv_orderstatus)
    RecyclerView orderstatusList;
    @BindView(R.id.ll_billing_address)
    LinearLayout billingAddressLinearLayout;
    @BindView(R.id.ll_shipping_address)
    LinearLayout ShippingAddressLinearLayout;
    @BindView(R.id.tv_order_title)
    TextView orderTitleTextView;
    @BindView(R.id.ordered_and_approved)
    TextView orderSummeryTextView;
    @BindView(R.id.tv_payment_details)
    TextView paymentDetailsTextView;
    @BindView(R.id.tv_shipping_details)
    TextView shippingDetailsTextView;
    @BindView(R.id.tv_checkout_attr_info)
    TextView checkoutAttrInfoTextView;
    @BindView(R.id.tv_total_amount)
    TextView tv_total_amount;
    @BindView(R.id.tv_full_address)
    AutoCompleteTextView tv_full_address;
    @BindView(R.id.tv_full_address2)
    AutoCompleteTextView tv_full_address2;
    @BindView(R.id.tv_payment_mode)
    TextView tv_payment_mode;
    @BindView(R.id.tv_product_refrence_number)
    TextView tv_product_refrence_number;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_product_order_number)
    TextView tv_product_order_number;
    @BindView(R.id.ll_store_address)
    LinearLayout ll_store_address;
    @BindView(R.id.tv_product_amount)
    TextView tv_product_amount;
    @BindView(R.id.shippingLayout)
    LinearLayout shippingLayout;
    @BindView(R.id.storeLayouts)
    LinearLayout storeLayouts;
    @BindView(R.id.taxKey)
    TextView taxKey;
    @BindView(R.id.tv_phone_number)
    TextView tv_phone_number;
    @BindView(R.id.tax_row)
    TableRow taxrow;
    @BindView(R.id.deliverypoint)
    TextView deliverypoint;
    @BindView(R.id.customerName)
    TextView customerName;
    @BindView(R.id.tv_falatNumber)
    TextView tv_falatNumber;
    @BindView(R.id.tvPaymentStatus)
    TextView tvPaymentStatus;

    private String orderNumber, CreateOn;
    private OrderDetailsResponse OrderDetails = null;
    private int _orderId;
    private View view = null;
    private CustomerOrderStatusAdapter customerOrderStatusAdapter = null;
    private String TAG = CustomerOrderDetailsFragment.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_customer_order_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.order_details));
        nesScrell.setVisibility(View.GONE);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if (getArguments() != null) {
            _orderId = getArguments().getInt("orderId");
        }
        setLayoutManagerofRecyclerList();
        callOrderDetailsWebService();
    }

    @Override
    public void onResume() {
        super.onResume();
//        AppDialogs.dismissDialog();
    }

    private void callOrderDetailsWebService() {
        RetroClient.getApi().getOrderDetails(_orderId).enqueue(new CustomCB<OrderDetailsResponse>(this.getView()));
    }

    @SuppressLint({"SetTextI18n", "SimpleDateFormat", "RtlHardcoded"})
    public void onEvent(OrderDetailsResponse response) {
        if (response != null) {
            nesScrell.setVisibility(View.VISIBLE);
            OrderDetails = response;
//            String orderSummery = "";
            String paymentDetails = "";
            String shippingDetails = "";
            if (response.getStatusCode() == 200) {
                orderNumber = response.getCustomOrderNumber();
                CreateOn = response.getCreatedOn();

                /* for pdf  */

                File filePath = new File(Environment.getExternalStorageDirectory() + File.separator + getResources().getString(R.string.kitchen_app_name_for_pdf) + File.separator);
                String fileName = filePath + "/" + orderNumber + "-" + CreateOn + ".pdf";
                File file = new File(fileName);
                if (!file.exists()) {
                    if (null != getActivity() && null != response.getItems()) {
                        ProductDetailsPDFInvoiceCreater.getInstance(response, getActivity());
                    }
                }


                Log.d("OrderDetails", String.valueOf(response.getShippingStatus()));
                orderTitleTextView.setText(getString(R.string.order) + response.getId());
                SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                String date = "";
                try {
                    Date originalDate = parser.parse(TextUtils.getNullSafeString(response.getCreatedOn()));
                    Format dateFormat = DateFormat.getDateFormat(getContext());
                    Format timeFormat = DateFormat.getTimeFormat(getContext());
                    String format = ((SimpleDateFormat) dateFormat).toLocalizedPattern() + " " + ((SimpleDateFormat) timeFormat).toLocalizedPattern();
                    SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());
                    date = formatter.format(originalDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
//                orderSummery += getString(R.string.order_date) + " " + date + " \n";
//                orderSummery += getString(R.string.order_status) + " " + TextUtils.getNullSafeString(response.getOrderStatus()) + " \n";
//                orderSummery += getString(R.string.order_total) + " " + TextUtils.getNullSafeString(response.getOrderTotal());

                orderSummeryTextView.setText(date);
                tv_total_amount.setText(AppUtils.getMMKString(tv_total_amount.getTextSize(), response.getOrderTotal(), 0));
                tv_product_amount.setText(response.getOrderSubtotal());
                tv_product_order_number.setText(response.getCustomOrderNumber());
                tv_product_refrence_number.setText(response.getReferenceNumber());
                if (response.getTax().equalsIgnoreCase(getString(R.string.zero))) {
                    taxrow.setVisibility(View.GONE);
                } else {
                    taxTextView.setText(response.getTax());
                }
                tv_payment_mode.setText(getString(R.string.payment_mode) + ": " + response.getPaymentMethod());

                if (null != response.getPaymentMethodStatus()) {
                    tvPaymentStatus.setText(response.getPaymentMethodStatus());
                    if (response.getPaymentMethodStatus().equalsIgnoreCase("Pending")) {
                        tvPaymentStatus.setTextColor(getResources().getColor(R.color.lightred));
                    } else if (response.getPaymentMethodStatus().equalsIgnoreCase("Paid")) {
                        tvPaymentStatus.setTextColor(getResources().getColor(R.color.light_green));
                    } else {
                        tvPaymentStatus.setTextColor(getResources().getColor(R.color.lightblack));
                    }
                }
                tv_name.setText(response.getBillingAddress().getFirstName());
                tv_phone_number.setText(AppUtils.formattedNumber(response.getBillingAddress().getPhoneNumber()));

                populateAddress(response.getBillingAddress(), billingAddressLinearLayout);
                if (response.isPickUpInStore()) {
                    deliverypoint.setText(TextUtils.getNullSafeString(getString(R.string.pickup_point_address)));
                    customerName.setText(TextUtils.getNullSafeString(response.getPickupAddress().getFirstName()));
                    String houseNo = (response.getPickupAddress().getHouseNo() != null) ? (!response.getPickupAddress().getHouseNo().isEmpty()) ? getString(R.string.house_no) + response.getPickupAddress().getHouseNo() + "," : "" : "";
                    String roomNo = (response.getPickupAddress().getRoomNo() != null) ? (!response.getPickupAddress().getRoomNo().isEmpty() ? getString(R.string.room_no) + response.getPickupAddress().getRoomNo() + "," : "") : "";
                    String floorNo = (response.getPickupAddress().getFloorNo() != null) ? (!response.getPickupAddress().getFloorNo().isEmpty() ? getString(R.string.floor_no) + response.getPickupAddress().getFloorNo() + "," : "") : "";
                    if (houseNo.equalsIgnoreCase("") && roomNo.equalsIgnoreCase("") && floorNo.equalsIgnoreCase("")) {
                        tv_falatNumber.setVisibility(View.GONE);
                    }
                    tv_falatNumber.setText(TextUtils.getNullSafeString(houseNo) + TextUtils.getNullSafeString(floorNo) + TextUtils.getNullSafeString(roomNo));
                    if (null != response.getPickupAddress().getAddress1()) {
                        tv_full_address.setText(TextUtils.getNullSafeString(response.getPickupAddress().getAddress1()));
                    } else {
                        tv_full_address.setText("");
                    }
                    tv_full_address2.setText(((response.getPickupAddress().getAddress2() != null) ? response.getPickupAddress().getAddress2() + ", " : "") /*+ "  " + TextUtils.getNullSafeString(response.getPickupAddress().getCity())*/ + TextUtils.getNullSafeString(response.getPickupAddress().getStateProvinceName()) + " , " + TextUtils.getNullSafeString(response.getPickupAddress().getCountryName()));
                    storeLayouts.setVisibility(View.VISIBLE);
                    shippingLayout.setVisibility(View.GONE);
                    storeAddress(response.getPickupAddress(), ll_store_address);
                } else {
                    String houseNo = (response.getShippingAddress().getHouseNo() != null) ? (!response.getShippingAddress().getHouseNo().isEmpty()) ? getString(R.string.house_no) + response.getShippingAddress().getHouseNo() + "," : "" : "";
                    String roomNo = (response.getShippingAddress().getRoomNo() != null) ? (!response.getShippingAddress().getRoomNo().isEmpty() ? getString(R.string.room_no) + response.getShippingAddress().getRoomNo() + "," : "") : "";
                    String floorNo = (response.getShippingAddress().getFloorNo() != null) ? (!response.getShippingAddress().getFloorNo().isEmpty() ? getString(R.string.floor_no) + response.getShippingAddress().getFloorNo() + "," : "") : "";
                    if (houseNo.equalsIgnoreCase("") && roomNo.equalsIgnoreCase("") && floorNo.equalsIgnoreCase("")) {
                        tv_falatNumber.setVisibility(View.GONE);
                    }
                    tv_falatNumber.setText(TextUtils.getNullSafeString(houseNo) + TextUtils.getNullSafeString(floorNo) + TextUtils.getNullSafeString(roomNo));
                    deliverypoint.setText(TextUtils.getNullSafeString(getString(R.string.delivery_address)));
                    customerName.setText(TextUtils.getNullSafeString(response.getShippingAddress().getFirstName()));
                    if (!TextUtils.getNullSafeString(response.getShippingAddress().getAddress1()).equalsIgnoreCase("")) {
                        tv_full_address.setText(response.getShippingAddress().getAddress1());
                    } else {
                        tv_full_address.setText("");
                    }
                    Log.e("####--", "--response.getShippingAddress().getAddress2()-->" + "" + response.getShippingAddress().getAddress2());
                    String addressTwo = ((null != response.getShippingAddress().getAddress2()) && (!response.getShippingAddress().getAddress2().isEmpty()) ? response.getShippingAddress().getAddress2() + " , " : "");
                    tv_full_address2.setText(addressTwo + TextUtils.getNullSafeString(response.getShippingAddress().getCity()) + " , " + response.getShippingAddress().getStateProvinceName() + " , " + response.getShippingAddress().getCountryName());
                    storeLayouts.setVisibility(View.GONE);
                    shippingLayout.setVisibility(View.VISIBLE);
                    populateAddress(response.getShippingAddress(), ShippingAddressLinearLayout);
                }
                subTotalTextView.setText(TextUtils.getNullSafeString(response.getOrderSubtotal()));
                if (response.getOrderShipping().equalsIgnoreCase(getString(R.string.zero))) {
                    shippingTextView.setText(R.string.free);
                } else {
                    shippingTextView.setText(TextUtils.getNullSafeString(response.getOrderShipping()));
                }
                if (response.getTax().equalsIgnoreCase(getString(R.string.zero))) {
                    taxrow.setVisibility(View.GONE);
                } else {
                    taxTextView.setText(TextUtils.getNullSafeString(response.getTax()));
                }
                discountTextView.setText(TextUtils.getNullSafeString(response.getOrderTotalDiscount()));
                totalAmountTextView.setText(TextUtils.getNullSafeString(response.getOrderTotal()));
                paymentDetails += getString(R.string.payment_method) + " " + response.getPaymentMethod() + "\n";
                paymentDetails += getString(R.string.payment_status) + " " + response.getPaymentMethodStatus();
                paymentDetailsTextView.setText(paymentDetails);
                shippingDetails += getString(R.string.shipping_method) + " " + response.getShippingMethod() + "\n";
                shippingDetails += getString(R.string.shipping_status) + " " + response.getShippingStatus();
                shippingDetailsTextView.setText(shippingDetails);
                if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
                    shippingDetailsTextView.setGravity(Gravity.RIGHT);
                    paymentDetailsTextView.setGravity(Gravity.RIGHT);
                    subTotalTextView.setGravity(Gravity.LEFT);
                    shippingTextView.setGravity(Gravity.LEFT);
                    taxTextView.setGravity(Gravity.LEFT);
                    discountTextView.setGravity(Gravity.LEFT);
                    totalAmountTextView.setGravity(Gravity.LEFT);
                    taxKey.setGravity(Gravity.RIGHT);
                }
                if (response.getCheckoutAttributeInfo() != null && !response.getCheckoutAttributeInfo().isEmpty()) {
                    checkoutAttrInfoTextView.setVisibility(View.VISIBLE);
                    checkoutAttrInfoTextView.setText(response.getCheckoutAttributeInfo());
                }
                processCheckoutProductList(response.getItems(), response.getShippingStatus());
                if (null != response.getOrderNotes() && response.getOrderNotes().size() > 0)
                    processOrderNotes(response.getOrderNotes());
            }
        } else
            Toast.makeText(

                    getContext(), getString(R.string.some_thing_went), Toast.LENGTH_SHORT).

                    show();

    }

    private void processOrderNotes(ArrayList<OrderNotes> orderNoteList) {
        customerOrderStatusAdapter = new CustomerOrderStatusAdapter(getActivity(), orderNoteList);
        if (orderstatusList != null) {
            orderstatusList.setLayoutManager(new LinearLayoutManager(getActivity()));
            orderstatusList.setAdapter(customerOrderStatusAdapter);
            customerOrderStatusAdapter.notifyDataSetChanged();
        }
    }

    private void processCheckoutProductList(ArrayList<CartProduct> Items, String orderStatus) {
        if (Items != null) {
            if (Items.size() > 0) {
                populatedDatainAdapter(Items, orderStatus);
            }
        }
    }

    private void populatedDatainAdapter(ArrayList<CartProduct> cartProductList, String orderStatus) {
        OrderDetailsProductAdapter orderDetailsProductAdapter = new OrderDetailsProductAdapter(getActivity(), cartProductList, orderStatus);
        orderDetailsProductAdapter.setOnRatingListener(this);
        orderProductList.setAdapter(orderDetailsProductAdapter);
//        cartProductList.add(new CartProduct());
//        orderDetailsProductAdapter.notifyDataSetChanged();
    }

    private void setLayoutManagerofRecyclerList() {
//        CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        orderProductList.setHasFixedSize(true);
//        orderProductList.setLayoutManager(layoutManager);
//        layoutManager = new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL, false);
        orderstatusList.setHasFixedSize(true);
//        orderstatusList.setLayoutManager(layoutManager);
    }

    @SuppressLint("RtlHardcoded")
    private void storeAddress(CustomerAddress billingAddress, LinearLayout layout) {
        FormViews.setText(R.id.tv_addresss, "" + TextUtils.getNullSafeString(billingAddress.getAddress1()), layout);
        FormViews.setText(R.id.tv_citys, "" + TextUtils.getNullSafeString(billingAddress.getCity() + ", " + billingAddress.getZipPostalCode()), layout);
        FormViews.setText(R.id.tv_countrys, "" + TextUtils.getNullSafeString(billingAddress.getCountryName()), layout);
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            ((TextView) layout.findViewById(R.id.tv_addresss)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_citys)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_countrys)).setGravity(Gravity.RIGHT);
        }
    }

    @SuppressLint("RtlHardcoded")
    private void populateAddress(CustomerAddress address, LinearLayout layout) {
        ((TextView) layout.findViewById(R.id.tv_name)).setTypeface(null, Typeface.BOLD);
        ((TextView) layout.findViewById(R.id.tv_name)).setTextColor(Color.parseColor("#444444"));
        FormViews.setText(R.id.tv_name, TextUtils.getNullSafeString(address.getFirstName()) + " " + TextUtils.getNullSafeString(address.getLastName()), layout);
        FormViews.setText(R.id.tv_phone_number, TextUtils.getNullSafeString(AppUtils.formattedNumber(address.getPhoneNumber())), layout);
        FormViews.setText(R.id.tv_email, TextUtils.getNullSafeString(address.getEmail()), layout);
        FormViews.setTextOrHideIfEmpty(R.id.tv_street_address, TextUtils.getNullSafeString(address.getAddress1()), layout);
        FormViews.setTextOrHideIfEmpty(R.id.tv_address2, TextUtils.getNullSafeString(address.getAddress2()), layout);
        FormViews.setTextOrHideIfEmpty(R.id.tv_city, TextUtils.getNullSafeString(address.getCity()) + "," + TextUtils.getNullSafeString(address.getStateProvinceName()) + " ", layout);
        FormViews.setTextOrHideIfEmpty(R.id.tv_country, TextUtils.getNullSafeString(address.getCountryName()), layout);
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            ((TextView) layout.findViewById(R.id.tv_name)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_phone_number)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_email)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_street_address)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_address2)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_city)).setGravity(Gravity.RIGHT);
            ((TextView) layout.findViewById(R.id.tv_country)).setGravity(Gravity.RIGHT);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_cart).setVisible(false);
        menu.findItem(R.id.menu_search).setVisible(false);
        MenuItem item2 = menu.findItem(R.id.menu_invoice_share).setVisible(true);
        item2.setOnMenuItemClickListener(itemView -> {
            PDF_Sharing();
            return false;
        });
    }

    private void PDF_Sharing() {
        try {
            File filePath = new File(Environment.getExternalStorageDirectory() + File.separator + getResources().getString(R.string.kitchen_app_name_for_pdf) + File.separator);
            String fileName = filePath + "/" + orderNumber + "-" + CreateOn + ".pdf";
            File file = new File(fileName);
            if (file.exists()) {
                Uri photoUri;
                if (Build.VERSION.SDK_INT >= 24)
                    photoUri = FileProvider.getUriForFile(Objects.requireNonNull(getActivity()), "com.mart.onestopkitchen" + ".share", file);
                else
                    photoUri = Uri.fromFile(file);
                Intent share_intent = new Intent();
                share_intent.setAction(Intent.ACTION_SEND);
                share_intent.setType("application/pdf");
                share_intent.putExtra(Intent.EXTRA_STREAM, photoUri);
                share_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                share_intent.putExtra(Intent.EXTRA_SUBJECT, "1 Stop Kitchen Invoice");
                share_intent.putExtra(Intent.EXTRA_TEXT, "This is an 1 Stop Kitchen Invoice to share with you");
                startActivity(Intent.createChooser(share_intent, "1 Stop Kitchen Invoice"));
            } else {
                ProductDetailsPDFInvoiceCreater.getInstance(OrderDetails, Objects.requireNonNull(getActivity()));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        releaseObj();
//        Log.e(TAG, "--------onDestroyView()###------");
    }

    @Override
    public void onDestroy() {
        releaseObj();
        super.onDestroy();
//        Log.e(TAG, "--------onDestroy()###------");
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        Log.e(TAG, "--------onDetach()###------");
    }

    private void releaseObj() {
        if (null != OrderDetails)
            OrderDetails = null;

        if (null != customerOrderStatusAdapter) {
            customerOrderStatusAdapter.releaseObj();
            customerOrderStatusAdapter = null;
        }
    }

    @Override
    public void OnRatingClickListener(int productId) {
        if (null != getActivity()) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new RatingAndReviewFragment("UserReview", 0, productId, ""))
                    .addToBackStack(null)
                    .commit();
        }
    }


//    private void callReOrderWebService() {
//    RetroClient.getApi().getReOrder(_orderId).enqueue(new CustomCB<ReOrderResponse>(this.getView()));
//    }
//    public void onEvent(ReOrderResponse reOrderResponse) {
//        if (reOrderResponse.getStatusCode() == 200) {
//            if (getFragmentManager() != null) {
//                getFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.container, new CartFragment())
//                        .addToBackStack(null).commit();
//            }
//        }
//    }

}
