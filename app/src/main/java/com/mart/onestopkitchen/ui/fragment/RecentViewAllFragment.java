package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BaseProductModel;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.filterslection.UserSelectionResponseModel;
import com.mart.onestopkitchen.networking.BaseResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.AddtoCartResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.adapter.ProductAdapterRecent;
import com.mart.onestopkitchen.utils.AppUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RecentViewAllFragment extends Fragment implements ProductAdapterRecent.RecentProductClick {

    @BindView(R.id.imgBtn_viewtype)
    ImageButton imgBtnViewtype;
    @BindView(R.id.grid_view)
    ImageView gridView;
    @BindView(R.id.grid_view_border)
    View gridViewBorder;
    @BindView(R.id.tv_popularity)
    TextView tvPopularity;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.rl_sortby)
    RelativeLayout rlSortby;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.rl_filter)
    RelativeLayout rlFilter;
    @BindView(R.id.tv_category_name)
    TextView tvCategoryName;
    @BindView(R.id.txt_total)
    TextView txtTotal;
    @BindView(R.id.rv_view_all)
    RecyclerView rvViewAll;
    @BindView(R.id.txt_norecord)
    TextView txtNorecord;
    @BindView(R.id.bottomsheet)
    BottomSheetLayout bottomsheet;
    @BindView(R.id.rel)
    RelativeLayout rel;
    List<ProductModel> dataItems;
    private ArrayList<UserSelectionResponseModel> userSelectionResponseModelList;
    private ProductAdapterRecent productAdapterRecent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataItems = (List<ProductModel>) getArguments().getSerializable("data");

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recent_view_all, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.recently_view));
        initUi();
    }

    private void initUi() {
        productAdapterRecent = new ProductAdapterRecent(getContext(), this,getView());
        rvViewAll.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rvViewAll.setAdapter(productAdapterRecent);
        productAdapterRecent.loadData(dataItems);
        if (dataItems != null && dataItems.size() == 0) {
            txtNorecord.setVisibility(View.VISIBLE);
            txtTotal.setVisibility(View.GONE);
            rvViewAll.setVisibility(View.GONE);
        } else {
            txtNorecord.setVisibility(View.GONE);
            txtTotal.setVisibility(View.VISIBLE);
            rvViewAll.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

  /*  @Override
    public void addToCart(long id) {
        List<KeyValuePair> productAttributes = new ArrayList<>();

        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey("addtocart_" + id + ".EnteredQuantity");
        keyValuePair.setValue("1");
        productAttributes.add(keyValuePair);

        RetroClient.getApi()
                .addProductIntoCart(id, 2, productAttributes)
                .enqueue(new CustomCB<AddtoCartResponse>(rel));
    }

    @Override
    public void removeCart(long id) {
        RetroClient.getApi()
                .removeProductFromWishList(String.valueOf(id), "2")
                .enqueue(new CustomCB<BaseResponse>(rel));
    }

   */

    private void gotoProductListFragment(BaseProductModel baseProductModel) {
        ProductDetailFragment.productModel = (ProductModel) baseProductModel;
        getFragmentManager().beginTransaction()
//                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog,R.anim.fade_in_dialog,R.anim.fade_out_dialog)
                .replace(R.id.container, new ProductDetailFragment()).
                addToBackStack(null).commit();
    }


    @Override
    public void productClick(ProductModel dataItem) {
        ProductDetailFragment.productModel = dataItem;
        getFragmentManager().beginTransaction()
//                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog,R.anim.fade_in_dialog,R.anim.fade_out_dialog)
                .replace(R.id.container, new ProductDetailFragment()).
                addToBackStack(null).commit();
    }
    public void onEvent(AddtoCartResponse response) {
        if (response != null && response.isSuccess()) {
            updateList(true);
            return;
        }
         String errors = "";
        if (response.getErrorList().length > 0) {
            for (int i = 0; i < response.getErrorList().length; i++) {
                errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
            }
            showSnackBar(errors);
        }
    }

    private void updateList(boolean b) {
        String recentData = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS);
        if (!AppUtils.cutNull(recentData).isEmpty()) {
            Type listType = new TypeToken<List<ProductModel>>() {
            }.getType();
            List<ProductModel> productModels = new Gson().fromJson(recentData, listType);
            List<ProductModel> updateModel = new ArrayList<>();
            for (ProductModel model : productModels) {
                if (model.getId() == id) {
                    model.setWishList(b);
                }
                updateModel.add(model);
            }
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS, new Gson().toJson(updateModel));
            String getData = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS);
            Type type = new TypeToken<List<ProductModel>>() {
            }.getType();
            List<ProductModel> updateModels = new Gson().fromJson(getData, type);
            productAdapterRecent.loadData(updateModels);
        }

    }

    public void showSnackBar(String message) {
        Snackbar.make(Objects.requireNonNull(getView()), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void callAddWishListApi(long ids) {
        this.id=ids;
        List<KeyValuePair> productAttributes = new ArrayList<>();
        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey("addtocart_" + id + ".EnteredQuantity");
        keyValuePair.setValue("1");
        productAttributes.add(keyValuePair);
        RetroClient.getApi()
                .addProductIntoCart(id, 2, productAttributes)
                .enqueue(new CustomCB<AddtoCartResponse>(getView()));

    }
    public void onEvent(BaseResponse response) {
        if (response.getStatusCode()==200){
           updateList(false) ;
           return;
        }
        String errors = "";
        if (response.getErrorList().length > 0) {
            for (int i = 0; i < response.getErrorList().length; i++) {
                errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
            }
            showSnackBar(errors);
        }

    }
    private long id=-1;
    @Override
    public void removeWishlist(long ids) {this.id=ids;
        RetroClient.getApi()
                .removeProductFromWishList(String.valueOf(id), "2")
                .enqueue(new CustomCB<BaseResponse>(getView() ));
    }
}