package com.mart.onestopkitchen.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.listener.InternetConnection;
import com.mart.onestopkitchen.loginuser.loginnewmodel.DeviceInfo;
import com.mart.onestopkitchen.service.PreferenceService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfflineActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.btn_tryagain)
    Button offlineButton;

    @BindView(R.id.id_relative_layout)
    RelativeLayout relative_layout;

    DeviceInfo deviceInfo;
    Animation animBlink;
    private InternetConnection connection;
    private BroadcastReceiver mNetworkReceiver;

    public static void passInterObject() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);
        ButterKnife.bind(this);
        //  deviceInfo = new DeviceInformation().getDeviceInfo();
        // load the animation
        animBlink = AnimationUtils.loadAnimation(this, R.anim.anim_refresh_blink);
        offlineButton = (Button) findViewById(R.id.btn_tryagain);
        offlineButton.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }


    @Override
    public void onClick(View v) {

        int resourceId = v.getId();
        if (resourceId == R.id.btn_tryagain) {
            checkinInternet();
        }
    }

    private void checkinInternet() {
        if (!isNetworkAvailable(this)) {
            relative_layout.startAnimation(animBlink);
        } else {
            PreferenceService.getInstance().setInternetChecking(MyApplication.getAppContext(), false);
            finish();
        }
    }


    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


}
