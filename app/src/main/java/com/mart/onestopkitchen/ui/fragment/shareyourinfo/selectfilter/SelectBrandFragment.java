package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.FragmentActivityResult;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.ShareYourInfoFragment;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.BrandsItem;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.SelectedSizes;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")

public class SelectBrandFragment extends Fragment implements SelectBrandAdapter.OnBrandClick {


    @BindView(R.id.img_search)
    ImageView imgSearch;

    @BindView(R.id.txt_no_record)
    TextView txtNoRecord;
    @BindView(R.id.rel_search)
    RelativeLayout relSearch;
    @BindView(R.id.rv_brandes)
    RecyclerView rvBrandes;
    Unbinder unbinder;
    @BindView(R.id.edt_search_view)
    EditText edtSearchView;
    @BindView(R.id.img_search_close)
    ImageView imgSearchClose;

    private MainActivity activity;
    private SelectBrandAdapter selectBrandAdapter;
    private FragmentActivityResult activityResult;
    private List<BrandsItem> list;

    public SelectBrandFragment(FragmentActivityResult activityResult) {
        this.activityResult = activityResult;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_brand, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intUi();

    }

    private void intRv() {
        selectBrandAdapter = new SelectBrandAdapter(this);
        rvBrandes.setLayoutManager(new LinearLayoutManager(getContext()));
        rvBrandes.setAdapter(selectBrandAdapter);
        this.list = getDataToList();
        selectBrandAdapter.loadData(getDataToList());
    }

    private List<BrandsItem> getDataToList() {
        SelectedSizes response = new Gson().fromJson(getDataString(), SelectedSizes.class);
        return response.getBrands();
    }


    private void intUi() {
        intRv();
        activity = (MainActivity) getActivity();
        uiListener();
    }

    private void uiListener() {
        edtSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String searchText = editable.toString();
                if (!AppUtils.cutNull(searchText).isEmpty()) {
                    List<BrandsItem> items = new ArrayList<>();
                    for (BrandsItem brandsItem : list) {
                        if (brandsItem.getName().toLowerCase().contains(searchText.toLowerCase())) {
                            items.add(brandsItem);
                        }
                    }
                    if (items.size() > 0)
                        selectBrandAdapter.loadData(items);
                    else noRecordFound(true);
                } else {
                    selectBrandAdapter.loadData(list);
                    noRecordFound(false);
                }
            }
        });
    }

    private void noRecordFound(boolean b) {
        if (b) {
            txtNoRecord.setVisibility(View.VISIBLE);
            rvBrandes.setVisibility(View.GONE);
        } else {
            txtNoRecord.setVisibility(View.GONE);
            rvBrandes.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private String getDataString() {
        return "{\n" +
                "  \"Brands\": [\n" +
                "    {\n" +
                "      \"Name\": \"20 dreess\",\n" +
                "      \"TailoringSizes\": [\n" +
                "        {\n" +
                "      \"size\": \"S\",\n" +
                "      \"Data\":[{\n" +
                "        \"Type\":\"Lenth\",\n" +
                "        \"Value\":\"30\"},\n" +
                "        {\n" +
                "        \"Type\":\"Width\",\n" +
                "        \"Value\":\"30\"},\n" +
                "        {\n" +
                "        \"Type\":\"Shoulder\",\n" +
                "        \"Value\":\"30\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Length\",\n" +
                "        \"Value\":\"30\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Opening\",\n" +
                "        \"Value\":\"30\"},\n" +
                "        {\n" +
                "        \"Type\":\"Armhole\",\n" +
                "        \"Value\":\"30\"}\n" +
                "        ,{\n" +
                "        \"Type\":\"Bottom\",\n" +
                "        \"Value\":\"30\"}\n" +
                "    ]   \n" +
                "        },\n" +
                "        {\n" +
                "          \"size\": \"M\",\n" +
                "      \"Data\":[{\n" +
                "        \"Type\":\"Lenth\",\n" +
                "        \"Value\":\"40\"},\n" +
                "        {\n" +
                "        \"Type\":\"Width\",\n" +
                "        \"Value\":\"40\"},\n" +
                "        {\n" +
                "        \"Type\":\"Shoulder\",\n" +
                "        \"Value\":\"40\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Length\",\n" +
                "        \"Value\":\"40\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Opening\",\n" +
                "        \"Value\":\"40\"},\n" +
                "        {\n" +
                "        \"Type\":\"Armhole\",\n" +
                "        \"Value\":\"40\"}\n" +
                "        ,{\n" +
                "        \"Type\":\"Bottom\",\n" +
                "        \"Value\":\"40\"}\n" +
                "   \n" +
                "        ]   \n" +
                "        },\n" +
                "        {\n" +
                "          \"size\": \"L\",\n" +
                "        \"Data\":[{\n" +
                "        \"Type\":\"Lenth\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Width\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Shoulder\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Length\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Opening\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Armhole\",\n" +
                "        \"Value\":\"50\"}\n" +
                "        ,{\n" +
                "        \"Type\":\"Bottom\",\n" +
                "        \"Value\":\"50\"}\n" +
                "   \n" +
                "        ]   \n" +
                "        },\n" +
                "        {\n" +
                "          \"size\": \"XL\",\n" +
                "         \"Data\":[{\n" +
                "        \"Type\":\"Lenth\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Width\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Shoulder\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Length\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Opening\",\n" +
                "        \"Value\":\"50\"},\n" +
                "        {\n" +
                "        \"Type\":\"Armhole\",\n" +
                "        \"Value\":\"50\"}\n" +
                "        ,{\n" +
                "        \"Type\":\"Bottom\",\n" +
                "        \"Value\":\"50\"}\n" +
                "        ]   \n" +
                "        },\n" +
                "        {\n" +
                "      \"size\": \"XXL\",\n" +
                "        \"Data\":[{\n" +
                "        \"Type\":\"Lenth\",\n" +
                "        \"Value\":\"60\"},\n" +
                "        {\n" +
                "        \"Type\":\"Width\",\n" +
                "        \"Value\":\"60\"},\n" +
                "        {\n" +
                "        \"Type\":\"Shoulder\",\n" +
                "        \"Value\":\"60\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Length\",\n" +
                "        \"Value\":\"60\"},\n" +
                "        {\n" +
                "        \"Type\":\"Sleeve Opening\",\n" +
                "        \"Value\":\"60\"},\n" +
                "        {\n" +
                "        \"Type\":\"Armhole\",\n" +
                "        \"Value\":\"60\"}\n" +
                "        ,{\n" +
                "        \"Type\":\"Bottom\",\n" +
                "        \"Value\":\"60\"}\n" +
                "        \n" +
                "        \n" +
                "        ]   \n" +
                "        }\n" +
                "      ] \n" +
                "     \n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"499\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"7 color life style\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"9 rasa\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"A dress\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"Akkriti\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"Amor\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"AND\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"ANIT\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"Adidas\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"buma\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"Allen\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"Beter england\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"US POLO\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"Name\": \"Remond\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
    }

    @Override
    public void OnBrandViewClicked(final BrandsItem item) {
        activity.addFragmentWithAnimation(new ShareYourInfoFragment(), 0, 0, 0, 0, true, true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                activityResult.listSelection(item);
            }
        }, 300);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activity.hideKeyboard();
    }

    @OnClick(R.id.img_search_close)
    public void onViewClicked() {
        edtSearchView.setText("");
    }
}
