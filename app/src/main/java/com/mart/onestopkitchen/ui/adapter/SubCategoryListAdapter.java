package com.mart.onestopkitchen.ui.adapter;

import android.app.FragmentManager;
import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.ProductService;
import com.mart.onestopkitchen.model.SecondSubCategory;
import com.mart.onestopkitchen.model.SubCategory;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.fragment.ProductListFragmentFor3_8;
import com.mart.onestopkitchen.utils.Language;

import java.util.List;


public class SubCategoryListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<SubCategory> categories;
    private PreferenceService preferenceService;
    private Fragment fragment;

    public SubCategoryListAdapter(Context context, List<SubCategory> categories, PreferenceService preferenceService, Fragment fragment) {
        this.context = context;
        this.categories = categories;
        this.preferenceService = preferenceService;
        this.fragment = fragment;
    }

    @Override
    public SecondSubCategory getChild(int groupPosition, int childPosition) {
        return categories.get(groupPosition).getChildren().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        SecondSubCategory subCategory = getChild(groupPosition, childPosition);
        convertView = (LayoutInflater.from(context)).inflate(R.layout.item_expandable_list_child, parent, false);
        TextView textView_name = (TextView) convertView.findViewById(R.id.textView_name);
        textView_name.setText(subCategory.getName());
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            textView_name.setGravity(Gravity.RIGHT);
        }
        convertView.setOnClickListener(new CategoryonClicklistener(subCategory.getId(), subCategory.getName()));
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return categories.get(groupPosition).getChildren().size();
    }

    @Override
    public SubCategory getGroup(int groupPosition) {
        return categories.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return categories.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_expandable_list_group, null);
        AppCompatImageView iv_icon = (AppCompatImageView) convertView.findViewById(R.id.iv_icon);
        AppCompatImageView expandableIcon = (AppCompatImageView) convertView.findViewById(R.id.expandableIcon);
        TextView text = (TextView) convertView.findViewById(R.id.textView_name);
        SubCategory subCategory = getGroup(groupPosition);
        text.setText(subCategory.getName());
        if (getChildrenCount(groupPosition) < 1) {
            expandableIcon.setVisibility(View.INVISIBLE);
            convertView.setOnClickListener(new CategoryonClicklistener(subCategory.getId(), subCategory.getName()));
        } else {
            expandableIcon.setVisibility(View.VISIBLE);
            if (isExpanded)
                expandableIcon.setImageResource(R.drawable.ic_chevron_up);
            else
                expandableIcon.setImageResource(R.drawable.ic_chevron_down);
        }
        Glide.with(context).load(subCategory.getIconPath())
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv_icon);
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            text.setGravity(Gravity.RIGHT);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class CategoryonClicklistener implements View.OnClickListener {
        private int id;
        private String name;

        public CategoryonClicklistener(int id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public void onClick(View v) {
            ProductService.productId = id;
            ((MainActivity) context).closeDrawer();
            fragment.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment.getFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog,R.anim.fade_in_dialog,R.anim.fade_out_dialog)
                    .replace(R.id.container, ProductListFragmentFor3_8.newInstance(name, id))
                    .addToBackStack(null)
                    .commit();
        }
    }
}