package com.mart.onestopkitchen.ui.activity;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.viewpagerindicator.CirclePageIndicator;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.PictureModel;
import com.mart.onestopkitchen.ui.adapter.DetailsSliderAdapter;
import com.mart.onestopkitchen.ui.adapter.FullImagesAdapter;
import com.mart.onestopkitchen.ui.customview.TouchImageView;
import com.mart.onestopkitchen.ui.views.PinchViewPager;
import com.mart.onestopkitchen.utils.Keys;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.mart.onestopkitchen.ui.adapter.FullImagesAdapter.row_index;

/**
 * Created by Ashraful on 2/11/2016.
 */
public class FullScreenImageActivity extends MotherActivity implements FullImagesAdapter.OnImageSelectListener {

    public static int sliderPosition = 0;
    /* @BindView(R.id.view_pager)
     ExtendedViewPager extendedViewPager;*/
    public static List<PictureModel> pictureModels;
    //@BindView(R.id.view_pager_slider)
    ViewPager viewPager;
    @BindView(R.id.btn_wishlist)
    ImageButton btnWishList;
    @BindView(R.id.indicator)
    CirclePageIndicator circlePageIndicator;
    @BindView(R.id.recyclerView_image)
    RecyclerView recyclerView_image;
    @BindView(R.id.app_toolbar)
    Toolbar toolbar;
    private FullImagesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        row_index = 0;
        viewPager = (PinchViewPager) findViewById(R.id.view_pager_slider);
        showHideCart(false);
        if (getSupportActionBar() != null) {
            String title = getIntent().getStringExtra(Keys.PRODUCT_NAME);
            int position = getIntent().getIntExtra("slider", 0);
            sliderPosition = position;
            pictureModels = ((List<PictureModel>) getIntent().getExtras().getSerializable("list"));
            title = (!title.isEmpty() && title != null) ? title : "";
            getSupportActionBar().setTitle(title);
            toolbar.setNavigationIcon(R.drawable.ic_action_nav_arrow_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        setViewPager();

        onGalleryCloseBtnClicked();
        setImagesIntoRecycler(pictureModels);

    }

    private void setImagesIntoRecycler(List<PictureModel> pictureModels) {
        recyclerView_image.setItemAnimator(new DefaultItemAnimator());
        recyclerView_image.setLayoutManager(getLinearLayoutManagerHorizontal());
        adapter = new FullImagesAdapter(recyclerView_image, FullScreenImageActivity.this, pictureModels, sliderPosition, this);
        recyclerView_image.setAdapter(adapter);
        recyclerView_image.scrollToPosition(sliderPosition);
        recyclerView_image.setNestedScrollingEnabled(false);
    }

    private void onGalleryCloseBtnClicked() {
        btnWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setViewPager() {
        //int position = getIntent().getIntExtra("slider", 0);
        //sliderPosition = position;
        DetailsSliderAdapter detailsSliderAdapter = new DetailsSliderAdapter(FullScreenImageActivity.this, pictureModels);
        viewPager.setAdapter(detailsSliderAdapter);
        viewPager.setCurrentItem(sliderPosition);
        /*circlePageIndicator.setViewPager(viewPager);
        circlePageIndicator.setPageColor(ContextCompat.getColor(FullScreenImageActivity.this, R.color.textHintColor));
        circlePageIndicator.setFillColor(ContextCompat.getColor(FullScreenImageActivity.this, R.color.priceColor));*/


//        extendedViewPager.setAdapter(new TouchImageAdapter());
//        circlePageIndicator.setViewPager(extendedViewPager);
//        circlePageIndicator.setCurrentItem(sliderPosition);
//        circlePageIndicator.setPageColor(ContextCompat.getColor(this, R.color.container));
//        circlePageIndicator.setFillColor(ContextCompat.getColor(this, R.color.priceColor));
//        circlePageIndicator.setStrokeColor(getResources().getColor(R.color.appSceondaryColor));

        pageChageListener();
    }


    @Override
    public void onItemSelect(int position, PictureModel img) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void showCirclePageIndicator(boolean show) {
        if (show) {
            circlePageIndicator.setViewPager(viewPager);
            circlePageIndicator.setFillColor(ContextCompat.getColor(this, R.color.primaryDark));
            circlePageIndicator.setPageColor(ContextCompat.getColor(this, R.color.dark_gray));
            circlePageIndicator.setStrokeColor(ContextCompat.getColor(this, R.color.transparent));
//            circlePageIndicator.setPageColor(ContextCompat.getColor(FullScreenImageActivity.this, R.color.textHintColor));
//            circlePageIndicator.setFillColor(ContextCompat.getColor(FullScreenImageActivity.this, R.color.priceColor));
        }
    }

    private void pageChageListener() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                row_index = position;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class TouchImageAdapter extends PagerAdapter {

        // private static int[] images = { R.drawable.nature_1, R.drawable.nature_2, R.drawable.nature_3, R.drawable.nature_4, R.drawable.nature_5 };

        @Override
        public int getCount() {
            return pictureModels.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            TouchImageView img = new TouchImageView(container.getContext());
           /* RadioButton radioButton=new RadioButton(container.getContext());
            group.addView(radioButton);*/
            img.setMaxZoom(4f);
            Glide.with(FullScreenImageActivity.this).load(pictureModels.get(position).getImageUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(img);
            container.addView(img, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}
