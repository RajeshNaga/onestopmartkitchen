package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.AboutUsResponse;
import com.mart.onestopkitchen.ui.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ashraful on 2/11/2016.
 */
public class PrivacyPolicyFragment extends BaseFragment {

    @BindView(R.id.wv_privacy_policy)
    WebView wv_privacy_policy;

    String aboutUsText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ((BaseActivity) getActivity()).getSupportActionBar().setLogo(null);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);

        getActivity().setTitle(getString(R.string.privacy_policy));

        callWebService();

    }

    protected void callWebService() {
        RetroClient.getApi().getPrivacyPolicy()
                .enqueue(new CustomCB<AboutUsResponse>(this.getView()));
    }

    public void onEvent(AboutUsResponse aboutUsResponse) {

        aboutUsText = aboutUsResponse.getAboutUsModel().getTopicBody();

        if (aboutUsText != null) {
            wv_privacy_policy.loadDataWithBaseURL("", aboutUsText,
                    "text/html",
                    "UTF-8",
                    "");
        }
       /* else if (aboutUsResponse.getAboutUsModel().getShortDescription() != null)
        {
            wv_privacy_policy.loadDataWithBaseURL("",
                    aboutUsResponse.getAboutUsModel().getShortDescription(),
                    "text/html",
                    "UTF-8",
                    "");
        }*/

    }

}
