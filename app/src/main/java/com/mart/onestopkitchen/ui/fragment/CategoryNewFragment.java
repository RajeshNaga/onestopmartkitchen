package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.listener.DialogBtnClickListener;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;
import com.mart.onestopkitchen.model.Category;
import com.mart.onestopkitchen.model.simUtils.SimModels;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.CategoryNewResponse;
import com.mart.onestopkitchen.rxjava.ApiHelper;
import com.mart.onestopkitchen.rxjava.RxJavaUtils;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.adapter.CategoryListAdapter;
import com.mart.onestopkitchen.ui.adapter.SliderMenuAdapter;
import com.mart.onestopkitchen.ui.fragment.helpandsupport.HelpAndSupportFragment;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.Language;
import com.mart.onestopkitchen.utils.UiUtils;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

import static com.mart.onestopkitchen.utils.Language.ENGLISH;
import static com.mart.onestopkitchen.utils.Language.MYANMAR;
import static com.mart.onestopkitchen.utils.Language.MYANMAR_UNICODE;

/**
 * Created by Zayok on 9/17/2015.
 */
public class CategoryNewFragment extends BaseFragment implements CategoryListAdapter.OnItemClickUserDetails, SliderMenuAdapter.OnMenuClick, SwipeRefreshLayout.OnRefreshListener, SliderListener, MainActivity.RefreshMenuListener {
    @BindView(R.id.ll_rootLayout)
    RelativeLayout RootViewLinearLayout;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.expandList)
    RecyclerView expandableRecylerview;
    @BindView(R.id.customer_info)
    RecyclerView customerInfo;
    @BindView(R.id.txt_user_name)
    TextView txtUserName;
    @BindView(R.id.tvAppVersion)
    TextView tvAppVersionName;
    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.tvHome)
    TextView tvHome;
    @BindView(R.id.txt_eng_rel)
    RelativeLayout txtEngEel;
    @BindView(R.id.txt_burm_rel)
    RelativeLayout txtBurmRel;
    @BindView(R.id.lin_language)
    LinearLayout language;
    @BindView(R.id.tv_en_lan)
    TextView tvEng;
    @BindView(R.id.tv_bu_lan)
    TextView tvBu;
    @BindView(R.id.txt_choose_lang)
    TextView txt_choose_lang;
    @BindView(R.id.txt_choose_bur)
    TextView txt_choose_bur;
    @BindView(R.id.lin_change_lang)
    LinearLayout lin_change_lang;

    private SliderMenuAdapter menuAdapter;
    private CategoryListAdapter categoryListAdapter;
    private View rootView = null;
    private MyApplication myApplication;
    private boolean isDataLoaded = false;
    private List<Category> productList = null;

    private static boolean validateSimIdAndMsID(Context context) {
        boolean back = false;
        for (SimModels simModels : AppUtils.getAllSimInfo(context)) {
            if (simModels != null && !TextUtils.isEmpty(simModels.getSimId())) {
                if (simModels.getSimId().equalsIgnoreCase(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_SIM_ID)) || simModels.getSimId().contains(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_SIM_ID))) {
                    back = true;
                    break;
                }
            }
        }
        return back;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null)
            rootView = inflater.inflate(R.layout.category, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myApplication = (MyApplication) Objects.requireNonNull(getContext()).getApplicationContext();
        Utility.setActivity(getActivity());
        intRv();
        initRefreshLayout();
        intServerLang();
        tvAppVersionName.setVisibility(View.GONE);
        if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.MYANMAR)) {
            txtBurmRel.setBackgroundResource(R.drawable.burmese_selected);
            txtEngEel.setBackgroundResource(R.drawable.eng_un_selected);
        } else {
            txtBurmRel.setBackgroundResource(R.drawable.burmese_un_selected);
            txtEngEel.setBackgroundResource(R.drawable.eng_selected);
        }
        txtBurmRel.setOnClickListener(view12 ->
                changeLanguage(Language.MYANMAR, 2)
        );
        txtEngEel.setOnClickListener(view1 ->
                changeLanguage(ENGLISH, 1)
        );

        String preferredLanguage = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
        Typeface font;
        if (preferredLanguage.equals(ENGLISH)) {
            font = Typeface.createFromAsset(getContext().getAssets(), "fonts/zawgyione.ttf");
        } else {
            font = Typeface.createFromAsset(getContext().getAssets(), "fonts/zawgyione.ttf");
        }
        tvBu.setText(getString(R.string.myanmar_language));
        tvEng.setText(getString(R.string.english_language));
        tvEng.setTypeface(font);
        tvBu.setTypeface(font);

        lin_change_lang.setOnClickListener(v -> showLangPopUp());
    }

    private void showLangPopUp() {
        new Handler().postDelayed(this::closeNavDrawer, 200);
        Dialog dialogs = new Dialog(Objects.requireNonNull(getContext()), android.R.style.Theme_Dialog);
        Objects.requireNonNull(dialogs.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialogs.getWindow().setBackgroundDrawableResource(R.drawable.diloag_rounded_corner);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.choose_lang_dialog);
        dialogs.setCancelable(true);
        TextView txt_title = dialogs.findViewById(R.id.txt_title);
        RadioButton rd_my_eng = dialogs.findViewById(R.id.rd_my_eng);
        RadioButton rd_my_zaw = dialogs.findViewById(R.id.rd_my_zaw);
        RadioButton rb_uni = dialogs.findViewById(R.id.rb_uni);
        txt_title.setTypeface(UiUtils.getFont(getContext()));
        txt_title.setText(getString(R.string.choose_lang));
//        Button update = dialogs.findViewById(R.id.btn_up);
        String lang = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
        if (lang.equalsIgnoreCase(ENGLISH)) {
            rd_my_eng.setChecked(true);
            rb_uni.setChecked(false);
            rd_my_zaw.setChecked(false);

        } else if (lang.equalsIgnoreCase(MYANMAR_UNICODE)) {
            rb_uni.setChecked(true);
            rd_my_eng.setChecked(false);
            rd_my_zaw.setChecked(false);
        } else {
            rd_my_zaw.setChecked(true);
            rd_my_eng.setChecked(false);
            rb_uni.setChecked(false);
        }
        rd_my_eng.setOnClickListener(v -> {
            if (((CompoundButton) v).isChecked()) {
                dialogs.dismiss();
            }
        });
        rd_my_eng.setOnCheckedChangeListener(
                (buttonView, isChecked) -> {
                    if (isChecked) {
                        dialogs.dismiss();
                        rb_uni.setChecked(false);
                        rd_my_zaw.setChecked(false);
                        //PreferenceService.getInstance().SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, ENGLISH);
                        changeLanguage(ENGLISH, 1);
                    }
                }
        );
        rd_my_zaw.setOnClickListener(v -> {
            if (((CompoundButton) v).isChecked()) {
                dialogs.dismiss();
            }
        });
        rd_my_zaw.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                dialogs.dismiss();
                rd_my_eng.setChecked(false);
                rb_uni.setChecked(false);
                //PreferenceService.getInstance().SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, MYANMAR);
                changeLanguage(MYANMAR, 2);
            }
        });
        rb_uni.setOnClickListener(v -> {
            if (((CompoundButton) v).isChecked()) {
                dialogs.dismiss();
            }
        });
        rb_uni.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                dialogs.dismiss();
                rd_my_eng.setChecked(false);
                rd_my_zaw.setChecked(false);
                //  PreferenceService.getInstance().SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, MYANMAR_UNICODE);
                changeLanguage(MYANMAR_UNICODE, 3);
            }
        });
        dialogs.show();
    }

    private void intServerLang() {
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(ENGLISH)) {
                initLangFromServer(1);
            } else if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(MYANMAR_UNICODE)) {
                initLangFromServer(3);
            } else {
                initLangFromServer(2);
            }
        }
        ((MainActivity) Objects.requireNonNull(getActivity())).setListener(this);
        ((MainActivity) Objects.requireNonNull(getActivity())).setMenuListener(this);
    }

    private void initRefreshLayout() {
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        refreshLayout.setEnabled(true);
    }


    @SuppressLint("CheckResult")
    private void initLangFromServer(final int i) {
        ApiHelper
                .Companion.getDataHelper()
                .setUserLanguage(i)
                .compose(RxJavaUtils.Companion.applySchedulers())
                .subscribe((Consumer<Object>) obj -> {
                            if (i == 2)
                                setPreferredLanguage(Language.MYANMAR);
                            else if (i == 3)
                                setPreferredLanguage(Language.MYANMAR_UNICODE);
                            else setPreferredLanguage(ENGLISH);
                        },
                        throwable -> {
                            try {
                                if (!AppUtils.isReallyConnectedToInternet())
                                    Toast.makeText(getContext(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(getContext(), getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                            } catch (Exception ignored) {
                            }
                        });
    }

    private void intRv() {
        if (menuAdapter == null)
            menuAdapter = new SliderMenuAdapter(this);
        if (categoryListAdapter == null)
            categoryListAdapter = new CategoryListAdapter(this, getActivity(), this);

        customerInfo.setLayoutManager(new LinearLayoutManager(getActivity()));
        customerInfo.setNestedScrollingEnabled(false);
        expandableRecylerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        expandableRecylerview.setNestedScrollingEnabled(false);
    }

    @SuppressLint("SetTextI18n")
    public void onEvent(CategoryNewResponse response) {
        if (response.getCount() > 0) {
            Utility.setCartCounter(response.getCount());
        }
        if (productList == null)
            productList = new ArrayList<>();
        productList.clear();
        productList = response.getData();

        createCategoryList();

        preferenceService.SetPreferenceValue(PreferenceService.taxShow, response.isDisplayTaxInOrderSummary());
        preferenceService.SetPreferenceValue(PreferenceService.discuntShow, response.isShowDiscountBox());
    }

    @SuppressLint("SetTextI18n")
    private void createCategoryList() {
        categoryListAdapter.loadData(productList);
        expandableRecylerview.setAdapter(categoryListAdapter);
        if (null != productList && productList.size() > 0) {
            isDataLoaded = true;
            refreshLayout.setEnabled(false);
            menuAdapter.loadData(addUserMenus());
            customerInfo.setAdapter(menuAdapter);
            menuAdapter.notifyDataSetChanged();
            tvAppVersionName.setVisibility(View.VISIBLE);
            language.setVisibility(View.GONE);

            new Handler().postDelayed(() -> {
                String preferredLanguage = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
                Typeface font = null;
                if (preferredLanguage.equals(ENGLISH)) {
                    font = Typeface.createFromAsset(Objects.requireNonNull(getActivity()).getAssets(), "fonts/centurygothic.ttf");
                } else {
                    if (null != getActivity()) {
                        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/zawgyione.ttf");
                    }
                }
                tvAppVersionName.setText(getString(R.string.app_version_name) + " " + BuildConfig.VERSION_NAME + "  ( " + BuildConfig.FLAVOR + "-" + BuildConfig.BUILD_TYPE + " ) ");
                tvHome.setText(Objects.requireNonNull(getActivity()).getText(R.string.home));
                tvHome.setVisibility(View.VISIBLE);
                if (null != font)
                    tvHome.setTypeface(font);
            }, 120);
        }
    }

    private List<String> addUserMenus() {
        List<String> list = new ArrayList<>();
        list.add(getString(R.string.menu_item_customer_Info));
        list.add(getString(R.string.my_cart));
        list.add(getString(R.string.my_wish_list));
        list.add(getString(R.string.my_order));
        list.add(getString(R.string.help));
        if (PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE) || PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE)) {
            list.add(getString(R.string.log_out));
            ((MyApplication) Objects.requireNonNull(getActivity()).getApplicationContext()).isLogoutOptionShow = true;
        }
        return list;
    }

    //Called from LoginFragment
    public void onEvent(String verifiedUser) {
        if (verifiedUser.equals("-1") || verifiedUser.equals("1")) {    // 1 for logout enable
            createCategoryList();
        }
    }

    @Override
    public void onItemClick(int pos) {

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("CheckResult")
    private void changeLanguage(final String lang, int i) {
        if (!lang.equalsIgnoreCase(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE))) {
            ApiHelper.
                    Companion.getDataHelper().
                    setUserLanguage(i).
                    compose(RxJavaUtils.Companion.applySchedulers())
                    .subscribe((Consumer<Object>) obj ->
                                    setPreferredLanguage(lang),
                            throwable -> {
                                if (!AppUtils.isReallyConnectedToInternet())
                                    Toast.makeText(getContext(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(getContext(), getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
                            });
        }
        new Handler().postDelayed(this::closeNavDrawer, 200);
    }

    @SuppressLint("SetTextI18n")
    private void setPreferredLanguage(String lang) {
        RetroClient.getApi().getNewCategory().enqueue(new CustomCB<CategoryNewResponse>(RootViewLinearLayout));
        preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, lang);
        Locale myLocale = new Locale(lang);
        Resources res = Objects.requireNonNull(getContext()).getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        menuAdapter.loadData(null);
        categoryListAdapter.loadData(null);
        ((MainActivity) Objects.requireNonNull(getActivity())).setToolBarText();
        goToHomePage(true);
        tvHome.setVisibility(View.GONE);
        tvAppVersionName.setVisibility(View.GONE);
        language.setVisibility(View.GONE);
        txt_choose_bur.setTypeface(AppUtils.getFonts(getContext()));
        txt_choose_lang.setTypeface(AppUtils.getFonts(getContext()));
        //if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(ENGLISH)){
        txt_choose_bur.setText(getString(R.string.change_language_menu_burmese) + " (" + getString(R.string.myanmer_txt) + ")");
        //  }

        txt_choose_lang.setText(getString(R.string.change_language_menu_new) + " (" + getString(R.string.english_txt) + ")");
        if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.MYANMAR)) {
            txtBurmRel.setBackgroundResource(R.drawable.burmese_selected);
            txtEngEel.setBackgroundResource(R.drawable.eng_un_selected);
        } else {
            txtBurmRel.setBackgroundResource(R.drawable.burmese_un_selected);
            txtEngEel.setBackgroundResource(R.drawable.eng_selected);
        }
    }

    private void nextFragment(@NonNull Fragment fragment) {
        closeNavDrawer();
        if (fragment instanceof HelpAndSupportFragment) {
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, fragment).addToBackStack(null).commit();
            closeNavDrawer();
            return;
        }
        if (fragment instanceof WishListFragment) {
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, fragment).addToBackStack(null).commit();
            closeNavDrawer();
            return;
        }

        if (fragment instanceof CartFragment) {
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, fragment).addToBackStack(null).commit();
            closeNavDrawer();
            return;
        }

        if (!validateSimIdAndMsID(getContext())) {
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.USER_PROFILE, "");
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.USER_SIM_ID, "");
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.USER_MOBILE_NUMBER, "");
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.VERIFIED_DONE, false);
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.REGISTER_DONE, false);
        }

        if (myApplication.fromMenuSelection.equalsIgnoreCase(Keys.CUS_ORDER) && preferenceService.GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE)) {
            fragment = new CustomerOrdersFragment();
        } else if (!preferenceService.GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE)) {
            fragment = new LoginFragment();
        }
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                .replace(R.id.container, fragment).addToBackStack(null).commit();
    }


    @Override
    public void onViewClicked(int pos) {
        Fragment currentFragment = Objects.requireNonNull(getActivity()).getSupportFragmentManager().findFragmentById(R.id.container);
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            switch (pos) {
                case 0:
                    if (currentFragment instanceof CustomerInfoFragment) {
                        closeNavDrawer();
                        return;
                    } else {
                        myApplication.fromMenuSelection = Keys.MY_ACCOUNT;
                        nextFragment(new CustomerInfoFragment());
                    }
                    break;

                case 1:
                    if (currentFragment instanceof CartFragment) {
                        closeNavDrawer();
                        return;
                    } else {
                        myApplication.fromMenuSelection = "";
                        nextFragment(new CartFragment());
                    }
                    break;

                case 2:

                    if (currentFragment instanceof WishListFragment) {
                        closeNavDrawer();
                        return;
                    } else {
                        myApplication.fromMenuSelection = Keys.WISH_LIST;
                        nextFragment(new WishListFragment());
                    }
                    break;

                case 3:
                    if (currentFragment instanceof CustomerOrdersFragment) {
                        closeNavDrawer();
                        return;
                    } else {
                        myApplication.fromMenuSelection = Keys.CUS_ORDER;
                        nextFragment(new CustomerOrdersFragment()/*CustomerOrdersFragment.getInstance()*/);
                    }
                    break;

                case 4:
                    if (currentFragment instanceof HelpAndSupportFragment) {
                        closeNavDrawer();
                        return;
                    } else {
                        myApplication.fromMenuSelection = "";
                        nextFragment(new HelpAndSupportFragment());
                    }
                    break;

                case 5:
                    closeNavDrawer();
                    new Handler().postDelayed(this::logoutDialog, 180);
                    break;

                case 6:
                    changeLanguage(Language.MYANMAR, 2);
                    break;

                case 7:
                    changeLanguage(ENGLISH, 1);
                    break;
            }
        }
    }

    private void logoutDialog() {
        AppDialogs.customDialogWithCancel(getActivity(), getResources().getString(R.string.are_you_logout), new DialogBtnClickListener() {
            @Override
            public void yesBtnClick(Dialog dialog) {
                logoutFun();
                dialog.dismiss();

                changeLanguage(Language.MYANMAR, 2);
            }

            @Override
            public void cancelBtnClick() {
            }
        });
    }

    private void logoutFun() {
        if (getFragmentManager() != null && !(getFragmentManager().findFragmentById(R.id.container) instanceof HomePageFragment)) {
            goToHomePage(false);
            PreferenceService.getInstance().clearAppPreference(getActivity());
            NetworkUtil.setToken("");
        } else {
            PreferenceService.getInstance().clearAppPreference(getActivity());
            NetworkUtil.setToken("");
        }
        txtUserName.setText(getString(R.string.kitchen_app_name));
        ((MyApplication) Objects.requireNonNull(getActivity()).getApplicationContext()).isLogoutOptionShow = false;
    }

    @Override
    public void onResume() {
        if (preferenceService.GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE)) {
            LoginResponseModel loginResponseModel = new Gson().fromJson(preferenceService.GetPreferenceValue(PreferenceService.USER_PROFILE), LoginResponseModel.class);
            if (loginResponseModel != null && loginResponseModel.getData().getUserName() != null) {
                txtUserName.setText(loginResponseModel.getData().getFirstName());
                if (null != loginResponseModel.getData().getFirstName() && !loginResponseModel.getData().getFirstName().isEmpty())
                    PreferenceService.getInstance().SetPreferenceValue(PreferenceService.USER_NAME_TXT, loginResponseModel.getData().getFirstName());

            } else txtUserName.setText(getString(R.string.kitchen_app_name));
        }
        super.onResume();
        imgLogo.setOnClickListener(view -> goToHomePage(false));
        tvHome.setOnClickListener(view -> goToHomePage(false));
    }

    private void goToHomePage(boolean langSelected) {
        if (getFragmentManager() != null) {
            if (langSelected) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new HomePageFragment()).commit();
                return;
            }
            if (!(getFragmentManager().findFragmentById(R.id.container) instanceof HomePageFragment)) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new HomePageFragment()).commit();
            }
        }
        new Handler().postDelayed(() -> ((MainActivity) Objects.requireNonNull(getActivity())).closeDrawer(), 200);
    }

    @Override
    public void onRefresh() {
        refreshLayout.setEnabled(false);
        refreshLayout.setRefreshing(false);
        if (!isDataLoaded) {
            refreshLayout.setEnabled(true);
            refreshLayout.setRefreshing(true);
            intServerLang();
            menuAdapter.loadData(null);
            categoryListAdapter.loadData(null);
        }
    }

    @Override
    public void setUserName() {
        onResume();
    }

    private void closeNavDrawer() {
        if (null != getActivity())
            ((MainActivity) getActivity()).closeDrawer();
    }

    @Override
    public void setRefresh() {
        if (!isDataLoaded && new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            String language = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
            RetroClient.getApi().getNewCategory().enqueue(new CustomCB<CategoryNewResponse>(RootViewLinearLayout));
            preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, language);
            Locale myLocale = new Locale(language);
            Resources res = Objects.requireNonNull(getContext()).getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
            menuAdapter.loadData(null);
            categoryListAdapter.loadData(null);
            ((MainActivity) Objects.requireNonNull(getActivity())).setToolBarText();
            goToHomePage(true);
            tvHome.setVisibility(View.GONE);
            tvAppVersionName.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        freeObj();
        Log.e("CategoryNewFragment-:", "----------  onDestroy()--------------");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void freeObj() {
        if (null != menuAdapter) {
            menuAdapter.onReleaseObj();
            menuAdapter = null;
        }
        if (null != categoryListAdapter) {
            categoryListAdapter.releaseObj();
            categoryListAdapter = null;
        }
        if (null != productList) {
            productList = null;
        }

    }


}