package com.mart.onestopkitchen.ui.activity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.BounceInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.chat.utils.ChatHelper;
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.mapper.BaseActivityMapper;
import com.mart.onestopkitchen.model.LoginResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.fragment.CartFragment;
import com.mart.onestopkitchen.ui.fragment.LoginFragment;
import com.mart.onestopkitchen.ui.fragment.Utility;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Language;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;

import static com.mart.onestopkitchen.chat.utils.ErrorUtilsKt.showSnackbar;


@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();
    public ActionBarDrawerToggle mDrawerToggle = null;
    protected PreferenceService preferenceService = PreferenceService.getInstance();
    private TextView ui_hot = null;
    private boolean isMenuShow = true;
    private BaseActivityMapper mapper = null;
    private ProgressDialog progressDialog = null;

//    protected ActionBar actionBar;


    public BaseActivityMapper getMapper() {
        if (mapper == null)
            mapper = new BaseActivityMapper();
        mapper.withActivity(this);
        return mapper;
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivityName();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        getMapper();
    }

    @Override
    protected void onStop() {
        System.gc();
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void onEvent(LoginResponse response) {
        if (response.getToken() != null) {
            invalidateOptionsMenu();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
        releaseObj();
    }


    public void goMenuItemFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment).addToBackStack(null).commit();

    }

    public void goMenuItemFragmentifloggedIn(Fragment fragment) {
        if (!isLoggedIn())
            fragment = new LoginFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isMenuShow) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_main, menu);
            setOrRefreshCurtMenuItem(menu);
        }
        return true;
    }

    private void setOrRefreshCurtMenuItem(@NotNull Menu menu) {
        final View menu_hotlist = menu.findItem(R.id.menu_cart).getActionView();
        ui_hot = menu_hotlist.findViewById(R.id.hotlist_hot);
        menu_hotlist.setOnClickListener(v -> {
            Utility.closeLeftDrawer();
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                if (!(getSupportFragmentManager().findFragmentById(R.id.container) instanceof CartFragment))
                    goMenuItemFragment(new CartFragment());
            }
        });
        updateHotCount(Utility.cartCounter);
    }


    @SuppressLint("SetTextI18n")
    public void updateHotCount(final int badgeCount) {
        if (ui_hot == null) return;
        runOnUiThread(() -> {
            if (badgeCount == 0)
                ui_hot.setVisibility(View.INVISIBLE);
            else {
                ui_hot.setVisibility(View.VISIBLE);
                ui_hot.setText(Long.toString(badgeCount));
            }
        });
    }

    public void animate() {
        ObjectAnimator anim = ObjectAnimator.ofFloat(ui_hot, "ScaleY", 0, 1);
        anim.setInterpolator(new BounceInterpolator());
        anim.setDuration(1000);
        anim.start();
    }

    /* @Override
     public boolean onPrepareOptionsMenu(Menu menu) {
         super.onPrepareOptionsMenu(menu);
         MenuItem menuCart = menu.findItem(R.id.menu_cart);
         return true;
     }

     @Override
     public boolean onOptionsItemSelected(MenuItem item) {

         if (mDrawerToggle.onOptionsItemSelected(item)) {
             return true;
         }
         int resource = item.getItemId();
         Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
         if (resource == R.id.menu_cart) {
             if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                 goMenuItemFragmentifloggedIn(new CartFragment());
                 goMenuItemFragment(new CartFragment());
                 return true;
             }
         }
         return super.onOptionsItemSelected(item);
     }
 */
    public void makeActionOverflowMenuShown() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            menuKeyField.setAccessible(true);
            menuKeyField.setBoolean(config, false);
        } catch (Exception e) {
            Log.d("", e.getLocalizedMessage());
        }
    }

    public boolean isLoggedIn() {
        return preferenceService.GetPreferenceBooleanValue(PreferenceService.LOGGED_PREFER_KEY);
    }

    public void setLocale(boolean recreate) {
        String preferredLanguage = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
        //boolean shouldRecreate = getActivityContentView() != null;
        if (TextUtils.isEmpty(preferredLanguage)) {
            preferredLanguage = Language.MYANMAR;
            preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, preferredLanguage);
        }
        Locale preferredLocale = new Locale(preferredLanguage);
        Locale.setDefault(preferredLocale);
        Configuration configuration = new Configuration();
        configuration.locale = preferredLocale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
        if (recreate) {
            recreate();
        }
    }


    private void currentActivityName() {
        try {
            ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> tasks = null;
            if (activityManager != null) {
                tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
            }
            String actvityName = null;
            if (tasks != null) {
                actvityName = tasks.get(0).topActivity.getClassName();
            }
            Log.d(TAG, "currentActivity:" + actvityName);
        } catch (Exception e) {
            Log.d(TAG, "currentActivity:" + e.getMessage());
        }
    }

    public void showNormalToast(String msg) {
        if (msg != null && !msg.trim().isEmpty())
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void appLog(String TAG, String s) {
        AppUtils.appLog(TAG, s);
    }


    public void showHideCart(final boolean isShow) {
        isMenuShow = isShow;
        invalidateOptionsMenu();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getMapper().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getMapper().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public void showProgressDialog(@StringRes Integer messageId) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            // Disable the back button
            DialogInterface.OnKeyListener keyListener = (dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK;
            progressDialog.setOnKeyListener(keyListener);
        }
        progressDialog.setMessage(getString(messageId));
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void showErrorSnackbar(@StringRes int resId, Exception e) {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        if (rootView != null) {
            Log.e("###", "-----showErrorSnackbar -------");
            showSnackbar(rootView, resId, e, R.string.dlg_retry, null).show();
        }
    }

    public void showErrorSnackbar(@StringRes int resId, Exception e, View.OnClickListener clickListener) {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        if (rootView != null) {
            showSnackbar(rootView, resId, e, R.string.dlg_retry, clickListener);
        }
    }

    private int RESTART_DELAY = 200;

    public void restartApp(Context context) {
        // Application needs to restart when user declined some permissions at runtime
        Intent restartIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        PendingIntent intent = PendingIntent.getActivity(context, 0, restartIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC, System.currentTimeMillis() + RESTART_DELAY, intent);
        System.exit(0);
    }

    private void releaseObj() {
        if (null != mDrawerToggle) {
            mDrawerToggle = null;
        }
        if (null != progressDialog) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (null != preferenceService) {
            preferenceService = null;
        }
        if (null != mapper) {
            mapper = null;
        }
        if (null != ui_hot) {
            ui_hot = null;
        }

        System.gc();
    }

    public void initDefaultActionBar(View view) {
        Toolbar toolbar = view.findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportFragmentManager()) {
            String currentUserFullName = SharedPrefsHelper.getInstance().getQbUser().getFullName();
            toolbar.setTitle(currentUserFullName);
            toolbar.setNavigationIcon(R.drawable.ic_action_nav_arrow_back);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
                hideKeyboard();
            });
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        hideNotifications();
        if (SharedPrefsHelper.getInstance().hasQbUser() && !QBChatService.getInstance().isLoggedIn()) {
            ChatHelper.getInstance().loginToChat(SharedPrefsHelper.getInstance().getQbUser(), new QBEntityCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid, Bundle bundle) {
//                    Log.e(TAG, "###### onResume onSuccess ### ---------: ");
                    onResumeFinished();
                }

                @Override
                public void onError(QBResponseException e) {
                    onResumeFinished();
                    if (e.getErrors() != null && e.getErrors().size() > 0)
                        showNormalToast(e.getErrors().get(0));
//                    Log.e(TAG, "###### onResume onError ### ---------: " + e.getErrors());
                }
            });
        } else {
            onResumeFinished();
        }
    }

//    private void hideNotifications() {
//        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//        if (notificationManager != null) {
//            notificationManager.cancelAll();
//        }
//    }

    public void onResumeFinished() {
        // Need to Override onResumeFinished() method in nested classes if we need to handle returning from background in Activity
//        Log.e(TAG, "######  onResumeFinished ### ---------: ");
    }

//   public QBUser getUserDetailss() {
//        QBUser qbUser = new QBUser();
//        String userName = "", fullname = "";
//
//        if (null != PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME)
//                && !PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME).isEmpty()) {
//
//            userName = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME);
//
//            if (null != PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT) && !PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT).isEmpty()) {
//                fullname = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT);
//            } else {
//                fullname = "OneStopMart";
//            }
//        } else {
//            userName = NetworkUtil.getDeviceId();
//        }
//
//        if (fullname.isEmpty()) {
//            fullname = userName;
//        }
//
//        StringifyArrayList tags = new StringifyArrayList<>();
//        tags.add("User");
//        qbUser.setLogin(userName);
//        qbUser.setFullName(fullname);
//        qbUser.setPassword(USER_DEFAULT_PASSWORD);
//        qbUser.setTags(tags);
//
//        return qbUser;
//    }


    public String getAppVersionName(Context context) {
        String version;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return BuildConfig.VERSION_NAME;
        }
        return version;
    }
}
