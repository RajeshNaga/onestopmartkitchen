package com.mart.onestopkitchen.ui.fragment.addaddres;

import android.content.Context;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.baseview.ResponseHandler;
import com.mart.onestopkitchen.networking.ApiBroker;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomerAddressPresenter implements ResponseHandler {

    private static final String TAG = CustomerAddressPresenter.class.getSimpleName();
    private CustomerAddressListener listener;
    private Context mContext;
    private ApiBroker apiBroker;

    public CustomerAddressPresenter(CustomerAddressListener listener) {
        this.listener = listener;
        this.mContext = listener.getActivityContext();
        apiBroker = new ApiBroker(this);
    }

    /**
     * 1 =>getAddress
     */
    public void apiCall(int whichApi) {
        showDialog(true);
        switch (whichApi) {
            case 1:
                apiBroker.getAvailableAddress(whichApi);
                break;

            case 2:
                apiBroker.addAddress(whichApi, getAddressData());
                break;
        }
    }

    private List<KeyValuePairNew> getAddressData() {

        List<KeyValuePairNew> keyValuePairs = new ArrayList<>();
        keyValuePairs.add(new KeyValuePairNew("value", 1));
        keyValuePairs.add(new KeyValuePairNew("Address.StateProvinceId", listener.stateId()));
        keyValuePairs.add(new KeyValuePairNew("Address.FirstName", listener.userName()));
        keyValuePairs.add(new KeyValuePairNew("Address.LastName", ""));
        keyValuePairs.add(new KeyValuePairNew("Address.Email", ""));
        keyValuePairs.add(new KeyValuePairNew("Address.Company", ""));
        keyValuePairs.add(new KeyValuePairNew("Address.City", listener.cityId()));
        keyValuePairs.add(new KeyValuePairNew("Address.Address1", listener.addressOne()));
        keyValuePairs.add(new KeyValuePairNew("Address.Address2", listener.addressTwo()));
        keyValuePairs.add(new KeyValuePairNew("Address.ZipPostalCode", "1"));
        keyValuePairs.add(new KeyValuePairNew("Address.PhoneNumber", "1"));
        keyValuePairs.add(new KeyValuePairNew("Address.FaxNumber", "1"));
        return keyValuePairs;
    }

    private void failureAction(String s) {
        showDialog(false);
        if (listener != null)
            listener.showError(s);

    }

    private void showDialog(boolean isShow) {
        if (listener != null)
            if (isShow)
                listener.showLoading();
            else listener.hideLoading();
    }

    /**
     * 1 =>getAddress
     */
    @Override
    public void onSuccess(Object o, int api) {
        showDialog(false);
        try {
            switch (api) {
                case 1:
                    getAddress(o);
                    break;
                case 2:
                    addAddress(o);
                    break;
            }
        } catch (Exception e) {
            log(TAG, e.getMessage());
            failureAction(mContext.getString(R.string.some_thing_went));
        }
    }

    private void addAddress(Object o) {

    }

    private void getAddress(Object o) {
        try {
            BillingAddressResponse response = (BillingAddressResponse) o;
            if (listener != null)
                if (response != null && response.getNewAddress() != null)
                    listener.successFromServerGetAddress(response);
                else failureAction(mContext.getString(R.string.some_thing_went));
        } catch (Exception e) {
            log(TAG, e.getMessage());
            failureAction(mContext.getString(R.string.some_thing_went));
        }

    }

    @Override
    public void onErrorBody(Object s, int api) {

    }

    @Override
    public void onFailure(String s) {
        failureAction(s);
    }

    private void log(String TAG, String msg) {
        AppUtils.appLog(TAG, msg);
    }
}
