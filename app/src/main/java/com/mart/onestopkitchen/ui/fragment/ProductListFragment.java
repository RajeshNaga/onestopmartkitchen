package com.mart.onestopkitchen.ui.fragment;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.constants.ProductSort;
import com.mart.onestopkitchen.model.AvailableSortOption;
import com.mart.onestopkitchen.model.Category;
import com.mart.onestopkitchen.model.CategoryFeaturedProductAndSubcategoryResponse;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.ProductService;
import com.mart.onestopkitchen.model.ViewType;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.ProductsResponse;
import com.mart.onestopkitchen.ui.adapter.ProductListAdapter;
import com.mart.onestopkitchen.ui.adapter.SubCategoryAdapter;
import com.mart.onestopkitchen.ui.views.DrawerManipulationFromFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bs156 on 23-Feb-17.
 * This filter is for nopCommerce version 3.7 and earlier
 */

public class ProductListFragment extends BaseFragment {

    @BindView(R.id.list_product)
    RecyclerView listProduct;
    @BindView(R.id.rl_rootLayout)
    RelativeLayout rootViewRelativeLayout;
    @BindView(R.id.tv_category_name)
    TextView categoryNameTextView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.bottomsheet)
    BottomSheetLayout bottomSheetLayout;

    protected int categoryId = 0;
    protected boolean resetList = true;
    private static final String CATEGORY_NAME = "categoryName";
    private static final String CATEGORY_ID = "categoryId";
    public Map<String, String> queryMapping = new HashMap<>();
    protected int pageNumber = 1;
    protected ProductListAdapter productAdapter = null;
    private FilterFragment filterFragment = null;
    private View rootView = null;
    private GridLayoutManager layoutManager = null;
    private List<AvailableSortOption> availableSortOptions = new ArrayList<>(0);
    private int selectedPosition = -1;
    //private ProductSortAdapter sortAdapter;
    private ArrayAdapter<String> sortAdapter;

    private int newSpanCount = 2;
    private int itemViewType = ViewType.GRID;

    public static ProductListFragment newInstance(String categoryName, int categoryId) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putString(CATEGORY_NAME, categoryName);
        args.putInt(CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_product_list_custom, container, false);
        } else {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null) {
                parent.removeView(rootView);
            }
        }
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_product_list_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_filter) {
            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawers();
            } else {
                drawerLayout.openDrawer(GravityCompat.END);
            }

            return true;
        } else if (item.getItemId() == R.id.menu_item_sort) {
            showSortByView();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void drawerSetup() {
        DrawerManipulationFromFragment drawerManipulationFromFragment =
                new DrawerManipulationFromFragment(drawerLayout);
        drawerManipulationFromFragment.DrawerSetup(this);
        filterFragment = new FilterFragment();
        getChildFragmentManager().beginTransaction()
//                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog,R.anim.fade_in_dialog,R.anim.fade_out_dialog)
                .replace(R.id.drawer_filter, filterFragment).commit();
    }

    protected void showSortByView() {
        if (availableSortOptions != null) {
            LinearLayout sortLinearLayout = (LinearLayout) getLayoutInflater().
                    inflate(R.layout.list_sort_by, bottomSheetLayout, false);
            ListView sortListView = sortLinearLayout.findViewById(R.id.lv_sortby);
            bottomSheetLayout.showWithSheetView(sortLinearLayout);
            sortListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            sortAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                    R.layout.simple_list_item_single_choice, ProductSort.getSortOptionTextList(availableSortOptions));

//            sortAdapter = new ProductSortAdapter(getActivity(), ProductSort.getSortOptionTextList(availableSortOptions));

            sortListView.setAdapter(sortAdapter);
            if (selectedPosition >= 0) {
                sortListView.setItemChecked(selectedPosition, true);
            }

            sortListView.setOnItemClickListener((parent, view, position, id) -> {
                String value = availableSortOptions.get(position).getValue();
                selectedPosition = position;
                queryMapping.put("orderBy", value);
                callWebService();
                bottomSheetLayout.dismissSheet();

            });
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        checkEventBusRegistration();
        calculateAutomaticGridColumn();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            categoryId = bundle.getInt(CATEGORY_ID, categoryId);
            String categoryName = bundle.getString(CATEGORY_NAME, "");
            categoryNameTextView.setText(categoryName);
        }

        if (productAdapter == null) {
            callWebService();
            drawerSetup();
        }

        callSubCategoryList();
    }

    private void calculateAutomaticGridColumn() {
        layoutManager = new GridLayoutManager(getActivity(), 2);
        listProduct.setLayoutManager(layoutManager);
        listProduct.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        listProduct.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int viewWidth = listProduct.getMeasuredWidth();
                        float cardViewWidth = Objects.requireNonNull(getActivity()).getResources().getDimension(R.dimen.cardviewWidth);

                        newSpanCount = (int) Math.floor(viewWidth / cardViewWidth);
                        if (itemViewType == ViewType.GRID) {
                            updateColumnPerRow(newSpanCount);
                        }
                    }
                });
    }

    private void updateColumnPerRow(int spanCount) {
        layoutManager.setSpanCount(spanCount);
        layoutManager.requestLayout();
    }

    public void callWebService() {
        if (productAdapter != null) {
            productAdapter.resetList();
        }
        resetList = true;
        pageNumber = 1;
        getQueryMap();
        RetroClient.getApi().getProductList(categoryId, queryMapping)
                .enqueue(new CustomCB<ProductsResponse>(rootViewRelativeLayout));
    }

    private void callWebServiceMoreLoad(int pageNumber) {
        this.pageNumber = pageNumber;
        resetList = false;
        getQueryMap();
        RetroClient.getApi().getProductList(categoryId, queryMapping)
                .enqueue(new CustomCB<ProductsResponse>());
    }

    private void callSubCategoryList() {
        RetroClient.getApi().getCategoryFeaturedProductAndSubcategory(categoryId)
                .enqueue(new CustomCB<CategoryFeaturedProductAndSubcategoryResponse>());
    }

    public void getQueryMap() {
        queryMapping.put(Api.qs_page_number, "" + pageNumber);
    }

    public void onEvent(ProductsResponse response) {
        if (response != null && response.getStatusCode() == 200) {

            categoryNameTextView.setText(response.getName());

            if (response.getAvailableSortOptions() != null && productAdapter == null) {
                availableSortOptions.clear();
                availableSortOptions.addAll(response.getAvailableSortOptions());
            }

            List<ProductModel> responseProducts = response.getProducts();
            if (responseProducts != null && responseProducts.size() > 0) {
                if (productAdapter == null) {

                    if (response.getPriceRange() != null) {
                        filterFragment.setPriceFilter(response.getPriceRange());
                    }

                    listProduct.setLayoutManager(layoutManager);
                    productAdapter = new ProductListAdapter(getContext(), responseProducts, listProduct, response.getTotalPages());
                    listProduct.setAdapter(productAdapter);

                    productAdapter.setOnLoadMoreListener(currentPage -> {
                        callWebServiceMoreLoad(currentPage);
                        productAdapter.showLoader();
                        listProduct.post(() -> productAdapter.notifyItemInserted(productAdapter.getItemCount() - 1));
                    });

                    productAdapter.setOnProductClickListener(new ProductListAdapter.OnProductClickListener() {
                        @Override
                        public void onProductClick(View view, ProductModel product) {
                            ProductDetailFragment.productModel = product;
                            if (getFragmentManager() != null) {
                                getFragmentManager().beginTransaction().replace(R.id.container, new ProductDetailFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                    });
                } else {
                    productAdapter.setLoaded();
                    if (resetList) {
                        resetList = false;
                        productAdapter.addMoreProducts(responseProducts);
                        productAdapter.notifyDataSetChanged();
                    } else {
                        productAdapter.hideLoader();

                        int start = productAdapter.getItemCount();
                        int size = responseProducts.size();
                        productAdapter.addMoreProducts(responseProducts);
                        productAdapter.notifyItemRangeChanged(start, size);
                    }
                }
            } else if (responseProducts != null && responseProducts.size() == 0 && productAdapter != null && productAdapter.getItemCount() > 0) {
                productAdapter.setLoaded();
                productAdapter.hideLoader();
            }

            filterFragment.clearAllSpecificationItem();

            if (response.getNotFilteredItems() != null && response.getNotFilteredItems().size() > 0) {
                filterFragment.generateFilterView(response.getNotFilteredItems());
            }

            if (response.getAlreadyFilteredItems() != null && response.getAlreadyFilteredItems().size() > 0) {
                filterFragment.generateAlreadyFilteredView(response.getAlreadyFilteredItems());
            }
        } else if (productAdapter != null && productAdapter.getItemCount() > 0) {
            productAdapter.setLoaded();
            productAdapter.hideLoader();
        }
    }

    public void onEvent(final CategoryFeaturedProductAndSubcategoryResponse response) {
        if (response != null
                && response.getStatusCode() == 200
                && response.getSubCategories() != null
                && response.getSubCategories().size() > 0) {

            categoryNameTextView.setVisibility(View.VISIBLE);

            final ListPopupWindow subcategoryPopupWindow = new ListPopupWindow(Objects.requireNonNull(getContext()));
            subcategoryPopupWindow.setAdapter(new SubCategoryAdapter(getActivity(), response.getSubCategories()));
            subcategoryPopupWindow.setAnchorView(categoryNameTextView);
            subcategoryPopupWindow.setModal(true);
            subcategoryPopupWindow.setBackgroundDrawable(new ColorDrawable(0));

            categoryNameTextView.setOnClickListener(v -> {
                if (subcategoryPopupWindow.isShowing()) {
                    subcategoryPopupWindow.dismiss();
                } else {
                    subcategoryPopupWindow.show();
                }
            });

            subcategoryPopupWindow.setOnItemClickListener((parent, view, position, id) -> {
                ProductService.productId = response.getSubCategories().get(position).getId();
                gotoSubCategory(response.getSubCategories().get(position));
                subcategoryPopupWindow.dismiss();
                categoryNameTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_down_light, 0);
            });
        }
    }

    private void gotoSubCategory(Category category) {
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, ProductListFragment.newInstance(category.getName(), category.getId()))
                    .addToBackStack(null).commit();
        }
    }

    @Override
    public void onDestroyView() {
        releaseObj();
        super.onDestroyView();
    }

    private void releaseObj() {
        if (null != productAdapter) {
            productAdapter = null;
        }
        if (null != availableSortOptions) {
            availableSortOptions = null;
        }
        if (null != sortAdapter) {
            sortAdapter = null;
        }
        if (null != queryMapping) {
            queryMapping = null;
        }
        if (null != rootView) {
            rootView = null;
        }
        if (null != filterFragment) {
            filterFragment = null;
        }
        if (null != layoutManager) {
            layoutManager = null;
        }
    }
}
