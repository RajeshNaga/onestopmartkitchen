package com.mart.onestopkitchen.ui.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.os.ConfigurationCompat;
import androidx.core.os.LocaleListCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.contacts.ContactsService;
import com.mart.onestopkitchen.permission.PermissionListener;
import com.mart.onestopkitchen.permission.PermissionUtils;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.Language;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

import static com.mart.onestopkitchen.service.PreferenceService.CURRENT_DATE_UPLOAD_CONTACT;
import static com.mart.onestopkitchen.utils.Language.MYANMAR;
import static com.mart.onestopkitchen.utils.Language.MYANMAR_UNICODE;

/**
 * Created on 2/17/2016.
 */


@SuppressLint("StaticFieldLeak")
public class SplashScreenActivity extends BaseActivity implements AppDialogs.AlertDialogOkClick, PermissionListener {
    @BindView(R.id.txt_to_use_app)
    TextView txtToUseApp;
    @BindView(R.id.lin_allow_gif)
    LinearLayout linAllowGif;
    @BindView(R.id.rel_permission)
    RelativeLayout relPermission;
    @BindView(R.id.rel_permission_ui)
    RelativeLayout relPermissionUi;
    @BindView(R.id.rel_spl_loading)
    RelativeLayout relSplLoading;
    @BindView(R.id.txt_to_get_started)
    TextView txtToGetStarted;
    @BindView(R.id.gif)
    GifImageView gif;

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdp = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String TAG = SplashScreenActivity.class.getCanonicalName();
    private boolean isPermissionGranted = false;
    private PermissionUtils permissionUtils = null;
    private Timer timer = null;
    private long SPLASH_TIME = 2800;
    public static SplashScreenActivity self;
    public final static String[] MULTI_PERMISSIONS = new String[]{
            PermissionUtils.Companion.getREAD_PHONE_STATE(),
            PermissionUtils.Companion.getGET_ACCOUNTS(), PermissionUtils.Companion.getACCESS_FINE_LOCATION(),
            PermissionUtils.Companion.getREAD_CONTACTS(), PermissionUtils.Companion.getREAD_EXTERNAL_STORAGE(),
            PermissionUtils.Companion.getWRITE_EXTERNAL_STORAGE(), PermissionUtils.Companion.getCAMERA(), PermissionUtils.Companion.getRECORD_AUDIO()};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        setDefaultLang();
        self = this;
        iniUi();
        AppUtils.setScreenSize(this);
        PreferenceService.getInstance().setInternetChecking(MyApplication.getAppContext(), false);
        permissionUtils = PermissionUtils.Companion.getInstance().with(this).setListener(this);
        FirebaseAnalytics.getInstance(this);
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    private void setDefaultLang() {
        if (AppUtils.cutNull(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE)).isEmpty()) {
            setMyanmmerLanguage();
        } else if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ENGLISH)) {
            setLang(Language.ENGLISH);
        } else if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(MYANMAR_UNICODE)) {
            setLang(MYANMAR_UNICODE);
        } else {
            setLang(MYANMAR);
        }
    }

    private void setMyanmmerLanguage() {
        String langTag;
        try {
            LocaleListCompat list = ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration());
            if (!AppUtils.cutNull(list).isEmpty() && !AppUtils.cutNull(list.toLanguageTags()).isEmpty() && list.toLanguageTags().length() > 2 && list.size() > 0) {
                if (list.toLanguageTags().toLowerCase().contains("zg") && list.toLanguageTags().toLowerCase().contains("mm"))
                    langTag = MYANMAR_UNICODE;
                else if (list.toLanguageTags().toLowerCase().contains("mm"))
                    langTag = MYANMAR_UNICODE;
                else
                    langTag = MYANMAR;

            } else {
                langTag = MYANMAR;
            }
        } catch (Exception e) {
            langTag = MYANMAR;
        }
        setLang(langTag);
    }

    private void setLang(String s) {
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, s);
        Locale myLocale = new Locale(s);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void iniUi() {
        for (String s : MULTI_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, s) == PackageManager.PERMISSION_GRANTED) {
                isPermissionGranted = true;
            } else {
                isPermissionGranted = false;
                break;
            }
        }
        if (!isPermissionGranted) {
            relPermissionUi.setVisibility(View.VISIBLE);
            relSplLoading.setVisibility(View.GONE);
            relPermission.setVisibility(View.VISIBLE);
            linAllowGif.setVisibility(View.GONE);
        } else {
            relPermissionUi.setVisibility(View.GONE);
            relSplLoading.setVisibility(View.VISIBLE);
            relPermission.setVisibility(View.GONE);
            initializeData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onPermissionRequestResult(requestCode, permissions, grantResults);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 111) {
            //  iniUi();
            linAllowGif.setVisibility(View.GONE);
            relPermission.setVisibility(View.VISIBLE);
            txtToUseApp.setVisibility(View.VISIBLE);
            txtToGetStarted.setVisibility(View.VISIBLE);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initializeData() {
        if (AppUtils.getSimState(this) == 0) {
            checkSimStatus();
            relSplLoading.setVisibility(View.VISIBLE);
            ((GifDrawable) gif.getDrawable()).stop();
            gif.setVisibility(View.GONE);
            AppDialogs.showAlertFinish(this, "splash", false, "", getResources().getString(R.string.no_sim), R.drawable.app_icon, 0);
        } else {
            AppUtils.CreateNstToken();
            startThread();
            runOnUiThread(() -> gif.setVisibility(View.VISIBLE));
            Calendar calendar = AppUtils.fromUserSelectDate();
            String formattedDateNew = sdp.format(calendar.getTime());
            try {
                if (AppUtils.cutNull(PreferenceService.getInstance().GetPreferenceValue(CURRENT_DATE_UPLOAD_CONTACT)).isEmpty()
                        || (!formattedDateNew.equalsIgnoreCase(PreferenceService.getInstance().GetPreferenceValue(CURRENT_DATE_UPLOAD_CONTACT)))) {
                    if (!AppUtils.isMyServiceRunning(ContactsService.class, getApplicationContext()))
                        startService(new Intent(getApplicationContext(), ContactsService.class));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void checkSimStatus() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (AppUtils.getSimState(SplashScreenActivity.this) != 0) {
                    timer.cancel();
                    relSplLoading.setVisibility(View.VISIBLE);
                    ((GifDrawable) gif.getDrawable()).start();
                    AppDialogs.dismissAlertFinishDialog();
                    initializeData();
                    Looper.loop();
                }
            }
        }, 0, 3000);
    }

    private void startThread() {
        if (Looper.myLooper() == null)
            Looper.prepare();

        ((GifDrawable) gif.getDrawable()).start();
        final Handler handler = new Handler();
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                handler.postDelayed(() -> {
                    ((GifDrawable) gif.getDrawable()).stop();
                    startMainActivity();
                }, SPLASH_TIME);
            }
        }, 0);
    }


    private void startMainActivity() {
        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
        if (getIntent().getExtras() != null) {
            i.putExtras(getIntent().getExtras());
        }
        startActivity(i);
        finish();
    }

    @SuppressLint("HardwareIds")
    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(PreferenceService.REGISTRATION_COMPLETE));

        String deviceID = Settings.Secure.getString(SplashScreenActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d(TAG, ":------- DeviceId-------: " + deviceID);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void clickOk(int which) {
        onBackPressed();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked() {
        new Handler().postDelayed(() -> {
            txtToUseApp.setVisibility(View.GONE);
            txtToGetStarted.setVisibility(View.GONE);
            linAllowGif.setVisibility(View.VISIBLE);
        }, 500);
        permissionUtils.requestPermission(MULTI_PERMISSIONS);
    }

    @Override
    public void allPermissionGranted() {
        iniUi();
        if (null != relPermission) {
            relPermission.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNeverAskAgainPermission(@NotNull String[] string) {
        AppUtils.goToSettings(this);
    }

    @Override
    public void onDenied(@NotNull String[] string) {
        if (null != permissionUtils)
            permissionUtils.requestPermission(MULTI_PERMISSIONS);
    }

    @Override
    protected void onDestroy() {
        releaseObj();
        super.onDestroy();
    }

    private void releaseObj() {
        if (timer != null) {
            timer.cancel();
        }
        if (null != permissionUtils) {
            permissionUtils = null;
        }
        if (null != mRegistrationBroadcastReceiver) {
            mRegistrationBroadcastReceiver = null;
        }
        if (null != sdp) {
            sdp = null;
        }
        System.gc();
    }


   /*
   private void simCardChangedAlert() {
        ((GifDrawable) gif.getDrawable()).stop();
    }

   private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

   @SuppressLint("MissingPermission")
    private void setUserSimcards() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager sm = SubscriptionManager.from(SplashScreenActivity.this);
            List<SubscriptionInfo> sis = sm.getActiveSubscriptionInfoList();
            if (sis != null) {
                if (sis.size() == 1) {
                    SubscriptionInfo si = sis.get(0);
                    simId1 = si.getIccId();
                }
                if (sis.size() == 2) {
                    SubscriptionInfo si = sis.get(0);
                    SubscriptionInfo si2 = sis.get(1);
                    simId1 = si.getIccId();
                    simId2 = si2.getIccId();
                }//89950617110479140826
            }
        } else {
            TelephonyManager mTelephonyMgr = (TelephonyManager) MyApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephonyMgr != null) {
                simId1 = mTelephonyMgr.getSimSerialNumber() == null ? "" : mTelephonyMgr.getSimSerialNumber();
            }
        }
        //    PreferenceService.getInstance().SetPreferenceValue(SIM_ID_2,simId2);
    }*/
}
