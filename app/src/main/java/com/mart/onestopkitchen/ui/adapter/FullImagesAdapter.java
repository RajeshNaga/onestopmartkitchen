package com.mart.onestopkitchen.ui.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.PictureModel;
import com.mart.onestopkitchen.ui.activity.FullScreenImageActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FullImagesAdapter extends RecyclerView.Adapter<FullImagesAdapter.ImageHolder> {
    public static int row_index = 0;
    private Context mContext;
    private List<PictureModel> pictureModels;
    private OnImageSelectListener listener;
    private RecyclerView recyclerView;

    public FullImagesAdapter(RecyclerView recyclerView, Context mContext, List<PictureModel> pictureModels, int position, FullScreenImageActivity activity) {
        this.mContext = mContext;
        this.pictureModels = pictureModels;
        listener = activity;
        this.recyclerView = recyclerView;
        row_index = position;
    }

    @Override
    public FullImagesAdapter.ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_imageview_slider, parent, false);
        return new ImageHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final FullImagesAdapter.ImageHolder holder, final int position) {
        Glide.with(mContext)
                .load(pictureModels.get(position).getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.shopping_logo)
                .into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                listener.onItemSelect(position, pictureModels.get(position));
                notifyDataSetChanged();

            }
        });
        if (row_index == position) {
            holder.image.setBackground(mContext.getDrawable(R.drawable.blue_ractangle_border));
        } else {
            holder.image.setBackground(mContext.getDrawable(R.drawable.image_border));
        }
        if (pictureModels.size() > 1) {
            listener.showCirclePageIndicator(true);
        } else {
            listener.showCirclePageIndicator(false);
        }
        recyclerView.smoothScrollToPosition(row_index);
    }

    @Override
    public int getItemCount() {
        return pictureModels.size() > 0 ? pictureModels.size() : 0;
    }


    public interface OnImageSelectListener {
        void onItemSelect(int position, PictureModel img);

        void showCirclePageIndicator(boolean show);
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;

        public ImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
