package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.AttributeControlType;
import com.mart.onestopkitchen.model.AttributeControlValue;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductAttribute;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.PriceResponse;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hemnath on 12/5/2018.
 */
public class DynamicViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_COLOR = 1;
    private static final int TYPE_SIZE = 2;
    private Context context;
    private String key;
    private onSelectDynamicAdapterListener dynamicAdapterListener;
    private ProductAttribute productAttribute;
    private boolean isClickColor = false;
    private boolean isClick = false;
    private int selectedIndex = -1;
    private String measurementKey;
    private List<String> meaList;

    public DynamicViewAdapter(List<ProductAttribute> productAttributeList, ProductAttribute productAttribute, Context context, onSelectDynamicAdapterListener dynamicAdapterListener) {
        this.context = context;
        this.dynamicAdapterListener = dynamicAdapterListener;
        key = getKey(productAttribute);
        measurementKey = getMeasurementKey(productAttribute);
        meaList = new ArrayList<>();
        // this.productAttributeList = productAttributeList;
        this.productAttribute = productAttribute;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_COLOR) {
            View v = LayoutInflater.from(context).inflate(R.layout.color_linear_layout, viewGroup, false);
            return new ColorViewHolder(v);

        } else {
            View v = LayoutInflater.from(context).inflate(R.layout.size_linear_layout, viewGroup, false);
            return new OtherViewHolder(v);
        }

    }

    @Override
    public int getItemViewType(int position) {

        if (productAttribute.getAttributeControlType() == AttributeControlType.ColorSquares) {
            return TYPE_COLOR;

        } else if (productAttribute.getAttributeControlType() == AttributeControlType.RadioList || productAttribute.getAttributeControlType() == AttributeControlType.DropdownList
                || productAttribute.getAttributeControlType() == AttributeControlType.ReadonlyCheckboxes) {
            return TYPE_SIZE;
        } else {
            return -1;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int i) {

        switch (holder.getItemViewType()) {
            case TYPE_COLOR:
                initLayoutColorSquare((ColorViewHolder) holder, i);
                break;
            case TYPE_SIZE:
                initLayoutOtherSquare((OtherViewHolder) holder, i);
                break;
        }


    }

    private void initLayoutColorSquare(ColorViewHolder viewHolder, int i) {

        if (productAttribute.getValues().get(i).isPreSelected() && !isClickColor) {
            selectedIndex = i;
            Map<String, String> valueTextPairMap = new HashMap<>();
            valueTextPairMap.put(String.valueOf(productAttribute.getValues().get(i).getId()), key);
            dynamicAdapterListener.getSelectedDynamicValue(valueTextPairMap);
            dynamicAdapterListener.controlSliderFromColor(productAttribute.getValues().get(i).getPictureModel().getId());
        }

        if (i == selectedIndex) {
            selectColorCircle(i, 2, viewHolder);
        } else {
            selectColorCircle(i, 12, viewHolder);
        }

        viewHolder.iv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedIndex = i;
                isClickColor = true;
                dynamicAdapterListener.controlSliderFromColor(productAttribute.getValues().get(i).getPictureModel().getId());
                Map<String, String> valueTextPairMap = new HashMap<>();
                valueTextPairMap.put(String.valueOf(productAttribute.getValues().get(i).getId()), key);
                callPriceWebservice(dynamicAdapterListener.getSelectedDynamicValue(valueTextPairMap));
                notifyDataSetChanged();
            }
        });
    }

    private void selectColorCircle(int i, int strokeWidth, ColorViewHolder colorViewHolder) {
        final GradientDrawable shapeDrawable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            shapeDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable.bg_color_square, context.getTheme());
        } else {
            shapeDrawable = (GradientDrawable) context.getResources().getDrawable(R.drawable.bg_color_square);
        }

        try {
            List<AttributeControlValue> attributeControlValues = productAttribute.getValues();
            shapeDrawable.setColor(Color.parseColor(attributeControlValues.get(i).getColorSquaresRgb()));
            shapeDrawable.setStroke(strokeWidth, Color.TRANSPARENT, 10, 10);
        } catch (Exception ex) {
            // who cares
        }
        colorViewHolder.iv_view.setBackground(shapeDrawable);
    }

    private void initLayoutOtherSquare(OtherViewHolder dynamicViewHolder, int i) {
     /*   if (productAttribute.getName().equalsIgnoreCase("size")) {
            for (String s : meaList) {
                TextView iv = new TextView(context);
                iv.setText(s);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                Log.d("insideee",meaList.toString());
                iv.setLayoutParams(lp);
                dynamicViewHolder.linearLayout.addView(iv);
            }
        }
*/

        if (productAttribute.getValues().get(i).isPreSelected() && !isClick) {
            selectedIndex = i;
            Map<String, String> valueTextPairMap = new HashMap<>();
            valueTextPairMap.put(String.valueOf(productAttribute.getValues().get(i).getId()), key);
            dynamicAdapterListener.getSelectedDynamicValue(valueTextPairMap);
        }
        dynamicViewHolder.tv_size.setText(productAttribute.getValues().get(i).getName());

        if (i == selectedIndex) {
            dynamicViewHolder.tv_size.setBackgroundResource(R.drawable.size_background_blue);
        } else {
            dynamicViewHolder.tv_size.setBackgroundResource(R.drawable.size_background);
        }

        dynamicViewHolder.itemView.setOnClickListener(view -> {
            isClick = true;
            selectedIndex = i;
            Map<String, String> valueTextPairMap = new HashMap<>();
            if (productAttribute.getName().equalsIgnoreCase("size")) {
                valueTextPairMap.put(String.valueOf(productAttribute.getValues().get(i).getId() + " "), measurementKey);
                valueTextPairMap.put(String.valueOf(productAttribute.getValues().get(i).getId()), key);
                // dynamicAdapterListener.expandView();
            } else {
                valueTextPairMap.put(String.valueOf(productAttribute.getValues().get(i).getId()), key);
            }
            //if (!productAttribute.getName().equalsIgnoreCase("size"))
            //      notifyDataSetChanged();
            callPriceWebservice(dynamicAdapterListener.getSelectedDynamicValue(valueTextPairMap));
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return productAttribute.getValues().size();
    }

    public String getKey(ProductAttribute productAttribute) {
        String key = String.format("%s_%s_%s_%s", "product_attribute", productAttribute.getProductId()
                , productAttribute.getProductAttributeId(), productAttribute.getId());
        return key;
    }

    public void loadData(List<String> measurementList) {
        meaList.clear();
        this.meaList = measurementList;
        Log.d("vvvvv", meaList.toString());
        notifyDataSetChanged();
    }

    private void callPriceWebservice(List<KeyValuePair> valuePair) {
        RetroClient.getApi().getUpdatedPrice(ProductDetailFragment.productModel.getId(), valuePair)
                .enqueue(new CustomCB<PriceResponse>(ProductDetailFragment.self.getView()));
    }

    private String getMeasurementKey(ProductAttribute productAttribute) {
        return String.format("%s_%s_%s_%s", "product_measurement", productAttribute.getProductId()
                , productAttribute.getProductAttributeId(), productAttribute.getId());
    }

    public interface onSelectDynamicAdapterListener {
        List<KeyValuePair> getSelectedDynamicValue(Map<String, String> pairValue);

        void controlSliderFromColor(int imageId);

        void expandView();
    }

    static class OtherViewHolder extends RecyclerView.ViewHolder {
        TextView tv_size;

        OtherViewHolder(View itemView) {
            super(itemView);
            tv_size = itemView.findViewById(R.id.tv_size_view);
        }
    }

    static class ColorViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_view;

        ColorViewHolder(View itemView) {
            super(itemView);
            iv_view = itemView.findViewById(R.id.iv_color_view);
        }

    }

}
