package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.PictureModel;

import java.util.List;

/**
 * Created by BS148 on 11/8/2016.
 */

public class DetailsSliderAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<PictureModel> pictureModels;

    private OnSliderClickListener sliderClickListener;

    public DetailsSliderAdapter(Context context, List<PictureModel> imageUrl) {
        this.context = context;
        this.pictureModels = imageUrl;
    }

    @Override
    public int getCount() {
        return pictureModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (LinearLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int pos) {
        final int position = pos;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_viewpager_slider, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.layout);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);


        if (pictureModels.get(position).getFullSizeImageUrl().isEmpty() ? pictureModels.get(position).getImageUrl().contains(".gif") : pictureModels.get(position).getFullSizeImageUrl().contains("gif")) {
            Glide.with(context)
                    .load(pictureModels.get(position).getFullSizeImageUrl().isEmpty() ? pictureModels.get(position).getImageUrl() : pictureModels.get(position).getFullSizeImageUrl())
                    .asGif()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //   .placeholder(R.drawable.placeholder)   .error(R.drawable.placeholder)
                    .into(imageView);
        } else {
            Glide.with(context)
                    .load(pictureModels.get(position).getFullSizeImageUrl().isEmpty() ? pictureModels.get(position).getImageUrl() : pictureModels.get(position).getFullSizeImageUrl())
                    //  .placeholder(R.drawable.placeholder)
                    // .error(R.drawable.placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }

        container.addView(view);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sliderClickListener != null) {
                    sliderClickListener.onSliderClick(v, position);
                }
            }
        });
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void setOnSliderClickListener(OnSliderClickListener sliderClickListener) {
        this.sliderClickListener = sliderClickListener;
    }

    public interface OnSliderClickListener {
        void onSliderClick(View view, int position);
    }
}
