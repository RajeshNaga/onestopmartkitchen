package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.AddAddressResponse;
import com.mart.onestopkitchen.model.AvailableCity;
import com.mart.onestopkitchen.model.AvailableCountry;
import com.mart.onestopkitchen.model.AvailableState;
import com.mart.onestopkitchen.model.CustomerAddress;
import com.mart.onestopkitchen.model.EditAddressResponse;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.networking.response.CityListResponse;
import com.mart.onestopkitchen.networking.response.StateListResponse;
import com.mart.onestopkitchen.utils.TextUtils;
import com.mart.onestopkitchen.utils.UiUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by mart-110 on 12/15/2015.
 */
public class CustomerAddAddressFragment extends BaseFragment implements View.OnClickListener {
    CustomerAddress address;
    @BindView(R.id.et_first_name)
    EditText firstNameEditText;
    @BindView(R.id.et_last_name)
    EditText lastNameEditText;
    @BindView(R.id.et_email)
    EditText emailEditText;
    @BindView(R.id.et_city)
    EditText cityEditText;
    @BindView(R.id.et_address1)
    EditText address1EditText;
    @BindView(R.id.et_address2)
    EditText address2EditText;
    @BindView(R.id.et_zip_code)
    EditText zipOrPostalCodeEditText;
    @BindView(R.id.et_fax_number)
    EditText faxNumberEditText;
    @BindView(R.id.et_phone_number)
    EditText phoneNumberEditText;
    @BindView(R.id.et_company)
    EditText companyEditText;
    @BindView(R.id.btn_save)
    Button submitButton;

    @BindView(R.id.sp_country)
    AppCompatSpinner countrySpinner;
    @BindView(R.id.spinner_state)
    AppCompatSpinner stateSpinner;

    @BindView(R.id.spinner_city)
    AppCompatSpinner citySpinner;

    String countryCode;
    String StateProvinceCode = "";
    String CityCode = "";
    HashMap<String, String> addressHashMap;
    BillingAddressResponse billingAddressResponse;
    List<KeyValuePair> keyValuePairs;
    private boolean isEdit = false;
    private int addressIndex;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_add_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        getActivity().setTitle(getString(R.string.add_address));
        callBillingAddressApi();

        addressHashMap = new HashMap();

        Bundle args = getArguments();
        if (args != null) {
            isEdit = true;
            getActivity().setTitle(getString(R.string.edit_address));
            addressIndex = args.getInt("index");
            String addressJson = args.getString("addressJson");

            address = new Gson().fromJson(addressJson, CustomerAddress.class);

            firstNameEditText.setText(TextUtils.getNullSafeString(address.getFirstName()));
            lastNameEditText.setText(TextUtils.getNullSafeString(address.getLastName()));
            emailEditText.setText(TextUtils.getNullSafeString(address.getEmail()));
            companyEditText.setText(TextUtils.getNullSafeString(address.getCompany()));
            cityEditText.setText(TextUtils.getNullSafeString(address.getCity()));
            address1EditText.setText(TextUtils.getNullSafeString(address.getAddress1()));
            address2EditText.setText(TextUtils.getNullSafeString(address.getAddress2()));
            phoneNumberEditText.setText(TextUtils.getNullSafeString(address.getPhoneNumber()));
            zipOrPostalCodeEditText.setText(TextUtils.getNullSafeString(address.getZipPostalCode()));
            faxNumberEditText.setText(TextUtils.getNullSafeString(address.getFaxNumber()));
        }
        emptyInitializationSpinner();
        submitButton.setOnClickListener(this);


    }

    public void onEvent(EditAddressResponse response) {
        submitButton.setEnabled(true);
        if (response.getStatusCode() == 400) {
            String errors = getString(R.string.error_adding_address) + "\n";
            if (response.getErrorList().length > 0) {
                for (int i = 0; i < response.getErrorList().length; i++) {
                    errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
                }
                Toast.makeText(getActivity(), errors, Toast.LENGTH_LONG).show();
            }
        } else if (response.getStatusCode() == 200) {
            Toast.makeText(getActivity(), R.string.address_update_succss, Toast.LENGTH_SHORT).show();
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container, new CustomerAddressesFragment()).
                    addToBackStack(null).commit();
        }
    }

    public void onEvent(AddAddressResponse response) {
        if (response.getStatusCode() == 400) {
            String errors = getString(R.string.error_adding_address) + "\n";
            if (response.getErrorList().length > 0) {
                for (int i = 0; i < response.getErrorList().length; i++) {
                    errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
                }
                Toast.makeText(getActivity(), errors, Toast.LENGTH_LONG).show();
            }
        } else if (response.getStatusCode() == 200) {
            Toast.makeText(getActivity(), R.string.address_added_success, Toast.LENGTH_SHORT).show();

           /* getFragmentManager().beginTransaction().replace(R.id.container, new CustomerAddressesFragment()).
                    addToBackStack(null).commit();*/
            getFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog,R.anim.fade_in_dialog,R.anim.fade_out_dialog)
                    .replace(R.id.container, new CartFragment()).
                    addToBackStack(null).commit();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_save) {
            addOrUpdateAddress();
            //submitButton.setEnabled(false);
            UiUtils.hideSoftKeyboard(getActivity());
        }
    }

    private void addOrUpdateAddress() {
        if (validateForm()) {
            if (isEdit) {
                callEditAddressesWebservice();
            } else {
                callAddAddressWebservice();
            }
        }
    }

    private void callAddAddressWebservice() {
        RetroClient.getApi().addAddress(keyValuePairs).enqueue(new CustomCB<AddAddressResponse>(this.getView()));
    }

    private void callEditAddressesWebservice() {
        RetroClient.getApi().editAddress(Integer.parseInt(address.getId()), keyValuePairs)
                .enqueue(new CustomCB<EditAddressResponse>(this.getView()));
    }


    private String getString(EditText et) {
        return et.getText().toString();
    }

    private void callBillingAddressApi() {
        RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
    }

    public void onEvent(BillingAddressResponse billingAddressResponse) {
        this.billingAddressResponse = billingAddressResponse;
        List<String> CountryListName = getCountryList(billingAddressResponse.getNewAddress().getAvailableCountries());
        populateDatainCountrySpinner(CountryListName, billingAddressResponse.getNewAddress().getAvailableCountries());
    }


    private void populateDatainCountrySpinner(List<String> CountryListName, final List<AvailableCountry> availableCountries) {

        setAdapter(countrySpinner, CountryListName);
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    StateProvinceCode = "";
                    countryCode = availableCountries.get(position).getValue();
                    RetroClient.getApi().getStates(countryCode).enqueue(new CustomCB<StateListResponse>());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (isEdit && address.getCountryId() != null) {
            int i = 0;
            for (AvailableCountry aCountry : availableCountries) {
                if (aCountry.getValue().equalsIgnoreCase(address.getCountryId())) {
                    countrySpinner.setSelection(i);
                    break;
                }
                i++;
            }
        }

    }

    private void setAdapter(AppCompatSpinner spinner, List<String> stringList) {
//        com.mart.onestopkitchen.ui.adapter.SpinnerAdapter adapter=new com.mart.onestopkitchen.ui.adapter.SpinnerAdapter(getActivity(), R.layout.simple_spinner_item_black_color,stringList);
        ArrayAdapter categorySpinnerAdapter = new ArrayAdapter<>(getContext(), R.layout.simple_spinner_item_black_color, stringList);
        categorySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(categorySpinnerAdapter);
    }

    private List<String> getCountryList(List<AvailableCountry> availableCountries) {
        List<String> CountryListName = new ArrayList<>();
        for (AvailableCountry country : availableCountries)
            CountryListName.add(country.getText());
        return CountryListName;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem your_icon = menu.findItem(R.id.add);
    }

    public void onEvent(final StateListResponse stateListResponse) {
        setAdapter(stateSpinner, getStateList(stateListResponse.getData()));
        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    StateProvinceCode = "" + stateListResponse.getData().get(position - 1).getId();
                    //Log.d("test", stateListResponse.getData().get(position-1).getId()+"");
                    RetroClient.getApi().getCityList(StateProvinceCode).enqueue(new CustomCB<CityListResponse>());
                } else {
                    StateProvinceCode = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (isEdit && address.getStateProvinceId() != null) {
            int stateId = Integer.parseInt(address.getStateProvinceId());
            //stateSpinner.setSelection(3);
            for (int i = 0; i <= stateListResponse.getData().size(); i++) {
                //Log.d("test", stateListResponse.getData().get(i).getId() + " " +stateId);
                if (stateListResponse.getData().get(i).getId() == stateId) {
                    stateSpinner.setSelection(i + 1);
                    break;
                }
            }
        }
    }

    public void onEvent(final CityListResponse cityListResponse) {
        setAdapter(citySpinner, getCityList(cityListResponse.getData()));
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    CityCode = "" + cityListResponse.getData().get(position - 1).getId();
                    //Log.d("test", stateListResponse.getData().get(position-1).getId()+"");
//                    RetroClient.getApi().getCityList(CityCode).enqueue(new CustomCB<CityListResponse>());
                } else {
                    CityCode = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (isEdit && address.getCityId() != null) {
            int stateId = Integer.parseInt(address.getCityId());
            //stateSpinner.setSelection(3);
            for (int i = 0; i <= cityListResponse.getData().size(); i++) {
                //Log.d("test", stateListResponse.getData().get(i).getId() + " " +stateId);
                if (cityListResponse.getData().get(i).getId() == stateId) {
                    citySpinner.setSelection(i + 1);
                    break;
                }
            }
        }
    }

    public List<String> getCityList(List<AvailableCity> citis) {
        List<String> cityList = new ArrayList<>();
        cityList.add(getString(R.string.select_city));
        for (AvailableCity availableState : citis)
            cityList.add(availableState.getName());
        return cityList;
    }

    public List<String> getStateList(List<AvailableState> states) {
        List<String> stateList = new ArrayList<>();
        stateList.add(getString(R.string.select_state));
//        if(states.size() == 1 && states.get(0).getId() ==0){
//            StateProvinceCode="0";
//        } else {
//            StateProvinceCode="";
//        }
        for (AvailableState availableState : states)
            stateList.add(availableState.getName());
        return stateList;
    }

    private boolean validateForm() {
        boolean isValid = true;
        if (countryCode == null || countryCode.isEmpty()) {
            Snackbar.make(getView(), getString(R.string.select_a_country), Snackbar.LENGTH_SHORT).show();
            isValid = false;
        } else if (StateProvinceCode == null || StateProvinceCode.isEmpty()) {
            Snackbar.make(getView(), getString(R.string.select_state), Snackbar.LENGTH_SHORT).show();
            isValid = false;
        } else if (CityCode == null || CityCode.isEmpty()) {
            Snackbar.make(getView(), getString(R.string.select_city), Snackbar.LENGTH_SHORT).show();
            isValid = false;
        }
        if (isValid) {
            keyValuePairs = new ArrayList<>();
            keyValuePairs.add(new KeyValuePair("Address.CountryId", countryCode));
            keyValuePairs.add(new KeyValuePair("Address.StateProvinceId", StateProvinceCode));
            keyValuePairs.add(new KeyValuePair("Address.CityId", CityCode));
            keyValuePairs.add(new KeyValuePair("Address.FirstName", getString(firstNameEditText)));
            keyValuePairs.add(new KeyValuePair("Address.LastName", getString(lastNameEditText)));
            keyValuePairs.add(new KeyValuePair("Address.Email", getString(emailEditText)));
            keyValuePairs.add(new KeyValuePair("Address.Company", getString(companyEditText)));
            keyValuePairs.add(new KeyValuePair("Address.Address1", getString(address1EditText)));
            keyValuePairs.add(new KeyValuePair("Address.Address2", getString(address2EditText)));
            keyValuePairs.add(new KeyValuePair("Address.ZipPostalCode", "000000"));
            keyValuePairs.add(new KeyValuePair("Address.PhoneNumber", getString(phoneNumberEditText)));
            keyValuePairs.add(new KeyValuePair("Address.FaxNumber", getString(faxNumberEditText)));

        }
        return isValid;
    }

    private void emptyInitializationSpinner() {
        setEmptyAdapter(countrySpinner, getString(R.string.select_state));
        setEmptyAdapter(stateSpinner, getString(R.string.select_state));
        setEmptyAdapter(citySpinner, getString(R.string.select_city));
    }

    private void setEmptyAdapter(Spinner spinner, String hint) {
        List<String> emtList = new ArrayList<>();
        emtList.add(hint);
        ArrayAdapter empAdapter = new ArrayAdapter<>(getContext(), R.layout.simple_spinner_item_black_color, emtList);
        empAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(empAdapter);
    }

}
