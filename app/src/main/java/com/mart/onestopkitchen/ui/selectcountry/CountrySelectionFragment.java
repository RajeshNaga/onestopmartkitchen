package com.mart.onestopkitchen.ui.selectcountry;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.AppConstant;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.selectcountry.model.DataItem;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.mobilevalidation.GetCacheData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class CountrySelectionFragment extends Fragment implements AdapterListener {

    @BindView(R.id.tool_common)
    Toolbar toolCommon;
    @BindView(R.id.edt_search_country)
    EditText edtSearchCountry;
    @BindView(R.id.rel_country_list)
    RecyclerView relCountryList;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_no_record)
    TextView txtNoRecord;
    int position = 0;
    String countryCode = "", countryName = "", MCC = "", MNC = "", networkName = "", operatorName = "", status = "", countryFlagCode = "";
    int simMCC, simMNC, imageIndex = -1;
    int countryImageId = -1;
    private List<DataItem> items;
    private CountryListSelectionAdapter adapter;
    private List<DataItem> searchItem = new ArrayList<>();
    private GetCacheData cacheMapper;
    private ArrayList<DataItem> CountryDataItem;
    private MainActivity mainActivity;
    private CountryListener countryListener;
    private List<DataItem> dataItems;

    public CountrySelectionFragment(CountryListener listener) {
        this.countryListener = listener;
    }
   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_selection);
        ButterKnife.bind(this);
        initUi();

    }*/

    @Override
    public void onStop() {
        super.onStop();
        hideKeyboard();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_country_selection, container, false);
        ButterKnife.bind(this, rootView);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.login));
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUi();
    }

    private void initUi() {
        mainActivity = (MainActivity) getActivity();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainActivity.showKeyboard();
            }
        }, 100);
        mainActivity.hideToolBar(true);
        uiListener();
        //loadCountryList();
        edtSearchCountry.requestFocus();
        cacheMapper = new GetCacheData();
        adapter = new CountryListSelectionAdapter(this);
        relCountryList.setLayoutManager(new LinearLayoutManager(getActivity()));
        relCountryList.setAdapter(adapter);
        txtNoRecord.setVisibility(View.GONE);
        adapter.loadData(loadJSONFromAsset());
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mainActivity.hideToolBar(false);
    }

    private void uiListener() {
        toolCommon.setNavigationIcon(this.getResources().getDrawable(R.drawable.ic_action_nav_arrow_back));
        txtTitle.setText(getString(R.string.select_country));
        toolCommon.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        edtSearchCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String data = AppUtils.cutNull(charSequence.toString().trim());
                if (data.length() > 0) {
                    searchRecord(data);
                } else adapter.loadData(loadJSONFromAsset());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void searchRecord(String data) {
        //need to change loadJson file into local list
        searchItem.clear();
        for (DataItem dataItem0 : loadJSONFromAsset()) {
            if (dataItem0.getCountryCode().toLowerCase().contains(data.toLowerCase()) || dataItem0.getCountryName().toLowerCase().contains(data.toLowerCase())) {
                // if(items.indexOf(dataItem0)==AppConstant.listOfFlags[items.indexOf(dataItem0)]){
                searchItem.add(dataItem0);
                // }

            }
        }
        adapter.loadData(searchItem);
        adapter.notifyDataSetChanged();
    }


    public void onBackPressed() {
        if (!edtSearchCountry.getText().toString().trim().isEmpty()) {
            edtSearchCountry.setText("");
        } else {
            onItemClick(null);
        }
    }

    private void hideKeyboard() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainActivity.hideKeyboard();
            }
        }, 100);
    }

    public List<DataItem> loadJSONFromAsset() {
        String json = null;
        if (dataItems != null && dataItems.size() > 0)
            return dataItems;
        dataItems = new ArrayList<>();
        try {
            InputStream is = getActivity().getAssets().open("county_list.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            try {
                JSONObject resp = new JSONObject(json);
                JSONObject status = resp.getJSONObject("status");
                JSONArray data = null;
                String message = status.getString("msg");
                int status_code = status.getInt("code");
                DataItem model;
                switch (status_code) {
                    case 200:
                        if (resp.has("data")) {
                            CountryDataItem = new ArrayList<>();
                            data = resp.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                model = new DataItem();
                                JSONObject jsonObject = (JSONObject) data.get(i);
                                countryCode = jsonObject.getString("CountryCode");
                                countryName = jsonObject.getString("CountryName");
                                String CountryFlagCode = jsonObject.getString("CountryFlagCode");
                                String MCC = jsonObject.getString("MCC");
                                model.setLocalImg(AppConstant.listOfFlags[i]);
                                model.setCountryCode(countryCode);
                                model.setCountryName(countryName);
                                model.setCountryFlagCode(CountryFlagCode);
                                model.setMCC(MCC);
                                dataItems.add(model);
                            }
                        }
                        break;
                    default:
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } catch (IOException ex) {
            dataItems = null;
            ex.printStackTrace();
            return null;
        }
        return dataItems;
    }


    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void onItemClick(DataItem countryListModel) {
        hideKeyboard();
        // Log.i("----------CountryCode-------------",countryListModel.getCountryCode());
        //hideKeyboard();
      /*  Bundle bundle = new Bundle();
        Intent intent = new Intent();
        bundle.putSerializable("countrydata", countryListModel);
        Log.i("-----CountryCode---", countryListModel.getCountryCode());
        intent.putExtras(bundle);*/
        countryListener.onSelectedCountry(countryListModel);
        mainActivity.onBackPressed();
        //  setResult(1000, intent);
        //finish();

    }

    @Override
    public void noRecordFound(List<DataItem> listModels) {
        if (listModels != null && listModels.size() > 0)
            txtNoRecord.setVisibility(View.GONE);
        else
            txtNoRecord.setVisibility(View.VISIBLE);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
