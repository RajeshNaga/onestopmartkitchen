package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SelectedSizes {

    @SerializedName("Brands")
    private List<BrandsItem> brands;

    public List<BrandsItem> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandsItem> brands) {
        this.brands = brands;
    }
}