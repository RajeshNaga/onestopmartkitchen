package com.mart.onestopkitchen.ui.fragment.shareyourinfo;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.adapter.ClothSizeAdapter;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.adapter.EnterManualAdapter;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.model.ManuallSize;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.SelectBrandFragment;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.BrandsItem;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.DataItem;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.TailoringSizesItem;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ShareYourInfoFragment extends Fragment implements FragmentActivityResult, ClothSizeAdapter.OnSizeClicked {


    @BindView(R.id.txt_header)
    TextView txtHeader;
    @BindView(R.id.txt_or)
    TextView txtOr;
    @BindView(R.id.txt_enter_manually_size)
    TextView txtEnterManuallySize;
    @BindView(R.id.txt_selete)
    TextView txtSelete;
    @BindView(R.id.rel_fav_brand_select)
    RelativeLayout relFavBrandSelect;
    @BindView(R.id.lin_selected_sizes)
    LinearLayout linSelectedSizes;
    @BindView(R.id.rv_sizes)
    RecyclerView rvSizes;
    @BindView(R.id.txt_enter_size)
    TextView txtEnterSize;
    @BindView(R.id.rv_manually_sizes)
    RecyclerView rvManuallySizes;
    @BindView(R.id.btn_search)
    TextView btnSearch;
    Unbinder unbinder;
    private MainActivity mainActivity;
    private EnterManualAdapter manualAdapter;
    private ClothSizeAdapter sizeAdapter;
    private BrandsItem selectedBrand;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_share_your_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intUi();
    }

    private void intUi() {
        linSelectedSizes.setVisibility(View.GONE);
        mainActivity = (MainActivity) getActivity();
        getActivity().setTitle(R.string.share_your_info);
        intRvs();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void intRvs() {
        manualAdapter = new EnterManualAdapter();
        rvManuallySizes.setLayoutManager(new LinearLayoutManager(getContext()));
        rvManuallySizes.setAdapter(manualAdapter);
        manualAdapter.loadDataSizes(getDefaultData(), true);
        sizeAdapter = new ClothSizeAdapter(this);
        rvSizes.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvSizes.setAdapter(sizeAdapter);
    }

    private List<ManuallSize> getDefaultData() {
        List<ManuallSize> list = new ArrayList<>();
        ManuallSize manuallSize = new ManuallSize();
        //manuallSize.setEdtValue("0");
        manuallSize.setTitle(getString(R.string.length));
        ManuallSize manuallSize1 = new ManuallSize();
        // manuallSize1.setEdtValue("0");
        manuallSize1.setTitle(getString(R.string.width));
        ManuallSize manuallSize2 = new ManuallSize();
        manuallSize2.setTitle(getString(R.string.shoulder));
        // manuallSize2.setEdtValue("0");
        ManuallSize manuallSize3 = new ManuallSize();
        manuallSize3.setTitle(getString(R.string.sleeve_length));
        // manuallSize3.setEdtValue("0");
        ManuallSize manuallSize4 = new ManuallSize();
        //   manuallSize4.setEdtValue("0");
        manuallSize4.setTitle(getString(R.string.sleeve_opening));
        ManuallSize manuallSize5 = new ManuallSize();
        //  manuallSize5.setEdtValue("0");
        manuallSize5.setTitle(getString(R.string.armhole));
        ManuallSize manuallSize6 = new ManuallSize();
        //  manuallSize6.setEdtValue("0");
        manuallSize6.setTitle(getString(R.string.bottom));
        list.add(manuallSize);
        list.add(manuallSize1);
        list.add(manuallSize2);
        list.add(manuallSize3);
        list.add(manuallSize4);
        list.add(manuallSize5);
        list.add(manuallSize6);
        return list;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_search, R.id.rel_fav_brand_select, R.id.txt_enter_manually_size})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_search:
                btnSearchAction();
                break;
            case R.id.rel_fav_brand_select:
                brandSelection();
                break;
            case R.id.txt_enter_manually_size:
                enterSizeManually();
                break;
        }
    }

    private void btnSearchAction() {
        for (int i = 0; i < manualAdapter.getItemCount(); i++) {
            EnterManualAdapter.ViewHolder viewHolder = (EnterManualAdapter.ViewHolder)
                    rvManuallySizes.findViewHolderForAdapterPosition(i);
            String editText = viewHolder.edtTitle.getText().toString();
            String titleText = viewHolder.txtTitle.getText().toString();
            Log.d("editText", titleText + "   " + editText);
        }
    }

    private void enterSizeManually() {
        enterManuallyView();
        manualAdapter.loadDataSizes(getDefaultData(), true);
        linSelectedSizes.setVisibility(View.GONE);
        txtSelete.setText(getString(R.string.my_fv_brand));
    }

    private void brandSelection() {
        SelectBrandFragment selectBrandFragment = new SelectBrandFragment(this);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, selectBrandFragment).addToBackStack(null).commit();
    }

    @Override
    public void listSelection(BrandsItem item) {
        this.selectedBrand = item;
        if (item != null && item.getTailoringSizes().size() > 0) {
            sizeAdapter.loadData(item.getTailoringSizes());
            linSelectedSizes.setVisibility(View.VISIBLE);
            txtSelete.setText(AppUtils.cutNull(item.getName()));
            txtEnterManuallySize.setVisibility(View.VISIBLE);
            txtEnterSize.setText(getString(R.string.your_size));
            txtOr.setVisibility(View.GONE);
        } else {
            enterManuallyView();
        }
    }

    private void enterManuallyView() {
        txtEnterManuallySize.setVisibility(View.GONE);
        txtOr.setVisibility(View.VISIBLE);
        txtEnterSize.setText(getString(R.string.enter_size_manually));
    }

    @Override
    public void onSizeClicked(TailoringSizesItem sizesItem) {
        List<ManuallSize> list = new ArrayList<>();
        if (sizesItem.getData() != null && sizesItem.getData().size() > 0) {
            for (DataItem sizesItem1 : sizesItem.getData()) {
                ManuallSize size = new ManuallSize();
                size.setTitle(sizesItem1.getType());
                size.setEdtValue(sizesItem1.getValue());
                list.add(size);
            }
            manualAdapter.loadDataSizes(list, false);
        }
    }


}
