package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel;

import com.google.gson.annotations.SerializedName;

public class DataItem {

    @SerializedName("Type")
    private String type;

    @SerializedName("Value")
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}