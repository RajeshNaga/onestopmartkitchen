package com.mart.onestopkitchen.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BaseProductModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Updated by Akash Garg on 23/01/2019.
 */
public class HomePageProductAdapter extends RecyclerView.Adapter<HomePageProductAdapter.ProductSummaryHolder> {
    public List<BaseProductModel> productsList;
    protected Context context;
    private OnItemClickListener mItemClickListener;

    public HomePageProductAdapter(Context context, List<? extends BaseProductModel> productsList) {
        try {
            this.productsList = new ArrayList<>();
            this.productsList.addAll(productsList);
            this.context = context;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @NonNull
    @Override
    public ProductSummaryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout;
        layout = R.layout.item_homepage_product;

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(layout, parent, false);

        return new ProductSummaryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductSummaryHolder holder, final int position) {
        BaseProductModel product = productsList.get(position);
        Glide.with(context).load(product.getDefaultPictureModel().getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter().into(holder.productImage);
        holder.productName.setText(product.getName());
        holder.productPrice.setVisibility(View.GONE);
        holder.productOldPrice.setVisibility(View.GONE);
        holder.productOldPrice.setPaintFlags(holder.productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }


    @Override
    public int getItemCount() {
        if (productsList == null)
            return 0;
        return productsList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.img_productImage)
        protected ImageView productImage;
        @BindView(R.id.tv_productPrice)
        protected TextView productPrice;
        @BindView(R.id.tv_productName)
        protected TextView productName;
        @BindView(R.id.tv_productOldPrice)
        protected TextView productOldPrice;
        @BindView(R.id.tv_offerPercentage)
        protected TextView productOfferPercentage;

        public ProductSummaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}

