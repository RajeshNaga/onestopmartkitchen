package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.constants.ProductSort;
import com.mart.onestopkitchen.model.AdvanceSearchSpinnerOption;
import com.mart.onestopkitchen.model.AvailableSortOption;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.filterslection.GroupDataItem;
import com.mart.onestopkitchen.model.filterslection.UserSelectionResponseModel;
import com.mart.onestopkitchen.model.searchkey.SearchResponseModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.FilterMainActivityNew;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.SelectedModel;
import com.mart.onestopkitchen.mvp.ui.filterupdatedui.model.selectedmodel.UserSelectedList;
import com.mart.onestopkitchen.networking.BaseResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.AddtoCartResponse;
import com.mart.onestopkitchen.networking.response.AdvanceSearchSpinnerOptionResponse;
import com.mart.onestopkitchen.networking.response.ProductsResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.adapter.SearchPopUpWindowAdapter;
import com.mart.onestopkitchen.ui.searchadapter.SearchAdapterNew;
import com.mart.onestopkitchen.utils.AppUtils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

import static com.mart.onestopkitchen.ui.fragment.ProductListFragmentFor3_8.CATEGORY_ID;
import static com.mart.onestopkitchen.utils.AppConstants.FILTER_OPTION_INTENT;

/**
 * Created by bs156 on 16-Feb-17.
 */

public class SearchFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, SearchAdapterNew.OnViewClick, ViewTreeObserver.OnGlobalLayoutListener, SearchPopUpWindowAdapter.OnItemClick {
    @BindView(R.id.rv_product_list)
    RecyclerView productListRecyclerView;
    @BindView(R.id.searchView)
    LinearLayout searchView;
    /*@BindView(R.id.lin_add_view)
     LinearLayout lin_add_view;*/
    @BindView(R.id.abbBar)
    RelativeLayout abbBar;
    @BindView(R.id.txt_hide)
    TextView txtHide;
    @BindView(R.id.txt_no_record)
    TextView txtNoRecord;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.edt_search_text)
    EditText edtSearchText;
    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.grid_view)
    ImageView gridView;
    @BindView(R.id.grid_view_border)
    View gridViewBorder;
    @BindView(R.id.rel_root)
    RelativeLayout relRoot;
    @BindView(R.id.imgBtn_viewtype)
    ImageButton imgBtnViewtype;
    @BindView(R.id.tv_popularity)
    TextView tvPopularity;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.rl_sortby)
    RelativeLayout rlSortby;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.rl_filter)
    RelativeLayout rlFilter;
    @BindView(R.id.tv_category_name)
    TextView tvCategoryName;
    @BindView(R.id.bottomsheet)
    BottomSheetLayout bottomSheetLayout;
    @BindView(R.id.sub_filter)
    LinearLayout sortAndFilter;
    @BindView(R.id.rel_search_view)
    RelativeLayout relSearchView;
    @BindView(R.id.txt_show_text)
    TextView txtShowText;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.rel_search_show)
    RelativeLayout relSearchShow;
    @BindView(R.id.lin_root_rv)
    LinearLayout linRootRv;
    TextView txt_re;
    private SearchAdapterNew productAdapter;
    private ArrayAdapter categorySpinnerAdapter;
    private List<AdvanceSearchSpinnerOption> categoryList;
    private int selectedPosition = -1;
    private ArrayAdapter manufacturerSpinAdapter;
    private List<AdvanceSearchSpinnerOption> manufacturerList;
    private MainActivity mainActivity;
    private ArrayList<String> searchStr;
    private SearchPopUpWindowAdapter searchPopUpWindowAdapter;
    private List<AvailableSortOption> availableSortOptions = new ArrayList<>(0);
    private ProductsResponse productsResponse;
    private boolean isListView;
    private GridLayoutManager layoutManager;
    private Timer timer = null;
    private View view = null;
    private int alreadySortClicked = -1;
    private boolean isFilterClicked;
    private boolean sortclicked;
    private boolean isKeyBoardOpen;
    private int minPriceSel = 0, maxPriceSel = 0;
    private boolean isFilterClickSingleTime;
    private MyApplication application;
    private HashMap<String, Object> search;
    private boolean isPageApiCall;
    // private PopupWindow popupWindow;
    private int screenHeight;
    private int keypadHeight;
    private String searchData = "test";
    private TextWatcher textWatcher;
    private boolean isSearchClicked;
    private boolean editable;
    //   private RecyclerView recyclerView;
    private boolean isDrawerOpen;
    private MyApplication getApplication;
    private ViewTreeObserver observer;
    private boolean isApiCalled = false;
    private int heightContentView;

    @BindView(R.id.lin_add_view)
    RecyclerView recyclerView;

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {

        ArrayList<T> newList = new ArrayList<T>();

        // Traverse through the first list
        for (T element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }

    public void onCreate(View rootView) {
        this.view = rootView;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_products_search, container, false);
        ButterKnife.bind(this, view);
//        onCreate(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkEventBusRegistration();
        mainActivity = (MainActivity) getActivity();
        //   setupSearchCategorySpinner();
        //setupSearchManufacturerSpinner();
//        getAdvanceSearchOptions();
        observer = relRoot.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(this);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        heightContentView = displaymetrics.heightPixels;
        //  heightOfKeyboard = heightContentView / 2;
        /// heightOfKeyboard = heightOfKeyboard - 10;
        if (!MyApplication.getAppContext().clearTextCalled) {
            productAdapter = new SearchAdapterNew(this, getContext());
            layoutManager = new GridLayoutManager(getContext(), 2);
            productListRecyclerView.setLayoutManager(layoutManager);
            productListRecyclerView.setAdapter(productAdapter);
            callRecentSearchApi();
            uiListener();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    edtSearchText.setText("");
                    txtShowText.setText("");
                }
            }, 400);
            isApiCalled = true;
        }
    }

    private void callRecentSearchApi() {
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.RECENT_SEARCH_PRODUCTS, "");
        RetroClient.getApi().searchKeyWord().enqueue(new CustomCB<SearchResponseModel>(this.getView()));

    }

    public void onEvent(SearchResponseModel productsResponse) {
        isApiCalled = true;
        abbBar.setVisibility(View.VISIBLE);
        if (isAdded() && isVisible()) {
            if (productsResponse != null && productsResponse.getData() != null && productsResponse.getData().size() > 0) {
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.RECENT_SEARCH_PRODUCTS, new Gson().toJson(productsResponse.getData()));
                intPopUp(productsResponse.getData());
                searchPopUpWindowAdapter.loadData(productsResponse.getData());
                searchView.setVisibility(View.VISIBLE);
                bottomSheetLayout.setVisibility(View.GONE);
                // txt_re.setVisibility(View.VISIBLE);
                //  popupWindow.update(0,30, -20, screenHeight-keypadHeight);
            } else {
                searchView.setVisibility(View.GONE);
                bottomSheetLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    public void removePhoneKeypad() {
        hideKeyboard();
    }

    public void onKeyBoardHide(boolean isDrawerOpened) {
        this.isDrawerOpen = isDrawerOpened;
        if (isDrawerOpened) {
            observer.removeGlobalOnLayoutListener(this);
            bottomSheetLayout.dismissSheet();
            hideKeyboard();
        /*    if (popupWindow != null && popupWindow.isShowing())
                popupWindow.dismiss();
       */
            if (recyclerView != null && recyclerView.getVisibility() == View.VISIBLE)
                dismissRecyclerview();
            return;
        }
        if (sortclicked || !isKeyBoardOpen) {
            hideKeyboard();
            return;
        }
        if (edtSearchText.getText().toString().isEmpty())
            if (recyclerView != null && recyclerView.getVisibility() == View.GONE)
                recyclerViewVisisble();
        showKeyboard();
    }

    private void recyclerViewVisisble() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        editable = false;
        abbBar.setVisibility(View.VISIBLE);
        searchData = "";
        mainActivity.hideToolBar(true);

       /* if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();
       */
        if (recyclerView != null && recyclerView.getVisibility() == View.VISIBLE)
            dismissRecyclerview();

        if (MyApplication.getAppContext().clearTextCalled && !MyApplication.getAppContext().fromSearchView) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!onAct)
                        loadRecentSearch();
                }
            }, 200);
        }
    }

    public void checkEventBusRegistration() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideKeyboard();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removePhoneKeypad();
        if (null != mainActivity)
            mainActivity.hideToolBar(false);
       /* if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();*/
        if (recyclerView != null && recyclerView.getVisibility() == View.VISIBLE)
            dismissRecyclerview();

    }

    private void resetData() {
        if (null != application) {
            application.alreadySelectedPrice = "0-0";
            application.alreadySelected = null;
            application.rightValInt = 0;
            application.leftValInt = 0;
        }
    }

    private void uiListener() {

        application = (MyApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        resetData();
        txtNoRecord.setVisibility(View.VISIBLE);
        sortAndFilter.setVisibility(View.GONE);
        productListRecyclerView.setVisibility(View.GONE);
        sortAndFilter.setVisibility(View.GONE);

        searchStr = new ArrayList<>(0);
        edtSearchText.requestFocus();
        imgClose.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showKeyboard();
            }
        }, 500);

        edtSearchText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edtSearchText.setFilters(new InputFilter[]{AppUtils.removeEmoji});

        edtSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("charSequence", charSequence.toString() + "   " + edtSearchText.getText().toString());
                isFilterClicked = false;
                alreadySortClicked = -1;
                if (editable)
                    return;
                //  if (searchData.equals(charSequence.toString()))
                //     return;
                if (timer != null) {
                    timer.cancel();
                }
                textWatcher = this;
                AppUtils.editTextSpaceRemove(edtSearchText);
                if (charSequence.length() > 0) {
                    searchView.setVisibility(View.GONE);

                    if (recyclerView != null && recyclerView.getVisibility() == View.VISIBLE)
                        dismissRecyclerview();
                    /*  if (popupWindow != null && popupWindow.isShowing())
                        popupWindow.dismiss();*/

                    if (charSequence.toString().startsWith(" "))
                        imgClose.setVisibility(View.GONE);
                    else imgClose.setVisibility(View.VISIBLE);
                } else {
                    bottomSheetLayout.setVisibility(View.VISIBLE);
                    if (isApiCalled)
                        loadRecentSearch();
                    edtSearchText.requestFocus();
                    imgClose.setVisibility(View.GONE);
                    txtNoRecord.setVisibility(View.VISIBLE);
                    productListRecyclerView.setVisibility(View.GONE);
                    sortAndFilter.setVisibility(View.GONE);
                    //productAdapter.loadData(new ArrayList<>(0));
                }
            }

            @Override
            public void afterTextChanged(Editable editables) {
                if (editable)
                    return;
                // if (searchData.equals(editables.toString()))
                //   return;
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            if (editables.toString().trim().length() > 1) {
                                Objects.requireNonNull(getActivity()).runOnUiThread(() -> SearchFragment.this.searchProduct(edtSearchText.getText().toString()));
                            }

                        } catch (Exception ignored) {
                        }
                    }
                }, 1000);
            }

        });

        edtSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    removePhoneKeypad();
                    // Objects.requireNonNull(getActivity()).runOnUiThread(() -> searchProduct());
                    return true;
                }
                return false;
            }
        });
    }

    @SuppressLint("InflateParams")
    private void intPopUp(List<String> data) {

        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);

        if (fragment instanceof SearchFragment) {



          /*  if (popupWindow != null && popupWindow.isShowing())
                popupWindow.dismiss();
         */
            dismissRecyclerview();

            //   LinearLayout layout = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.search_pop_up, lin_add_view, false);

            //  LayoutInflater inflater = (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //  View layoutOfPopup = null;
            //   layoutOfPopup = inflater.inflate(R.layout.search_pop_up, null);
            //recyclerView = layout.findViewById(R.id.rv_serach);
            // txt_re = layout.findViewById(R.id.txt_re);
            searchPopUpWindowAdapter = new SearchPopUpWindowAdapter(this);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(searchPopUpWindowAdapter);
          /*  if (data.size() >= 5)
                popupWindow = new PopupWindow(layoutOfPopup, relRoot.getRootView().getWidth(), (relRoot.getRootView().getHeight() - heightOfKeyboard));
            else
                popupWindow = new PopupWindow(layoutOfPopup, relRoot.getRootView().getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);

            popupWindow.setContentView(layoutOfPopup);
            popupWindow.setOutsideTouchable(false);*/
            // lin_add_view.addView(layout);
            if (!isDrawerOpen)
                recyclerViewVisisble();

        } else {
            hideKeyboard();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
       /* if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
            dismissRecyclerview();
        }*/
    }

    private void setupSearchCategorySpinner() {
        categoryList = new ArrayList<>(0);
        categoryList.add(new AdvanceSearchSpinnerOption(0, getString(R.string.all)));
        categorySpinnerAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), R.layout.simple_spinner_item_black_color, categoryList);
        categorySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void setupSearchManufacturerSpinner() {
        manufacturerList = new ArrayList<>(0);
        manufacturerList.add(new AdvanceSearchSpinnerOption(0, getString(R.string.all)));

        manufacturerSpinAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), R.layout.simple_spinner_item_black_color, manufacturerList);
        manufacturerSpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void getAdvanceSearchOptions() {
        RetroClient.getApi().getAdvanceSearchOptions().enqueue(new CustomCB<AdvanceSearchSpinnerOptionResponse>());
    }

    public void onEvent(AdvanceSearchSpinnerOptionResponse spinnerOptionResponse) {
        if (spinnerOptionResponse != null && spinnerOptionResponse.getStatusCode() == 200) {
            if (spinnerOptionResponse.getCategories() != null && spinnerOptionResponse.getCategories().size() > 0) {
                if (categoryList == null) {
                    categoryList = new ArrayList<>(0);
                } else {
                    categoryList.clear();
                }

                categoryList.add(new AdvanceSearchSpinnerOption(0, getString(R.string.all)));
                categoryList.addAll(spinnerOptionResponse.getCategories());

                if (categorySpinnerAdapter != null) {
                    categorySpinnerAdapter.notifyDataSetChanged();
                }
            }

            if (spinnerOptionResponse.getManufacturer() != null && spinnerOptionResponse.getManufacturer().size() > 0) {
                if (manufacturerList == null) {
                    manufacturerList = new ArrayList<>(0);
                } else {
                    manufacturerList.clear();
                }

                manufacturerList.add(new AdvanceSearchSpinnerOption(0, getString(R.string.all)));
                manufacturerList.addAll(spinnerOptionResponse.getManufacturer());

                if (manufacturerSpinAdapter != null) {
                    manufacturerSpinAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void paginationApiCall(int position) {
        isPageApiCall = true;
        RetroClient.getApi().productSearch(String.valueOf(position), search).enqueue(new CustomCB<ProductsResponse>(this.getView()));
    }

    @Override
    public void noRecord(List<ProductModel> models) {
     /*   if (models == null || models.size() == 0) {
            productListRecyclerView.setVisibility(View.GONE);
            txtNoRecord.setVisibility(View.VISIBLE);
            sortAndFilter.setVisibility(View.GONE);
        }
        else {
            productListRecyclerView.setVisibility(View.VISIBLE);
            txtNoRecord.setVisibility(View.GONE);
            sortAndFilter.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void callApiForWishList(long pId, int cartTypeId) {
        List<KeyValuePair> productAttributes = new ArrayList<>();

        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey("addtocart_" + pId + ".EnteredQuantity");
        keyValuePair.setValue("1");
        productAttributes.add(keyValuePair);

        RetroClient.getApi()
                .addProductIntoCart(pId, cartTypeId, productAttributes)
                .enqueue(new CustomCB<AddtoCartResponse>(getView()));
    }

    @Override
    public void removeWishlist(long pId, int cartId) {
        RetroClient.getApi()
                .removeProductFromWishList(String.valueOf(pId), "2")
                .enqueue(new CustomCB<BaseResponse>(getView()));
    }

    private void searchProduct(String s) {

        if (recyclerView != null && recyclerView.getVisibility() == View.VISIBLE)
            return;
        Log.d("cccccccccc", s);
        productAdapter.apiPosition = 2;
        isPageApiCall = false;
        String query = AppUtils.cutNull(s).trim();
        rlFilter.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        rlSortby.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        resetData();
        edtSearchText.requestFocus();
        selectedPosition = -1;
        if (query.length() > 1) {
            this.searchData = s;
            edtSearchText.setEnabled(false);
            imgClose.setEnabled(false);
            imgClose.setClickable(false);
            search = new HashMap<>(0);
            search.put("q", query);
            RetroClient.getApi().productSearch("1", search).enqueue(new CustomCB<ProductsResponse>(this.getView()));

        } else {
            sortAndFilter.setVisibility(View.GONE);
            imgClose.setVisibility(View.GONE);
            txtNoRecord.setVisibility(View.VISIBLE);
            sortAndFilter.setVisibility(View.GONE);
            productAdapter.loadData(new ArrayList<ProductModel>());
            Toast.makeText(getContext(), R.string.search_limit, Toast.LENGTH_LONG).show();
        }
    }

    private void setRecentSearchKeyWord(String searchData) {
        String recentData = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.RECENT_SEARCH_PRODUCTS);
        if (!AppUtils.cutNull(recentData).isEmpty()) {
            Type listType = new TypeToken<List<String>>() {
            }.getType();
            List<String> productModels = new Gson().fromJson(recentData, listType);
            if (productModels != null) {
                for (String model : productModels) {
                    if (model.equalsIgnoreCase(searchData)) {
                        productModels.remove(model);
                        break;
                    }
                }
                productModels.add(0, searchData);
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.RECENT_SEARCH_PRODUCTS, new Gson().toJson(productModels));
            }
        }
    }

    public void onEvent(ProductsResponse productsResponse) {
        if (isAdded() && isVisible()) {
            bottomSheetLayout.setVisibility(View.VISIBLE);
            if (productsResponse != null) {
                edtSearchText.setEnabled(true);
                hideKeyboard();
                imgClose.setEnabled(true);
                imgClose.setClickable(true);
                relSearchView.setVisibility(View.GONE);
                relSearchShow.setVisibility(View.VISIBLE);
                txtShowText.setText(searchData);
                sortAndFilter.setVisibility(View.VISIBLE);
                this.productsResponse = productsResponse;
                availableSortOptions.clear();
                availableSortOptions.addAll(productsResponse.getAvailableSortOptions());
                List<ProductModel> responseProducts = productsResponse.getProducts();
                if (responseProducts != null && responseProducts.size() > 0) {
                    setRecentSearchKeyWord(searchData);
                    for (int i = 0; i < productsResponse.getProducts().size(); i++) {
                        productsResponse.getProducts().get(i).setDisplayOrder(i);
                    }
                    //setProductFilterData(productsResponse);
                    productAdapter.loadData(responseProducts);
                    txtNoRecord.setVisibility(View.GONE);
                    productListRecyclerView.setVisibility(View.VISIBLE);

                } else {
                    productListRecyclerView.setVisibility(View.GONE);
                    txtNoRecord.setVisibility(View.VISIBLE);
                    sortAndFilter.setVisibility(View.GONE);
                }
                //   edtSearchText.clearFocus();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(ProductModel productModel) {
        if (productModel != null) {
            MyApplication.getAppContext().isSearchFragment = true;
            removePhoneKeypad();
            abbBar.setVisibility(View.GONE);
            mainActivity.hideToolBar(false);
            ProductDetailFragment.productModel = productModel;
            //getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction().add(R.id.container, new ProductDetailFragment()).addToBackStack(null).commit();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.txt_show_text, R.id.img_back, R.id.img_close, R.id.rl_sortby, R.id.rl_filter, R.id.grid_view, R.id.img_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                if (bottomSheetLayout != null && bottomSheetLayout.isSheetShowing()) {
                    bottomSheetLayout.dismissSheet();
                }
                if (edtSearchText.getText().toString().length() > 0) {
                    edtSearchText.setEnabled(true);
                    imgClose.setEnabled(true);
                    imgClose.setClickable(true);
                    isApiCalled = true;
                    edtSearchText.setText("");
                    txtShowText.setText("");
                    sortAndFilter.setVisibility(View.GONE);
                    txtNoRecord.setVisibility(View.VISIBLE);
                    sortAndFilter.setVisibility(View.GONE);
                    relSearchShow.setVisibility(View.GONE);
                    relSearchView.setVisibility(View.VISIBLE);
                    return;
                }
                //   if (bottomSheetLayout != null && bottomSheetLayout.isSheetShowing()) {
                //      bottomSheetLayout.dismissSheet();
                // }
                editable = true;
              /*  if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                } */
                dismissRecyclerview();

                mainActivity.hideToolBar(false);
                mainActivity.onBackPressed();
                break;
            case R.id.img_close:
                if (AppUtils.cutNull(edtSearchText.getText().toString()).length() > 0) {
                    edtSearchText.setText("");
                    // showKeyboard();
                    searchData = "";
                    edtSearchText.requestFocus();
                    sortAndFilter.setVisibility(View.GONE);
                    productListRecyclerView.setVisibility(View.GONE);
                    if (bottomSheetLayout.isSheetShowing()) bottomSheetLayout.dismissSheet();
                }
                break;
            case R.id.rl_sortby:
                sortclicked = true;
                hideKeyboard();
                if (productAdapter.getItemCount() > 0)
                    sortAction();
                else
                    Toast.makeText(getContext(), getString(R.string.no_records), Toast.LENGTH_SHORT).show();
                break;
            case R.id.rl_filter:
                filterAction();
                break;
            case R.id.grid_view:
                gridAndListOption();
                break;
            case R.id.img_search:
            case R.id.txt_show_text:
                // popupWindow = null;
                recyclerView.setVisibility(View.GONE);
                relSearchShow.setVisibility(View.GONE);
                relSearchView.setVisibility(View.VISIBLE);
                /* edtSearchText.setText(searchData);*/
                edtSearchText.setSelection(edtSearchText.getText().toString().length());
                edtSearchText.requestFocus();
                if (!isKeyBoardOpen)
                    showKeyboard();
                isApiCalled = true;
                // loadRecentSearch();
                break;
        }
    }
    //     },0);

    //   }

    private void loadRecentSearch() {
        //  new Handler().postDelayed(new Runnable() {
        //       @Override
        //      public void run() {
        String s = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.RECENT_SEARCH_PRODUCTS);
        if (!AppUtils.cutNull(s).isEmpty()) {
            Type listType = new TypeToken<List<String>>() {
            }.getType();
            List<String> getDate = new Gson().fromJson(s, listType);
            SearchResponseModel searchResponseModel = new SearchResponseModel();
            if (getDate != null && getDate.size() > 12)  //12>13
                getDate = getDate.subList(0, 12);

            searchResponseModel.setData(getDate);
            onEvent(searchResponseModel);
        } else {
            callRecentSearchApi();
            Log.d("ddd", AppUtils.cutNull(s));
        }
    }

    private void gridAndListOption() {
        if (!isListView) {
            gridView.setImageResource(R.drawable.grid);
            isListView = true;
            productListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            productAdapter.notifyDataSetChanged();
        } else {
            productListRecyclerView.setLayoutManager(layoutManager);
            productAdapter.notifyDataSetChanged();
            gridView.setImageResource(R.drawable.list);
            isListView = false;
        }
    }

    private void filterAction() {
        //  Intent intent = new Intent(getContext(), FilterMainActivity.class);
        if (productsResponse != null && productsResponse.getProducts() != null && productsResponse.getProducts().size() > 0 && !isFilterClickSingleTime) {
            onAct = true;
            Intent intent = new Intent(getContext(), FilterMainActivityNew.class);
            Bundle bundle = new Bundle();
            isFilterClickSingleTime = true;
            mainActivity.hideKeyboard();
            minPriceSel = (int) Math.round(Double.parseDouble(String.valueOf(productsResponse.getPriceRange().getFrom()).replaceAll("[a-zA-Z(),]", "")));
            maxPriceSel = (int) Math.round(Double.parseDouble(String.valueOf(productsResponse.getPriceRange().getTo()).replaceAll("[a-zA-Z(),]", "")));

            bundle.putSerializable(CATEGORY_ID, productsResponse);
            intent.putExtras(bundle);
            startActivityForResult(intent, FILTER_OPTION_INTENT);
        }
    }

    private void sortAction() {
        if (availableSortOptions != null) {
            LinearLayout sortLinearLayout = (LinearLayout) getLayoutInflater().
                    inflate(R.layout.list_sort_by, bottomSheetLayout, false);
            ListView sortListView = sortLinearLayout.findViewById(R.id.lv_sortby);
            bottomSheetLayout.showWithSheetView(sortLinearLayout);
            sortListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            ArrayAdapter<String> sortAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
                    R.layout.simple_list_item_single_choice, ProductSort.getSortOptionTextList(availableSortOptions));

            sortListView.setAdapter(sortAdapter);
            if (selectedPosition >= 0) {
                sortListView.setItemChecked(selectedPosition, true);
            }

            sortListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String value = availableSortOptions.get(position).getValue();
                    selectedPosition = position;
                    alreadySortClicked = position;
                    productAdapter.loadData(doSortOption(position));
                    bottomSheetLayout.dismissSheet();
                    sortAndFilterColorChanged();
                }
            });
        }
    }

    private List<ProductModel> doSortOption(int value) {
        Log.d("TAG", "" + value);
        switch (value) {
            case 0:
                sortByPosition();
                break;
            case 1:
                sortByAtoZ(true);
                break;
            case 2:
                sortByAtoZ(false);
                break;
            case 3:
                sortByLowToHigh(true);
                break;
            case 4:
                sortByLowToHigh(false);
                break;
            case 5:
                sortByDate();
                break;
        }
        return productAdapter.mDataSet;
    }

    private void sortByDate() {
        try {
            Collections.sort(productAdapter.mDataSet, new Comparator<ProductModel>() {
                @SuppressLint("SimpleDateFormat")
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null && productModel.getProductPrice() == null)
                        return 0;
                    String sDate1 = productModel.getCreatedOn().replace("T", " ");//2018-02-19T10:35:25
                    String date2 = t1.getCreatedOn().replace("T", " ");
                    Date date1 = null;
                    Date date2s = null;
                    try {
                        date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(sDate1);
                        date2s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(date2);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (date1 != null && date2s != null) {
                        return date2s.compareTo(date1);
                    } else return 0;
                }
            });
        } catch (Exception ignored) {

        }
    }

    private void sortByLowToHigh(final boolean b) {
        try {
            Collections.sort(productAdapter.mDataSet, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null && productModel.getProductPrice() == null)
                        return 0;
                    if (b)
                        return Double.compare(productModel.getProductPrice().getPriceValue(),
                                (t1.getProductPrice().getPriceValue()));
                    else return Double.compare(t1.getProductPrice().getPriceValue(),
                            (productModel.getProductPrice().getPriceValue()));
                }
            });
        } catch (Exception ignored) {

        }
    }

    private void sortByAtoZ(final boolean b) {
        try {
            Collections.sort(productAdapter.mDataSet, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null)
                        return 0;
                    if (b)
                        return productModel.getName().compareToIgnoreCase(t1.getName());
                    else return t1.getName().compareToIgnoreCase(productModel.getName());
                }
            });
        } catch (Exception ignored) {

        }
    }

    private void sortByPosition() {
        try {
            Collections.sort(productAdapter.mDataSet, new Comparator<ProductModel>() {
                @Override
                public int compare(ProductModel productModel, ProductModel t1) {
                    if (productModel == null)
                        return 0;
                    return Integer.compare(productModel.getDisplayOrder(),
                            (t1.getDisplayOrder()));
                }
            });
        } catch (Exception ignored) {

        }
    }

    private boolean onAct = false;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case FILTER_OPTION_INTENT:

                isFilterClickSingleTime = false;
                filterApiCall(data);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onAct = false;
                    }
                }, 3000);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void filterApiCall(Intent data) {
        if (data != null) {
            String price = data.getStringExtra("price");
            Boolean isFilterReset = data.getExtras().getBoolean("resetData");
            String priceSelected[] = price.split("-");
            double minPrice = Double.parseDouble(priceSelected[0]);
            double maxPrice = Double.parseDouble(priceSelected[1]);
            int minPriceInt = (int) Math.round(minPrice);
            int maxPriceInt = (int) Math.round(maxPrice);
            UserSelectedList model = (UserSelectedList) data.getSerializableExtra("category");
            Log.d("modelmodel", new Gson().toJson(model));
            List<UserSelectionResponseModel> userSelectionResponseModelList;
            if (model.getSelectedModelList() != null && model.getSelectedModelList().size() > 0) {
                isFilterClicked = true;
                LinkedHashSet<String> cateName = new LinkedHashSet();
                LinkedHashSet<String> optionName = new LinkedHashSet();
                for (SelectedModel selectedModel : model.getSelectedModelList()) {
                    cateName.add(selectedModel.getCateName());
                }
                for (SelectedModel selectedModel : model.getSelectedModelList()) {
                    optionName.add(selectedModel.getOptionName().substring(0, 1).toUpperCase() + selectedModel.getOptionName().substring(1));
                }
                userSelectionResponseModelList = new ArrayList<>();
                for (String integer : cateName) {
                    UserSelectionResponseModel userSelectionResponseModel = new UserSelectionResponseModel();
                    userSelectionResponseModel.setId(integer);
                    List<GroupDataItem> listGroupDataItem = new ArrayList<>();
                    for (SelectedModel model1 : model.getSelectedModelList()) {
                        if (integer.equalsIgnoreCase(model1.getCateName())) {
                            GroupDataItem groupDataItem = new GroupDataItem();
                            groupDataItem.setCateName(model1.getCateName());
                            groupDataItem.setOptionName(model1.getOptionName());
                            groupDataItem.setFilterId(model1.getFilterId());
                            groupDataItem.setProductId(model1.getProductId());
                            listGroupDataItem.add(groupDataItem);
                        }
                    }
                    userSelectionResponseModel.setGroupData(listGroupDataItem);
                    userSelectionResponseModelList.add(userSelectionResponseModel);
                }
                Log.d("userSelection", new Gson().toJson(userSelectionResponseModelList));

            } else {
                userSelectionResponseModelList = new ArrayList<>(0);
                isFilterClicked = maxPriceSel != maxPriceInt || minPriceSel != minPriceInt;
            }
            List<ProductModel> selectedItems = new ArrayList<>(0);
            LinkedHashSet<Integer> pId = new LinkedHashSet<>();
            ArrayList<Integer> pIdMulSelection = new ArrayList<>();
            List<Integer> ids = new ArrayList<>();
            int countUserSelection = 0;
            try {
                if (userSelectionResponseModelList.size() == 1) {
                    for (UserSelectionResponseModel userSelectionResponseModel : userSelectionResponseModelList) {

                        for (GroupDataItem groupDataItem : userSelectionResponseModel.getGroupData()) {
                            pId.add(groupDataItem.getProductId());
                        }
                    }
                } else {
                    for (UserSelectionResponseModel userSelectionResponseModel : userSelectionResponseModelList) {
                        LinkedHashSet<Integer> integers = new LinkedHashSet<>();
                        for (GroupDataItem groupDataItem : userSelectionResponseModel.getGroupData()) {
                            integers.add(groupDataItem.getProductId());
                        }
                        if (integers.size() > 0) {
                            countUserSelection++;
                            ids.addAll(integers);
                        }
                    }
                }
                if (userSelectionResponseModelList.size() == 1) {
                    ArrayList<Integer> integers = new ArrayList<>(pId);
                    for (ProductModel response : productsResponse.getProducts()) {
                        double productAmount = response.getProductPrice().getPriceValue();
                        if (minPrice <= productAmount && maxPrice >= productAmount) {
                            if (integers.size() > 0) {
                                for (int i = 0; i < integers.size(); i++) {
                                    if (integers.get(i) == response.getId()) {
                                        selectedItems.add(response);
                                    }
                                }
                            }
                        }
                    }
                } else if (userSelectionResponseModelList.size() == 0) {
                    for (ProductModel response : productsResponse.getProducts()) {
                        double productAmount = response.getProductPrice().getPriceValue();
                        if (minPrice <= productAmount && maxPrice >= productAmount) {
                            selectedItems.add(response);
                        }
                    }
                } else {
                    ArrayList<Integer> listIds = new ArrayList<>();
                    Set<Integer> unique = new HashSet<Integer>(ids);
                    for (Integer key : unique) {
                        if (Collections.frequency(ids, key) == countUserSelection) {
                            listIds.add(key);
                        }
                        System.out.println(key + ": " + Collections.frequency(ids, key));
                        Log.d("answer", "" + Collections.frequency(ids, key));
                    }
                    for (ProductModel responsea : productsResponse.getProducts()) {
                        double productAmounat = responsea.getProductPrice().getPriceValue();
                        if (minPrice <= productAmounat && maxPrice >= productAmounat) {
                            if (listIds.size() > 0) {
                                for (int i = 0; i < listIds.size(); i++) {
                                    if (listIds.get(i) == responsea.getId()) {
                                        selectedItems.add(responsea);
                                    }
                                }
                            }
                        }
                    }
                }

                if (alreadySortClicked == -1)
                    productAdapter.loadData(selectedItems);
                else {
                    productAdapter.mDataSet = selectedItems;
                    alReadySortClicked();
                }
                if (selectedItems.size() > 0) {
                    txtNoRecord.setVisibility(View.GONE);
                    productListRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    txtNoRecord.setVisibility(View.VISIBLE);
                    productListRecyclerView.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (productsResponse != null && productsResponse.getProducts().size() > 0)
                    productAdapter.loadData(productsResponse.getProducts());
            }
        } else {
            isFilterClicked = false;
        }
        sortAndFilterColorChanged();
    }

    private List<ProductModel> removeDuplicateFromLastSelection(List<ProductModel> selectedItems) {
        List<ProductModel> productModels = new ArrayList<>(0);

        for (ProductModel model : selectedItems) {
            if (productModels.size() == 0) {
                productModels.add(model);
            }

            for (ProductModel model1 : productModels) {
                if (model1.getId() != model.getId()) {
                    productModels.add(model);
                }
            }
        }
        return productModels;
    }

    private Set<Integer> findDuplicates(List<Integer> listContainingDuplicates) {
        final Set<Integer> setToReturn = new HashSet<>();
        final Set<Integer> set1 = new HashSet<>();

        for (Integer yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    private void alReadySortClicked() {
        productAdapter.loadData(doSortOption(alreadySortClicked));
    }

    public Set<Integer> getMultiSelection(List<Integer> listContainingDuplicates) {

        final Set<Integer> setToReturn = new HashSet<Integer>();
        final Set<Integer> set1 = new HashSet<Integer>();

        for (Integer yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    private void sortAndFilterColorChanged() {
        if (alreadySortClicked == -1)
            rlSortby.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        else rlSortby.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));

        if (isFilterClicked)
            rlFilter.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));
        else rlFilter.setBackgroundColor(getContext().getResources().getColor(R.color.white));

    }

    private int heightOfKeyboard;

    @Override
    public void onGlobalLayout() {
        try {
            Rect r = new Rect();
            relRoot.getWindowVisibleDisplayFrame(r);
            screenHeight = relRoot.getRootView().getHeight();
            keypadHeight = screenHeight - r.bottom;
            if (keypadHeight > screenHeight * 0.15)
                isKeyBoardOpen = true;
            else isKeyBoardOpen = false;
            int newHeight = screenHeight - (r.bottom - r.top);
            if (newHeight > heightOfKeyboard) {
                heightOfKeyboard = screenHeight
                        - (r.bottom - r.top);

            }
            Log.d("fffff", "" + screenHeight + " keypadHeight  " + heightOfKeyboard);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onEvent(BaseResponse response) {
        String errors = "";
        if (response.getErrorList().length > 0) {
            for (int i = 0; i < response.getErrorList().length; i++) {
                errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
            }
            showSnackBar(errors);
        }

    }

    public void onEvent(AddtoCartResponse response) {
        String errors = "";
        if (response.getErrorList().length > 0) {
            for (int i = 0; i < response.getErrorList().length; i++) {
                errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
            }
            showSnackBar(errors);
        }

    }

    private void showSnackBar(String message) {
        Snackbar.make(Objects.requireNonNull(getView()), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onClickViewFromAdapter(String s) {
       /* if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();*/
        dismissRecyclerview();
        isApiCalled = false;
        bottomSheetLayout.setVisibility(View.VISIBLE);
        edtSearchText.setText(s);
        edtSearchText.setSelection(edtSearchText.getText().toString().length());
    }

    private void dismissRecyclerview() {
        if (recyclerView != null && recyclerView.getVisibility() == View.VISIBLE)
            recyclerView.setVisibility(View.GONE);
    }

    public void removePopUp() {

        if (recyclerView != null && recyclerView.getVisibility() == View.VISIBLE) {
            loadRecentSearch();
        } else {
            abbBar.setVisibility(View.VISIBLE);
            mainActivity.hideToolBar(true);
            if (edtSearchText.getText().toString().isEmpty()) {
                loadRecentSearch();
            }
        }
    }

    public void clearText() {
        if (edtSearchText != null) {
            MyApplication.getAppContext().clearTextCalled = true;
            editable = false;
            recyclerView.setVisibility(View.GONE);
            searchData = "test";
            isApiCalled = false;
            edtSearchText.setText("");
            txtShowText.setText("");
            sortAndFilter.setVisibility(View.GONE);
            txtNoRecord.setVisibility(View.VISIBLE);
            sortAndFilter.setVisibility(View.GONE);
            relSearchShow.setVisibility(View.GONE);
            relSearchView.setVisibility(View.VISIBLE);
            Log.d("edtSearchText", edtSearchText.getText().toString());
            // callRecentSearchApi();
          /*  if (popupWindow != null && popupWindow.isShowing()) {
                popupWindow.dismiss();
            }*/
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadRecentSearch();
                    showKeyboard();
                    isApiCalled = true;
                }
            }, 400);
        }
    }

    public void setDeFault() {
        if (null != application)
            application.isSearchFragment = false;
    }
}
