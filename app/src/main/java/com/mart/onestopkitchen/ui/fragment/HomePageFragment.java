package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.BaseProductModel;
import com.mart.onestopkitchen.model.DefaultPictureModel;
import com.mart.onestopkitchen.model.FeaturedCategory;
import com.mart.onestopkitchen.model.ImageModel;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.ProductPrice;
import com.mart.onestopkitchen.model.ProductService;
import com.mart.onestopkitchen.model.ViewType;
import com.mart.onestopkitchen.model.featureproduct.DataItem;
import com.mart.onestopkitchen.model.featureproduct.FeatureProductResponseModel;
import com.mart.onestopkitchen.model.recentview.RecentProductsResponseModel;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.FeaturedCategoryResponse;
import com.mart.onestopkitchen.networking.response.HomePageBannerResponse;
import com.mart.onestopkitchen.networking.response.HomePageCategoryResponse;
import com.mart.onestopkitchen.networking.response.HomePageProductResponse;
import com.mart.onestopkitchen.networking.response.ProductDetailResponse;
import com.mart.onestopkitchen.products.view.ProductListFragment;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.adapter.FeaturedProductAdapter;
import com.mart.onestopkitchen.ui.adapter.HomePageProductAdapter;
import com.mart.onestopkitchen.ui.adapter.HomeProductAdapterRecent;
import com.mart.onestopkitchen.ui.adapter.ProductAdapter;
import com.mart.onestopkitchen.utils.AppConstants;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.CustomTextView;
import com.mart.onestopkitchen.utils.Language;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Updated by Akash Garg on 11/02/2019.
 */

public class HomePageFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, HomeProductAdapterRecent.RecentProductClick {
    @BindView(R.id.banner)
    View bannerView;
    @BindView(R.id.slider)
    SliderLayout sliderLayout;
    @BindView(R.id.rootView)
    RelativeLayout rlRootView;
    @BindView(R.id.vg_featureCategories)
    LinearLayout featureCategoriesLinearLayout;
    @BindView(R.id.vg_featureProduct)
    LinearLayout featureProductLinearLayout;
    @BindView(R.id.featuredCategoryContainerLayout)
    LinearLayout featuredCategoryContainerLayout;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.btn_view_all_top_recent)
    Button btn_view_all_top_recent;

    @BindView(R.id.rel_recent)
    RelativeLayout rel_recent;
    @BindView(R.id.recent_Rv)
    RecyclerView recent_Rv;


    private boolean isDataLoaded = false;
    private RecyclerView featureCategoriesRv = null, featureProductRv = null;
    private View view = null;
    private HomePageProductResponse productResponseFromServer = null;
    private FeatureProductResponseModel featureProductFromServer = null;
    private HomeProductAdapterRecent productAdapterRecent = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.e("Home Fragment", "##-----------onCreate-------####");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("Home Fragment", "##-----------onCreateView-------####");
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_homepage, container, false);
        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.e("Home Fragment", "##----------- onViewCreated --------####");
        ((MainActivity) Objects.requireNonNull(getActivity())).hideToolBar(false);
        Objects.requireNonNull(getActivity()).setTitle(getActivity().getResources().getString(R.string.kitchen_app_name));
        initRv();
        checkEventBusRegistration();
        initRefreshLayout();
        if (!isDataLoaded) {
            callWebservice();
        } else {
            loadRecentView();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e("Home Fragment", "##---------onStart() ####");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("Home Fragment", "##---------onResume() ####");
        ((MainActivity) Objects.requireNonNull(getActivity())).hideToolBar(false);
        Objects.requireNonNull(getActivity()).setTitle(getActivity().getResources().getString(R.string.kitchen_app_name));
        AppConstants.mCartMgmt.setmBuyNowProduct(null);
        ((MainActivity) Objects.requireNonNull(getContext())).setUserName();

        if (!isDataLoaded) {
            callWebservice();

        }
        /*else
            loadRecentView();*/
    }


    private void callWebservice() {
        RetroClient.getApi().getHomePageBanner(Api.imageSize).enqueue(new CustomCB<HomePageBannerResponse>(rlRootView));
        RetroClient.getApi().getHomePageCategoriesWithProduct(Api.VersionCode).enqueue(new CustomCB<FeaturedCategoryResponse>(rlRootView));
        RetroClient.getApi().recentProducts().enqueue(new CustomCB<RecentProductsResponseModel>(rlRootView));
        Log.e("Home Fragment", "##-----------callWebservice()---------- ####");
//      RetroClient.getApi().getHomePageCategories(Api.imageSize).enqueue(new CustomCB<HomePageCategoryResponse>(rlRootView));
//      RetroClient.getApi().getHomePageProducts(Api.imageSize).enqueue(new CustomCB<FeatureProductResponseModel>(rlRootView));
    }


    private void initRv() {
        featureCategoriesRv = featureCategoriesLinearLayout.findViewById(R.id.rv_product);
        featureProductRv = featureProductLinearLayout.findViewById(R.id.rv_product);
        featureCategoriesRv.setLayoutManager(getLinearLayoutManager());
        featureProductRv.setLayoutManager(getLinearLayoutManager());
        hideBtns(featureProductLinearLayout);

        setTitle(getString(R.string.feature_product), featureProductLinearLayout);
        setTitle(getString(R.string.feature_categories), featureCategoriesLinearLayout);

        if (productAdapterRecent == null)
            productAdapterRecent = new HomeProductAdapterRecent(getContext(), this);

        recent_Rv.setLayoutManager(getLinearLayoutManager());
        recent_Rv.setAdapter(productAdapterRecent);
    }

    private void initRefreshLayout() {
        if (null != refreshLayout) {
            refreshLayout.setOnRefreshListener(this);
            refreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);
            refreshLayout.setEnabled(true);
        }
    }

    private LinearLayoutManager getLinearLayoutManager() {
        return new GridLayoutManager(getActivity(), 2);
    }

    private void setTitle(@NotNull String title, @NotNull View view) {
        ((CustomTextView) view.findViewById(R.id.title)).setText(AppUtils.capitalizeWords(title.toUpperCase()));
    }

    public void onEvent(@NotNull HomePageBannerResponse bannerResponse) {
        if (bannerResponse.getData() != null && bannerResponse.getData().size() > 0) {
            sliderLayout.removeAllSliders();
            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            sliderLayout.setDuration(3400);
            for (ImageModel imageModel : bannerResponse.getData()) {
                DefaultSliderView textSliderView = new DefaultSliderView(Utility.getActivity());
                textSliderView.image(imageModel.getImageUrl())
                        .setScaleType(BaseSliderView.ScaleType.Fit);

                Bundle bundle = new Bundle();
                bundle.putInt("isProduct", imageModel.getIsProduct());
                bundle.putInt("ProdOrCatId", imageModel.getProdOrCatId());
                bundle.putString("CategoryName", imageModel.getCategoryName());
                textSliderView.bundle(bundle);
                textSliderView.setOnSliderClickListener(slider -> {
                    Bundle bundle1 = slider.getBundle();
                    int isProduct = bundle1.getInt("isProduct");
                    int catOrProductId = bundle1.getInt("ProdOrCatId");
                    String categoryName = bundle1.getString("CategoryName") != null ? bundle1.getString("CategoryName") : "";
                    if (catOrProductId != 0) {
                        if (isProduct != 0) {
                            RetroClient.getApi().getProductDetails(catOrProductId).enqueue(new CustomCB<ProductDetailResponse>());
                        } else {
                            if (getFragmentManager() != null) {
//                                    getFragmentManager().beginTransl bvccaction().replace
//                                            (R.id.container, ProductListFragmentFor3_8.newInstance("Category", catOrProductId)).addToBackStack(null).commit();

                                getFragmentManager().beginTransaction()
                                        .replace(R.id.container, ProductListFragment.getInstance(catOrProductId, categoryName, 0))
                                        .addToBackStack(null).commit();
                            }
                        }
                    }
                });
                if (null != sliderLayout) {
                    sliderLayout.addSlider(textSliderView);
                    sliderLayout.setCustomIndicator(view.findViewById(R.id.custom_indicator));
                }
            }
        } else {
            if (null != sliderLayout && null != bannerView) {
                sliderLayout.setVisibility(View.GONE);
                bannerView.setVisibility(View.GONE);
            }
        }
    }

    public void onEvent(@NotNull HomePageCategoryResponse categoryResponse) {
        if (categoryResponse.getData() != null) {
            featureCategoriesLinearLayout.setVisibility(View.VISIBLE);
            HomePageProductAdapter homePageProductAdapter = new HomePageProductAdapter(getActivity(), categoryResponse.getData());
            featureCategoriesRv.setAdapter(homePageProductAdapter);
            featureCategoriesRv.setNestedScrollingEnabled(true);
            addAdapterOnclickListener(homePageProductAdapter);
        }
    }

    //feature product details
    @SuppressLint("RtlHardcoded")
    public void onEvent(FeatureProductResponseModel productResponse) {
        featureProductFromServer = productResponse;
        HomePageProductResponse homePageProductResponse = new HomePageProductResponse();
        List<ProductModel> models = new ArrayList<>();
        if (productResponse.getData() != null && productResponse.getData().size() == 0) return;
        if (productResponse.getData() != null) {
            for (DataItem item : productResponse.getData()) {
                ProductModel productModel = new ProductModel();
                productModel.setId(item.getId());
                productModel.setName(item.getName());
                DefaultPictureModel defaultPictureModel = new DefaultPictureModel();
                defaultPictureModel.setImageUrl(item.getDefaultPictureModel().getImageUrl());
                productModel.setDefaultPictureModel(defaultPictureModel);
                productModel.setShortDescription(item.getShortDescription());
                ProductPrice productPrice = new ProductPrice();
                productPrice.setPriceWithDiscount(item.getProductPrice().getPriceWithDiscount());
                productPrice.setDiscountPercentage(item.getProductPrice().getDiscountPercentage());
                productPrice.setPrice(item.getProductPrice().getPrice());
                productPrice.setOldPrice(item.getProductPrice().getOldPrice());
                productPrice.setPriceValue(item.getProductPrice().getPriceValue());
                productPrice.setProductId(item.getId());
                productModel.setProductPrice(productPrice);
                models.add(productModel);
            }
        }
        homePageProductResponse.setData(models);
        this.productResponseFromServer = homePageProductResponse;

        if (productResponse.getData() != null) {
            featureProductLinearLayout.setVisibility(View.VISIBLE);
            CustomTextView title = featureProductLinearLayout.findViewById(R.id.title);
            Button button = featureProductLinearLayout.findViewById(R.id.btn_view_all_top);
            button.setOnClickListener(view -> {
                if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", productResponseFromServer);
                    bundle.putSerializable("filter", featureProductFromServer);
                    FeatureViewAllFragment viewAllFragment = new FeatureViewAllFragment();
                    viewAllFragment.setArguments(bundle);
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().replace(R.id.container, viewAllFragment).addToBackStack(null).commit();
                    }
                }
            });
            title.setGravity(Gravity.LEFT);
        }

        FeaturedProductAdapter featuredProductAdapter;
        if (null != productResponse.getData() && productResponse.getData().size() > 3) {
            featuredProductAdapter = new FeaturedProductAdapter(getActivity(), productResponseFromServer.getData().subList(0, 4));
        } else {
            featuredProductAdapter = new FeaturedProductAdapter(getActivity(), productResponseFromServer.getData());
        }

        if (null != featureProductRv) {
            featureProductRv.setAdapter(featuredProductAdapter);
            featureProductRv.setNestedScrollingEnabled(false);
        }
        homepageProductClickListener(featuredProductAdapter);

    }

    private void hideBtns(LinearLayout linearLayout) {
        AppCompatImageButton subCatsBtn = linearLayout.findViewById(R.id.btn_menu_sub_cats);
        Button viewAllBtn = linearLayout.findViewById(R.id.btn_view_all);
        subCatsBtn.setVisibility(View.GONE);
        viewAllBtn.setVisibility(View.GONE);
        Button btnViewAllTop = linearLayout.findViewById(R.id.btn_view_all_top);
        btnViewAllTop.setVisibility(View.VISIBLE);
    }

    public void onEvent(FeaturedCategoryResponse featuredCategoryResponse) {
        refreshLayout.setRefreshing(false);
        populateFeaturedCatoriesIntoRecyclerView(featuredCategoryResponse.getData());
    }

    public void onEvent(RecentProductsResponseModel featuredCategoryResponse) {
        refreshLayout.setRefreshing(false);
        if (null != rel_recent)
            rel_recent.setVisibility(View.GONE);
        new Handler().postDelayed(() ->
                recentSearch(featuredCategoryResponse.getData()), 2000);

    }

    private void recentSearch(List<ProductModel> dataItems) {

        if (dataItems != null && dataItems.size() > 0) {
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS, new Gson().toJson(dataItems));

            if (null != btn_view_all_top_recent) {
                btn_view_all_top_recent.setOnClickListener(view -> {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", getItems());
                    RecentViewAllFragment recentViewAllFragment = new RecentViewAllFragment();
                    recentViewAllFragment.setArguments(bundle);
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().replace(R.id.container, recentViewAllFragment).addToBackStack(null).commit();
                    }
                });
            }

            if (null != rel_recent)
                rel_recent.setVisibility(View.VISIBLE);

            List<ProductModel> recentList;
            if (dataItems.size() > 3)
                recentList = dataItems.subList(0, 4);
            else
                recentList = dataItems;

            if (null != productAdapterRecent)
                productAdapterRecent.loadData(recentList);
        }
    }

    private Serializable getItems() {
        String recentData = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS);
        if (!AppUtils.cutNull(recentData).isEmpty()) {
            Type listType = new TypeToken<List<ProductModel>>() {
            }.getType();
            return new Gson().fromJson(recentData, listType);
        }
        return null;
    }

    @SuppressLint("RtlHardcoded")
    private void populateFeaturedCatoriesIntoRecyclerView(List<FeaturedCategory> featuredCategories) {
        isDataLoaded = true;
        LayoutInflater inflater = getLayoutInflater();
        featuredCategoryContainerLayout.removeAllViews();
        for (final FeaturedCategory featuredCategory : featuredCategories) {
            if (featuredCategory.getProduct() != null && featuredCategory.getProduct().size() > 0) {
                final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.merge_product_list_with_header, featureCategoriesLinearLayout, false);

                linearLayout.setVisibility(View.VISIBLE);
                CustomTextView title = linearLayout.findViewById(R.id.title);
                final AppCompatImageButton subCatsBtn = linearLayout.findViewById(R.id.btn_menu_sub_cats);
                subCatsBtn.setVisibility(View.GONE);
                Button btnViewAllTop = linearLayout.findViewById(R.id.btn_view_all_top);
                RecyclerView productRecyclerlist = linearLayout.findViewById(R.id.rv_product);
                productRecyclerlist.setHasFixedSize(true);
                title.setText(AppUtils.capitalizeWords(featuredCategory.getCategory().getName()));
                if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
                    title.setGravity(Gravity.RIGHT);
                }

                productRecyclerlist.setLayoutManager(new GridLayoutManager(getContext(), 2));
                List<ProductModel> list = featuredCategory.getProduct();

                if (list.size() > 3)
                    list = list.subList(0, 4);
                final ProductAdapter productAdapter = new ProductAdapter(getActivity(), list, ViewType.HOMEPAGE_VIEW);
                productRecyclerlist.setAdapter(productAdapter);
                productRecyclerlist.setNestedScrollingEnabled(false);
                btnViewAllTop.setOnClickListener(view -> {
                    ProductService.productId = featuredCategory.getCategory().getId();
                    gotoProductListFragment(featuredCategory.getCategory());
                });

                featuredCategoryContainerLayout.addView(linearLayout);

                productAdapter.SetOnItemClickListener((view, position) -> {
                    ProductDetailFragment.productModel = productAdapter.products.get(position);
                    gotoProductDetailsFragment();
                });

                if (featuredCategory.getSubCategory() != null && featuredCategory.getSubCategory().size() > 0) {
                    subCatsBtn.setOnClickListener(view -> addSubCats(subCatsBtn, featuredCategory.getSubCategory()));
                } else {
                    subCatsBtn.setVisibility(View.GONE);
                }
            }
        }
    }

    private void addSubCats(View view, final List<BaseProductModel> subCategories) {
        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        for (BaseProductModel subCat : subCategories) {
            popupMenu.getMenu().add(subCat.getName());
        }
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            for (BaseProductModel bpl : subCategories) {
                if (bpl.getName().contentEquals(menuItem.getTitle())) {
                    ProductService.productId = bpl.getId();
                    gotoProductListFragment(bpl);
                    break;
                }
            }
            return true;
        });
        popupMenu.show();

    }

    private void addAdapterOnclickListener(final HomePageProductAdapter homePageProductAdapter) {
        homePageProductAdapter.SetOnItemClickListener((view, position) -> {
            ProductService.productId = homePageProductAdapter.productsList.get(position).getId();
            gotoProductListFragment(homePageProductAdapter.productsList.get(position));
        });
    }

    private void homepageProductClickListener(final HomePageProductAdapter homePageProductAdapter) {
        homePageProductAdapter.SetOnItemClickListener((view, position) -> {
            ProductDetailFragment.productModel = (ProductModel) homePageProductAdapter.productsList.get(position);
            gotoProductDetailsFragment();
        });
    }

    private void gotoProductListFragment(BaseProductModel baseProductModel) {
        //     getFragmentManager().beginTransaction().replace
        //          (R.id.container, ProductListFragmentFor3_8.newInstance(baseProductModel.getName(), (int) baseProductModel.getId())).addToBackStack(null).commit();
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction().replace(R.id.container, ProductListFragment.getInstance(Integer.parseInt(String.valueOf(baseProductModel.getId())), HomePageFragment.class.getSimpleName(), baseProductModel.getName())).addToBackStack(null).commit();
            }
        }
    }

    private void gotoProductDetailsFragment() {
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            FragmentTransaction transaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, new ProductDetailFragment()).addToBackStack(null).commit();
        }
    }

    @Override
    public void onRefresh() {
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            refreshLayout.setRefreshing(true);
            callWebservice();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (null != menu.findItem(R.id.menu_search))
            menu.findItem(R.id.menu_search).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void productClick(ProductModel dataItem) {
        ProductDetailFragment.productModel = dataItem;
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new ProductDetailFragment()).
                    addToBackStack(null).commit();
        }
    }

    private void loadRecentView() {
        String recentData = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.RECENT_VIEW_PRODUCTS);
        if (!AppUtils.cutNull(recentData).isEmpty()) {
            Type listType = new TypeToken<List<ProductModel>>() {
            }.getType();
            List<ProductModel> getDate = new Gson().fromJson(recentData, listType);
            if (getDate != null && getDate.size() > 0) {
                rel_recent.setVisibility(View.VISIBLE);
                if (getDate.size() > 4)
                    productAdapterRecent.loadData(getDate.subList(0, 4));
                else productAdapterRecent.loadData(getDate);
            } else
                rel_recent.setVisibility(View.GONE);
        } else rel_recent.setVisibility(View.GONE);

        Log.e("Home Fragment", "###------loadRecentView() ####");
    }


    @Override
    public void onDestroyView() {
        Log.e("HomePageFragmnt", "###--------onDestroyView-------");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.e("HomePageFragmnt", "###---------onDestroy-------");
        releaseObj();
        super.onDestroy();
    }

    private void releaseObj() {
        if (null != productAdapterRecent) {
            productAdapterRecent = null;
        }
        if (null != featureProductFromServer) {
            featureProductFromServer = null;
        }
        if (null != productResponseFromServer) {
            productResponseFromServer = null;
        }
        if (null != featureProductRv) {
            featureProductRv = null;
        }
        if (null != featureCategoriesRv) {
            featureCategoriesRv = null;
        }
        if (null != sliderLayout) {
            sliderLayout = null;
        }
        if (null != view) {
            view = null;
        }
    }

}
