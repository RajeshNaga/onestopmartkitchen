package com.mart.onestopkitchen.ui.activity;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.LinearLayoutManager;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashraful on 2/10/2016.
 */
public class MotherActivity extends BaseActivity {
    public ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onStart() {
        super.onStart();
        try {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }
        } catch (Exception ex) {

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    public LinearLayoutManager getLinearLayoutManagerHorizontal() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(MotherActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return layoutManager;
    }

}