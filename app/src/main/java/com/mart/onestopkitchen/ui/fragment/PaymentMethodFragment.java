package com.mart.onestopkitchen.ui.fragment;

import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.PaymentMethod;
import com.mart.onestopkitchen.mvp.ui.cashin.NearByOk$ServiceFragment;
import com.mart.onestopkitchen.networking.Api;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.postrequest.ValuePost;
import com.mart.onestopkitchen.networking.response.PaymentMethodRetrievalResponse;
import com.mart.onestopkitchen.networking.response.PaymentMethodSaveResponse;
import com.mart.onestopkitchen.ui.customview.RadioGridGroupforReyMaterial;
import com.mart.onestopkitchen.ui.views.MethodSelctionProcess;
import com.mart.onestopkitchen.utils.Keys;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Updated by Akash Garg on 15/03/2019.
 */

public class PaymentMethodFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.rg_shipiingMethod)
    RadioGridGroupforReyMaterial radioGridGroup;
    @BindView(R.id.btn_continue)
    Button continueBtn;
    private int id = 0;
    private MethodSelctionProcess methodSelctionProcess;
    private String PaymentMethodValue = "";

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shipping_method, container, false);
        unbinder = ButterKnife.bind(this, view);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.payment));
        return view;
    }

    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.payment));
        id = 0;
        callApiOfGettingPaymentMethod();
        continueBtn.setOnClickListener(this);
    }

    private void callApiOfGettingPaymentMethod() {
        RetroClient.getApi().getPaymentMethod().enqueue(new CustomCB<PaymentMethodRetrievalResponse>(this.getView()));
    }

    public void onEvent(PaymentMethodRetrievalResponse paymentMethodRetrievalResponse) {
        Log.d("paymentMethods", new Gson().toJson(paymentMethodRetrievalResponse));
        addMethodRadioGroup(paymentMethodRetrievalResponse.getPaymentMethods());
    }

    private void addMethodRadioGroup(List<PaymentMethod> paymentMethods) {
        methodSelctionProcess = new MethodSelctionProcess(radioGridGroup);
        for (PaymentMethod method : paymentMethods) {
            generateRadioButton(method);
        }
    }

    private void generateRadioButton(final PaymentMethod method) {
        String paymentMethodName = "";
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().
                inflate(R.layout.item_payment_method, radioGridGroup, false);
        TextView nearByService = linearLayout.findViewById(R.id.txt_near_by);

        final RadioButton radioButton = linearLayout.findViewById(R.id.rb_paymentChoice);

        if (method.getName().equalsIgnoreCase("2C2P")) {
            paymentMethodName = getString(R.string.select_payment_type);
        } else if (method.getName().equalsIgnoreCase("OK Dollar")) {
            paymentMethodName = getString(R.string.select_okdollar_payment);
        } else {
            paymentMethodName = method.getName();
        }

//        radioButton.setText(method.getName());
        radioButton.setText(paymentMethodName);
        radioButton.setId(++id);

        if (isPreselected(method)) {
            radioButton.setChecked(true);
            PaymentMethodValue = method.getPaymentMethodSystemName();
        }

        if (method.getPaymentMethodSystemName().contains(Keys.OKDOLLAR_PAYMENT) || method.getName().contains("OK Dollar")) {
            nearByService.setVisibility(View.VISIBLE);
            nearByService.setPaintFlags(nearByService.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        nearByService.setOnClickListener(view -> {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet())
                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new NearByOk$ServiceFragment()).addToBackStack(null).commit();
                }
        });
        ImageView imageView = linearLayout.findViewById(R.id.iv_paymentMethodImage);
        Glide.with(getActivity()).load(method.getLogoUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

        radioGridGroup.addView(linearLayout);

        radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                methodSelctionProcess.resetRadioButton(buttonView.getId());
                PaymentMethodValue = method.getPaymentMethodSystemName();
            }
        });

        linearLayout.setOnClickListener(v -> radioButton.setChecked(true));
    }

    private boolean isPreselected(PaymentMethod method) {
        return method.isSelected();
    }

    @Override
    public void onClick(View v) {
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet())
            if (!PaymentMethodValue.isEmpty()) {
                RetroClient.getApi().saveCheckoutPaymentMethod(new ValuePost(PaymentMethodValue, Api.VersionCode))
                        .enqueue(new CustomCB<PaymentMethodSaveResponse>(this.getView()));
                continueBtn.setOnClickListener(null);
            }
    }

    public void onEvent(PaymentMethodSaveResponse saveResponse) {
        if (saveResponse.getStatusCode() == 200) {
            continueBtn.setOnClickListener(this);
            ConfirmOrderFragment.PaymentMethodSystemName = PaymentMethodValue;

            /*if (Build.VERSION.SDK_INT >= M) {
                if (PaymentMethodValue.equalsIgnoreCase(Keys.OKDOLLAR_PAYMENT)) {
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().replace(R.id.container, new ConfirmOrderFragment()).addToBackStack(null).commit();
                    }
                } else {
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().replace(R.id.container, new CardDetailsFragment()).addToBackStack(null).commit();
                    }
                }
            } else*/

            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                        .add(R.id.container, new ConfirmOrderFragment()).addToBackStack(null).commit();
            }
        } else {
            continueBtn.setOnClickListener(this);
            if (null != saveResponse.getErrorList() && saveResponse.getErrorList().length > 0) {
                Toast.makeText(getActivity(), saveResponse.getErrorList()[0], Toast.LENGTH_SHORT).show();
            }
        }
    }
}

