package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.AboutUsResponse;
import com.mart.onestopkitchen.ui.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ashraful on 2/11/2016.
 */
public class AboutUsFragment extends BaseFragment {

    @BindView(R.id.wv_about_us)
    WebView wv_about_us;

    String aboutUsText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ((BaseActivity) getActivity()).getSupportActionBar().setLogo(null);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);

        getActivity().setTitle(getString(R.string.about_us));

        callWebService();

    }

    protected void callWebService() {
        RetroClient.getApi().getAboutUs()
                .enqueue(new CustomCB<AboutUsResponse>(this.getView()));
    }

    public void onEvent(AboutUsResponse aboutUsResponse) {

        aboutUsText = aboutUsResponse.getAboutUsModel().getTopicBody();

        if (aboutUsText != null) {
            wv_about_us.loadDataWithBaseURL("", aboutUsText,
                    "text/html",
                    "UTF-8",
                    "");
        }
       /* else if (aboutUsResponse.getAboutUsModel().getShortDescription() != null)
        {
            wv_about_us.loadDataWithBaseURL("",
                    aboutUsResponse.getAboutUsModel().getShortDescription(),
                    "text/html",
                    "UTF-8",
                    "");
        }*/

    }

}
