package com.mart.onestopkitchen.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.base64imge.CircleTransform;
import com.mart.onestopkitchen.camera.FaceTrackerActivity;
import com.mart.onestopkitchen.model.CustomerInfo;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.presenter.GetAwsImageUrlPresenter;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.CircleImageView;
import com.mart.onestopkitchen.utils.CustomEdittext;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;
import com.mart.onestopkitchen.utils.mobileNumberValidation.MobileNumberValidation;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.mart.onestopkitchen.service.PreferenceService.PROFILE_PIC;
import static com.mart.onestopkitchen.service.PreferenceService.USER_COUNTRY_CODE;
import static com.mart.onestopkitchen.utils.AppConstants.FROM_SIGN_UP_SELECT_CAPTURE;
import static com.mart.onestopkitchen.utils.AppUtils.cutNull;

/**
 * Created by Nidhin on 10/01/2018 and 10/02/2018.
 */

public class CustomerInfoFragment extends BaseFragment implements ViewTreeObserver.OnGlobalLayoutListener, GetAwsImageUrlPresenter.ImageResponseListener {
    @BindView(R.id.customer_name)
    TextView customerNameTextView;
    @BindView(R.id.dateOfBirth)
    TextView dateOfBirthTextView;
    @BindView(R.id.customer_email)
    TextView emailTextView;
    @BindView(R.id.row_customerEmail)
    TableRow row_customerEmail;
    @BindView(R.id.et_customer_email)
    EditText emailEditText;
    @BindView(R.id.rb_male)
    RadioButton genderMaleRadioButton;
    @BindView(R.id.rb_female)
    RadioButton genderFemaleRadioButton;
    @BindView(R.id.genderRadioGroup)
    RadioGroup genderRadioGroup;
    @BindView(R.id.btn_save)
    Button saveBtn;
    @BindView(R.id.img_user)
    CircleImageView img_user;
    @BindView(R.id.et_Customer_phoneone)
    CustomEdittext et_Customer_phoneone;
    @BindView(R.id.txt_country_code)
    TextView txt_country_code;
    @BindView(R.id.showpass)
    ImageView show_password;
    @BindView(R.id.password_show)
    TextView password_show;
    @BindView(R.id.txt_country_code_anotherNumber)
    TextView txt_country_code_anotherNumber;
    @BindView(R.id.additional_customer_phone)
    TextView additional_customer_phone;
    @BindView(R.id.et_customer_phone)
    CustomEdittext phoneEditText;
    @BindView(R.id.rel_root)
    RelativeLayout relRoot;
    @BindView(R.id.lin_empty_mobile)
    LinearLayout linEmptyMobile;
    @BindView(R.id.lin_enter_phone)
    LinearLayout linEnterPhone;

    private String TAG = "CustomerInfoFragment";
    private String gender = "", fName = "", imageUrl = "", base64String = "", userPass = "", UserName = "", CustomerAnotherNUmber = "", CountryCode = "";
    private int minimumLength = 0, maximumLength = 0;
    private boolean isKeyBoardOpen, eyeIconClicked, needToshowToast, userTakePhoto = false, buttonStatus = true;
    private int dateDOB = 0, monthDOB = 0, yearDOB = 0;

    private View view = null;
    private CustomerInfo customerInfo = null;
    private MobileNumberValidation mobileNumberValidation = null;
    private GetAwsImageUrlPresenter awsImageUrlPresenter = null;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.customer_basic_info_new, container, false);
        unbinder = ButterKnife.bind(this, view);
//        Log.e(TAG, "-----###onCreateView()###------");
        return view;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkEventBusRegistration();
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.my_account));
        initView();
        mobileNumberListener("");
        initEditButtonsAction();
        keyBoardListener();
        callWebservice();
//        Log.e(TAG, "-----###onViewCreated()###------");
    }

    private void initView() {
        if (mobileNumberValidation == null)
            mobileNumberValidation = MobileNumberValidation.getInstance(getActivity(), "95");
        if (customerInfo == null)
            customerInfo = new CustomerInfo();
        saveBtn.setBackgroundColor(getResources().getColor(R.color.light_gray));
        saveBtn.setTextColor(getResources().getColor(R.color.gray_normal));
        saveBtn.setEnabled(false);
        saveBtn.setClickable(false);
        CountryCode = PreferenceService.getInstance().GetPreferenceValue(USER_COUNTRY_CODE);
        emailEditText.setVisibility(View.GONE);
    }

    private void btnStatus() {
        saveBtn.setBackgroundColor(getResources().getColor(R.color.light_gray));
        saveBtn.setTextColor(getResources().getColor(R.color.gray_normal));
        saveBtn.setEnabled(false);
        saveBtn.setClickable(false);
        buttonStatus = false;
    }

    @SuppressLint("SetTextI18n")
    private void mobileNumberListener(String phoneEditTexts) {
        phoneEditTexts = AppUtils.cutNull(phoneEditTexts);
        mobileNumberValidation = MobileNumberValidation.getInstance(getActivity(), "95");
        if (null != CustomerAnotherNUmber && CustomerAnotherNUmber.startsWith("0095")) {
            phoneEditText.setText("09" + phoneEditTexts.replace("0095", ""));
        } else {
            phoneEditText.setText(phoneEditTexts);
        }
        phoneEditText.setSelection(phoneEditText.getText().toString().length());
        mobileNumberValid();
        phoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().startsWith("09")) {
                    phoneEditText.setText(setMobileNumberTextColor(s));
                    phoneEditText.setSelection(phoneEditText.length());
                }

                btnStatus();
                int length = s.toString().length();
                if (length > 3) {
                    mobileNumberValidation.validateMobileNumber(true, "95", s.toString(), new MobileNumberValidation.MobileValidationListener() {
                        @Override
                        public void onSetValidationLimit(boolean validNumber, int minLength, int maxLength, String operator, String operatorColor) {
                            if (validNumber) {
                                minimumLength = minLength;
                                maximumLength = maxLength;
                                setEditTextMaxLength(phoneEditText, maximumLength);
                            } else {
                                phoneEditText.setText(setMobileNumberTextColor(s));
                                Toast.makeText(getContext(), getString(R.string.invalid_mobile_number), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onSelectedOtherCountry(boolean error) {
                        }
                    });

                }
                if (length > 3) {
                    if (length >= minimumLength && length <= maximumLength && maximumLength == minimumLength) {
                        btnStatusChanged();
                    } else {
                        if (maximumLength != minimumLength && length >= maximumLength) {
                            btnStatusChanged();
                        }
                        if (maximumLength != minimumLength && length >= minimumLength) {
                            btnStatusChanged();
                        }
                    }
                } else if (length <= 2)
                    btnStatusChanged();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().startsWith("090") || s.toString().startsWith("091")) {
                    phoneEditText.setText(setMobileNumberTextColor(s.toString()));
                    phoneEditText.setSelection(phoneEditText.length());
                }
            }
        });
        this.phoneEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
    }

    private void btnStatusChanged() {
        saveBtn.setBackgroundColor(getResources().getColor(R.color.bt_blue));
        saveBtn.setTextColor(getResources().getColor(R.color.white));
        saveBtn.setEnabled(true);
        saveBtn.setClickable(true);
        buttonStatus = true;
    }

    private CharSequence setMobileNumberTextColor(CharSequence s) {
        SpannableString ss1 = new SpannableString("0");
        ss1.setSpan(new ForegroundColorSpan(Color.GRAY), 0, ss1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannableString ss2 = new SpannableString("9");
        ss2.setSpan(new ForegroundColorSpan(Color.GRAY), 0, ss2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return TextUtils.concat(ss1, ss2);
    }

    @SuppressLint("ShowToast")
    private void mobileNumberValid() {
        if (null != mobileNumberValidation) {
            mobileNumberValidation.validateMobileNumber(true, "95", phoneEditText.getText().toString(), new MobileNumberValidation.MobileValidationListener() {

                @Override
                public void onSetValidationLimit(boolean validNumber, int minLength, int maxLength, String operator, String operatorColor) {
                    if (validNumber) {
                        minimumLength = minLength;
                        maximumLength = maxLength;
                        setEditTextMaxLength(phoneEditText, maximumLength);
                    } else {
                        phoneEditText.setText(AppUtils.getMyanmarNumber());
                        Toast.makeText(getActivity(), getString(R.string.invalid_mobile_number), Toast.LENGTH_SHORT);
                    }
                    if (phoneEditText.getText().toString().length() > 3) {
                        if (phoneEditText.getText().toString().length() >= minimumLength) {
                            phoneEditText.getText().toString().length();
                        }
                    }
                }

                @Override
                public void onSelectedOtherCountry(boolean error) {

                }
            });
        }
    }

    private void setEditTextMaxLength(EditText editText, int length) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                String character = String.valueOf(source.charAt(i));
                if (character.equalsIgnoreCase("/") || character.equalsIgnoreCase("%") || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("&") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";") || character.equalsIgnoreCase("<") || character.equalsIgnoreCase(">") || character.equalsIgnoreCase("?") || character.equalsIgnoreCase("[") || character.equalsIgnoreCase("]") || character.equalsIgnoreCase("\"") || character.equalsIgnoreCase("^") || character.equalsIgnoreCase("_") || character.equalsIgnoreCase("|") || character.equalsIgnoreCase("~") || character.equalsIgnoreCase("@") || character.equalsIgnoreCase("-")) {
                    return "";
                }
            }
            return null;
        };
        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(length);
        fArray[1] = filter;
        editText.setFilters(fArray);
    }

    private void callWebservice() {
        RetroClient.getApi().getCustomerInfo().enqueue(new CustomCB<CustomerInfo>(relRoot));
        saveBtn.setBackgroundColor(getResources().getColor(R.color.bt_blue));
        saveBtn.setTextColor(getResources().getColor(R.color.white));
        saveBtn.setEnabled(true);
        saveBtn.setClickable(true);
        needToshowToast = false;
    }

    private void callSaveCustomerInfoWebService() {
        RetroClient.getApi().saveCustomerInfo(customerInfo).enqueue(new CustomCB<CustomerInfo>(this.getView()));
        saveBtn.setEnabled(true);
        saveBtn.setClickable(false);
        saveBtn.setBackgroundColor(getResources().getColor(R.color.light_gray));
        saveBtn.setTextColor(getResources().getColor(R.color.gray_normal));
        row_customerEmail.setVisibility(View.VISIBLE);
        emailEditText.setVisibility(View.GONE);
        needToshowToast = true;
    }

    private void takePhoto() {
        Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        startActivityForResult(new Intent(getActivity(), FaceTrackerActivity.class), FROM_SIGN_UP_SELECT_CAPTURE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == FROM_SIGN_UP_SELECT_CAPTURE) {
            userTakePhoto = true;
            passDataForProfilePic(data);
        }
    }

    private void passDataForProfilePic(Intent data) {
        if (data != null && data.hasExtra(Keys.IMAGE_PATH)) {
            Bitmap bitmap = BitmapFactory.decodeFile(data.getStringExtra(Keys.IMAGE_PATH));
            if (bitmap != null) {
                saveBtn.setEnabled(true);
                saveBtn.setClickable(true);
                saveBtn.setBackgroundColor(getResources().getColor(R.color.bt_blue));
                saveBtn.setTextColor(getResources().getColor(R.color.white));
                img_user.setImageBitmap(bitmap);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                base64String = Base64.encodeToString(byteArray, Base64.DEFAULT);
                preferenceService.SetPreferenceValue(PROFILE_PIC, cutNull(data.getStringExtra(Keys.IMAGE_PATH)));
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void onEvent(CustomerInfo customer) {
        String monthString = "";
        userTakePhoto = false;

        et_Customer_phoneone.setEnabled(false);
        et_Customer_phoneone.setClickable(false);
        et_Customer_phoneone.setCursorVisible(false);
        if (null != customer && customer.getStatusCode() == 200) {
            customerNameTextView.setText(customer.getFirstName());
            fName = customer.getFirstName();
            if (!customer.getDisplayAvatar().equals("false")) {
                try {
                    imageUrl = customer.getProfilePictureUrl() == null ? "" : customer.getProfilePictureUrl();
                    Glide.with(getActivity())
                            .load(customer.getProfilePictureUrl())
                            .transform(new CircleTransform(getActivity()))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(img_user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (customer.getDateOfBirthDay() > 0) {
                try {
                    SimpleDateFormat monthParse = new SimpleDateFormat("MM", Locale.ENGLISH);
                    SimpleDateFormat monthDisplay = new SimpleDateFormat("MMM", Locale.ENGLISH);
                    monthString = monthDisplay.format(monthParse.parse(String.valueOf(customer.getDateOfBirthMonth())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dateDOB = customer.getDateOfBirthDay();
                monthDOB = customer.getDateOfBirthMonth();
                yearDOB = customer.getDateOfBirthYear();
                dateOfBirthTextView.setText(customer.getDateOfBirthDay() + "-" + monthString + "-" + customer.getDateOfBirthYear());
                Calendar myCalendar = Calendar.getInstance();
                myCalendar.set(Calendar.YEAR, customer.getDateOfBirthYear());
                myCalendar.set(Calendar.MONTH, customer.getDateOfBirthMonth() - 1);
                myCalendar.set(Calendar.DAY_OF_MONTH, customer.getDateOfBirthDay());
                dateOfBirthTextView.setEnabled(false);
                dateOfBirthTextView.setClickable(false);
                dateOfBirthTextView.setCursorVisible(false);
            } else {
                dateOfBirthTextView.setText("");
            }

            if (!CountryCode.equalsIgnoreCase("")) {
                txt_country_code.setText(CountryCode);
                txt_country_code_anotherNumber.setText(CountryCode);
            } else {
                txt_country_code.setText(R.string.country_code);
                txt_country_code_anotherNumber.setText(R.string.country_code);
            }
            emailTextView.setText(customer.getEmail());
            emailEditText.setText(customer.getEmail());
            userPass = customer.getPassword();
            password_show.setText("\u2022" + "\u2022" + "\u2022" + "\u2022" + "\u2022" + "\u2022");
            et_Customer_phoneone.setText(AppUtils.formattedNumber(customer.getUsername()));
            UserName = customer.getPhone();
            show_password.setImageDrawable(getResources().getDrawable(R.drawable.disable));
            eyeIconClicked = false;
            if (customer.getGender().equalsIgnoreCase("M")) {
                genderMaleRadioButton.setChecked(true);
                gender = "0";
            } else if (customer.getGender().equalsIgnoreCase("F")) {
                genderFemaleRadioButton.setChecked(true);
                gender = "1";
            }
            if (needToshowToast) {
                saveBtn.setEnabled(true);
                Toast.makeText(getActivity(), R.string.customer_info_updated, Toast.LENGTH_LONG).show();
            }

            if (TextUtils.isEmpty(customer.getOtherMobileNumber().trim())) {
                additional_customer_phone.setHint(R.string.additional_mobile_number);
                linEmptyMobile.setVisibility(View.VISIBLE);
                linEnterPhone.setVisibility(View.GONE);
                phoneEditText.setText("");
            } else {
                if (customer.getOtherMobileNumber().trim().substring(4).length() > 3) {
                    CustomerAnotherNUmber = customer.getOtherMobileNumber();
                    if (customer.getOtherMobileNumber().startsWith("0095")) {
                        phoneEditText.setText(customer.getOtherMobileNumber().substring(4).trim());
                    } else {
                        phoneEditText.setText(customer.getOtherMobileNumber());
                    }
                    linEmptyMobile.setVisibility(View.VISIBLE);
                    linEnterPhone.setVisibility(View.GONE);
                    additional_customer_phone.setText(AppUtils.formattedNumber(customer.getOtherMobileNumber()));
                } else {
                    linEmptyMobile.setVisibility(View.VISIBLE);
                    linEnterPhone.setVisibility(View.GONE);
                    phoneEditText.setText("");
                    additional_customer_phone.setText("");
                    additional_customer_phone.setHint(R.string.additional_mobile_number);
                }
            }
        } else {
            if (!saveBtn.isEnabled()) {
                saveBtn.setEnabled(true);
                StringBuilder errors = new StringBuilder(getString(R.string.error_saving_data) + "\n");
                if (Objects.requireNonNull(customer).getErrorList().length > 0) {
                    for (int i = 0; i < customer.getErrorList().length; i++) {
                        errors.append("  ").append(i + 1).append(": ").append(customer.getErrorList()[i]).append(" \n");
                    }
                    Toast.makeText(getActivity(), errors.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }
        btnStatus();
    }

    @OnClick({R.id.img_user, R.id.camera_img, R.id.showpass, R.id.editAdditionalCustomerPhone, R.id.editCustomerEmail, R.id.btn_save})
    void OnClickBtn(@NotNull View view) {
        switch (view.getId()) {
            case R.id.showpass:
                showPassword();
                break;
            case R.id.img_user:
                openCamera();
                break;
            case R.id.camera_img:
                openCamera();
                break;
            case R.id.editAdditionalCustomerPhone:
                editAdditionalNumber();
                break;
            case R.id.editCustomerEmail:
                editEmailId();
                break;
            case R.id.btn_save:
                btnSubmit();
                break;
            default:
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void initEditButtonsAction() {
        try {
            genderRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                try {
                    View radioButton = genderRadioGroup.findViewById(checkedId);
                    int index = genderRadioGroup.indexOfChild(radioButton);
                    if (!gender.equals(index)) {
                        saveBtn.setBackgroundColor(getResources().getColor(R.color.bt_blue));
                        saveBtn.setTextColor(getResources().getColor(R.color.white));
                        saveBtn.setEnabled(true);
                        saveBtn.setClickable(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            emailEditText.setOnFocusChangeListener((view1, b) -> {
                if (isAdded() && saveBtn != null) {
                    if (buttonStatus) {
                        saveBtn.setEnabled(true);
                        saveBtn.setClickable(true);
                        saveBtn.setBackgroundColor(getResources().getColor(R.color.bt_blue));
                        saveBtn.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        btnStatus();
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void btnSubmit() {
        if (isKeyBoardOpen) {
            hideKeyboard();
        }
        needToshowToast = true;
        updateUserInfo();
    }

    private void editEmailId() {
        try {
            row_customerEmail.setVisibility(View.GONE);
            emailEditText.setVisibility(View.VISIBLE);
            emailEditText.setSelection(emailEditText.getText().length());
            emailEditText.requestFocus();
            saveBtn.setEnabled(true);
            saveBtn.setClickable(true);
            saveBtn.setBackgroundColor(getResources().getColor(R.color.bt_blue));
            saveBtn.setTextColor(getResources().getColor(R.color.white));
            if (!isKeyBoardOpen)
                ((MainActivity) Objects.requireNonNull(getActivity())).showKeyboard();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void editAdditionalNumber() {
        saveBtn.setBackgroundColor(getResources().getColor(R.color.bt_blue));
        saveBtn.setTextColor(getResources().getColor(R.color.white));
        saveBtn.setEnabled(true);
        saveBtn.setClickable(true);
        linEmptyMobile.setVisibility(View.GONE);
        linEnterPhone.setVisibility(View.VISIBLE);
        phoneEditText.setText(additional_customer_phone.getText().toString().trim().replace("+959", "09"));
        phoneEditText.setSelection(phoneEditText.getText().toString().trim().length());
        phoneEditText.requestFocus();
        if (!isKeyBoardOpen)
            ((MainActivity) Objects.requireNonNull(getActivity())).showKeyboard();
    }

    private void openCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                takePhoto();
            } else {
                checkAndRequestPermissions();
            }
        } else {
            takePhoto();
        }
    }

    @SuppressLint("SetTextI18n")
    private void showPassword() {
        if (!eyeIconClicked) {
            eyeIconClicked = true;
            password_show.setText(userPass);
            show_password.setImageDrawable(getResources().getDrawable(R.drawable.enable));

        } else {
            show_password.setImageDrawable(getResources().getDrawable(R.drawable.disable));
            eyeIconClicked = false;
            password_show.setText("\u2022" + "\u2022" + "\u2022" + "\u2022" + "\u2022" + "\u2022");
        }
    }

    private void checkAndRequestPermissions() {
        int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
        int permissionSendMessage = ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }

    private void updateUserInfo() {
        if (emailEditText.getText().toString().trim().length() > 0 && !AppUtils.isValidEmail(emailEditText.getText().toString().trim())) {
            Toast.makeText(getActivity(), R.string.enter_valid_email, Toast.LENGTH_SHORT).show();
        } else {
            if (!phoneEditText.getText().toString().equalsIgnoreCase("") && phoneEditText.getText().toString().trim().length() > 2) {
                if (phoneEditText.getText().toString().trim().length() > 7) {
                    callUpdationApi();
                } else {
                    Toast.makeText(getActivity(), R.string.please_enter_valid_additional_mobile_number, Toast.LENGTH_SHORT).show();
                }
            } else {
                callUpdationApi();
            }
        }
    }

    private void callUpdationApi() {
        if (userTakePhoto) {
            showLoading();
            awsImageUrlPresenter = new GetAwsImageUrlPresenter(this, getImageRequest());
        } else {
            callUpdateApi();
        }

    }

    private JsonObject getImageRequest() {
        String mobileNumber = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_MOBILE_NUMBER);
        String code = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_COUNTRY_CODE);
        if (code.startsWith("+95")) {
            mobileNumber = "00959" + mobileNumber.substring(2);
        }
        JsonObject jsonObject = new JsonObject();
        try {
            JsonArray array = new JsonArray();
            array.add(base64String);
            jsonObject.addProperty("MobileNumber", mobileNumber);
            jsonObject.add("Base64String", array);
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void imageOnSuccess(String imgUrl) {
        imageUrl = imgUrl;
        callUpdateApi();
        hideLoading();
    }

    @Override
    public void onFailure(String s) {
        hideLoading();
        img_user.setImageResource(R.drawable.user_profile_avatar);
    }

    @Override
    public void showLoading() {
        AppDialogs.loadingDialog(getContext(), false, "");
    }

    @Override
    public void hideLoading() {
        AppDialogs.dismissDialog();
    }

    private void callUpdateApi() {
        customerInfo.setEmail(emailEditText.getText().toString());
        customerInfo.setPhone(UserName);
        customerInfo.setUsername(UserName);
        customerInfo.setProfilePictureUrl(imageUrl);
        customerInfo.setNewsletter(true);
        customerInfo.setLastName(".");
        customerInfo.setCompany("ERP");
        customerInfo.setFirstName(fName);
        customerInfo.setDateOfBirthDay(dateDOB);
        customerInfo.setDateOfBirthMonth(monthDOB);
        customerInfo.setDateOfBirthYear(yearDOB);
        if (imageUrl.isEmpty()) {
            customerInfo.setDisplayAvatar("false");
        } else {
            customerInfo.setDisplayAvatar("true");
        }

        if (!emailEditText.getText().toString().equals("")) {
            customerInfo.setIsDisplayEmail(true);
        } else {
            customerInfo.setIsDisplayEmail(false);
        }

        if (genderMaleRadioButton.isChecked()) {
            customerInfo.setGender("M");
            gender = "0";
        } else if (genderFemaleRadioButton.isChecked()) {
            customerInfo.setGender("F");
            gender = "1";
        }

       /* if (!phoneEditText.getText().toString().isEmpty() && phoneEditText.getText().toString().trim().substring(2).isEmpty()) {
            customerInfo.setOtherMobileNumber(CustomerAnotherNUmber);
        } else {
            if (null != UserName && UserName.startsWith("0095")) {
                customerInfo.setOtherMobileNumber("0095" + phoneEditText.getText().toString().substring(1));
            } else {
                customerInfo.setOtherMobileNumber(phoneEditText.getText().toString().trim());
            }
        }*/
        customerInfo.setOtherMobileNumber("0095" + phoneEditText.getText().toString().substring(1));

        callSaveCustomerInfoWebService();

        saveBtn.setBackgroundColor(getResources().getColor(R.color.light_gray));
        saveBtn.setTextColor(getResources().getColor(R.color.gray_normal));
    }

    @SuppressLint("SetTextI18n")
    private void keyBoardListener() {
        ViewTreeObserver observer = relRoot.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(this);
        password_show.setText("\u2022" + "\u2022" + "\u2022" + "\u2022" + "\u2022" + "\u2022");
    }

    @Override
    public void onGlobalLayout() {
        try {
            Rect r = new Rect();
            if (relRoot != null) {
                relRoot.getWindowVisibleDisplayFrame(r);
                int screenHeight = relRoot.getRootView().getHeight();
                int keypadHeight = screenHeight - r.bottom;
                isKeyBoardOpen = keypadHeight > screenHeight * 0.15;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
//        Log.e(TAG, "-----###onStart()###------");
    }

    @Override
    public void onStop() {
        super.onStop();
//        Log.e(TAG, "---------###onStop()###------");
    }

    @Override
    public void onPause() {
        super.onPause();
//        Log.e(TAG, "-------######onPause()#########");
    }

    @Override
    public void onDestroy() {
        onReleaseObj();
        super.onDestroy();
//        Log.e(TAG, "---------######onDestroy()#########");

    }

    @Override
    public void onDestroyView() {
        onReleaseObj();
        super.onDestroyView();
//        Log.e(TAG, "-----###onDestroyView()###------");
    }

    @Override
    public void onResume() {
        super.onResume();
//        Log.e(TAG, "--------###onResume()###------");
    }

    private void onReleaseObj() {
        if (null != awsImageUrlPresenter) {
            awsImageUrlPresenter.onDispose();
        }
        if (null != customerInfo) {
            customerInfo = null;
        }
        if (null != mobileNumberValidation) {
            mobileNumberValidation = null;
        }
        if (null != base64String) {
            base64String = null;
        }
        if (null != imageUrl) {
            imageUrl = null;
        }
        if (null != CustomerAnotherNUmber) {
            CustomerAnotherNUmber = null;
        }
        if (null != view) {
            view = null;
        }
        if (null != gender) {
            gender = null;
        }
        if (null != TAG) {
            TAG = null;
        }
    }
}
