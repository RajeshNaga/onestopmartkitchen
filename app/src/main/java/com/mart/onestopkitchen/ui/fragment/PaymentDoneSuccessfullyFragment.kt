package com.mart.onestopkitchen.ui.fragment

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.mart.onestopkitchen.BR
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.application.MyApplication
import com.mart.onestopkitchen.model.PaymentResponseModel
import com.mart.onestopkitchen.mvp.ui.ratingandreview.RatingAndReviewFragment
import com.mart.onestopkitchen.service.PreferenceService
import com.mart.onestopkitchen.ui.adapter.OrderGroupReceiptPageAdapter
import com.mart.onestopkitchen.utils.AppConstants
import com.mart.onestopkitchen.utils.Keys
import com.mart.onestopkitchen.utils.PDFInvoiceCreater
import kotlinx.android.synthetic.main.fragment_payment_done_successfully.*
import java.io.File

class PaymentDoneSuccessfullyFragment : BaseFragment() {
    private val TAG = PaymentDoneSuccessfullyFragment::class.java.simpleName
    private var binding: ViewDataBinding? = null
    var data: PaymentResponseModel? = null
    private lateinit var pdfFileName: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment_done_successfully, container, false)
        activity!!.title = getString(R.string.payment_successful)
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.IS_PAYMENT_DONE, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPaymentResponse()
        MyApplication.PROCEED_AS_GUEST = false
        btnDone.setOnClickListener {
            nextFragment()
        }
        btnPDF.setOnClickListener {
            showToast(getString(R.string.share_pdf))
            pdfSharing()
        }
    }

    private fun nextFragment() {
        @Suppress("DEPRECATION")
        fragmentManager?.beginTransaction()?.replace(R.id.container, RatingAndReviewFragment("paymentdone", 1,
                AppConstants.APP_SERVICE_RATE, data?.orders?.get(0)?.CustomOrderNumber))?.commit()
    }

    private fun setPaymentResponse() {
        arguments?.let {
            data = it.getSerializable(Keys.PAYMENT_DONE_RESPONSE) as PaymentResponseModel
            Log.e(TAG, "---DATA--: " + data.toString())
//            pdfFileName = AppConstants.PDF_PATH + "/" + data!!.orders[0].CustomOrderNumber + ".pdf"

            data!!.orders?.let { listOrderModel ->
                if (listOrderModel.size > 0) {
                    rv_order_group.layoutManager = LinearLayoutManager(activity!!)
                    rv_order_group.adapter = OrderGroupReceiptPageAdapter(activity!!, data!!.orders)
                }
                binding?.setVariable(BR.model, data)
                binding?.executePendingBindings()

                createPdfInvoice(data!!)
            }
        }
    }

    private fun createPdfInvoice(response: PaymentResponseModel) {
        try {
            PDFInvoiceCreater.getInstance(response, activity!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun pdfSharing() {
        try {
            val filePath = File(Environment.getExternalStorageDirectory().toString() + File.separator + resources.getString(R.string.kitchen_app_name_for_pdf) + File.separator)
            val fileName = filePath.toString() + "/" + data!!.orders[0].CustomOrderNumber + "-" + data!!.orders[0].CreatedOn + ".pdf"
            val file = File(fileName)

            if (file.exists()) {
                val photoUri: Uri? = if (Build.VERSION.SDK_INT >= 24)
                    activity?.let { FileProvider.getUriForFile(it, "com.mart.onestopkitchen" + ".share", file) }
                else
                    Uri.fromFile(file)

                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "application/pdf"
                shareIntent.putExtra(Intent.EXTRA_STREAM, photoUri)
                shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "1 Stop Kitchen Invoice")
                shareIntent.putExtra(Intent.EXTRA_TEXT, "This is an 1 Stop Kitchen Invoice to share with you")
                startActivity(Intent.createChooser(shareIntent, "1 Stop Kitchen Invoice"))
            } else {
                createPdfInvoice(response = data!!)
                Log.e(TAG, "-----------------file not exist for pdf-----------------")
            }
        } catch (ex: android.content.ActivityNotFoundException) {
            AlertDialog.Builder(context).setMessage("1 Stop Kitchen Invoice Sharing failed").setPositiveButton("OK"
            ) { dialog, whichButton -> }.create().show()
        }
    }
}

