package com.mart.onestopkitchen.ui.activity;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.androidhiddencamera.CameraConfig;
import com.androidhiddencamera.config.CameraFacing;
import com.androidhiddencamera.config.CameraImageFormat;
import com.androidhiddencamera.config.CameraResolution;
import com.androidhiddencamera.config.CameraRotation;
import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.barcode.BarcodeFragment;
import com.mart.onestopkitchen.baseview.AppUpdateView;
import com.mart.onestopkitchen.chat.services.LoginService;
import com.mart.onestopkitchen.chat.utils.SharedPrefsHelper;
import com.mart.onestopkitchen.contacts.UpdateAllContactsOnceSynced;
import com.mart.onestopkitchen.fcm.MyFirebaseMessagingService;
import com.mart.onestopkitchen.listener.FragmentListener;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.AppStartRequest;
import com.mart.onestopkitchen.model.AppThemeResponse;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.model.ProductService;
import com.mart.onestopkitchen.mvp.ui.cashin.NearByOk$ServiceFragment;
import com.mart.onestopkitchen.mvp.ui.ratingandreview.RatingAndReviewFragment;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.permission.PermissionListener;
import com.mart.onestopkitchen.permission.PermissionUtils;
import com.mart.onestopkitchen.products.view.ProductListFragment;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.fragment.BaseBillingAddressFragment;
import com.mart.onestopkitchen.ui.fragment.BaseFragment;
import com.mart.onestopkitchen.ui.fragment.BillingAddressFragment;
import com.mart.onestopkitchen.ui.fragment.CartFragment;
import com.mart.onestopkitchen.ui.fragment.ConfirmOrderFragment;
import com.mart.onestopkitchen.ui.fragment.CustomerInfoFragment;
import com.mart.onestopkitchen.ui.fragment.CustomerOrderDetailsFragment;
import com.mart.onestopkitchen.ui.fragment.CustomerOrdersFragment;
import com.mart.onestopkitchen.ui.fragment.HomePageFragment;
import com.mart.onestopkitchen.ui.fragment.LoginFragment;
import com.mart.onestopkitchen.ui.fragment.PaymentDoneSuccessfullyFragment;
import com.mart.onestopkitchen.ui.fragment.PaymentMethodFragment;
import com.mart.onestopkitchen.ui.fragment.PickupPointListFragment;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;
import com.mart.onestopkitchen.ui.fragment.ProductListFragmentFor3_8;
import com.mart.onestopkitchen.ui.fragment.RegisterFragment;
import com.mart.onestopkitchen.ui.fragment.SearchFragment;
import com.mart.onestopkitchen.ui.fragment.SliderListener;
import com.mart.onestopkitchen.ui.fragment.helpandsupport.HelpAndSupportFragment;
import com.mart.onestopkitchen.update.ApkDownloadWorker;
import com.mart.onestopkitchen.update.AppUpdateModel;
import com.mart.onestopkitchen.update.AppUpdatePresenter;
import com.mart.onestopkitchen.update.LiveDataHelper;
import com.mart.onestopkitchen.utils.AppConstants;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.callback.CurrentLocationCallback;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;
import com.quickblox.auth.QBAuth;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBNotificationChannel;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.users.model.QBUser;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

import static com.mart.onestopkitchen.chat.utils.ConstsKt.USER_DEFAULT_PASSWORD;
import static com.mart.onestopkitchen.service.PreferenceService.CURRENT_DATE_UPLOAD_CONTACT;
import static com.mart.onestopkitchen.service.PreferenceService.USER_MOBILE_NUMBER;
import static com.mart.onestopkitchen.ui.activity.SplashScreenActivity.MULTI_PERMISSIONS;

public class MainActivity extends BaseActivity implements View.OnClickListener, CurrentLocationCallback, FragmentManager.OnBackStackChangedListener, FragmentListener, PermissionListener, AppUpdateView {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.search_parent_layout)
    LinearLayout searchParentLayout;

    private Location mLocation = new Location("");
    private RefreshMenuListener refreshMenuListener;
    private Toolbar toolbar;
    private boolean doubleBackToExitPressedOnce = false;
    private long mBackPressed;
    private AppBarLayout.LayoutParams parms;
    private String TAG = MainActivity.class.getSimpleName();
    private SliderListener sliderListener;
    private String fcmToken = "";
    private PermissionUtils permissionUtils = null;
    private SearchFragment fragment;
    private static final int TIME_INTERVAL = 1800;

    @SuppressLint("StaticFieldLeak")
    public static MainActivity self;
    public CameraConfig cameraConfig = null;
    private AppUpdateModel appUpdateModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        AppUtils.CreateNstToken();
        self = this;
        setToolbar();
        //  Log.e(TAG, ":------MA----onCreate()--------- :");
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new HomePageFragment()).commit();
        }
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        setUp();
        resetAuthenticationToken();
        checkNotificationBundle();
        hiddenCamera();
        contactSendToServer();

        if (!preferenceService.GetPreferenceBooleanValue(PreferenceService.FIRST_RUN)) {
            FirebaseMessaging.getInstance().subscribeToTopic("all");
            FirebaseMessaging.getInstance().subscribeToTopic("android");
            preferenceService.SetPreferenceValue(PreferenceService.FIRST_RUN, true);
        }

        new Handler().postDelayed(() -> {
            new AppUpdatePresenter(MainActivity.this, this);
        }, 6000);

//            ForceUpdateChecker.Companion.with(this).onUpdateNeeded(this).check();

    }

    @Override
    protected void onStart() {
        super.onStart();
        //Log.e(TAG, ":------MA----onStart()--------- :");
        permissionUtils = PermissionUtils.Companion.getInstance().with(this).setListener(this);
        permissionUtils.requestPermission(MULTI_PERMISSIONS);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this, instanceIdResult -> {
            fcmToken = instanceIdResult.getToken();
            Log.d(TAG, "OnSuccess: Fcm token -:  " + fcmToken);
        });
//        new AppSignatureHelperr(getApplicationContext()).getAppSignatures(MainActivity.this);
    }

    private void hiddenCamera() {
        if (!preferenceService.GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE)) {
            if (cameraConfig == null) {
                cameraConfig = new CameraConfig()
                        .getBuilder(this)
                        .setCameraFacing(CameraFacing.FRONT_FACING_CAMERA)
                        .setCameraResolution(CameraResolution.MEDIUM_RESOLUTION)
                        .setImageFormat(CameraImageFormat.FORMAT_JPEG)
                        // .setCameraFocus(CameraFocus.NO_FOCUS)
                        .setImageRotation(CameraRotation.ROTATION_270)
                        .build();
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void contactSendToServer() {
        try {
            if (!PreferenceService.getInstance().GetPreferenceValue(USER_MOBILE_NUMBER).isEmpty()) {
                Calendar calendar = AppUtils.fromUserSelectDate();
                SimpleDateFormat calenderNew = new SimpleDateFormat(Keys.SIMPLE_DATE_FORMAT);
                String formattedDateNew = calenderNew.format(calendar.getTime());
                if (AppUtils.cutNull(preferenceService.GetPreferenceValue(CURRENT_DATE_UPLOAD_CONTACT)).isEmpty() || (!formattedDateNew.equalsIgnoreCase(preferenceService.GetPreferenceValue(CURRENT_DATE_UPLOAD_CONTACT)))) {
                    if (!AppUtils.isMyServiceRunning(UpdateAllContactsOnceSynced.class, this)) {
                        startService(new Intent(this, UpdateAllContactsOnceSynced.class));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onPermissionRequestResult(requestCode, permissions, grantResults);
    }

    private void checkNotificationBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int itemType = Integer.valueOf(bundle.getString(MyFirebaseMessagingService.ITEM_TYPE, "0"));
            int itemId = Integer.valueOf(bundle.getString(MyFirebaseMessagingService.ITEM_ID, "0"));

            if (itemType == MyFirebaseMessagingService.ITEM_PRODUCT) {
                ProductModel productModel = new ProductModel();
                productModel.setId(itemId);
                productModel.setName("");
                ProductDetailFragment.productModel = productModel;
                gotoFragment(new ProductDetailFragment());
            } else if (itemType == MyFirebaseMessagingService.ITEM_CATEGORY) {
                ProductService.productId = itemId;
                gotoFragment(ProductListFragmentFor3_8.newInstance("", itemId));
            }
        }
    }

    private void gotoFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    private void setToolbar() {
        toolbar = findViewById(R.id.app_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            parms = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        }
        setMarqueText();
    }

    private void setMarqueText() {
        TextView titleTextView;
        try {
            Field field = toolbar.getClass().getDeclaredField("mTitleTextView");
            field.setAccessible(true);
            titleTextView = (TextView) field.get(toolbar);
            titleTextView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            titleTextView.setFocusable(true);
            titleTextView.setFocusableInTouchMode(true);
            titleTextView.requestFocus();
            titleTextView.setSingleLine(true);
            titleTextView.setSelected(true);
            titleTextView.setMarqueeRepeatLimit(-1);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {

        }
    }

    private void resetAuthenticationToken() {
        NetworkUtil.setToken("");
        if (preferenceService.GetPreferenceIntValue(PreferenceService.APP_VERSION_CODE_KEY) != getApplicationVersionCode()) {
            preferenceService.SetPreferenceValue(PreferenceService.APP_VERSION_CODE_KEY, getApplicationVersionCode());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, false);

        }
        if (preferenceService.GetPreferenceBooleanValue(PreferenceService.LOGGED_PREFER_KEY)) {
            NetworkUtil.setToken(preferenceService.GetPreferenceValue(PreferenceService.TOKEN_KEY));
        }
    }

    @SuppressLint("PackageManagerGetSignatures")
    private int getApplicationVersionCode() {
        int versionCode = -1;
        PackageManager manager = getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(getPackageName(), -1);
            versionCode = info.versionCode;
            return versionCode;
        } catch (Exception ignored) {
        }
        return versionCode;
    }

    public void setUp() {

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment instanceof SearchFragment) {
                    ((SearchFragment) fragment).onKeyBoardHide(false);
                }
                invalidateOptionsMenu();
                syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment instanceof SearchFragment) {
                    ((SearchFragment) fragment).onKeyBoardHide(true);
                }
                refreshMenuListener.setRefresh();
                invalidateOptionsMenu();
                syncState();
                hideKeyboard();
            }
        };
        if (drawerLayout != null) {
            drawerLayout.addDrawerListener(mDrawerToggle);
        }
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        mDrawerToggle.syncState();
        toolbar.setNavigationOnClickListener(this);
//        listeningFragmentTransaction();
        makeActionOverflowMenuShown();
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void handleAppExit() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            showNormalToast(getString(R.string.back_button_click_msg));
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers();
            }
        }
        mBackPressed = System.currentTimeMillis();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        showNormalToast(getString(R.string.back_button_click_msg));
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    public void onConfigurationChanged(@NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onClick(View v) {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        if (getSupportActionBar() != null) {
            if (shouldOpenDrawer()) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                setArrowIconInDrawer();
                onBackPressed();
                shouldOpenDrawer();
            } else {
                mDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        }
    }

    public boolean shouldOpenDrawer() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            return true;
        } else {
            return false;
        }
    }

    public void closeDrawer() {
        drawerLayout.closeDrawers();
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
        setUp();
    }

    private void setAnimationOndrwerIcon() {
        ValueAnimator anim = ValueAnimator.ofFloat(1, 0);
        anim.addUpdateListener(valueAnimator -> {
            float slideOffset = (Float) valueAnimator.getAnimatedValue();
            if (null != mDrawerToggle && null != drawerLayout)
                mDrawerToggle.onDrawerSlide(drawerLayout, slideOffset);
        });
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(800);
        anim.start();
    }

    public void setArrowIconInDrawer() {
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(getNavIconResId());
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    protected int getNavIconResId() {
        return R.drawable.ic_action_nav_arrow_back;
    }

    /*public void displayHomeIcon(boolean displayHomeIcon) {
        if (getSupportActionBar() != null) {
            if (displayHomeIcon) {
                setArrowIconInDrawer();
            } else {
                setDrawerIcon();
            }
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        //Log.e(TAG, ":------MA----onResume()--------- :");
        new Handler().postDelayed(() -> {
            getMapper().enableWifiTracking();
            getMapper().enableLocationTracking(MainActivity.this);
            if (AppUtils.isReallyConnectedToInternet()) {
                initQB();
                sendRegistrationToServer();
            }
        }, 10 * 700);
        openSharedProduct();
    }

    @Override
    public void currentLocation(@NotNull Location mLocation, @Nullable Location oldLocation, double mDistanceFromFirstLocation) {
        setmLocation(mLocation);
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.LOCATIONS_LAT, String.valueOf(mLocation.getLatitude()));
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.LOCATIONS_LONG, String.valueOf(mLocation.getLongitude()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void addFragmentWithAnimation(Fragment fragment, int enterAnimationResource, int endAnimationResource, int popEnterAnimationResource,
                                         int popEndAnimationResource, Boolean stacked, Boolean addToBackStack) {

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment available = fragmentManager.findFragmentByTag(fragment.getClass().getName());
        if (stacked && available != null) {
            int stackPosition = isFragmentInBackStack(fragmentManager, fragment.getClass().getName());
            if (stackPosition != -1 && fragmentManager.getBackStackEntryCount() > stackPosition) {
                FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(stackPosition);
                fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } else {
            if (endAnimationResource != 0) {
                fragmentTransaction.setCustomAnimations(enterAnimationResource, endAnimationResource,
                        popEnterAnimationResource, popEndAnimationResource);
            }

            fragmentTransaction.add(R.id.container, fragment, fragment.getClass().getName());
            if (addToBackStack)
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void disableNavigation(boolean disabled) {
        int lockMode;
        if (disabled) {
            lockMode = DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
        } else {
            lockMode = DrawerLayout.LOCK_MODE_UNLOCKED;
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
        }
        drawerLayout.setDrawerLockMode(lockMode);
        showHideCart(true);
    }

    /**
     * This method to check whether the fragment is in back stack
     *
     * @param fragmentManager the fragment manager
     * @param fragmentTagName the fragment tag name
     * @return int Number of fragments in back stack, if no fragments are available by default it returns -1
     */
    public int isFragmentInBackStack(FragmentManager fragmentManager, String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount() - 1; entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return entry + 1;
            }
        }
        return -1;
    }

    public void hideToolBar(boolean status) {
        if (status)
            toolbar.setVisibility(View.GONE);
        else toolbar.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.rel_search, R.id.iv_scan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rel_search:
                MyApplication.getAppContext().fromSearchView = true;
                searchAction();
                break;
            case R.id.iv_scan:
                qrCodeScanner();
                break;
        }
    }

    private void qrCodeScanner() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, BarcodeFragment.getInstance(this)).addToBackStack(null).commit();
    }

    private void searchAction() {
        if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, getFrag()).addToBackStack(null).commit();
        }
    }

    private SearchFragment getFrag() {
        if (fragment == null) {
            fragment = new SearchFragment();
            return fragment;
        } else {
            fragment.clearText();
            return fragment;
        }
    }

    private void sendRegistrationToServer() {
        String token = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.FCM_TOKEN);
//        Log.e(TAG, "------------- FCM Token ::------------------------: " + token);

        AppStartRequest appStartRequest = new AppStartRequest();
        appStartRequest.setDeviceTypeId(Keys.ANRDOID);
        appStartRequest.setSubscriptionId(fcmToken);
        if (token.isEmpty() || !PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.SENT_TOKEN_TO_SERVER))
            RetroClient.getApi().initApp(appStartRequest).enqueue(new CustomCB<AppThemeResponse>());

        subscribeToPushNotifications(token);
    }

    public void onEvent(AppThemeResponse response) {
        EventBus.getDefault().post("1");
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.SENT_TOKEN_TO_SERVER, true);
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.FCM_TOKEN, fcmToken);

    }

    public void setToolBarText() {
//        String preferredLanguage = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
//        Typeface font;
//        if (preferredLanguage.equals(Language.ENGLISH)) {
//            font = Typeface.createFromAsset(getAssets(), "fonts/centurygothic.ttf");
//        } else {
//            font = Typeface.createFromAsset(getAssets(), "fonts/zawgyione.ttf");
//        }
        tvSearch.setTypeface((AppUtils.getFonts(this)), Typeface.NORMAL);
        tvSearch.setText(getString(R.string.search_products));
    }

    public void setUserName() {
        if (preferenceService.GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE))
            sliderListener.setUserName();
    }

    public void setListener(SliderListener listener) {
        this.sliderListener = listener;
    }

    public void setMenuListener(RefreshMenuListener menuListener) {
        this.refreshMenuListener = menuListener;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int resource = item.getItemId();
        if (resource == R.id.menu_cart) {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                goMenuItemFragmentifloggedIn(new CartFragment());
                goMenuItemFragment(new CartFragment());
                return true;
            }
        } else if (resource == R.id.menu_search) {
            hideToolBar(true);
            MyApplication.getAppContext().fromSearchView = false;
            searchAction();
           /* if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, getFrag()).addToBackStack(null).commit();
            }*/
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void allPermissionGranted() {
        Log.e(TAG, "----------------allPermissionGranted--------------:  ");
    }

    @Override
    public void onNeverAskAgainPermission(@NotNull String[] string) {
        AppUtils.goToSettings(this);
        Log.e(TAG, "------------------onNeverAskAgainPermission ------------ ");
    }

    @Override
    public void onDenied(@NotNull String[] permissions) {
        permissionUtils.requestPermission(MULTI_PERMISSIONS);
        Log.e(TAG, "--------------onDenied-----------------  ");
//        permissionStatus = false;
//        PermissionUtils().getInstance().getAlertDialogss(getString(R.string.permission_needed), this, permissionHelper).show();
//        new PermissionUtils().getAlertDialogss(String.valueOf(permissions),MainActivity.this);
    }

    @Override
    protected void onDestroy() {
        releaseObj();
        super.onDestroy();
    }

    private void openSharedProduct() {
        Uri uriData;
        Intent intent = getIntent();
        if (intent != null) {
            uriData = intent.getData();
            if (uriData != null) {
                String parameterValue = uriData.getQueryParameter("Pid");
                if (parameterValue != null) {
                    Log.e(TAG, ":------### UriData ParameterValue ----- :");
                    ProductModel productModel = new ProductModel();
                    productModel.setId(Long.parseLong(parameterValue));
                    productModel.setName("");
                    ProductDetailFragment.productModel = productModel;
                    new Handler().postDelayed(() -> gotoFragment(new ProductDetailFragment()), 1800);
                } else {
                    showNormalToast(getString(R.string.product_not_found));
                }
            }
        }
    }

    public Location getmLocation() {
        return mLocation;
    }

    public void setmLocation(Location mLocation) {
        this.mLocation = mLocation;
    }

    private void releaseObj() {
        if (permissionUtils != null) {
            permissionUtils = null;
        }
        if (null != cameraConfig) {
            cameraConfig = null;
        }
        if (null != self) {
            self = null;
        }
        if (null != mLocation) {
            mLocation = null;
        }
        if (null != appUpdateModel) {
            appUpdateModel = null;
        }
        System.gc();
    }

    public interface RefreshMenuListener {
        void setRefresh();
    }

    public void searchBarCodeLayout(boolean isShow) {
        if (isShow)
            searchParentLayout.setVisibility(View.VISIBLE);
        else
            searchParentLayout.setVisibility(View.GONE);
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                searchParentLayout.setVisibility(View.GONE);
                setArrowIconInDrawer();
                parms.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED);
            } else {
                parms.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
                searchParentLayout.setVisibility(View.VISIBLE);
                mDrawerToggle.setDrawerIndicatorEnabled(true);
                setAnimationOndrwerIcon();
            }
            toolbar.setLayoutParams(parms);

            if ((fragment instanceof ProductListFragment)) {
                fragment.onResume();
            } else if ((fragment instanceof LoginFragment)) {
                showHideCart(false);
            } else if (fragment instanceof HomePageFragment) {
                disableNavigation(false);
            } else if ((fragment instanceof RegisterFragment)) {
                showHideCart(false);
            } else if ((fragment instanceof BillingAddressFragment)) {
                showHideCart(false);
            } else if ((fragment instanceof PaymentDoneSuccessfullyFragment)) {
                showHideCart(false);
            } else if ((fragment instanceof CustomerInfoFragment)) {
                showHideCart(false);
            } else if ((fragment instanceof PickupPointListFragment)) {
                showHideCart(false);
            } else if (fragment instanceof BaseBillingAddressFragment) {
                showHideCart(false);
            } else if (fragment instanceof PaymentMethodFragment) {
                setTitle(getString(R.string.payment));
                showHideCart(false);
            } else if (fragment instanceof NearByOk$ServiceFragment) {
                showHideCart(false);
            } else if (fragment instanceof ConfirmOrderFragment) {
                showHideCart(false);
            } else if (fragment instanceof CustomerOrdersFragment) {
                showHideCart(true);
            } else if (fragment instanceof HelpAndSupportFragment) {
                showHideCart(false);
            } else if (fragment instanceof CustomerOrderDetailsFragment) {
                setTitle(R.string.order_details);
            } else if (fragment instanceof SearchFragment) {
                ((SearchFragment) fragment).removePopUp();
            } else {
                showHideCart(true);
            }
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers();
                return;
            }
            if (fragment instanceof HomePageFragment) {
                ((MyApplication) getApplicationContext()).alreadySelected = null;
                ((MyApplication) getApplicationContext().getApplicationContext()).rightValInt = 0;
                ((MyApplication) getApplicationContext().getApplicationContext()).leftValInt = 0;
                ((MyApplication) getApplicationContext().getApplicationContext()).alreadySelectedPrice = "0-0";
            }
            if (fragment instanceof LoginFragment && ((LoginFragment) fragment).onBackPress()) {
                ((LoginFragment) fragment).onBackPressed();
            } else if (fragment instanceof ProductDetailFragment && MyApplication.getAppContext().isSearchFragment) {
                hideToolBar(true);
                super.onBackPressed();
            } else if (fragment instanceof SearchFragment) {
                ((SearchFragment) fragment).setDeFault();
                super.onBackPressed();
            } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                handleAppExit();
            } else if (fragment instanceof RatingAndReviewFragment && ((MyApplication) getApplicationContext()).fromWhere.equalsIgnoreCase("paymentdone")) {
                ((BaseFragment) fragment).moveToHomeFragment();
            } else if (fragment instanceof PaymentDoneSuccessfullyFragment) {
                ((BaseFragment) fragment).moveToHomeFragment();
            } else if (fragment instanceof ConfirmOrderFragment) {
                if (((ConfirmOrderFragment) fragment).IsPaymentApiRunning) {
                    ((ConfirmOrderFragment) fragment).onBackPressed();
                } else {
                    super.onBackPressed();
                }
            } else {
                super.onBackPressed();
            }

        } catch (Exception e) {
            Log.e(TAG, ":-----@@@@@@ Exception in onBackPressed ### ---- :");
            e.printStackTrace();
            super.onBackPressed();
        }
    }

    /**
     * Subscribe user at quickblox server
     */

    @SuppressLint({"MissingPermission", "HardwareIds"})
    public void subscribeToPushNotifications(String registrationID) {
        QBSubscription subscription = new QBSubscription(QBNotificationChannel.GCM);
        subscription.setEnvironment(QBEnvironment.DEVELOPMENT);
        //
        String deviceId;
        final TelephonyManager mTelephony = (TelephonyManager) getSystemService(
                Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null) {
            deviceId = mTelephony.getDeviceId(); //*** use for mobiles
        } else {
            deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID); //*** use for tablets
        }
        subscription.setDeviceUdid(deviceId);
        subscription.setRegistrationID(registrationID);
        QBPushNotifications.createSubscription(subscription);
    }

    public QBUser getUserDetails() {
        QBUser qbUser = new QBUser();
        String userName = "", fullname = "";

        if (null != PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME)
                && !PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME).isEmpty()) {
            userName = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME);
        } else {
            userName = NetworkUtil.getDeviceId();
        }
        if (null != PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT) && !PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT).isEmpty()) {
            fullname = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT);
        } else {
            fullname = "OneStopMart";
        }

        StringifyArrayList tags = new StringifyArrayList<>();
        tags.add("User");
        qbUser.setLogin(userName);
        qbUser.setFullName(fullname);
        qbUser.setPassword(USER_DEFAULT_PASSWORD);
        qbUser.setTags(tags);
        return qbUser;
    }

    private void initQB() {
        if (SharedPrefsHelper.getInstance().hasQbUser()) {
            LoginService.start(MainActivity.this, SharedPrefsHelper.getInstance().getQbUser());
        }
        QBAuth.createSession(getUserDetails());
    }


    //For the app Update
    @Override
    public void appUpdateData(@NotNull AppUpdateModel data) {
        appUpdateModel = data;
        if (null != data.getAppLatestVersionName()) {
            if ((data.getAppLatestVersionName()).compareTo(getAppVersionName(MainActivity.this)) > 0) {
                if (data.isUpdateRequired()) {
                    AppConstants.APK_URL = data.getAppURL();

                    if (data.isUpdateForceRequired()) {
                        File apkFolder = AppConstants.APkFilePath;
                        if (!apkFolder.exists()) {
                            Log.e(TAG, "----- apk folder path not found--> ");
                            startWork();
                            return;
                        }
                        File file = new File(apkFolder, AppConstants.LATEST_APK_NAME);
                        if (!file.exists()) {
                            Log.e(TAG, "----- apk file not downloaded --> ");
                            startWork();
                        } else {
                            Log.e(TAG, "----- apk file already downloaded to install --> ");
                            openUpdateDialog(data, false);
                        }
                    } else {
                        openUpdateDialog(data, true);
                    }
                }
            }
        }
    }

    private void openUpdateDialog(AppUpdateModel data, boolean isPlaStore) {
        Log.e(TAG, "------ Force Update -: " + data.isUpdateForceRequired());
        AppDialogs.dialogForceUpdate(MainActivity.this, data, () -> {
            if (isPlaStore) {
                if (null != data.getPlayStoreUrl())
                    redirectStore(data.getPlayStoreUrl());
            } else {
                installLatestApk();
            }
        });
    }

    @Override
    public void error() {

    }

    private void startWork() {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresStorageNotLow(true)
                .build();
        OneTimeWorkRequest oneTimeWorkRequest = new OneTimeWorkRequest.Builder(ApkDownloadWorker.class)
                .setConstraints(constraints).build();

        WorkManager.getInstance().enqueue(oneTimeWorkRequest);
        LiveDataHelper.getInstance().observePercentage().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                Log.e(TAG, "-------------%---------: " + integer + "/100");
            }
        });

        WorkManager.getInstance().getWorkInfoByIdLiveData(oneTimeWorkRequest.getId()).observe(this, new Observer<WorkInfo>() {
            @Override
            public void onChanged(WorkInfo workInfo) {
                Log.e(TAG, "-------------%----onChanged-----: ");
                if (null != workInfo) {
                    if (workInfo.getState().isFinished()) {
                        if (null != appUpdateModel)
                            openUpdateDialog(appUpdateModel, false);
                    }
                }
            }
        });
    }

    private void installLatestApk() {
        try {
            File apkFolder = AppConstants.APkFilePath;
            if (!apkFolder.exists()) {
                return;
            }

            File file = new File(apkFolder, AppConstants.LATEST_APK_NAME);

            if (!file.exists()) {
                showNormalToast("file is not exist!");
                return;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri apkUri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".share", file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                MainActivity.this.startActivity(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri apkUri = Uri.fromFile(file);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                MainActivity.this.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
    }

    private void redirectStore(String urlApp) {
        String packageName = urlApp.split("id\\=")[1];
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + packageName));
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }
}
