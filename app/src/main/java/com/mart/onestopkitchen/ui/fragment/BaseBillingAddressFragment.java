package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.AvailableCity;
import com.mart.onestopkitchen.model.AvailableState;
import com.mart.onestopkitchen.model.BillingAddress;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.postrequest.ValuePost;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.networking.response.BillingAddressSaveResponse;
import com.mart.onestopkitchen.networking.response.CityListResponse;
import com.mart.onestopkitchen.networking.response.StateListResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.views.FormViews;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.CustomEdittext;
import com.mart.onestopkitchen.utils.GenericTextWatcherUtil;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;
import com.mart.onestopkitchen.utils.mobileNumberValidation.MobileNumberValidation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;

/**
 * Updated by Akash garg on 03/12/2018.
 */

public class BaseBillingAddressFragment extends BaseFragment {
    public static StateCityListener stateCityListener;
    public List<String> ropeColorList = Arrays.asList("Black", "Red", "Blue", "Green", "Yellow");
    public String countryCode = "60", StateProvinceCode = "", CityCode = "";
    private String cityName = "", stateName = "";

    @BindView(R.id.et_firstName)
    EditText fullName;
    @BindView(R.id.et_address1)
    EditText addressOne;
    @BindView(R.id.txt_country_code)
    TextView tvCountryCode;
    @BindView(R.id.et_address2)
    EditText addressTwo;
    @BindView(R.id.spinner_state)
    AppCompatSpinner stateSpinner;
    @BindView(R.id.et_phone_number)
    CustomEdittext phoneNumber;
    @BindView(R.id.ll_adddressParentLayout)
    LinearLayout adddressParentLinearLayout;
    @BindView(R.id.lin_ropes)
    LinearLayout linRopes;
    @BindView(R.id.spinner_city)
    AppCompatSpinner citySpinner;
    @BindView(R.id.spinner_rope)
    AppCompatSpinner spinnerRope;
    @BindView(R.id.et_house_number)
    TextInputEditText etHouseNumber;
    @BindView(R.id.et_floor_number)
    TextInputEditText etFloorNumber;
    @BindView(R.id.et_room_number)
    TextInputEditText etRoomNumber;
    @BindView(R.id.radio_yes)
    RadioButton rbYes;
    @BindView(R.id.radio_no)
    RadioButton rbNo;
    @BindView(R.id.rg_lift_option)
    RadioGroup rg_lift_option;
    @BindView(R.id.tvFullName)
    TextView tvFullName;
    @BindView(R.id.tvAddressOne)
    TextView tvAddressOne;
    @BindView(R.id.tvAddressTwo)
    TextView tvAddressTwo;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    @BindView(R.id.tv_PhoneNumber)
    TextView tv_PhoneNumber;
    @BindView(R.id.tv_stateDivision)
    TextView tv_stateDivision;
    @BindView(R.id.tv_Township)
    TextView tv_Township;
    long addressID = 0;
    boolean IsComeFromCart = false;
    private String selectedColor = "";
    private String keyPrefixTag;
    private MobileNumberValidation mobileNumberValidation;
    private int minimumLength = 0, maximumLength = 0;
    private boolean isValidNumber = false;
    private View view = null;
    private String TAG = BaseBillingAddressFragment.class.getSimpleName();

    public static void setStateCityListener(StateCityListener stateCityListener) {
        BaseBillingAddressFragment.stateCityListener = stateCityListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_billing_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.add_address));
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.add_address));
        setTagName();
        loadColorSpinner();
        setRequiredFieldWithTextView(Arrays.asList(tvFullName, tvAddressOne, tv_PhoneNumber, tv_stateDivision, tv_Township));
        mobileNumberListenerPhone();
    }

    @SuppressLint("SetTextI18n")
    void mobileNumberListener(String number) {
        number = AppUtils.cutNull(number);
        String ary[] = AppUtils.formattedNumberWithCountryCode(number);
        tvCountryCode.setText(ary[0]);
        if (phoneNumber != null)
            phoneNumber.setText(ary[1]);
        mobileNumberValidation = MobileNumberValidation.getInstance(getActivity(), ary[0]);
        phoneNumber.setSelection(phoneNumber.getText().toString().length());
        mobileNumberListenerPhone();
    }

    private void mobileNumberListenerPhone() {
        mobileNumberValid();
        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().startsWith("09")) {
                    phoneNumber.setText(AppUtils.getMyanmarNumber());
                    phoneNumber.setSelection(BaseBillingAddressFragment.this.phoneNumber.length());
                }
                int length = s.toString().length();
                if (length > 3 && null != mobileNumberValidation) {
                    mobileNumberValidation.validateMobileNumber(true, "95", s.toString(), new MobileNumberValidation.MobileValidationListener() {
                        @Override
                        public void onSetValidationLimit(boolean validNumber, int minLength, int maxLength, String operator, String operatorColor) {
                            if (validNumber) {
                                minimumLength = minLength;
                                maximumLength = maxLength;
                                setEditTextMaxLength(BaseBillingAddressFragment.this.phoneNumber, maximumLength);
                            } else {
                                isValidNumber = false;
                                phoneNumber.setText(AppUtils.getMyanmarNumber());
                                Toast.makeText(getContext(), getString(R.string.invalid_mobile_number), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onSelectedOtherCountry(boolean error) {
                        }
                    });

                    if (length >= minimumLength && length <= maximumLength && maximumLength == minimumLength) {
                        phoneNumber.clearFocus();
                        addressOne.clearFocus();
                        addressTwo.clearFocus();
                        isValidNumber = true;
                        removePhoneKeypad();
                    } else {
                      /*  if (maximumLength != minimumLength && length >= maximumLength) {
                            addressOne.clearFocus();
                            addressTwo.clearFocus();
                            phoneNumber.clearFocus();
                            isValidNumber = true;
                        }*/
                        if (maximumLength != minimumLength && length >= minimumLength) {
                            //  addressOne.clearFocus();
                            //  addressTwo.clearFocus();
                            //  phoneNumber.clearFocus();
                            isValidNumber = true;
                        } else {
                            isValidNumber = false;
                            phoneNumber.requestFocus();
                        }
                    }
                } else {
                    addressOne.clearFocus();
                    addressTwo.clearFocus();
                    isValidNumber = false;
                    phoneNumber.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().startsWith("090") || s.toString().startsWith("091")) {
                    BaseBillingAddressFragment.this.phoneNumber.setText(AppUtils.getMyanmarNumber());
                    BaseBillingAddressFragment.this.phoneNumber.setSelection(BaseBillingAddressFragment.this.phoneNumber.length());
                }
            }
        });
        this.phoneNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
    }

    private void removePhoneKeypad() {
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        IBinder binder = view.getWindowToken();
        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(binder, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void mobileNumberValid() {
        if (null != mobileNumberValidation) {
            mobileNumberValidation.validateMobileNumber(true, "95", phoneNumber.getText().toString(), new MobileNumberValidation.MobileValidationListener() {
                @Override
                public void onSetValidationLimit(boolean validNumber, int minLength, int maxLength, String operator, String operatorColor) {
                    if (validNumber) {
                        minimumLength = minLength;
                        maximumLength = maxLength;
                        setEditTextMaxLength(BaseBillingAddressFragment.this.phoneNumber, maximumLength);
                    } else {
                        isValidNumber = false;
                        BaseBillingAddressFragment.this.phoneNumber.setText(AppUtils.getMyanmarNumber());
                        Toast.makeText(getContext(), getString(R.string.invalid_mobile_number), Toast.LENGTH_SHORT).show();
                    }
                    if (phoneNumber.getText().toString().length() > 3) {
                        if (phoneNumber.getText().toString().length() >= minimumLength && phoneNumber.getText().toString().length() <= maximumLength) {
                            isValidNumber = true;
                            phoneNumber.clearFocus();
                        }
                    }
                }

                @Override
                public void onSelectedOtherCountry(boolean error) {

                }
            });
        }
    }

    private void setEditTextMaxLength(CustomEdittext editText, int length) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                String character = String.valueOf(source.charAt(i));
                if (character.equalsIgnoreCase("/") || character.equalsIgnoreCase("%") || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("&") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";") || character.equalsIgnoreCase("<") || character.equalsIgnoreCase(">") || character.equalsIgnoreCase("?") || character.equalsIgnoreCase("[") || character.equalsIgnoreCase("]") || character.equalsIgnoreCase("\"") || character.equalsIgnoreCase("^") || character.equalsIgnoreCase("_") || character.equalsIgnoreCase("|") || character.equalsIgnoreCase("~") || character.equalsIgnoreCase("@") || character.equalsIgnoreCase("-")) {
                    return "";
                }
            }
            return null;
        };
        InputFilter[] fArray = new InputFilter[2];
        fArray[0] = new InputFilter.LengthFilter(length);
        fArray[1] = filter;
        editText.setFilters(fArray);
    }

    private void loadColorSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), R.layout.simple_spinner_item_black_color, ropeColorList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRope.setAdapter(adapter);
        spinnerRope.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedColor = ropeColorList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                if (ropeColorList != null)
                    selectedColor = ropeColorList.get(0);
            }
        });
    }

    private void setTagName() {
        keyPrefixTag = "BillingNewAddress.";
    }

    void callBillingAddressApi() {
        RetroClient.getApi().getBillingAddress().enqueue(new CustomCB<BillingAddressResponse>(this.getView()));
    }

    public void onEvent(BillingAddressResponse billingAddressResponse) {
        if (null != billingAddressResponse) {
            if (null != billingAddressResponse.getNewAddress() && billingAddressResponse.getNewAddress().getAvailableCountries().size() > 0)
                countryCode = billingAddressResponse.getNewAddress().getAvailableCountries().get(1).getValue();
            callStateRetrievalResponseApi();
            setValueinFormField(billingAddressResponse.getNewAddress());
        }
    }

    void callStateRetrievalResponseApi() {
        RetroClient.getApi().getStates(countryCode).enqueue(new CustomCB<StateListResponse>(this.getView()));
    }

    public void onEvent(final StateListResponse stateListResponse) {
        setAdapter(stateSpinner, getStateList(stateListResponse.getData()));
        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    StateProvinceCode = String.valueOf(stateListResponse.getData().get(position - 1).getId());
                    stateName = stateListResponse.getData().get(position - 1).getName();
                } else {
                    StateProvinceCode = "0";
                    stateName = "yangon";
                }
                RetroClient.getApi().getCityList(StateProvinceCode).enqueue(new CustomCB<CityListResponse>(getView()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (null != stateCityListener)
            stateCityListener.getStateAll(getStateList(stateListResponse.getData()));
    }

    private void setAdapter(Spinner spinner, List<String> stringList) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), R.layout.simple_spinner_item_black_color, stringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private List<String> getStateList(List<AvailableState> states) {
        List<String> stateList = new ArrayList<>();
        stateList.add(getString(R.string.select_state_division));
        for (AvailableState availableState : states)
            stateList.add(availableState.getName());
        return stateList;
    }

    public void onEvent(final CityListResponse cityListResponse) {
        setAdapter(citySpinner, getCityList(cityListResponse.getData()));
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    if (!StateProvinceCode.equals("0")) {
                        CityCode = String.valueOf(cityListResponse.getData().get(position - 1).getId());
                        cityName = cityListResponse.getData().get(position - 1).getName();
                        if (CityCode.equalsIgnoreCase("0")) {
                            Snackbar.make(Objects.requireNonNull(getView()), R.string.select_another_state_division_first, Snackbar.LENGTH_SHORT).show();
                            CityCode = "";
                            stateSpinner.requestFocus();
                            return;
                        }
                    } else {
                        Snackbar.make(Objects.requireNonNull(getView()), R.string.select_state_division_first, Snackbar.LENGTH_SHORT).show();
                        CityCode = "";
                        stateSpinner.requestFocus();
                        if (null != cityListResponse.getData())
                            setAdapter(citySpinner, getCityList(cityListResponse.getData()));
                    }
                } else {
                    CityCode = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (null != stateCityListener)
            stateCityListener.getCityAll(getCityList(cityListResponse.getData()));
    }

    private List<String> getCityList(List<AvailableCity> citis) {
        List<String> cityList = new ArrayList<>();
        cityList.add(getString(R.string.select_township));
        for (AvailableCity availableState : citis)
            cityList.add(availableState.getName());
        return cityList;
    }

    @OnClick(R.id.btn_continue)
    void onBtnContinue() {
        validateForm();
    }

    private void validateForm() {
        boolean isValid;
        FormViews.view = getView();
        FormViews.isFormValid = true;
        FormViews.isValidWithMark(R.id.et_firstName, getString(R.string.fullname));
        FormViews.isValidWithMark(R.id.et_phone_number, getString(R.string.phone_number));
        FormViews.isValidWithMark(R.id.et_address1, getString(R.string.street_address));
        isValid = FormViews.isFormValid;

        if (Objects.requireNonNull(fullName.getText().toString().trim().length() < 3)) {
            fullName.requestFocus();
            setMinimumLength(getString(R.string.enter_full_name));
            isValid = false;
        } else if (Objects.requireNonNull(addressOne.getText()).toString().trim().isEmpty()) {
            addressOne.requestFocus();
            Snackbar.make(Objects.requireNonNull(getView()), R.string.enter_address, Snackbar.LENGTH_SHORT).show();
            isValid = false;
        } else if (Objects.requireNonNull(addressOne.getText()).toString().trim().length() > 0 && addressOne.getText().toString().trim().length() <= 3) {
            setMinimumLength(getString(R.string.enter_full_addressone));
            isValid = false;
        } else if (StateProvinceCode.isEmpty() || StateProvinceCode.equalsIgnoreCase("0")) {
            Snackbar.make(Objects.requireNonNull(getView()), R.string.select_state_division, Snackbar.LENGTH_SHORT).show();
            isValid = false;
        } else if (CityCode.isEmpty()) {
            Snackbar.make(Objects.requireNonNull(getView()), R.string.select_city, Snackbar.LENGTH_SHORT).show();
            isValid = false;
        } else if (!isValidNumber) {
            setMinimumLength(getString(R.string.please_enter_valid_mobile_number));
            isValid = false;
        }
        if (isValid) {
            btnContinue.setOnClickListener(null);
            String house = AppUtils.cutNull(Objects.requireNonNull(etHouseNumber.getText()).toString());
            String floor = AppUtils.cutNull(Objects.requireNonNull(etFloorNumber.getText()).toString());
            String room = AppUtils.cutNull(Objects.requireNonNull(etRoomNumber.getText()).toString());
            String ropeColor = AppUtils.cutNull(selectedColor);

            List<KeyValuePair> keyValuePairs = FormViews.getForMFieldValue(adddressParentLinearLayout, keyPrefixTag);
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "CountryId", countryCode));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "StateProvinceId", StateProvinceCode));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "CityId", CityCode));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "Id", String.valueOf(addressID)));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "Email", ""));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "LastName", ","));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "HouseNo", house));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "FloorNo", floor));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "RoomNo", room));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "RopeColor", ropeColor));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "IsLiftOption", String.valueOf(rbYes.isChecked())));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "PhoneNumber", "0095" + phoneNumber.getText().toString().substring(1)));
            String address = addressOne.getText().toString().trim() + "," + addressTwo.getText().toString().trim() + "," + cityName + "," + stateName;
            Log.e(TAG, "::---------Current Address---------: " + address);
            LatLng latlng = getLatLng(address);
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "Longitude", String.valueOf(latlng.longitude)));
            keyValuePairs.add(new KeyValuePair(keyPrefixTag + "Latitude", String.valueOf(latlng.latitude)));
            Log.e(TAG, "::----------Lat Lng-------: " + "lat: " + latlng.latitude + ": lng : " + latlng.longitude);

            callSaveAddressByFormApi(keyValuePairs);


            /*    List<WifiModel> wifiList = ((MainActivity) Objects.requireNonNull(getActivity())).getMapper().getMTracker().getWifiScanResult();
                      for (WifiModel wifi : wifiList) {
                        Log.e(TAG, "::--------------WIFI BSSID-------: " + wifi.getBssid());
                     }
            */
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        rg_lift_option.setOnCheckedChangeListener((radioGroup, checkedId) -> {
            switch (checkedId) {
                case R.id.radio_yes:
                    linRopes.setVisibility(GONE);
                    break;

                case R.id.radio_no:
                    linRopes.setVisibility(View.VISIBLE);
                    break;
            }
        });

        phoneNumber.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateForm();
                return true;
            }
            return false;
        });

        try {
            fullName.addTextChangedListener(new GenericTextWatcherUtil(fullName));
            addressOne.addTextChangedListener(new GenericTextWatcherUtil(addressOne));
            addressTwo.addTextChangedListener(new GenericTextWatcherUtil(addressTwo));
        } catch (Exception ignored) {
        }
    }

    private void setMinimumLength(String msg) {
        Snackbar.make(Objects.requireNonNull(getView()), msg, Snackbar.LENGTH_SHORT).show();
    }

    private void callSaveAddressByFormApi(List<KeyValuePair> keyValuePairs) {
        RetroClient.getApi().saveBillingAddress(keyValuePairs).enqueue(new CustomCB<BillingAddressSaveResponse>(getView()));
    }

    private void callSaveAddressFromAddressApi() {
        RetroClient.getApi().saveBillingAddressFromAddress(new ValuePost("" + addressID)).enqueue(new CustomCB<BillingAddressSaveResponse>(this.getView()));
    }

    public void onEvent(BillingAddressSaveResponse billingAddressSaveResponse) {
        if (billingAddressSaveResponse.isData() && billingAddressSaveResponse.getStatusCode() == 200) {
            preferenceService.SetPreferenceValue(PreferenceService.ADDRESS_DONE, true);
            preferenceService.SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0");
            preferenceService.SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, "");
            if (getFragmentManager() != null) {
                if (addressID != 0)
                    showUnDeliveryPopup(getString(R.string.update_address_succefully));
                else
                    showUnDeliveryPopup(getString(R.string.add_address_succefully));
            }
        }
    }

    private void showUnDeliveryPopup(String msg) {
        AppDialogs.customDialog(getActivity(), msg, () -> {
            if (getFragmentManager() != null) {
                if (IsComeFromCart) {
                    getFragmentManager().popBackStack();
                } else {
                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                            .replace(R.id.container, new CartFragment()).commit();
                }
            }
        });
    }

    private void setValueinFormField(BillingAddress billingAddress) {
        if (null != billingAddress) {
            FormViews.setText(R.id.et_firstName, billingAddress.getFirstName(), this.getView());
            mobileNumberListener(billingAddress.getPhoneNumber());
        } else {
            Log.e(TAG, "--MB--> " + PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_MOBILE_NUMBER));
            mobileNumberListener(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_MOBILE_NUMBER));
        }
    }

    public void onEvent(BillingAddress existingAddress) {
        adddressParentLinearLayout.setVisibility(GONE);
        addressID = existingAddress.getId();
        callSaveAddressFromAddressApi();
    }

    private LatLng getLatLng(String myLocation) {
        try {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocationName(myLocation, 2);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                double longitude = address.getLongitude();
                double latitude = address.getLatitude();
                return new LatLng(latitude, longitude);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
        return new LatLng(0.0, 0.0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != mobileNumberValidation) {
            mobileNumberValidation = null;
        }
    }

    interface StateCityListener {
        void getStateAll(List<String> states);

        void getCityAll(List<String> cities);
    }
}
