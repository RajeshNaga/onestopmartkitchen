package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.listener.DialogBtnClickListener;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.CartProductListResponse;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.fragment.CartFragment;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Updated by AkashGarg on 10/20/2018.
 */
public class CartAdapter extends RecyclerSwipeAdapter {
    public List<CartProduct> products;
    protected Context context;
    OnItemClickListener mItemClickListener;
    Fragment fragment;
    ProductSummaryHolder holder = null;
    CartProduct productModel = null;

    public CartAdapter(Context context, List productsList, Fragment fragment, PreferenceService preferenceService) {
        try {
            this.products = new ArrayList<>(0);
            this.products.addAll(productsList);
            this.context = context;
            this.fragment = fragment;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout;
        layout = R.layout.item_cart_list;
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new ProductSummaryHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return products.size();
    }

    private void setTouchListener(final ProductSummaryHolder holder, final CartProduct productModel) {
        if (productModel != null) {
            final TextView editText = holder.productQuantity;

            if (productModel.getQuantity() == 1) {
                holder.qunatityDownImageView.getDrawable().mutate().setAlpha(70);
                holder.qunatityDownImageView.invalidate();
            }

            holder.qunatityUpImageView.setOnClickListener(v -> updateQuantity(1, editText, productModel));

            holder.qunatityDownImageView.setOnClickListener(v -> updateQuantity(-1, editText, productModel));
        }
    }

    private void updateQuantity(int quantity, TextView textView, CartProduct productModel) {
        int PreviousQuantity = Integer.parseInt(textView.getText().toString());
        int totalQuantity = PreviousQuantity + quantity;
        if (totalQuantity > 0) {
            updateCartItem("itemquantity" + productModel.getId(), "" + totalQuantity);
        }
    }

    private void updateCartItem(String key, String value) {
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        KeyValuePair keyValuePair = new KeyValuePair();
        keyValuePair.setKey(key);
        keyValuePair.setValue(value);
        keyValuePairs.add(keyValuePair);
        RetroClient.getApi().updateCartProductList(keyValuePairs).enqueue(new CustomCB<CartProductListResponse>(fragment.getView()));
    }

    private void addWishList(CartProduct cartProduct) {
        CartFragment.addToWishList(cartProduct, fragment);
        Toast.makeText(context, context.getString(R.string.moved_to_wishlist), Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder bindViewHolder, final int position) {
        try {
            String productAttr = "";
            if (bindViewHolder instanceof ProductSummaryHolder) {
                productModel = products.get(position);
                holder = (ProductSummaryHolder) bindViewHolder;
                holder.productName.setText(productModel.getProductName());
                holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getUnitPrice(), 0));
                holder.productShortdescription.setVisibility(View.GONE);
                holder.productQuantity.setText(String.valueOf(productModel.getQuantity()));

                // actually we are getting oldprice from price key because this complicated thing coming from server side so we have to do like this
                if ((null != productModel.getDiscountPercentage() && Integer.parseInt(productModel.getDiscountPercentage()) == 0)
                        && (null != productModel.getOldPrice() && productModel.getOldPrice().startsWith("0"))) {
                    holder.tvOldPrice.setVisibility(View.GONE);
                    holder.tvOffer.setVisibility(View.GONE);
                } else if ((null != productModel.getDiscountPercentage() && Integer.parseInt(productModel.getDiscountPercentage()) > 0)) {
                    holder.tvOldPrice.setVisibility(View.VISIBLE);
                    holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOldPrice.setText(AppUtils.getMMKString(holder.tvOldPrice.getTextSize(), productModel.getPrice(), 0));
                    holder.tvOffer.setVisibility(View.VISIBLE);
                    holder.tvOffer.setText(productModel.getDiscountPercentage() + "%");
                } else {
                    holder.tvOldPrice.setVisibility(View.VISIBLE);
                    holder.tvOffer.setVisibility(View.GONE);
                    holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOldPrice.setText(AppUtils.getMMKString(holder.tvOldPrice.getTextSize(), productModel.getOldPrice(), 0));
                }

                Glide
                        .with(context)
                        .load(productModel.getPicture().getImageUrl())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                        .into(holder.productImage);


                onRemoveClicked(holder.removeItem, position);
                onSaveLaterClick(holder.tvAddToWishLish, position);

                setTouchListener(holder, productModel);


                holder.tvDeliveryDate.setText((!productModel.getDeliveryDays().isEmpty()) ? ((context.getString(R.string.delivered_on) + ":" + productModel.getDeliveryDays())) : "");
                StringBuilder attributes = new StringBuilder();
                productAttr = productModel.getAttributeInfo();
                if (null != productModel && productModel.getAttributeInfo().contains("&quot;")) {
                    productAttr = productModel.getAttributeInfo().replace("&quot;", " ");
                }
                String[] attr = productAttr.split("<br />");

                for (int i = 0; i < attr.length; i++) {
                    if (i == attr.length - 1) {
                        attributes.append(attr[i]).append("");
                    } else attributes.append(attr[i]).append("\n");
                }

                if (!attributes.toString().trim().isEmpty()) {
                    holder.productsAttributes.setVisibility(View.VISIBLE);
                    holder.productsAttributes.setText(attributes.toString());
                } else {
                    holder.productsAttributes.setVisibility(View.INVISIBLE);
                }
                if (products.get(position).getQuantity() > products.get(position).getAvailableQuantity() || products.get(position).getAllowedQuantities().size() > 0) {
                    holder.tvStockMsg.setVisibility(View.VISIBLE);
                    if ((products.get(position).getWarnings() != null && products.get(position).getWarnings().size() > 0)) {
                        holder.tvStockMsg.setText(products.get(position).getWarnings().get(0).toString());
                    } else {
                        holder.tvStockMsg.setVisibility(View.GONE);
                    }
                } else {
                    if ((products.get(position).getWarnings() != null && products.get(position).getWarnings().size() > 0)) {
                        holder.tvStockMsg.setVisibility(View.VISIBLE);
                        holder.tvStockMsg.setText(products.get(position).getWarnings().get(0).toString());
                    } else
                        holder.tvStockMsg.setVisibility(View.GONE);
                }

            }
        } catch (ClassCastException ignored) {
        }
    }

    private void onSaveLaterClick(TextView tvAddToWishLish, final int position) {
        tvAddToWishLish.setOnClickListener(view -> {
            addWishList(products.get(position));
            //clickListener.onItemClick(products.get(position));
        });
    }

    private void onRemoveClicked(TextView itemView, final int position) {
        itemView.setOnClickListener(view ->
                AppDialogs.customDialogWithCancel(context, context.getResources().getString(R.string.remove_item_cart), new DialogBtnClickListener() {
                    @Override
                    public void yesBtnClick(Dialog dialog) {
                        updateCartItem("removefromcart", "" + products.get(position).getId());
                        dialog.dismiss();
                    }

                    @Override
                    public void cancelBtnClick() {
                    }
                }));
    }

    @Override
    public int getItemCount() {
        if (products == null)
            return 0;
        return products.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipe;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private void disableTextBoxSelection(TextView productQuantity) {
        productQuantity.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface OnSaveItemClickListener {
        void onItemClick(CartProduct products);
    }

    public class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvDeliveryDate)
        TextView tvDeliveryDate;
        @BindView(R.id.img_productImage)
        ImageView productImage;
        @BindView(R.id.tv_productPrice)
        TextView productPrice;
        @BindView(R.id.tv_productName)
        TextView productName;
        @BindView(R.id.tv_product_short_descrption)
        TextView productShortdescription;
        @BindView(R.id.et_quantity)
        TextView productQuantity;
        @BindView(R.id.tv_remove)
        TextView removeItem;
        @BindView(R.id.tv_add_to_wishlish)
        TextView tvAddToWishLish;
        @BindView(R.id.iv_up)
        ImageView qunatityUpImageView;
        @BindView(R.id.iv_down)
        ImageView qunatityDownImageView;
        @BindView(R.id.tvStockMsg)
        TextView tvStockMsg;
        @BindView(R.id.tvAttributes)
        TextView productsAttributes;

        @BindView(R.id.tvOldPrice)
        TextView tvOldPrice;
        @BindView(R.id.tvOffer)
        TextView tvOffer;

        public ProductSummaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            removeItem.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}


