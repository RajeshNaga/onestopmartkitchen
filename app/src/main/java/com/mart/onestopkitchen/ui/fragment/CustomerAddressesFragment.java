package com.mart.onestopkitchen.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.event.EditAddressEvent;
import com.mart.onestopkitchen.event.RemoveAddressEvent;
import com.mart.onestopkitchen.model.CustomerAddress;
import com.mart.onestopkitchen.model.CustomerAddressResponse;
import com.mart.onestopkitchen.model.RemoveCustomerAddressResponse;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.ui.adapter.CustomerAddressAdapter;
import com.mart.onestopkitchen.ui.fragment.addaddres.CustomerAddAddressFragmentNew;
import com.mart.onestopkitchen.utils.Language;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by mart-110 on 12/15/2015.
 */
public class CustomerAddressesFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.btn_add)
    Button addAddressButton;

    @BindView(R.id.recycler_view_address)
    RecyclerView mRecyclerView;

    @BindView(R.id.txt_no_record)
    TextView txtNoRecord;

    private ArrayList<CustomerAddress> customerAddresses;
    private CustomerAddressAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_addresses, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        getActivity().setTitle(getString(R.string.addresses));
        addAddressButton.setOnClickListener(this);

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        customerAddresses = new ArrayList<>();
        mAdapter = new CustomerAddressAdapter(getActivity(), customerAddresses, preferenceService);
        mRecyclerView.setAdapter(mAdapter);
        txtNoRecord.setVisibility(View.GONE);
        callGetAddressesWebservice();
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ARABIC)) {
            mRecyclerView.setRotationY(180);
        }
    }

    public void callGetAddressesWebservice() {
        RetroClient.getApi().getCustomerAddresses().enqueue(new CustomCB<CustomerAddressResponse>(this.getView()));
    }

    public void callRemoveAddressWebservice(int addressId) {
        RetroClient.getApi().removeCustomerAddresses(addressId)
                .enqueue(new CustomCB<RemoveCustomerAddressResponse>(this.getView()));
    }

    public void onEvent(CustomerAddressResponse response) {
        if (response.getStatusCode() == 200) {
            txtNoRecord.setVisibility(View.GONE);
            //Log.d("ADDRESSES", String.valueOf(response.getExistingAddresses()));
            customerAddresses.clear();
            customerAddresses.addAll(response.getExistingAddresses());
            mAdapter.notifyDataSetChanged();

            if (customerAddresses.size() == 0) {
                txtNoRecord.setVisibility(View.VISIBLE);
                txtNoRecord.setText(getString(R.string.no_address_found));
                //showSnack(getString(R.string.no_address_found));
            }
        }
    }

    public void onEvent(RemoveCustomerAddressResponse response) {
        String message = "";
        if (response.getStatusCode() == 200) {
            message = getString(R.string.address_remove_succssfully);
        } else {
            message = getString(R.string.error_removing_address);
        }

        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void onEvent(EditAddressEvent event) {

        Fragment editCustomerFragment = new CustomerAddAddressFragment();
        Bundle args = new Bundle();
        args.putInt("index", event.getIndex() + 1);
        args.putString("addressJson", new Gson().toJson(event.getAddress()));
        editCustomerFragment.setArguments(args);

        getFragmentManager().beginTransaction()
//                .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog,R.anim.fade_in_dialog,R.anim.fade_out_dialog)
                .replace(R.id.container,
                        editCustomerFragment)
                .addToBackStack(null).commit();
    }

    public void onEvent(RemoveAddressEvent event) {
        showRemoveAddressConfirmationDialog(event.getAddress());
    }

    private void showRemoveAddressConfirmationDialog(final CustomerAddress address) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.are_you_sure_delete_address)
                .setTitle(R.string.delete_address);

        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mAdapter.remove(address);
                callRemoveAddressWebservice(Integer.parseInt(address.getId()));
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_add) {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fade_in_dialog, R.anim.fade_out_dialog, R.anim.fade_in_dialog, R.anim.fade_out_dialog)
                    .replace(R.id.container,
                            new CustomerAddAddressFragmentNew())
                    .addToBackStack(null).commit();
        }
    }
}
