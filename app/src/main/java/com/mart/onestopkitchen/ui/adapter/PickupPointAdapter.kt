package com.mart.onestopkitchen.ui.adapter

import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mart.onestopkitchen.BR
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.model.PickupPointItem
import com.mart.onestopkitchen.networking.response.PickupPointAddressResponse
import com.mart.onestopkitchen.presenter.PickupPointSelectedPresenter
import com.mart.onestopkitchen.service.PreferenceService

/**
 * Created by Akash Garg
 */
class PickupPointAdapter(var mContext: Context, private var pickupList: PickupPointAddressResponse, val listener: SelectedPickupPoint)
    : RecyclerView.Adapter<PickupPointAdapter.PickupPointHolder>() {

    private var inflater: LayoutInflater? = null

    init {
        inflater = LayoutInflater.from(mContext)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PickupPointHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater!!, R.layout.layout_pickup_point_adapter, parent, false)
        return PickupPointHolder(binding)
    }

    override fun getItemCount(): Int {
        return pickupList.pickupPoints?.size!!
    }

    override fun onBindViewHolder(holder: PickupPointHolder, position: Int) {
        pickupList.pickupPoints?.get(position)?.let { holder.bind(it) }
    }

    inner class PickupPointHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: PickupPointItem) {
            binding.setVariable(BR.ItemListener, OnListItemClick())
            binding.setVariable(BR.pickupPointItem, data)
            binding.executePendingBindings()
        }
    }


    open inner class OnListItemClick : PickupPointSelectedPresenter.SavePickUpPoint {
        private var pickupPointAddress: PickupPointItem? = null
        fun onListItemClicked(pickupPointAddress: PickupPointItem) {
            this.pickupPointAddress = pickupPointAddress
            listener.showLoader()
            PickupPointSelectedPresenter(mContext, pickupPointAddress.id.toString(), this)
        }

        override fun savedPickupPoint(isSave: Boolean) {
            listener.hideLoader()
            if (isSave) {

                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_NAME, pickupPointAddress?.name)
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, pickupPointAddress?.description)
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, pickupPointAddress?.id.toString())
                listener.onSelectedPickupPoint()
            } else {
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, "")
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0")
                listener.onSelectedPickupPoint()
            }
        }

        override fun errorPickupPoint() {
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS, "")
            PreferenceService.getInstance().SetPreferenceValue(PreferenceService.PICKUP_POINT_ADDRESS_ID, "0")
        }
    }

    interface SelectedPickupPoint {
        fun onSelectedPickupPoint()
        fun showLoader()
        fun hideLoader()
    }


}

