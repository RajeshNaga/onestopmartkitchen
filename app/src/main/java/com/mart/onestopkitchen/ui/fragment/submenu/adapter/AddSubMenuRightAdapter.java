package com.mart.onestopkitchen.ui.fragment.submenu.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.SubCategory;

import java.util.List;

public class AddSubMenuRightAdapter extends RecyclerView.Adapter<AddSubMenuRightAdapter.ViewHolder> {
    List<SubCategory> children;
    private OnViewClickRight onViewClick;
    private Context mContext;

    public AddSubMenuRightAdapter(OnViewClickRight onViewClick, Context mContext) {
        this.onViewClick = onViewClick;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sub_menu_new, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(children.get(position).getName());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onViewClick.viewClickedRightRv(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    public interface OnViewClickRight {
        void viewClickedRightRv(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt_title);
        }
    }
}
