package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BillingAddress;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.CityListResponse;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;

import java.util.List;
import java.util.Objects;

import static android.view.View.GONE;

/**
 * Updated by Akash Garg on 11/9/2018.
 */
public class BillingAddressFragment extends BaseBillingAddressFragment implements BaseBillingAddressFragment.StateCityListener {
    private BillingAddress address = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.add_address));
        if ((getArguments() != null) && (null != getArguments().getSerializable(Keys.BILLING_ADDRESS))) {
            getActivity().setTitle(getActivity().getString(R.string.edit_address));
            IsComeFromCart = getArguments().getBoolean("IS_COME_FROM_CART", false);
            setStateCityListener(this);
            address = (BillingAddress) getArguments().getSerializable(Keys.BILLING_ADDRESS);
            if (address != null) {
                addressID = address.getId();
                fullName.setText(cutNull(address.getFirstName()));
                addressOne.setText(cutNull(address.getAddress1()));
                mobileNumberListener(cutNull(address.getPhoneNumber()));
                countryCode = (!cutNull(address.getCountryId()).isEmpty() ? address.getCountryId() : Keys.MYANMAR_COUNTRY_ID);

                /*
                if (address.getAddress2() != null) {
                    String[] add = address.getAddress2().split("!");
                    addressTwo.setText(add.length > 0 ? add[0] : "");
                }
                if (address.getHouseNo() != null && address.getHouseNo().length() > 0 && address.getHouseNo().charAt(address.getHouseNo().length() - 1) == '!')
                    etHouseNumber.setText(address.getHouseNo().substring(0, address.getHouseNo().length() - 1));
                else
                    etHouseNumber.setText(AppUtils.cutNull(address.getHouseNo()));
                if (address.getRoomNo() != null && address.getRoomNo().length() > 0 && address.getRoomNo().charAt(address.getRoomNo().length() - 1) == '!')
                    etRoomNumber.setText(address.getRoomNo().substring(0, address.getRoomNo().length() - 1));
                else
                    etRoomNumber.setText(AppUtils.cutNull(address.getRoomNo()));
                if (address.getFloorNo() != null && address.getFloorNo().length() > 0 && address.getFloorNo().charAt(address.getFloorNo().length() - 1) == '!')
                    etFloorNumber.setText(address.getFloorNo().substring(0, address.getFloorNo().length() - 1));
                else
                    etFloorNumber.setText(AppUtils.cutNull(address.getFloorNo()));
                */

                addressTwo.setText(cutNull(address.getAddress2()));
                etHouseNumber.setText(cutNull(address.getHouseNo()));
                etRoomNumber.setText(AppUtils.cutNull(address.getRoomNo()));
                etFloorNumber.setText(AppUtils.cutNull(address.getFloorNo()));
                if (address.getIsLiftOption()) {
                    rbYes.setChecked(true);
                    linRopes.setVisibility(GONE);
                } else {
                    rbNo.setChecked(true);
                    linRopes.setVisibility(View.VISIBLE);
                    spinnerRope.setVisibility(View.VISIBLE);
                    spinnerRope.setSelection(ropeColorList.indexOf(address.getRopeColor()));
                }
            }
        } else {
            callBillingAddressApi();
        }
        callStateRetrievalResponseApi();
    }

    private String cutNull(Object o) {
        return AppUtils.cutNull(o);//09778610238
    }

    @Override
    public void getStateAll(List<String> states) {
        if (states != null && null != address && stateSpinner != null) {
            stateSpinner.setSelection(states.indexOf(address.getStateProvinceName()));
            fullName.setText(address.getFirstName());
            RetroClient.getApi().getCityList(address.getStateProvinceId()).enqueue(new CustomCB<CityListResponse>(getView()));
        }
    }

    @Override
    public void getCityAll(List<String> cities) {
        if (cities != null && null != address && citySpinner != null)
            citySpinner.setSelection(cities.indexOf(address.getCity()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != address) {
            address = null;
        }
    }
}
