package com.mart.onestopkitchen.ui.capturephoto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.Keys;
import com.mart.onestopkitchen.utils.dialog.AppDialogs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

import static com.mart.onestopkitchen.utils.AppConstants.FROM_SIGN_UP_SELECT_CAPTURE;


public class NewFaceDetectionActivity extends AppCompatActivity implements AppDialogs.AlertDialogOkClick {

    //    public FrameLayout cardViewLanguageFlag;
    //    public ImageView mDefaultLanguage;
    //    ImageView imageViewFlash;
    //    boolean isFrontFlashOn = false;
    //    boolean hasFlash;
    //    private Dialog dialog;

    private static final String TAG = "FaceTest";
    public static String path;
    private static boolean safeToTakePicture = false;
    private static FaceOverlayView overlayView;
    private FrameLayout frameLayout;
    private Camera camera;
    private SurfaceView preview;
    private CameraListener cameraListener;
    private Camera.PictureCallback jpegCallback;
    private SurfaceHolder surfaceHolder = null;
    private File mPhotoFile;
    private int mNoOfFaces = 0;
    private boolean isFaceDetected = false;
    private int captureCountWithoutFD = 0;
    private SurfaceView overlay = null;
    private OverlayListener overlayListener = null;
    private boolean isFinish;


    // FOR RESTRICT THE ROTATION
    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, Camera camera) {
        Camera.CameraInfo info =
                new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        System.out.println("rotation 1" + rotation);
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }


        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        System.out.println("rotation result" + result);
        System.out.println("rotation result" + rotation);

        camera.setDisplayOrientation(result);
        Camera.Parameters parameters = camera.getParameters();
        parameters.setRotation(rotation);
        camera.setParameters(parameters);

        overlayView.setDisplayOrientation(result);
        safeToTakePicture = true;
    }

    private static boolean isLandscape270() {

        return Build.MANUFACTURER.equals("Amazon")
                && !(Build.MODEL.equals("KFOT") || Build.MODEL.equals("Kindle Fire"));
    }

    private void setOrientation() {
        switch (getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT: {
                int rotation = getWindowManager().getDefaultDisplay().getRotation();
                if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_180) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
            break;

            case Configuration.ORIENTATION_LANDSCAPE:
                int rotation = getWindowManager().getDefaultDisplay().getRotation();
                if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                }
                break;
        }
    }

    public void lockOrientation() {
        setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LOCKED);
    }

    private int getOrientation() {

        int port = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        int revP = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
        int land = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        int revL = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
        if (isLandscape270()) {
            land = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
            revL = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        }

        Display display = getWindowManager().getDefaultDisplay();
        boolean wide = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        switch (display.getRotation()) {
            case Surface.ROTATION_0:
                return wide ? land : port;
            case Surface.ROTATION_90:
                return wide ? land : revP;
            case Surface.ROTATION_180:
                return wide ? revL : revP;
            case Surface.ROTATION_270:
                return wide ? revL : port;
            default:
                throw new AssertionError();
        }
    }

    public void unlockOrientation() {
        setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_new_face_detection);
        initActivity();
        takePicture();
        setupToolbar();
        overlayView = new FaceOverlayView(this);
        frameLayout.addView(overlayView);
        // backFinish();
        takeAndCancelListner();
        showAlert(getString(R.string.photo_msg), false);
//        setFrontFlashAvailable();
    }


    private void initActivity() {
        preview = findViewById(R.id.surfaceView);
        surfaceHolder = preview.getHolder();
        cameraListener = new CameraListener(preview);
//        imageViewFlash = (ImageView) findViewById(R.id.image_front_flash);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        overlay = findViewById(R.id.overlay);
        overlayListener = new OverlayListener(overlay);
        frameLayout = findViewById(R.id.container);
    }


    private void takePicture() {
        jpegCallback = new Camera.PictureCallback() {

            public void onPictureTaken(byte[] data, Camera camera) {

                boolean isNoSpance = false;
                if (data != null) {
                    int screenWidth = getResources().getDisplayMetrics().widthPixels;
                    int screenHeight = getResources().getDisplayMetrics().heightPixels;
                    Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length);

                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                        Bitmap scaled = Bitmap.createScaledBitmap(bm, screenHeight, screenWidth, true);
                        int w = scaled.getWidth();
                        int h = scaled.getHeight();
                        Matrix mtx = new Matrix();
                        mtx.postRotate(-90);
                        bm = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);

                    } else {
                        bm = Bitmap.createScaledBitmap(bm, screenWidth, screenHeight, true);
                    }

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                    try {
                        Date currentTime = Calendar.getInstance().getTime();
                        //Log.i("---------currentTime--------",currentTime.toString());
                        mPhotoFile = new File(Environment.getExternalStorageDirectory() + File.separator + currentTime.getTime() + ".jpg");
                        mPhotoFile.createNewFile();
                        FileOutputStream fo = new FileOutputStream(mPhotoFile);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                        isNoSpance = true;
                    }
                }

                if (isNoSpance) {
                    Intent i = getIntent();
                    setResult(FROM_SIGN_UP_SELECT_CAPTURE, i);
                    releaseCamera();
                } else {
                    path = mPhotoFile.getAbsolutePath();
                    if (mPhotoFile != null && mPhotoFile.exists()) {
                        Intent i = getIntent();
                        i.putExtra(Keys.IMAGE_PATH, mPhotoFile.getAbsolutePath());
                        setResult(FROM_SIGN_UP_SELECT_CAPTURE, i);
                        releaseCamera();
                    }
                }
            }
        };
    }

    private void takeAndCancelListner() {
        findViewById(R.id.capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (!isFaceDetected && captureCountWithoutFD < 1) {
                        AppUtils.showCustomToastMsg(NewFaceDetectionActivity.this, "", getString(R.string.no_face_detected_more), false);
                        captureCountWithoutFD++;
                        return;
                    }
                    if (camera != null && safeToTakePicture && !isFaceDetected) {
                        camera.takePicture(null, null, jpegCallback);
                        safeToTakePicture = false;
                    } else {
                        if (camera != null && safeToTakePicture && mNoOfFaces == 1) {
                            camera.takePicture(null, null, jpegCallback);
                            safeToTakePicture = false;
                        } else if (mNoOfFaces == 0 && safeToTakePicture) {
                            AppUtils.showCustomToastMsg(NewFaceDetectionActivity.this, "", getString(R.string.no_face_detected_more), false);
                        } else if (mNoOfFaces > 1) {
                            AppUtils.showCustomToastMsg(NewFaceDetectionActivity.this, "", getString(R.string.more_face), false);
                        }
                    }
                } catch (Exception e) {
                    AppUtils.showCustomToastMsg(NewFaceDetectionActivity.this, "", getString(R.string.error_camera), false);


                }
            }
        });
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        preview.getHolder().addCallback(cameraListener);
        overlay.getHolder().addCallback(overlayListener);
    }


    public void showAlert(String message, final boolean status) {
        this.isFinish = status;
        AppDialogs.showAlertFinish(NewFaceDetectionActivity.this, "splash", isFinish, "", message, R.drawable.app_icon, 1);

    }

    private void releaseCamera() {
        finish();
    }

    @Override
    protected void onDestroy() {
        releaseObj();
        super.onDestroy();
        Log.e("FaceACtivity:", "########onDestroy#########");
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar1);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_nav_arrow_back));
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        Intent i = getIntent();
        setResult(FROM_SIGN_UP_SELECT_CAPTURE, i);
        finish();
    }

    @Override
    public void clickOk(int which) {
        if (isFinish)
            releaseCamera();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    private boolean isNightTime() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        //timeOfDay= 17;
        return timeOfDay >= 16 || timeOfDay < 7;
    }

    private class CameraListener implements SurfaceHolder.Callback, Camera.FaceDetectionListener {
        private SurfaceView surfaceView;
        private SurfaceHolder surfaceHolder;

        private CameraListener(SurfaceView surfaceView) {
            this.surfaceView = surfaceView;
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            surfaceHolder = holder;
            safeToTakePicture = true;
            try {
                int cameraId = -1;
                Camera.CameraInfo info = new Camera.CameraInfo();
                for (int id = 0; id < Camera.getNumberOfCameras(); id++) {
                    Camera.getCameraInfo(id, info);
                    if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                        cameraId = id;
                        break;
                    }
                }
                camera = Camera.open(cameraId);
                camera.setPreviewDisplay(holder);
                camera.getParameters().setPreviewFpsRange(1, 20);
                //setCameraDisplayOrientation(90, camera);
                setCameraDisplayOrientation(NewFaceDetectionActivity.this, cameraId, camera);
            } catch (Exception e) {
                if (camera == null) {
                    showAlert(getString(R.string.camera_permission_error), true);
                }
                Log.e(TAG, e.toString(), e);
            }
        }

//        public void setCameraDisplayOrientation(int degrees, Camera camera) {
//                Camera.CameraInfo info = new Camera.CameraInfo();
//                int result = (info.orientation + degrees) % 360;
//                camera.setDisplayOrientation(result);
//                overlayView.setDisplayOrientation(result);
//                safeToTakePicture = true;
//        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format,
                                   int width, int height) {
            try {
                surfaceHolder = holder;
                camera.startPreview();
                camera.setFaceDetectionListener(cameraListener);
                camera.startFaceDetection();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if (camera != null) {
                camera.setFaceDetectionListener(null);
                camera.release();
                camera = null;
            }
        }

        @Override
        public void onFaceDetection(Face[] faces, Camera camera) {

            isFaceDetected = true;
            int length = faces.length;
            if (length == 0) {
                mNoOfFaces = faces.length;
                return;
            }
            Face face = faces[0];
            overlayListener.drawFace(faceRect2PixelRect(face), Color.RED);
            overlayView.setFaces(faces);
            mNoOfFaces = faces.length;
            Log.e("face", "No of face =" + faces.length);
        }

        private Rect faceRect2PixelRect(Face face) {
            int w = surfaceView.getWidth();
            int h = surfaceView.getHeight();
            Rect rect = new Rect();

            // Left and right inversion because the front camera, portrait so coordinate axis inversion
            rect.left = w * (-face.rect.top + 1000) / 2000;
            rect.right = w * (-face.rect.bottom + 1000) / 2000;
            rect.top = h * (-face.rect.left + 1000) / 2000;
            rect.bottom = h * (-face.rect.right + 1000) / 2000;
            //Log.d(TAG, "rect=" + face.rect + "=>" + rect);
            return rect;
        }
    }

    private class OverlayListener implements SurfaceHolder.Callback {
        private SurfaceView surfaceView;
        private SurfaceHolder surfaceHolder;

        private Paint paint = new Paint();

        private OverlayListener(SurfaceView surfaceView) {
            this.surfaceView = surfaceView;
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            surfaceHolder = holder;
            surfaceHolder.setFormat(PixelFormat.TRANSPARENT);
            paint.setStyle(Style.STROKE);
            paint.setStrokeWidth(surfaceView.getWidth() / 100);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            surfaceHolder = holder;
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // nop.
        }

        void drawFace(Rect rect1, int color) {
            try {
                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    try {
                        canvas.drawColor(Color.RED, PorterDuff.Mode.CLEAR);
                        paint.setColor(color);
                        canvas.drawRect(rect1, paint);
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            } catch (IllegalArgumentException e) {
                Log.w(TAG, e.toString());
            }
        }
    }

    public void releaseObj() {
        if (null != camera) {
            camera = null;
        }
        if (null != cameraListener) {
            cameraListener = null;
        }
        if (null != preview) {
            preview = null;
        }
        if (null != surfaceHolder) {
            surfaceHolder = null;
        }
        if (null != overlay) {
            overlay = null;
        }
        if (null != overlayListener) {
            overlayListener = null;
        }
        if (null != overlayView) {
            overlayView = null;
        }
        if (null != jpegCallback) {
            jpegCallback = null;
        }
        if (null != mPhotoFile) {
            mPhotoFile = null;
        }
        if (null != path) {
            path = null;
        }
    }
}
