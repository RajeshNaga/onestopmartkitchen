package com.mart.onestopkitchen.ui.selectcountry.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CountryResponseModel implements Serializable {

    @SerializedName("data")
    private ArrayList<DataItem> data;
    @SerializedName("status")
    private Status status;

    public ArrayList<DataItem> getData() {
        return data;
    }

    public void setData(ArrayList<DataItem> data) {
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


}