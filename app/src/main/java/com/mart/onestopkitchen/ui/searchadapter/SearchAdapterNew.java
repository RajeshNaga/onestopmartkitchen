package com.mart.onestopkitchen.ui.searchadapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.utils.AppUtils;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Updated by Akash Garg on 01-Feb-19.
 */

public class SearchAdapterNew extends RecyclerView.Adapter<SearchAdapterNew.ItemViewHolder> {
    public List<ProductModel> mDataSet;
    public int apiPosition = 2;
    private OnViewClick onViewClick;
    private Context mContext;
    private boolean isLastDataIsEmpty;

    public SearchAdapterNew(OnViewClick onViewClickContext, Context context) {
        this.mDataSet = new ArrayList<>();
        this.mContext = context;
        this.onViewClick = onViewClickContext;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_products_grid, parent, false));

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, final int position) {
        ProductModel productModel = mDataSet.get(position);
/*
         if (productModel != null && productModel.getShortDescription() != null && productModel.getShortDescription().equals("lazyLoderPaging")) {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.relContentView.setVisibility(View.GONE);
            holder.cardView.setClickable(false);
            holder.cardView.setEnabled(false);
            return;
        } else {
            holder.progressBar.setVisibility(View.GONE);
            holder.cardView.setClickable(true);
            holder.cardView.setEnabled(true);
            holder.relContentView.setVisibility(View.VISIBLE);
        }

        if (position == mDataSet.size() - 2&&!isLastDataIsEmpty) {
            onViewClick.paginationApiCall(apiPosition);
            apiPosition++;
        }*/
        holder.productName.setText(productModel.getName());

        if (null != productModel.getProductPrice().getPriceWithDiscount() && !productModel.getProductPrice().getPriceWithDiscount().isEmpty()) {
            holder.productOldPrice.setVisibility(View.VISIBLE);
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPriceWithDiscount(), 0));
            holder.productOldPrice.setText(AppUtils.getMMKString(holder.productOldPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));

        } else if (null != productModel.getProductPrice().getOldPrice()) {
            holder.productOldPrice.setVisibility(View.VISIBLE);
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
            holder.productOldPrice.setText(AppUtils.getMMKString(holder.productOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));

        } else {
            holder.productPrice.setText(AppUtils.getMMKString(holder.productPrice.getTextSize(), productModel.getProductPrice().getPrice(), 0));
            holder.productOldPrice.setText(AppUtils.getMMKString(holder.productOldPrice.getTextSize(), productModel.getProductPrice().getOldPrice(), 0));
            holder.productOldPrice.setVisibility(View.GONE);
        }
        holder.productOldPrice.setPaintFlags(holder.productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        Glide
                .with(mContext)
                .load(productModel.getDefaultPictureModel().getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(mContext.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.productImage);


        if (null != holder.gif_mark_new) {
            if (productModel.getMarkAsNew()) {
                holder.gif_mark_new.setVisibility(View.VISIBLE);
                Glide
                        .with(mContext)
                        .load(R.drawable.new_3)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.gif_mark_new);
            } else {
                holder.gif_mark_new.setVisibility(View.GONE);
            }
        }


        if (productModel.getProductPrice().getDiscountPercentage() != 0) {
            holder.tv_discount_percentage.setVisibility(View.VISIBLE);
            holder.tv_discount_percentage.setText(productModel.getProductPrice().getDiscountPercentage() + " %");
        } else holder.tv_discount_percentage.setVisibility(View.GONE);

        holder.cardView.setOnClickListener(view -> onViewClick.onItemClick(mDataSet.get(position)));

        holder.wishList.setVisibility(View.VISIBLE);

        holder.wishList.setChecked(mDataSet.get(position).getWishList());

        holder.wishList.setOnClickListener(view -> {
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet()) {
                if (holder.wishList.isChecked()) {
                    mDataSet.get(position).setWishList(true);
                    onViewClick.callApiForWishList(mDataSet.get(position).getId(), 2);
                } else {
                    if (mDataSet.get(position).getId() != 0L) {
                        mDataSet.get(position).setWishList(false);
                        onViewClick.removeWishlist(mDataSet.get(position).getId(), 2);
                    }
                }
            } else {
                holder.wishList.setChecked(false);
            }
        });

    }

    @Override
    public int getItemCount() {
        // onViewClick.noRecord(mDataSet);
        return mDataSet.size();
    }

    public void loadData(List<ProductModel> mDataSets) {
     /*    if (mDataSets.size() > 0)
            this.mDataSet = setProgress(mDataSets);*/
        this.mDataSet = mDataSets;
        notifyDataSetChanged();
    }

    private List<ProductModel> setProgress(List<ProductModel> mDataSet) {
        isLastDataIsEmpty = false;
        ProductModel model = new ProductModel();
        model.setShortDescription("lazyLoderPaging");
        mDataSet.add(model);
        return mDataSet;
    }

    public void lazyLoader(List<ProductModel> responseProducts) {

        mDataSet.remove(mDataSet.size() - 1);
        this.mDataSet.addAll(setProgress(responseProducts));
        notifyDataSetChanged();

    }

    public void isLastData() {
        if (!isLastDataIsEmpty) {
            isLastDataIsEmpty = true;
            mDataSet.remove(mDataSet.size() - 1);
            notifyDataSetChanged();
        }
    }

    public interface OnViewClick {
        void onItemClick(ProductModel productModel);

        void paginationApiCall(int position);

        void noRecord(List<ProductModel> models);

        void callApiForWishList(long pId, int cartTypeId);

        void removeWishlist(long pId, int cartId);


    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_total)
        CardView cardView;
        @BindView(R.id.button_favorite_category)
        ToggleButton wishList;

        @BindView(R.id.img_productImage)
        ImageView productImage;

        @BindView(R.id.tv_productPrice)
        TextView productPrice;

        @BindView(R.id.tv_productOldPrice)
        TextView productOldPrice;

        @BindView(R.id.tv_productName)
        TextView productName;

        @BindView(R.id.tv_discount_percentage)
        TextView tv_discount_percentage;
        @BindView(R.id.rel_content_view)
        RelativeLayout relContentView;
        @BindView(R.id.progress_bar)
        ProgressWheel progressBar;
        @BindView(R.id.gif_mark_new)
        ImageView gif_mark_new;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            productOldPrice.setPaintFlags(productOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
}
