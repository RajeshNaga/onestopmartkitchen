package com.mart.onestopkitchen.ui.fragment.submenu.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.SecondSubCategory;
import com.mart.onestopkitchen.model.SubCategory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SubMenuItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        SectionHeaderViewHolder.HeaderViewHolderCallback {

    private static final int USER_TYPE = 1;
    private static final int HEADER_TYPE = 2;
    List<SubCategory> category;
    private ViewClick viewClick;
    private Context mContext;
    private List<SecondSubCategory> usersList;
    private List<SubCategory> userTypeList;
    private SparseArray<ViewType> viewTypes;
    private SparseIntArray headerExpandTracker;

    public SubMenuItemAdapter(ViewClick viewClick, Context mContext) {
        this.viewClick = viewClick;
        this.mContext = mContext;
    }

    public void loadData(List<SubCategory> category, List<SecondSubCategory> children) {
        if (category != null && children != null) {
            this.usersList = children;
            this.userTypeList = category;
            viewTypes = new SparseArray<>(usersList.size() + userTypeList.size());
            headerExpandTracker = new SparseIntArray(userTypeList.size());
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewTypes.get(position).getType() == HEADER_TYPE) {
            return HEADER_TYPE;
        } else {
            return USER_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case USER_TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_user_list_item, parent, false);
                return new UserViewHolder(view);
            case HEADER_TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_cate_list_section_header, parent, false);
                return new SectionHeaderViewHolder(view, this);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_user_list_item, parent, false);
                return new UserViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        ViewType viewType = viewTypes.get(position);
        if (itemViewType == USER_TYPE) {
            bindUserViewHolder(holder, viewType);
        } else {
            bindHeaderViewHolder(holder, position, viewType);
        }
    }

    private void bindHeaderViewHolder(RecyclerView.ViewHolder holder, int position, ViewType viewType) {
        int dataIndex = viewType.getDataIndex();
        SectionHeaderViewHolder headerViewHolder = (SectionHeaderViewHolder) holder;
        headerViewHolder.sectionTitle.setText(userTypeList.get(dataIndex).getName());
        if (isExpanded(position)) {
            headerViewHolder.sectionTitle
                    .setCompoundDrawablesWithIntrinsicBounds(null, null, headerViewHolder.arrowUp, null);
        } else {
            headerViewHolder.sectionTitle
                    .setCompoundDrawablesWithIntrinsicBounds(null, null, headerViewHolder.arrowDown, null);
        }
    }

    private void bindUserViewHolder(RecyclerView.ViewHolder holder, ViewType viewType) {
        int dataIndex = viewType.getDataIndex();
        ((UserViewHolder) holder).username.setText(usersList.get(dataIndex).getName());
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (userTypeList != null && usersList != null) {
            viewTypes.clear();
            int collapsedCount = 0;
            for (int i = 0; i < userTypeList.size(); i++) {
                viewTypes.put(count, new ViewType(i, HEADER_TYPE));
                count += 1;
                String userType = userTypeList.get(i).getName();
                int childCount = getChildCount(userTypeList.get(i));
                if (headerExpandTracker.get(i) != 0) {
                    // Expanded State
                    for (int j = 0; j < childCount; j++) {
                        viewTypes.put(count, new ViewType(count - (i + 1) + collapsedCount, USER_TYPE));
                        count += 1;
                    }
                } else {
                    // Collapsed
                    collapsedCount += childCount;
                }
            }
        }
        return count;
    }

    private int getChildCount(SubCategory s) {
        return s.getChildren().size();
    }

    @Override
    public void onHeaderClick(int position) {
        ViewType viewType = viewTypes.get(position);
        int dataIndex = viewType.getDataIndex();
        int childCount = getChildCount(userTypeList.get(dataIndex));
        if (headerExpandTracker.get(dataIndex) == 0) {
            // Collapsed. Now expand it
            headerExpandTracker.put(dataIndex, 1);
            notifyItemRangeInserted(position + 1, childCount);
        } else {
            // Expanded. Now collapse it
            headerExpandTracker.put(dataIndex, 0);
            notifyItemRangeRemoved(position + 1, childCount);
        }
    }

    @Override
    public boolean isExpanded(int position) {
        int dataIndex = viewTypes.get(position).getDataIndex();
        return headerExpandTracker.get(dataIndex) == 1;
    }


    public interface ViewClick {
        void viewClickEvent();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title)
        TextView txtTitle;

        @BindView(R.id.img_drop)
        ImageView imgDrop;
      /*  @BindView(R.id.lin_child)
        LinearLayout linChild;*/

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        ImageView userAvatar;
        TextView username;

        public UserViewHolder(View itemView) {
            super(itemView);
            userAvatar = itemView.findViewById(R.id.imageview_profile);
            username = itemView.findViewById(R.id.textview_name);
        }
    }
}
