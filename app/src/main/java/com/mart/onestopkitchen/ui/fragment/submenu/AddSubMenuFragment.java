package com.mart.onestopkitchen.ui.fragment.submenu;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.Category;
import com.mart.onestopkitchen.model.addsubmenu.SubMenuResponseModel;
import com.mart.onestopkitchen.mvp.ui.filteroption.FragmentListener;
import com.mart.onestopkitchen.ui.fragment.ProductListFragmentFor3_8;
import com.mart.onestopkitchen.ui.fragment.submenu.adapter.AddSubMenuRightAdapter;
import com.mart.onestopkitchen.ui.fragment.submenu.adapter.SubMenuItemAdapter;
import com.mart.onestopkitchen.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Ashraful on 2/11/2016.
 */
public class AddSubMenuFragment extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener, AddSubMenuRightAdapter.OnViewClickRight, FragmentListener {
    @BindView(R.id.container_new)
    FrameLayout containerNew;
    @BindView(R.id.rv_selectd_list)
    RecyclerView rvSelectdList;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    AddSubMenuFragment subMenuFragment;
    private String cateName = "";
    private int cateId = 0;
    private Category category;
    private SubMenuItemAdapter subMenuItemAdapter;
    private Context mContext;
    private SubMenuResponseModel addSubMenuResponseModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sub_menu);
        ButterKnife.bind(this);
        iniUi();
    }


    private void iniUi() {
        mContext = this;
        apiCall();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            category = (Category) bundle.getSerializable("CATEGORY");
        initRv();
        uiListener();
    }

    private void apiCall() {
        addSubMenuResponseModel = new Gson().fromJson(getLocalData(), SubMenuResponseModel.class);
        loadFragments();
    }

    private void loadFragments() {
        if (addSubMenuResponseModel.getMainItems().size() > 0) {
            visibleRv(true);
            loadFragement(addSubMenuResponseModel);
        } else {
            nextFragment(new ProductListFragmentFor3_8());
            visibleRv(false);
        }
    }

    private void uiListener() {
        toolBar.setNavigationIcon(this.getResources().getDrawable(R.drawable.ic_action_nav_arrow_back));
        toolBar.setTitle(category.getName());
    }

    //after api call
    private void loadFragement(SubMenuResponseModel addSubMenuResponseModel) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_new, AddSubMenuFragmentChild.newInstance(addSubMenuResponseModel), AddSubMenuFragmentChild.class.getName())
                .addToBackStack(AddSubMenuFragmentChild.class.getName())
                .commitAllowingStateLoss();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    private void initRv() {
        //  AddSubMenuRightAdapter addSubMenuRightAdapter = new AddSubMenuRightAdapter(this, mContext);
        //   rvSelectdList.setLayoutManager(new LinearLayoutManager(mContext));
        //   rvSelectdList.setAdapter(addSubMenuRightAdapter);
    }

    private void visibleRv(boolean b) {
        if (b)
            rvSelectdList.setVisibility(View.VISIBLE);
        else rvSelectdList.setVisibility(View.GONE);

    }


    private void logD(String tag, String msg) {
        AppUtils.appLog(tag, msg);
    }


    @Override
    public void viewClickedRightRv(int pos) {

    }


    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_new);
        if (fragment != null)
            fragment.onResume();
    }

    private String getLocalData() {
        return "{\n" +
                "  \"MainItemName\":\"Womens Fashion\",\n" +
                "  \"MainItems\": [ \n" +
                "    \n" +
                "    {\n" +
                "      \"CategeryName\": \"Clothing\",\n" +
                "      \"CategeryId\": 1,\n" +
                "      \"CategeryItems\": [\n" +
                "        {\n" +
                "          \"ItemName\": \"KurtasKurtis\",\n" +
                "          \"SubItems\": [\n" +
                "            {\n" +
                "              \"SubItemId\": 11,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 12,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 13,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 14,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Sarees\",\n" +
                "          \"SubItems\": [\n" +
                "            {\n" +
                "              \"SubItemId\": 21,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 22,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 23,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 24,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 25,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 26,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 27,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 28,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 29,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemId\": 30,\n" +
                "              \"SubItemName\": \"Shades Women's Solid Straight Kurta\"\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Tops\",\n" +
                "          \"SubItems\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"WesternWear\",\n" +
                "          \"SubItems\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"DressMeteriel\",\n" +
                "          \"SubItems\": []\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"CategeryName\": \"FootWear\",\n" +
                "      \"CategeryId\": 2,\n" +
                "      \"CategeryItems\": [\n" +
                "        {\n" +
                "          \"ItemName\": \"FlibFlibs\",\n" +
                "          \"SubItems\": [\n" +
                "            {\n" +
                "              \"SubItemName\": \"NorMal flib\",\n" +
                "              \"SubItemId\": 1\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Shoes\",\n" +
                "          \"SubItems\": [\n" +
                "            {\n" +
                "              \"SubItemName\": \"NorMal Shoes\",\n" +
                "              \"SubItemId\": 1\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"CausualShoes\",\n" +
                "          \"SubItems\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Ballers\",\n" +
                "          \"SubItems\": []\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"CategeryName\": \"Beauty & Grooming\",\n" +
                "      \"CategeryId\": 1,\n" +
                "      \"CategeryItems\": [\n" +
                "        {\n" +
                "          \"ItemName\": \"MakeUp\",\n" +
                "          \"SubItems\": [\n" +
                "            {\n" +
                "              \"SubItemName\": \"NorMal MakeUpsets\",\n" +
                "              \"SubItemId\": 1\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Hair\",\n" +
                "          \"SubItems\": [\n" +
                "            {\n" +
                "              \"SubItemName\": \"NorMal Hairs\",\n" +
                "              \"SubItemId\": 1\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemName\": \"Stright Hairs\",\n" +
                "              \"SubItemId\": 1\n" +
                "            }\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Skin\",\n" +
                "          \"SubItems\": []\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"CategeryName\": \"FragNes\",\n" +
                "      \"CategeryId\": 1,\n" +
                "      \"CategeryItems\": []\n" +
                "    },\n" +
                "    {\n" +
                "      \"CategeryName\": \"Jewellary\",\n" +
                "      \"CategeryId\": 1,\n" +
                "      \"CategeryItems\": [\n" +
                "        {\n" +
                "          \"ItemName\": \"Precious \",\n" +
                "          \"SubItems\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Artificial Jewellary\",\n" +
                "          \"SubItems\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"Silver Jewellary\",\n" +
                "          \"SubItems\": []\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"SportsWear\",\n" +
                "          \"SubItems\": [  {\n" +
                "              \"SubItemName\": \"T shirts\",\n" +
                "              \"SubItemId\": 1\n" +
                "            },\n" +
                "            {\n" +
                "              \"SubItemName\": \"Track pants\",\n" +
                "              \"SubItemId\": 1\n" +
                "            }]\n" +
                "        },\n" +
                "        {\n" +
                "          \"ItemName\": \"DressMeteriel\",\n" +
                "          \"SubItems\": [ ]\n" +
                "        }\n" +
                "      ]\n" +
                "    } \n" +
                "     \n" +
                "  ] \n" +
                " \n" +
                "}";

    }

    @Override
    public void nextFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction
                // .setCustomAnimations(startAnim, endAnim )
                //  .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .add(R.id.container_new, fragment, fragment.getClass().getName())
                .addToBackStack(fragment.getClass().getName())
                .commitAllowingStateLoss();

    }

}
