package com.mart.onestopkitchen.ui.selectcountry.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DataItem implements Serializable {

    @SerializedName("CountryName")
    private String countryName;

    @SerializedName("CountryCode")
    private String countryCode;


    private int localImg = -1;
    @SerializedName("MCC")
    private String mCC;

    @SerializedName("networks")
    private List<NetworksItem> networks;

    @SerializedName("CountryFlagCode")
    private String countryFlagCode;

    @SerializedName("DialingCodes")
    private List<DialingCodesItem> dialingCodes;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getLocalImg() {
        return localImg;
    }

    public void setLocalImg(int localImg) {
        this.localImg = localImg;
    }

    public String getMCC() {
        return mCC;
    }

    public void setMCC(String mCC) {
        this.mCC = mCC;
    }

    public List<NetworksItem> getNetworks() {
        return networks;
    }

    public void setNetworks(List<NetworksItem> networks) {
        this.networks = networks;
    }

    public String getCountryFlagCode() {
        return countryFlagCode;
    }

    public void setCountryFlagCode(String countryFlagCode) {
        this.countryFlagCode = countryFlagCode;
    }

    public List<DialingCodesItem> getDialingCodes() {
        return dialingCodes;
    }

    public void setDialingCodes(List<DialingCodesItem> dialingCodes) {
        this.dialingCodes = dialingCodes;
    }
}