package com.mart.onestopkitchen.ui.fragment;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BaseProductModel;
import com.mart.onestopkitchen.model.ManuFacturer;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class
ViewAllAdapterFeature extends RecyclerView.Adapter<ViewAllAdapterFeature.ViewHolder> {

    private List<ManuFacturer> homePageProductResponses;
    private Context mContext;
    private OnViewClickFeature onViewClick;

    public ViewAllAdapterFeature(OnViewClickFeature featureViewAllFragment, Context context, List<ManuFacturer> data) {
        this.homePageProductResponses = data;
        this.mContext = context;
        this.onViewClick = featureViewAllFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_homepage_product_feature, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BaseProductModel product = homePageProductResponses.get(position);
        holder.onItemBind(product);
        Glide
                .with(mContext)
                .load(product.getDefaultPictureModel().getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgProductImage);
        holder.tvProductName.setText(product.getName());

    }

    @Override
    public int getItemCount() {
        return homePageProductResponses != null ? homePageProductResponses.size() : 0;
    }


    public interface OnViewClickFeature {
        void OnViewClickedFeature(BaseProductModel productModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fav)
        CheckBox fav;
        @BindView(R.id.img_productImage)
        ImageView imgProductImage;
        @BindView(R.id.tv_productName)
        TextView tvProductName;
        @BindView(R.id.lin_root)
        LinearLayout linRoot;

        private BaseProductModel productModel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.lin_root)
        public void onViewClicked() {
            onViewClick.OnViewClickedFeature(productModel);
        }

        public void onItemBind(BaseProductModel product) {
            this.productModel = product;
        }
    }
}
