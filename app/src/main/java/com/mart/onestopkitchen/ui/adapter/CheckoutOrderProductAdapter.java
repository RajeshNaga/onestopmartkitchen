package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Updated by Akash GArg on 22/12/2018.
 */
public class CheckoutOrderProductAdapter extends CartAdapter {
    boolean isProductRemovable;
    private String TAG = CheckoutOrderProductAdapter.class.getSimpleName();

    public CheckoutOrderProductAdapter(Context context, List productsList, Fragment fragment, PreferenceService preferenceService) {
        super(context, productsList, fragment, preferenceService);
    }

    public CheckoutOrderProductAdapter(Context context, List productsList, Fragment fragment, boolean isProductRemovable, PreferenceService preferenceService) {
        super(context, productsList, fragment, preferenceService);
        this.isProductRemovable = isProductRemovable;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        layout = R.layout.item_checkout_product;
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new ProductSummaryHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder bindViewHolder, final int position) {
        try {
            if (bindViewHolder instanceof ProductSummaryHolder) {
                CartProduct productModel = products.get(position);
                ProductSummaryHolder holder = (ProductSummaryHolder) bindViewHolder;
                holder.productName.setText(productModel.getProductName());
                addMultiColoredTextInView(context.getString(R.string.price) + ": ", productModel.getUnitPrice(), holder.productPrice, R.color.accentDark);
                addMultiColoredTextInView(context.getString(R.string.quantity), " " + productModel.getQuantity(), holder.productQuantity);
                addMultiColoredTextInView(context.getString(R.string.delivered_on) + ": ", "" + productModel.getDeliveryDays(), holder.tvDeliveryDate);

                // actually we are getting oldprice from price key because this complicated thing coming from server side so we have to do like this
                if ((null != productModel.getDiscountPercentage() && Integer.parseInt(productModel.getDiscountPercentage()) == 0)
                        && (null != productModel.getOldPrice() && productModel.getOldPrice().startsWith("0"))) {
                    holder.tvOldPrice.setVisibility(View.GONE);
                } else if ((null != productModel.getDiscountPercentage() && Integer.parseInt(productModel.getDiscountPercentage()) > 0)) {
                    holder.tvOldPrice.setVisibility(View.VISIBLE);
                    holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOldPrice.setText(AppUtils.getMMKString(holder.tvOldPrice.getTextSize(), productModel.getPrice(), 0));
                    holder.tvOffer.setVisibility(View.VISIBLE);
                    holder.tvOffer.setText(productModel.getDiscountPercentage() + "%");
                } else {
                    holder.tvOldPrice.setVisibility(View.VISIBLE);
                    holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOldPrice.setText(AppUtils.getMMKString(holder.tvOldPrice.getTextSize(), productModel.getOldPrice(), 0));
                }

                Glide
                        .with(context)
                        .load(productModel.getPicture().getImageUrl())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                        .into(holder.productImage);


                if (products.get(position).getQuantity() > products.get(position).getAvailableQuantity()) {
                    holder.tvStockMsg.setVisibility(View.VISIBLE);
                    holder.tvStockMsg.setText((products.get(position).getWarnings() != null && products.get(position).getWarnings().size() > 0) ? products.get(position).getWarnings().get(0).toString() : context.getString(R.string.out_of_stock));
                } else {
                    holder.tvStockMsg.setVisibility(View.GONE);
                }
                setProductAttrInfo(holder, products.get(position).getAttributeInfo());
            }
        } catch (ClassCastException ex) {
            Log.d(TAG, "Exception ---:" + ex.getMessage());
        }
    }

    private void setProductAttrInfo(ProductSummaryHolder holder, String productAttributeInfo) {
        if (productAttributeInfo.isEmpty()) {
            holder.tvAttributes.setVisibility(View.GONE);
            return;
        }
        StringBuilder attributes = new StringBuilder();
        if (productAttributeInfo.contains("&quot;")) {
            productAttributeInfo = productAttributeInfo.replace("&quot;", " ");
        }
        String[] attr = productAttributeInfo.split("<br />");
        for (int i = 0; i < attr.length; i++) {
            if (i == attr.length - 1) {
                attributes.append(attr[i]).append("");
            } else attributes.append(attr[i]).append("\n");
        }
        if (!attributes.toString().trim().isEmpty()) {
            holder.tvAttributes.setVisibility(View.VISIBLE);
            holder.tvAttributes.setText(attributes.toString());
        } else {
            holder.tvAttributes.setVisibility(View.INVISIBLE);
        }
    }


    private void addMultiColoredTextInView(String firstWord, String secondWord, TextView textView) {
        Spannable word = new SpannableString(firstWord);
        word.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.textPrimaryColor)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(word);
        Spannable wordTwo = new SpannableString(secondWord);
        wordTwo.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.textSecondarColor)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.append(wordTwo);
    }

    private void addMultiColoredTextInView(String firstWord, String secondWord,
                                           TextView textView, int colorResource) {
        Spannable word = new SpannableString(firstWord);
        word.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.textPrimaryColor)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(word);
        Spannable wordTwo;// = new SpannableString(secondWord);
        wordTwo = AppUtils.getMMKString(textView.getTextSize(), secondWord, 1);
        wordTwo.setSpan(new ForegroundColorSpan(ContextCompat.
                getColor(context, colorResource)), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.append(wordTwo);
    }

    public class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_productImage)
        ImageView productImage;
        @BindView(R.id.tv_productPrice)
        TextView productPrice;
        @BindView(R.id.tv_productName)
        TextView productName;
        @BindView(R.id.tv_product_quantity)
        TextView productQuantity;
        @BindView(R.id.tv_delivery_date)
        TextView tvDeliveryDate;
        @BindView(R.id.tv_stock_msg)
        TextView tvStockMsg;
        @BindView(R.id.tvAttributes)
        TextView tvAttributes;

        @BindView(R.id.tvOldPrice)
        TextView tvOldPrice;
        @BindView(R.id.tvOffer)
        TextView tvOffer;

//        @BindView(R.id.swipe)
//        SwipeLayout swipeLayout;

        public ProductSummaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}
