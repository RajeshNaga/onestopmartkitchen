package com.mart.onestopkitchen.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.mart.onestopkitchen.ui.activity.MainActivity;

/**
 * Created by Ashraful on 11/6/2015.
 */
public class Utility {
    public static final String orderIdKey = "orderIdKey";
    public static int categoryId;
    @SuppressLint("StaticFieldLeak")
    public static Activity activity;
    public static int cartCounter;

    public static Activity getActivity() {
        return activity;
    }

    public static void setActivity(Activity activity) {
        Utility.activity = activity;
    }

    public static void closeLeftDrawer() {
        if (null != MainActivity.self)
            MainActivity.self.closeDrawer();
    }

    public static void setCartCounter(int counter) {
        cartCounter = counter;
        if (null != MainActivity.self)
            MainActivity.self.updateHotCount(counter);
    }

    static void doAnimate() {
        if (null != MainActivity.self)
            MainActivity.self.animate();
    }
}
