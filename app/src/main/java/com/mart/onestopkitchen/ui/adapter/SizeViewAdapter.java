package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.AttributeControlValue;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductAttribute;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.PriceResponse;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Hemnath on 10/30/2018.
 */
public class SizeViewAdapter extends RecyclerView.Adapter<SizeViewAdapter.SizeViewHolder> {

    Context context;
    private List<AttributeControlValue> values;
    private String key;
    private String measurementKey;
    private onSelectSizeAdapterListener onSelectSizeAdapterListener;
    private int selectedIndex = -1;

    public SizeViewAdapter(Context context, onSelectSizeAdapterListener onSelectSizeAdapterListener) {
        this.onSelectSizeAdapterListener = onSelectSizeAdapterListener;
        this.context = context;
        values = new ArrayList<>(0);
    }

    public void loadData(List<AttributeControlValue> values, ProductAttribute productAttribute) {
        key = getKey(productAttribute);
        measurementKey = getMeasurementKey(productAttribute);
        this.values = values;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SizeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.size_linear_layout, viewGroup, false);
        return new SizeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SizeViewHolder sizeViewHolder, @SuppressLint("RecyclerView") final int i) {
        sizeViewHolder.tv_size.setText(values.get(i).getName());
        if (i == selectedIndex) {
            sizeViewHolder.tv_size.setBackgroundResource(R.drawable.size_background_blue);
        } else {
            sizeViewHolder.tv_size.setBackgroundResource(R.drawable.size_background);
        }
        sizeViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedIndex = i;
                Map<String, String> valueTextPairMap = new HashMap<>();
                valueTextPairMap.put(String.valueOf(values.get(i).getId()), key);
                valueTextPairMap.put(values.get(i).getName(), measurementKey);
                callPriceWebservice(onSelectSizeAdapterListener.getSelectedSizeFromadapter(valueTextPairMap));
                onSelectSizeAdapterListener.expandView();
                notifyDataSetChanged();

            }
        });
    }

    public String getKey(ProductAttribute productAttribute) {
        return String.format("%s_%s_%s_%s", "product_attribute", productAttribute.getProductId()
                , productAttribute.getProductAttributeId(), productAttribute.getId());
    }

    private String getMeasurementKey(ProductAttribute productAttribute) {
        return String.format("%s_%s_%s_%s", "product_measurement", productAttribute.getProductId()
                , productAttribute.getProductAttributeId(), productAttribute.getId());
    }

    private void callPriceWebservice(List<KeyValuePair> valuePair) {
        RetroClient.getApi().getUpdatedPrice(ProductDetailFragment.productModel.getId(), valuePair)
                .enqueue(new CustomCB<PriceResponse>(ProductDetailFragment.self.getView()));
    }


    @Override
    public int getItemCount() {
        return values.size();
    }

    public interface onSelectSizeAdapterListener {
        List<KeyValuePair> getSelectedSizeFromadapter(Map<String, String> pairValue);

        void expandView();
    }

    class SizeViewHolder extends RecyclerView.ViewHolder {

        TextView tv_size;

        SizeViewHolder(View itemView) {
            super(itemView);
            tv_size = itemView.findViewById(R.id.tv_size_view);
        }

    }
}
