package com.mart.onestopkitchen.ui.selectcountry;

import android.content.Context;

import com.mart.onestopkitchen.ui.selectcountry.model.DataItem;

import java.util.List;

public interface AdapterListener {
    Context getAppContext();

    void onItemClick(DataItem countryListModel);

    void noRecordFound(List<DataItem> listModels);

}
