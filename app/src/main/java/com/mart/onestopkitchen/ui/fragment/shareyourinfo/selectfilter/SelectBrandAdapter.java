package com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.fragment.shareyourinfo.selectfilter.demomodel.BrandsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectBrandAdapter extends RecyclerView.Adapter<SelectBrandAdapter.ViewHolder> {

    private OnBrandClick onBrandClick;
    private List<BrandsItem> sizeResponse;

    public SelectBrandAdapter(OnBrandClick onBrandClick) {
        this.sizeResponse = new ArrayList<>();
        this.onBrandClick = onBrandClick;
    }

    public void loadData(List<BrandsItem> items) {
        this.sizeResponse = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_availbel_brands, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final BrandsItem sizeResponseBrands = sizeResponse.get(position);
        holder.onItemBind(sizeResponseBrands);
        holder.txtTitle.setText(sizeResponseBrands.getName());
    }

    @Override
    public int getItemCount() {
        return sizeResponse == null ? 0 : sizeResponse.size();
    }


    public interface OnBrandClick {
        void OnBrandViewClicked(BrandsItem item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.lin_root)
        LinearLayout linRoot;
        private BrandsItem item;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.lin_root)
        public void onViewClicked() {
            onBrandClick.OnBrandViewClicked(item);

        }


        public void onItemBind(BrandsItem sizeResponseBrands) {
            this.item = sizeResponseBrands;
        }
    }
}
