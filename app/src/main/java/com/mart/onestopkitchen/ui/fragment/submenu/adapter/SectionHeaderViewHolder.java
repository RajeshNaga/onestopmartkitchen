package com.mart.onestopkitchen.ui.fragment.submenu.adapter;

import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mart.onestopkitchen.R;

public class SectionHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    final HeaderViewHolderCallback callback;
    TextView sectionTitle;
    Drawable arrowUp;
    Drawable arrowDown;

    public SectionHeaderViewHolder(View itemView, HeaderViewHolderCallback callback) {
        super(itemView);
        sectionTitle = itemView.findViewById(R.id.textview_section_header);
        this.callback = callback;

        arrowUp = ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_arrow_up_new);
        arrowDown = ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_arrow_down);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        callback.onHeaderClick(position);
        if (callback.isExpanded(position)) {
            sectionTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowUp, null);
        } else {
            sectionTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowDown, null);
        }
    }

    public interface HeaderViewHolderCallback {
        void onHeaderClick(int position);

        boolean isExpanded(int position);
    }

}
