package com.mart.onestopkitchen.ui.fragment.addaddres;

import com.mart.onestopkitchen.baseview.LoadView;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;

public interface CustomerAddressListener extends LoadView {

    void successFromServerGetAddress(BillingAddressResponse addressResponse);

    String userName();

    String addressOne();

    String addressTwo();

    String countryId();

    String stateId();

    String cityId();

}
