package com.mart.onestopkitchen.ui.fragment;

import android.os.Bundle;

import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.ProductsResponse;

/**
 * Created by Ashraful on 2/2/2016.
 */
public class ManufaturerFragment extends ProductListFragmentFor3_8 {
    // added
    public static ManufaturerFragment newInstance(String categoryName, int categoryId) {
        ManufaturerFragment fragment = new ManufaturerFragment();
        Bundle args = new Bundle();
        args.putString(CATEGORY_NAME, categoryName);
        args.putInt(CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void callWebService() {
        if (productAdapter != null)
            productAdapter.resetList();

        resetList = true;
        pageNumber = 1;

        getQueryMap();
        RetroClient.getApi().getProductListByManufacturer(categoryId, queryMapping)
                .enqueue(new CustomCB<ProductsResponse>(rootViewRelativeLayout));
    }

    @Override
    public void callWebServiceMoreLoad(int pageNumber) {
        this.pageNumber = pageNumber;
        resetList = false;
        getQueryMap();
        RetroClient.getApi().getProductListByManufacturer(categoryId, queryMapping)
                .enqueue(new CustomCB<ProductsResponse>(rootViewRelativeLayout));
    }
}
