package com.mart.onestopkitchen.ui.activity;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ashraful on 1/26/2016.
 */
public class CyberSourceActivity extends BaseActivity {

    @BindView(R.id.wv_cyber_source)
    WebView cyberSourceWebView;
    private String url = AppConstants.BASE_URL + "/checkout/OpcCompleteRedirectionPayment";
    private String toolbarTitle;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cyber_source);
        ButterKnife.bind(this);

        setWebViewRequiredInfo();
        toolbarTitle = getString(R.string.cyber_source_payment);
        setToolbar();
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(toolbarTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    private void setWebViewRequiredInfo() {
        cyberSourceWebView.getSettings().setJavaScriptEnabled(true);
        cyberSourceWebView.getSettings().setDomStorageEnabled(true);
        cyberSourceWebView.getSettings().setBuiltInZoomControls(true);
        cyberSourceWebView.getSettings().setDisplayZoomControls(false);
        cyberSourceWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url, NetworkUtil.getHeaders());
                return false;
            }
        });
        cyberSourceWebView.loadUrl(url, NetworkUtil.getHeaders());
    }

}
