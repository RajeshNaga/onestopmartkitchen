package com.mart.onestopkitchen.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.quickblox.auth.QBAuth;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.model.QBUser;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.service.PreferenceService;
import com.mart.onestopkitchen.utils.Language;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import butterknife.Unbinder;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.NoSubscriberEvent;

import static com.mart.onestopkitchen.chat.utils.ConstsKt.USER_DEFAULT_PASSWORD;

/**
 * Created by Ashraful on 11/5/2015.
 */


public class BaseFragment extends Fragment {
    protected Unbinder unbinder;
    protected PreferenceService preferenceService = PreferenceService.getInstance();
    private Toast mToast;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initQB();
        if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.MYANMAR)) {
            Objects.requireNonNull(getContext()).setTheme(R.style.BurmeseFont);
        } else {
            Objects.requireNonNull(getContext()).setTheme(R.style.EnglishFont);
        }
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStart() {
        hideKeyboard();
        super.onStart();
    }

    public void hideKeyboard() {
        View view = Objects.requireNonNull(getActivity()).getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showKeyboard() {
        if (null != getActivity()) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }
    }

    @Override
    public void onResume() {
        showLogoutOpt();
        hideKeyboard();
        checkEventBusRegistration();
        initQB();
        super.onResume();
    }

    void showLogoutOpt() {
        if (!((MyApplication) Objects.requireNonNull(getActivity()).getApplicationContext()).isLogoutOptionShow) {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

            if (PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.VERIFIED_DONE) || PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.REGISTER_DONE))
                EventBus.getDefault().post("-1");
        }
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void checkEventBusRegistration() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void onEvent(NoSubscriberEvent noSubscriberEvent) {

    }

    public void onEvent(ClassCastException noSubscriberEvent) {

    }

    /*public String getAppVersionName(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }*/


    /*public void goMenuItemFragment(Fragment fragment) {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment).addToBackStack(null).commit();
    }*/


    void gotoNewFragment(Fragment fragment) {
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, fragment).addToBackStack(null).commit();
        }
    }

    public void showSnack(String message) {
        Snackbar.make(Objects.requireNonNull(getView()), message, Snackbar.LENGTH_LONG).show();
    }


    public void showToast(String msg) {
        try {
            if (mToast != null) {
                mToast.cancel();
            }
            mToast = Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG);
            mToast.show();
        } catch (Exception ignored) {
        }
    }

    protected void showProgress(@NotNull RelativeLayout layout) {
        RelativeLayout.LayoutParams params = new RelativeLayout
                .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        ProgressWheel progress = (ProgressWheel) getLayoutInflater().inflate(R.layout.materialish_progressbar, null);
        layout.addView(progress, params);
        progress.spin();
    }

    public LinearLayoutManager getLinearLayoutManager(int orientation) {
        return new LinearLayoutManager(getActivity(), orientation, false);
    }

    boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    public String getText(@NotNull AppCompatEditText editText) {
        String value = Objects.requireNonNull(editText.getText()).toString().trim();
        return value.isEmpty() ? "" : value;
    }

    public boolean checkDeviceFingerPrintAvailability(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED && context.getSystemService(FingerprintManager.class).isHardwareDetected();
        } else {
            return FingerprintManagerCompat.from(context).isHardwareDetected();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setRequiredField(@NotNull List<TextInputLayout> textInputLayouts) {
        for (TextInputLayout textLabel : textInputLayouts)
            textLabel.setHint(setStarToLabel(Objects.requireNonNull(textLabel.getHint()).toString()));
    }

    void setRequiredFieldWithTextView(@NotNull List<TextView> textviews) {
        for (TextView txtLabel : textviews)
            txtLabel.setText(setStarToLabel(Objects.requireNonNull(txtLabel.getText()).toString()));
    }

    @NonNull
    private SpannableStringBuilder setStarToLabel(String text) {
        String colored = " *";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(text);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return builder;
    }

    public void moveToHomeFragment() {
        if (getFragmentManager() != null) {
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getFragmentManager().beginTransaction().replace(R.id.container, new HomePageFragment()).commit();
        }
    }

    int getBatteryPercentage(@NotNull Context context) {
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);
        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;
        float batteryPct = level / (float) scale;
        return (int) (batteryPct * 100);
    }

    QBUser getUserDetails() {
        QBUser qbUser = new QBUser();
        String userName = "", fullname = "";

        if (null != PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME)
                && !PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME).isEmpty()) {

            userName = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME);

            if (null != PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT) && !PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT).isEmpty()) {
                fullname = PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_NAME_TXT);
            } else {
                fullname = "OneStopMart";
            }
        } else {
            userName = NetworkUtil.getDeviceId();
        }

        if (fullname.isEmpty()) {
            fullname = userName;
        }

        StringifyArrayList tags = new StringifyArrayList<>();
        tags.add("User");
        qbUser.setLogin(userName);
        qbUser.setFullName(fullname);
        qbUser.setPassword(USER_DEFAULT_PASSWORD);
        qbUser.setTags(tags);

        return qbUser;
    }


    private void initQB() {
        QBAuth.createSession(getUserDetails());
    }

}
