package com.mart.onestopkitchen.ui.fragment.addaddres;

/**
 * Created by Ashraful on 11/27/2015.
 */
public class KeyValuePairNew {
    private Object Value;
    private String Key;

    public KeyValuePairNew(String key, Object value) {
        this.Value = value;
        this.Key = key;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public Object getValue() {
        return Value;
    }

    public void setValue(Object value) {
        Value = value;
    }
}
