package com.mart.onestopkitchen.ui.selectcountry;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.selectcountry.model.DataItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CountryListSelectionAdapter extends RecyclerView.Adapter<CountryListSelectionAdapter.ViewHolder> {


    int position = 0;
    private Context mContext;
    private AdapterListener listener;
    private List<DataItem> listModels;
    private List<DataItem> listModelsFiltered;
    private int[] listOfImages;
    private List<DataItem> overSeaList = null;

   /* public CountryListSelectionAdapter(AdapterListener mContext) {
        this.listener = mContext;
        this.mContext = mContext.getAppContext();
        this.listModels = new ArrayList<>(0);
    }*/

    public CountryListSelectionAdapter(AdapterListener mContext, int[] listOfFlags, List<DataItem> items) {
        this.listener = mContext;
        //this.mContext = mContext.getAppContext();
        this.listModels = new ArrayList<>(0);
        this.listOfImages = listOfFlags;
        this.overSeaList = items;
    }

    public CountryListSelectionAdapter(AdapterListener context) {
        this.listener = context;
        this.mContext = context.getAppContext();
        this.listModels = new ArrayList<>(0);
        //this.listener = context;

    }

    public void loadData(List<DataItem> listModels) {
        listener.noRecordFound(listModels);
        if (listModels != null)
            this.listModels = listModels;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_country_list, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DataItem countryListModel = listModels.get(position);
        try {
            holder.onItemBind(countryListModel);
            holder.txtCountyName.setText(countryListModel.getCountryName() + "  (" + countryListModel.getCountryCode() + ")");
            //holder.imgCounty.setImageResource(AppConstant.listOfFlags[countryListModel.getLocalImg()]);
            holder.imgCounty.setImageResource(countryListModel.getLocalImg());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_county)
        ImageView imgCounty;
        @BindView(R.id.txt_county_name)
        TextView txtCountyName;
        @BindView(R.id.lin_county_select)
        LinearLayout linCountySelect;
        private DataItem countryListModel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.lin_county_select)
        public void onViewClicked() {
            listener.onItemClick(countryListModel);
            /*LoginFragment fragment = new LoginFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("countrydata", countryListModel);
            fragment.setArguments(bundle);
            AppCompatActivity activity = (AppCompatActivity) itemView.getContext();
            activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();*/
        }

        public void onItemBind(DataItem countryListModel) {
            this.countryListModel = countryListModel;
        }
    }
}
