package com.mart.onestopkitchen.ui.adapter;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.loginuser.DeviceInformation;
import com.mart.onestopkitchen.model.Category;
import com.mart.onestopkitchen.model.ProductService;
import com.mart.onestopkitchen.model.ViewType;
import com.mart.onestopkitchen.products.view.ProductListFragment;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Arif Islam on 23-Feb-17.
 */

public class CategoryListAdapter extends RecyclerView.Adapter {
    private List<Category> productList;
    private Context context;
    private OnItemClickListener productClickListener;
    private OnItemClickUserDetails userDetails;
    private Fragment fragment;
    private boolean isLongPressed;
    private PopupWindow popupMessage;

   /* private View.OnTouchListener speakTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View pView, MotionEvent pEvent) {
            pView.onTouchEvent(pEvent);
            SimpleTooltip.Builder tooltip = new SimpleTooltip.Builder(context);
            if (pEvent.getAction() == MotionEvent.ACTION_UP) {
                // if (isLongPressed) {
                isLongPressed = false;
                tooltip.anchorView(pView)
                        .gravity(Gravity.END)
                        .dismissOnOutsideTouch(false)
                        .dismissOnInsideTouch(true)
                        .contentView(R.layout.custom_tool_tip)
                        .modal(true)
                        .build()
                        .show();
                //  } else tooltip.build().dismiss();
            }
            return false;
        }
    };*/

    public CategoryListAdapter(OnItemClickUserDetails itemClickUserDetails, Context context, Fragment fragment) {
        this.userDetails = itemClickUserDetails;
        this.context = context;
        this.productList = new ArrayList<>();
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_custom, parent, false);
        return new ProductSummaryHolder(itemView);

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder bindViewHolder, final int position) {
        final Category productModel = productList.get(position);
        if (productModel != null) {
            final ProductSummaryHolder holder = (ProductSummaryHolder) bindViewHolder;
            if (productModel.getId() == -1) {
                holder.linLine.setVisibility(View.VISIBLE);
            }
            holder.subName.setText(AppUtils.capitalizeWords(productModel.getName()));

           /* recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (popupMessage != null && popupMessage.isShowing()) {
                        popupMessage.dismiss();
                    }
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (popupMessage != null && popupMessage.isShowing()) {
                        popupMessage.dismiss();
                    }
                }
            });*/
            holder.subName.setOnClickListener(new CategoryonClicklistener(productModel));
            holder.subName.setOnLongClickListener(view -> {
                if (!isLongPressed) {
                    if (popupMessage != null && popupMessage.isShowing()) {
                        popupMessage.dismiss();
                    }
                    isLongPressed = true;
                }
                return true;
            });
            holder.subName.setOnTouchListener((view, motionEvent) -> {
                view.onTouchEvent(motionEvent);
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (popupMessage != null && popupMessage.isShowing()) {
                        popupMessage.dismiss();
                    }
                } else {
                    if (productModel.getId() > 0)
                        if (isLongPressed) {
                            isLongPressed = false;
                            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View layoutOfPopup = inflater.inflate(R.layout.custom_tool_tip, null);
                            popupMessage = new PopupWindow(layoutOfPopup, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            popupMessage.setContentView(layoutOfPopup);
                            popupMessage.setOutsideTouchable(true);
                            popupMessage.setFocusable(true);
                            // popupMessage.showAtLocation(view, Gravity.RIGHT, 0, 0);
                        }
                }
                return false;
            });
            /*holder.txtBurm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userDetails.onItemClick(-10);
                }
            });
            holder.txtEng.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userDetails.onItemClick(-11);
                }
            });*/
            //  holder.customList.setGroupIndicator(null);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return ViewType.LIST;
    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    private void gotoProductListPage(Category category) {
        ((MainActivity) context).closeDrawer();

        if (fragment.getFragmentManager() != null) {
            fragment.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        if (fragment.getFragmentManager() != null) {
            ((MyApplication) context.getApplicationContext()).toolBarTitle = new ArrayList<>(0);
            fragment.getFragmentManager().beginTransaction()
                    //.replace(R.id.container, ProductListFragmentFor3_8.newInstance(category.getName(), category.getId()))
                    .replace(R.id.container, ProductListFragment.getInstance(category.getId(), category.getName(), 0))
                    // .replace(R.id.container, AddSubMenuFragment.newInstance(category))
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void loadData(List<Category> list) {
        this.productList = list;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener {
        void onItemClick(View view, Category category);
    }

    public interface OnItemClickUserDetails {
        void onItemClick(int pos);
    }

    class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.lin_line)
        LinearLayout linLine;
        @BindView(R.id.subName)
        TextView subName;

        ProductSummaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (productClickListener != null) {
                productClickListener.onItemClick(v, productList.get(getAdapterPosition()));
            }
        }
    }

    private class CategoryonClicklistener implements View.OnClickListener {
        Category category;

        private CategoryonClicklistener(Category category) {
            this.category = category;
        }

        @Override
        public void onClick(View v) {
            if (category.getId() < 0) {
                ((MainActivity) context).closeDrawer();
                userDetails.onItemClick(category.getId());
                return;
            }

            ProductService.productId = category.getId();
            if (new DeviceInformation().getDeviceInfo().getCheckInterNet())
                gotoProductListPage(category);
        }
    }

  /*  private String capitalize(String text) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(text);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }
        return capMatcher.appendTail(capBuffer).

        toString();
    }*/

    public void releaseObj() {
        if (null != productClickListener) {
            productClickListener = null;
        }
        if (null != userDetails) {
            userDetails = null;
        }
        if (null != productList) {
            productList = null;
        }
        if (null != fragment) {
            fragment = null;
        }
        if (null != popupMessage) {
            popupMessage = null;
        }
        if (null != context) {
            context = null;
        }

    }
}
