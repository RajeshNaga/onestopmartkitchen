package com.mart.onestopkitchen.ui.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mart.onestopkitchen.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchPopUpWindowAdapter extends RecyclerView.Adapter<SearchPopUpWindowAdapter.ViewHolder> {
    private List<String> stringList;
    private OnItemClick onItemClick;

    public SearchPopUpWindowAdapter(OnItemClick onItemClicks) {
        this.stringList = new ArrayList<>();
        this.onItemClick = onItemClicks;
    }

    public void loadData(List<String> strings) {
        this.stringList = strings;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_pop_up_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        // if (i == 0)
        //      viewHolder.txt_re.setVisibility(View.VISIBLE);
        viewHolder.txt.setText(stringList.get(i));
        viewHolder.onItemBind(stringList.get(i));
    }

    @Override
    public int getItemCount() {
        return stringList == null ? 0 : stringList.size();
    }


    public interface OnItemClick {
        void onClickViewFromAdapter(String s);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt)
        TextView txt;
        private String string;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.txt)
        public void onViewClicked() {
            onItemClick.onClickViewFromAdapter(string);
        }

        public void onItemBind(String s) {
            this.string = s;
        }
    }
}
