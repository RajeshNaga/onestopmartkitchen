package com.mart.onestopkitchen.ui.activity;

import android.content.Context;
import android.os.Bundle;

import com.mart.onestopkitchen.utils.ContextWrapper;
import com.mart.onestopkitchen.utils.Language;

import java.util.Locale;


public class LanguageBaseActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        // .. create or get your new Locale object here.
//        preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, Language.ITALIAN);
        Locale newLocale = new Locale(Language.ENGLISH);

        Context context = ContextWrapper.wrap(newBase, newLocale);
        super.attachBaseContext(context);
    }

}
