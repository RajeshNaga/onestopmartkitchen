package com.mart.onestopkitchen.ui.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.okdollar.paymentgateway.OKDollar;
import com.okdollar.paymentgateway.OKListener;
import com.okdollar.paymentgateway.OKTransactionSuccessDetails;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OkDollarPayment extends BaseFragment {

    private static final String TAG = OkDollarPayment.class.getSimpleName();
    @BindView(R.id.btn_payment)
    Button btn_payment;
    OKListener okListener = new OKListener() {
        @Override
        public void onSuccess(OKTransactionSuccessDetails okTransactionSuccessDetails) {
            Log.e(TAG, ":onSuccess---okTransactionSuccessDetails-: " + okTransactionSuccessDetails.getAmount() + "--" + okTransactionSuccessDetails.getAge());
        }

        @Override
        public void onFailure(String className) {

        }
    };
    private String countryCode = "+95";
    private String phoneNumber = "09768077189";
    private String amount = "1.0";
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.okdollar_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        progressBar = new ProgressBar(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((BaseActivity) getActivity()).getSupportActionBar().setLogo(null);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);

        getActivity().setTitle("Pay with OK$");

        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OKDollar.init(getActivity())
                        .setCountryCode(countryCode) // e.g. +95
                        .setDestinationMobileNumber(phoneNumber) // e.g. 09976656086
                        .setAmount(Double.parseDouble(amount)) // e.g. 1.0
                        .setListener(okListener)
                        .build();
            }
        });
    }
}
