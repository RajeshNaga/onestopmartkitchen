package com.mart.onestopkitchen.ui.selectcountry;

import com.mart.onestopkitchen.ui.selectcountry.model.DataItem;

public interface CountryListener {
    void onSelectedCountry(DataItem bundle);
}
