package com.mart.onestopkitchen.ui.fragment.helpandsupport

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import butterknife.ButterKnife
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.model.HelpSupportDataItem
import com.mart.onestopkitchen.model.HelpSupportModel
import com.mart.onestopkitchen.networking.CustomCB
import com.mart.onestopkitchen.networking.RetroClient
import com.mart.onestopkitchen.ui.adapter.HelpSupportAdapter
import com.mart.onestopkitchen.ui.fragment.BaseFragment
import com.quickblox.users.model.QBUser
import de.greenrobot.event.EventBus
import kotlinx.android.synthetic.main.fragment_help_support.*
import java.net.URLEncoder
import java.util.*

/**
 * Created By Akash Garg-14-Feb-2019
 */

class HelpAndSupportFragment : BaseFragment(), HelpSupportAdapter.CallToHelpSupport {
    private val TAG = "HelpAndSupportFragment"
    private lateinit var user: QBUser
    var phoneNumber = ""

    companion object {
        private const val MY_PERMISSIONS_REQUEST_CALL_PHONE = 1
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_help_support, container, false)
        unbinder = ButterKnife.bind(this, view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Objects.requireNonNull<FragmentActivity>(activity).title = getString(R.string.help_and_support)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        callGetHelpSupportWebservice()
    }

    private fun callGetHelpSupportWebservice() {
        RetroClient.getApi().helpSupport.enqueue(CustomCB<HelpSupportModel>(cordinateLayout))
    }

    fun onEvent(response: HelpSupportModel) {
        response.let {
            val adapter = response.Data?.let { it1 -> HelpSupportAdapter(activity!!, it1) }
            rvHelpSupport.layoutManager = getLinearLayoutManager(LinearLayout.VERTICAL)
            rvHelpSupport.adapter = adapter
            adapter!!.setListener(this)
            rlChat.visibility = View.GONE
        }
    }

    override fun callToHelp(data: HelpSupportDataItem) {
        when (data.Title!!) {
            "CALL US" -> {
                callUsingPhone(data.Value!!)
            }
            "SMS US" -> {
                sendSMS(data.Value!!, "")
            }
            "VIBER" -> {
                callUsingViber(data.Value!!)
            }
            "WhatsApp" -> {
                onWhatsAppClick(data.Value!!)
            }
            "EMAIL US" -> {
                sendEmail(data.Value!!)
            }
        }
    }

    private fun onWhatsAppClick(number: String) {
        onWhatsApp(number)
    }

    private fun onWhatsApp(whatsappNumber: String) {
        val packageManager = activity!!.packageManager
        val i = Intent(Intent.ACTION_VIEW)
        try {
            val url = "https://api.whatsapp.com/send?phone=" + whatsappNumber + "&text=" + URLEncoder.encode("", "UTF-8")
            i.setPackage("com.whatsapp")
            i.data = Uri.parse(url)
            if (i.resolveActivity(packageManager) != null) {
                activity!!.startActivity(i)
            }
        } catch (e: Exception) {
            Toast.makeText(activity!!, com.mart.onestopkitchen.R.string.whatsapp_not_install, Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendSMS(phoneNo: String, msg: String) {
        try {
            val uri = Uri.parse("smsto:$phoneNo")
            val intent = Intent(Intent.ACTION_SENDTO, uri)
            intent.putExtra("sms_body", msg)
            activity!!.startActivity(intent)
        } catch (ex: Exception) {
        }
    }

    private fun callUsingViber(dialNumber: String) {
        try {
            val uri = Uri.parse("tel:" + Uri.encode(dialNumber))
            val intent = Intent("android.intent.action.VIEW")
            intent.setClassName("com.viber.voip", "com.viber.voip.WelcomeActivity")
            intent.data = uri
            activity!!.startActivity(intent)
        } catch (e: Exception) {
            Toast.makeText(activity!!, R.string.viber_not_install, Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendEmail(mail: String) {
        try {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mail, null))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            activity!!.startActivity(Intent.createChooser(emailIntent, "Send email..."))
        } catch (e: Exception) {
        }
    }

    @SuppressLint("MissingPermission")
    private fun callUsingPhone(number: String) {
        phoneNumber = number
        val callPermission = Manifest.permission.CALL_PHONE
        val hasPermission = ContextCompat.checkSelfPermission(activity!!, callPermission)
        val permissions = arrayOf(callPermission)
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!, permissions, MY_PERMISSIONS_REQUEST_CALL_PHONE)
        } else {
            callPhone(phoneNumber)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_CALL_PHONE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPhone(phoneNumber)
                }
            }
        }
    }

    private fun callPhone(phoneNumber: String) {
        try {
            if (phoneNumber.isEmpty())
                return
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$phoneNumber"))
            activity!!.startActivity(intent)
            if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(intent)
            }
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    /*-------------- Chat Code at Click ---------------*/
    override fun onResume() {
        super.onResume()
        /*videoBtn?.let {
            it.setOnClickListener {
                LoginUserForChat.getInstance().videoChat(activity!!, userDetails)
            }
        }*/
    }


    /*

   private fun openChat() {
       if (SharedPrefsHelper.hasQbUser()) {
           LoginService.start(activity!!, SharedPrefsHelper.getQbUser()!!)
           OpponentsActivity.start(activity!!)
       } else {
           // LoginActivity.start(activity!!)
           val user = createUserWithEnteredData()
           signUpNewUser(user)
       }
   }

   private fun signUpNewUser(newUser: QBUser) {
       (activity!! as BaseActivity).showProgressDialog(R.string.dlg_creating_new_user)
       signUp(newUser, object : QBEntityCallback<QBUser> {
           override fun onSuccess(result: QBUser, params: Bundle) {
               SharedPrefsHelper.saveQbUser(newUser)
               loginToChat(result)
           }

           override fun onError(e: QBResponseException) {
               if (e.httpStatusCode == ERROR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                   signInCreatedUser(newUser)
               } else {
                   (activity!! as BaseActivity).hideProgressDialog()
                   longToast(R.string.sign_up_error)
               }
           }
       })
   }



   private fun loginToChat(qbUser: QBUser) {
      qbUser.password = USER_DEFAULT_PASSWORD
      user = qbUser
      startLoginService(qbUser)
  }

  private fun createUserWithEnteredData(): QBUser {
      val qbUser = QBUser()
      val userName: String = if (null != PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME)
              && PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME).isNotEmpty()) {
          PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USERNAME)
      } else {
          "ZAYOK"
      }
      qbUser.login = userName
      qbUser.fullName = userName
      qbUser.password = USER_DEFAULT_PASSWORD
      return qbUser
  }

  private fun signInCreatedUser(user: QBUser) {
      signInUser(user, object : QBEntityCallback<QBUser> {
          override fun onSuccess(result: QBUser, params: Bundle) {
              SharedPrefsHelper.saveQbUser(user)
              updateUserOnServer(user)
          }

          override fun onError(responseException: QBResponseException) {
              (activity!! as BaseActivity).hideProgressDialog()
              longToast(R.string.sign_in_error)
          }
      })
  }

  private fun updateUserOnServer(user: QBUser) {
      user.password = null
      QBUsers.updateUser(user).performAsync(object : QBEntityCallback<QBUser> {
          override fun onSuccess(updUser: QBUser?, params: Bundle?) {
              (activity!! as BaseActivity).hideProgressDialog()
              OpponentsActivity.start(activity!!)
//                finish()
          }

          override fun onError(responseException: QBResponseException?) {
              (activity!! as BaseActivity).hideProgressDialog()
              longToast(R.string.update_user_error)
          }
      })
  }

  private fun startLoginService(qbUser: QBUser) {
      val tempIntent = Intent(activity!!, LoginService::class.java)
      val pendingIntent = activity!!.createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0)
      LoginService.start(activity!!, qbUser, pendingIntent)
  }

private fun restoreChatSession() {
      if (ChatHelper.isLogged()) {
          DialogsActivity.start(activity!!)
//            finish()
      } else {
          val currentUser = getUserFromSession()
          if (currentUser == null) {
              LoginActivity.start(activity!!)
//                finish()
          } else {
              loginToChat2(currentUser)
          }
      }
  }

  private fun getUserFromSession(): QBUser? {
      val user = SharedPrefsHelper.getQbUser()
      val qbSessionManager = QBSessionManager.getInstance()
      qbSessionManager.sessionParameters?.let {
          val userId = qbSessionManager.sessionParameters.userId
          user?.id = userId
          return user
      } ?: run {
          ChatHelper.destroy()
          return null
      }
  }

  private fun loginToChat2(user: QBUser) {
      showProgressDialog(context, getString(R.string.dlg_restoring_chat_session), false)
      ChatHelper.loginToChat(user, object : QBEntityCallback<Void> {
          override fun onSuccess(result: Void?, bundle: Bundle?) {
              Log.v(TAG, "Chat login onSuccess()")
              hideProgressDialog()
              DialogsActivity.start(activity!!)
//                finish()
          }

          override fun onError(e: QBResponseException) {
              hideProgressDialog()
              Log.w(TAG, "Chat login onError(): $e")
//                showErrorSnackbar(R.string.error_recreate_session, e, View.OnClickListener { loginToChat(user) })
          }
      })
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
      super.onActivityResult(requestCode, resultCode, data)
      if (resultCode == EXTRA_LOGIN_RESULT_CODE) {

          Log.e("######", "#######-------HelpandSupport Fragment ------");
          (activity!! as BaseActivity).hideProgressDialog()

          var isLoginSuccess = false
          data?.let {
              isLoginSuccess = it.getBooleanExtra(EXTRA_LOGIN_RESULT, false)
          }

          var errorMessage = getString(R.string.unknown_error)
          data?.let {
              it.getStringExtra(EXTRA_LOGIN_ERROR_MESSAGE)?.let { msg ->
                  errorMessage = msg
              }.run {
                  Log.e(TAG, "-----errorMessage----: ${data.dataString}")
              }
          }

          if (isLoginSuccess) {
              SharedPrefsHelper.saveQbUser(user)
              signInCreatedUser(user)
          } else {
              longToast(getString(R.string.login_chat_login_error) + errorMessage)
//                userLoginEditText.setText(user.login)
//                userFullNameEditText.setText(user.fullName)
          }
      }
  }*/
}