package com.mart.onestopkitchen.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.okdollar.paymentgateway.OKDollar;
import com.okdollar.paymentgateway.OKListener;
import com.okdollar.paymentgateway.OKTransactionSuccessDetails;
import com.mart.onestopkitchen.utils.Keys;

import static com.mart.onestopkitchen.utils.Keys.PAYMENT_RESULT_DETAILS;

public class OkDollarPaymentActivity extends BaseActivity {
    public String TAG = OkDollarPaymentActivity.class.getSimpleName();
    OKListener okListener = new OKListener() {
        Intent intent = new Intent();

        @Override
        public void onSuccess(OKTransactionSuccessDetails successDetails) {
            Log.e(TAG, ":onSuccess---okTransactionSuccessDetails-: " + successDetails.getAmount() + "--" + successDetails.getAge());
            Toast.makeText(OkDollarPaymentActivity.this, "Payment Successfull", Toast.LENGTH_SHORT).show();
            intent.putExtra(PAYMENT_RESULT_DETAILS, successDetails);
            intent.putExtra(Keys.PAYMENT_RESULT, true);
            setResult(RESULT_OK, intent);
            finish();
        }

        @Override
        public void onFailure(String result) {
            Log.e(TAG, ":onFailure----: " + result);
            intent.putExtra(Keys.PAYMENT_RESULT, false);
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setOkDollarConfig();
    }

    private void setOkDollarConfig() {

        String countryCode = getIntent().getStringExtra(Keys.COUNTRY_CODE);
        String mobileNo = getIntent().getStringExtra(Keys.MOBILE_NO);
        String amount = getIntent().getStringExtra(Keys.AMOUNT);
        Log.e(TAG, ":countryCode:" + countryCode + " : mobileNo:" + mobileNo + " :amount:  " + amount);
        if (!countryCode.isEmpty() && !mobileNo.isEmpty() && !amount.isEmpty()) {
            OKDollar.init(this)
                    .setCountryCode(countryCode) // e.g. +95
                    .setDestinationMobileNumber(mobileNo) // e.g. 09976656086
                    .setAmount(Double.parseDouble(amount)) // e.g. 1.0
                    .setListener(okListener)
                    .build();
        }
    }
}
