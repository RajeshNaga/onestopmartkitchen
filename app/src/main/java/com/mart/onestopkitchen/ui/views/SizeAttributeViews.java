package com.mart.onestopkitchen.ui.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import com.google.android.material.appbar.AppBarLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.AttributeControlType;
import com.mart.onestopkitchen.model.AttributeControlValue;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.ProductAttribute;
import com.mart.onestopkitchen.networking.CustomCB;
import com.mart.onestopkitchen.networking.RetroClient;
import com.mart.onestopkitchen.networking.response.PriceResponse;
import com.mart.onestopkitchen.ui.fragment.ProductDetailFragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SizeAttributeViews extends LinearLayout {

    List<ProductAttribute> attributes;

    Context context;

    LinearLayout sizeLayout;

    LinearLayout attributeLayout;

    private onSelectSizeListener onSelectSizeListener;

    public SizeAttributeViews(Context context) {
        super(context);
    }

    public SizeAttributeViews(Context context, List<ProductAttribute> attributes, LinearLayout layout, onSelectSizeListener onSelectSizeListener) {
        super(context);
        this.onSelectSizeListener = onSelectSizeListener;
        this.context = context;
        this.attributes = attributes;
        this.sizeLayout = layout;
        generateView();

    }

    private void generateView() {
        for (ProductAttribute productAttribute : attributes) {
            if (productAttribute.getAttributeControlType() == AttributeControlType.RadioList) {
                if (productAttribute.getValues().size() > 0) {
                    onSelectSizeListener.showSizeLayout();
                    generateSizeSqauares(productAttribute);
                }

            }
        }
    }

    @SuppressLint("InflateParams")
    private void generateSizeSqauares(final ProductAttribute productAttribute) {
        final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.linear_layout, null);
        for (final AttributeControlValue value : productAttribute.getValues()) {
            LayoutParams textViewParams = new AppBarLayout.LayoutParams(60, 60);
            textViewParams.setMargins(0, 0, 10, 0);
            TextView textView = new TextView(context);
            textView.setLayoutParams(textViewParams);
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setMaxHeight((int) getResources().getDimension(R.dimen._18sdp));
            textView.setMaxWidth((int) getResources().getDimension(R.dimen._18sdp));

            GradientDrawable shapeDrawable;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.size_background, context.getTheme());
            } else {
                shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.size_background);
            }

            textView.setBackground(shapeDrawable);
            textView.setText(value.getName());
            textView.setGravity(Gravity.CENTER);
            linearLayout.addView(textView);

            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    clearSelection(linearLayout);
                    selectSizeSquare(view, value);
                    String key = getKey(productAttribute);
                    Map<String, String> valueTextPairMap = new HashMap<>();
                    valueTextPairMap.put(String.valueOf(value.getId()), key);
                    // onSelectSizeListener.expandView();
                    callPriceWebservice(onSelectSizeListener.getSelectedSize(valueTextPairMap));

                }
            });


        }
        attributeLayout = (LinearLayout) getLayoutInflater().
                inflate(R.layout.separate_layout_each_attribute_product_details, sizeLayout, false);
        attributeLayout.addView(linearLayout);
        sizeLayout.addView(attributeLayout);
    }

    public void callPriceWebservice(List<KeyValuePair> valuePair) {
        RetroClient.getApi().getUpdatedPrice(ProductDetailFragment.productModel.getId(), valuePair)
                .enqueue(new CustomCB<PriceResponse>(ProductDetailFragment.self.getView()));
    }

    private void selectSizeSquare(View view, AttributeControlValue value) {
        GradientDrawable shapeDrawable;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.size_background_blue, context.getTheme());
        } else {
            shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.size_background_blue);
        }

        view.setBackground(shapeDrawable);
    }

    private LayoutInflater getLayoutInflater() {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater;
    }

    private void clearSelection(LinearLayout layout) {
        for (int i = 0; i < layout.getChildCount(); i++) {
            TextView textView = (TextView) layout.getChildAt(i);
            GradientDrawable shapeDrawable = (GradientDrawable) textView.getBackground();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.size_background, context.getTheme());
            } else {
                shapeDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.size_background);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                textView.setBackground(shapeDrawable);
            } else {
                textView.setBackgroundDrawable(shapeDrawable);
            }
        }

    }

    public String getKey(ProductAttribute productAttribute) {
        String key = String.format("%s_%d_%d_%d", "product_attribute", productAttribute.getProductId()
                , productAttribute.getProductAttributeId(), productAttribute.getId());
        return key;
    }


    public interface onSelectSizeListener {
        List<KeyValuePair> getSelectedSize(Map<String, String> pairValue);

        void expandView();

        void showSizeLayout();
    }

}
