package com.mart.onestopkitchen.mapper

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.utils.AppUtils
import com.mart.onestopkitchen.utils.Language
import com.mart.onestopkitchen.utils.LocaleUtils
import com.mart.onestopkitchen.utils.Tracker
import com.mart.onestopkitchen.utils.callback.CurrentLocationCallback
import kotlinx.coroutines.Job
import java.util.*

class BaseActivityMapper {
    private var networkBroadcast: BroadcastReceiver? = null
    private var syncReceiver: BroadcastReceiver? = null
    private var isLocaleEnglish = true
    //    var mPreData: AppDatabase? = null
    var isLanguageChanged: Boolean = false
    val mTracker: Tracker by lazy { Tracker() }
    private val localeUtils: LocaleUtils by lazy { LocaleUtils() }
    var mActivity: AppCompatActivity? = null
    private var isEntered = false

    var job: Job? = null

    init {
        isLocaleEnglish = Language.LANG_EN.contains(Locale.getDefault().language)
    }

    fun withActivity(mActivity: AppCompatActivity) {
        this.mActivity = mActivity
    }


    fun registerNetworkBroadcast(mListner: OnNetworkChange?) {
        networkBroadcast = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                mListner?.networkUpdated(AppUtils.isReallyConnectedToInternet())
            }
        }
        try {
            //  mActivity?.unregisterReceiver(networkBroadcast)

        } catch (exception: Exception) {
            //exception.printStackTrace()
        }

        try {
            //  mActivity?.apply { mActivity?.registerReceiver(networkBroadcast as BroadcastReceiver, IntentFilter(Consts.NETWORK_BR)) }
        } catch (ex: IllegalArgumentException) {
        }
    }

    fun registerDataSyncReceiver(mListner: OnDataSynced?) {
        syncReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                mListner?.onDataSynced()
            }
        }

        try {
            mActivity?.unregisterReceiver(syncReceiver)
        } catch (ignore: IllegalArgumentException) {
            //  exception.printStackTrace()
        }

        try {
//              mActivity?.apply { mActivity?.registerReceiver(syncReceiver as BroadcastReceiver, IntentFilter(Consts.SYNC_BR)) }
        } catch (ignore: IllegalArgumentException) {
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mTracker.locationSettings?.onPermissionActivityResult(requestCode, resultCode, data)
        mTracker.bluetoothScanning?.onPermissionActivityResult(requestCode, resultCode, data)
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        mTracker.locationSettings?.onPermissionRequestResult(requestCode, permissions, grantResults)

    }

    fun enableLocationTracking(mListner: CurrentLocationCallback? = null) {
        mActivity?.apply {
            mTracker.trackLocation(this, mListner)
            if (isBluetoothNeedEnable)
                startBluetoothScanning()

            if (isWifiNeedEnable)
                startWifiScanning()
        }
    }

    private var isBluetoothNeedEnable: Boolean = false

    fun enableBluetoothTracking() {
        isBluetoothNeedEnable = true
    }

    private fun startBluetoothScanning() {
        mActivity?.apply {
            mTracker.locationSettings?.takeIf { it.isLocationEnabled(this) and it.isValidAppPermission(this) }?.apply {
                mTracker.trackBluetooth(mActivity)
            }
        }
    }

    private var isWifiNeedEnable: Boolean = false

    fun enableWifiTracking() {
        isWifiNeedEnable = true
    }

    private fun startWifiScanning() {
        mActivity?.apply {
            mTracker.locationSettings?.takeIf { it.isLocationEnabled(this) and it.isValidAppPermission(this) }?.apply {
                mTracker.trackWifi(mActivity)
            }
        }
    }

    fun disbleTracking(mListner: AppCompatActivity) {

        mTracker.disableTrack(mListner)

    }

    fun getFlagIcon(): Int {
        return if (Language.LANG_EN.contains(Locale.getDefault().language)) R.drawable.uk else R.drawable.myanmar_flag
    }


    interface OnNetworkChange {
        fun networkUpdated(isConnected: Boolean)
    }

    interface OnDataSynced {
        fun onDataSynced()
    }
}