package com.mart.onestopkitchen.baseview

import basicsetup.me.com.base.BasePresenter
import basicsetup.me.com.base.LoadBaseView
import com.mart.onestopkitchen.R
import com.mart.onestopkitchen.rxjava.ApiHelper
import com.mart.onestopkitchen.utils.AppInitialize
import retrofit2.HttpException
import java.net.ConnectException
import java.util.concurrent.TimeoutException


/**
 * Created by Thavam on 10-Nov-18.
 */
open class AbstractBasePresenter<in V : LoadBaseView> : BasePresenter<V> {

    protected var apiHelper: ApiHelper = ApiHelper.getDataHelper()
    private var view: V? = null
    override fun destroyView() {
        view = null
    }

    override fun setView(getView: V) {
        this.view = getView
    }

    fun exceptionHandler(throwable: Throwable) {
        if (throwable is HttpException) {
            if (throwable.code() <= 400 && throwable.code() <= 500) {
                val appErrorResponse = AppInitialize.getGsonSingleton().fromJson(throwable.response().errorBody()?.string(), AppErrorResponseModel::class.java)
                view?.showServerError(appErrorResponse)
            } else view?.showError(view!!.getActContext().getString(R.string.some_thing_went))
        } else if (throwable is ConnectException)
            view?.showError(view!!.getActContext().getString(R.string.no_network_connect))
        else if (throwable is TimeoutException)
            view?.showError(view!!.getActContext().getString(R.string.some_thing_went))
        else if (throwable is Exception)
            view?.showError(throwable.message!!)
        view?.hideLoading()

    }
}