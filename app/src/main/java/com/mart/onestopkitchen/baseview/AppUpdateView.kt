package com.mart.onestopkitchen.baseview

import com.mart.onestopkitchen.update.AppUpdateModel

interface AppUpdateView {
    fun appUpdateData(data: AppUpdateModel)
    fun error()
}