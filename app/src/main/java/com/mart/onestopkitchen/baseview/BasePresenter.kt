package basicsetup.me.com.base

/**
 * Created by Thavam on 10-Nov-18.
 */
interface BasePresenter<in V> {
    fun destroyView()
    fun setView(getView: V)
}