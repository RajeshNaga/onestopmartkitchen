package com.mart.onestopkitchen.baseview;

public interface ResponseHandler {
    void onSuccess(Object o, int api);

    void onErrorBody(Object s, int api);

    void onFailure(String s);
}
