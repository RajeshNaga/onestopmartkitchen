package com.mart.onestopkitchen.baseview

interface NotificationView {
    fun noData()
    fun notificationsList()
    fun error()
}