package com.mart.onestopkitchen.baseview

import com.mart.onestopkitchen.networking.response.PickupPointAddressResponse

interface PickupPointView {
    fun loadData(pickupPoints: PickupPointAddressResponse)
    fun noDataFound()
    fun error(error: String?)

}