package com.mart.onestopkitchen.baseview

import com.google.gson.annotations.SerializedName

data class AppErrorResponseModel(
        @field:SerializedName("Message")
        val message: String? = null
)