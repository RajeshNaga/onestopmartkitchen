package basicsetup.me.com.base

import android.content.Context
import com.mart.onestopkitchen.baseview.AppErrorResponseModel

/**
 * Created by Thavam on 10-Nov-18.
 */
interface LoadBaseView {
    fun showLoading()
    fun hideLoading()
    fun showError(msg: String)
    fun showServerError(msg: AppErrorResponseModel?)
    fun getActContext(): Context
}