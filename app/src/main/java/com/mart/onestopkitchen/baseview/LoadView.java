package com.mart.onestopkitchen.baseview;

import android.content.Context;

public interface LoadView {

    Context getActivityContext();

    void showLoading();

    void hideLoading();

    void showError(String s);
}
