package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.BillingAddress;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.List;

/**
 * Created by Ashraful on 12/7/2015.
 */
public class BillingAddressResponse extends BaseResponse{

    private List<BillingAddress> ExistingAddresses;
    private BillingAddress NewAddress;
    private boolean NewAddressPreselected;

    public List<BillingAddress> getExistingAddresses() {
        return ExistingAddresses;
    }

    public void setExistingAddresses(List<BillingAddress> existingAddresses) {
        ExistingAddresses = existingAddresses;
    }

    public boolean isNewAddressPreselected() {
        return NewAddressPreselected;
    }

    public void setNewAddressPreselected(boolean newAddressPreselected) {
        NewAddressPreselected = newAddressPreselected;
    }

    public BillingAddress getNewAddress() {
        return NewAddress;
    }

    public void setNewAddress(BillingAddress newAddress) {
        NewAddress = newAddress;
    }

}
