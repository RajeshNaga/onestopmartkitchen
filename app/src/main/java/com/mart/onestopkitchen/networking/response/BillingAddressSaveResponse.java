package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 12/8/2015.
 */
public class BillingAddressSaveResponse extends BaseResponse {
    private boolean Data;
    private boolean IsDeliveryAllowed = false;

    public boolean isData() {
        return Data;
    }

    public void setData(boolean data) {
        Data = data;
    }

    public boolean isDeliveryAllowed() {
        return IsDeliveryAllowed;
    }

    public void setDeliveryAllowed(boolean deliveryAllowed) {
        IsDeliveryAllowed = deliveryAllowed;
    }
}
