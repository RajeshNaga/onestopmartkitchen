package com.mart.onestopkitchen.networking.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.PickupPointItem;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Updated by AkashGarg on 19/11/2018.
 */
public class PickupPointAddressResponse extends BaseResponse {
    @SerializedName("Warnings")
    @Expose
    private List<Object> warnings = null;
    @SerializedName("PickupPoints")
    @Expose
    private List<PickupPointItem> PickupPoints = new ArrayList<>();

    @SerializedName("AllowPickUpInStore")
    @Expose
    private Boolean allowPickUpInStore;

    @SerializedName("SuccessMessage")
    @Expose
    private Object successMessage;

    public List<PickupPointItem> getPickupPoints() {
        return PickupPoints;
    }

    public void setPickupPoints(List<PickupPointItem> pickupPoints) {
        PickupPoints = pickupPoints;
    }
}
