package com.mart.onestopkitchen.networking.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 11/30/2015.
 */
public class PriceResponse extends BaseResponse {
    private String Price;
    @SerializedName("MeasurementData")
    @Expose
    private String measurementData;
    @SerializedName("StockQuantity")
    @Expose
    private Integer stockQuantity;

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getMeasurementData() {
        return measurementData;
    }

    public void setMeasurementData(String measurementData) {
        this.measurementData = measurementData;
    }
}
