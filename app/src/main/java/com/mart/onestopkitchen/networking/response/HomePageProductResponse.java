package com.mart.onestopkitchen.networking.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 11/9/2015.
 */
public class HomePageProductResponse extends BaseResponse implements Serializable {

    @SerializedName("Data")
    @Expose
    private List<ProductModel> Data = new ArrayList<>();

    public List<ProductModel> getData() {
        return Data;
    }

    public void setData(List<ProductModel> data) {
        Data = data;
    }
}
