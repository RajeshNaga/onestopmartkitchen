package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.CartProduct;
import com.mart.onestopkitchen.model.ProductAttribute;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.List;

/**
 * Created by Ashraful on 11/30/2015.
 */
public class CartProductListResponse extends BaseResponse {
    private List<CartProduct> Items;
    private List<ProductAttribute> CheckoutAttributes;
    private com.mart.onestopkitchen.model.OrderReviewData OrderReviewData;
    private OrderTotalResponse OrderTotalResponseModel;
    private int Count;
    private boolean CanContinue = true;


    public boolean isCanContinue() {
        return CanContinue;
    }

    public void setCanContinue(boolean canContinue) {
        CanContinue = canContinue;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }


    public List<ProductAttribute> getCheckoutAttributes() {
        return CheckoutAttributes;
    }

    public void setCheckoutAttributes(List<ProductAttribute> checkoutAttributes) {
        CheckoutAttributes = checkoutAttributes;
    }

    public com.mart.onestopkitchen.model.OrderReviewData getOrderReviewData() {
        return OrderReviewData;
    }

    public void setOrderReviewData(com.mart.onestopkitchen.model.OrderReviewData orderReviewData) {
        OrderReviewData = orderReviewData;
    }

    public List<CartProduct> getItems() {
        return Items;
    }

    public void setItems(List<CartProduct> items) {
        Items = items;
    }

    public OrderTotalResponse getOrderTotalResponseModel() {
        return OrderTotalResponseModel;
    }

    public void setOrderTotalResponseModel(OrderTotalResponse orderTotalResponseModel) {
        OrderTotalResponseModel = orderTotalResponseModel;
    }

    @Override
    public String toString() {
        return "CartProductListResponse{" +
                "Items=" + Items +
                ", CheckoutAttributes=" + CheckoutAttributes +
                ", OrderReviewData=" + OrderReviewData +
                ", OrderTotalResponseModel=" + OrderTotalResponseModel +
                ", Count=" + Count +
                '}';
    }
}
