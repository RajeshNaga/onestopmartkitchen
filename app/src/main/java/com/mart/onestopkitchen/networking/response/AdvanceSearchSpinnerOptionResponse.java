package com.mart.onestopkitchen.networking.response;

import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.AdvanceSearchSpinnerOption;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.List;

/**
 * Created by bs156 on 20-Feb-17.
 */
public class AdvanceSearchSpinnerOptionResponse extends BaseResponse {
    @SerializedName("Categories")
    private List<AdvanceSearchSpinnerOption> categories;

    @SerializedName("Manufacturers")
    private List<AdvanceSearchSpinnerOption> manufacturer;

    public List<AdvanceSearchSpinnerOption> getCategories() {
        return categories;
    }

    public void setCategories(List<AdvanceSearchSpinnerOption> categories) {
        this.categories = categories;
    }

    public List<AdvanceSearchSpinnerOption> getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(List<AdvanceSearchSpinnerOption> manufacturer) {
        this.manufacturer = manufacturer;
    }
}
