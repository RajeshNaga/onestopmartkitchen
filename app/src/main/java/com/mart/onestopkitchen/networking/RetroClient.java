package com.mart.onestopkitchen.networking;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.utils.AppConstants;
import com.mart.onestopkitchen.utils.Keys;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetroClient {

    private RetroClient() {

    }

    @NotNull
    public static Api getDynamicUrlApi(String url) {
        return getClient(url).create(Api.class);
    }

    @NotNull
    public static Api getApi() {
        return getClient(AppConstants.BASE_URL).create(Api.class);
    }

    @NotNull
    private static Retrofit getClient(String s) {
        Gson gson = new GsonBuilder().serializeNulls()
                .setLenient()
                .create();
        return new Retrofit.Builder()
                .baseUrl(s)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getOkHttp())
                .build();
    }

    @NotNull
    private static OkHttpClient getOkHttp() {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.connectTimeout(200, TimeUnit.SECONDS);
        okHttpBuilder.readTimeout(200, TimeUnit.SECONDS);
        okHttpBuilder.writeTimeout(200, TimeUnit.SECONDS);
        okHttpBuilder.followSslRedirects(true);
        okHttpBuilder.addInterceptor(getInterceptor());
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpBuilder.addNetworkInterceptor(loggingInterceptor);
        }
        return okHttpBuilder.build();
    }

    @NotNull
    private static Interceptor getInterceptor() {
        return chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder();
            if (!NetworkUtil.getToken().isEmpty()) {
                builder.addHeader(Keys.HEADER_KEY_TOKEN, NetworkUtil.getToken());
            }
            Request request = builder
                    .addHeader("Content-Type", "application/json")
                    .addHeader(Keys.HEADER_KEY_DEVICE_ID, NetworkUtil.getDeviceId())
                    .addHeader(Keys.HEADER_KEY_NST, NetworkUtil.getNst())
                    .addHeader("User-Agent", BuildConfig.APPLICATION_ID + "/" + BuildConfig.VERSION_NAME)
                    .method(original.method(), original.body())
                    .build();
            if (BuildConfig.DEBUG) {
                Log.d("NetworkHeader--> ", "Token: " + request.header(Keys.HEADER_KEY_TOKEN) +
                        "\n DeviceId: " + request.header(Keys.HEADER_KEY_DEVICE_ID) + " \n NST: " + request.header(Keys.HEADER_KEY_NST));
            }
            return chain.proceed(request);
        };

    }

    private static void setInterceptor(OkHttpClient.Builder clientBuilder) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(interceptor);
        }
    }

    /*ok dollar service*/
    @NotNull
    public static Retrofit initializeRetrofitsToken(String url) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .readTimeout(6, TimeUnit.MINUTES)
                .connectTimeout(6, TimeUnit.MINUTES)
                .writeTimeout(6, TimeUnit.MINUTES)
                .followSslRedirects(true);
        setInterceptor(clientBuilder);

        return new Retrofit.Builder()
                .baseUrl(url)
                .client(clientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static class NullOnEmptyConverterFactory extends Converter.Factory {
        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, @NotNull Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return (Converter<ResponseBody, Object>) body -> {
                if (body.contentLength() == 0)
                    return null;
                return delegate.convert(body);
            };
        }
    }

    /*public void getNetworkSpeed() {

        NetworkInfo info = Connectivity.getNetworkInfo(context);
        if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            // do something
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            // check NetworkInfo subtype
            if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS) {
                // Bandwidth between 100 kbps and below
            } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE) {
                // Bandwidth between 50-100 kbps
            } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_0) {
                // Bandwidth between 400-1000 kbps
            } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_A) {
                // Bandwidth between 600-1400 kbps
            }

            // Other list of various subtypes you can check for and their bandwidth limits
            // TelephonyManager.NETWORK_TYPE_1xRTT       ~ 50-100 kbps
            // TelephonyManager.NETWORK_TYPE_CDMA        ~ 14-64 kbps
            // TelephonyManager.NETWORK_TYPE_HSDPA       ~ 2-14 Mbps
            // TelephonyManager.NETWORK_TYPE_HSPA        ~ 700-1700 kbps
            // TelephonyManager.NETWORK_TYPE_HSUPA       ~ 1-23 Mbps
            // TelephonyManager.NETWORK_TYPE_UMTS        ~ 400-7000 kbps
            // TelephonyManager.NETWORK_TYPE_UNKNOWN     ~ Unknown

        }
    }*/
}
