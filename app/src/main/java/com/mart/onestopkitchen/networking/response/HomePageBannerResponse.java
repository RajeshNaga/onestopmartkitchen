package com.mart.onestopkitchen.networking.response;

import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.ImageModel;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 11/9/2015.
 */
public class HomePageBannerResponse extends BaseResponse {
    @SerializedName("Data")
    private List<ImageModel> Data = new ArrayList<>();

    public List<ImageModel> getData() {
        return Data;
    }

    public void setData(List<ImageModel> data) {
        Data = data;
    }


}
