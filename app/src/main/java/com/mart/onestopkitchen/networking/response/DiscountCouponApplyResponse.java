package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 12/4/2015.
 */
public class DiscountCouponApplyResponse extends BaseResponse {
    private OrderTotalResponse OrderTotalResponseModel;
    private boolean Data;

    public boolean isData() {
        return Data;
    }

    public void setData(boolean data) {
        Data = data;
    }

    public OrderTotalResponse getOrderTotalResponseModel() {
        return OrderTotalResponseModel;
    }

    public void setOrderTotalResponseModel(OrderTotalResponse orderTotalResponseModel) {
        OrderTotalResponseModel = orderTotalResponseModel;
    }
}
