package com.mart.onestopkitchen.networking;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.drawerlayout.widget.DrawerLayout;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.ui.fragment.Utility;

import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bs156 on 23-Dec-16.
 */

public class CustomCB<T> implements Callback<T> {

    private ProgressWheel progressWheel;

    public CustomCB() {
        // no loading
    }

    public CustomCB(View layout) {
        this((RelativeLayout) layout);
    }

    public CustomCB(View layout, int dummy) {
        this((CoordinatorLayout) layout);
    }

    public CustomCB(RelativeLayout layout) {
        if (layout == null)
            return;
        RelativeLayout.LayoutParams params = new RelativeLayout
                .LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        ProgressWheel progressWheel = getProgressWheel();
        if (null != progressWheel)
            layout.addView(progressWheel, params);

    }

    public CustomCB(CoordinatorLayout layout) {
        if (layout == null)
            return;
        CoordinatorLayout.LayoutParams params = new CoordinatorLayout
                .LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;

        ProgressWheel progressWheel = getProgressWheel();
        layout.addView(progressWheel, params);

    }

    public CustomCB(@NonNull DrawerLayout layout) {
        DrawerLayout.LayoutParams params = new DrawerLayout
                .LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ProgressWheel progressWheel = getProgressWheel();
        layout.addView(progressWheel, params);

    }

    @SuppressLint("InflateParams")
    private ProgressWheel getProgressWheel() {
        if (progressWheel == null) {
            progressWheel = (ProgressWheel) Utility.getActivity()
                    .getLayoutInflater().inflate(R.layout.materialish_progressbar, null);
        }
        progressWheel.spin();
        return progressWheel;
    }


    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        try {
            manipulateBaseResponse(response);
            if (response.body() != null) {
                EventBus.getDefault().post(response.body());
            }
            dismissProgress();
        } finally {
            call.cancel();
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        try {
            dismissProgress();
            if (BuildConfig.DEBUG) {
                t.printStackTrace();
            }
            Log.e("###-----CustomCB--", ":exception->:  " + t.getMessage());
//            Toast.makeText(Utility.getActivity(), R.string.server_side_issue, Toast.LENGTH_SHORT).show();
        } finally {
            call.cancel();
        }
    }

    private void manipulateBaseResponse(Response<T> response) {
        try {
            BaseResponse baseResponse = (BaseResponse) response.body();
            if (baseResponse != null && baseResponse.getErrorList().length > 0) {
                //Toast.makeText(Utility.getActivity(), baseResponse.getErrorsAsFormattedString(), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ignored) {
        }
    }

    private void dismissProgress() {
        if (progressWheel != null) {
            progressWheel.stopSpinning();
        }
    }
}
