package com.mart.onestopkitchen.networking;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by Ashraful on 11/6/2015.
 */
public class BaseResponse implements Serializable {
    @SerializedName("StatusCode")
    private int StatusCode;
    @SerializedName("ErrorList")
    private String[] ErrorList;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String[] getErrorList() {
        return ErrorList;
    }

    public void setErrorList(String[] errorList) {
        ErrorList = errorList;
    }


    public String getErrorsAsFormattedString() {
        String errors = "";
        if (getErrorList().length > 0) {
            for (int i = 0; i < getErrorList().length; i++) {
                errors += "  " + (i + 1) + ": " + getErrorList()[i] + " \n";
            }
        }
        return errors;
    }

    @Override
    public String toString() {
        return "StatusCode=" + StatusCode +
                ", ErrorList=" + Arrays.toString(ErrorList);
    }
}
