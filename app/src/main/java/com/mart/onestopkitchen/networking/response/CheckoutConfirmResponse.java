package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 12/11/2015.
 */
public class CheckoutConfirmResponse extends BaseResponse {

    private long OrderId;
    private com.mart.onestopkitchen.model.PayPal PayPal;
    private int Data;
    private boolean CompleteOrder;
    private int PaymentType;

    public int getData() {
        return Data;
    }

    public void setData(int data) {
        Data = data;
    }

    public boolean isCompleteOrder() {
        return CompleteOrder;
    }

    public void setCompleteOrder(boolean completeOrder) {
        CompleteOrder = completeOrder;
    }

    public com.mart.onestopkitchen.model.PayPal getPayPal() {
        return PayPal;
    }

    public void setPayPal(com.mart.onestopkitchen.model.PayPal payPal) {
        PayPal = payPal;
    }


    public long getOrderId() {
        return OrderId;
    }

    public void setOrderId(long orderId) {
        OrderId = orderId;
    }

    public int getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(int paymentType) {
        PaymentType = paymentType;
    }
}
