package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 12/10/2015.
 */
public class ShoppingCartCheckoutAttributeApplyResponse extends BaseResponse {
    private boolean Data;
    private OrderTotalResponse OrderTotalResponseModel;

    public boolean isData() {
        return Data;
    }

    public void setData(boolean data) {
        Data = data;
    }

    public OrderTotalResponse getOrderTotalResponseModel() {
        return OrderTotalResponseModel;
    }

    public void setOrderTotalResponseModel(OrderTotalResponse orderTotalResponseModel) {
        OrderTotalResponseModel = orderTotalResponseModel;
    }
}
