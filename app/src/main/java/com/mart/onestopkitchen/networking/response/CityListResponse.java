package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.AvailableCity;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.List;

public class CityListResponse extends BaseResponse {
    List<AvailableCity> Data;

    public List<AvailableCity> getData() {
        return Data;
    }

    public void setData(List<AvailableCity> data) {
        Data = data;
    }
}
