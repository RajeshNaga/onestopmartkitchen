package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.ProductModel;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 11/27/2015.
 */
public class RelatedProductResponse extends BaseResponse {
    private List<ProductModel> Data = new ArrayList<>();

    public List<ProductModel> getData() {
        return Data;
    }

    public void setData(List<ProductModel> data) {
        Data = data;
    }
}
