package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.PaymentMethod;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.List;

/**
 * Created by Ashraful on 12/9/2015.
 */
public class PaymentMethodRetrievalResponse extends BaseResponse {
    private Object RewardPointsAmount;

    private boolean DisplayRewardPoints;
    private int RewardPointsBalance;
    private boolean UseRewardPoints;
    private List<PaymentMethod> PaymentMethods;


    public List<PaymentMethod> getPaymentMethods() {
        return PaymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        PaymentMethods = paymentMethods;
    }


    public boolean isUseRewardPoints() {
        return UseRewardPoints;
    }

    public void setUseRewardPoints(boolean useRewardPoints) {
        UseRewardPoints = useRewardPoints;
    }

    public int getRewardPointsBalance() {
        return RewardPointsBalance;
    }

    public void setRewardPointsBalance(int rewardPointsBalance) {
        RewardPointsBalance = rewardPointsBalance;
    }

    public boolean isDisplayRewardPoints() {
        return DisplayRewardPoints;
    }

    public void setDisplayRewardPoints(boolean displayRewardPoints) {
        DisplayRewardPoints = displayRewardPoints;
    }

}
