package com.mart.onestopkitchen.networking.response;

import com.google.gson.annotations.SerializedName;
import com.mart.onestopkitchen.model.ManuFacturer;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 11/9/2015.
 */
public class HomePageManufacturerResponse extends BaseResponse implements Serializable {
    @SerializedName("Data")
    private List<ManuFacturer> Data = new ArrayList<>();

    public List<ManuFacturer> getData() {
        return Data;
    }

    public void setData(List<ManuFacturer> data) {
        Data = data;
    }
}
