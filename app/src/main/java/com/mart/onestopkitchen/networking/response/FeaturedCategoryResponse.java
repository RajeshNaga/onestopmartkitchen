package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.FeaturedCategory;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashraful on 12/11/2015.
 */
public class FeaturedCategoryResponse extends BaseResponse implements Serializable {
    private List<FeaturedCategory> Data = new ArrayList<>();

    public List<FeaturedCategory> getData() {
        return Data;
    }

    public void setData(List<FeaturedCategory> data) {
        Data = data;
    }
}