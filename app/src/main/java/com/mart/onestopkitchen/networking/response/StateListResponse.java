package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.AvailableState;
import com.mart.onestopkitchen.networking.BaseResponse;

import java.util.List;

/**
 * Created by Ashraful on 12/8/2015.
 */
public class StateListResponse extends BaseResponse {
    List<AvailableState> Data;

    public List<AvailableState> getData() {
        return Data;
    }

    public void setData(List<AvailableState> data) {
        Data = data;
    }
}
