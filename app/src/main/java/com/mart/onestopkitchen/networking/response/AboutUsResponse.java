package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.AboutUsModel;
import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 11/20/2015.
 */
public class AboutUsResponse extends BaseResponse {
    private AboutUsModel Data;

    public AboutUsModel getAboutUsModel() {
        return Data;
    }

    public void setAboutUsModel(AboutUsModel Data) {
        this.Data = Data;
    }
}
