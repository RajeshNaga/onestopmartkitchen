package com.mart.onestopkitchen.networking;

import com.mart.onestopkitchen.utils.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bs156 on 09-Dec-16.
 */

public class NetworkUtil {
    private static String deviceId = "";
    private static String token = "";
    private static String nst = "";

    public static String getDeviceId() {
        if (deviceId.isEmpty() || deviceId.equals("0")) {
            deviceId = DeviceId.get();
        }
        return deviceId;
    }

    public static String getNst() {
        return nst;
    }

    public static void setNst(String nst) {
        NetworkUtil.nst = nst;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        NetworkUtil.token = token;
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> headerMap = new HashMap<>();
        if (deviceId.isEmpty()) {
            deviceId = DeviceId.get();
        }
        headerMap.put(Keys.HEADER_KEY_DEVICE_ID, deviceId);

        if (token != null && !token.isEmpty()) {
            headerMap.put(Keys.HEADER_KEY_TOKEN, token);
        }
        headerMap.put(Keys.HEADER_KEY_NST, nst);

        return headerMap;
    }
}
