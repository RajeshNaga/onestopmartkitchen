package com.mart.onestopkitchen.networking.postrequest;

/**
 * Created by Ashraful on 12/8/2015.
 */
public class ValuePost {

    private String value;
    private int VersionCode = 0;
    private String PaymentMethod = "";

    public ValuePost() {

    }

    public ValuePost(String value) {
        this.value = value;
    }

    public ValuePost(String PaymentMethod, int VersionCode) {
        this.PaymentMethod = PaymentMethod;
        this.VersionCode = VersionCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getVersionCode() {
        return VersionCode;
    }

    public void setVersionCode(int versionCode) {
        VersionCode = versionCode;
    }
}
