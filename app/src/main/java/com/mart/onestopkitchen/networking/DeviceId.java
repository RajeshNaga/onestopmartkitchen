package com.mart.onestopkitchen.networking;

import android.annotation.SuppressLint;
import android.provider.Settings;

import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.activity.SplashScreenActivity;

/**
 * Created by arif on 09-Dec-16.
 */

@SuppressLint("HardwareIds")
public class DeviceId {
    private static final String instance;

    static {
        if (MainActivity.self != null) {
            instance = Settings.Secure.getString(MainActivity.self.getContentResolver(), Settings.Secure.ANDROID_ID);
        } else if (SplashScreenActivity.self != null) {
            instance = Settings.Secure.getString(SplashScreenActivity.self.getContentResolver(), Settings.Secure.ANDROID_ID);
        } else {
            instance = "0";
        }
    }

    public static String get() {
        return instance;
    }
}
