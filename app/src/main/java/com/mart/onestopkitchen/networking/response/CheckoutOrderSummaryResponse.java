package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 12/11/2015.
 */
public class CheckoutOrderSummaryResponse extends BaseResponse {
    private OrderTotalResponse OrderTotalModel;
    private CartProductListResponse ShoppingCartModel;
    private String MerchantNumber = "";

    public String getMerchantNumber() {
        return MerchantNumber;
    }

    public void setMerchantNumber(String merchantNumber) {
        MerchantNumber = merchantNumber;
    }

    public OrderTotalResponse getOrderTotalModel() {
        return OrderTotalModel;
    }

    public void setOrderTotalModel(OrderTotalResponse orderTotalModel) {
        OrderTotalModel = orderTotalModel;
    }

    public CartProductListResponse getShoppingCartModel() {
        return ShoppingCartModel;
    }

    public void setShoppingCartModel(CartProductListResponse shoppingCartModel) {
        ShoppingCartModel = shoppingCartModel;
    }

    @Override
    public String toString() {
        return "CheckoutOrderSummaryResponse{" +
                "OrderTotalModel=" + OrderTotalModel +
                ", ShoppingCartModel=" + ShoppingCartModel +
                ", MerchantNumber='" + MerchantNumber + '\'' +
                '}';
    }
}
