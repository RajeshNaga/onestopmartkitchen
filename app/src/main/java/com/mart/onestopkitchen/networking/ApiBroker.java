package com.mart.onestopkitchen.networking;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.base64imge.ImageUrlRequestModel;
import com.mart.onestopkitchen.baseview.ResponseHandler;
import com.mart.onestopkitchen.loginuser.loginnewmodel.LoginOrRegistrationModel;
import com.mart.onestopkitchen.loginuser.model.RegisterRequestModel;
import com.mart.onestopkitchen.loginuser.modelnew.requestmodel.login.LoginRequestModel;
import com.mart.onestopkitchen.model.AutoRegisterokDollar;
import com.mart.onestopkitchen.model.CustomerRegistrationInfo;
import com.mart.onestopkitchen.model.ForgetResponse;
import com.mart.onestopkitchen.model.LoginData;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.CashInRequestModel;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.filter.FilterRequestModel;
import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;
import com.mart.onestopkitchen.ui.fragment.addaddres.KeyValuePairNew;
import com.mart.onestopkitchen.utils.AppConstants;
import com.mart.onestopkitchen.utils.AppUtils;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mart.onestopkitchen.utils.AppConstants.CONTACT_SYN;
import static com.mart.onestopkitchen.utils.AppConstants.NEAR_BY_SERVICE;
import static com.mart.onestopkitchen.utils.AppConstants.OK_DOLLAR_PROFILE;

public class ApiBroker implements Callback {
    private Api api;
    private ResponseHandler handler;
    private int whichApi = 0;

    public ApiBroker(ResponseHandler handler) {
        api = MyApplication.getApi();
        this.handler = handler;
    }


    @Override
    public void onResponse(@NonNull Call call, @NonNull Response response) {
        if (response.isSuccessful())
            handler.onSuccess(response.body(), whichApi);
        else handler.onErrorBody(response.errorBody(), whichApi);
    }


    @Override
    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
        if (!AppUtils.isReallyConnectedToInternet())
            handler.onFailure(MyApplication.getAppContext().getResources().getString(R.string.no_internet));
            // else if (t instanceof TimeoutException)
            //    handler.onFailure(MyApplication.getAppContext().getResources().getString(R.string.please_try_again));
        else
            //  handler.onFailure(t.toString());
            handler.onFailure(MyApplication.getAppContext().getResources().getString(R.string.please_try_again));

    }


    public void getOtp(int api, HashMap otpData) {
        this.whichApi = api;
        Call<OtpResponseModel> call = RetroClient.getDynamicUrlApi(AppConstants.BASE_URL).getOtpStatus(otpData);
        call.enqueue(this);
    }

    public void registration(int whichApi, RegisterRequestModel otpData) {
        this.whichApi = whichApi;
        api.registerUser(otpData).enqueue(this);
    }

    public void getProfile(int whichApi, HashMap<String, String> profileData) {
        this.whichApi = whichApi;
        Call<JsonObject> call = RetroClient.getDynamicUrlApi(OK_DOLLAR_PROFILE).retrieveProfile(profileData);
        call.enqueue(this);
    }

    public void checkUserAlreadyRegister(int whichApi, HashMap profileData) {
        this.whichApi = whichApi;
        // Call<JsonObject> call = api.registerUser(profileData);
        //call.enqueue(this);
    }

    public void checkOkDollarUser(int whichApi, RegisterRequestModel userData) {
        this.whichApi = whichApi;
    }

    public void loginApi(int whichApi, HashMap<String, String> forgot) {
        this.whichApi = whichApi;
        Call<ForgetResponse> forgetResponseCall = api.forgetPasswordNew(forgot);
        forgetResponseCall.enqueue(this);
    }// apiBroker.loginApi(whichApi, getMobileNmber());

    public void getAvailableAddress(int whichApi) {
        this.whichApi = whichApi;
        api.getBillingAddress().enqueue(this);
    }

    public void addAddress(int whichApi, List<KeyValuePairNew> valuePairNews) {
        this.whichApi = whichApi;
        api.addAddressNew(valuePairNews).enqueue(this);
    }

    public void okDollarAutoRegistration(int whichApi, AutoRegisterokDollar valuePairNews) {
        this.whichApi = whichApi;
        api.okDollarRegistration(valuePairNews).enqueue(this);
    }


    public void normalRegistrationLocal(int whichApi, CustomerRegistrationInfo dollar) {
        this.whichApi = whichApi;
        api.preformRegistration(dollar).enqueue(this);
    }

    public void localServerLogin(int whichApi, LoginData normalRegistration) {
        this.whichApi = whichApi;
        api.performLogin(normalRegistration).enqueue(this);
    }


    public void checkLoginApi(int whichApi, LoginData userLogin) {
        this.whichApi = whichApi;
        api.performLogin(userLogin).enqueue(this);
    }

    public void categoryFilter(int whichApi, long categoryId) {
        this.whichApi = whichApi;
        api.getProductPriceList(categoryId).enqueue(this);
    }

    public void userLogin(int whichApi, LoginRequestModel loginData) {
        this.whichApi = whichApi;
        api.loginUser(loginData).enqueue(this);
    }

    public void userRegister(int whichApi, LoginRequestModel registrationData) {
        this.whichApi = whichApi;
        api.loginRegister(registrationData).enqueue(this);
    }

    public void userLoginOrRegister(int whichApi, LoginOrRegistrationModel registrationData) {
        this.whichApi = whichApi;
        api.loginOrRegister(registrationData).enqueue(this);
    }

    public void getSubcategoryDetails(int whichApi, int id) {
        this.whichApi = whichApi;
        api.getSubcategoryNew(id).enqueue(this);
    }

    public void getImageUrl(int whichApi, ImageUrlRequestModel imageUrlResponse) {
        this.whichApi = whichApi;
        RetroClient.getDynamicUrlApi(CONTACT_SYN).GetMultiImageUrlByBase64String(imageUrlResponse).enqueue(this);
    }

    public void getUserAddress(int whichApi) {
        this.whichApi = whichApi;
        api.getBillingAddress().enqueue(this);
    }

    public void nearByCashInList(int i, CashInRequestModel postData) {
        this.whichApi = i;
        RetroClient.getDynamicUrlApi(NEAR_BY_SERVICE).nearByList(postData).enqueue(this);
    }

    public void nearByCashFilter(int i, FilterRequestModel postData) {
        this.whichApi = i;
        RetroClient.getDynamicUrlApi(NEAR_BY_SERVICE).nearByList(postData).enqueue(this);
    }
}
