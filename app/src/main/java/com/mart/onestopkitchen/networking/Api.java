package com.mart.onestopkitchen.networking;

import com.google.gson.JsonObject;
import com.mart.onestopkitchen.BuildConfig;
import com.mart.onestopkitchen.barcode.BarcodeResponse;
import com.mart.onestopkitchen.base64imge.ImageUrlRequestModel;
import com.mart.onestopkitchen.base64imge.ImageUrlResponseModel;
import com.mart.onestopkitchen.contacts.model.ResponseContact;
import com.mart.onestopkitchen.loginuser.loginnewmodel.LoginOrRegistrationModel;
import com.mart.onestopkitchen.loginuser.loginnewmodel.response.LoginResponseModel;
import com.mart.onestopkitchen.loginuser.model.RegisterRequestModel;
import com.mart.onestopkitchen.loginuser.modelnew.error.RegisterResponseModel;
import com.mart.onestopkitchen.loginuser.modelnew.requestmodel.login.LoginRequestModel;
import com.mart.onestopkitchen.loginuser.modelnew.responsemodel.LoginResponseModelNew;
import com.mart.onestopkitchen.model.AddAddressResponse;
import com.mart.onestopkitchen.model.AppStartRequest;
import com.mart.onestopkitchen.model.AppThemeResponse;
import com.mart.onestopkitchen.model.AuthorizePayment;
import com.mart.onestopkitchen.model.AutoRegisterokDollar;
import com.mart.onestopkitchen.model.CategoryFeaturedProductAndSubcategoryResponse;
import com.mart.onestopkitchen.model.ChangePasswordModel;
import com.mart.onestopkitchen.model.ChangePasswordResponse;
import com.mart.onestopkitchen.model.CommonResponseDao;
import com.mart.onestopkitchen.model.ConfirmAutorizeDotNetCheckoutResponse;
import com.mart.onestopkitchen.model.ConfirmPayPalCheckoutResponse;
import com.mart.onestopkitchen.model.CustomerAddressResponse;
import com.mart.onestopkitchen.model.CustomerInfo;
import com.mart.onestopkitchen.model.CustomerOrdersResponse;
import com.mart.onestopkitchen.model.CustomerRegistrationInfo;
import com.mart.onestopkitchen.model.EditAddressResponse;
import com.mart.onestopkitchen.model.ForgetData;
import com.mart.onestopkitchen.model.ForgetResponse;
import com.mart.onestopkitchen.model.HelpSupportModel;
import com.mart.onestopkitchen.model.IsGuestCheckoutResponse;
import com.mart.onestopkitchen.model.KeyValuePair;
import com.mart.onestopkitchen.model.LoginData;
import com.mart.onestopkitchen.model.LoginResponse;
import com.mart.onestopkitchen.model.NotificationModel;
import com.mart.onestopkitchen.model.NotifyModel;
import com.mart.onestopkitchen.model.OrderDetailsResponse;
import com.mart.onestopkitchen.model.PaymentResponseModel;
import com.mart.onestopkitchen.model.RegistrationResponse;
import com.mart.onestopkitchen.model.RemoveCustomerAddressResponse;
import com.mart.onestopkitchen.model.ResponseModelAutoLogin;
import com.mart.onestopkitchen.model.SaveTransactionStatusModel;
import com.mart.onestopkitchen.model.Search;
import com.mart.onestopkitchen.model.SubCategoryProductDetails.SubCategoryProduct;
import com.mart.onestopkitchen.model.WishistUpdateResponse;
import com.mart.onestopkitchen.model.appratingandreview.AppRatingResponseModel;
import com.mart.onestopkitchen.model.appratingandreview.AppReviewAndRating;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.CashInRequestModel;
import com.mart.onestopkitchen.model.cashin.cashinrequestmodel.filter.FilterRequestModel;
import com.mart.onestopkitchen.model.cashin.cashinresponsemodel.NearbyResponseModel;
import com.mart.onestopkitchen.model.featureproduct.FeatureProductResponseModel;
import com.mart.onestopkitchen.model.otpreceived.OtpResponseModel;
import com.mart.onestopkitchen.model.ratingreview.RatingGettingModel;
import com.mart.onestopkitchen.model.ratingreview.RatingRequestModel;
import com.mart.onestopkitchen.model.ratingreview.RatingReviewRequestModel;
import com.mart.onestopkitchen.model.recentview.RecentProductsResponseModel;
import com.mart.onestopkitchen.model.searchkey.SearchResponseModel;
import com.mart.onestopkitchen.networking.postrequest.DiscountCouponRequest;
import com.mart.onestopkitchen.networking.postrequest.FacebookLogin;
import com.mart.onestopkitchen.networking.postrequest.PaypalCheckoutRequest;
import com.mart.onestopkitchen.networking.postrequest.ValuePost;
import com.mart.onestopkitchen.networking.response.AboutUsResponse;
import com.mart.onestopkitchen.networking.response.AddtoCartResponse;
import com.mart.onestopkitchen.networking.response.AdvanceSearchSpinnerOptionResponse;
import com.mart.onestopkitchen.networking.response.BillingAddressResponse;
import com.mart.onestopkitchen.networking.response.BillingAddressSaveResponse;
import com.mart.onestopkitchen.networking.response.CartProductListResponse;
import com.mart.onestopkitchen.networking.response.CategoryNewResponse;
import com.mart.onestopkitchen.networking.response.CheckoutOrderSummaryResponse;
import com.mart.onestopkitchen.networking.response.CityListResponse;
import com.mart.onestopkitchen.networking.response.DiscountCouponApplyResponse;
import com.mart.onestopkitchen.networking.response.FeaturedCategoryResponse;
import com.mart.onestopkitchen.networking.response.HomePageBannerResponse;
import com.mart.onestopkitchen.networking.response.HomePageCategoryResponse;
import com.mart.onestopkitchen.networking.response.HomePageManufacturerResponse;
import com.mart.onestopkitchen.networking.response.OrderTotalResponse;
import com.mart.onestopkitchen.networking.response.PaymentMethodRetrievalResponse;
import com.mart.onestopkitchen.networking.response.PaymentMethodSaveResponse;
import com.mart.onestopkitchen.networking.response.PickupPointAddressResponse;
import com.mart.onestopkitchen.networking.response.PriceResponse;
import com.mart.onestopkitchen.networking.response.ProductDetailResponse;
import com.mart.onestopkitchen.networking.response.ProductsResponse;
import com.mart.onestopkitchen.networking.response.ReOrderResponse;
import com.mart.onestopkitchen.networking.response.RelatedProductResponse;
import com.mart.onestopkitchen.networking.response.ShippingAddressSaveResponse;
import com.mart.onestopkitchen.networking.response.ShippingMethodRetrievalResponse;
import com.mart.onestopkitchen.networking.response.ShippingMethodSelttingResponse;
import com.mart.onestopkitchen.networking.response.ShoppingCartCheckoutAttributeApplyResponse;
import com.mart.onestopkitchen.networking.response.StateListResponse;
import com.mart.onestopkitchen.networking.response.StoreSaveResponse;
import com.mart.onestopkitchen.ui.fragment.addaddres.KeyValuePairNew;
import com.mart.onestopkitchen.update.AppUpdateModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by bs156 on 09-Dec-16.
 */

public interface Api {
    String imageSize = "600";
    String manufactureImageSize = "800";
    String queryString = "thumbPictureSize";
    String qs_price = "price";
    String qs_page_number = "pagenumber";
    String qs_spec = "specs";
    int shoppingCartTypeCart = 1;
    int shoppingCartTypeWishlist = 2;
    long cartProductId = 0;
    int VersionCode = BuildConfig.VERSION_CODE;


    @POST("AppStart")
    Call<AppThemeResponse> initApp(@Body AppStartRequest appStartRequest);

    @GET("v1/categories")
    Call<CategoryNewResponse> getNewCategory();

   /* @GET("categories")
    Call<CategoryResponse> getCategory();*/

    @GET("homepagebanner")
    Call<HomePageBannerResponse> getHomePageBanner(@Query(queryString) String query);

    @GET("homepagecategories")
    Call<HomePageCategoryResponse> getHomePageCategories(@Query(queryString) String query);

    @GET("homepageproducts")
    Call<FeatureProductResponseModel> getHomePageProducts(@Query(queryString) String query);

    @GET("catalog/homepagecategorieswithproduct/{versioncode}")
    Call<FeaturedCategoryResponse> getHomePageCategoriesWithProduct(@Path("versioncode") int VersionCode);

    @GET("homepagemanufacture")
    Call<HomePageManufacturerResponse> getHomePageManufacturer(@Query(queryString) String query);

    @GET("Category/{id}")
    Call<ProductsResponse> getProductList(@Path("id") long id, @QueryMap Map<String, String> options);

    @GET("Manufacturer/{manufacturerId}")
    Call<ProductsResponse> getProductListByManufacturer(@Path("manufacturerId") long id, @QueryMap Map<String, String> options);

    @GET("categoryfeaturedproductandsubcategory/{id}")
    Call<CategoryFeaturedProductAndSubcategoryResponse> getCategoryFeaturedProductAndSubcategory(@Path("id") long id);

    @GET("categoryfeaturedproductandsubcategory/{id}")
    Call<CategoryFeaturedProductAndSubcategoryResponse> getSubcategory(@Path("id") long id);

    @GET("productdetails/{id}")
    Call<ProductDetailResponse> getProductDetails(@Path("id") long id);

    @GET("topics/about-us")
    Call<AboutUsResponse> getAboutUs();

    @GET("topics/privacy-policy")
    Call<AboutUsResponse> getPrivacyPolicy();

    @GET("relatedproducts/{id}?thumbPictureSize=320")
    Call<RelatedProductResponse> getRelatedProducts(@Path("id") long id);

    @POST("ProductDetailsPagePrice/{productId}")
    Call<PriceResponse> getUpdatedPrice(@Path("productId") long id, @Body List<KeyValuePair> list);

    // Get shopping cart
    @POST("AddProductToCart/{productId}/{shoppingCartTypeId}")
    Call<AddtoCartResponse> addProductIntoCart(@Path("productId") long id, @Path("shoppingCartTypeId") long shoppingCartTypeId, @Body List<KeyValuePair> list);

    @GET("ShoppingCart")
    Call<CartProductListResponse> getShoppingCart();

    @POST("ShoppingCart/UpdateCart")
    Call<CartProductListResponse> updateCartProductList(@Body List<KeyValuePair> list);

    @GET("productdetails/{id}")
    Call<ProductDetailResponse> getCartItemProductDetailResponse(@Path("id") long id, @QueryMap Map<String, String> options);

    @POST("ShoppingCart/ApplyDiscountCoupon")
    Call<DiscountCouponApplyResponse> applyDiscountCoupon(@Body DiscountCouponRequest request);

    @GET("ShoppingCart/RemoveDiscountCoupon")
    Call<BaseResponse> removeDiscountCoupon();

    @GET("ShoppingCart/OrderTotal")
    Call<OrderTotalResponse> getOrderTotal();

    @POST("ShoppingCart/applycheckoutattribute")
    Call<ShoppingCartCheckoutAttributeApplyResponse> applyCheckoutAttribute(@Body List<KeyValuePair> list);

    @GET("checkout/billingform")
    Call<BillingAddressResponse> getBillingAddress();

    @GET("country/getstatesbycountryid/{countryId}")
    Call<StateListResponse> getStates(@Path("countryId") String id);

    @GET("country/getshippingallowedcitiesbystateid/{stateId}")
    Call<CityListResponse> getCityList(@Path("stateId") String stateId);

    @POST("checkout/checkoutsaveadress/1")
    Call<BillingAddressSaveResponse> saveBillingAddress(@Body List<KeyValuePair> list);

    @POST("checkout/checkoutsaveadressid/1")
    Call<BillingAddressSaveResponse> saveBillingAddressFromAddress(@Body ValuePost valuePost);

    /*@GET("checkout/savecheckoutpickuppoint")
    Call<StoreSaveResponse> saveStoreAddress(@Query("pickupPointId") String pickupPointId);*/

   /* @GET("checkout/getCheckoutPickupPoints")
    Call<PickupPointAddressResponse> getPickupPoints();*/


    @GET("checkout/getCheckoutPickupPoints")
    Observable<PickupPointAddressResponse> getPickupPoints();

    @GET("checkout/savecheckoutpickuppoint")
    Observable<StoreSaveResponse> saveStoreAddress(@Query("pickupPointId") String pickupPointId);

    @POST("checkout/checkoutsaveadressid/2")
    Call<ShippingAddressSaveResponse> saveShippingAddressFromAddress(@Body ValuePost valuePost);

    @POST("checkout/checkoutsaveadress/2")
    Call<ShippingAddressSaveResponse> saveShippingAddressByForm(@Body List<KeyValuePair> list);

    @GET("checkout/checkoutgetshippingmethods")
    Call<ShippingMethodRetrievalResponse> getShippingMethod();

    @POST("checkout/checkoutsetshippingmethod")
    Call<ShippingMethodSelttingResponse> setShippingMethod(@Body ValuePost valuePost);

    @GET("checkout/checkoutgetpaymentmethod")
    Call<PaymentMethodRetrievalResponse> getPaymentMethod();

    @POST("checkout/checkoutsavepaymentmethod")
    Call<PaymentMethodSaveResponse> saveCheckoutPaymentMethod(@Body ValuePost valuePost);

    // Customer
    @GET("customer/info")
    Call<CustomerInfo> getCustomerInfo();

    @POST("customer/info")
    Call<CustomerInfo> saveCustomerInfo(@Body CustomerInfo customerInfo);

    @POST("login")
    Call<LoginResponse> performLogin(@Body LoginData loginData);


    @POST("customer/passwordrecovery")
    Call<ForgetResponse> forgetPassword(@Body ForgetData forgetData);

    @POST("customer/register")
    Call<RegistrationResponse> preformRegistration(@Body CustomerRegistrationInfo customerRegistrationInfo);

    @GET("shoppingcart/checkoutorderinformation")
    Call<CheckoutOrderSummaryResponse> getCheckoutOrderSummary();

    @POST("checkout/checkoutcomplete")
    Observable<PaymentResponseModel> confirmCheckout(@Body SaveTransactionStatusModel request);

    @POST("checkout/checkpaypalaccount")
    Call<ConfirmPayPalCheckoutResponse> confirmPayPalCheckout(@Body PaypalCheckoutRequest request);

    @GET("customer/addresses")
    Call<CustomerAddressResponse> getCustomerAddresses();

    @POST("customer/address/edit/{id}")
    Call<EditAddressResponse> editAddress(@Path("id") int id, @Body List<KeyValuePair> list);

    @POST("customer/address/add")
    Call<AddAddressResponse> addAddress(@Body List<KeyValuePair> keyValuePairs);

    @GET("customer/address/remove/{id}")
    Call<RemoveCustomerAddressResponse> removeCustomerAddresses(@Path("id") int id);

    @GET("order/customerorders")
    Call<CustomerOrdersResponse> getCustomerOrders();

    @GET("order/details/{id}")
    Call<OrderDetailsResponse> getOrderDetails(@Path("id") int id);

    @GET("order/reorder/{id}")
    Call<ReOrderResponse> getReOrder(@Path("id") int id);

    @POST("customer/changepass")
    Call<ChangePasswordResponse> changePassword(@Body ChangePasswordModel changePassword);

    @GET("checkout/opccheckoutforguest")
    Call<IsGuestCheckoutResponse> getIsGuestCheckout();

    @GET("shoppingCart/wishlist")
    Call<CartProductListResponse> getWishList();

    @POST("facebookLogin")
    Call<LoginResponse> loginUsingFaceBook(@Body FacebookLogin facebookLogin);

    @POST("ShoppingCart/UpdateWishlist")
    Call<WishistUpdateResponse> updateWishList(@Body List<KeyValuePair> keyValuePairs);

    @POST("ShoppingCart/AddItemsToCartFromWishlist")
    Call<CartProductListResponse> addItemsToCartFromWishList(@Body List<KeyValuePair> keyValuePairs);

    @POST("ShoppingCart/AddItemsToCartFromWishlist")
    Call<CartProductListResponse> addAllItemsToCartFromWishList(@Body List<KeyValuePair> keyValuePairs);

    @POST("checkout/checkauthorizepayment")
    Call<ConfirmAutorizeDotNetCheckoutResponse> checkAuthorizePayment(@Body AuthorizePayment authorizePayment);

    @POST("catalog/search")
    Call<ProductsResponse> searchProduct(@Body Search q);

    @GET("categoriesNmanufactures/search")
    Call<AdvanceSearchSpinnerOptionResponse> getAdvanceSearchOptions();

    @POST("customer/address/add")
    Call<AddAddressResponse> addAddressNew(@Body List<KeyValuePairNew> keyValuePairs);


    /*new api*/
//    @POST("sms/SendSMSVMart")
//    Call<OtpResponseModel> getOtpStatus(@Body Map<String, String> sendData);
    @POST("customer/VerifyMobileNumber")
    Call<OtpResponseModel> getOtpStatus(@Body Map<String, String> sendData);


    /*ok dollar profile*/
    @GET("RestService.svc/RetrieveProfile")
    Call<JsonObject> retrieveProfile(@QueryMap Map<String, String> filters);

    @POST("customer/register")
    Call<JsonObject> registerUser(@Body RegisterRequestModel filters);

    @POST("customer/passwordrecovery")
    Call<ForgetResponse> forgetPasswordNew(@QueryMap Map<String, String> filters);

    @POST("customer/registerfromokdollar")
    Call<ResponseModelAutoLogin> okDollarRegistration(@Body AutoRegisterokDollar autoRegisterokDollar);

    @POST("customer/savecontactdetails")
    Call<ResponseContact> MultiUpdateContacts(@Body JsonObject jsonObject);

    @Multipart
    @PUT
    Call<JsonObject> putCSVFile(@Url String url,
                                @Header("Content-Type") String contentType,
                                @Part MultipartBody.Part file);

    @GET("RestService.svc/GetContactsPresignedUrlAwsS3")
    Call<JsonObject> getUrl(@QueryMap Map<String, String> val);

    @GET("Category/{id}")
    Call<ProductsResponse> getProductPriceList(@Path("id") long id);

    @GET("categoryfeaturedproductandsubcategory/{id}")
    Call<SubCategoryProduct> getSubcategoryNew(@Path("id") long id);

    @GET("Category/{id}")
    Call<ProductsResponse> getProductListFromSubcategory(@Path("id") long id);


    @POST("login")
    Call<LoginResponseModelNew> loginUser(@Body LoginRequestModel loginData);

    @POST("customer/register")
    Call<RegisterResponseModel> loginRegister(@Body LoginRequestModel loginData);

    @POST("checkout/UpdatePaymentStatus")
    Call<PaymentResponseModel> paymentStatusUpdate(@Body PaymentResponseModel data);

    @POST("customer/registerorlogin")
    Call<LoginResponseModel> loginOrRegister(@Body LoginOrRegistrationModel data);

    @GET("Manufacturer/{manufacturerId}")
    Call<ProductsResponse> getProductListByManufacturerNew(@Path("manufacturerId") long id);


    @POST("AdService.svc/GetMultiImageUrlByBase64String")
    Call<ImageUrlResponseModel> GetMultiImageUrlByBase64String(@Body ImageUrlRequestModel jsonObject);

    @GET("product/GetProductIdbyBarcode/{id}")
    Call<BarcodeResponse> getScannedProductId(@Path("id") String id);

    @POST("catalog/search")
    Call<ProductsResponse> productSearch(@Query("pagenumber") String pageNumber, @Body HashMap<String, Object> q);

    @POST("UserService/SearchAgentsForOkDollar")
    Call<NearbyResponseModel> nearByList(@Body CashInRequestModel q);

    @POST("UserService/SearchAgentsForOkDollar")
    Call<NearbyResponseModel> nearByList(@Body FilterRequestModel q);

    @POST("checkout/SavePaymentTransactionHistory")
    Call<SaveTransactionStatusModel> savePaymentTransactionHistory(@Body SaveTransactionStatusModel model);

    //multiApiCall
    @GET("homepagebanner")
    Observable<HomePageBannerResponse> getHomeBanner(@Query(queryString) String query);

    @GET("homepageproducts")
    Observable<FeatureProductResponseModel> getHomeProducts(@Query(queryString) String query);

    @GET("catalog/homepagecategorieswithproduct")
    Observable<FeaturedCategoryResponse> getHomeCategoriesWithProduct();

    @GET("homepagemanufacture")
    Observable<HomePageManufacturerResponse> getHomeManufacturer(@Query(queryString) String query);

    @GET("v1/categories")
    Observable<CategoryNewResponse> getCategoryNew();

    @POST("product/productreviewsadd/{productId}")
    Observable<RatingRequestModel> ratingAndReview(@Path("productId") int id, @Body RatingReviewRequestModel ratingReviewRequestModel);

    @GET("product/getcustomerproductreview/{productId}")
    Observable<RatingGettingModel> getratingAndReview(@Path("productId") int id);

    @GET("order/details/{id}")
    Observable<OrderDetailsResponse> getOrderDetailsRating(@Path("id") int id);

    @POST("SetLanguage/{id}")
    Observable<OrderDetailsResponse> setUserLanguage(@Path("id") int id);

    @GET("homepageproducts")
    Observable<FeatureProductResponseModel> featureProductSort(@Query("thumbPictureSize") String size, @Query("orderBy") String order);

    @DELETE("ShoppingCart/RemoveProduct/{productId}/{shoppingCartTypeId}")
    Call<BaseResponse> removeProductFromWishList(@Path("productId") String id, @Path("shoppingCartTypeId") String cartId);

    @GET("")
    Observable<NotificationModel> getNotificationsList();

    @GET("appupgrade/getappupgradedetails")
    Observable<AppUpdateModel> getAppUpdate();

    @GET("customer/helpandsupport")
    Call<HelpSupportModel> getHelpSupport();

    @POST("reviews/ReviewsOfAppAndAddress")
    Observable<AppRatingResponseModel> ratingAndReview(@Body AppReviewAndRating ratingReviewRequestModel);

    @GET("product/SaveNotifyRequest/{id}")
    Call<NotifyModel> notifyMe(@Path("id") long id);

    @GET("product/RecentlyViewedProducts")
    Call<RecentProductsResponseModel> recentProducts();

    @GET("catalog/RecentlySearchedKeyWords")
    Call<SearchResponseModel> searchKeyWord();

    @POST("https://www.okdollar.co/RestService.svc/GetMultiImageUrlByBase64String")
    Observable<CommonResponseDao> getImageUrlByBase64String(@Header("position") int requestCode, @Body JsonObject jsonObject);

}
