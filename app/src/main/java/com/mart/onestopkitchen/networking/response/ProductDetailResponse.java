package com.mart.onestopkitchen.networking.response;

import com.mart.onestopkitchen.model.ProductDetail;
import com.mart.onestopkitchen.networking.BaseResponse;

/**
 * Created by Ashraful on 11/20/2015.
 */
public class ProductDetailResponse extends BaseResponse {
    private ProductDetail Data;

    public ProductDetail getData() {
        return Data;
    }

    public void setData(ProductDetail data) {
        Data = data;
    }
}
