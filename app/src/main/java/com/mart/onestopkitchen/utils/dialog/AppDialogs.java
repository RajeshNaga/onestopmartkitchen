package com.mart.onestopkitchen.utils.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.listener.DialogBtnClickListener;
import com.mart.onestopkitchen.listener.OkBtnClickListener;
import com.mart.onestopkitchen.model.okdollar.OKDollarLoginInfo;
import com.mart.onestopkitchen.ui.fragment.LoginFragment;
import com.mart.onestopkitchen.update.AppUpdateModel;
import com.mart.onestopkitchen.utils.AppUtils;
import com.mart.onestopkitchen.utils.CircleImageView;
import com.mart.onestopkitchen.utils.CustomTextView;

import java.util.Objects;

public class AppDialogs {
    private static Dialog dialog;
    private static Dialog dialogs;

    public static void loadingDialog(Context context, boolean isCanceled, String message) {
        if (dialog != null)
            dialog = null;

        dialog = new Dialog(context, android.R.style.Theme_Dialog);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(isCanceled);
        dialog.setContentView(R.layout.loading_dialog);
       /* TextView textView = dialog.findViewById(R.id.txt_loding);
        if (message != null && !message.isEmpty())
            textView.setText(message);*/
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            if (dialog != null) {
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissDialog() {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //do some use fromWhere key
    public static void showAlertFinish(AlertDialogOkClick alertDialogOkClick, String fromWhere, boolean isCancelable, String titles, String contents, @DrawableRes int id, final int which) {

        final AlertDialogOkClick dialogOkClick = alertDialogOkClick;
        if (dialogOkClick.getActivityContext() == null) {
            Log.d("Exception", "provide context");
            return;
        }
        dialogs = new Dialog(dialogOkClick.getActivityContext(), android.R.style.Theme_Dialog);
        Objects.requireNonNull(dialogs.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialogs.getWindow().setBackgroundDrawableResource(R.drawable.diloag_rounded_corner);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.app_custom_dailog);
        dialogs.setCancelable(isCancelable);
        ImageView imageView = dialogs.findViewById(R.id.img_logo);
        TextView title = dialogs.findViewById(R.id.txt_title);
        TextView content = dialogs.findViewById(R.id.txt_content);
        Button cancel = dialogs.findViewById(R.id.btn_cancel);
        Button ok = dialogs.findViewById(R.id.btn_ok);
        content.setText(cutNull(contents));
        if (fromWhere.equalsIgnoreCase("splash"))
            cancel.setVisibility(View.GONE);
        if (cutNull(titles).isEmpty())
            title.setVisibility(View.GONE);
        else title.setText(cutNull(titles));
        if (id == 0)
            imageView.setVisibility(View.GONE);
        else imageView.setImageResource(id);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
                dialogOkClick.clickOk(which);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
            }
        });
        if (dialogs != null)
            dialogs.show();
    }

    public static void dismissAlertFinishDialog() {
        if (dialogs != null && dialogs.isShowing())
            dialogs.dismiss();
    }

    public static void showAlertFinishPopup(AlertDialogOkClick alertDialogOkClick, String fromWhere, boolean isCancelable, String titles, String contents, @DrawableRes int id, final int which) {
        final AlertDialogOkClick dialogOkClick = alertDialogOkClick;
        if (dialogOkClick.getActivityContext() == null) {
            Log.d("Exception", "provide context");
            return;
        }
        dialogs = new Dialog(dialogOkClick.getActivityContext(), android.R.style.Theme_Dialog);
        Objects.requireNonNull(dialogs.getWindow()).setBackgroundDrawableResource(R.drawable.diloag_rounded_corner);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.customdialog);
        //   dialogs.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialogs.setCancelable(isCancelable);
        ImageView imageView = dialogs.findViewById(R.id.alert);
        TextView title = dialogs.findViewById(R.id.tv_title);
        TextView content = dialogs.findViewById(R.id.dialog_editext);
        Button cancel = dialogs.findViewById(R.id.tv_cancel);
        Button ok = dialogs.findViewById(R.id.tv_ok);
        content.setText(cutNull(contents));
        if (fromWhere.equalsIgnoreCase("splash"))
            cancel.setVisibility(View.GONE);
        if (cutNull(titles).isEmpty())
            title.setVisibility(View.GONE);
        else title.setText(cutNull(titles));

        ok.setOnClickListener(view -> {
            dialogs.dismiss();
            dialogOkClick.clickOk(which);
        });
        cancel.setOnClickListener(view -> dialogs.dismiss());
        if (dialogs != null)
            dialogs.show();
    }

    //
    //do some use fromWhere key
    public static void showAlertFinish1(AlertDialogOkClick alertDialogOkClick, String fromWhere, boolean isCancelable, String titles, String contents, @DrawableRes int id, final int which) {

        final AlertDialogOkClick dialogOkClick = alertDialogOkClick;
        if (dialogOkClick.getActivityContext() == null) {
            Log.d("Exception", "provide context");
            return;
        }
        dialogs = new Dialog(dialogOkClick.getActivityContext(), android.R.style.Theme_Dialog);
        Objects.requireNonNull(dialogs.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialogs.getWindow().setBackgroundDrawableResource(R.drawable.diloag_rounded_corner);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.app_custom_dailog);
        dialogs.setCancelable(isCancelable);
        ImageView imageView = dialogs.findViewById(R.id.img_logo);
        TextView title = dialogs.findViewById(R.id.txt_title);
        TextView content = dialogs.findViewById(R.id.txt_content);
        Button cancel = dialogs.findViewById(R.id.btn_cancel);
        Button ok = dialogs.findViewById(R.id.btn_ok);
        content.setText(cutNull(contents));
        if (fromWhere.equalsIgnoreCase("splash"))
            cancel.setVisibility(View.GONE);
        if (cutNull(titles).isEmpty())
            title.setVisibility(View.GONE);
        else title.setText(cutNull(titles));
        if (id == 0)
            imageView.setVisibility(View.GONE);
        else imageView.setImageResource(id);
        ok.setOnClickListener(view -> {
            dialogs.dismiss();
            dialogOkClick.clickOk(which);
        });
        cancel.setOnClickListener(view -> dialogs.dismiss());
        if (dialogs != null)
            dialogs.show();
    }

    private static String cutNull(Object o) {
        return AppUtils.cutNull(o);
    }

    public static void customDialog(Context mContext, String msg, final OkBtnClickListener listener) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.setContentView(R.layout.layout_custon_dialog);
        Objects.requireNonNull(mDialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mDialog.setCancelable(false);
        Button btnOk = mDialog.findViewById(R.id.btnOk);
        CustomTextView mTitle = mDialog.findViewById(R.id.tvTitle);
        mTitle.setText(msg);
        btnOk.setOnClickListener(view -> {
            if (listener != null) {
                listener.onOkClick();
            }
            mDialog.dismiss();
        });
        mDialog.show();
    }

    @SuppressLint("SetTextI18n")
    public static void dialogForceUpdate(Context mContext, AppUpdateModel data, final OkBtnClickListener listener) {
        Dialog mDialog = new Dialog(mContext);
        mDialog.setContentView(R.layout.layout_force_update);
        Objects.requireNonNull(mDialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mDialog.setCancelable(false);
        Button btnOk = mDialog.findViewById(R.id.btnUpdate);
        ImageView btnClose = mDialog.findViewById(R.id.imgClose);
        CustomTextView mTitle = mDialog.findViewById(R.id.tvTitle);
        CustomTextView tvVersion = mDialog.findViewById(R.id.tvVersion);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tvVersion.setText("V: " + data.getAppLatestVersionName());
        mTitle.setText(data.getMessage());
        if (data.isUpdateForceRequired()) {
            btnClose.setVisibility(View.GONE);
        } else {
            btnClose.setVisibility(View.VISIBLE);
        }
        btnClose.setOnClickListener(view -> mDialog.dismiss());

        btnOk.setOnClickListener(view -> {
            if (listener != null) {
                listener.onOkClick();
                mDialog.dismiss();
            }
            mDialog.dismiss();
        });
        mDialog.show();
    }

    @SuppressLint("SetTextI18n")
    public static void customDialogWithCancel(Context mContext, String msg, final DialogBtnClickListener listener) {
        final Dialog mDialog = new Dialog(mContext);
        mDialog.setContentView(R.layout.layout_logout_dialog);
        Objects.requireNonNull(mDialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mDialog.setCancelable(false);
        Button btnOk = mDialog.findViewById(R.id.btnYes);
        Button btnCancel = mDialog.findViewById(R.id.btnCancel);
        CustomTextView mTitle = mDialog.findViewById(R.id.tvTitle);
        mTitle.setText(msg);
        btnOk.setOnClickListener(view -> {
            if (listener != null) {
                listener.yesBtnClick(mDialog);
            }
        });
        btnCancel.setOnClickListener(view -> {
            if (listener != null) {
                listener.cancelBtnClick();
            }
            mDialog.dismiss();
        });
        mDialog.show();
    }

    @SuppressLint("SetTextI18n")
    public void popUpWindow(LoginFragment context, final OKDollarLoginInfo okDollarLoginModel) {
        final OkDollarLogin okDollarLogin = (OkDollarLogin) context;
        // final Dialog dialog = new Dialog(context.getContext(), android.R.style.Theme_Holo_Dialog);
        final Dialog dialog = new Dialog(Objects.requireNonNull(context.getContext()), android.R.style.Theme_Dialog);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationAnimationTopToBottom; //style id
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.ok_dollar_login);
        CircleImageView userImnage = dialog.findViewById(R.id.imgUserProfile);
        ShimmerFrameLayout prodress = dialog.findViewById(R.id.shimmerLayout);
        Button btn = dialog.findViewById(R.id.btnClick);
        userImnage.setVisibility(View.GONE);
        prodress.setVisibility(View.VISIBLE);
        prodress.startShimmer();
   /*     Glide.with(context.getContext())
                .load(okDollarLoginModel.getProfilePicture())
                //.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.ok)
                .error(R.drawable.ok)
                .into(userImnage);*/

        Glide.with(context.getContext())
                .load(okDollarLoginModel.getProfilePicture())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        userImnage.setVisibility(View.VISIBLE);
                        prodress.setVisibility(View.GONE);
                        prodress.stopShimmer();
                        return false;
                    }
                })
                .into(userImnage);
        ;
        dialog.setCancelable(true);
        btn.setText(context.getResources().getString(R.string.continue_) + " " + okDollarLoginModel.getName());
        btn.setOnClickListener(view -> {
            dialog.dismiss();
            okDollarLogin.clickContinue(okDollarLoginModel);
        });

        dialog.show();

    }

    public interface OkDollarLogin {
        void clickContinue(OKDollarLoginInfo okDollarLoginModel);
    }

    public interface AlertDialogOkClick {
        void clickOk(int fromWhichDialog);

        Context getActivityContext();
    }
}
