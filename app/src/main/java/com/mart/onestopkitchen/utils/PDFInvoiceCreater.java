package com.mart.onestopkitchen.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.OrdersModel;
import com.mart.onestopkitchen.model.PDFProductItem;
import com.mart.onestopkitchen.model.PaymentResponseModel;
import com.mart.onestopkitchen.ui.activity.MainActivity;
import com.mart.onestopkitchen.ui.fragment.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class PDFInvoiceCreater {
    private static int Total_item = 0;
    private static Document document;
    private static String itemorderId = "0";

    @SuppressLint("HardwareIds,SimpleDateFormat")
    public static void getInstance(PaymentResponseModel response, final FragmentActivity activity) {
        List<OrdersModel> items = response.getOrders();

        File dir = new File(Environment.getExternalStorageDirectory() + File.separator + activity.getResources().getString(R.string.kitchen_app_name_for_pdf) + File.separator);
        if (!dir.exists()) {
            dir.mkdir();
        }

        Utility.setCartCounter(0);
        TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        String imei = null;
        if (tm != null) {
            imei = tm.getDeviceId();
        }
        Location currentLocation = ((MainActivity) activity).getmLocation();
        String latitude = String.valueOf(currentLocation.getLatitude());
        String longitude = String.valueOf(currentLocation.getLongitude());

        /*
         * File validation
         * */

        if (new File(dir + items.get(0).getCustomOrderNumber() + ".pdf").exists()) {
            new File(dir + items.get(0).getCustomOrderNumber() + ".pdf").delete();
        }
        try {
            /**
             * Creating Document
             */
            document = new Document();
            // Location to save
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(dir + "/" + items.get(0).getCustomOrderNumber() + "-" + items.get(0).getCreatedOn() + ".pdf"));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            document.addCreator("1 Stop Kitchen Limited");
            BaseFont urName = BaseFont.createFont("assets/fonts/arial.ttf", "UTF-8", BaseFont.EMBEDDED);
            /*
             * Water mark
             * */
            PdfContentByte canvas = pdfWriter.getDirectContentUnder();
            Phrase watermark = new Phrase(" 1 Stop Kitchen", new Font(Font.FontFamily.TIMES_ROMAN, 50, Font.NORMAL, new BaseColor(0, 0, 0, 44)));
            ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, watermark, 110, 100, 58);

            Phrase watermark2 = new Phrase(" 1 Stop Kitchen", new Font(Font.FontFamily.TIMES_ROMAN, 50, Font.NORMAL, new BaseColor(0, 0, 0, 44)));
            ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER, watermark2, 310, 400, 58);

            Phrase watermark3 = new Phrase(" 1 Stop Kitchen", new Font(Font.FontFamily.TIMES_ROMAN, 50, Font.NORMAL, new BaseColor(0, 0, 0, 44)));
            ColumnText.showTextAligned(canvas, Element.ALIGN_RIGHT, watermark3, 510, 700, 58);
            Phrase watermark1 = new Phrase(items.get(0).getCreatedOn() + "   Latitude :" + latitude + "  Longitude :" + longitude + " Device id :" + imei, new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, new BaseColor(0, 0, 0, 61)));
            ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, watermark1, 30, 200, 90);
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));
            // get input stream
            Drawable d = activity.getResources().getDrawable(R.drawable.app_icon_for_pdf);
            BitmapDrawable bitDw = ((BitmapDrawable) d);
            Bitmap bmp = bitDw.getBitmap();
            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream1);
            Image image1 = Image.getInstance(stream1.toByteArray());
            image1.scaleAbsolute(50f, 50f);
            image1.setAlignment(Element.ALIGN_CENTER);
            document.add(image1);
            // Title Order Details...
            // Adding Title....
            Font thanksforusingFont = new Font(urName, 20.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk thanksforusingChunk = new Chunk("1 Stop Kitchen", thanksforusingFont);
            Paragraph thanksforusingParagraph = new Paragraph(thanksforusingChunk);
            thanksforusingParagraph.setAlignment(Element.ALIGN_CENTER);
            thanksforusingParagraph.setPaddingTop(50);
            document.add(thanksforusingParagraph);
            /*
             * PDf Table for harder two
             * */
            PdfPTable table = new PdfPTable(new float[]{2, 3, 3});
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.setWidthPercentage(90);
            Font PdfPCell = new Font(urName, 12.0f, Font.NORMAL, BaseColor.BLACK);
            com.itextpdf.text.pdf.PdfPCell cell1 = new PdfPCell(new Paragraph("Invoice", PdfPCell));
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setPadding(05.0f);
            cell1.setPaddingTop(10.0f);
            cell1.setVerticalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell1);
            Font PdfPCell1 = new Font(urName, 8.0f, Font.NORMAL, BaseColor.BLACK);

            String localDate = items.get(0).getCreatedOn();
            Date value;
            try {
                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
                SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
                Date date = input.parse(localDate);
                String formattedDate = input.format(date);
                value = input.parse(formattedDate);
                localDate = output.format(value);
            } catch (Exception e) {
                e.printStackTrace();
            }


            PdfPCell cell2 = new PdfPCell(new Paragraph("Order Date : " + localDate, PdfPCell1));
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setPadding(05.0f);
            cell2.setPaddingTop(10.0f);
            cell2.setVerticalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell2);
            PdfPCell cell3 = new PdfPCell(new Paragraph("Invoice NO : " + response.getReferenceNumber(), PdfPCell1));
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setPadding(05.0f);
            cell3.setPaddingTop(10.0f);
            cell3.setVerticalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell3);
            document.add(table);
            /*
             * PDf Table for address header
             * */
            Font thanksforusingFont3 = new Font(urName, 12.0f, Font.NORMAL, BaseColor.BLACK);
            PdfPTable table3 = new PdfPTable(new float[]{3, 4, 3});
            table3.setWidthPercentage(100);
            table3.setTotalWidth(500);
            table3.setPaddingTop(10.0f);
            table3.setLockedWidth(true);
            table3.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell table3cell = new PdfPCell(new Paragraph("Sold By", thanksforusingFont3));
            table3cell.setBorder(Rectangle.NO_BORDER);
            table3cell.setPaddingTop(10.0f);
            table3.addCell(table3cell);
            PdfPCell table3cell2 = new PdfPCell(new Paragraph(""));
            table3cell2.setBorder(Rectangle.NO_BORDER);
            table3.addCell(table3cell2);
            if (null != items.get(0) && items.get(0).getPickUpInStore()) {
                PdfPCell table3cell3 = new PdfPCell(new Paragraph("Pickup Address", thanksforusingFont3));
                table3cell3.setBorder(Rectangle.NO_BORDER);
                table3cell3.setPaddingTop(10.0f);
                table3.addCell(table3cell3);
                document.add(table3);
            } else {
                PdfPCell table3cell3 = new PdfPCell(new Paragraph("Shipping Address", thanksforusingFont3));
                table3cell3.setBorder(Rectangle.NO_BORDER);
                table3cell3.setPaddingTop(10.0f);
                table3.addCell(table3cell3);
                document.add(table3);
            }
            /*
             * PDf Table for address
             * */
            Font addressgFont3 = new Font(urName, 10.0f, Font.NORMAL, BaseColor.BLACK);
            PdfPTable table4 = new PdfPTable(new float[]{3, 4, 3});
            table4.setTotalWidth(500);
            table4.setWidthPercentage(100);
            table4.setPaddingTop(5.0f);
            table4.setLockedWidth(true);
            table4.setHorizontalAlignment(Element.ALIGN_LEFT);
            PdfPCell table4cell = new PdfPCell(new Paragraph(" CGM \n No:501 , Junction Square, \n Kamaryurt , Yangon , \n Myanmar.", addressgFont3));
            table4cell.setBorder(Rectangle.NO_BORDER);
            table4cell.setPaddingBottom(10.0f);
            table4.addCell(table4cell);
            PdfPCell table4cell2 = new PdfPCell(new Paragraph(""));
            table4cell2.setBorder(Rectangle.NO_BORDER);
            table4.addCell(table4cell2);
            String address = response.getShippingAddress();
            address = address.replace("null", "");
            PdfPCell table4cell3 = new PdfPCell(new Paragraph(address, addressgFont3));
            table4cell3.setBorder(Rectangle.NO_BORDER);
            table4cell3.setPaddingBottom(10.0f);
            table4.addCell(table4cell3);
            document.add(table4);
            /*
             * PDf Table for order item header
             * */
            Font iteamheaderFont3 = new Font(urName, 12.0f, Font.NORMAL, BaseColor.BLACK);
            float[] pointColumnWidths1 = {250f, 250f, 250f, 250f, 250f};
            PdfPTable iteamheader = new PdfPTable(pointColumnWidths1);
            iteamheader.setHorizontalAlignment(Element.ALIGN_LEFT);
            iteamheader.setWidthPercentage(100);
            iteamheader.setSpacingBefore(0f);
            iteamheader.setSpacingAfter(0f);
            iteamheader.setTotalWidth(500);

            PdfPCell order_id_heder = new PdfPCell(new Paragraph("Order NO", iteamheaderFont3));
            order_id_heder.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.TOP);
            order_id_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            order_id_heder.setUseVariableBorders(true);
            order_id_heder.setBorderColorTop(BaseColor.BLACK);
            order_id_heder.setBorderColorRight(BaseColor.BLACK);
            order_id_heder.setBorderColorBottom(BaseColor.BLACK);
            order_id_heder.setBorderColorLeft(BaseColor.BLACK);
            PdfPCell product_heder = new PdfPCell(new Paragraph("Product", iteamheaderFont3));
            product_heder.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
            product_heder.setUseVariableBorders(true);
            product_heder.setBorderColorTop(BaseColor.BLACK);
            product_heder.setBorderColorRight(BaseColor.BLACK);
            product_heder.setBorderColorBottom(BaseColor.BLACK);
            product_heder.setBorderColorLeft(BaseColor.BLACK);
            product_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            PdfPCell quality_heder = new PdfPCell(new Paragraph("Quantity", iteamheaderFont3));
            quality_heder.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
            quality_heder.setUseVariableBorders(true);
            quality_heder.setBorderColorTop(BaseColor.BLACK);
            quality_heder.setBorderColorRight(BaseColor.BLACK);
            quality_heder.setBorderColorBottom(BaseColor.BLACK);
            quality_heder.setBorderColorLeft(BaseColor.BLACK);
            quality_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            PdfPCell grossAmount_heder = new PdfPCell(new Paragraph("Product \n Amount", iteamheaderFont3));
            grossAmount_heder.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
            grossAmount_heder.setUseVariableBorders(true);
            grossAmount_heder.setBorderColorTop(BaseColor.BLACK);
            grossAmount_heder.setBorderColorRight(BaseColor.BLACK);
            grossAmount_heder.setBorderColorBottom(BaseColor.BLACK);
            grossAmount_heder.setBorderColorLeft(BaseColor.BLACK);
            grossAmount_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
           /* PdfPCell Discount_heder = new PdfPCell(new Paragraph("Discount", iteamheaderFont3));
            Discount_heder.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
            Discount_heder.setUseVariableBorders(true);
            Discount_heder.setBorderColorTop(BaseColor.BLACK);
            Discount_heder.setBorderColorRight(BaseColor.BLACK);
            Discount_heder.setBorderColorBottom(BaseColor.BLACK);
            Discount_heder.setBorderColorLeft(BaseColor.BLACK);
            Discount_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);*/
            PdfPCell Total_heder = new PdfPCell(new Paragraph("Total", iteamheaderFont3));
            Total_heder.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM | Rectangle.TOP);
            Total_heder.setUseVariableBorders(true);
            Total_heder.setBorderColorTop(BaseColor.BLACK);
            Total_heder.setBorderColorRight(BaseColor.BLACK);
            Total_heder.setBorderColorBottom(BaseColor.BLACK);
            Total_heder.setBorderColorLeft(BaseColor.BLACK);
            Total_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            iteamheader.addCell(order_id_heder);
            iteamheader.addCell(product_heder);
            iteamheader.addCell(quality_heder);
            iteamheader.addCell(grossAmount_heder);
            //iteamheader.addCell(Discount_heder);
            iteamheader.addCell(Total_heder);
            document.add(iteamheader);
            int totalCount = 0;
            /*
             * PDf Table for order items
             * */
            for (int i = 0; i < items.size(); i++) {
                List<PDFProductItem> item = items.get(i).getItems();
                if (item != null) {
                    Total_item += item.size();
                }
                if (item != null) {
                    for (int j = 0; j < item.size(); j++) {
                        Font iteamFont3 = new Font(urName, 8.0f, Font.NORMAL, BaseColor.BLACK);
                        float[] iteamWidths1 = {250f, 250f, 250f, 250f, 250f};
                        PdfPTable iteamlistheader = new PdfPTable(iteamWidths1);
                        iteamlistheader.setHorizontalAlignment(Element.ALIGN_LEFT);
                        iteamlistheader.setWidthPercentage(100);
                        iteamlistheader.setSpacingBefore(0f);
                        iteamlistheader.setSpacingAfter(0f);
                        iteamlistheader.setTotalWidth(500);
                        iteamlistheader.setPaddingTop(50.0f);
                        totalCount = totalCount + Integer.parseInt(item.get(j).getQuantity());
                        if (!itemorderId.equals(String.valueOf(items.get(i).getId()))) {
                            PdfPCell iteamlist_id_heder;
                            iteamlist_id_heder = new PdfPCell(new Paragraph(String.valueOf(items.get(i).getCustomOrderNumber()), iteamFont3));
                            iteamlist_id_heder.setBorder(Rectangle.LEFT);
                            iteamlist_id_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_id_heder.setUseVariableBorders(true);
                            iteamlist_id_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_id_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_id_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_id_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_id_heder.setPaddingBottom(10.0f);
                            iteamlist_id_heder.setPaddingTop(20.0f);
                            iteamlistheader.addCell(iteamlist_id_heder);
                            itemorderId = String.valueOf(items.get(i).getId());
                            PdfPCell iteamlist_product_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getProductName()), iteamFont3));
                            iteamlist_product_heder.setPaddingTop(20.0f);
                            iteamlist_product_heder.setPaddingBottom(10.0f);
                            iteamlist_product_heder.setUseVariableBorders(true);
                            iteamlist_product_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_product_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_product_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_product_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_product_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_product_heder.setBorder(Rectangle.NO_BORDER);
                            PdfPCell iteamlist_quality_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getQuantity()), iteamFont3));
                            iteamlist_quality_heder.setPaddingTop(20.0f);
                            iteamlist_quality_heder.setPaddingBottom(10.0f);
                            iteamlist_quality_heder.setUseVariableBorders(true);
                            iteamlist_quality_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_quality_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_quality_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_quality_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_quality_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_quality_heder.setBorder(Rectangle.NO_BORDER);
                            PdfPCell iteamlist_grossAmount_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getUnitPrice()), iteamFont3));
                            iteamlist_grossAmount_heder.setPaddingTop(20.0f);
                            iteamlist_grossAmount_heder.setPaddingBottom(10.0f);
                            iteamlist_grossAmount_heder.setUseVariableBorders(true);
                            iteamlist_grossAmount_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_grossAmount_heder.setBorder(Rectangle.NO_BORDER);
                            /*PdfPCell iteamlist_Discount_heder = new PdfPCell(new Paragraph("0", iteamFont3));
                            iteamlist_Discount_heder.setPaddingTop(20.0f);
                            iteamlist_Discount_heder.setPaddingBottom(10.0f);
                            iteamlist_Discount_heder.setUseVariableBorders(true);
                            iteamlist_Discount_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_Discount_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_Discount_heder.setBorder(Rectangle.NO_BORDER);*/
                            PdfPCell iteamlist_Total_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getSubTotal()), iteamFont3));
                            iteamlist_Total_heder.setPaddingTop(20.0f);
                            iteamlist_Total_heder.setPaddingBottom(10.0f);
                            iteamlist_Total_heder.setBorder(Rectangle.RIGHT);
                            iteamlist_Total_heder.setUseVariableBorders(true);
                            iteamlist_Total_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_Total_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_Total_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_Total_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_Total_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlistheader.addCell(iteamlist_product_heder);
                            iteamlistheader.addCell(iteamlist_quality_heder);
                            iteamlistheader.addCell(iteamlist_grossAmount_heder);
                            //iteamlistheader.addCell(iteamlist_Discount_heder);
                            iteamlistheader.addCell(iteamlist_Total_heder);
                            document.add(iteamlistheader);
                        } else {
                            PdfPCell iteamlist_id_heder;
                            iteamlist_id_heder = new PdfPCell(new Paragraph(",,", iteamFont3));
                            iteamlist_id_heder.setBorder(Rectangle.LEFT);
                            iteamlist_id_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_id_heder.setUseVariableBorders(true);
                            iteamlist_id_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_id_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_id_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_id_heder.setBorderColorLeft(BaseColor.BLACK);
                            PdfPCell iteamlist_product_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getProductName()), iteamFont3));
                            iteamlist_product_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_product_heder.setUseVariableBorders(true);
                            iteamlist_product_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_product_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_product_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_product_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_product_heder.setBorder(Rectangle.NO_BORDER);
                            PdfPCell iteamlist_quality_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getQuantity()), iteamFont3));
                            iteamlist_quality_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_quality_heder.setUseVariableBorders(true);
                            iteamlist_quality_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_quality_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_quality_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_quality_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_quality_heder.setBorder(Rectangle.NO_BORDER);
                            PdfPCell iteamlist_grossAmount_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getUnitPrice()), iteamFont3));
                            iteamlist_grossAmount_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_grossAmount_heder.setUseVariableBorders(true);
                            iteamlist_grossAmount_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_grossAmount_heder.setBorder(Rectangle.NO_BORDER);
                            /*PdfPCell iteamlist_Discount_heder = new PdfPCell(new Paragraph("0", iteamFont3));
                            iteamlist_Discount_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlist_Discount_heder.setUseVariableBorders(true);
                            iteamlist_Discount_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_Discount_heder.setBorder(Rectangle.NO_BORDER);*/
                            PdfPCell iteamlist_Total_heder = new PdfPCell(new Paragraph(String.valueOf(item.get(j).getSubTotal()), iteamFont3));
                            iteamlist_Total_heder.setBorder(Rectangle.RIGHT);
                            iteamlist_Total_heder.setUseVariableBorders(true);
                            iteamlist_Total_heder.setBorderColorTop(BaseColor.BLACK);
                            iteamlist_Total_heder.setBorderColorRight(BaseColor.BLACK);
                            iteamlist_Total_heder.setBorderColorBottom(BaseColor.BLACK);
                            iteamlist_Total_heder.setBorderColorLeft(BaseColor.BLACK);
                            iteamlist_Total_heder.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                            iteamlistheader.addCell(iteamlist_id_heder);
                            iteamlistheader.addCell(iteamlist_product_heder);
                            iteamlistheader.addCell(iteamlist_quality_heder);
                            iteamlistheader.addCell(iteamlist_grossAmount_heder);
                            //iteamlistheader.addCell(iteamlist_Discount_heder);
                            iteamlistheader.addCell(iteamlist_Total_heder);
                            document.add(iteamlistheader);
                        }
                    }
                }
            }


            //total
            Font TotaliteamFont3 = new Font(urName, 10.0f, Font.NORMAL, BaseColor.BLACK);
            float[] TotaliteamWidths1 = {250f, 250f};
            PdfPTable Taxiteamlistheader = new PdfPTable(TotaliteamWidths1);
            Taxiteamlistheader.setHorizontalAlignment(Element.ALIGN_LEFT);
            Taxiteamlistheader.setWidthPercentage(100);
            Taxiteamlistheader.setSpacingBefore(0f);
            Taxiteamlistheader.setSpacingAfter(0f);
            Taxiteamlistheader.setTotalWidth(500);
            //PdfPCell Totaliteamlist_id_heder = new PdfPCell(new Paragraph("TOTAL QUANTITY : "+Total_item, TotaliteamFont3));
            PdfPCell Taxiteamlist_id_heder = new PdfPCell(new Paragraph("", TotaliteamFont3));
            Taxiteamlist_id_heder.setBorder(Rectangle.LEFT | Rectangle.TOP);
            Taxiteamlist_id_heder.setUseVariableBorders(true);
            Taxiteamlist_id_heder.setBorderColor(BaseColor.BLACK);

            PdfPCell Taxiteamlist_product_heder = new PdfPCell(new Paragraph("Sub-total: " + items.get(0).getOrderSubtotal(), TotaliteamFont3));
            Taxiteamlist_product_heder.setBorder(Rectangle.RIGHT | Rectangle.TOP);
            //Taxiteamlist_product_heder.setBorder(Rectangle.RIGHT  | Rectangle.BOTTOM);
            Taxiteamlist_product_heder.setUseVariableBorders(true);
            Taxiteamlist_product_heder.setBorderColorTop(BaseColor.BLACK);
            Taxiteamlist_product_heder.setBorderColorRight(BaseColor.BLACK);
            Taxiteamlist_product_heder.setBorderColorBottom(BaseColor.BLACK);
            Taxiteamlist_product_heder.setBorderColorLeft(BaseColor.BLACK);
            Taxiteamlist_product_heder.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            Taxiteamlist_product_heder.setPaddingBottom(5.0f);
            Taxiteamlist_product_heder.setPaddingTop(20);
            Taxiteamlist_product_heder.setPaddingRight(10.0f);
            Taxiteamlistheader.addCell(Taxiteamlist_id_heder);
            Taxiteamlistheader.addCell(Taxiteamlist_product_heder);
            document.add(Taxiteamlistheader);



            /*
             * PDf Table for Tax binding
             * */

            if ((items.size() > 0) && (!Objects.requireNonNull(items.get(0).getTax()).equalsIgnoreCase("0 MMK"))) {
                PdfPTable Taxiteamlistheader10 = new PdfPTable(TotaliteamWidths1);
                Taxiteamlistheader10.setPaddingTop(20);
                Taxiteamlistheader10.setHorizontalAlignment(Element.ALIGN_LEFT);
                Taxiteamlistheader10.setWidthPercentage(100);
                Taxiteamlistheader10.setSpacingBefore(0f);
                Taxiteamlistheader10.setSpacingAfter(0f);
                Taxiteamlistheader10.setTotalWidth(500);
                //PdfPCell Totaliteamlist_id_heder = new PdfPCell(new Paragraph("TOTAL QUANTITY : "+Total_item, TotaliteamFont3));
                PdfPCell Taxiteamlist_id_heder21 = new PdfPCell(new Paragraph("", TotaliteamFont3));
                Taxiteamlist_id_heder21.setBorder(Rectangle.LEFT);
                Taxiteamlist_id_heder21.setBorderColor(BaseColor.BLACK);
                PdfPCell Taxiteamlist_product_heder21 = new PdfPCell(new Paragraph("Tax: " + items.get(0).getTax(), TotaliteamFont3));
                Taxiteamlist_product_heder21.setBorder(Rectangle.RIGHT);
                Taxiteamlist_id_heder21.setBorderColor(BaseColor.BLACK);
                Taxiteamlist_product_heder21.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
                Taxiteamlist_product_heder21.setPaddingBottom(5.0f);
                Taxiteamlist_product_heder21.setPaddingTop(5.0f);
                Taxiteamlist_product_heder21.setPaddingRight(10.0f);
                Taxiteamlistheader10.addCell(Taxiteamlist_id_heder21);
                Taxiteamlistheader10.addCell(Taxiteamlist_product_heder21);
                document.add(Taxiteamlistheader10);
            }

            /*
             * PDf Table for shipping charges
             * */
            PdfPTable Shipingiteamlistheader = new PdfPTable(TotaliteamWidths1);
            Shipingiteamlistheader.setHorizontalAlignment(Element.ALIGN_LEFT);
            Shipingiteamlistheader.setWidthPercentage(100);
            Shipingiteamlistheader.setSpacingBefore(0f);
            Shipingiteamlistheader.setSpacingAfter(0f);
            Shipingiteamlistheader.setTotalWidth(500);
            //PdfPCell Totaliteamlist_id_heder = new PdfPCell(new Paragraph("TOTAL QUANTITY : "+Total_item, TotaliteamFont3));
            PdfPCell Shipingiteamlist_id_heder = new PdfPCell(new Paragraph("", TotaliteamFont3));
            Shipingiteamlist_id_heder.setBorder(Rectangle.LEFT);
            //Taxiteamlist_id_heder.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            Shipingiteamlist_id_heder.setUseVariableBorders(true);
            Shipingiteamlist_id_heder.setBorderColorTop(BaseColor.BLACK);
            Shipingiteamlist_id_heder.setBorderColorRight(BaseColor.BLACK);
            Shipingiteamlist_id_heder.setBorderColorBottom(BaseColor.BLACK);
            Shipingiteamlist_id_heder.setBorderColorLeft(BaseColor.BLACK);
            Shipingiteamlist_id_heder.setHorizontalAlignment(Rectangle.ALIGN_LEFT);
            Shipingiteamlist_id_heder.setPaddingBottom(10.0f);
            Shipingiteamlist_id_heder.setPaddingTop(5.0f);
            Shipingiteamlist_id_heder.setPaddingLeft(10.0f);
            PdfPCell Shipingteamlist_product_heder = new PdfPCell(new Paragraph("Shipping Charges : " + response.getShippingCharges(), TotaliteamFont3));
            Shipingteamlist_product_heder.setBorder(Rectangle.RIGHT);
            //Taxiteamlist_product_heder.setBorder(Rectangle.RIGHT  | Rectangle.BOTTOM);
            Shipingteamlist_product_heder.setUseVariableBorders(true);
            Shipingteamlist_product_heder.setBorderColorTop(BaseColor.BLACK);
            Shipingteamlist_product_heder.setBorderColorRight(BaseColor.BLACK);
            Shipingteamlist_product_heder.setBorderColorBottom(BaseColor.BLACK);
            Shipingteamlist_product_heder.setBorderColorLeft(BaseColor.BLACK);
            Shipingteamlist_product_heder.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            Shipingteamlist_product_heder.setPaddingBottom(5.0f);
            Shipingteamlist_product_heder.setPaddingTop(5.0f);
            Shipingteamlist_product_heder.setPaddingRight(10.0f);
            Shipingiteamlistheader.addCell(Shipingiteamlist_id_heder);
            Shipingiteamlistheader.addCell(Shipingteamlist_product_heder);
            document.add(Shipingiteamlistheader);
            /*
             * PDf Table for total quantity
             * */
            PdfPTable Totaliteamlistheader = new PdfPTable(TotaliteamWidths1);
            Totaliteamlistheader.setHorizontalAlignment(Element.ALIGN_LEFT);
            Totaliteamlistheader.setWidthPercentage(100);
            Totaliteamlistheader.setSpacingBefore(0f);
            Totaliteamlistheader.setSpacingAfter(0f);
            Totaliteamlistheader.setTotalWidth(500);
            //PdfPCell Totaliteamlist_id_heder = new PdfPCell(new Paragraph("TOTAL QUANTITY : "+Total_item, TotaliteamFont3));
            PdfPCell Totaliteamlist_id_heder = new PdfPCell(new Paragraph("TOTAL QUANTITY : " + totalCount, TotaliteamFont3));
            Totaliteamlist_id_heder.setBorder(Rectangle.LEFT | Rectangle.BOTTOM);
            Totaliteamlist_id_heder.setUseVariableBorders(true);
            Totaliteamlist_id_heder.setBorderColorTop(BaseColor.BLACK);
            Totaliteamlist_id_heder.setBorderColorRight(BaseColor.BLACK);
            Totaliteamlist_id_heder.setBorderColorBottom(BaseColor.BLACK);
            Totaliteamlist_id_heder.setBorderColorLeft(BaseColor.BLACK);
            Totaliteamlist_id_heder.setHorizontalAlignment(Rectangle.ALIGN_LEFT);
            Totaliteamlist_id_heder.setPaddingBottom(10.0f);
            Totaliteamlist_id_heder.setPaddingTop(5.0f);
            Totaliteamlist_id_heder.setPaddingLeft(10.0f);
            PdfPCell Totaliteamlist_product_heder = new PdfPCell(new Paragraph("TOTAL PRICE: " + response.getTotalPaidAmount(), TotaliteamFont3));
            Totaliteamlist_product_heder.setBorder(Rectangle.RIGHT | Rectangle.BOTTOM);
            Totaliteamlist_product_heder.setUseVariableBorders(true);
            Totaliteamlist_product_heder.setBorderColorTop(BaseColor.BLACK);
            Totaliteamlist_product_heder.setBorderColorRight(BaseColor.BLACK);
            Totaliteamlist_product_heder.setBorderColorBottom(BaseColor.BLACK);
            Totaliteamlist_product_heder.setBorderColorLeft(BaseColor.BLACK);
            Totaliteamlist_product_heder.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            Totaliteamlist_product_heder.setPaddingBottom(10.0f);
            Totaliteamlist_product_heder.setPaddingTop(05.0f);
            Totaliteamlist_product_heder.setPaddingRight(10.0f);
            Totaliteamlistheader.addCell(Totaliteamlist_id_heder);
            Totaliteamlistheader.addCell(Totaliteamlist_product_heder);
            document.add(Totaliteamlistheader);
            Toast.makeText(activity, "Thank you for using 1 Stop Kitchen, Your invoice created.)", Toast.LENGTH_SHORT).show();
        } catch (IOException | DocumentException ignored) {
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(activity, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
        document.close();
    }
}
