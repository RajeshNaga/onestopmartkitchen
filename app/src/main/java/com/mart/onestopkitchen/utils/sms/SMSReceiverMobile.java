package com.mart.onestopkitchen.utils.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.mart.onestopkitchen.listener.OtpListener;
import com.mart.onestopkitchen.service.PreferenceService;

public class SMSReceiverMobile extends BroadcastReceiver {
    private static OtpListener otpListener;
    private static String otpMessage = "";

    public static void bindData(OtpListener otpListeners, String otpString) {
        otpListener = otpListeners;
        otpMessage = otpString;
        String[] strings = otpMessage.split("-");
        PreferenceService.getInstance().SetPreferenceValue(PreferenceService.OTP_MESSAGE, strings.length > 1 ? strings[2] : "");

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null)
            readSms(intent);
    }

    private void readSms(Intent bundle) {
        Object[] pdus = (Object[]) bundle.getExtras().get("pdus");
        if (pdus != null) {
            SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[0]);
            Bundle extras = bundle.getExtras();
            int slotName = extras.getInt("slot", -1);
            Log.d("slotName", "" + slotName);
            if (otpListener != null) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                    if (slotName == 1) {
                        otpListener.otpNotWorkingLowEndDevices();
                        return;
                    }
                }
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.SLOT_NAME, slotName);
                String message = sms.getMessageBody();
                if (message.equalsIgnoreCase(otpMessage))
                    otpListener.otpReceived();
            }
        }
    }
}
