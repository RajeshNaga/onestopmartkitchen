package com.mart.onestopkitchen.utils.mobilevalidation;

/**
 */

public enum CACHEDKEY {
    COUNTRYOPERATOR,
    NETWORKOPERATOR,
    BUSINESS_CATEGORY_FILE_ENG {
        @Override
        public String toString() {
            return "business_category_eng.txt";
        }
    },
    BUSINESS_CATEGORY_FILE_MY {
        @Override
        public String toString() {
            return "business_category_my.txt";
        }
    },
    BUSINESS_EMOJI_ICON_FILE {
        @Override
        public String toString() {
            return "business_category_icon.txt";
        }
    },
    BUSLIST_JSON {
        @Override
        public String toString() {
            return "BusList.json";
        }
    },
    CITYLIST_JSON {
        @Override
        public String toString() {
            return "city_list.json";
        }
    },
    MCC_MNC {
        @Override
        public String toString() {
            return "mcc_mnc.txt";
        }
    },
    MYANMARCITY {
        @Override
        public String toString() {
            return "MyanmarCity.json";
        }
    },
    NUMBER_VALIDATION {
        @Override
        public String toString() {
            return "number_series.json";
        }
    },
    TOWNSHIP_JSON {
        @Override
        public String toString() {
            return "township_list.json";
        }
    },
    TERMS {
        @Override
        public String toString() {
            return "okdollar_terms.html";
        }
    },
    GPSINSTANCE

}
