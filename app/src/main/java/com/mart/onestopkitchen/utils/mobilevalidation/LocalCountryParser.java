package com.mart.onestopkitchen.utils.mobilevalidation;


import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.model.okdollar.NetworkOperatorModel;
import com.mart.onestopkitchen.ui.AppConstant;

import java.util.ArrayList;


public class LocalCountryParser {
    public Object parseNetworkCountry() {
        String oldCountryId = "";
        ArrayList<LocalCountryOperaterModel> allCountryData = (ArrayList<LocalCountryOperaterModel>) MyApplication.getAppContext().getCachedObject(CACHEDKEY.COUNTRYOPERATOR);
        ArrayList<NetworkOperatorModel> networkOperatorModels = new ArrayList<>(allCountryData.size());
        ArrayList<String> listOfDialingCodes = null;
        int flagPos = -1;
        for (LocalCountryOperaterModel item : allCountryData) {
            ++flagPos;
            if (oldCountryId.equalsIgnoreCase(item.getCountryCode()))
                continue;
            NetworkOperatorModel model = new NetworkOperatorModel();
            if (item.getCountryName().equalsIgnoreCase("Canada")) {
                listOfDialingCodes = new ArrayList<>(item.getDialingCodes().size());
                for (DialingCode codes : item.getDialingCodes()) {
                    listOfDialingCodes.add(codes.getCode());
                }
                model.setListOfDialingCodes(listOfDialingCodes);
                listOfDialingCodes.clear();
                listOfDialingCodes = null;
            }

            if (item.getCountryName().equalsIgnoreCase("UnitedStates")) {
                listOfDialingCodes = new ArrayList<>(item.getDialingCodes().size());
                for (DialingCode codes : item.getDialingCodes()) {
                    listOfDialingCodes.add(codes.getCode());
                }
                model.setListOfDialingCodes(listOfDialingCodes);
                listOfDialingCodes.clear();
                listOfDialingCodes = null;
            }

            model.setCountryImageId(AppConstant.listOfFlags[flagPos]);
            model.setCountryCode(item.getCountryCode());
            model.setCountryName(item.getCountryName());
            networkOperatorModels.add(model);
            oldCountryId = model.getCountryCode();
            model = null;

        }
        oldCountryId = null;
        return networkOperatorModels;
    }
}
