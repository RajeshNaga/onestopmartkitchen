package com.mart.onestopkitchen.utils.mobilevalidation;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocalCountryOperaterModel {

    @SerializedName("CountryCode")
    private String mCountryCode;
    @SerializedName("CountryFlagCode")
    private String mCountryFlagCode;
    @SerializedName("CountryName")
    private String mCountryName;
    @SerializedName("DialingCodes")
    private List<DialingCode> mDialingCodes;
    @SerializedName("MCC")
    private String mMCC;
    @SerializedName("networks")
    private List<Network> mNetworks;

    public String getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(String CountryCode) {
        mCountryCode = CountryCode;
    }

    public String getCountryFlagCode() {
        return mCountryFlagCode;
    }

    public void setCountryFlagCode(String CountryFlagCode) {
        mCountryFlagCode = CountryFlagCode;
    }

    public String getCountryName() {
        return mCountryName;
    }

    public void setCountryName(String CountryName) {
        mCountryName = CountryName;
    }

    public List<DialingCode> getDialingCodes() {
        return mDialingCodes;
    }

    public void setDialingCodes(List<DialingCode> DialingCodes) {
        mDialingCodes = DialingCodes;
    }

    public String getMCC() {
        return mMCC;
    }

    public void setMCC(String MCC) {
        mMCC = MCC;
    }

    public List<Network> getNetworks() {
        return mNetworks;
    }

    public void setNetworks(List<Network> networks) {
        mNetworks = networks;
    }

}
