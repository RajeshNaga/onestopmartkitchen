package com.mart.onestopkitchen.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mart.onestopkitchen.model.PaymentResponseModel
import com.mart.onestopkitchen.model.SaveTransactionStatusModel

class ConfirmOrderViewModel : ViewModel() {
    private var placeOrderResponse: LiveData<PaymentResponseModel>? = null
    private var placeOrder: ConfirmOrderResource? = null


    fun getPlaceOrder(request: SaveTransactionStatusModel): LiveData<PaymentResponseModel>? {
        placeOrder = ConfirmOrderResource(request)
        if (placeOrderResponse == null) {
            placeOrderResponse = placeOrder?.getResponse()
        }
        return placeOrderResponse
    }

    fun releaseObj() {
        placeOrderResponse?.let {
            placeOrderResponse = null
        }
        placeOrder?.let {
            placeOrder = null
        }
    }
}