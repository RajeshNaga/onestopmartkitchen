package com.mart.onestopkitchen.utils

import com.google.gson.Gson

open class AppInitialize {
    init {
        initializeModules()
    }

    companion object {
        var gson: Gson? = null

        fun getGsonSingleton(): Gson {
            if (gson == null)
                gson = Gson()
            return gson!!
        }
    }

    fun initializeModules() {
        gson = Gson()
    }
}