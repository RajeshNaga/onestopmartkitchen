package com.mart.onestopkitchen.utils.fingerprint

interface FingerprintBaseView {
    fun showFingerPrintPopup()
    fun showMsg(msg: String)
    fun success()
    fun failure()
    fun error()
}