package com.mart.onestopkitchen.utils.wifi

import java.util.*

class WifiUtils {
    private val channelsFrequency = ArrayList(
            Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
                    2452, 2457, 2462, 2467, 2472, 2484))

    fun getFrequencyFromChannel(channel: Int): Int? {
        return channelsFrequency[channel]
    }

    fun getChannelFromFrequency(frequency: Int): Int {
        return channelsFrequency.indexOf(Integer.valueOf(frequency))
    }

    fun calculateDistance(signalLevelInDb: Double, freqInMHz: Double): Double {
        val exp = (27.55 - 20 * Math.log10(freqInMHz) + Math.abs(signalLevelInDb)) / 20.0
        return Math.pow(10.0, exp)
    }

}
