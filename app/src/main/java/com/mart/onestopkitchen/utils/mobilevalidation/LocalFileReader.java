package com.mart.onestopkitchen.utils.mobilevalidation;


import com.mart.onestopkitchen.application.MyApplication;

import java.util.ArrayList;


class LocalFileReader {

    ArrayList<LocalCountryOperaterModel> getListOfNetworkOperatorModel() {
        try {
            return (ArrayList<LocalCountryOperaterModel>) new FileParser(MyApplication.getAppContext()).parseKeyData("mcc_mnc.txt", "data", LocalCountryOperaterModel.class);
        } catch (Exception ignored) {
            return new ArrayList<>(0);
        }
    }

    String readFile(CACHEDKEY fileName) {
        return new FileParser(MyApplication.getAppContext()).readFileFromAsset(fileName.toString());
    }
}
