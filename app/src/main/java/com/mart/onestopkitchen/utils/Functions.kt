package com.mart.onestopkitchen.utils

import android.content.Context
import android.location.Location
import android.widget.Toast

fun showToast(mMessage: String, mContext: Context) {
    mContext.run { Toast.makeText(mContext, mMessage, Toast.LENGTH_SHORT).show() }
}

fun calculateDistance(mLocationNew: Location, mLocationOld: Location, unit: Char): Double {
    val theta = mLocationNew.longitude - mLocationOld.longitude
    var dist = Math.sin(deg2rad(mLocationNew.latitude)) * Math.sin(deg2rad(mLocationOld.latitude)) + Math.cos(deg2rad(mLocationNew.latitude)) * Math.cos(deg2rad(mLocationOld.latitude)) * Math.cos(deg2rad(theta))
    dist = Math.acos(dist)
    dist = rad2deg(dist)
    dist *= 60.0 * 1.1515
    if (unit == 'K') {
        dist *= 1.609344
    } else if (unit == 'N') {
        dist *= 0.8684
    }
    return dist
}

fun deg2rad(deg: Double): Double {
    return deg * Math.PI / 180.0
}

fun rad2deg(rad: Double): Double {
    return rad * 180.0 / Math.PI
}