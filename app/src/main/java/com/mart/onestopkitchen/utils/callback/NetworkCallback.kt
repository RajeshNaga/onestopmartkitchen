package com.mart.onestopkitchen.utils.callback

/**
 * Created by DineshS on 9/11/2017.
 */
interface NetworkCallback {
    fun networkChange(isConnected: Boolean)
}