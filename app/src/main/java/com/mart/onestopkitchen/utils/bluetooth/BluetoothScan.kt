package com.mart.onestopkitchen.utils.bluetooth

import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import androidx.lifecycle.LiveData
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Looper
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


@SuppressWarnings("ALL")
class BluetoothScan(private var appContext: Context) : LiveData<List<BluetoothModel>>() {

    private var TAG: String = BluetoothScan::class.java.simpleName

    private var mBluetoothAdapter: BluetoothAdapter? = null

    private val RC_BLUETOOTH: Int = 99

    init {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    }

    private var allBluetoothList: MutableList<BluetoothModel>? = mutableListOf()

    private var scanResultReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                val rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, java.lang.Short.MIN_VALUE)
                val uuid = intent.getStringExtra(BluetoothDevice.EXTRA_UUID)

                println("Bluetooth device " + Gson().toJson(device))
                println("Bluetooth rssi " + Gson().toJson(rssi))

                addDeviceDetails(device!!, rssi, uuid)
            }
        }
    }

    private fun addDeviceDetails(device: BluetoothDevice?, rssi: Short, uuid: String) {

        synchronized(Looper.getMainLooper()) {
            if (value == null)
                value = mutableListOf()

            val mlist = value?.toMutableList()
            val isBeacom = when (device?.type) {
                BluetoothDevice.DEVICE_TYPE_LE -> true
                BluetoothDevice.DEVICE_TYPE_DUAL -> true
                else -> false
            }
            mlist?.add(BluetoothModel(Mac = device?.address!!,
                    CurrentRssi = rssi.toString(),
                    Name = device.name ?: "", Uuid = uuid, IBeacon = isBeacom.toString()))

            value = mlist
        }
    }

    private fun getPairedDevices(): ArrayList<BluetoothModel>? {
        var arrayOfAlreadyPairedBTDevices: ArrayList<BluetoothModel>? = null

        val pairedDevices = mBluetoothAdapter?.bondedDevices
        if (pairedDevices?.size!! > 0) {
            arrayOfAlreadyPairedBTDevices = ArrayList<BluetoothModel>()

            for (device in pairedDevices) {
                arrayOfAlreadyPairedBTDevices.add(BluetoothModel(device.address))
            }
        }

        return arrayOfAlreadyPairedBTDevices
    }

    fun scanBluetoothDevices(): Pair<BluetoothScan, Boolean> {
        if (mBluetoothAdapter == null) {
            Toast.makeText(appContext, "This device does not have a bluetooth adapter", Toast.LENGTH_SHORT).show()
            return Pair(this, false)
        }

        if (!mBluetoothAdapter?.isEnabled!!) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            (appContext as Activity).startActivityForResult(enableBtIntent, RC_BLUETOOTH)
            return Pair(this, false)
        }

        value = arrayListOf<BluetoothModel>()

        if (isBleAvailable())
            startBLeScanning()
        else
            mBluetoothAdapter?.startDiscovery()
        return Pair(this, true)
    }

    fun onPermissionActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_BLUETOOTH -> when (resultCode) {
                RESULT_OK -> {
                    scanBluetoothDevices()
                }
                RESULT_CANCELED -> Toast.makeText(appContext, "Bluetooth Permission Needed, for this operation", Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        private var instance: BluetoothScan? = null
        fun getInstance(appContext: Context) = instance?.also {
            instance!!.appContext = appContext
        }
                ?: synchronized(this) { BluetoothScan(appContext).also { instance = it } }
    }

    override fun onActive() {
        (appContext as Activity).registerReceiver(scanResultReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
        super.onActive()
    }

    override fun onInactive() {
        (appContext as Activity).unregisterReceiver(scanResultReceiver)
        super.onInactive()
    }

//    class BluetoothModel(var name: String?, var address: String?, var bondState: Int?, var type: Int?, var parcelIds: Array<ParcelUuid>?, rssi: Short)

    private var bleScanner: BluetoothLeScanner? = null

    var bleJob: Job? = null

    /***********************************************************************  BLE Scanner ***********************************************************/

    private fun isBleAvailable(): Boolean {
        return appContext.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
    }

    private fun startBLeScanning() {
        bleScanner = mBluetoothAdapter?.bluetoothLeScanner
        bleScanner?.run {
            bleJob = GlobalScope.launch {
                startScan(mBleCallback)
                delay(10000)
                stopScan(mBleCallback)
            }
        }
    }

    val mBleCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            addDeviceDetails(result)
        }
    }

    private fun addDeviceDetails(scanResult: ScanResult?) {

        synchronized(Looper.getMainLooper()) {
            if (value == null)
                value = mutableListOf()

            val mlist = value?.toMutableList()
            val scanRecord = scanResult?.scanRecord

            val isBeacom = when (scanResult?.device!!.type) {
                BluetoothDevice.DEVICE_TYPE_LE -> true
                BluetoothDevice.DEVICE_TYPE_DUAL -> true
                else -> false
            }

            mlist?.add(BluetoothModel(Mac = scanResult?.device!!.address,
                    CurrentRssi = scanResult?.rssi.toString(),
                    Name = scanRecord?.deviceName ?: "",
                    Uuid = UUID.nameUUIDFromBytes(scanRecord!!.bytes).toString(),
                    TxPower = scanRecord?.txPowerLevel.toString(),
                    CurrentTimestamp = Date(scanResult.timestampNanos).toString(),
                    IBeacon = isBeacom.toString()
            ))

            value = mlist
        }
    }


}