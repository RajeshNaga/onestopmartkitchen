package com.mart.onestopkitchen.utils;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.model.BillingAddress;
import com.mart.onestopkitchen.ui.adapter.AddressAdapter;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akash Garg
 */
public class AddressBottomSheet extends BottomSheetDialogFragment {

    @SuppressLint("StaticFieldLeak")
    static AddressBottomSheet addressBottomSheet = null;
    @BindView(R.id.recycler_view_address)
    RecyclerView recyclerView;

    public static AddressBottomSheet getInstance(List<BillingAddress> existingAddressList) {
        if (addressBottomSheet == null)
            addressBottomSheet = new AddressBottomSheet();

        Bundle bundle = new Bundle();
        bundle.putSerializable("Address", (Serializable) existingAddressList);
        addressBottomSheet.setArguments(bundle);
        return addressBottomSheet;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_address_bottom_sheet, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            List<BillingAddress> existingAddressList = (List<BillingAddress>) getArguments().getSerializable("Address");
            if (null != existingAddressList) {
                AddressAdapter addressAdapter = new AddressAdapter(getActivity(), existingAddressList);
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(addressAdapter);
            }
        }
        return view;
    }
}
