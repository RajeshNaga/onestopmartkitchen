package com.mart.onestopkitchen.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import com.mart.onestopkitchen.application.MyApplication;


public class ResourceUtils {

    public static String getString(@StringRes int stringId) {
        return MyApplication.getAppContext().getString(stringId);
    }

    public static Drawable getDrawable(@DrawableRes int drawableId) {
        return MyApplication.getAppContext().getResources().getDrawable(drawableId);
    }

    public static int getColor(@ColorRes int colorId) {
        return MyApplication.getAppContext().getResources().getColor(colorId);
    }

    public static int getDimen(@DimenRes int dimenId) {
        return (int) MyApplication.getAppContext().getResources().getDimension(dimenId);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}