package com.mart.onestopkitchen.utils;

/**
 * Created by Ashraful on 2/16/2016.
 */
public class AnalyticsUtil {

    public static String ANALYTICAL_EVENT = "screenViews";
    public static String ANALYTICAL_SCREEN_VARIABLE_NAME = "screen";
    public static String ANALYTICAL_OPEN_SCREEN_EVENT_NAME = "openScreen";
}
