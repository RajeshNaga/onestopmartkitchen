package com.mart.onestopkitchen.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.mart.onestopkitchen.service.PreferenceService;

/**
 * Created by Hemnath on 2/4/2019.
 */
public class CustomTextView extends androidx.appcompat.widget.AppCompatTextView {
    protected PreferenceService preferenceService = PreferenceService.getInstance();
    Context context;

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        Typeface font;
        String preferredLanguage = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
        if (preferredLanguage.equals(Language.ENGLISH)) {
            font = Typeface.createFromAsset(context.getAssets(), "fonts/centurygothic.ttf");
        } else {
            font = Typeface.createFromAsset(context.getAssets(), "fonts/zawgyione.ttf");
        }
        setTypeface(font);
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(tf);
    }
}
