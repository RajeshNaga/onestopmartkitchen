package com.mart.onestopkitchen.utils.mobileNumberValidation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OperatorsModel {
    @SerializedName("OperatorName")
    @Expose
    private String OperatorName = "";
    @SerializedName("maxLenth")
    @Expose
    private int maxLenth = 11;
    @SerializedName("numberSeries")
    @Expose
    private List<String> numberSeries = null;
    @SerializedName("minLength")
    @Expose
    private int minLength = 8;
    @SerializedName("isActive")
    @Expose
    private boolean isActive;
    @SerializedName("OperatorColor")
    @Expose
    private String OperatorColor = "#f6c600";

    public String getOperatorName() {
        return this.OperatorName;
    }

    public void setOperatorName(String OperatorName) {
        this.OperatorName = OperatorName;
    }

    public int getMaxLenth() {
        return this.maxLenth;
    }

    public void setMaxLenth(int maxLenth) {
        this.maxLenth = maxLenth;
    }

    public List<String> getNumberSeries() {
        return this.numberSeries == null ? new ArrayList<String>() : this.numberSeries;
    }

    public void setNumberSeries(List<String> numberSeries) {
        this.numberSeries = numberSeries;
    }

    public int getMinLength() {
        return this.minLength;
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getOperatorColor() {
        return OperatorColor;
    }

    public void setOperatorColor(String operatorColor) {
        OperatorColor = operatorColor;
    }
}
