package com.mart.onestopkitchen.utils.mobilevalidation;

import android.content.Context;
import androidx.annotation.NonNull;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.model.okdollar.NetworkOperatorModel;

import java.util.ArrayList;

/**
 */

public class LocalNumberParse {

    private static LocalNumberParse instance;
    private Context mContext;

    private LocalNumberParse() {
        mContext = MyApplication.getAppContext();
    }

    public static LocalNumberParse getInstance() {
        if (null == instance)
            instance = new LocalNumberParse();
        return instance;
    }

    private String[] getCountryCodeFlagNumber(@NonNull String countryCode, @NonNull String no, boolean isLogin) {
        String countryName = "", flagStr = "";
        int flag = -1;
        String[] countryData = null;
        ArrayList<NetworkOperatorModel> listOfCountryCodesAndNames = (ArrayList<NetworkOperatorModel>) MyApplication.getAppContext().getCachedObject(CACHEDKEY.NETWORKOPERATOR);
        if (countryCode.equalsIgnoreCase("+95"))
            countryData = getMyanmarData(countryCode, no);
        if (null != countryData)
            return countryData;
        try {
            no = formatNumber(no);
            int id = -1;

            if (no.startsWith("+") || no.startsWith("00")) {
                String code = countryCode.isEmpty() ? no.substring(0, 6) : countryCode.replace("+", "00");
                for (int i = 0; i < listOfCountryCodesAndNames.size(); i++) {
                    ArrayList<String> dialingCodes = listOfCountryCodesAndNames.get(i).getListOfDialingCodes();
                    if (dialingCodes != null) {
                        for (int j = 0; j < dialingCodes.size(); j++) {
                            String countryId = "00" + dialingCodes.get(j).substring(1);
                            if (no.length() >= 6) {
                                if (code.startsWith(countryId)) {
                                    id = i;
                                    no = no.replaceFirst(countryId.replace("+", "00"), "");
                                    countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                                    countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                                    flag = listOfCountryCodesAndNames.get(id).getCountryImageId();
                                    flagStr = String.valueOf(flag);
                                    break;
                                }
                            }
                            countryId = null;
                        }
                        dialingCodes = null;
                    } else {
                        String countryId = "00" + listOfCountryCodesAndNames.get(i).getCountryCode().substring(1);
                        if (code.startsWith(countryId)) {
                            id = i;
                            no = no.replaceFirst(countryId.replace("+", "00"), "");
                            countryCode = listOfCountryCodesAndNames.get(id).getCountryCode();
                            countryName = listOfCountryCodesAndNames.get(id).getCountryName();
                            flag = listOfCountryCodesAndNames.get(id).getCountryImageId();
                            flagStr = String.valueOf(flag);
                            break;
                        }
                    }
                }
                if (code.startsWith("0095")) {
                    no = "0" + no;
                }
            }

            if (id == -1) {
                //  if (no.startsWith("0")) {
                //     if (!AppPreference.getPrefsHelper().getCountryCode(mContext).equals("+95"))
                //          no = no.substring(1);
                // } else {
                no = "0" + no;
                //   }

                //     countryCode = AppPreference.getPrefsHelper().getCountryCode(mContext);
                //    flag = AppPreference.getPrefsHelper().getPref(isLogin ? AppPreference.getPrefsHelper().FLAG_ID_LOGIN : AppPreference.getPrefsHelper().FLAG_ID, -1);
                //    countryName = AppPreference.getPrefsHelper().getCountryName(mContext);
                //    flagStr = String.valueOf(flag);
            }

            if (no.trim().equals("0"))
                no = "";

            return new String[]{countryCode, countryName, flagStr, no};

        } catch (Exception e) {
            // countryCode = AppPreference.getPrefsHelper().getCountryCode(mContext);
            // flag = AppPreference.getPrefsHelper().getPref(isLogin ? AppPreference.getPrefsHelper().FLAG_ID_LOGIN : AppPreference.getPrefsHelper().FLAG_ID, -1);
            //  countryName = AppPreference.getPrefsHelper().getCountryName(mContext);
            flagStr = String.valueOf(flag);
            return new String[]{countryCode, countryName, flagStr, ""};
        } finally {
            countryName = null;
            flagStr = null;
            flag = -1;
            no = null;
            listOfCountryCodesAndNames.clear();
            listOfCountryCodesAndNames = null;
        }
    }

    public String[] getCountryCodeFlagNumber(@NonNull String countryCode, @NonNull String no) {
        return getCountryCodeFlagNumber(countryCode, no, false);
    }

    private String[] getMyanmarData(String countryCode, String no) {
        String countryName = "", flagStr = "";
        int flag = -1;
        try {
            if (countryCode.equals("+95")) {
                flag = R.drawable.myanmar_flag;
                countryName = "Myanmar";
                flagStr = String.valueOf(flag);
                String code = "00" + countryCode.substring(1);
                no = "0" + no.substring(code.length());
                if (no.trim().equals("0"))
                    no = "";
                return new String[]{countryCode, countryName, flagStr, no};
            }
            return null;
        } catch (Exception e) {
            //  countryCode = AppPreference.getPrefsHelper().getCountryCodeLogin(mContext);
            //   flag = AppPreference.getPrefsHelper().getPref(AppPreference.getPrefsHelper().FLAG_ID_LOGIN, 0);
            //   countryName = AppPreference.getPrefsHelper().getCountryNameLogin(mContext);
            //   flagStr = String.valueOf(flag);
            return new String[]{countryCode, countryName, flagStr, ""};
        } finally {
            countryName = null;
            flagStr = null;
            flag = -1;
        }
    }

    private String formatNumber(String number) {
        if (number.contains("+"))
            number = number.replace("+", "00");
        if (number.contains(" "))
            number.replace(" ", "");
        if (number.contains("-"))
            number = number.replace("-", "");
        return number;
    }

    private String[] getCountryCodeFlagNumber(@NonNull String number) {
        return getCountryCodeFlagNumber("", number, false);
    }

    public String[] getCountryCodeFlagNumber(@NonNull String number, boolean isLogin) {
        return getCountryCodeFlagNumber("", number, isLogin);
    }

    public String removeCountryCode(String number) {
        try {
            if (number.startsWith("+95")) {
                number = number.replace("+95", "0");
                return number;
            }
            if (number.startsWith("0095")) {
                number = number.replaceFirst("0095", "0");
                return number;
            } else {
                if (number.startsWith("+") || number.startsWith("00")) {
                    String[] countryCodeArray = getCountryCodeFlagNumber(number);
                    String scountryCode = countryCodeArray[0];
                    number = countryCodeArray[3];
                    if (number.contains(" ")) {
                        number = number.trim();
                        number = number.replace(" ", "");
                    }
                    return number.substring(scountryCode.length());
                } else {
                    return number;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getCountryCodeFromNumber(String numberWithCountryCode, String number) {
        try {
            if (!numberWithCountryCode.equals("") && !number.equals("")) {
                if (numberWithCountryCode.startsWith("0095") && number.startsWith("0")) {
                    number = number.substring(1);
                }
                String[] strings = numberWithCountryCode.split(number);
                return strings[0].replaceAll("00", "+");
            } else {
                return "+95";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "+95";
        }
    }


    private String formatUniversal(String mobileNo, String countryCode) {
        if (mobileNo.isEmpty() && mobileNo.length() > 3)
            return "";
        if (countryCode.equalsIgnoreCase("+95") && mobileNo.startsWith("0")) {
            mobileNo = "00" + countryCode.substring(1) + mobileNo.substring(1);
        } else {
            mobileNo = "00" + countryCode.substring(1) + mobileNo;
        }
        return mobileNo;
    }

    public String getFormatedNumber(String countryCode, String mobileNo) {
        if (mobileNo.isEmpty() && mobileNo.length() < 3)
            return "";
        if (countryCode.equalsIgnoreCase("+95") && mobileNo.startsWith("0")) {
            mobileNo = countryCode + mobileNo.substring(1);
        } else {
            mobileNo = countryCode + mobileNo;
        }
        return mobileNo;
    }

    public String addPlus(String mobileNo) {
        if (!mobileNo.isEmpty() && mobileNo.length() > 2)
            return "+" + mobileNo.substring(2);
        else
            return mobileNo;
    }

    public String replacePlus(String mobileNo) {
        if (!mobileNo.isEmpty() && mobileNo.length() > 2)
            return "00" + mobileNo.substring(1);
        else
            return mobileNo;
    }

    public String getFormatedNumberForRequest(String countryCode, String mobileNo) {
        return replacePlus(getFormatedNumber(countryCode, mobileNo));
    }

    public String getNumberWithoutCountryInitials(String number) {
        String numberWithZero = getCountryCodeFlagNumber(number)[3];
        if (numberWithZero.startsWith("0")) {
            if (numberWithZero.length() > 0)
                return numberWithZero.substring(1, numberWithZero.length());
        }
        return numberWithZero;
    }

}
