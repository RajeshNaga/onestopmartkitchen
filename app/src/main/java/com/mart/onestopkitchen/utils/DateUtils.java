package com.mart.onestopkitchen.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static String getInputType(int type) {

        return type == 1 ? "yyyy-MM-dd'T'HH:mm:ss" :
                type == 2 ? "yyyy-MM-dd'T'HH:mm:ss.SSS" :
                        type == 3 ? "yyyy-MM-dd HH:mm:ss" :
                                type == 4 ? "EEE MMM dd HH:mm:ss zzz yyyy" :
                                        type == 5 ? "dd-MMM-yyyy" :
                                                "";
    }

    public static String getOutputType(int type) {
        return type == 1 ? "E, dd-MMM-yyyy HH:mm:ss" :
                type == 2 ? "yyyy" :
                        type == 3 ? "E, dd-MMM-yyyy" :
                                type == 4 ? "EEE,dd-MMM-yyyy" :
                                        type == 5 ? "MMM yyyy" :
                                                type == 6 ? "dd-MM-yyyy" :
                                                        type == 7 ? "MM dd yyyy" :
                                                                type == 8 ? "dd-MMM-yyyy HH:mm:ss" :
                                                                        type == 9 ? "yyyy-MM-dd HH:mm:ss" :
                                                                                type == 10 ? "yyyy-MM-dd" :
                                                                                        type == 11 ? "dd-MMM-yyyy" :
                                                                                                type == 12 ? "hh:mm a" :
                                                                                                        type == 13 ? "HH:mm:ss a" :
                                                                                                                type == 14 ? "dd-MM-yyyy hh:mm a" :
                                                                                                                        type == 15 ? "dd-MMM-yyyy hh:mm:ss a" :
                                                                                                                                type == 16 ? "HH:mm:ss" :
                                                                                                                                        type == 17 ? "yyyy-MM-dd'T'HH:mm:ss.SSS" :

                                                                                                                                                "";
    }

    public static String getDateString(Object obj, String inputType, String outputType) {
        String o = parse(false, obj, inputType, outputType).toString();
        return o == null ? "" : o;
    }

    public static Date getDate(Object obj, String inputType, String outputType) {
        return (Date) parse(true, obj, inputType, outputType);
    }

    private static Object parse(boolean date, Object obj, String inputType, String outputType) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputType, Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputType, Locale.ENGLISH);
        try {
            if (date)
                return outputFormat.parse(inputFormat.format(obj.toString()));
            else
                return outputFormat.parse(inputFormat.parse(obj.toString()).toString());
        } catch (ParseException parseException) {
            return "";
        }
    }

    public static Date getDate(String date, String parsingType) {
        DateFormat df = new SimpleDateFormat(parsingType, Locale.ENGLISH);
        try {
            return df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String parseDate(Date date, String parsingType) {
        DateFormat df = new SimpleDateFormat(parsingType, Locale.ENGLISH);
        return df.format(date);
    }
}
