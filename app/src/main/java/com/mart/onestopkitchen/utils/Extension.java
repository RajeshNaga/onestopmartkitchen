package com.mart.onestopkitchen.utils;

import androidx.fragment.app.Fragment;

import com.mart.onestopkitchen.R;

public class Extension {
    public static void replaceFragmentSafely(Fragment fragment) {
        fragment.getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();

    }
}
