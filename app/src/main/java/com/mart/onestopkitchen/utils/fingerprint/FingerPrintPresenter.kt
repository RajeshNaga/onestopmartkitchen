package com.mart.onestopkitchen.utils.fingerprint

import android.Manifest
import android.annotation.TargetApi
import android.app.KeyguardManager
import android.content.Context
import android.content.Context.FINGERPRINT_SERVICE
import android.content.Context.KEYGUARD_SERVICE
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey

private val KEY_NAME = "yourKey"
private var cipher: Cipher? = null
private var keyStore: KeyStore? = null
private var keyGenerator: KeyGenerator? = null
private var cryptoObject: FingerprintManager.CryptoObject? = null
private var fingerprintManager: FingerprintManager? = null
private var keyguardManager: KeyguardManager? = null

/**
 * Created by Akash Garg on 17/01/2019.
 */


@TargetApi(Build.VERSION_CODES.M)
@RequiresApi(Build.VERSION_CODES.M)
class FingerPrintPresenter(mContext: Context, private val fingerPrintBaseView: FingerprintBaseView) : FingerprintResult {
    init {
        keyguardManager = mContext.getSystemService(KEYGUARD_SERVICE) as KeyguardManager
        fingerprintManager = mContext.getSystemService(FINGERPRINT_SERVICE) as FingerprintManager

        @Suppress("DEPRECATION")
        if (!fingerprintManager!!.isHardwareDetected) {
            fingerPrintBaseView.showMsg("Your device doesn't support fingerprints authentication")
        } else if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            fingerPrintBaseView.showMsg("Please enable the fingerprints permission")
        } else if (!fingerprintManager!!.hasEnrolledFingerprints()) {
            fingerPrintBaseView.showMsg("No Fingerprints configured. Please register at least one fingerprints in your device's Settings")
        } else if (!keyguardManager!!.isKeyguardSecure) {
            fingerPrintBaseView.showMsg("Please enable lockscreen security in your device's Settings")
        } else {
            fingerPrintBaseView.showFingerPrintPopup()
            try {
                generateKey()
            } catch (e: FingerprintException) {
                e.printStackTrace()
            }

            if (initCipher()) {
                cryptoObject = FingerprintManager.CryptoObject(cipher)
                val helper = FingerprintHandler(mContext, this)
                helper.startAuth(fingerprintManager!!, cryptoObject!!)
            }
        }
    }

    @Throws(FingerprintException::class)
    private fun generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore")
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
            keyStore?.load(null)
            keyGenerator?.init(KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build())

            keyGenerator?.generateKey()

        } catch (exc: KeyStoreException) {
            exc.printStackTrace()
            throw FingerprintException(exc)
        } catch (exc: NoSuchAlgorithmException) {
            exc.printStackTrace()
            throw FingerprintException(exc)
        } catch (exc: NoSuchProviderException) {
            exc.printStackTrace()
            throw FingerprintException(exc)
        } catch (exc: InvalidAlgorithmParameterException) {
            exc.printStackTrace()
            throw FingerprintException(exc)
        } catch (exc: CertificateException) {
            exc.printStackTrace()
            throw FingerprintException(exc)
        } catch (exc: IOException) {
            exc.printStackTrace()
            throw FingerprintException(exc)
        }
    }


    private fun initCipher(): Boolean {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to get Cipher", e)
        } catch (e: NoSuchPaddingException) {
            throw RuntimeException("Failed to get Cipher", e)
        }

        try {
            keyStore?.load(null)
            val key: SecretKey? = keyStore?.getKey(KEY_NAME, null) as? SecretKey
            cipher?.init(Cipher.ENCRYPT_MODE, key)
            return true
        } catch (e: KeyPermanentlyInvalidatedException) {
            return false
        } catch (e: KeyStoreException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: CertificateException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: UnrecoverableKeyException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: IOException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: NoSuchAlgorithmException) {
            return false
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: InvalidKeyException) {
            return false
            throw RuntimeException("Failed to init Cipher", e)
        }

    }

    override fun authenticationSuccess() {
        fingerPrintBaseView.success()
    }

    override fun authenticationFailure() {
        fingerPrintBaseView.failure()
    }

    override fun authenticationError() {
        fingerPrintBaseView.error()
    }

    private inner class FingerprintException(e: Exception) : Exception(e)

}