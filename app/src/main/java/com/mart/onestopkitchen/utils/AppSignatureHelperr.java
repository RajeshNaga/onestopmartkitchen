package com.mart.onestopkitchen.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.mart.onestopkitchen.BuildConfig;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;


public class AppSignatureHelperr extends ContextWrapper {
    public static final String TAG = AppSignatureHelperr.class.getSimpleName();
    public static final int NUM_HASHED_BYTES = 9;
    public static final int NUM_BASE64_CHAR = 11;
    private static final String HASH_TYPE = "SHA-256";

    public AppSignatureHelperr(Context context) {
        super(context);
    }

    private static String hash(Context mContext, String packageName, String signature) {
        String appInfo = packageName + " " + signature;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(HASH_TYPE);
            messageDigest.update(appInfo.getBytes(StandardCharsets.UTF_8));
            byte[] hashSignature = messageDigest.digest();

            // truncated into NUM_HASHED_BYTES
            hashSignature = Arrays.copyOfRange(hashSignature, 0, NUM_HASHED_BYTES);
            // encode into Base64
            String base64Hash = Base64.encodeToString(hashSignature, Base64.NO_PADDING | Base64.NO_WRAP);
            base64Hash = base64Hash.substring(0, NUM_BASE64_CHAR);

            Log.e(TAG, "##----1Stop Kitchen - pkg--- : " + packageName + "\n  - HashKey-: " + base64Hash + " \n BuildType-: " + BuildConfig.BUILD_TYPE);
            Toast.makeText(mContext, "Pkg-: " + packageName + "\n  - HashKey-: " + base64Hash + " \n BuildType-: " + BuildConfig.BUILD_TYPE, Toast.LENGTH_LONG).show();
            return base64Hash;
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "hash:NoSuchAlgorithm", e);
        }
        return null;
    }

    @SuppressLint("PackageManagerGetSignatures")
    public ArrayList<String> getAppSignatures(Context mContext) {
        ArrayList<String> appCodes = new ArrayList<>();

        try {
            String packageName = getPackageName();
            PackageManager packageManager = getPackageManager();
            Signature[] signatures = packageManager.getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES).signatures;

            // For each signature create a compatible hash
            for (Signature signature : signatures) {
                String hash = hash(mContext, packageName, signature.toCharsString());
                if (hash != null) {
                    appCodes.add(String.format("%s", hash));
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Unable to find package to obtain hash.", e);
        }
        return appCodes;
    }
}