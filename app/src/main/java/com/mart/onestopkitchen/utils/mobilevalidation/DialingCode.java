package com.mart.onestopkitchen.utils.mobilevalidation;

import com.google.gson.annotations.SerializedName;

public class DialingCode {

    @SerializedName("City")
    private String mCity;
    @SerializedName("Code")
    private String mCode;

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String Code) {
        mCode = Code;
    }

}
