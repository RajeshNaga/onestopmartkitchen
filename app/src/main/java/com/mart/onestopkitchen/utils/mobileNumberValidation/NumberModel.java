package com.mart.onestopkitchen.utils.mobileNumberValidation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NumberModel {
    @SerializedName("operators")
    @Expose
    private List<OperatorsModel> operators = new ArrayList<>(0);
    @SerializedName("countryCode")
    @Expose
    private String countryCode = "";
    @SerializedName("countryName")
    @Expose
    private String countryName = "";

    public NumberModel() {
    }

    public NumberModel(String country) {
        this.countryCode = country;
    }

    public List<OperatorsModel> getOperators() {
        return this.operators;
    }

    public void setOperators(List<OperatorsModel> operators) {
        this.operators = operators;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return this.countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
