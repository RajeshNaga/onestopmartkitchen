package com.mart.onestopkitchen.utils

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.util.Log
import com.mart.onestopkitchen.model.PaymentResponseModel
import com.mart.onestopkitchen.model.SaveTransactionStatusModel
import com.mart.onestopkitchen.networking.RetroClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.net.ConnectException

class ConfirmOrderResource(var request: SaveTransactionStatusModel) {

    private var data: MutableLiveData<PaymentResponseModel> = MutableLiveData()

    init {
        getPlaceOrder()
    }

    fun getResponse(): LiveData<PaymentResponseModel>? {
        return data
    }


    @SuppressLint("CheckResult")
    fun getPlaceOrder() {
        getPlaceorderObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver())
    }

    private fun getObserver(): DisposableObserver<PaymentResponseModel> {
        return object : DisposableObserver<PaymentResponseModel>() {
            override fun onComplete() {
            }

            override fun onNext(result: PaymentResponseModel) {
                data.postValue(result)
                Log.e("ConfirmOrderResource##", "onNext-----: $result")
            }

            override fun onError(e: Throwable) {
                if (e is ConnectException) {
                    val error = PaymentResponseModel()
                    error.statusCode = 401
                    data.postValue(error)
                }
                Log.e("----", "#No Internet Connectoion -----: ${e.message}")
            }
        }
    }

    private fun getPlaceorderObservable(): Observable<PaymentResponseModel> {
        return RetroClient.getApi().confirmCheckout(request)
    }
}