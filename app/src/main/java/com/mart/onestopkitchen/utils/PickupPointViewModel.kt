package com.mart.onestopkitchen.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mart.onestopkitchen.networking.response.PickupPointAddressResponse
import com.mart.onestopkitchen.presenter.PickupPointPresenter

open class PickupPointViewModel : ViewModel() {
    private var pickPointResponse: LiveData<PickupPointAddressResponse>? = null
    private var pickupPointPresenter: PickupPointPresenter? = null

    init {
        pickupPointPresenter = PickupPointPresenter()
    }

    fun getPickupPointList(): LiveData<PickupPointAddressResponse>? {
        pickupPointPresenter = PickupPointPresenter()
        if (pickPointResponse == null) {
            pickPointResponse = pickupPointPresenter?.getResponse()
        }
        return pickPointResponse
    }

    fun releaseObj() {
        pickPointResponse = null
        pickupPointPresenter = null
    }
}