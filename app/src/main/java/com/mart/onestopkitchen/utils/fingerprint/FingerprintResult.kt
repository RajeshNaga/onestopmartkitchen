package com.mart.onestopkitchen.utils.fingerprint

interface FingerprintResult {
    fun authenticationSuccess()
    fun authenticationFailure()
    fun authenticationError()
}