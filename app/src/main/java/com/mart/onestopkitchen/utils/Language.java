package com.mart.onestopkitchen.utils;


/**
 * Created by BS62 on 26-Oct-16.
 */

public class Language {
    public static final String ENGLISH = "en";
    public static final String PERSIAN = "fa";
    public static final String ARABIC = "ar";
    public static final String ITALIAN = "it";
    public static final String MYANMAR = "my";
    public static final String MYANMAR_UNICODE = "mm";
    public static final String LANG_EN = "en_EN";
    public static final String LANG_MY = "my_MM";

}
