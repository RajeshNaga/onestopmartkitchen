package com.mart.onestopkitchen.utils.wifi

import android.annotation.SuppressLint
import android.app.Activity
import androidx.lifecycle.LiveData
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import androidx.appcompat.app.AlertDialog

@SuppressLint("ALL")
class WifiScan(private var appContext: Context) : LiveData<List<ScanResult>>() {

    var wifi: WifiManager? = null

    private var isWifiEnabledForcefully: Boolean = false

    fun with(appContext: Context): WifiScan {
        this.appContext = appContext
        return getInstance(appContext)
    }

    val scanResultreceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            value = wifi?.scanResults as MutableList<ScanResult>
            if (isWifiEnabledForcefully) {
                isWifiEnabledForcefully = false
                wifi?.isWifiEnabled = false
            }
        }
    }

    init {
        wifi = appContext.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }

    override fun onActive() {
        super.onActive()
        (appContext as Activity).registerReceiver(scanResultreceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
    }

    override fun onInactive() {
        super.onInactive()
        (appContext as Activity).unregisterReceiver(scanResultreceiver)
    }

    private fun isWifiEnabled(): Boolean {
        return wifi?.isWifiEnabled!!
    }

    fun startScanning(): WifiScan {
        if (!isWifiEnabled() && !isWifiEnabledForcefully) {
            value = arrayListOf()
            // enableWifi()
            wifi?.isWifiEnabled = true; isWifiEnabledForcefully = true
        }

        wifi?.startScan()

        return this
    }

    private fun enableWifi() {

        val mAlert: AlertDialog.Builder

        mAlert = AlertDialog.Builder(appContext)
                .setTitle("Wifi Enable Needed")
                .setMessage("Wifi is Off Cannot Scan, Enable it for scanning")
                .setPositiveButton("Enable Wifi") { dialog, which -> run { dialog.dismiss(); wifi?.isWifiEnabled = true; isWifiEnabledForcefully = true;startScanning(); } }
                .setNegativeButton("Dismiss") { dialog, which -> dialog.dismiss(); }
                .setCancelable(false)

        mAlert.show()
    }

    companion object {
        private var instance: WifiScan? = null
        fun getInstance(appContext: Context) = instance
                ?: synchronized(this) { WifiScan(appContext).also { instance = it } }
    }
}