package com.mart.onestopkitchen.utils.wifi

class WifiModel(
        var CenterChannel: String? = "",
        var Bssid: String? = "",
        var PrimaryChannel: String? = "",
        var PrimaryFrequency: String? = "0",
        var CenterFrequency: String? = "0",
        var Security: String? = "",
        var Ssid: String? = "",
        var Width: String? = "",
        var Strength: String? = "",
        var Distance: String? = "",
        var Timestamp: String? = ""

) {
    override fun toString(): String {
        return "WifiModel(CenterChannel=$CenterChannel, Bssid=$Bssid, PrimaryChannel=$PrimaryChannel, PrimaryFrequency=$PrimaryFrequency, CenterFrequency=$CenterFrequency, Security=$Security, Ssid=$Ssid, Width=$Width, Strength=$Strength, Distance=$Distance, Timestamp=$Timestamp)"
    }
}
