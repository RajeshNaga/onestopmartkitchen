package com.mart.onestopkitchen.utils.location

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Looper
import com.mart.onestopkitchen.utils.AppUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.exceptions.CompositeException
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.HashMap


class GetAutoLocationDetails {
    private var activeLocale = ""
    private var defaultTag = ""
    private var mCallback: OnLocationFetched? = null
    private var dataIntent: Intent? = null
    private var showProgress: Boolean = false
    var mContext: Context? = null

    private val defaultAddress: Address
        get() {
            val address = Address(Locale(activeLocale))
            address.latitude = 0.0
            address.longitude = 0.0
            address.adminArea = "XXXX"
            address.countryCode = "MY"
            address.featureName = "XXXX"
            address.countryName = "Myanmar"
            address.locality = "XXXX"
            address.subAdminArea = "XXXX"
            address.subLocality = "XXXX"
            return address
        }

    @SuppressLint("CheckResult")
    fun getCurrentLocationDetails(mCallback: OnLocationFetched?, mVarargs: List<String> = mutableListOf("en", "my")) {
        this.mCallback = mCallback
        Observable.fromIterable(mVarargs).map {
            getAddress(dataIntent!!.getDoubleExtra("lat", 0.0), dataIntent!!.getDoubleExtra("lan", 0.0), dataIntent!!.getStringExtra("search"), it)
        }.dropBreadcrumb().doOnError {
            it.printStackTrace()
        }.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.from(Looper.myLooper())).subscribe {
            setLocation(it)
        }
    }

    fun initialize(intent: Intent) {
        this.dataIntent = intent
        activeLocale = intent.getStringExtra("activeLocale")
        defaultTag = intent.getStringExtra("default")
    }

    private fun getAddress(latitude: Double, longitude: Double, streetAddress: String, locale: String): Intent {
        var city = ""
        var state = ""
        var roadName: String = ""
        var oldName = ""
        val intent = Intent()
        val mData = HashMap<String, String>()
        mData["road"] = roadName
        mData["city"] = city
        mData["state"] = state
        mData["oldName"] = oldName
        var address = defaultAddress
        try {
            if (!AppUtils.isReallyConnectedToInternet()) {
                mData["oldName"] = defaultTag
                intent.putExtra("data", mData)
                intent.putExtra("address", address)
                intent.putExtra("activeLocale", locale)
                //setLocation(mData);
                return intent
            }
            val geocoder = Geocoder(mContext, Locale(locale))
            var addresses: List<Address>? = null
            if (streetAddress.isEmpty())
                addresses = geocoder.getFromLocation(latitude, longitude, 5)
            else
                addresses = geocoder.getFromLocationName(streetAddress, 5)

            if (addresses != null && addresses.isNotEmpty()) {
                addresses = addresses.sortedWith(compareBy({ it.latitude }, { it.latitude }))
                address = addresses[0]
                city = validateCity(address)
                state = validateState(address)
                roadName = validateStreet(address)
            }

            if (locale.equals("my", ignoreCase = true))
                oldName = UnicodeConverter.unicodeToZwagi((if (roadName != null) "$roadName-" else "") + state + " ၿမိဳ႕နယ္" + "-" + city.replace("Region", "").replace("region", ""))
            else
                oldName = (if (roadName != null) "$roadName, " else "") + state.trim { it <= ' ' } + " Township" + ", " + city.replace("Region", "").replace("region", "").trim { it <= ' ' }
        } catch (ignored: Exception) {
            //ignored.printStackTrace();
        }

        oldName = oldName.replace("-", " ၊ ")

        if (oldName.isEmpty())
            oldName = defaultTag

        mData["road"] = roadName
        mData["city"] = city
        mData["state"] = state
        mData["oldName"] = oldName
        intent.putExtra("data", mData)
        intent.putExtra("address", address)
        intent.putExtra("activeLocale", locale)
        //setLocation(mData);
        return intent
    }

    private fun validateCity(address: Address): String {
        if (address.adminArea != null)
            return address.adminArea
        return if (address.subAdminArea != null) address.subAdminArea else ""
    }

    private fun validateState(address: Address): String {
        if (address.subLocality != null)
            return address.subLocality
        return if (address.locality != null) address.locality else ""
    }

    private fun validateStreet(address: Address): String {
        return if (address.featureName != null) address.featureName else ""
    }

    private fun setLocation(intent: Intent) {
        try {
            if (null != mCallback) {
                if (intent.getStringExtra("activeLocale") == null)
                    return
                val data = intent.getSerializableExtra("data") as HashMap<String, String>
                val address = intent.getParcelableExtra<Address>("address")
                if (!intent.getStringExtra("activeLocale").equals("my", ignoreCase = true))
                    mCallback!!.onLocationEnglish(data["road"]!!, data["city"]!!, data["state"]!!, data["oldName"]!!, address, activeLocale)
                if (intent.getStringExtra("activeLocale").equals("my", ignoreCase = true)) {
                    mCallback!!.onLocationBurmese(data["road"]!!, data["city"]!!, data["state"]!!, data["oldName"]!!, address, activeLocale)
                }
            }
        } catch (ignored: Exception) {
            ignored.printStackTrace()
        }
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var INSTANCE: GetAutoLocationDetails? = null

        fun getInstance(mContext: Context): GetAutoLocationDetails = INSTANCE?.also { it.mContext = mContext }
                ?: GetAutoLocationDetails().also { it.mContext = mContext;INSTANCE = it }
    }
}


inline fun <T> Observable<T>.dropBreadcrumb(): Observable<T> {
    val breadcrumb = BreadcrumbException()
    return this.onErrorResumeNext { error: Throwable ->
        throw CompositeException(error, breadcrumb)
    }
}

class BreadcrumbException : Exception()

interface OnLocationFetched {
    fun onLocationEnglish(roadName: String, city: String, state: String, fullAddress: String, address: Address, locale: String)

    fun onLocationBurmese(roadName: String, city: String, state: String, fullAddress: String, address: Address, locale: String)
}