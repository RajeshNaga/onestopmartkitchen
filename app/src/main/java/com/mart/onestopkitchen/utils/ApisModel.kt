package com.mart.onestopkitchen.utils

import com.mart.onestopkitchen.BuildConfig


/**
 * Created by Akash Garg 01/10/2018
 */

class ApisModel {
    var BASE_URL = ""
    var OK_DOLLAR_PROFILE = ""
    var CONTACT_SYN = ""
    var CICO_AGENT_LIST = ""
    var NST_KEY = ""
    var NST_SECRET = ""
    var OK_DOLLAR_URL = ""

    init {
        initServer()
    }

    companion object {
        private var Instance: ApisModel? = null
        fun getInstance(): ApisModel = Instance ?: ApisModel().apply { Instance = this }
    }

    private fun initServer() {
        if (BuildConfig.isProduction) {
            productionServer()
        } else {
            stagingServer()
        }
    }

    private fun stagingServer() {
        BASE_URL = "http://69.160.4.149:1005/api/"
        OK_DOLLAR_PROFILE = "https://www.okdollar.co/"
        CONTACT_SYN = "http://advertisement.api.okdollar.org/"
        CICO_AGENT_LIST = "http://120.50.43.157:1318/"
        NST_KEY = "bm9wU3RhdGlvblRva2Vu"
        NST_SECRET = "bm5xS6V5"
        OK_DOLLAR_URL = "https://www.okdollar.co/"
    }

    private fun productionServer() {
        BASE_URL = "https://www.zayok.com/api/"
        OK_DOLLAR_PROFILE = "https://www.okdollar.co/"
        CONTACT_SYN = "http://advertisement.api.okdollar.org/"
        CICO_AGENT_LIST = "http://120.50.43.157:1318/"
        NST_KEY = "bm9wU3RhdGlvblRva2Vu"
        NST_SECRET = "bm9wS2V5"
        OK_DOLLAR_URL = "https://www.okdollar.co/"
    }
}
