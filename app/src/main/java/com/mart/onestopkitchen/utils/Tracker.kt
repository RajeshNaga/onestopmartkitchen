package com.mart.onestopkitchen.utils

import android.annotation.SuppressLint
import androidx.lifecycle.Observer
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.location.Location
import android.net.wifi.ScanResult
import androidx.appcompat.app.AppCompatActivity
import com.mart.onestopkitchen.utils.bluetooth.BluetoothModel
import com.mart.onestopkitchen.utils.bluetooth.BluetoothScan
import com.mart.onestopkitchen.utils.callback.CurrentLocationCallback
import com.mart.onestopkitchen.utils.location.CurrentLocationListener
import com.mart.onestopkitchen.utils.location.GetAutoLocationDetails
import com.mart.onestopkitchen.utils.location.LocationSettings
import com.mart.onestopkitchen.utils.location.OnLocationFetched
import com.mart.onestopkitchen.utils.wifi.WifiModel
import com.mart.onestopkitchen.utils.wifi.WifiScan
import com.mart.onestopkitchen.utils.wifi.WifiUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*


/**
 * Created by dinesh on 9/1/18
 *
 */

class Tracker {

    private var locationDisposable: Disposable? = null

    private var latestLocation: Location? = null

    private var oldLocation: Location? = null

    @SuppressLint("StaticFieldLeak")
    var locationSettings: LocationSettings? = null

    fun trackLocation(mActivity: AppCompatActivity, mCallback: CurrentLocationCallback? = null) {

        if (locationDisposable != null && !locationDisposable!!.isDisposed) {
            locationSettings!!.getLocationOnObserver()
            return
        }
        locationSettings = LocationSettings(mActivity)
        locationDisposable = locationSettings!!.getObserver()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { isSettingsValid ->
                    run {
                        if (isSettingsValid) {
                            getCurrentLocation(mActivity = mActivity, mCallback = mCallback)
                        }
                    }
                }
    }

    private fun getCurrentLocation(mActivity: AppCompatActivity, mCallback: CurrentLocationCallback? = null) {
        CurrentLocationListener.getInstance(mActivity).observe(mActivity, Observer { location ->
            mActivity.isFinishing.takeIf { !it }?.apply {
                oldLocation ?: run { oldLocation = location }
                latestLocation = location
                location?.apply { mCallback?.currentLocation(location, oldLocation, calculateDistance(location, oldLocation!!, 'M')) }
            }
        }
        )
    }

    fun disableTrack(mActivity: AppCompatActivity) {
        locationDisposable?.dispose()
        locationSettings = null
        latestLocation = null
        CurrentLocationListener.getInstance(mActivity).removeObservers(mActivity)
    }

    fun getLatestLocation(): Location? {
        return latestLocation
    }

    /**************************** bluetooth track **********************/

    @SuppressLint("StaticFieldLeak")
    var bluetoothScanning: BluetoothScan? = null

    var isBluetoothObserving: Boolean = false

    private var latestBluetoothResult: List<BluetoothModel>? = arrayListOf()

    fun trackBluetooth(mActivity: AppCompatActivity) {
        bluetoothScanning = BluetoothScan.getInstance(mActivity)
        if (isBluetoothObserving)
            bluetoothScanning!!.scanBluetoothDevices()
        else if (BluetoothScan.getInstance(mActivity).scanBluetoothDevices().second) {
            bluetoothScanning!!.scanBluetoothDevices().first.observe(mActivity, Observer { mScanResult -> saveScanResult(mScanResult) })
            isBluetoothObserving = true
        }
    }

    private fun saveScanResult(mScanResult: List<BluetoothModel>?) {
        mScanResult?.takeIf { it.isNotEmpty() }?.apply {
            val list = latestBluetoothResult?.toMutableList()
            list?.addAll(mScanResult)
            latestBluetoothResult = list
        }
    }

    fun getBluetoothResult(): List<BluetoothModel> {
        return latestBluetoothResult!!
    }

    fun disableBluetoothTrack(mActivity: AppCompatActivity, languageChanged: Boolean) {
        bluetoothScanning = null
        isBluetoothObserving = false
        latestBluetoothResult = listOf()
        BluetoothScan.getInstance(mActivity).bleJob?.cancel()
        BluetoothScan.getInstance(mActivity).removeObservers(mActivity)
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter.isEnabled && !languageChanged) {
            mBluetoothAdapter.disable()
        }
    }


    /********************** wifi track *************************/

    private var isObserving: Boolean = false

    private var mWifiScanResult: List<ScanResult>? = arrayListOf()

    fun trackWifi(mActivity: AppCompatActivity) {
        if (isObserving)
            WifiScan.getInstance(mActivity).with(mActivity).startScanning()
        else
            WifiScan.getInstance(mActivity).with(mActivity).startScanning().observe(mActivity, Observer { mScanResult -> saveResult(mScanResult) })

        isObserving = true
    }

    private fun saveResult(mList: List<ScanResult>?) {

        mList?.takeIf {
            it.isNotEmpty()
        }?.apply {
            val list = mWifiScanResult?.toMutableList()
            list?.addAll(mList!!)
            mWifiScanResult = list
            mWifiScanResult = mWifiScanResult?.distinctBy { it.SSID }
        }
    }

    fun getWifiScanResult(): List<WifiModel> {
        val utils = WifiUtils()
        val list: MutableList<WifiModel> = mutableListOf()
        mWifiScanResult?.forEach {
            list.add(WifiModel(Bssid = it.BSSID,
                    Distance = utils.calculateDistance(it.level.toDouble(),
                            it.frequency.toDouble()).toString(),
                    Ssid = it.SSID,
                    PrimaryChannel = utils.getChannelFromFrequency(frequency = it.frequency).toString(),
                    Timestamp = Date(it.timestamp).toString(), Strength = it.level.toString(), Security = it.capabilities))
        }
        return list
    }

    fun disableWifiTrack(mActivity: AppCompatActivity) {
        isObserving = false
        mWifiScanResult = listOf()
        WifiScan.getInstance(mActivity).removeObservers(mActivity)
    }


    /**************************************** Get Location From Geo Coder *************************************/

    fun getGeoCoderLocation(mContext: Context, mLat: Double, mLang: Double, mSearchAddress: String = "", mCallback: OnLocationFetched? = null, locales: MutableList<String> = mutableListOf("en,my")) {
        val locationDetails = GetAutoLocationDetails.getInstance(mContext)
        val mLocationIntent = Intent()
        mLocationIntent.putExtra("lat", mLat)
        mLocationIntent.putExtra("lan", mLang)
        mLocationIntent.putExtra("search", mSearchAddress)
        mLocationIntent.putExtra("activeLocale", getActiveLocale())
        mLocationIntent.putExtra("default", "Current Location")
        locationDetails.initialize(mLocationIntent)
        locationDetails.getCurrentLocationDetails(mCallback)
    }

    private fun getActiveLocale(): String {
        return Language.ENGLISH.contains(Locale.getDefault().language).let { "en" } ?: "my"
    }

}