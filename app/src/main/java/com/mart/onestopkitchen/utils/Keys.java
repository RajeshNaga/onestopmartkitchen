package com.mart.onestopkitchen.utils;

public interface Keys {
    String DATABASE_NAME = "mart.db";
    String PAYMENT_RESULT = "payment_result";
    String AMOUNT = "amount";
    String COUNTRY_CODE = "country_code";
    String MOBILE_NO = "mobile-no";
    String PAYMENT_RESULT_DETAILS = "result_data";
    String PRODUCT_NAME = "product_name";
    String HEADER_KEY_DEVICE_ID = "DeviceId";
    String HEADER_KEY_TOKEN = "Token";
    String HEADER_KEY_NST = "NST";
    String PAYMENT_DONE_RESPONSE = "PaymentDoneResponse";
    String IMAGE_PATH = "imagePath";
    String BILLING_ADDRESS = "billing_address";
    String INVOICE_NAME = "Invoice";
    String OK_DOLLAR_PACKAGE_NAME = "com.jas.digitalkyats";
    String PDF_NAME = "zayok_pdf";
    String ZAYOK_PACKAGE = "com.mart.onestopkitchen";
    int ANRDOID = 10;
    String SIMPLE_DATE_FORMAT = "dd-MMM-yyyy";
    String OKDOLLAR_PAYMENT = "Payments.OKDollar";
    String MY_ACCOUNT = "myaccount";
    int SMS_REQUEST_CODE = 10001;
    String WISH_LIST = "WishListFragment";
    String CUS_ORDER = "CustomerOrdersFragment";
    String MYANMAR_COUNTRY_ID = "60";
    String MYANMAR_COUNTRY_CODE = "+95";

}

