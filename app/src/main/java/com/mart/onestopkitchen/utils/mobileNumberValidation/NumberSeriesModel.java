package com.mart.onestopkitchen.utils.mobileNumberValidation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NumberSeriesModel {
    @SerializedName("msg")
    @Expose
    private String msg = "";
    @SerializedName("code")
    @Expose
    private int code = 300;
    @SerializedName("validations")
    @Expose
    private List<NumberModel> validations;

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<NumberModel> getValidations() {
        return this.validations;
    }

    public void setValidations(List<NumberModel> validations) {
        this.validations = validations;
    }
}
