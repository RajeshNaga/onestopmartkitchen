package com.mart.onestopkitchen.utils.mobilevalidation;

import com.google.gson.annotations.SerializedName;

public class Network {

    @SerializedName("MNC")
    private String mMNC;
    @SerializedName("NetworkName")
    private String mNetworkName;
    @SerializedName("OperatorName")
    private String mOperatorName;
    @SerializedName("Status")
    private String mStatus;

    public String getMNC() {
        return mMNC;
    }

    public void setMNC(String MNC) {
        mMNC = MNC;
    }

    public String getNetworkName() {
        return mNetworkName;
    }

    public void setNetworkName(String NetworkName) {
        mNetworkName = NetworkName;
    }

    public String getOperatorName() {
        return mOperatorName;
    }

    public void setOperatorName(String OperatorName) {
        mOperatorName = OperatorName;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

}
