package com.mart.onestopkitchen.utils;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.mart.onestopkitchen.R;


/**
 * Created by Akash Garg on 15/01/2019.
 */


public class GenericTextWatcherUtil implements TextWatcher {
    private View view;
    private InputFilter EMOJI_FILTER = (source, start, end, dest, dstart, dend) -> {
        for (int index = start; index < end; index++) {
            int type = Character.getType(source.charAt(index));
            if (type == Character.SURROGATE) {
                return "";
            }
        }
        return null;
    };

    public GenericTextWatcherUtil(View view) {
        this.view = view;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        switch (view.getId()) {
            case R.id.et_firstName:
                removeSpace((EditText) view);
                break;
            case R.id.et_address1:
                removeSpace((EditText) view);
                break;
            case R.id.et_address2:
                removeSpace((EditText) view);
                break;
            default:
                break;
        }
    }

    private void removeSpace(EditText etText) {
        try {
            String text = etText.getText().toString();
            if (!text.isEmpty()) {
                if (text.startsWith(" ")) {
                    etText.setText(text.trim());
                    etText.setSelection(etText.getText().length());
                    return;
                }
                if (text.contains("  ")) {
                    String trimS = text.replace("  ", " ");
                    etText.setText(trimS);
                    etText.setSelection(etText.getText().length());
                }
            }
        } catch (Exception ignored) {
        }

        if (null != EMOJI_FILTER && null != etText)
            etText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(80), EMOJI_FILTER});
    }
}
