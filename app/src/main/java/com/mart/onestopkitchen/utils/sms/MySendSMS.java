package com.mart.onestopkitchen.utils.sms;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;

import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.utils.AppUtils;


/**
 * Created by kapil on 05/10/15.
 */
public class MySendSMS {
    private static int flag = 0;
    boolean flags = false;
    private Context context;
    private BroadcastReceiver sentReceiver = null, receiveReceiver = null;
    private SmsValidateSms mySendSMSStatus;

    public MySendSMS(Context context, SmsValidateSms mySendSMSStatus) {
        this.mySendSMSStatus = mySendSMSStatus;
        this.context = context;
    }

    @SuppressLint("NewApi")
    public void sendSMS(String mobileNo, String msg) {

        final String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

        sentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        flag = 0;
                        try {
                            AppUtils.showCustomToastMsg(context, "", context.getResources().getString(R.string.sms_sent), true);
                            context.unregisterReceiver(sentReceiver);
                        } catch (Exception e) {

                        }
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        flag = 0;
                        try {
                            mySendSMSStatus.onErrorSendingSms();
                            AppUtils.showCustomToastMsg(context, "", context.getResources().getString(R.string.generic_faluire), true);
                            context.unregisterReceiver(sentReceiver);
                        } catch (Exception e) {

                        }
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        flag = 0;
                        try {
                            AppUtils.showCustomToastMsg(context, "", context.getResources().getString(R.string.no_service_msg), true);
                            mySendSMSStatus.onErrorSendingSms();
                            context.unregisterReceiver(sentReceiver);
                        } catch (Exception e) {

                        }
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        flag = 0;
                        try {
                            AppUtils.showCustomToastMsg(context, "", context.getResources().getString(R.string.sms_null), false);
                            mySendSMSStatus.onErrorSendingSms();
                            context.unregisterReceiver(sentReceiver);
                        } catch (Exception e) {

                        }
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        flag = 0;
                        try {
                            AppUtils.showCustomToastMsg(context, "", context.getResources().getString(R.string.sms_radio_off), true);
                            mySendSMSStatus.onErrorSendingSms();
                            context.unregisterReceiver(sentReceiver);
                        } catch (Exception e) {

                        }
                        break;
                }
            }
        };


        receiveReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {

                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        flag = 0;
                        try {
                            // Utils.showCustomToastMsg(context, R.string.sms_delivered, true);
                            context.unregisterReceiver(receiveReceiver);
                        } catch (Exception e) {

                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        flag = 0;
                        try {
                            AppUtils.showCustomToastMsg(context, "", context.getResources().getString(R.string.sms_not_delivered), false);
                            mySendSMSStatus.onErrorSendingSms();
                            context.unregisterReceiver(receiveReceiver);
                        } catch (Exception e) {

                        }
                        break;
                }
            }
        };

        if (flags) {
            context.unregisterReceiver(sentReceiver);
            context.unregisterReceiver(receiveReceiver);
            flags = false;
        }

        if (!flags) {
            context.registerReceiver(sentReceiver, new IntentFilter(SENT));
            context.registerReceiver(receiveReceiver, new IntentFilter(DELIVERED));
            flags = true;
        }

        SmsManager sms = SmsManager.getDefault();
        String mobileNoFORSMS = mobileNo;
        if (mobileNoFORSMS.startsWith("00"))
            mobileNoFORSMS = "+" + mobileNoFORSMS.substring(2);
        sms.sendTextMessage(mobileNoFORSMS.replace(" ", ""), null, msg, sentPI, deliveredPI);
        if (flag > 1) {
            try {
                //  AppUtils.showAlert(context, context.getString(R.string.sms_permission_error), 0);
            } catch (Exception e) {
                // WindowManager$BadTokenException will be caught and the app would not display
                // the 'Force Close' message.tr
            }

        }

    }


    public void UnregisterBroadcast() {
        if (context != null && sentReceiver != null
                && receiveReceiver != null) {
            if (flags) {
                try {
                    context.unregisterReceiver(sentReceiver);
                } catch (IllegalArgumentException e) {
                    Log.e(getClass().getSimpleName(), e.getMessage());
                }

                try {
                    context.unregisterReceiver(receiveReceiver);
                } catch (IllegalArgumentException e) {
                    Log.e(getClass().getSimpleName(), e.getMessage());
                }

                flags = false;
            }
        }

    }
}
