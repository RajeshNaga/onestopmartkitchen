package com.mart.onestopkitchen.utils;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;

public class CustomEdittext extends EditText {
    private Drawable dRight;
    private Rect rBounds;
    private Context context;

    public CustomEdittext(Context context) {
        super(context);
        this.context = context;
        setEditTextFilter();
    }


    public CustomEdittext(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // setTypeface(AppUtils.getFont(context));
        this.context = context;
        setEditTextFilter();
    }

    public CustomEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setTypeface(Utils.getFont(context));
        this.context = context;
        setEditTextFilter();
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top,
                                     Drawable right, Drawable bottom) {
        if (right != null) {
            dRight = right;
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        setInsertionDisabled();
        if (event.getAction() == MotionEvent.ACTION_UP && dRight != null) {
            rBounds = dRight.getBounds();
            final int x = (int) event.getX();
            final int y = (int) event.getY();
            if (x >= (this.getRight() - rBounds.width()) && x <= (this.getRight() - this.getPaddingRight())
                    && y >= this.getPaddingTop() && y <= (this.getHeight() - this.getPaddingBottom())) {
                //  RightDrawableListener right = (RightDrawableListener) context;
                //  right.rightDrwableListener();
                event.setAction(MotionEvent.ACTION_CANCEL);//use this to prevent the keyboard from coming up
            }
        }
        try {
            return super.onTouchEvent(event);
        } catch (Exception e) {
            return true;
        }
    }

    private void setInsertionDisabled() {
        try {
            Field editorField = TextView.class.getDeclaredField("mEditor");
            editorField.setAccessible(true);
            Object editorObject = editorField.get(this);

            Class editorClass = Class.forName("android.widget.Editor");
            Field mInsertionControllerEnabledField = editorClass.getDeclaredField("mInsertionControllerEnabled");
            mInsertionControllerEnabledField.setAccessible(true);
            mInsertionControllerEnabledField.set(editorObject, false);
        } catch (Exception ignored) {
            // ignore exception here
        }
    }

    @Override
    protected void finalize() throws Throwable {
        dRight = null;
        rBounds = null;
        super.finalize();
    }

    public void setEditTextFilter() {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence src, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String character = String.valueOf(src.charAt(i));
                    if (character.equalsIgnoreCase("%") || character.equalsIgnoreCase("/") || character.equalsIgnoreCase("#") || character.equalsIgnoreCase("\\") || character.equalsIgnoreCase(";") || character.equalsIgnoreCase("<") || character.equalsIgnoreCase(">") || character.equalsIgnoreCase("?")) {
                        return "";
                    }
                }

                if (src.equals("")) { // for backspace
                    return src;
                }
                if (src.toString().matches("[\\x00-\\x7F]+")) {
                    return src;
                }
                return "";
            }
        };
        setFilters(new InputFilter[]{filter});

    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        try {
            setSelection(this.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }

}


