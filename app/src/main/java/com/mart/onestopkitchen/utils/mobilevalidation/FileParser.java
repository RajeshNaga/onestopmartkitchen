package com.mart.onestopkitchen.utils.mobilevalidation;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.internal.$Gson$Types;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 */

public class FileParser {

    private final Context mContext;
    private Type mType;

    public FileParser(Context mContext) {
        this.mContext = mContext;
    }

    public <T> T parseFile(String fileName, Type tTypeOf) {
        mType = tTypeOf;
        if (fileName == null || fileName.isEmpty()) {
            return null;
        }
        String data = readFile(fileName);

        return convertSingle(data);
    }

    public <T> ArrayList<T> parseArray(String fileName, Type tTypeOf) {
        mType = tTypeOf;
        if (fileName == null || fileName.isEmpty()) {
            return null;
        }
        return convertCollection(fileName);
    }

    private <T> T convertSingle(String data) {
        if (null == data)
            return null;
        return (T) new Gson().fromJson(data, mType);
    }

    private <T> ArrayList<T> convertCollection(String data) {
        if (null == data)
            return null;
        return (ArrayList<T>) new Gson().fromJson(data, getTokenType());
    }

    public <T> ArrayList<T> parseFileCollection(String fileName, Type tTypeOf) {
        mType = tTypeOf;
        if (fileName == null || fileName.isEmpty()) {
            return null;
        }
        String data = readFile(fileName);
        return convertCollection(data);
    }

    private String readFile(String fileName) {
        return readFileFromAsset(fileName);
    }

    public Type getTokenType() {
        return $Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, mType);
    }

    public Object parseKeyData(String jsonData, String dataKey, Type tTypeOf) {
        mType = tTypeOf;
        try {
            JSONObject jsonRes = new JSONObject(readFile(jsonData));
            if (jsonRes.get(dataKey) instanceof JSONArray) {
                return convertCollection(jsonRes.getJSONArray(dataKey).toString());
            } else if (jsonRes.get(dataKey) instanceof JSONObject) {
                return convertSingle(jsonRes.get(dataKey).toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String readFileFromAsset(String fileName) {
        AssetManager assetManager = mContext.getAssets();
        try {
            return streamToString(assetManager.open(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private String streamToString(InputStream is) {
        InputStreamReader isr = new InputStreamReader(is);
        StringBuilder br = new StringBuilder();
        try {
            char buf[] = new char[10000];
            int len;
            while ((len = isr.read(buf)) != -1) {
                br.append(buf, 0, len);
            }
        } catch (Exception ee) {
            ee.printStackTrace();

        } finally {
            try {
                is.close();
                isr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return br.toString();
    }
}