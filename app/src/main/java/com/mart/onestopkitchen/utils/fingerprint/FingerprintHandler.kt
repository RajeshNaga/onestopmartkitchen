@file:Suppress("DEPRECATION")

package com.mart.onestopkitchen.utils.fingerprint

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat


@Suppress("DEPRECATION")
@RequiresApi(Build.VERSION_CODES.M)
class FingerprintHandler(var mContext: Context, var listener: FingerprintResult) : FingerprintManager.AuthenticationCallback() {

    private var cancellationSignal: CancellationSignal? = null

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
        super.onAuthenticationError(errorCode, errString)
        listener.authenticationError()
        onAuthenticationFailed()
    }

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult?) {
        super.onAuthenticationSucceeded(result)
        listener.authenticationSuccess()
    }

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
        super.onAuthenticationHelp(helpCode, helpString)
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
        listener.authenticationFailure()
    }

    fun startAuth(fingerprintManager: FingerprintManager, cryptoObject: FingerprintManager.CryptoObject) {
        cancellationSignal = CancellationSignal()
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null)
    }
}
