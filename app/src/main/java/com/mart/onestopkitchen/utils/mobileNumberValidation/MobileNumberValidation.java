package com.mart.onestopkitchen.utils.mobileNumberValidation;

import android.app.Activity;

import com.okdollar.paymentgateway.utils.FileParser;

/**
 * This common class is used for mobile number validation
 * Validation Digits will loaded from server
 * <p>
 */
public class MobileNumberValidation {

    private static MobileNumberValidation mobileNumberValidation;

    private NumberSeriesModel mNumberSeries = null;

    private NumberModel mSeriesModel = new NumberModel();

    private boolean overseasCountry = false;

    public static MobileNumberValidation getInstance(Activity activity, String countryCode) {
        if (mobileNumberValidation == null)
            mobileNumberValidation = new MobileNumberValidation();

        mobileNumberValidation.mNumberSeries = (NumberSeriesModel) new FileParser(activity).parseFile("number_series.json", NumberSeriesModel.class);
        mobileNumberValidation.setCountry(countryCode);
        return mobileNumberValidation;
    }

    /**
     * This method is used to set the country Code in Validation
     *
     * @param countryCode Country Code
     */
    public void setCountry(String countryCode) {
        countryCode = countryCode.replace("+", "");
        if (!countryCode.equals("95")) {
            overseasCountry = true;
        }
        if (mSeriesModel.getCountryCode().contains(countryCode))
            return;
        if (mNumberSeries.getValidations() != null) {
            for (NumberModel mNumber : mNumberSeries.getValidations()) {
                if (mNumber.getCountryCode().contains(countryCode)) {
                    mSeriesModel = mNumber;
                    break;
                } else
                    mSeriesModel = new NumberModel(countryCode);
            }
        }

    }

    /**
     * This method is used to validate the mobile number for both local and overseas number
     *
     * @param validateOtherCountry Boolean Key to validate the overseas number or not
     * @param countryCode          Country code of the mobile number
     * @param mobileNumber         Mobile Number for validation
     * @param callback             callback for MobileValidationListener
     */
    public void validateMobileNumber(Boolean validateOtherCountry, String countryCode, String mobileNumber, MobileValidationListener callback) {

        int minLength = 0;
        int maxLength = 0;
        String operator = "";
        String operatorColor = "";
        boolean validNumber = true;
        boolean isFound = false;

        if (mobileNumber.isEmpty() || callback == null) {
            return;
        }

        if (overseasCountry && validateOtherCountry) {
            callback.onSelectedOtherCountry(true);
            return;
        }
        setCountry(countryCode);


        for (OperatorsModel item : mSeriesModel.getOperators()) {
            for (String series : item.getNumberSeries()) {
                if (mobileNumber.startsWith(series)) {
                    if (!item.getIsActive()) {
                        validNumber = false;
                    } else {
                        validNumber = true;
                        minLength = item.getMinLength();
                        maxLength = item.getMaxLenth();
                        operator = item.getOperatorName();
                        operatorColor = item.getOperatorColor();

                    }
                    isFound = true;
                    break;
                }
            }
            if (isFound)
                break;
        }

        if (!isFound) {
            validNumber = true;
            if (overseasCountry) {
                minLength = 4;
                maxLength = 13;
            } else {
                minLength = 11;
                maxLength = 11;
                operator = "Mpt";
                operatorColor = "#013d84";
            }
        }
        callback.onSetValidationLimit(validNumber, minLength, maxLength, operator, operatorColor);
    }

    /**
     * This listener is used to pass the validation response whether success or failure.
     */
    public interface MobileValidationListener {

        /**
         * This method is used to pass the validation data
         *
         * @param validNumber   Whether Mobile Number is correct or not
         * @param minLength     Minimum Digits of the validation
         * @param maxLength     Maximum Digits of the validation
         * @param operator      Operator of the Number
         * @param operatorColor Operator Color
         */
        void onSetValidationLimit(boolean validNumber, int minLength, int maxLength, String operator, String operatorColor);

        /**
         * This is method is used when other country validation is true. To Notify Users Selected Other Country
         *
         * @param error returns boolean whether error or not.
         */
        void onSelectedOtherCountry(boolean error);
    }
}