package com.mart.onestopkitchen.utils.bluetooth

class BluetoothModel(
        var Minor: String = "",
        var Distance: String = "",
        var CurrentRssi: String = "",
        var IBeacon: String = "",
        var Accuracy: String = "",
        var FirstRssi: String = "",
        var Uuid: String = "",
        var Mac: String = "",
        var FirstTimestamp: String = "",
        var TxPower: String = "",
        var Major: String = "",
        var CurrentTimestamp: String = "",
        var Name: String = "",
        var AdRecord: String = ""
)