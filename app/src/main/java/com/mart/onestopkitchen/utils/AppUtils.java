package com.mart.onestopkitchen.utils;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.google.gson.Gson;
import com.okdollar.paymentgateway.utils.simUtils.TelephonyInfo;
import com.mart.onestopkitchen.R;
import com.mart.onestopkitchen.application.MyApplication;
import com.mart.onestopkitchen.model.okdollar.OKDollarLoginInfo;
import com.mart.onestopkitchen.model.simUtils.SimModels;
import com.mart.onestopkitchen.networking.NetworkUtil;
import com.mart.onestopkitchen.service.PreferenceService;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import static android.content.Context.TELEPHONY_SERVICE;
import static android.content.Context.TELEPHONY_SUBSCRIPTION_SERVICE;

/**
 * Created by mart-110 on 12/18/2015.
 */
public class AppUtils {
    public static String TAG = AppUtils.class.getSimpleName();
    public static InputFilter removeSpecialCharacter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            String blockCharacterSet = "~;[]{}:#^|$%&*!?+";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };
    public static InputFilter removeEmoji = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {
                int type = Character.getType(source.charAt(index));
                if (type == Character.SURROGATE) {
                    return "";
                }
            }
            return null;
        }
    };
    private static int prevLenght = 0;
    private static float screenFontSize = 5.0f;

    public static String getNullSafeString(String string) {
        return string == null ? "" : string;
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isReallyConnectedToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) MyApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    @SuppressLint("InflateParams")
    public static void showCustomToastMsg(Context mContext, String title, String msg, Boolean status) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout = inflater.inflate(R.layout.toast_layout_app, null);
        TextView content = layout.findViewById(R.id.alertMsg);
        TextView appTitle = layout.findViewById(R.id.appTitle);
        content.setText(msg);
        if (!cutNull(title).isEmpty()) {
            appTitle.setVisibility(View.VISIBLE);
            appTitle.setText(title);
        } else appTitle.setVisibility(View.GONE);
        Toast customtoast = new Toast(mContext);
        customtoast.setView(layout);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();
    }

    public static String randomGeneratorNumber() {
        long min = 10000L;
        long max = 999999999999999L;

        int min2 = 5;
        int max2 = 16;
        /*Random r = new Random();
        long number = x+((long)(r.nextDouble()*(y-x)));
        return number;*/
        String msg = min + (long) (Math.random() * (max - min)) + "";
        int random = min2 + (int) (Math.random() * (max2 - min2));
        if (msg.length() < random)
            return msg;
        else
            return msg.substring(0, random);

    }

    public static boolean chekcPermission(String[] permissions, Activity activity) {
        boolean flag = false;
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
                flag = true;
        }
        return !flag;
    }

    public static boolean shouldShowPermissionRationale(String[] permissions, Activity activity) {
        for (String permission : permissions) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return true;
        }
        return false;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String cutNull(Object format) {
        return format == null ? "" : format.toString().trim().equalsIgnoreCase("") ? "" :
                format.toString().trim().equalsIgnoreCase("null") ? "" : format.toString();
    }

    public static void goToSettings(Activity context) {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + context.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        context.startActivityForResult(myAppSettings, 111);
    }

    public static CharSequence getMyanmarNumber() {
        SpannableString ss1 = new SpannableString("0");
        ss1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ss1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannableString ss2 = new SpannableString("9");
        ss2.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ss2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return TextUtils.concat(ss1, ss2);
    }

    public static boolean isValidForAutoFire(int lenght, String countryCode, String operatorName) {
        if (lenght == 10 && countryCode.equals("+95") && prevLenght < lenght) {
            prevLenght = lenght;
            return true;
        } else if (lenght == 7 && countryCode.equals("+95") && prevLenght < lenght) {
            prevLenght = lenght;
            return true;
        } else if (lenght == 8 && countryCode.equals("+95") && prevLenght < lenght) {
            prevLenght = lenght;
            return true;
        } else if (lenght == 9 && countryCode.equals("+95") && prevLenght < lenght) {
            prevLenght = lenght;
            return true;
        } else if (lenght == 9 && countryCode.equals("+95") && prevLenght < lenght && operatorName.contains("CDMA")) {
            prevLenght = lenght;
            return true;
        } else if (lenght == 8 && countryCode.equals("+65") && prevLenght < lenght) {        //singapore
            prevLenght = lenght;
            return true;
        } else if (lenght == 9 && countryCode.equals("+66") && prevLenght < lenght) {        //Thailand
            prevLenght = lenght;
            return true;
        } else if (lenght == 9 && countryCode.equals("+60") && prevLenght < lenght) {        // Malaysia
            prevLenght = lenght;
            return true;
        } else if (lenght == 10 && countryCode.equals("+91") && prevLenght < lenght) {        //India
            prevLenght = lenght;
            return true;
        } else if (lenght == 11 && countryCode.equals("+86") && prevLenght < lenght) {        //China
            prevLenght = lenght;
            return true;
        } else if (lenght == 10 && countryCode.equals("+1") && prevLenght < lenght) {        //USA
            prevLenght = lenght;
            return true;
        }
        prevLenght = lenght;
        return false;
    }

    public static String getSimOperater(Context mContext) {
        try {
            TelephonyManager telMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return telMgr.getSimOperator();
        } catch (Exception e) {
            return "";
        }
    }

    public static String readTextFileFromAssets(Context context, String filePath) throws IOException {
        AssetManager assetManager = context.getAssets();
        InputStream is;
        is = assetManager.open(filePath);
        return streamToString(is);
    }

    private static String streamToString(InputStream is) {
        InputStreamReader isr = new InputStreamReader(is);// this class is used

        StringBuilder br = new StringBuilder();// modifing sequece of charater
        // for build the string
        try {
            char buf[] = new char[10000];
            int len = 0;
            while ((len = isr.read(buf)) != -1)// read byte data utill reach the
            // end location
            {
                br.append(buf, 0, len);// add byte data and converted into
                // string
            }
        } catch (Exception ee) {
            ee.printStackTrace();

        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return br.toString();

    }

    public static void appLog(String tag, String s) {
        Log.d(cutNull(tag), cutNull(s));
    }

    //check permission
    public static String getDefaultEmail() {

        StringBuilder emails = new StringBuilder();
        AccountManager am = AccountManager.get(MyApplication.getAppContext());
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            String acname = ac.name;
            String actype = ac.type;
            if (isValidEmail(acname)) {
                emails.append(acname).append(",");
            }
        }
        return emails.toString();
    }
//
//    @SuppressLint({"MissingPermission"})
//    public static String getSimId() {
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
//                SubscriptionManager sm = SubscriptionManager.from(MyApplication.getAppContext());
//
//                List<SubscriptionInfo> sis = sm.getActiveSubscriptionInfoList();
//                String val = "";
//                // getting first SubscriptionInfo
//                if (sis != null) {
//                    int simSlot = PreferenceService.getInstance().GetPreferenceIntValue(PreferenceService.SLOT_NAME);
//                    SubscriptionInfo si = sis.get(0);
//                    // SubscriptionInfo si2 = sis.get(1);
//                    // getting iccId
//                    return si.getIccId();
//                }
//            } else {
//                TelephonyManager mTelephonyMgr = (TelephonyManager) MyApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
//                if (mTelephonyMgr != null) {
//                    return mTelephonyMgr.getSimSerialNumber() == null ? "123456789" : mTelephonyMgr.getSimSerialNumber();
//                }
//            }
//        } catch (Exception id) {
//            id.printStackTrace();
//        }
//        return "123456789";
//    }

    public static String getDefaultFacebook() {
        String s = "";
        AccountManager am = AccountManager.get(MyApplication.getAppContext());
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            String acname = ac.name;
            String actype = ac.type;
            if (actype.equals("com.facebook.auth.pg_login")) {
                return s;
            }
        }
        return s;
    }

    public static ArrayList<SimModels> getAllSimInfo(Context context) {
        ArrayList<SimModels> simSlots = new ArrayList<>();
        try {
            SimModels simModels = new SimModels();
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                    Method getSubId = TelephonyManager.class.getMethod("getSubscriberId", int.class);
                    SubscriptionManager subscriptionManager = (SubscriptionManager) context.getSystemService(TELEPHONY_SUBSCRIPTION_SERVICE);
                    if (subscriptionManager != null && subscriptionManager.getActiveSubscriptionInfoCount() == 1) {
                        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(context);
                        if (telephonyInfo.isSIM1Ready()) {
                            simModels.setSimId(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getIccId());
                            simModels.setMsids((String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getSubscriptionId()));
                            simModels.setOperator(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getCarrierName().toString());

//                            System.out.println("setSimId 11::"+subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getIccId());
//                            System.out.println("setMsids 11::"+(String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getSubscriptionId()));
//                            System.out.println("setOperator 11::"+subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getCarrierName().toString());

                        }
                        if (telephonyInfo.isSIM2Ready()) {
                            simModels.setSimId(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getIccId());
                            simModels.setMsids((String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getSubscriptionId()));
                            simModels.setOperator(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getCarrierName().toString());

//                            System.out.println("setSimId 12::"+subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getIccId());
//                            System.out.println("setMsids 12::"+(String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getSubscriptionId()));
//                            System.out.println("setOperator 12::"+subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getCarrierName().toString());

                        }
                        simSlots.add(simModels);
                    } else if (subscriptionManager != null && subscriptionManager.getActiveSubscriptionInfoCount() >= 2) {
                        simModels.setSimId(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getIccId());
                        simModels.setMsids((String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getSubscriptionId()));
                        simModels.setOperator(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getCarrierName().toString());
                        simSlots.add(simModels);

//                        System.out.println("setSimId 21::"+(String) subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getIccId());
//                        System.out.println("setMsids 21::"+(String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getSubscriptionId()));
//                        System.out.println("setOperator 21::"+subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(0).getCarrierName().toString());

                        simModels = new SimModels();
                        simModels.setSimId(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getIccId());
                        simModels.setMsids((String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getSubscriptionId()));
                        simModels.setOperator(subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getCarrierName().toString());
                        simSlots.add(simModels);

//                        System.out.println("setSimId 22::"+(String) subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getIccId());
//                        System.out.println("setMsids 22::"+(String) getSubId.invoke(telephonyManager, subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getSubscriptionId()));
//                        System.out.println("setOperator 22::"+subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(1).getCarrierName().toString());

                    }
                } else {
                    simModels.setSimId(getSimID(context));
                    simModels.setMsids(getMsId(context));
                    simModels.setOperator("Unknown");
                    simSlots.add(simModels);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            simSlots = removeNullData(simSlots);
            return simSlots;
        }
    }

    public static boolean verifyPermissions(@NonNull int[] grantResults) {
        if (grantResults.length < 1) {
            return false;
        }
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            return "";
        }
    }

    @SuppressLint("WifiManagerLeak")
    public static String getMacID() {
        WifiManager manager = (WifiManager) MyApplication.getAppContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = null;
        if (manager != null) {
            info = manager.getConnectionInfo();
        }
        return info.getMacAddress();
    }

    @SuppressLint({"MissingPermission"})
    public static String getDualSimId(Context context, int slot) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            return getSimid(context, slot);
        } else {
            TelephonyManager mTelephonyMgr = (TelephonyManager) MyApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephonyMgr != null) {
                return mTelephonyMgr.getSimSerialNumber() == null ? "" : mTelephonyMgr.getSimSerialNumber();
            }
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    private static String getSimid(Context context, int slot) {
        SubscriptionManager sm = SubscriptionManager.from(context);
        @SuppressLint("MissingPermission")
        List<SubscriptionInfo> sis = sm.getActiveSubscriptionInfoList();
        if (sis != null) {
            if (sis.size() == 1) {
                SubscriptionInfo si = sis.get(0);
                return slot == 0 ? si.getIccId() : "";
            }
            if (sis.size() == 2) {
                if (slot == 0)
                    return sis.get(0).getIccId();
                if (slot == 1)
                    return sis.get(1).getIccId();

            }
        }
        return "";
    }

    public static int getSimState(Context mContext) {
        int isSim = 0;
        TelephonyManager telMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        ///  if (ActivityCompat.checkSelfPermission(MyApplication.getAppContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
        ///      return 0;
        //   }
        int state = telMgr.getSimState();
        switch (state) {
            case TelephonyManager.SIM_STATE_ABSENT:
                isSim = 0;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                break;
            case TelephonyManager.SIM_STATE_READY:
                isSim = 1;
                // do something
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                break;
        }
        return isSim;
    }

    public static void setScreenSize(Activity mActivity) {
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenFontSize = metrics.scaledDensity;
        } catch (Exception ignored) {
        }
    }

    public static Spannable getMMKString(float size, String value, int color) {
        if (value == null)
            return new SpannableStringBuilder();
        SpannableStringBuilder text = new SpannableStringBuilder(value);
//        value = value.toLowerCase().replace("(mmk)", "").replace(",", "").replace(" ","").trim();
        value = value.replaceAll("[a-zA-Z(),]", "");//.replace(",", "");
//        value = value.toLowerCase().replace("k", "").replace(",", "");
        try {
            Integer.parseInt(value.trim());
        } catch (Exception e) {
            return text;
        }
        String val = convertNumberStringThousand(value.trim());
        val = val + " MMK";
        text = new SpannableStringBuilder(val);
        float sp = size / screenFontSize;
        int newTextSize = (int) (sp / 2);
        if (newTextSize < 8)
            newTextSize = 8;
        text.setSpan(new AbsoluteSizeSpan(newTextSize, true), val.length() - 3, val.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new StyleSpan(Typeface.NORMAL), val.length() - 3, val.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannableStringBuilder colo = new SpannableStringBuilder(text);
        if (color != 0)
            colo.setSpan(new ForegroundColorSpan(Color.RED), 0, val.length() - 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return text;
    }

    private static String convertNumberStringThousand(String val) {
        int longval;
        //  val = conversion(Double.parseDouble(val));//
        if (!val.equals("")) {
            if (val.contains(",")) {
                val = val.replaceAll(",", "");
            }

            longval = Integer.parseInt(val);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,###,###,###");
            if (val.contains(".")) {
                return formatter.format(longval) + val.substring(val.lastIndexOf("."), val.length());
            }
            return formatter.format(longval);
        }
        return val;
    }

    public static String conversion(Double rate) {
        NumberFormat formatter = NumberFormat.getInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) formatter;
        df.applyPattern("#0.00");
        return df.format(rate);
    }

    public static String getViberNumber(Context context) {
        AccountManager am = AccountManager.get(context);
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            String acname = ac.name;
            String actype = ac.type;
            if (actype.equals("com.viber.voip")) {
                if (acname.startsWith("+95"))
                    return acname;
            }
        }
        return getMyPhoneNumber(context);
    }

    @SuppressLint("HardwareIds")
    private static String getMyPhoneNumber(Context context) {
        try {
            TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
            if (telMgr.getLine1Number() != null && telMgr.getLine1Number().startsWith("+95"))
                return telMgr.getLine1Number();
            else
                return "";
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean mptNumber7digitsValidation(String number) {
        if (number.startsWith("0969")) {
            return false;
        }
        return number.trim().startsWith("0920") ||
                number.trim().startsWith("0921") ||
                number.trim().startsWith("0922") ||
                number.trim().startsWith("0923") ||
                number.trim().startsWith("0924") ||
                number.trim().startsWith("0964") ||
                number.trim().startsWith("0966") ||
                number.trim().startsWith("0967") ||
                number.trim().startsWith("0968") ||
                number.trim().startsWith("095");

    }

    public static boolean mptNumber8digitsValidation(String number) {
        // inclues 09 - 9 digits
        return number.trim().startsWith("0941") ||
                number.trim().startsWith("0943") ||
                number.trim().startsWith("0947") ||
                number.trim().startsWith("0949") ||
                number.trim().startsWith("0973") ||
                number.trim().startsWith("0991");

    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static Date getCurrentDateAndTime() {
        return DateUtils.getDate("", DateUtils.getInputType(2), DateUtils.getOutputType(9));//.LocalDateParser.DATE.LOCALE_CURRENT, LocalDateParser.DATE.OUTPUT_YYYY_MM_DD_HHMMSS);
    }

    public static int compareDates(Date today, Date myDate) {
        return today.compareTo(myDate);
    }

    public static Date AddDays(int days) {
        return new DateTime().plusDays(days).toDate();
    }

    public static Date AddMinutes(int minutes) {
        return new DateTime().plusMinutes(minutes).toDate();
    }

    public static List<String> getDefaultEmails() {

        List<String> getDefaultEmails = new ArrayList<>(0);
        AccountManager am = AccountManager.get(MyApplication.getAppContext());
        Account[] accounts = am.getAccounts();
        for (Account ac : accounts) {
            String acname = ac.name;
            String actype = ac.type;
            if (isValidEmail(acname)) {
                getDefaultEmails.add(acname);//= emails + acname + ",";

            }
        }
        return getDefaultEmails;
    }

//    public static boolean checkSimChange() {
//        return !AppUtils.cutNull(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_SIM_ID)).isEmpty() && AppUtils.cutNull(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.USER_SIM_ID)).equalsIgnoreCase(getSimId());
//    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        // }
        return false;
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @SuppressLint("SimpleDateFormat")
    public static Date stringToDateFormat(String dateStr, String pattern) {
        Date date1 = null;
        try {
            date1 = new SimpleDateFormat(pattern).parse(dateStr);
        } catch (Exception ignored) {
        }
        return date1;
    }

    public static void rxErrorHandling(Context context) {
        if (!AppUtils.isReallyConnectedToInternet())
            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, context.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
    }

    public static Calendar fromUserSelectDate() {
        Calendar fromDate = Calendar.getInstance();
        fromDate.set(Calendar.DAY_OF_MONTH, fromDate.get(Calendar.DAY_OF_MONTH));
        fromDate.set(Calendar.MONTH, fromDate.get(Calendar.MONTH));
        fromDate.set(Calendar.YEAR, fromDate.get(Calendar.YEAR));
        fromDate.set(Calendar.SECOND, 0);
        fromDate.set(Calendar.MINUTE, 0);
        fromDate.set(Calendar.HOUR_OF_DAY, 0);
        return fromDate;
    }

    public static String capitalizeWords(String text) {   // zay ok --> Zay Ok
        if (!text.isEmpty()) {
            StringBuffer capBuffer = new StringBuffer();
            Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(text);
            while (capMatcher.find()) {
                capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
            }
            return capMatcher.appendTail(capBuffer).toString();
        }
        return "";
    }

    public static void editTextSpaceRemove(EditText text) {
        String string = text.getText().toString();
        if (string.length() > 0)
            if (string.startsWith(" ")) {
                text.setText("");
            } else if (string.contains("  ")) {
                text.setText(string.replace("  ", " "));
                text.setSelection(text.getText().toString().length());
            }
    }

    public static void changeFont(Activity activity) {
        if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.MYANMAR)) {
            activity.setTheme(R.style.EnglishFont);
        } else {
            activity.setTheme(R.style.BurmeseFont);
        }
    }

    public static String getMsId(Context mContext) {
        String subscriberId = "123456789";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                if (telephonyManager != null && !TextUtils.isEmpty(telephonyManager.getSubscriberId()))
                    subscriberId = telephonyManager.getSubscriberId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return subscriberId;
        }
    }

    public static String getSimID(Context mContext) {
        String subscriberId = "123456789";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                if (telephonyManager != null && !TextUtils.isEmpty(telephonyManager.getSimSerialNumber()))
                    subscriberId = telephonyManager.getSimSerialNumber();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return subscriberId;
        }
    }

    private static ArrayList<SimModels> removeNullData(ArrayList<SimModels> data) {
        ArrayList<SimModels> simModelsArrayList = new ArrayList<>();
        for (SimModels simModels : data) {
            //  System.out.println("simModels.getSimId() " + simModels.getSimId() + "simModels.getMsids() " + simModels.getMsids()  + "simModels.getOperator() " + simModels.getOperator());
            if (TextUtils.isEmpty(simModels.getOperator())) {
                simModels.setOperator("Unknown");
            }
            if (!TextUtils.isEmpty(simModels.getSimId()) && !TextUtils.isEmpty(simModels.getMsids()) && !TextUtils.isEmpty(simModels.getOperator())) {
                simModelsArrayList.add(simModels);
            }
        }
        return simModelsArrayList;
    }

    public static ArrayList<SimModels> removeNoServiceOperator(ArrayList<SimModels> data, String operatorName) {
        if (TextUtils.isEmpty(operatorName)) {
            operatorName = "Mpt";
        }
        System.out.println("Operator " + operatorName);
        ArrayList<SimModels> simModelsArrayList = new ArrayList<>();
        for (SimModels simModels : data) {
            if (simModels != null && !TextUtils.isEmpty(simModels.getSimId()) && !TextUtils.isEmpty(simModels.getMsids()) &&
                    !TextUtils.isEmpty(simModels.getOperator()) &&
                    !simModels.getOperator().equalsIgnoreCase("No service") &&
                    !simModels.getOperator().equalsIgnoreCase("Emergency calls only") &&
                    simModels.getOperator().toLowerCase().trim().contains(operatorName.toLowerCase().trim())) {
                simModelsArrayList.add(simModels);
            }
        }
        return simModelsArrayList;
    }

    public static void CreateNstToken() {
        String compactJws = "";
        try {
            if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.NST).isEmpty()) {
                compactJws = Jwts.builder()
                        .claim("NST_KEY", AppConstants.NST_KEY)
                        .signWith(SignatureAlgorithm.HS512, AppConstants.NST_SECRET.getBytes("UTF-8")).compact();
                PreferenceService.getInstance().SetPreferenceValue(PreferenceService.NST, compactJws);
                NetworkUtil.setNst(compactJws);
            } else {
                NetworkUtil.setNst(PreferenceService.getInstance().GetPreferenceValue(PreferenceService.NST));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //for fetch the formated number
    public static String formattedNumber(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.isEmpty())
            return "";

        else if (phoneNumber.startsWith("0095") && phoneNumber.startsWith("00"))
            return "+95" + phoneNumber.substring(4);

        else if (phoneNumber.startsWith("00")) {
            return "+" + phoneNumber.substring(2);
        }
        return "";
    }

    // when need for country code & mobile number
    public static String[] formattedNumberWithCountryCode(String phone) {
        String[] ary = new String[2];
        if (phone == null || phone.isEmpty()) {
            ary[0] = "+95";
            ary[1] = "09";
            return ary;
        } else if (phone.startsWith("0095") && phone.startsWith("00")) {
            ary[0] = "+95";
            ary[1] = "0" + phone.substring(4);
            return ary;
        } else {
            ary[0] = "+95";
            ary[1] = "09";
        }
        return ary;
    }

    public static String getTownShipName(Context context, String townShipId) {
        if (townShipId == null)
            return "";

        try {
            InputStream is = context.getAssets().open("township_list.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            try {
                JSONObject resp = new JSONObject(json);
                JSONArray cityList = resp.getJSONArray("TownShipList");
                Log.d("cityList", cityList.toString());
                for (int i = 0; i < cityList.length(); i++) {
                    JSONObject jsonObject = cityList.getJSONObject(i);
                    if (townShipId.equalsIgnoreCase(jsonObject.getString("TownshipCode"))) {
                        return jsonObject.getString("CityName");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("cityList", "JSONException" + e.getMessage());
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.d("cityList", "JSONException" + e.getMessage());
        }
        return townShipId;
    }

    public static String getStateName(Context context, String stateId) {
        if (stateId == null)
            return "";

        try {
            InputStream is = context.getAssets().open("city_list.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            try {
                JSONObject resp = new JSONObject(json);
                JSONArray cityList = resp.getJSONArray("CityList");
                for (int i = 0; i < cityList.length(); i++) {
                    JSONObject jsonObject = cityList.getJSONObject(i);
                    if (stateId.equalsIgnoreCase(jsonObject.getString("State Code"))) {
                        return jsonObject.getString("State Name");
                    }
                }
            } catch (JSONException e) {
                Log.d("cityList", "JSONException" + e.getMessage());
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.d("cityList", "JSONException" + e.getMessage());
        }
        return stateId;
    }

    public OKDollarLoginInfo validateOKAppLogin(Context context) {
        String URL = "content://com.jas.digitalkyats.provider/userdetails";
        Uri okDollar = Uri.parse(URL);
        //   Context context1 = context == null ? MyApplication.getAppContext() : context;
        OKDollarLoginInfo oKDollarLoginInfo = new OKDollarLoginInfo();
        Cursor cursor = null;
        try {
            if (okDollar != null) {

                cursor = context.getContentResolver().query(okDollar, null, null, null, null);
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    oKDollarLoginInfo.setRegisterdLang(cursor.getString(cursor.getColumnIndex("RegistedLanguage")));
                    oKDollarLoginInfo.setMobilNumber(cursor.getString(cursor.getColumnIndex("MobileNumber")));
                    oKDollarLoginInfo.setName(cursor.getString(cursor.getColumnIndex("Name")));
                    oKDollarLoginInfo.setProfilePicture(cursor.getString(cursor.getColumnIndex("ProfilePic")));
                    oKDollarLoginInfo.setDob(cursor.getString(cursor.getColumnIndex("DateOfBirth")));
                    oKDollarLoginInfo.setUserName(cursor.getString(cursor.getColumnIndex("BusinessName")));
                    oKDollarLoginInfo.setCountyCode(cursor.getString(cursor.getColumnIndex("CountryCode")));
                    oKDollarLoginInfo.setGender(cursor.getString(cursor.getColumnIndex("Gender")));
                    oKDollarLoginInfo.setEmail(cursor.getString(cursor.getColumnIndex("EmailID")));
                    oKDollarLoginInfo.setSimId(cursor.getString(cursor.getColumnIndex("SimID")));
                    oKDollarLoginInfo.setDeviceId(cursor.getString(cursor.getColumnIndex("IMEI")));
                    oKDollarLoginInfo.setAddress1(cursor.getString(cursor.getColumnIndex("Address1")));
                    oKDollarLoginInfo.setAddress2(cursor.getString(cursor.getColumnIndex("Address2")));
                    oKDollarLoginInfo.setCountry(cursor.getString(cursor.getColumnIndex("Country")));
                    oKDollarLoginInfo.setState(cursor.getString(cursor.getColumnIndex("State")));
                    oKDollarLoginInfo.setTownShip(cursor.getString(cursor.getColumnIndex("Township")));
                    oKDollarLoginInfo.setHouseBlockNo(cursor.getString(cursor.getColumnIndex("HouseBlockNo")));
                    oKDollarLoginInfo.setVillageName(cursor.getString(cursor.getColumnIndex("VillageName")));
                    oKDollarLoginInfo.setFloorNumber(cursor.getString(cursor.getColumnIndex("HouseFloorNo")));
                    oKDollarLoginInfo.setRoomNumber(cursor.getString(cursor.getColumnIndex("HouseRoomNo")));
                    oKDollarLoginInfo.setFBEmailId(cursor.getString(cursor.getColumnIndex("FBEmailId")));
                    System.out.println("oKDollarLoginInfo::::" + new Gson().toJson(oKDollarLoginInfo));
                    return oKDollarLoginInfo;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static String getStringByLocal(Context context, int resId, String locale) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            return getStringByLocalPlus17(context, resId, locale);
        else
            return getStringByLocalBefore17(context, resId, locale);
    }

    @NonNull
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private static String getStringByLocalPlus17(Context context, int resId, String locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(new Locale(locale));
        return context.createConfigurationContext(configuration).getResources().getString(resId);
    }

    private static String getStringByLocalBefore17(Context context, int resId, String language) {
        Resources currentResources = context.getResources();
        AssetManager assets = currentResources.getAssets();
        DisplayMetrics metrics = currentResources.getDisplayMetrics();
        Configuration config = new Configuration(currentResources.getConfiguration());
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        config.locale = locale;
        /*
         * Note: This (temporarily) changes the devices locale!
         * better way to get the string in the specific locale
         */
        Resources defaultLocaleResources = new Resources(assets, metrics, config);
        String string = defaultLocaleResources.getString(resId);
        // Restore device-specific locale
        new Resources(assets, metrics, currentResources.getConfiguration());
        return string;
    }

    public static Typeface getFonts(Context context) {
        if (PreferenceService.getInstance().GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.ENGLISH))
            return ResourcesCompat.getFont(context, R.font.centurygothic);
        return ResourcesCompat.getFont(context, R.font.zawgyione);
    }

    public static String fileName(Context context) {
        return Environment.getExternalStorageDirectory() + "/" + context.getString(R.string.app_name) + "/";
    }
}
