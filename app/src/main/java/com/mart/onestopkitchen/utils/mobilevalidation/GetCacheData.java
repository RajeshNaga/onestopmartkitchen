package com.mart.onestopkitchen.utils.mobilevalidation;


public class GetCacheData {

    public static Object cacheData(CACHEDKEY cachedkey) {
        switch (cachedkey) {
            case COUNTRYOPERATOR:
                return new LocalFileReader().getListOfNetworkOperatorModel();
            case NETWORKOPERATOR:
                return new LocalCountryParser().parseNetworkCountry();
        }
        return null;
    }
}
