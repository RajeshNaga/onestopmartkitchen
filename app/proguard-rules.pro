-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-ignorewarnings
-printmapping mapping.txt
-printusage usage.txt
-dontskipnonpubliclibraryclasses
#-optimizationpasses 3
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-keepattributes *Annotation*,EnclosingMethod,SourceFile,LineNumberTable,Exceptions,Deprecated
-dontwarn android.support.**
-dontwarn com.google.**
-keepattributes -keepattributes Exceptions
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-allowaccessmodification
-renamesourcefileattribute MyApplication
-repackageclasses ''


#ButterKnife Library--------------------------------------------------------------------------------
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }
-keep public class * extends android.support.v7.app.AppCompatActivity
-keepclassmembers class ** {
    public void onEvent*(**);
}

# Okio----------------------------------------------------------------------------------------------
-dontnote okio.**
-dontwarn okio.**
-dontwarn rx.**
-dontwarn com.squareup.okhttp.*
-dontwarn retrofit.appengine.UrlFetchClient

#GSON-----------------------------------------------------------------------------------------------
-dontwarn sun.misc.**
-keep class com.google.gson.stream.** { *; }


#ZAYOK-Classes -------------------------------------------------------------------------------------
-keep class com.mart.onestopkitchen.model.** { *; }
-keep public class com.mart.onestopkitchen.ui.customview.** {* ;}
-keep public class com.mart.onestopkitchen.networking.** { * ;}
-keep public class com.mart.onestopkitchen.ui.adapter.** { * ;}
-keep public class com.mart.onestopkitchen.networking.response.BillingAddressResponse.** { *; }
-keep public class com.mart.onestopkitchen.ui.fragment.CustomerOrderDetailsFragment.** { *; }

#-keep public class com.mart.onestopkitchentchen.ui.fragment.CartFragment.** { *; }
#-dontwarn com.mart.onestopkitchentchen.ui.fragment.CartFragment.**

-keep public class com.mart.onestopkitchen.ui.fragment.CustomerInfoFragment.** { *; }
-dontwarn com.mart.onestopkitchen.ui.fragment.CustomerInfoFragment.**
-keep public class com.mart.onestopkitchen.networking.BaseResponse.CheckoutOrderSummaryResponse.** { *; }

#SupportLibrary-------------------------------------------------------------------------------------
-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }


#DataBinding ---------------------------------------------------------------------------------------
-dontwarn android.databinding.**
-keep class android.databinding.** { *; }


#JODATIME ------------------------------------------------------------------------------------------
-keep class org.joda.time.** {
    <fields>;
    <methods>;
}


#FacebookShetho: Stetho ----------------------------------------------------------------------------
-keep class com.facebook.stetho.** {*;}
-dontwarn com.facebook.stetho.**


#OkDollarPaymentGateway ----------------------------------------------------------------------------
-keep public class com.okdollar.paymentgateway.**{* ; }


#Itext ---------------------------------------------------------------------------------------------
-keep class com.itextpdf.text.** { *; }
-dontwarn com.itextpdf.text.**


#Enum ----------------------------------------------------------------------------------------------
-keepclassmembers enum * { *; }


#RX-JAVA -------------------------------------------------------------------------------------------
-keep class io.reactivex.** { *; }
-dontwarn io.reactivex.**

-keep class android.widget.Space.** { *; }
-dontwarn android.widget.Space.**

-keep class org.simpleframework.xml.**{ *; }
-dontwarn org.simpleframework.xml.**

-keep class com.marshalchen.ultimaterecyclerview.**{ *; }
-dontwarn com.marshalchen.ultimaterecyclerview.**

-keep class com.squareup.picasso.**{ *; }
-dontwarn com.squareup.picasso.**

-keep class io.jsonwebtoken.**{ *; }
-dontwarn io.jsonwebtoken.**

-keep class com.malinskiy.superrecyclerview.**{ *; }
-dontwarn com.malinskiy.superrecyclerview.**

-keep class com.fasterxml.jackson.**{ *; }
-dontwarn com.fasterxml.jackson.**

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
    }


#Retrofit2 -----------------------------------------------------------------------------------------
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-dontwarn retrofit2.adapter.rxjava.CompletableHelper$**


-keepattributes EnclosingMethod
-keepclasseswithmembers class * {
    @retrofit2.* <methods>;
}

-keepclasseswithmembers interface * {
    @retrofit2.* <methods>;
}
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}


# Disable Android Logs------------------------------------------------------------------------------
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

-assumenosideeffects class java.io.PrintStream {
    public void println(...);
    public void print(...);
}

-keepattributes InnerClasses
-dontwarn org.simpleframework.xml.**
-dontwarn android.**
-dontnote android.**
-dontwarn com.google.android.gms.**


#Picasso Library -----------------------------------------------------------------------------------
-keep class com.squareup.picasso.**{ *; }
-dontwarn com.squareup.picasso.**


#2C2P PGW Library ----------------------------------------------------------------------------------
-dontwarn com.ccpp.my2c2psdk.**
-keep class com.ccpp.my2c2psdk.cores.** {*;}
-keep class com.ccpp.my2c2psdk.** {*;}
-dontwarn com.samsung.**
-dontwarn com.alipay.**


# okhttp3 ------------------------------------------------------------------------------------------
-keep class sun.misc.Unsafe { *; }
-keepattributes Annotation
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-keep class okhttp3.internal.**{ *; }
-dontwarn okhttp3.internal.**
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-keepclassmembers class * implements javax.net.ssl.SSLSocketFactory {
    private final javax.net.ssl.SSLSocketFactory delegate;
}
-dontwarn org.xmlpull.v1.**
-dontwarn com.squareup.**
-keep class com.squareup.** { *; }
-dontwarn com.squareup.okhttp.internal.huc.**
-keep class com.squareup.okhttp3.** { *; }
-dontwarn javax.annotation.**
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

#Security Spongycastle -----------------------------------------------------------------------------
-dontwarn org.spongycastle.x509.util.LDAPStoreHelper
-dontwarn org.spongycastle.jce.provider.X509LDAPCertStoreSpi
-keep public class org.spongycastle.** {*;}
-keep class java.security.** {*;}
-keep class javax.crypto.** {*;}

#--------------------------------- Proguard Rule Finished ------------------------------------------







##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
#-keepattributes *Annotation*

# Gson specific classes
#-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.quickblox.core.account.model.** { *; }

##---------------End: proguard configuration for Gson  ----------

##---------------Begin: proguard configuration for quickblox  ----------

#quickblox core module
-keep class com.quickblox.auth.parsers.** { *; }
-keep class com.quickblox.auth.model.** { *; }
-keep class com.quickblox.core.parser.** { *; }
-keep class com.quickblox.core.model.** { *; }
-keep class com.quickblox.core.server.** { *; }
-keep class com.quickblox.core.rest.** { *; }
-keep class com.quickblox.core.error.** { *; }
-keep class com.quickblox.core.Query { *; }

#quickblox users module
-keep class com.quickblox.users.parsers.** { *; }
-keep class com.quickblox.users.model.** { *; }

#quickblox messages module
-keep class com.quickblox.messages.parsers.** { *; }
-keep class com.quickblox.messages.model.** { *; }

#quickblox content module
-keep class com.quickblox.content.parsers.** { *; }
-keep class com.quickblox.content.model.** { *; }

#quickblox chat module
-keep class com.quickblox.chat.parser.** { *; }
-keep class com.quickblox.chat.model.** { *; }
-keep class org.jivesoftware.** { *; }
-keep class org.jxmpp.** { *; }
-dontwarn org.jivesoftware.smackx.**

#quickblox videochat-webrtc module
-keep class org.webrtc.** { *; }

##---------------End: proguard configuration for quickblox  ----------

# For Barcode Scanner
-keep class me.dm7.barcodescanner.** { *; }
-keep class net.sourceforge.zbar.** { *; }

#EventBus --------
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
   }
-keep enum org.greenrobot.eventbus.ThreadMode { *; }


##---------------End: proguard configuration ----------
